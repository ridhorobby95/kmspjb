<html><head>
<meta http-equiv="content-type" content="text/html; charset=utf8"></head><body>
<?php

$orcl = oci_connect('system', 'manager', '172.16.30.215/dbperpus');
if (!$orcl) {
    die('error connect oracle');
}

$file = fopen("/var/www/html/kms_pjbservices_com.txt", "r");
$rows = array();
//nama  pemateri_unit  jabatan_tim  subyek  judul  abstrak  date1  created
$i = 0;
$field = array('nama', 'pemateri_unit', 'jabatan_tim', 'subyek', 'judul', 'abstrak', 'date1', 'created');
while(!feof($file)){
    $line = fgets($file);
	$columns = explode('|', $line);
	foreach ($columns as $k=>$column) {
		$rows[$i][$field[$k]] = $column;
	}
	$i++;
}
fclose($file);

echo '<table border="1">';
echo '<tr><th>No.</th><th>Nama</th><th>Pemateri/Unit</th><th>Jabatan/Tim</th><th>Subyek</th><th>Judul</th><th>Abstrak</th><th>Waktu</th><th>Created</th><th>SQL</th></tr>';
$i=0;
foreach ($rows as $row) {
	$i++;
	$nama = addslashes2($row['nama']);
	$jabatan_tim = addslashes2($row['jabatan_tim']);
	
	$judul = strip_tags($row['judul']);
	$judul = preg_replace('/[\?]*/', '', $judul);
	$judul = addslashes2(preg_replace('/[^\x00-\x7F]/', '', $judul));

	$abstrak = strip_tags($row['abstrak']);
	$abstrak = preg_replace('/[\?]*/', '', $abstrak);
	$abstrak = addslashes2(preg_replace('/[^\x00-\x7F]/', '', $abstrak));
	
	$created_time = "TO_DATE('{$row['created']}','YYYY.MM.DD HH24:MI:SS')";
	//$created_time = $row['created'];
	//$waktu = $row['date1'];

	$sql = '';
	if (stristr($row['nama'], 'cop') or stristr($row['nama'], 'sharing')) {
		if (stristr($row['nama'], 'cop'))
			$idjeniskm = 2;
		else
			$idjeniskm = 1;
		$waktu = "TO_DATE('{$row['date1']}','YYYY.MM.DD')";

		$stid = oci_parse($orcl, "select idsubyek from km.subyek where upper(nama)='{$row['subyek']}'");
		oci_execute($stid);
		$idsubyek = '';
		while (($row2 = oci_fetch_array($stid, OCI_BOTH))) {
			if ($row2['IDSUBYEK']) {
				$idsubyek = $row2['IDSUBYEK'];
				break;
			}
		}		
		
		$id_pemateri = '';
		$nama_pemateri = strtoupper(trim($row['pemateri_unit']));
		$stid = oci_parse($orcl, "SELECT iduser FROM um.users where upper(nama) like '{$nama_pemateri}%'");
		oci_execute($stid);
		$nama_pemateri = addslashes2($nama_pemateri);
		while (($row2 = oci_fetch_array($stid, OCI_BOTH))) {
			if ($row2['IDUSER']) {
				$id_pemateri = $row2['IDUSER'];
				break;
			}
		}		
		if ($id_pemateri) {
			$sql = "insert into km.knowledge (idjeniskm, nama, judul, pemateri, jabatanpemateri, abstrak, waktupelaksanaan, createdtime, createdby, modifiedtime) 
				values ($idjeniskm,'$nama', '$judul', $id_pemateri, '$jabatan_tim', '$abstrak', $waktu, $created_time, 1, null); ";
		}
		else {
			$sql = "insert into km.knowledge (idjeniskm, nama, judul, pemateriluar, jabatanpemateri, abstrak, waktupelaksanaan, createdtime, createdby, modifiedtime) 
				values ($idjeniskm,'$nama', '$judul', '$nama_pemateri', '$jabatan_tim', '$abstrak', $waktu, $created_time, 1, null); ";
		}
		
		if ($idsubyek) {
			$sql .= "<br/>insert into km.kmsubyek (idknowledge, idsubyek)  select idknowledge, $idsubyek from km.knowledge where idjeniskm=$idjeniskm and nama='$nama' and judul='$judul'; ";
		}
		
	}
	else if (stristr($row['nama'], 'doc')) {
		preg_match('!\d+!', $row['subyek'], $tahun);
		$tahun = (int) $tahun[0];
		
		$abstrak = $judul;
		$judul = $jabatan_tim;
		
		$jabatan_tim = '';
		
		$id_pemateri = '';
		$nama_pemateri = strtoupper($row['pemateri_unit']);
		$stid = oci_parse($orcl, "SELECT iduser FROM um.users where upper(nama) like '{$nama_pemateri}%'");
		oci_execute($stid);
		$nama_pemateri = addslashes2($nama_pemateri);
		while (($row2 = oci_fetch_array($stid, OCI_BOTH))) {
			if ($row2['IDUSER']) {
				$id_pemateri = $row2['IDUSER'];
				break;
			}
		}		
		if ($id_pemateri) {
			$sql = "insert into km.knowledge (idjeniskm, nama, judul, pemateri, jabatanpemateri,  tahuntulis, abstrak, createdtime, createdby, modifiedtime) 
				values (3,'$nama', '$judul', $id_pemateri, '$jabatan_tim', $tahun, '$abstrak', $created_time, 1, null); ";
		}
		else {
			$sql = "insert into km.knowledge (idjeniskm, nama, judul, pemateriluar, jabatanpemateri,  tahuntulis, abstrak, createdtime, createdby, modifiedtime) 
				values (3,'$nama', '$judul', '$nama_pemateri', '$jabatan_tim', $tahun, '$abstrak', $created_time, 1, null); ";
		}
	}
	else if (stristr($row['nama'], 'innovation')) {
		$idjeniskm = 4;
		
		$kodeunit = '';
		$unit = strtoupper(trim($row['pemateri_unit']));
		$stid = oci_parse($orcl, "SELECT kodeunit FROM um.unit where upper(nama) like '{$unit}%'");
		oci_execute($stid);
		while (($row2 = oci_fetch_array($stid, OCI_BOTH))) {
			if ($row2['KODEUNIT']) {
				$kodeunit = $row2['KODEUNIT'];
				break;
			}
		}
		
		$waktu = "TO_DATE('{$row['date1']}','YYYY.MM.DD')";
		
		$sql = "insert into km.knowledge (idjeniskm, nama, judul, kodeunit, abstrak, waktupelaksanaan, createdtime, createdby, modifiedtime) 
			values ($idjeniskm,'$nama', '$judul', '$kodeunit', '$abstrak', $waktu, $created_time, 1, null); ";
		
	}
	
	echo '<tr>';
/*	echo '<td valign="top">' . $i . '.</td>';
	echo '<td valign="top">' . $row['nama'] . '</td>';
	echo '<td valign="top">' . $row['pemateri_unit'] . '</td>';
	echo '<td valign="top">' . $row['jabatan_tim'] . '</td>';
	echo '<td valign="top">' . $row['subyek'] . '</td>';
	echo '<td valign="top">' . $judul . '</td>';
	echo '<td valign="top">' . $abstrak . '</td>';
	echo '<td valign="top">' . $row['date1'] . '</td>';
	echo '<td valign="top">' . $row['created'] . '</td>'; */
	echo '<td valign="top">' . $sql . '</td>';
	echo '</tr>';
	echo "\r\n";
}
echo '</table>';

function addslashes2($str) {
	$str = preg_replace("/'/", "''", $str);
	$str = preg_replace("/&/", " & ", $str);
	return $str;
}
?>
</body>
</html>
