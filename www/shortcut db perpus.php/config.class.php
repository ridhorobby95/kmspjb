<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

class Config {
	// Koneksi ke Server Perpustakaan
	const server = '192.168.3.95';//'172.16.30.215';
	const driver = 'oci8po'; 
	const database = 'dbperpusprd';
	const schema = 'perpus';
	const user = 'PERPUS';
	const password = 'manager';
        	
	// Koneksi ke Server Authentication	
	const serverGate = '192.168.3.95';//'172.16.30.215';
	const driverGate = 'oci8po'; 
	const databaseGate = 'dbperpusprd';
	const schemaGate = 'um';
	const userGate = 'um';
	const passwordGate = 'um';
	
	// Koneksi ke Server Lampiran Perpustakaan
	const serverLamp = '192.168.3.95';//'172.16.30.215';
	const driverLamp = 'oci8po'; 
	const databaseLamp = 'dbperpusprd';
	const schemaLamp = 'lp';
	const userLamp = 'lp';
	const passwordLamp = 'lp';
	
	const acceptUnit = 'KP_##_KO_##_JP';
	
	const pageTitle = "Digital Library PJB";
	const webUrl = "http://192.168.3.94/digilib/index.php";
	const gateUrl = "http://192.168.3.94/digilib/index.php?page=home";
	const sysout = "http://192.168.3.94/digilib/index.php?page=sys_logout";
	const logoutUrl = "http://192.168.3.94/digilib/home.php";
	const fotoUrl = "http://192.168.3.94/perpus/images/perpustakaan/";
	const fotoPegUrl = "http://192.168.3.94/sdm/sdm/up_l04ds/fotopeg/";
	const fotoMhsUrl = "http://192.168.3.94/siakad/siakad/uploads/fotomhs/";

	const dirFoto = "/var/www/html/perpus/images/perpustakaan/";
	const dirFotoMhs = "/var/www/html/siakad/siakad/uploads/fotomhs/";
	const dirFotoPeg = "/var/www/html/sdm/sdm/up_l04ds/fotopeg/";
        
	const pagePath = "html";
	const pageDef = "home";
	
	const pageErr = "error404";
	const pathSeparator = "/";
	const email = "abu@sevima.com";
	const rpcsalt = "5mE0t6wrLCndHEOu5bQ1jJJ332PyJ2B9"; //  setiap client harus menyamakan nilainya dengan nilai di server ini
	const language = "id";
	const extLanguage = ".php";
	const pathLanguage = "includes/languages/";
	
        #Email
	/*
        //const SMTPHost = 'smtp.pjbservices.com';*/
        /*const SMTPHost = 'mail2.pjbservices.com';
	const SMTPUser = 'lib@pt.pjbservices.com';
	const SMTPPass = '123456';
	const SMTPPort = '25';*/
        
	/*const SMTPHost = 'mail.poltekkes-denpasar.ac.id';
	const SMTPUser = 'cac@poltekkes-denpasar.ac.id';
	const SMTPPass = 'direktorat2015';
	const SMTPPort = 26;*/
        
	const SMTPHost = 'ssl://smtp.gmail.com';
	const SMTPUser = 'lib@ptpjb.com';
	const SMTPPass = 'PJBS1414';
	const SMTPPort = '465';
        
	#km setting
	const G_SESSION = 'PJBS';
	const KMHOME = 'http://192.168.3.94/';
	const entranceUrl = 'http://192.168.3.94';
	const pictProf = "http://192.168.3.94/index.php/publ1c/profile?q=";
}

?>