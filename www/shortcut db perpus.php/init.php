<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	define(G_SESSION, 'PJBS');
	session_start();
	
	// gunakan buffer agar kalau ada tulisan masih bisa redirect
	ob_start();
	
	require_once('config.class.php');
	require_once('factory.class.php');
	require_once('ui.class.php');
	require_once('helper.class.php');
	require_once('language.class.php');
	require_once('html/_language.php');
	require_once('classes/perpus.class.php');
	
	Helper::normalizeRequest();
	
	$conn = Factory::getConn();
	//$connGate = Factory::getConnGate();
		
	//if(substr($_SERVER['REMOTE_ADDR'],-3) == ".185"){		
		$conn->debug = false;
	//}

	if($_SERVER['REMOTE_ADDR'] == "10.7.254.10"){		
		// $conn->debug = true;
	}

?>