<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	$lang['LOGIN'] = 'Login';
	$lang['USERNAME'] = 'Username';
	$lang['PASSWORD'] = 'Password';
	$lang['FORGOTPASS'] = 'Forgot Password ?';
	$lang['SD_NEWS'] = 'News';
	$lang['SD_SEARCH'] = 'Search...';
	$lang['INBOX'] = 'Inbox';
	$lang['BILL'] = 'Bill';
	$lang['STATISTIK'] = 'Statistic';
	$lang['ST_PENGUNJUNG'] = 'Visitor Today :';
	$lang['ST_ONLINE'] = 'Online :';
	$lang['ST_TOTAL'] = 'Total Visitor :';
	$lang['ALERT_SEARCH'] = 'Keyword can not be empty';
	$lang['ABOUT_SEVIMAPERPUS'] = 'About the Library SEVIMA';
	
	$lang['INFO_USER'] = 'User Information';
	$lang['MEMBERNAME'] = 'Member Name';
	$lang['MEMBERTYPE'] = 'Member Type';
	$lang['LOGINTIME'] = 'Login Time';
	$lang['LASTLOGIN'] = 'Last Login';
	$lang['LOGOUTMSG'] = "[Don't Forget to logout]";
	
	$lang['SKORSING'] = 'You get skorsing !';
	$lang['TGL_SKOR'] = 'Finishing date at';
	
	$lang['INFORMATION'] = 'Information';
	$lang['FACILITY'] = 'Facilities';
	$lang['SERVICE'] = 'Services';
	$lang['COLLECTION'] = 'Collection';
	$lang['MEMBERSHIP'] = 'Membership';
	$lang['CONTACT'] = 'Contact Us';
	$lang['CARIPOJOK'] = 'Library Search :';
?>