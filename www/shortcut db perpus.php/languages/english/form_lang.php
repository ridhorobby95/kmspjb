<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	$lang['NAME'] = 'Name';
	$lang['MEMBER_NAME'] = 'Member Name';
	$lang['GENDER'] = 'Gender';
	$lang['ADDRESS'] = 'Address';
	$lang['CITY'] = 'City';
	$lang['POSTAL_CODE'] = 'Postal Code';
	$lang['TELP'] = 'Phone';
	$lang['HP'] = 'Handphone';
	$lang['MEMBER_TYPE'] = 'Member Type';
	$lang['EMAIL'] = 'Email';
	$lang['ADVICE'] = 'Critique and Advice';
	$lang['PASS_CHANGE'] = 'Change Password';
	$lang['PASS_OLD'] = 'Old Password';
	$lang['PASS_NEW'] = 'New Password';
	$lang['PASS_NEWR'] = 'New Password [Repeat]';
	$lang['DATE'] = 'Date';
	$lang['DATE_REQUEST'] = 'Request Date';
	$lang['PAGE'] = 'Page';
	$lang['TOT_TOPIC'] = 'Total Topics';
	$lang['TOT_POST'] = 'Total Posts';
	$lang['TOT_REP'] = 'Total Repplies';
	$lang['NEW_POST'] = 'New Post';
	$lang['DET_SEE'] = 'Detail';
	$lang['NO_CALL'] = 'Call Number';
	$lang['DDC_CODE'] = 'DDC Code';
	$lang['EDITION'] = 'Edition';
	$lang['KEYWORD'] = 'Keyword';
	$lang['CLASSIFICATION'] = 'Classification';
	$lang['BIBLIO_TITLE'] = 'Bibliography Title';
	$lang['BIBLIO_TYPE'] = 'Bibliography Type';
	$lang['AUTHOR'] = 'Author';
	$lang['AUTHOR1'] = 'Author 1';
	$lang['AUTHOR2'] = 'Author 2';
	$lang['AUTHOR3'] = 'Author 3';
	$lang['TOPIC'] = 'Topic';
	$lang['LANGUAGE'] = 'Language';
	$lang['YEAR'] = 'Year';
	$lang['YEAR_PUBLISH'] = 'Publisher Year';
	$lang['DIRECTORY'] = 'Directory';
	$lang['DEPARTEMENT'] = 'Departement';
	$lang['TYPE'] = 'Type';
	$lang['REG_COMP'] = 'Reg Comp';
	$lang['LOAN_DATE'] = 'Loan Date';
	$lang['EXPIRED_DATE'] = 'Expired Date';
	$lang['RETURN_DATE'] = 'Return Date';
	$lang['RESERVATION_DATE'] = 'Reservation Date';
	$lang['EXTENSION'] = 'Extension';
	$lang['PRICE'] = 'Price';
	$lang['PUBLISHER'] = 'Publisher';
	$lang['ISBN'] = 'ISBN';
	$lang['DESC'] = 'Description';
	$lang['REQ_STATE'] = 'Request State';
	$lang['PAGU_REMAINING'] = 'Remaining Pagu';
	$lang['PAGU_SHARE'] = 'Share Pagu';
	$lang['TITLE'] = 'Title';
	$lang['MESSAGE'] = 'MESSAGE';
	$lang['LOCATION'] = 'Location';
	$lang['STATE'] = 'AVAILABILITY';
	$lang['COPIES'] = 'Copies';
	$lang['SYNOPSIS'] = 'Synopsis';
	$lang['DIMENSION'] = 'Dimension';
	$lang['RESULT_FOR'] = 'Result For';
	$lang['FROM_RESULT'] = 'from';
	$lang['SEARCH_RESULT'] = 'Result Search :';
	$lang['VAL_CODE'] = 'Enter the validation code';
	$lang['EMAIL_ADDRESS'] = 'E-mail';
	$lang['FP_TEXT'] = 'Enter your email address below. If you do not already have<br /> email address or forgotten, please contact Administrator.';
	$lang['s_title'] = 'By Title &nbsp;&nbsp;&nbsp;';
	$lang['s_author'] = 'By Author';
	$lang['MSG_NOTIF'] = 'Message Notification';
	$lang['ISOK'] = 'Is Valid?';
	$lang['PRODI'] = 'Dept.';
	
	$lang['PENULIS'] = 'Author';
	$lang['CONTRIBUTOR1'] = 'Contributor 1';
	$lang['CONTRIBUTOR2'] = 'Contributor 2';
	$lang['CONTRIBUTOR3'] = 'Contributor 3';
	$lang['NRP'] = 'NPP';
	$lang['NRP1'] = 'NPP Author';
	$lang['NRP2'] = 'NRP Author 2';
	$lang['NRP3'] = 'NRP Author 3';
	$lang['NPK'] = 'NPK';
	$lang['LIST_NOTIFIKASI'] = 'Notifications List';
	$lang['NOTIFIKASI_FROM'] = 'Notification From';
	$lang['REQUEST_VAL'] = 'List of Request Validation';	
	$lang['LIST_MONITORING'] = 'List of Upload Independently';	
	
	$lang['DATE_REQUEST_MHS'] = 'Request Date';	
	$lang['DATE_NOTIF'] = 'Notification Date';	
	$lang['SENDER'] = 'Sender';
	$lang['BIBLIO_TITLE_TA'] = 'Final Project Title';	
	$lang['FAK_JUR'] = 'Faculty/Department';
	$lang['STATE_VAL'] = 'Status Validasi';
	$lang['JABATAN'] = 'Position';
	
	//button
	$lang['SENT'] = 'Sent';
	$lang['SAVE'] = 'Save';
	$lang['RESET'] = 'Reset';
	$lang['CHANGE'] = 'Change';
	$lang['DELETE'] = 'Delete';
	$lang['B_SEARCH'] = 'Search';
	$lang['SIMPLE_SEARCH'] = 'Simple Search';
	$lang['ADV_SEARCH'] = 'Advanced Search';
	$lang['B_RES'] = 'Reserv';
	$lang['B_CANCELCART'] = 'Cancel Cart';
	$lang['ACTION'] = 'Action';	
	$lang['ADD'] = 'Add';	
	$lang['ADDFILE'] = 'Add File';
	$lang['REQUEST'] = 'Request Validation';	
	$lang['PRINT'] = 'Print';
	
	//untuk header
	$lang['EDIT_PROFILE'] = 'Edit Profile';
	$lang['RESULT_SEARCH'] = 'Result Search';
	$lang['RES_LIST'] = 'Reservation List';
	
	//untuk pesan
	$lang['EMPTY_TABLE'] = "Sorry, Data Can't be found";
	$lang['WAIT'] = 'Waiting';
	$lang['ALERT_SEARCH'] = 'ALERT: Seacrh Key must be contained!!';
	$lang['LOG_ERR'] = "Trouble with login";
	$lang['LOG_SUCC'] = "Login Success";
	$lang['LOG_FAIL'] = "Login Failed";
	$lang['P_SOKRS'] = "You have skors";
	$lang['P_DELSUCCESS'] = "Delete Succes";
	$lang['P_DELFAIL'] = "Delete Failed";
	$lang['P_SAVESUCCESS'] = "Save Succes";
	$lang['P_SAVEFAIL'] = "Save Failed";
	$lang['P_CANCELSUCCESS'] = "Drop Succes";
	$lang['P_CANCELFAIL'] = "Drop Failed";
	$lang['P_SHAREPAGU'] = "Click if want to share bibliography request with your friend";
	$lang['P_EMPTYSYNOPSIS'] = "-";
	$lang['P_AVAILABLE'] = "Available";
	$lang['P_LOANS'] = "Loaned";
	$lang['P_RESERVED'] = "Reserved";
	$lang['P_LOANMEMBERDATA'] = "Loans Member Data";
	$lang['P_RESMEMBERDATA'] = "Reservation Member Data";
	$lang['P_MEMBERONLY'] = "(Member Only)";
	$lang['FP_EMAIL'] = "Please fill your email address";
	$lang['FP_PROBLEM'] = "There are problems with the password reset process";
	$lang['FP_SENT'] = "Validation code has been sent to your e-mail address";
	$lang['FP_VALERROR'] = "Validation is wrong, please repeated";
	$lang['FP_EMAILERROR'] = "Email is not listed, please contact the Administrator";
	$lang['FP_RESETERROR'] = "Reset password failed, please try again later";
	
	//untuk title page
	$lang['LIST'] = 'List';
	$lang['T_CONTACT'] = 'Online Contact List';	
	$lang['T_HOME'] = 'Home Page';
	$lang['T_LINK'] = 'Related Link';
	$lang['T_NEWBOOK'] = 'New Book/Journal';
	$lang['T_RESERVASI'] = 'Data Reservation';
	$lang['T_LOAN'] = 'Loan History';
	$lang['T_LOANBIBLIO'] = 'Loans';
	$lang['T_REQUESTPUBLIC'] = 'General Request';
	$lang['T_HISREQUESTPUBLIC'] = 'History General Request';
	$lang['T_REQUESTGREEN'] = 'Green Request';
	$lang['T_HISREQUESTGREEN'] = 'History Green Request';
	$lang['T_ADDNEWPOST'] = 'Add New Post';
	$lang['T_UPLOAD'] = 'Form Upload Final Project';
	$lang['FORGOT_PASS'] = 'Forgot Password';
	
	//keterangan tambahan
	$lang['LASTEST_POST'] = 'Lastest Post';
	$lang['BY'] = 'By';
	$lang['AT'] = 'at';
	$lang['POST_BY'] = 'Posting By';
	$lang['EKS_NULL'] = 'Copies can\'t be borrow';
	
	//keterangan request
	$lang['Wait'] = 'Waiting';
	$lang['PO'] = 'Purchase Order';
	$lang['verpus'] = 'Library Verification';
	$lang['versup'] = 'Supplier Verification';
	$lang['realisasi'] = 'realization';
	
?>