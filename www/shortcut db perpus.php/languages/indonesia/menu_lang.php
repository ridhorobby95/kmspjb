<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	$lang['HOME'] = 'Beranda';
	$lang['DAFTAR_PUSTAKA'] = 'Daftar Pustaka';
	$lang['SEMUA_DAFTAR'] = 'Semua Daftar';
	$lang['RESERVASI'] = 'Pemesanan Pustaka';
	$lang['SEARCH'] = 'Pencarian Pustaka';
	$lang['TA'] = 'Tugas Akhir';
	$lang['INFORMATION'] = 'Informasi';
	$lang['NEWS'] = 'Berita';
	$lang['CONTACT'] = 'Hubungi Kami';
	$lang['ONLINE'] = 'Kontak Online';
	$lang['REPORT'] = 'Laporan Peminjaman';
	$lang['OFFER'] = 'Pengusulan Pustaka';
	$lang['OFFER_GREEN'] = 'Pengusulan Label Hijau';
	$lang['OFFER_PUBLIC'] = 'Pengusulan Biasa';
	$lang['PROFILE'] = 'Informasi Biodata';
	$lang['EXIT'] = 'Keluar';
	$lang['CHOOSE'] = 'Pilih Bahasa';
	$lang['UPLOADTA'] = 'Upload Tugas Akhir';
	$lang['UPLOADFILETA'] = 'Upload File Tugas Akhir';
	$lang['TCUPLOAD'] = 'Tata Cara Upload TA';
	$lang['VALIDASI'] = 'Validasi TA';
	$lang['NOTIFIKASI_TA'] = 'Daftar Permintaan Validasi';
	$lang['LIST_NOTIFIKASI'] = 'Daftar Notifikasi';
	$lang['LIST_MONITORING'] = 'Daftar Upload Mandiri';
	
	$lang['ABOUT_US'] = 'Tentang Kami';
	$lang['CATDIR'] = 'Kategori Direktori';
	//breadcrumb
	$lang['CONTACT_US'] = 'Hubungi Kami';	
	$lang['PROPOSED'] = 'Usulan Biasa';	
	$lang['PROPOSED_LECTURE'] = 'Usulan Label Hijau';	
	$lang['Facility'] = 'Fasilitas';	
	$lang['Profile'] = 'Biodata';		
	$lang['TOPIC'] = 'Topik';	
	$lang['DET_NEWS'] = 'Detail Berita';	
	$lang['CART'] = 'Keranjang';					
	$lang['LOAN'] = 'Peminjaman';			
	$lang['POST_DATA'] = 'Data Post';		
	$lang['BIBLIO'] = 'Pustaka';		
	$lang['B_FORMTA'] = 'Form Upload Tugas Akhir';		
	$lang['HIST_NOTIF'] = 'Daftar History Notifikasi';		
?>