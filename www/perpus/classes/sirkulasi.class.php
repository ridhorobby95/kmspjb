<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	
	class Sirkulasi {
		
		function setAvailable($conn,$arrkey){
			$record = array();
			$record['kdkondisi'] = 'V';
			$record['statuseksemplar'] = 'ADA';
			Helper::Identitas($record);
			
			$recolah['isfinish'] = 1;
			Helper::Identitas($recolah);
			$sql = "select ideksemplar from pp_eksemplarolah where ideksemplarolah in ('$arrkey') and stssirkulasi=1 order by ideksemplar";
			$rs = $conn->Execute($sql);
			while ($row = $rs->FetchRow()){
				$err = Query::recUpdate($conn,$recolah,'pp_eksemplarolah',"ideksemplar=$row[ideksemplar]");
				$err = Query::recUpdate($conn,$record,'pp_eksemplar',"ideksemplar=$row[ideksemplar]");
			}
			
			return $err;
		}
	}

?>