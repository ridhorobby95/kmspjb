<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	
	class Referensi {
	
		function deleteTopik($conn,$key) {
			$err = Query::qDelete($conn,'pp_kontakonline',"idparentol=$key");
			
			if (!$err)
				$err = Query::qDelete($conn,'pp_kontakonline',"idkontakol=$key");			
				
			return $err;
		}
		
		function deleteComments($conn,$key) {
			$err = Query::qDelete($conn,'pp_kontakonline',"idkontakol=$key");
			return $err;
		}
		
	}

?>