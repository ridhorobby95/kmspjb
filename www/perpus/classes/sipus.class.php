<?php	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

// Class untuk perpustakaan
class Sipus {
	
	# fungsi class Biasa
	function InsertBiasa($conn,$record,$table) {
		if($table == "ms_pustaka" or $table == "pp_eksemplar")
			$record['t_entrytime'] = date('Y-m-d H:i:s');
		
		$col = $conn->Execute("select * from $table where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record,true); 
		$conn->Execute($sql);
		
		return $conn->ErrorNo();
	}	

	function UpdateBiasa($conn,$record,$table,$kunci,$kondisi) {
		$col = $conn->Execute("select * from $table where $kunci = '$kondisi'");
		$sql = $conn->GetUpdateSQL($col,$record,false,true);
		
		if($sql !='')
			$conn->Execute($sql);
			
		return $conn->ErrorNo();
	}
	
	function InsertBlob($conn,$record,$table,$path,$f_id,$f_blob){
		$err = Sipus::InsertBiasa($conn,$record,$table);
		if($err==0){
			$id = $conn->GetOne("select max($f_id) from $table");
			$conn->UpdateBlobFile($table,$f_blob,$path,"$f_id = $id");
			return $conn->ErrorNo();
		}else
			return $err;
	}
	
	function UpdateBlob($conn,$record,$table,$path,$f_id,$f_blob,$id){
		$err = Sipus::UpdateBiasa($conn,$record,$table,$f_id,$id);
		if($err==0){
			$conn->UpdateBlobFile($table,$f_blob,$path,"$f_id = $id");
			return $conn->ErrorNo();
		}else
			return $err;
	}
	
	function loadBlobFile($file, $type = NULL){
		if(empty($file)){
			return null;
		}else{
			if($type == 'jpg'){
				$image = new Imagick();
				$image->readimageblob($file);
				return "data:image/png;base64," .  base64_encode($image->getimageblob());
			}
		}
	}
	
	function LoadBlob($conn,$file,$id,$lobcolname,$bindvars = array()){

		$col = strtoupper($lobcolname);
		//$sql = "SELECT ISIFILE FROM lampiranperpus where jenis = 1 and idlampiran = $id";
		
		$stid = oci_parse($conn, $sql);
		if(empty($stid)) return null;
		foreach ($bindvars as $bv) {
			// oci_bind_by_name(resource, bv_name, php_variable, length)
			oci_bind_by_name($stid, $bv[0], $bv[1], $bv[2]);
		}
		oci_execute($stid);
		$row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
		$lob = null;
		if (is_object($row[$col])) {
			$lob = $row[$col]->load();
			$row[$col]->free();
		}
		$stid = null;
		return($lob);
	}
		
	function UpdateSpecial($conn,$record,$table,$kunci1,$kondisi1,$kunci2,$kondisi2) {
		$col = $conn->Execute("select * from $table where $kunci1 = '$kondisi1' and $kunci2 = '$kondisi2'");
		$sql = $conn->GetUpdateSQL($col,$record,false,true);
		
		
		if($sql !='')
			$conn->Execute($sql);
			
		return $conn->ErrorNo();
	}
	
	function UpdateComplete($conn,$record,$table,$where) {		
		$col = $conn->Execute("select * from $table where $where");
		$sql = $conn->GetUpdateSQL($col,$record,false,true);
		
		
		if($sql !='')
			$conn->Execute($sql);
			
		return $conn->ErrorNo();
	}
	
	function GetIdAnggota($conn,$jns){
		$biaya=$conn->GetOne("select biayakeanggotaan from lv_jenisanggota where kdjenisanggota='$jns'");
		
		if($biaya!=''){
			$sql_c = "select kdjenisanggota from lv_jenisanggota where biayakeanggotaan is not null";
			$getId = $conn->GetOne("select max(idanggota) as maxid from ms_anggota where kdjenisanggota in ($sql_c) and date_part('year',tgldaftar)='".date('Y')."' and substring(idanggota,1,4)='99".substr(date('Y'),2,2)."'");
			
			$id_temp = $getId+1;
			if(!$getId)
				$idok="99".date('y').Helper::strPad($id_temp,'4','0','l');
			else
				$idok=$id_temp;
				
			$ketemu = true;
			while($ketemu){
				#cek hasil $idok di tabel ms-anggota
				$ada = $conn->GetOne("select count(*) as maxid from ms_anggota where idanggota='$idok'");
				
				if($ada > 0) {
					#jika ketemu terus
					$idok = $idok+1;
				}else {
					#jika tidak ketemu maka $ketemu di set false
					$ketemu = false;
				}
			}
			
		}else {
			$getId = Sipus::GetLast($conn,ms_anggota,idanggota);
			$idok = $getId;
		}
	
	return $idok;
	
	}
	
	function NamaUnit($conns){
		//$conns = Factory::getConnSdm();
		$satker=$conns->Execute("select * from ms_satker order by kdsatker");
		$show=array();
		while($rs=$satker->FetchRow()){
			$show[$rs['kdsatker']]=$rs['kdsatker']." - ".$rs['namasatker'];		
		}	
		return $show;
	}
	
	function getUnitIn($conns,$idIn){
		$conns = Factory::getConnSdm();
		$satker=$conns->GetOne("select s.namasatker from ms_pegawai p
								join ms_satker s on p.idsatker=s.idsatker
								where p.npk='$idIn'");
		
		return $satker;
	}
	
	function Search($conn,$table,$kunci,$kondisi,$show){
		$p_search="select * from $table where $kunci = '$kondisi'";
		$rs_s=$conn->GetRow($p_search);
		
		return $rs_s[$show];
	}
	function SearchL($conn,$table,$where,$show){
		$p_search="select * from $table where $where";
		$rs_s=$conn->GetRow($p_search);
		
		return $rs_s[$show];
	}
	
	function UpdateTrans($conn,$rectrans,$key){
		$coltrans = $conn->Execute("select * from pp_transaksi where ideksemplar=$key and statustransaksi='1'");
		$sqltrans = $conn->GetUpdateSQL($coltrans,$rectrans,false,true);
	
		if($sqltrans !='')
			$conn->Execute($sqltrans);
			
		return $conn->ErrorNo();
	}
	
	function DeleteComplete($conn,$table,$kondisi) {
		$sql="delete from $table $kondisi";
		$conn->Execute($sql);
		
		return $conn->ErrorNo();
		
	}
	
	function DeleteBiasa($conn,$table,$kunci,$kondisi) {
		$sql="delete from $table where $kunci = '$kondisi'";
		$conn->Execute($sql);
		
		return $conn->ErrorNo();
		
	}
	
	function GetLast($conn,$table,$field){
		$sql="select max($field) as maxid from $table"; 
		$exe=$conn->GetRow($sql);
		$lastnew=$exe['maxid']+1;
		
		return $lastnew;	
	}
	
	function GetCount($conn,$kondisi){
		$sql="select count(*) as jumlah from pp_eksemplar where idpustaka='$kondisi'";
		$exeC=$conn->GetRow($sql);
		$count=$exeC['jumlah']+1;
		
		return $count;
	}
	
	function GetSeri_OLD($conn, $kdjenispustaka){ 
		$getseri=$conn->GetRow("select max(noseri) as maxseri from ms_pustaka p where p.kdjenispustaka = '$kdjenispustaka' and p.noseri like '$kdjenispustaka%' ");	
		$noseribaru = $kdjenispustaka.str_pad(str_replace($kdjenispustaka,'',$getseri['maxseri'])+1,5,'0',STR_PAD_LEFT);

		return $noseribaru;
	}
	
	function GetSeri($conn, $kdjenispustaka){ 
		$getseri=$conn->GetRow("select max(idpustaka) as maxseri from ms_pustaka p");	
		$noseribaru = $getseri['maxseri']+1;

		return $noseribaru;
	}
	
	function GetESeri($conn, $idpustaka, $seripustaka){
		$getseri=$conn->GetOne("select max(substr(noseri,length(noseri)-1,2)) as maxseri from pp_eksemplar where idpustaka = '$idpustaka' ");	
		$iscount=str_pad((int)$getseri+1,2,'0',STR_PAD_LEFT);
		$noseribaru = $seripustaka.'.'.$iscount;
		return $noseribaru;
	}
	
	function &CreateSeri($conn,$noseri,$r_id){
			$countP=Sipus::GetCount($conn,$r_id);
			$iscount=str_pad($countP,2,'0',STR_PAD_LEFT);
			$serie = $noseri.'.'.$iscount;
			
			return $serie;
	}
	
	function GetOperator($conng){
		$namapetugas=$conng->GetOne("select nama from users where nid='".$_SESSION['PERPUS_USER']."'");
		
		return $namapetugas;
	}
	
	//khusus class untuk ms pustaka
	function InsertRef($conn,$table,$field1,$field2,$arr1,$id) {
		$query="insert into $table ($field1,$field2) VALUES ";
		$insertstr = "";
		foreach($arr1 AS $key => $value) {
			$value=Helper::removeSpecial($value);
			if (strlen($insertstr)>0)
			$insertstr .=  ", ";
			
			$insertstr .=  "('" . $value . "'," . $id . ")"; 
		}
		$query .= $insertstr;
		$conn->Execute($query);
	}
	
	function InsertAuthor($conn,$table,$field1,$field2,$field3,$arr1,$id) {
	$query="insert into $table ($field1,$field2,$field3) VALUES ";
			$insertstr = "";
			foreach($arr1 AS $key => $value) {
				$value=Helper::removeSpecial($value);
				if (strlen($insertstr)>0)
				$insertstr .=  ", ";
				
				$insertstr .=  "('" . $value . "'," . $id . ",'".$key."')"; 
			}
			$query .= $insertstr;
			$conn->Execute($query);
	}
	
	//khusus TTB
	function insertTTB($conn,$arrid,$arrqty,$idttb) {
	
		$query="insert into pp_orderpustakattb (idorderpustaka,idttb,qtyttbdetail,ststtbdetail,t_user,t_updatetime,t_host) VALUES (";
		$data=array();
			foreach($arrid AS $j => $val) {
				$data[] =Helper::removeSpecial($val) . ",'". $idttb ."','" . Helper::removeSpecial($arrqty[$j]) . "','1','".$_SESSION['PERPUS_USER']."','".date('Y-m-d H:i:s')."','".$_SERVER['REMOTE_ADDR']."'";
			}
			$query .= implode("),(",$data) . ")";
			$conn->Execute($query);
			return $conn->ErrorNo();
	}
	function getPaguAktif($conn){
		$pagu=$conn->GetRow("select periodepagu from pp_settingpagu where
							 '".date('Y-m-d')."' >= ((periodepagu-1)||'-'||bulanstart||'-01') 
							 and '".date('Y-m-d')."' < ((periodepagu)||'-'||bulanend+1||'-14') ");
		
		return $pagu['periodepagu'];
		
	}
	
	function InsertPagu($conn,$anggota,$idorder,$usulan) {
		$periode = Sipus::getPaguAktif($conn);
		$query="insert into pp_paguusulan (idorderpustaka,idanggota,paguusulan,ispengusulutama,periodeakad,t_user,t_updatetime,t_host) VALUES ";
		$insertstr = "";
		foreach($anggota AS $key => $value) {
			$value=Helper::removeSpecial($value);
			if (strlen($insertstr)>0)
			$insertstr .=  ", ";
			
			if($key==0)
			$id=1;
			else
			$id=0;
			
			$insertstr .=  "('".$idorder."','" . $value . "','". Helper::removeSpecial($usulan[$key]) ."'," . $id . ",'".$periode."','".$_SESSION['PERPUS_USER']."','".date('Y-m-d H:i:s')."','".$_SERVER['REMOTE_ADDR']."')"; 
			
		}
		$query .= $insertstr;
		$conn->Execute($query);
	}
	
	function UpdatePaguS($conn,$anggota,$usulan,$thpagu,$op){
		$jumlah=count($anggota);
		
		for($i=0;$i<$jumlah;$i++){
		$idanggota=Helper::removeSpecial($anggota[$i]);
		$ispagu=$conn->GetRow("select sisasementara from pp_pagulh where idanggota='$idanggota' and periodeakad='$thpagu'");
		if($op=='-')
		$recpagu['sisasementara']=$ispagu['sisasementara']-$usulan[$i];
		else if($op=='+')
		$recpagu['sisasementara']=$ispagu['sisasementara']+$usulan[$i];
		
		Helper::Identitas($recpagu);

		Sipus::UpdateSpecial($conn,$recpagu,pp_pagulh,idanggota,Helper::removeSpecial($anggota[$i]),periodeakad,$thpagu);
		
		}
	}
	
	function UpdatePagu($conn,$order,$thpagu){
		$p_order=$conn->Execute("select * from pp_paguusulan where idorderpustaka=$order");
		if(!empty($p_order)){
		while($rso=$p_order->FetchRow()){
		$idanggota=$rso['idanggota'];
		$ispagu=$conn->GetRow("select sisapagu from pp_pagulh where idanggota='$idanggota' and periodeakad='$thpagu'");
		$recpagu['sisapagu']=$ispagu['sisapagu']-$rso['paguusulan'];
		
		//echo "aaaaaaaaaaaaaaaaaaaaaaaaa".$record['sisapagu'];
		Sipus::UpdateSpecial($conn,$recpagu,pp_pagulh,idanggota,$idanggota,periodeakad,$thpagu);
		
		}
		}
	}
	
	//upload file
	function UploadPustaka($record,$upfile) {
		for ($i = 0; $i <= count ($_FILES[$upfile]['name']); $i++)
		{
			$test=($i-1)+2;
			$field = "file".$test;
			$tmp_file = $_FILES[$upfile]['tmp_name'][$i];
			$filetype = $_FILES[$upfile]['type'][$i];
			$filesize = $_FILES[$upfile]['size'][$i];
			$filename = $_FILES[$upfile]['name'][$i];
			$destination = 'uploads/' . $filename;

		if ($filetype=='application/msword' || $filetype=='application/pdf' || $filetype=='application/rtf' || $filetype=='application/vnd.ms-powerpoint') {

			if (copy($tmp_file,$destination))
				{	
					$record[$field]=$filename;
					
				}
			}
			return $record[$field];
		}
	}
	

	function UploadAbstrak($judul,$seri){
		$tmp_files = $_FILES['uploadsinopsis']['tmp_name'];
		$filetypes = $_FILES['uploadsinopsis']['type'];
		$filesizes = $_FILES['uploadsinopsis']['size'];
		$filenames = $_FILES['uploadsinopsis']['name'];

		$s_ext=explode(".",$filenames);
		$s_extjum=count($s_ext);
		$s_exts=$s_ext[$s_extjum-1];
		$fileups=$filenames;
		//File abstrak diletakkan langsung di folder upload/sinopsis tanpa membuat folder noseri yg baru 
		$directori = 'uploads/sinopsis/';
		
		if(!is_dir($directori))
				// mkdir($directori,0777);
				mkdir($directori,0750);
			
		$destinations = $directori .'/'.$fileups;
		
		if ($filesizes < 5000000){
			if ($filetypes=='application/pdf') {
				if (copy($tmp_files,$destinations))
				{	
					//$xfile=$destinations;
					// diambil nama filenya saja, 
					$xfile = $fileups;
					//Pdf::ghostFile($destinations,$seri);
					return $xfile;
				}else{
					$errdb = 'File gagal diupload.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}else {
			$errdb = 'Besar File Melebihi 5 MB.';	
			Helper::setFlashData('errdb', $errdb);
		}
	}
	
	function UploadFile($name,$seri,$pdf = true,$max = 5000000){
		$tmp_files = $_FILES[$name]['tmp_name'];
		$filetypes = $_FILES[$name]['type'];
		$filesizes = $_FILES[$name]['size'];
		$filenames = $_FILES[$name]['name'];

		$s_ext=explode(".",$filenames);
		$s_extjum=count($s_ext);
		$s_exts=$s_ext[$s_extjum-1];
		$fileups=Helper::removeSpecial($filenames,true);
		$directori = 'uploads/file/' . $seri;
		
		if(!is_dir($directori))
				mkdir($directori,0750);
			
		$destinations = $directori .'/'.$fileups;
		
		if ($filesizes < $max){
			if ($filetypes=='application/pdf') {
				if (copy($tmp_files,$destinations))
				{	
						$xfile=$destinations;
						//if($pdf==true)
						//Pdf::ghostFile($destination,$seri);
						return $xfile;
				}else{
					$errdb = 'File gagal diupload.';	
					Helper::setFlashData('errdb', $errdb);
					}
			
			}
		}else {
			$errdb = 'Besar Kapasitas File Melebihi ketentuan.';	
			Helper::setFlashData('errdb', $errdb);
		}
	}
		
	function fastback($conn,$noseri,$hariini,$ideksemplar2,$biasa=true,$bebasd=false){
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select max(ideksemplar) as ideksemplar from pp_eksemplar where ideksemplar=$ideksemplar2 ");
		$key=$p_eks['ideksemplar'];
		$p_trans=$conn->GetRow("select idtransaksi,ideksemplar,kdjenistransaksi, kdjenispustaka,idanggota, to_char(tgltransaksi,'YYYY-mm-dd') tgltransaksi,
				       tgl_tenggat tgltenggat, kdklasifikasi, (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg
				       from v_trans_list
				       where ideksemplar=$ideksemplar2 and statustransaksi='1' and kdjenispustaka in(".$_SESSION['roleakses'].")");
		$r_anggota=$p_trans['idanggota'];
		$r_idtrans=$p_trans['idtransaksi'];
		$r_jamdenda=floor($p_trans['jamdendaskrg']);
		$p_tagih=$conn->GetRow("select idtagihan from v_srttagih where idanggota='$r_anggota' and flag=1");
		$r_idtagih=$p_tagih['idtagihan'];
		
		$reserve = $conn->GetOne("select idtransaksi from pp_reservasi
					 where idtransaksi='$r_idtrans' and statusreservasi='1' and tglexpired >= current_date");
		if($reserve)
			$_SESSION['reserve'] = $r_idtrans;
		else
			unset($_SESSION['reserve']);
		
		$_SESSION['idtrans'] = $r_anggota;
		
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}else{
			if($p_trans){
				$conn->StartTrans();
				
				#update status pengembalian & tanggal pengembalian
				Helper::Identitas($rectrans);
				$rectrans['tglpengembalian']=$hariini;
				$rectrans['statustransaksi']=0;
				$rectrans['petugaskembali']=Helper::cStrNull($_SESSION['PERPUS_USER']);;
				Sipus::UpdateTrans($conn,$rectrans,$key);
						
				#update status eksemplar menjadi ada
				$receks['statuseksemplar']='ADA';
				Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,ideksemplar,$key);
				
				if($p_tagih){
					#update tgl pengembalihan pada detail tagihan
					$rectagih['tglpengembalian']=$hariini;
					Sipus::UpdateSpecial($conn,$rectagih,pp_tagihandetail,idtransaksi,$r_idtrans,idtagihan,$r_idtagih);
				}
				
				$p_anggota=$conn->GetRow("select idanggota, kdjenisanggota from ms_anggota where idanggota='$r_anggota'");
				$idanggota=$p_anggota['idanggota'];
				
				#get data uang jaminan dari jenis anggota
				$p_rupiah=$conn->GetRow("select uangjaminanpinjaman from lv_jenisanggota where kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
				$rupiah=$p_rupiah['uangjaminanpinjaman'];
				
				#get uang jaminan di anggota sekaligus update uang jaminan anggota
				$p_uangjaminan=$conn->GetRow("select uangjaminan from ms_anggota where idanggota='$idanggota'");
				$recuang['uangjaminan'] =$p_uangjaminan['uangjaminan']-$rupiah;
				Sipus::UpdateBiasa($conn,$recuang,ms_anggota,idanggota,$idanggota);
				
				
				if ($p_anggota){		
					if (strtotime($hariini)>strtotime($p_trans['tgltenggat']) and $p_trans['tgltenggat']!=''){
						if($p_trans['kdjenistransaksi'] == 'PJF'){
							$rectelat['statusanggota'] = '2';
							Sipus::UpdateBiasa($conn,$rectelat,ms_anggota,idanggota,$r_anggota);
						}else{
							$tglhariini=date("Y-m-d");
							$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur
									       from ms_libur
									       where to_char(tgllibur,'YYYY-mm-dd') >= '".$p_trans['tgltenggat']."' and to_char(tgllibur,'YYYY-mm-dd') <= '$tglhariini'
									       and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6");
							
							$range=(strtotime($tglhariini)-strtotime($p_trans['tgltenggat']))/86400;
							
							# mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
							$jmlsabtuminggu=0;
							for($k=1;$k<=$range;$k++){
								$tanggaljalan=date("Y-m-d",(strtotime($p_trans['tgltenggat'])+($k*86400)));
								if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
									$jmlsabtuminggu +=1;
								}
							}
							
							$_SESSION['anggotadenda']=$p_trans['idanggota'];
							//$sql_denda = $conn->GetRow("select denda,kdklasifikasi from lv_klasifikasi where kdklasifikasi='".$p_trans['kdklasifikasi']."'");
							$sql_denda = $conn->GetRow("select denda, satuanpinjam from pp_aturan
										   where kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
							
							
							//========================================================
							if($biasa==true or $biasa==false){
								if($sql_denda['satuanpinjam'] == "0")
									$_SESSION['telat']=$range - $p_libur['jmllibur'] - $jmlsabtuminggu;
								elseif($sql_denda['satuanpinjam'] == "1")
									$_SESSION['telat']=$r_jamdenda - ($p_libur['jmllibur']*24) - ($jmlsabtuminggu*24);
							}
													
							//insert denda ke tabel pp_denda dan update field denda di ms_anggota
							if($sql_denda['satuanpinjam'] == "0"){
								$jml_denda = ($range - $p_libur['jmllibur'] - $jmlsabtuminggu) * (int)$sql_denda['denda'];
							}elseif($sql_denda['satuanpinjam'] == "1")
								$jml_denda = ($r_jamdenda - ($p_libur['jmllibur']*24) - ($jmlsabtuminggu*24) ) * (int)$sql_denda['denda'];

							$_SESSION['denda'] = $jml_denda;
							$recdenda=array();
							Helper::Identitas($recdenda);
							$recdenda['idtransaksi']=$p_trans['idtransaksi'];
							$recdenda['keterangan']='TERLAMBAT';
							$recdenda['jml_denda']= $jml_denda;
							$recdenda['idanggota']= $p_trans['idanggota'];
							$recdenda['kdklasifikasi']= $p_trans['kdklasifikasi'];
							
							if($sql_denda['satuanpinjam'] == "0") #hari
								$recdenda['jmlhari'] = Sipus::hitung_telat($conn,$p_trans['tgltenggat'],$hariini);
							elseif($sql_denda['satuanpinjam'] == "1") #jam
								$recdenda['jmljam'] = Sipus::hitung_telatjam($conn,$p_trans['tgltenggat'],$hariini,floor($p_trans['jamdendaskrg']));
								
							if($jml_denda != 0 and $bebasd==false)
								Sipus::InsertBiasa($conn,$recdenda,pp_denda);	
							
							//akumulasi jumlah denda dengan denda yang lama jika masih punya denda
							$sql_cek_denda_lama = $conn->GetRow("select denda from ms_anggota where idanggota='".$p_trans['idanggota']."'");
							$akumulasi_denda = (int)$sql_cek_denda_lama['denda'] + $jml_denda;
							$recdendaanggota['denda'] = $akumulasi_denda;
							if($jml_denda != 0 and $bebasd==false)
								Sipus::UpdateBiasa($conn,$recdendaanggota,ms_anggota,idanggota,$r_anggota);
							//==========================================
						}
					}
					
					if($biasa==true or $biasa==false){
						$rectime['idanggota']=$r_anggota;
						if($biasa==false){
							$rectime['waktumulai']=$_SESSION['kembalicepat'];	
							$_SESSION['idcepat']=$r_anggota;
						}else{
							$rectime['waktumulai']=$_SESSION['waktu'];
						}
						$rectime['waktuselesai']=date('Y-m-d H:i:s');
						$rectime['regcomp']=$noseri;
						$rectime['status']='0';
						Helper::Identitas($rectime);
						if($p_trans['kdjenispustaka']=='B')
							Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
					}
					
					if($biasa==true)
					$_SESSION['waktu']=date('Y-m-d H:i:s');
				}
				//return $conn->ErrorNo();
				//die("SUKSES");
				$conn->CompleteTrans();
			}else {
				$errdb = 'Peminjaman untuk pustaka tersebut tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
			}
		}
	}
	
	//untuk fotocopy
	function fastback2($conn,$noseri,$hariini,$ideksemplar2,$biasa=true){
		//$hariini=date("Y-m-d");
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select max(ideksemplar) as ideksemplar from pp_eksemplar where ideksemplar=$ideksemplar2 ");
		$key=$p_eks['ideksemplar'];
		$p_trans=$conn->GetRow("select idtransaksi,ideksemplar, kdjenispustaka,idanggota, tgltransaksi, tgl_tenggat tgltenggat, kdklasifikasi from v_trans_list where 
								ideksemplar=$ideksemplar2 and statustransaksi='1' and kdjenispustaka in(".$_SESSION['roleakses'].")");
		$r_anggota=$p_trans['idanggota'];
		$r_idtrans=$p_trans['idtransaksi'];
		$p_tagih=$conn->GetRow("select idtagihan from v_srttagih where idanggota='$r_anggota' and flag=1");
		$r_idtagih=$p_tagih['idtagihan'];
		
		$reserve = $conn->GetOne("select idtransaksi from pp_reservasi where idtransaksi='$r_idtrans' and statusreservasi='1' and tglexpired >= current_date");
		if($reserve)
			$_SESSION['reserve'] = $r_idtrans;
		else
			unset($_SESSION['reserve']);
		
		$_SESSION['idtrans'] = $r_anggota;
		
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
		}else{
			if($p_trans){
				$conn->StartTrans();
				Helper::Identitas($rectrans);
				$rectrans['tglpengembalian']=$hariini;
				$rectrans['statustransaksi']=0;
				$rectrans['petugaskembali']=Helper::cStrNull($_SESSION['PERPUS_USER']);
				Sipus::UpdateTrans($conn,$rectrans,$key);
						
				$receks['statuseksemplar']='ADA';
				Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,ideksemplar,$key);
				
				if($p_tagih){
				$rectagih['tglpengembalian']=date('Y-m-d');
				Sipus::UpdateSpecial($conn,$rectagih,pp_tagihandetail,idtransaksi,$r_idtrans,idtagihan,$r_idtagih);
				}
				
				$p_anggota=$conn->GetRow("select idanggota, kdjenisanggota from ms_anggota where idanggota='$r_anggota'");
				$idanggota=$p_anggota['idanggota'];
				// $p_rupiah=$conn->GetRow("select uangjaminanpinjaman from lv_jenisanggota where kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
				// $rupiah=$p_rupiah['uangjaminanpinjaman'];
				
				// //update uang jaminan anggota
				// $p_uangjaminan=$conn->GetRow("select uangjaminan from ms_anggota where idanggota='$idanggota'");
				
				// $recuang['uangjaminan'] =$p_uangjaminan['uangjaminan']-$rupiah;
				// Sipus::UpdateBiasa($conn,$recuang,ms_anggota,idanggota,$idanggota);
				
				if ($p_anggota){		
					if (strtotime($hariini)>strtotime($p_trans['tgltenggat']) and $p_trans['tgltenggat']!=''){ //TERLAMBAT
						$rectelat['statusanggota'] = '2';
						Sipus::UpdateBiasa($conn,$rectelat,ms_anggota,idanggota,$r_anggota);
						//==========================================
					}
					
					if($biasa==true or $biasa==false){
						$rectime['idanggota']=$r_anggota;
						if($biasa==false){
							$rectime['waktumulai']=$_SESSION['kembalicepat'];	
							$_SESSION['idcepat']=$r_anggota;
						}else{
							$rectime['waktumulai']=$_SESSION['waktu'];
						}
						$rectime['waktuselesai']=date('Y-m-d H:i:s');
						$rectime['regcomp']=$noseri;
						$rectime['status']='0';
						Helper::Identitas($rectime);
						if($p_trans['kdjenispustaka']=='B')
							Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
					}
					
					if($biasa==true)
					$_SESSION['waktu']=date('Y-m-d H:i:s');
				}
				//return $conn->ErrorNo();
				//die("SUKSES");
				$conn->CompleteTrans();
			}else {
				$errdb = 'Peminjaman untuk pustaka tersebut tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
				// Helper::redirect();
			}
		}
	}

	//perpanjangan tidak normal
	function fastnew2($conn,$noseri,$hariini,$ideksemplar,$idanggota,$biasa=true,$bebasd=false){
		//$conn->StartTrans();		
		$noseri2 = Helper::RegNew($noseri);
		
		#data eksemplar
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and (upper(noseri)=upper('$noseri') or upper(noseri)=upper('$noseri2'))");
		$key=$p_eks['ideksemplar'];
		
		#data transaksi
		$p_trans=$conn->GetRow("select t.*,a.idanggota,a.kdjenisanggota,kdklasifikasi,kdjenispustaka, to_char(t.tgltransaksi, 'YYYY-MM-DD') tgltrans,
				       to_char(t.tgltenggat, 'YYYY-MM-DD') tgl_tenggat, to_char(t.tgltenggat, 'YYYY-MM-DD HH24:mi:ss') tglenggat,
				       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg 
				       from pp_transaksi t
				       join ms_anggota a on t.idanggota=a.idanggota
				       join pp_eksemplar e on e.ideksemplar = t.ideksemplar
				       JOIN ms_pustaka p ON e.idpustaka = p.idpustaka
				       where t.ideksemplar=$key and statustransaksi='1' order by tgltransaksi desc");

		$idtrans=$p_trans['idtransaksi'];
		$r_jamdenda=floor($p_trans['jamdendaskrg']);
		$j_anggota=$p_trans['kdjenisanggota'];
		$r_anggota=$p_trans['idanggota'];
		$_SESSION['idpanjang'] = $r_anggota;
		#detail utk aturan
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul, p.kdjenispustaka
				       from pp_eksemplar e 
					join ms_pustaka p on e.idpustaka=p.idpustaka 
					where e.ideksemplar=$key and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		#data aturan
		$p_aturan=$conn->GetRow("select * from pp_aturan where kdjenisanggota='$j_anggota'");
		
		#data perpanjang ke
		$p_panjang=$conn->GetRow("select perpanjangke from v_trans_list where idtransaksi='$idtrans'");
		
		#data anggota 
		$p_anggota=$conn->GetRow("select idanggota, tglselesaiskors,statusanggota,kdjenisanggota from ms_anggota where idanggota='$r_anggota'");
		
		#data pemesanan
		$cek_reserve=$conn->GetRow("select idreservasi,idanggotapesan from pp_reservasi where ideksemplarpesan=$key and tglexpired>=current_date and statusreservasi='1' order by idreservasi");
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}else{
			if($p_anggota['statusanggota']==0){
				$errdb = 'Anggota tersebut telah nonaktif !!!';	
				Helper::setFlashData('errdb', $errdb);
				// Helper::redirect();
			} else if($p_anggota['statusanggota']==2){ 
				$errdb = 'Anggota tersebut telah terblokir !!!';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
			}else {
				if (!$p_jenis){
					$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
					Helper::setFlashData('errdb', $errdb);
				} else {	
					if (!$p_aturan){		
							$errdb = 'Data aturan peminjaman anggota tidak ditemukan.';
							Helper::setFlashData('errdb', $errdb);
							
					} else {
						if ($p_trans['tgltrans'] == date('Y-m-d')){
							$errdb = 'Perpanjangan gagal, transaksi terjadi dihari yang sama.';
							Helper::setFlashData('errdb', $errdb);
						} else {						
							if(strtotime(date("Y-m-d"))>=strtotime($p_anggota['tglselesaiskors']) or $p_anggota['tglselesaiskors']=''){
								if ($p_panjang['perpanjangke']>=$p_aturan['jumlahperpanjangan'] and $p_aturan['jumlahperpanjangan']!=null){
									$errdb = 'Masa/Batas perpanjangan telah habis.';
									Helper::setFlashData('errdb', $errdb);
								}
								else {
									if($cek_reserve){
										$errdb = 'Pustaka dalam masa terpesan oleh ID '.$cek_reserve['idanggotapesan'];
										Helper::setFlashData('errdb', $errdb);
									}
									elseif ($p_jenis['statuseksemplar']=='PJM'){

										//Cek apakah telat/ga
										if ($p_trans['tglenggat']<date('Y-m-d H:i:s')){
											$tglhariini=date("Y-m-d");
											$p_libur_telat=$conn->GetRow("select count(tgllibur) as jmllibur
														     from ms_libur
														     where to_char(tgllibur,'YYYY-mm-dd') >= '".$p_trans['tgl_tenggat']."'
														     and to_char(tgllibur,'YYYY-mm-dd') <= '$tglhariini'
														     and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6");
											
											$range=(strtotime($tglhariini)-strtotime($p_trans['tgl_tenggat']))/86400;
												
											//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
											$jmlsabtuminggu=0;
											for($k=1;$k<=$range;$k++){
												$tanggaljalan=date("Y-m-d",(strtotime($p_trans['tgl_tenggat'])+($k*86400)));
												if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
													$jmlsabtuminggu +=1;
												}
											}
											
											$_SESSION['anggotadenda']=$p_trans['idanggota'];
											$sql_denda = $conn->GetRow("select denda,satuanpinjam
														from pp_aturan
														where
															kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
											
											//========================================================
											if($biasa==true or $biasa==false){
												if($sql_denda['satuanpinjam'] == "0"){
													$_SESSION['telat']=$range - $p_libur_telat['jmllibur'] - $jmlsabtuminggu;
												}
												elseif($sql_denda['satuanpinjam'] == "1"){
													$_SESSION['telat']=$r_jamdenda - ($p_libur_telat['jmllibur']*24) - ($jmlsabtuminggu*24);
												}
											}
											
											//insert denda ke tabel pp_denda dan update field denda di ms_anggota
											if($sql_denda['satuanpinjam'] == "0"){
												$jml_denda = ($range - $p_libur_telat['jmllibur'] - $jmlsabtuminggu) * (int)$sql_denda['denda'];
											}
											elseif($sql_denda['satuanpinjam'] == "1"){
												$jml_denda = ($r_jamdenda - ($p_libur_telat['jmllibur']*24) - ($jmlsabtuminggu*24) ) * (int)$sql_denda['denda'];
											}
											
											$_SESSION['denda'] = $jml_denda;
											$recdenda=array();
											Helper::Identitas($recdenda);
											$recdenda['idtransaksi']=$p_trans['idtransaksi'];
											$recdenda['keterangan']='TERLAMBAT';
											$recdenda['jml_denda']= $jml_denda;
											$recdenda['idanggota']= $p_trans['idanggota'];
											$recdenda['kdklasifikasi']= $p_trans['kdklasifikasi'];
							
											if($sql_denda['satuanpinjam'] == "0") #hari
												$recdenda['jmlhari'] = Sipus::hitung_telat($conn,$p_trans['tgl_tenggat'],$hariini);
											elseif($sql_denda['satuanpinjam'] == "1") #jam
												$recdenda['jmljam'] = Sipus::hitung_telatjam($conn,$p_trans['tgl_tenggat'],$hariini,floor($p_trans['jamdendaskrg']));
					
											if($jml_denda != 0 and $bebasd==false)
												Sipus::InsertBiasa($conn,$recdenda,pp_denda);	
											
											//akumulasi jumlah denda dengan denda yang lama jika masih punya denda
											$sql_cek_denda_lama = $conn->GetRow("select denda from ms_anggota where idanggota='".$p_trans['idanggota']."'");
											$akumulasi_denda = (int)$sql_cek_denda_lama['denda'] + $jml_denda;
											$recdendaanggota['denda'] = $akumulasi_denda;
											if($jml_denda != 0 and $bebasd==false)
												Sipus::UpdateBiasa($conn,$recdendaanggota,ms_anggota,idanggota,$r_anggota);
										}
										
						// ============================== filtering mencari tenggat dg filter hari sabtu minggu dan hari libur ===========
										if ($p_aturan['lamaperpanjangan']!=null){
											#per jam
											if($p_aturan['satuanpinjam'] == "1"){
												$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$p_aturan['lamaperpanjangan']));
											}
											#per hari
											elseif($p_aturan['satuanpinjam'] == "0"){
											
												$tglm=date("Y-m-d");
												$tgl=strtotime(date("Y-m-d"));
												$tgl2=($tgl)+(86400*$p_aturan['lamaperpanjangan']);
												$jum=0;
												$i=$tgl2; //tgl_tenggat tujuan
												
												$h=date("w",$i);
												if($h==0){ #minggu
														$jum +=1;
													}
													
												if($h==6){ #sabtu
														$jum +=2;
													}
												$tottgl=($tgl2+($jum*86400));
												
												$tgltenggat=date("Y-m-d",$tottgl);
												$tglrange = date("Y-m-d",($tottgl+(86400*30)));
												
												$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);
											}
										}
										else
											$tglhasil=null;	
						//============================== selesei filter hari libur ======================================
										$rec=array();
										$rec['kdjenistransaksi']=$p_trans['kdjenistransaksi'];
										$rec['ideksemplar']=$p_trans['ideksemplar'];
										$rec['idanggota']=$p_trans['idanggota'];
										$rec['kdlokasi']=$p_trans['kdlokasi'];
										$rec['tgltransaksi']=date('Y-m-d H:i:s');//date("Y-m-d");
										$rec['perpanjangke']=$p_panjang['perpanjangke']+1;
										$rec['tglperpanjang']=date('Y-m-d H:i:s');//date("Y-m-d");
										$rec['tgltenggat']=$tglhasil;
										$rec['statustransaksi']=1;
										$rec['petugaspinjam']=Helper::cStrNull($_SESSION['PERPUS_USER']);
										Helper::Identitas($rec);
										$conn->StartTrans();
										Sipus::InsertBiasa($conn,$rec,pp_transaksi);
										
										$record['tglpengembalian']=date('Y-m-d H:i:s');//date("Y-m-d");
										$record['statustransaksi']=2;
										$record['petugaskembali']=Helper::cStrNull($_SESSION['PERPUS_USER']);
										Helper::Identitas($record);
										Sipus::UpdateBiasa($conn,$record,pp_transaksi,idtransaksi,$idtrans);
										
										$rectime['idanggota']=$r_anggota;
										//jenis perpanjangan
										if($biasa==true)
										$rectime['waktumulai']=$_SESSION['waktu'];
										else
										$rectime['waktumulai']=$_SESSION['panjangcepat'];
										
										$rectime['waktuselesai']=date('Y-m-d H:i:s');
										$rectime['regcomp']=$noseri;
										$rectime['status']='2';
										Helper::Identitas($rectime);
										if($p_jenis['kdjenispustaka']=='B')
										Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
										
										if($biasa==true)
										$_SESSION['waktu']=date('Y-m-d H:i:s');
									
										$conn->CompleteTrans();
										
										if($conn->ErrorNo() != 0){
											$errdb = 'Perpanjangan peminjaman gagal.';	
											Helper::setFlashData('errdb', $errdb);
										}
										else {
											$sucdb = 'Perpanjangan ke-'.$rec['perpanjangke'].' berhasil, Masa tenggat sampai '.Helper::tglEng($rec['tgltenggat']);	
											if(!$biasa){
												$info = Sipus::getDataPerpanjangan($conn,$r_anggota);
												$sucdb .= $info;
											}
											Helper::setFlashData('sucdb', $sucdb);
											Helper::redirect();
										}
									}
								}
							}else{
								$errdb = 'Perpanjangan gagal, Anggota sedang bermasalah !';	
								Helper::setFlashData('errdb', $errdb);
							}
						} //
					}
				
				}
			}
		}	//$conn->CompleteTrans();
	}
	
	//perpanjangan tidak normal
	function perpanjangan($conn,$idperpanjangan,$noseri,$hariini,$ideksemplar,$idanggota,$biasa=true,$bebasd=false){
		//$conn->StartTrans();		
		$noseri2 = Helper::RegNew($noseri);
		
		#data eksemplar
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and (upper(noseri)=upper('$noseri') or upper(noseri)=upper('$noseri2'))");
		$key=$p_eks['ideksemplar'];
		
		#data transaksi
		$p_trans=$conn->GetRow("select t.*,a.idanggota,a.kdjenisanggota,kdklasifikasi,kdjenispustaka, to_char(t.tgltransaksi, 'YYYY-MM-DD') tgltrans,
				       to_char(t.tgltenggat, 'YYYY-MM-DD') tgl_tenggat, to_char(t.tgltenggat, 'YYYY-MM-DD HH24:mi:ss') tglenggat,
				       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg 
				       from pp_transaksi t
				       join ms_anggota a on t.idanggota=a.idanggota
				       join pp_eksemplar e on e.ideksemplar = t.ideksemplar
				       JOIN ms_pustaka p ON e.idpustaka = p.idpustaka
				       where t.ideksemplar=$key and statustransaksi='1' order by tgltransaksi desc");

		$idtrans=$p_trans['idtransaksi'];
		$r_jamdenda=floor($p_trans['jamdendaskrg']);
		$j_anggota=$p_trans['kdjenisanggota'];
		$r_anggota=$p_trans['idanggota'];
		$_SESSION['idpanjang'] = $r_anggota;
		#detail utk aturan
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul, p.kdjenispustaka
				       from pp_eksemplar e 
					join ms_pustaka p on e.idpustaka=p.idpustaka 
					where e.ideksemplar=$key and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		#data aturan
		$p_aturan=$conn->GetRow("select * from pp_aturan where kdjenisanggota='$j_anggota'");
		
		#data perpanjang ke
		$p_panjang=$conn->GetRow("select perpanjangke from v_trans_list where idtransaksi='$idtrans'");
		
		#data anggota 
		$p_anggota=$conn->GetRow("select idanggota, tglselesaiskors,statusanggota,kdjenisanggota from ms_anggota where idanggota='$r_anggota'");
		
		#data pemesanan
		$cek_reserve=$conn->GetRow("select idreservasi,idanggotapesan from pp_reservasi where ideksemplarpesan=$key and tglexpired>=current_date and statusreservasi='1' order by idreservasi");
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}else{
			if($p_anggota['statusanggota']==0){
				$errdb = 'Anggota tersebut telah nonaktif !!!';	
				Helper::setFlashData('errdb', $errdb);
				// Helper::redirect();
			} else if($p_anggota['statusanggota']==2){ 
				$errdb = 'Anggota tersebut telah terblokir !!!';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
			}else {
				if (!$p_jenis){
					$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
					Helper::setFlashData('errdb', $errdb);
				} else {
					if (!$p_aturan){		
							$errdb = 'Data aturan peminjaman anggota tidak ditemukan.';
							Helper::setFlashData('errdb', $errdb);
							
					} else {
						if ($p_trans['tgltrans'] == date('Y-m-d')){
							$errdb = 'Perpanjangan gagal, transaksi terjadi dihari yang sama.';
							Helper::setFlashData('errdb', $errdb);
						} else {					
							if(strtotime(date("Y-m-d"))>=strtotime($p_anggota['tglselesaiskors']) or $p_anggota['tglselesaiskors']=''){
								if ($p_panjang['perpanjangke']>=$p_aturan['jumlahperpanjangan'] and $p_aturan['jumlahperpanjangan']!=null){
									$errdb = 'Masa/Batas perpanjangan telah habis.';
									Helper::setFlashData('errdb', $errdb);
								}
								else {
									if($cek_reserve){
										$errdb = 'Pustaka dalam masa terpesan oleh ID '.$cek_reserve['idanggotapesan'];
										Helper::setFlashData('errdb', $errdb);
									}
									elseif ($p_jenis['statuseksemplar']=='PJM'){
										//Cek apakah telat/ga
										if ($p_trans['tglenggat']<date('Y-m-d H:i:s')){
											$tglhariini=date("Y-m-d");
											$p_libur_telat=$conn->GetRow("select count(tgllibur) as jmllibur
														     from ms_libur
														     where to_char(tgllibur,'YYYY-mm-dd') >= '".$p_trans['tgl_tenggat']."'
														     and to_char(tgllibur,'YYYY-mm-dd') <= '$tglhariini'
														     and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6");
											
											$range=(strtotime($tglhariini)-strtotime($p_trans['tgl_tenggat']))/86400;
												
											//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
											$jmlsabtuminggu=0;
											for($k=1;$k<=$range;$k++){
												$tanggaljalan=date("Y-m-d",(strtotime($p_trans['tgl_tenggat'])+($k*86400)));
												if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
													$jmlsabtuminggu +=1;
												}
											}
											
											$_SESSION['anggotadenda']=$p_trans['idanggota'];
											$sql_denda = $conn->GetRow("select denda,satuanpinjam
														from pp_aturan
														where
															kdklasifikasi='".$p_trans['kdklasifikasi']."'
															and kdjenispustaka='".$p_trans['kdjenispustaka']."'
															and kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
											
											//========================================================
											if($biasa==true or $biasa==false){
												if($sql_denda['satuanpinjam'] == "0"){
													$_SESSION['telat']=$range - $p_libur_telat['jmllibur'] - $jmlsabtuminggu;
												}
												elseif($sql_denda['satuanpinjam'] == "1"){
													$_SESSION['telat']=$r_jamdenda - ($p_libur_telat['jmllibur']*24) - ($jmlsabtuminggu*24);
												}
											}
											
											//insert denda ke tabel pp_denda dan update field denda di ms_anggota
											if($sql_denda['satuanpinjam'] == "0"){
												$jml_denda = ($range - $p_libur_telat['jmllibur'] - $jmlsabtuminggu) * (int)$sql_denda['denda'];
											}
											elseif($sql_denda['satuanpinjam'] == "1"){
												$jml_denda = ($r_jamdenda - ($p_libur_telat['jmllibur']*24) - ($jmlsabtuminggu*24) ) * (int)$sql_denda['denda'];
											}
											
											$_SESSION['denda'] = $jml_denda;
											$recdenda=array();
											Helper::Identitas($recdenda);
											$recdenda['idtransaksi']=$p_trans['idtransaksi'];
											$recdenda['keterangan']='TERLAMBAT';
											$recdenda['jml_denda']= $jml_denda;
											$recdenda['idanggota']= $p_trans['idanggota'];
											$recdenda['kdklasifikasi']= $p_trans['kdklasifikasi'];
							
											if($sql_denda['satuanpinjam'] == "0") #hari
												$recdenda['jmlhari'] = Sipus::hitung_telat($conn,$p_trans['tgl_tenggat'],$hariini);
											elseif($sql_denda['satuanpinjam'] == "1") #jam
												$recdenda['jmljam'] = Sipus::hitung_telatjam($conn,$p_trans['tgl_tenggat'],$hariini,floor($p_trans['jamdendaskrg']));
					
											if($jml_denda != 0 and $bebasd==false)
												Sipus::InsertBiasa($conn,$recdenda,pp_denda);	
											
											//akumulasi jumlah denda dengan denda yang lama jika masih punya denda
											$sql_cek_denda_lama = $conn->GetRow("select denda from ms_anggota where idanggota='".$p_trans['idanggota']."'");
											$akumulasi_denda = (int)$sql_cek_denda_lama['denda'] + $jml_denda;
											$recdendaanggota['denda'] = $akumulasi_denda;
											if($jml_denda != 0 and $bebasd==false)
												Sipus::UpdateBiasa($conn,$recdendaanggota,ms_anggota,idanggota,$r_anggota);
										}
										
						// ============================== filtering mencari tenggat dg filter hari sabtu minggu dan hari libur ===========
										if ($p_aturan['lamaperpanjangan']!=null){
											#per jam
											if($p_aturan['satuanpinjam'] == "1"){
												$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$p_aturan['lamaperpanjangan']));
											}
											#per hari
											elseif($p_aturan['satuanpinjam'] == "0"){
											
												$tglm=date("Y-m-d");
												$tgl=strtotime(date("Y-m-d"));
												$tgl2=($tgl)+(86400*$p_aturan['lamaperpanjangan']);
												$jum=0;
												$i=$tgl2; //tgl_tenggat tujuan
												
												$h=date("w",$i);
												if($h==0){ #minggu
														$jum +=1;
													}
													
												if($h==6){ #sabtu
														$jum +=2;
													}
												$tottgl=($tgl2+($jum*86400));
												
												$tgltenggat=date("Y-m-d",$tottgl);
												$tglrange = date("Y-m-d",($tottgl+(86400*30)));
												
												$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);
											}
										}
										else
											$tglhasil=null;	
						//============================== selesei filter hari libur ======================================
										$rec=array();
										$rec['kdjenistransaksi']=$p_trans['kdjenistransaksi'];
										$rec['ideksemplar']=$p_trans['ideksemplar'];
										$rec['idanggota']=$p_trans['idanggota'];
										$rec['kdlokasi']=$p_trans['kdlokasi'];
										$rec['tgltransaksi']=date('Y-m-d H:i:s');//date("Y-m-d");
										$rec['perpanjangke']=$p_panjang['perpanjangke']+1;
										$rec['tglperpanjang']=date('Y-m-d H:i:s');//date("Y-m-d");
										$rec['tgltenggat']=$tglhasil;
										$rec['statustransaksi']=1;
										$rec['petugaspinjam']=Helper::cStrNull($_SESSION['PERPUS_USER']);
										Helper::Identitas($rec);
										$conn->StartTrans();
										Sipus::InsertBiasa($conn,$rec,pp_transaksi);
										
										$record['tglpengembalian']=date('Y-m-d H:i:s');//date("Y-m-d");
										$record['statustransaksi']=2;
										$record['petugaskembali']=Helper::cStrNull($_SESSION['PERPUS_USER']);
										Helper::Identitas($record);
										Sipus::UpdateBiasa($conn,$record,pp_transaksi,idtransaksi,$idtrans);
										
										$rectime['idanggota']=$r_anggota;
										//jenis perpanjangan
										if($biasa==true)
										$rectime['waktumulai']=$_SESSION['waktu'];
										else
										$rectime['waktumulai']=$_SESSION['panjangcepat'];
										
										$rectime['waktuselesai']=date('Y-m-d H:i:s');
										$rectime['regcomp']=$noseri;
										$rectime['status']='2';
										Helper::Identitas($rectime);
										if($p_jenis['kdjenispustaka']=='B')
										Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
										
										if($biasa==true)
										$_SESSION['waktu']=date('Y-m-d H:i:s');
									
										$conn->CompleteTrans();
										
										if($conn->ErrorNo() != 0){
											$errdb = 'Perpanjangan peminjaman gagal.';	
											Helper::setFlashData('errdb', $errdb);
										}
										else {
											Mail::mailPerpanjangan($conn,"disetujui",$idperpanjangan);
											$sucdb = 'Perpanjangan ke-'.$rec['perpanjangke'].' berhasil, Masa tenggat sampai '.Helper::tglEng($rec['tgltenggat']);	
											if($idperpanjangan){
												$rectrans = array();
												Helper::Identitas($rectrans);
												$rectrans['statusajuan']=2;
												$err=Sipus::UpdateBiasa($conn,$rectrans,pp_perpanjangan,idperpanjangan,$idperpanjangan);
											}
											if(!$biasa){
												$info = Sipus::getDataPerpanjangan($conn,$r_anggota);
												$sucdb .= $info;
											}
											Helper::setFlashData('sucdb', $sucdb);
											Helper::redirect();
										}
									}
								}
							}else{
								$errdb = 'Perpanjangan gagal, Anggota sedang bermasalah !';	
								Helper::setFlashData('errdb', $errdb);
							}
						} //
					}
				
				}
			}
		}	//$conn->CompleteTrans();
	}
	
	//transaksi peminjaman
	function newTrans($conn,$noseri,$idanggota,$jenisanggota,$redirect=true,$email=false){
		$conn->debug=false;
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and statuseksemplar is not null and noseri='$noseri' or noseri='$noseri2'");
		$ideksemplar=$p_eks['ideksemplar'];
		
		$seunit = Sipus::checkUnit($conn,$idanggota,$ideksemplar);
		if(!$seunit){
			$errdb = 'Unit Peminjam tidak sama dengan Unit lokasi eksemplar';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}
		
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul,p.edisi, p.kdjenispustaka
				from pp_eksemplar e 
				join ms_pustaka p on e.idpustaka=p.idpustaka 
				where e.noseri='$noseri' and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		$p_aturan=$conn->GetRow("select * from pp_aturan where kdjenisanggota='$jenisanggota'");
		
		# cek anggota pemesan
		$c_resv=$conn->GetOne("select idanggotapesan
				      from pp_reservasi
				      where ideksemplarpesan=$ideksemplar and statusreservasi='1' and tglexpired>=current_date
				      order by tglexpired desc");
		#jika ada maka diambil idreservasi nya
		if($c_resv)
		$p_check=$conn->GetOne("select idreservasi
				       from pp_reservasi
				       where ideksemplarpesan=$ideksemplar and idanggotapesan='$idanggota' and statusreservasi='1' and tglexpired>=current_date
				       order by tglexpired desc");

		$p_anggota=$conn->GetRow("select idanggota, kdjenisanggota,tglselesaiskors,statusanggota,uangjaminan
					from ms_anggota
					where idanggota='$idanggota'");
		$cekTrans = "select idtransaksi
			from v_trans_list
			where idanggota='$idanggota' and statustransaksi='1' and idpustaka=".$p_jenis['idpustaka']."";
		
		$cTrans = $conn->GetOne($cekTrans);
		
		if(!$cTrans){
			if(!$p_eks){
				$errdb = 'Eksemplar tidak ditemukan';	
				Helper::setFlashData('errdb', $errdb);
			}else{
				if($p_anggota['statusanggota']==0){
					$errdb = 'Anggota tersebut telah nonaktif !!!';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				} else if($p_anggota['statusanggota']==2){
					$errdb = 'Anggota tersebut telah terblokir !!!';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				} else {
					if(strtotime(date("Y-m-d"))>=strtotime($p_anggota['tglselesaiskors']) || $p_anggota['tglselesaiskors']=''){		
						if (!$p_jenis){
							$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
							Helper::setFlashData('errdb', $errdb);
						} else {
							if (!$p_aturan){		
									$errdb = 'Data aturan peminjaman tidak ditemukan.';
									Helper::setFlashData('errdb', $errdb);
							} else {
								if ($c_resv!='' and $p_check==''){
									$errdb = 'Pustaka dalam masa terpesan oleh ID '. $c_resv;
									Helper::setFlashData('errdb', $errdb); 
									
								}else {
									$cek=$conn->Execute("select idtransaksi from v_trans_list 
											where idanggota='$idanggota' and kdjenistransaksi ='PJN' and statustransaksi='1' ");
									$max=$cek->RowCount();
									if ($max>=$p_aturan['batasmaxpinjam'] and $p_aturan['batasmaxpinjam']!=null){
										if($p_aturan['batasmaxpinjam'] == 0){
											$sql_anggota = $conn->GetRow("select namajenisanggota from lv_jenisanggota where kdjenisanggota='$jenisanggota'");
											$sql_jnspustaka = $conn->GetRow("select namajenispustaka from lv_jenispustaka where kdjenispustaka='".$p_jenis['kdjenispustaka']."'");
											$sql_klasifikasi = $conn->GetRow("select namaklasifikasi from lv_klasifikasi where kdklasifikasi='".$p_jenis['kdklasifikasi']."'");
											$errdb = $sql_anggota['namajenisanggota'].' tidak dapat meminjam pustaka '.$sql_jnspustaka['namajenispustaka'].' '.$sql_klasifikasi['namaklasifikasi'].'.';
										}else{
											$errdb = 'Jumlah peminjaman pustaka sudah maksimal. Maksimal peminjaman adalah '.$p_aturan['batasmaxpinjam'].' pustaka';
										}
										Helper::setFlashData('errdb', $errdb);
										
									} else{
								
										if (!$p_jenis){
											$errdb = 'Id Eksemplar '. $ideksemplar.' tidak ditemukan.';
											Helper::setFlashData('errdb', $errdb);
										} else { 
											if($p_jenis['statuseksemplar']=='PJM'){
												$errdb = 'Pustaka tersebut sedang terpinjam.'; 
												Helper::setFlashData('errdb', $errdb);
											}
											elseif ($p_jenis['statuseksemplar']=='ADA') {
					// ============================== filtering mencari tenggat dg filter hari sabtu minggu dan hari libur ===========
												if ($p_aturan['lamaperpanjangan']!=null and $p_aturan['lamaperpanjangan']!=0){
													#per jam
													if($p_aturan['satuanpinjam'] == "1"){
														$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$p_aturan['lamaperpanjangan']));
													}
													#per hari
													elseif($p_aturan['satuanpinjam'] == "0"){
														$tglm=date("Y-m-d");
														$tgl=strtotime(date("Y-m-d"));
														$tgl2=($tgl)+((86400)*$p_aturan['lamaperpanjangan']);
														$jum=0;
														//$i=$tgl;
														$i=$tgl2;
														
														$h=date("w",$i);
														if($h==0){ #minggu
															$jum +=1;
														}
															
														if($h==6){ #sabtu
															$jum +=2;
														}
														$tottgl=($tgl2+($jum*86400)); //waktu max pinjm 14 + jml minggu 2 = 16
														//$tottgl=$tgl2;
														$tgltenggat=date("Y-m-d",$tottgl);
														$tglrange = date("Y-m-d",($tottgl+(86400*30)));
					
														$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);
													}
												}
												else
													$tglhasil=date("Y-m-d");
					//============================== selesei filter hari libur ======================================
					
												$jtrans='PJN';
								
												$record=array();
												$record['kdjenistransaksi']=$jtrans;
												$record['ideksemplar']=$ideksemplar;
												$record['idanggota']=$idanggota;
												$record['tgltransaksi']=date('Y-m-d H:i:s');//date("Y-m-d");
												$record['tgltenggat']=$tglhasil;
												$record['statustransaksi']=1;
												$record['fix_status']=1;//0; #tanpa pembatalan
												$record['petugaspinjam']=Helper::cStrNull($_SESSION['PERPUS_USER']);
												Helper::Identitas($record);
												$p_rupiah=$conn->GetRow("select uangjaminanpinjaman from lv_jenisanggota where kdjenisanggota='".$p_anggota['kdjenisanggota']."'");
												$rupiah=$p_rupiah['uangjaminanpinjaman'];
											
												$conn->StartTrans();
												if($rupiah!='' or $rupiah!=0){
													$record['rpjaminan']=$rupiah;
												}
												Sipus::InsertBiasa($conn,$record,pp_transaksi);
												
												//update uang jaminan anggota
												$recuang['uangjaminan'] = $p_anggota['uangjaminan']+$rupiah;
												Sipus::UpdateBiasa($conn,$recuang,ms_anggota,idanggota,$idanggota);
												
												//update status
												$rec['statuseksemplar']='PJM';
												Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);
												//update reserve
												if ($p_check){
													$recr['statusreservasi']=0;
													$recr['tglpinjam']=date('Y-m-d H:i:s');//date('Y-m-d');
													Sipus::UpdateBiasa($conn,$recr,pp_reservasi,idreservasi,$p_check);
												
													#yg lain lsg batal = 3
													$conn->Execute("update pp_reservasi set statusreservasi='3' where ideksemplarpesan=$ideksemplar and statusreservasi='1'");
												
												}
							
												if($_SESSION['waktu']=='')
													$time=date('Y-m-d H:i:s');
												else
													$time=$_SESSION['waktu'];
							
												$rectime['idanggota']=$idanggota;
												$rectime['waktumulai']=$time;
												$rectime['waktuselesai']=date('Y-m-d H:i:s');
												$rectime['regcomp']=$noseri;
												$rectime['status']='1';
												Helper::Identitas($rectime);
												if($p_jenis['kdjenispustaka']=='B')
												Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
												
												$_SESSION['waktu']=date('Y-m-d H:i:s');
												
												$conn->CompleteTrans();
												if($conn->ErrorNo() != 0){
													$errdb = 'Peminjaman pustaka gagal.';	
													Helper::setFlashData('errdb', $errdb);
												}
												else {
													if($email){
														Mail::statuspesan($conn,$ideksemplar,$email,"disetujui");
													}
													$sucdb = 'Peminjaman berhasil.';	
													Helper::setFlashData('sucdb', $sucdb);
													if($redirect==true)
													Helper::redirect();
												}
											}
										}
									}
								}
							}
							
						}
					}else {
						$errdb = 'Anggota Dalam Masa Skorsing.';	
						Helper::setFlashData('errdb', $errdb);
						Helper::redirect();
					}
				}
			}
		}else {
			$errdb = 'Pustaka yang sama Tidak dapat dipinjam lagi';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}
	} 
	
	//transaksi peminjaman fotocopy
	function newTransFotocopy($conn,$noseri,$idanggota,$jenisanggota,$redirect=true){
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and statuseksemplar is not null and noseri='$noseri' or noseri='$noseri2'");
		$ideksemplar=$p_eks['ideksemplar'];
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul,p.edisi, p.kdjenispustaka
				from pp_eksemplar e
				join ms_pustaka p on e.idpustaka=p.idpustaka
				where e.noseri='$noseri' and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		$p_aturan=$conn->GetRow("select kdklasifikasi, kdjenispustaka, kdjenisanggota, lamaperpanjangan lamafotocopy, jumlahperpanjangan, 
					batasmaxpinjam batasmaxfotocopy, satuanpinjam satuanfotocopy, denda
					from pp_aturan
					where kdjenisanggota='$jenisanggota'");
		
		$c_resv=$conn->GetOne("select idanggotapesan from pp_reservasi where ideksemplarpesan=$ideksemplar and statusreservasi='1' and tglexpired>=current_date order by tglexpired desc");
		if($c_resv)
		$p_check=$conn->GetOne("select idreservasi from pp_reservasi where ideksemplarpesan=$ideksemplar and idanggotapesan='$idanggota' and statusreservasi='1' and tglexpired>=current_date order by tglexpired desc");
		
		$p_anggota=$conn->GetRow("select idanggota, kdjenisanggota,tglselesaiskors,statusanggota,uangjaminan from ms_anggota where idanggota='$idanggota'");
		$cekTrans = "select idtransaksi from v_trans_list where idanggota='$idanggota' and statustransaksi='1' and idpustaka=".$p_jenis['idpustaka']."";
		$cTrans = $conn->GetOne($cekTrans);
	
		if(!$cTrans){
			if(!$p_eks){
				$errdb = 'Eksemplar tidak ditemukan';	
				Helper::setFlashData('errdb', $errdb);
				//Helper::redirect();				
			}else{
				if($p_anggota['statusanggota']==0){
					$errdb = 'Anggota tersebut telah nonaktif !!!';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				} else if($p_anggota['statusanggota']==2){
					$errdb = 'Anggota tersebut telah terblokir !!!';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				} else {
					if(strtotime(date("Y-m-d"))>=strtotime($p_anggota['tglselesaiskors']) || $p_anggota['tglselesaiskors']=''){		
						if (!$p_jenis){
							$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
							Helper::setFlashData('errdb', $errdb);
						} else {
							if (!$p_aturan){		
									$errdb = 'Data aturan peminjaman tidak ditemukan.';
									Helper::setFlashData('errdb', $errdb);
									
							} else {
								if ($c_resv!='' and $p_check==''){
									$errdb = 'Pustaka dalam masa terpesan oleh ID '. $c_resv;
									Helper::setFlashData('errdb', $errdb); 
									
								}else {	
									$cek=$conn->Execute("select idtransaksi from v_trans_list 
											where idanggota='$idanggota' and kdjenispustaka='".$p_jenis['kdjenispustaka']."' 
											and kdklasifikasi='".$p_jenis['kdklasifikasi']."' and kdjenistransaksi ='PJF' and statustransaksi='1' ");
									$max=$cek->RowCount();
									if ($max>=$p_aturan['batasmaxfotocopy'] and $p_aturan['batasmaxfotocopy']!=null){
										if($p_aturan['batasmaxpinjam'] == 0){
											$sql_anggota = $conn->GetRow("select namajenisanggota from lv_jenisanggota where kdjenisanggota='$jenisanggota'");
											$sql_jnspustaka = $conn->GetRow("select namajenispustaka from lv_jenispustaka where kdjenispustaka='".$p_jenis['kdjenispustaka']."'");
											$sql_klasifikasi = $conn->GetRow("select namaklasifikasi from lv_klasifikasi where kdklasifikasi='".$p_jenis['kdklasifikasi']."'");
											$errdb = $sql_anggota['namajenisanggota'].' tidak dapat meminjam pustaka '.$sql_jnspustaka['namajenispustaka'].' '.$sql_klasifikasi['namaklasifikasi'].'.';
										}else{
											$errdb = 'Jumlah peminjaman pustaka sudah maksimal. Maksimal peminjaman adalah '.$p_aturan['batasmaxfotocopy'].' pustaka';
										}
										Helper::setFlashData('errdb', $errdb);
										
									} else{
			
										if (!$p_jenis){
											$errdb = 'Id Eksemplar '. $ideksemplar.' tidak ditemukan.';
											Helper::setFlashData('errdb', $errdb);
										} else { 
											if($p_jenis['statuseksemplar']=='PJM'){
												$errdb = 'Pustaka tersebut sedang terpinjam.'; 
												Helper::setFlashData('errdb', $errdb);
											}
											elseif ($p_jenis['statuseksemplar']=='ADA') {
								// ============================== filtering mencari tenggat dg filter hari sabtu minggu dan hari libur ===========
												if ($p_aturan['lamafotocopy']!=null and $p_aturan['lamafotocopy']!=0){
													#per jam
													if($p_aturan['satuanfotocopy'] == "1"){
														$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$p_aturan['lamaperpanjangan']));
													}
													#per hari
													elseif($p_aturan['satuanfotocopy'] == "0"){
														$tglm=date("Y-m-d");
														$tgl=strtotime(date("Y-m-d"));
														$tgl2=($tgl)+((86400)*$p_aturan['lamafotocopy']);
														$jum=0;
														$i=$tgl2; #tgl tenggat tujuan
														
														$h=date("w",$i);
														if($h==0){ # sabtu
																$jum +=1;
															}
															
														if($h==6){ # minggu
																$jum +=2;
															}
														$tottgl=($tgl2+($jum*86400)); //waktu max pinjm 14 + jml minggu 2 = 16
														
														$tgltenggat=date("Y-m-d",$tottgl);
														$tglrange = date("Y-m-d",($tottgl+(86400*30)));
														
														$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);
													}
												}
												else
													$tglhasil=date("Y-m-d");
									
									//============================== selesei filter hari libur ======================================
												$jtrans='PJF';
												$record=array();
												$record['kdjenistransaksi']=$jtrans;
												$record['ideksemplar']=$ideksemplar;
												$record['idanggota']=$idanggota;
												$record['tgltransaksi']=date('Y-m-d H:i:s');//date("Y-m-d");
												$record['tgltenggat']=date("Y-m-d");//$tglhasil;
												$record['statustransaksi']=1;
												$record['fix_status']=1;//0; #tanpa pembatalan
												$record['petugaspinjam']=Helper::cStrNull($_SESSION['PERPUS_USER']);
												Helper::Identitas($record);
											
												$conn->StartTrans();
												Sipus::InsertBiasa($conn,$record,pp_transaksi);
												
												//update status
												$rec['statuseksemplar']='PJM';
												Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);
												//update reserve
												if ($p_check){
													$recr['statusreservasi']=0;
													$recr['tglpinjam']=date('Y-m-d');
													Sipus::UpdateBiasa($conn,$recr,pp_reservasi,idreservasi,$p_check);
													$conn->Execute("update pp_reservasi set statusreservasi='3' where ideksemplarpesan=$ideksemplar and statusreservasi='1'");
												}
												
												if($_SESSION['waktu']=='')
													$time=date('Y-m-d H:i:s');
												else
													$time=$_SESSION['waktu'];
												
												$rectime['idanggota']=$idanggota;
												$rectime['waktumulai']=$time;
												$rectime['waktuselesai']=date('Y-m-d H:i:s');
												$rectime['regcomp']=$noseri;
												$rectime['status']='1';
												Helper::Identitas($rectime);
												//if($p_jenis['kdjenispustaka']=='B')
												Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
												
												$_SESSION['waktu']=date('Y-m-d H:i:s');
												
												$conn->CompleteTrans();
												if($conn->ErrorNo() != 0){
													$errdb = 'Peminjaman pustaka gagal.';	
														Helper::setFlashData('errdb', $errdb);
												}else {
													$sucdb = 'Peminjaman berhasil.';	
													Helper::setFlashData('sucdb', $sucdb);
												}
											}
										}
									}
								}
							}
						}
					}else {
						$errdb = 'Anggota Dalam Masa Skorsing.';	
						Helper::setFlashData('errdb', $errdb);
					}
				}
			}
		}else {
			$errdb = 'Pustaka yang sama Tidak dapat dipinjam lagi';	
			Helper::setFlashData('errdb', $errdb);
			// Helper::redirect();
		}
	} 
	
	function newTransSpecial($conn,$noseri,$idanggota,$jenisanggota,$direct=true){
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and statuseksemplar is not null and (upper(noseri)=upper('$noseri') or upper(noseri)=upper('$noseri2'))");
		$ideksemplar=$p_eks['ideksemplar'];
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul,p.edisi, p.kdjenispustaka from pp_eksemplar e 
								join ms_pustaka p on e.idpustaka=p.idpustaka 
								where e.ideksemplar=$ideksemplar and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		$p_aturan=$conn->GetRow("select * from pp_aturan where kdjenisanggota='$jenisanggota'");
		
		$c_resv=$conn->GetOne("select idanggotapesan from pp_reservasi where ideksemplarpesan=$ideksemplar and statusreservasi='1' and tglexpired>=current_date order by tglexpired desc");
		if($c_resv)
		$p_check=$conn->GetOne("select idreservasi from pp_reservasi where ideksemplarpesan=$ideksemplar and idanggotapesan='$idanggota' and statusreservasi='1' and tglexpired>=current_date order by tglexpired desc");
		
		$p_anggota=$conn->GetRow("select idanggota, kdjenisanggota,tglselesaiskors,statusanggota,uangjaminan from ms_anggota where idanggota='$idanggota'");
		$cekTrans = "select idtransaksi from v_trans_list where tglpengembalian is null and idpustaka=".$p_jenis['idpustaka']." and idanggota='".$idanggota."'";
		
		$cTrans = $conn->GetOne($cekTrans);
		
		if(!$cTrans){
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();				
		}else{
				
		if (!$p_jenis){
			$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
			Helper::setFlashData('errdb', $errdb);
		} else {
		if (!$p_aturan){		
				$errdb = 'Data aturan peminjaman tidak ditemukan.';
				Helper::setFlashData('errdb', $errdb);
				
		} else {
			if ($c_resv!='' and $p_check==''){
				$errdb = 'Pustaka dalam masa terpesan oleh ID '. $c_resv;
				Helper::setFlashData('errdb', $errdb); 
				
			}else {
				if($p_jenis['statuseksemplar']=='PJM'){
				$errdb = 'Pustaka tersebut sedang terpinjam.'; 
				Helper::setFlashData('errdb', $errdb);
				}
				elseif ($p_jenis['statuseksemplar']=='ADA') {
// ============================== filtering mencari tenggat dg filter hari sabtu minggu dan hari libur ===========
				if ($p_aturan['lamaperpanjangan']!=null){
					#per jam
					if($p_aturan['satuanpinjam'] == "1"){
						$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$p_aturan['lamaperpanjangan']));
					}
					#per hari
					elseif($p_aturan['satuanpinjam'] == "0"){
						$tglm=date("Y-m-d");
						$tgl=strtotime(date("Y-m-d"));
						$tgl2=($tgl)+((86400)*$p_aturan['lamaperpanjangan']);
						$jum=0;
						$i=$tgl2; #tgl tenggat tujuan
						
						$h=date("w",$i);
						if($h==0){#minggu
								$jum +=1;
							}
							
						if($h==6){#sabtu
								$jum +=2;
							}
						$tottgl=($tgl2+($jum*86400));
						//$tottgl=$tgl2;
						$tgltenggat=date("Y-m-d",$tottgl);
						$tglrange = date("Y-m-d",($tottgl+(86400*30)));
						
						$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);
					}
				}
				else
					$tglhasil=null;
//============================== selesei filter hari libur ======================================
				if ($p_jenis['kdklasifikasi']=='LT' or $p_jenis['kdklasifikasi']=='LM')
					$jtrans='PJN';
				elseif($p_jenis['kdklasifikasi']=='LH')
					$jtrans='PJH';
					
				$record=array();
				$record['kdjenistransaksi']=$jtrans;
				$record['ideksemplar']=$ideksemplar;
				$record['idanggota']=$idanggota;
				$record['tgltransaksi']=date('Y-m-d H:i:s');//date("Y-m-d");
				$record['tgltenggat']=$tglhasil;
				$record['statustransaksi']=1;
				$record['fix_status']=1;//0; #tanpa pembatalan
				$record['petugaspinjam']=Helper::cStrNull($_SESSION['PERPUS_USER']);
				Helper::Identitas($record);

			
				$conn->StartTrans();
				Sipus::InsertBiasa($conn,$record,pp_transaksi);
				
				//update status
				$rec['statuseksemplar']='PJM';
				Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);
				//update reserve
				if ($p_check){
				$recr['statusreservasi']=0;
				$recr['tglpinjam']=date('Y-m-d');
				Sipus::UpdateBiasa($conn,$recr,pp_reservasi,idreservasi,$p_check);
				$conn->Execute("update pp_reservasi set statusreservasi='0' where ideksemplarpesan=$ideksemplar and statusreservasi='0'");
				
				}
				
				$rectime['idanggota']=$idanggota;
				$rectime['waktumulai']=$_SESSION['waktu'];
				$rectime['waktuselesai']=date('Y-m-d H:i:s');
				$rectime['regcomp']=$noseri;
				$rectime['status']='1';
				Helper::Identitas($rectime);
				if($p_jenis['kdjenispustaka']=='B')
				Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
				
				$_SESSION['waktu']=date('Y-m-d H:i:s');
				
				$conn->CompleteTrans();
				if($conn->ErrorNo() != 0){
					$errdb = 'Peminjaman pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
					else {
					$sucdb = 'Peminjaman berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					if($direct==true)
					Helper::redirect();
					}
				}
				}
			}}}
			
	
	}
	else {
		$errdb = 'Pustaka Tidak dapat dipinjam / terdapat pustaka yang sama';
		Helper::setFlashData('errdb', $errdb);
		Helper::redirect();
	}
	} 
	
	//reservasi
	function newReserve($conn,$noseri,$idanggota,$jenisanggota){
		$noseri2 = Helper::RegNew($noseri);
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where kdkondisi='V' and statuseksemplar is not null and (upper(noseri)=upper('$noseri') or upper(noseri)=upper('$noseri2')) ");
		$idpesen=$p_eks['ideksemplar'];
		$seunit = Sipus::checkUnit($conn,$idanggota,$idpesen);
		if(!$seunit){
			$errdb = 'Unit Peminjam tidak sama dengan Unit lokasi eksemplar';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();
		}
		
		
		$p_jenis=$conn->GetRow("select e.ideksemplar, e.idpustaka, e.statuseksemplar, e.kdklasifikasi, p.judul,p.edisi,p.kdjenispustaka
				       from pp_eksemplar e
				       join ms_pustaka p on e.idpustaka=p.idpustaka
				       where e.ideksemplar=$idpesen and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
		
		$p_aturan=$conn->GetRow("select * from pp_aturan where kdjenisanggota='$jenisanggota'");
		
		$p_exp=$conn->GetRow("select tgl_tenggat tgltenggat,idtransaksi from v_trans_list where ideksemplar=$idpesen and kdjenistransaksi='PJN' and statustransaksi='1' ");

		$sqlcek = "SELECT * FROM (select inner_query.*, rownum rnum FROM (
				select tglexpired from v_reserve where idpustaka=".$p_jenis['idpustaka']." and statusreservasi='1' ";
		$sqlcek .=" and idanggotapesan='$idanggota')  inner_query WHERE rownum <= 1) order by tglexpired desc";
		$p_check=$conn->GetRow($sqlcek);
																		//ideksemplarpesan=$idpesen
		$p_anggota=$conn->GetRow("select idanggota,statusanggota,tglselesaiskors from ms_anggota where idanggota='$idanggota'");
		
		if(!$p_eks){
			$errdb = 'Eksemplar tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			Helper::redirect();				
		}else{
			if($p_anggota['statusanggota']==0){
				$errdb = 'Anggota tersebut telah nonaktif !!!';
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
			}else if($p_anggota['statusanggota']==2){
				$errdb = 'Anggota tersebut telah terblokir !!!';
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
			}else {

				if($p_check['tglexpired']<=date('Y-m-d') and (strtotime(date("Y-m-d"))>=strtotime($p_anggota['tglselesaiskors']) || $p_anggota['tglselesaiskors']='')) {
					if (!$p_jenis){
						$errdb = 'Petugas tidak dapat melakukan transaksi untuk jenis pustaka tersebut.';
						Helper::setFlashData('errdb', $errdb);
					} else {
						if (!$p_exp){
							if (!$p_aturan){		
								$errdb = 'Data aturan peminjaman tidak ditemukan.';
								Helper::setFlashData('errdb', $errdb);
							} else {
								if (!$p_jenis){
									$errdb = 'Nomor Seri '. $noseri.' tidak ditemukan.';
									Helper::setFlashData('errdb', $errdb);
					
								} else {
									if($p_jenis['statuseksemplar']=='ADA'){
										$tgltenggat = date('Y-m-d', strtotime('+3 days')); 
										$tglhasil = Sipus::tglTenggat($conn, $tgltenggat);

										$record=array();
										$record['ideksemplarpesan']=$idpesen;
										$record['idanggotapesan']=$idanggota;
										$record['tglreservasi']=date("Y-m-d");
										$record['tglexpired']=$tglhasil;
										$record['statusreservasi']=1;
										$record['idtransaksi']=$p_exp['idtransaksi'];
										$record['t_inserttime'] = date('Y-m-d H:i:s');
										Helper::Identitas($record);
										
										$conn->StartTrans();
										Sipus::InsertBiasa($conn,$record,pp_reservasi);
										
										$rectime['idanggota']=$idanggota;
										$rectime['waktumulai']=$_SESSION['waktu'];
										$rectime['waktuselesai']=date('Y-m-d H:i:s');
										$rectime['regcomp']=$noseri;
										$rectime['status']='3';
										Helper::Identitas($rectime);

										Sipus::InsertBiasa($conn,$rectime,pp_logtransaksi);
										
										$_SESSION['waktu']=date('Y-m-d H:i:s');
										$conn->CompleteTrans();
				
										if($conn->ErrorNo() != 0){
											$errdb = 'Pemesanan Pustaka gagal.';	
											Helper::setFlashData('errdb', $errdb);
										}
										else {
											$sucdb = 'Pemesanan Pustaka berhasil.';	
											Helper::setFlashData('sucdb', $sucdb);
											
											Helper::redirect();
										}
									}
								}
							}
						}
					}
				}
				else {
					$errdb = 'Pemesanan Pustaka gagal/duplicat.';
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}
	}
	
	function TanggalLibur($conn,$harimulai,$jumlahlibur,$hasile) {
		$tglm=$harimulai;
		$tgl=strtotime(date("Y-m-d"));
		$tgl2=($tgl)+((86400)*$jumlahlibur);
		$jum=0;
		$i=$tgl;
		while ($i<=$tgl2){
			$h=date("w",$i);
			if($h==0){
				$jum=$jum+1;}
			//echo " jum : ".$jum." tgl2 :".date("Y-m-d",$tgl2);
			$i=$i+86400;
			//echo " i :".$i;
		}
		
		$tottgl=($tgl2+(86400*$jum));
		$tgltenggat=date("Y-m-d",$tottgl);
		//$p_libur=$conn->Execute("select tgllibur from ms_libur where tgllibur between '$tglm' and '$tgltenggat' and EXTRACT(DOW from tgllibur)<>0");
		$p_libur=$conn->Execute("select tgllibur from ms_libur
					where to_char(tgllibur,'YYYY-mm-dd') >= '$tglm' and to_char(tgllibur,'YYYY-mm-dd') <= '$tgltenggat'
					and to_char (tgllibur, 'D')!=0 
					order by tgllibur");

		$thk=$p_libur->RowCount();
		$tglt=(($tottgl)+(86400*thk));
		$hasile=date("Y-m-d",$tglt);
	
		return $hasile;
	}
	
	function hitungdendaperbuku($conn, $tglkembali, $tglcek, $idanggota, $ideksemplar ){
		$rpdenda = 0;
		//Mencari jml hari yg diset libur
		$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur
				       from ms_libur
				       where to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') >= '$tglkembali'
						and to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') <= '$tglcek'
						and to_char (tgllibur, 'D')!=0
						and to_char (tgllibur, 'D')!=6
				       order by tgllibur");
		$p_trans=$conn->GetRow("select a.kdjenisanggota, idtransaksi,ideksemplar, kdjenispustaka,v.idanggota, tgltransaksi, tgltenggat, kdklasifikasi,
				       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg 
				       from v_trans_list v
				       left join ms_anggota a on a.idanggota=v.idanggota
				       where ideksemplar=$ideksemplar and statustransaksi='1' and kdjenispustaka in(".$_SESSION['roleakses'].")");
		
		//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
		$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;

		$jmlsabtuminggu=0;
		for($k=1;$k<=$range_sm;$k++){
			$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
			if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
				$jmlsabtuminggu +=1;
			}
		}
		//========================================================
				
		$sql_denda = $conn->GetRow("select denda, satuanpinjam from pp_aturan where kdjenisanggota='".$p_trans['kdjenisanggota']."'");
		
		if($sql_denda['denda'] > 0){
			if($sql_denda['satuanpinjam'] == '0'){ #status per hari
				$hitungmldenda = ((strtotime($tglcek)-strtotime($tglkembali))/86400) - $jmlsabtuminggu; //brp hari telatnya, sdh dikurangi sabtu minggu
				if($hitungmldenda > 0){ // jika telat
					$rpdenda = ($hitungmldenda-(int)$p_libur['jmllibur']) * $sql_denda['denda'];
				}else{
					$rpdenda = 0;
				}
			}elseif($sql_denda['satuanpinjam'] == '1'){ #status per jam
				$hitungmldenda = floor($p_trans['jamdendaskrg']) - ($jmlsabtuminggu*24);
				if($hitungmldenda > 0){ // jika telat
					$rpdenda = ($hitungmldenda-((int)$p_libur['jmllibur']*24)) * $sql_denda['denda'];
				}else{
					$rpdenda = 0;
				}
			}
		}else{
			$rpdenda = 0; //bukan yang ada di tabel pp_aturan
		}
		
		return $rpdenda;
	}
	
	function hitung_telat ($conn, $tglkembali, $tglcek){
		$conn->debug = false;
		//Mencari jml hari yg diset libur
		$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur from ms_libur
				       where to_char(tgllibur,'YYYY-mm-dd') >= '$tglkembali' and to_char(tgllibur,'YYYY-mm-dd') <= '$tglcek'
				       and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6");
			
		//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
		$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;
		$jmlsabtuminggu=0;
		for($k=1;$k<=$range_sm;$k++){
			$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
			if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
				$jmlsabtuminggu +=1;
			}
		}
		
		$jml_telat = (int)$range_sm - ((int)$p_libur['jmllibur'] + $jmlsabtuminggu);
		return $jml_telat; // hitung jml hari telat
		//========================================================
				
	}
	
	function hitung_telatjam ($conn, $tglkembali, $tglcek, $jamdenda){
		$conn->debug = false;
		//Mencari jml hari yg diset libur
		$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur
				       from ms_libur where to_char(tgllibur,'YYYY-mm-dd') >= '$tglkembali' and to_char(tgllibur,'YYYY-mm-dd') <= '$tglcek'
				       and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6
				       ");
			
		//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
		$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;
		$jmlsabtuminggu=0;
		for($k=1;$k<=$range_sm;$k++){
			$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
			if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
				$jmlsabtuminggu +=1;
			}
		}
		
		$jml_telat = (int)$jamdenda - ((int)($p_libur['jmllibur']*24) + ($jmlsabtuminggu*24));
		return $jml_telat; // hitung jml hari telat
		//========================================================
				
	}
	
	function isEksemplar($conn, $idserialcontrol){
		$conn->debug = false;
		$count = $conn->GetOne("select count(e.ideksemplar)
					  from pp_eksemplar e
					  join pp_serialitem s on e.idserialitem = s.idserialitem
					  join pp_serialcontrol c on s.idserial = c.idserial
					  where c.idserial = '$idserialcontrol' ");
		if($count > 0)
			return false;
		else
			return true;
	}
	
	function isEksemplarItem($conn, $idserialitem){
		$conn->debug = false;
		$count = $conn->GetOne("select count(e.ideksemplar)
					  from pp_eksemplar e 
					  where e.idserialitem = '$idserialitem' ");
		if($count > 0)
			return false;
		else
			return true;
	}
	
	function getHeadPerpus(){
		// koneksi ke sdm
		$conn = Factory::getConn();
		$sql = $conn->GetRow("select nid nik, nama as namalengkap, jabatan 
					from kabag_perpus
					where rownum = 1 ");
		return $sql;
	}
	
	function getDataPeminjaman($conn, $idanggota){
		$data = $conn->GetRow("SELECT * FROM (select inner_query.*, rownum rnum FROM (
				select a.idanggota, a.namaanggota, e.noseri, p.judul,
				to_char(t.tgltransaksi,'YYYY-mm-dd HH:mm:ss') tgltransaksi,
				to_char(t.tglpengembalian,'YYYY-mm-dd HH:mm:ss') tglpengembalian,
				a.denda, t.perpanjangke   
				from pp_transaksi t
				join ms_anggota a on a.idanggota = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka p on p.idpustaka = e.idpustaka 
				where a.idanggota = '$idanggota' and t.statustransaksi = '0'
				)  inner_query WHERE rownum <= 1) order by tglpengembalian desc ");
		
		if($data['denda']!=null or $data['denda'] != 0){
			$inp =  UI::createTextBox('bayar','','ControlStyleT',10,10,true,'onkeydown="return onlyNumber(event,this,false,true)"');
			$inp .= "<u title=\"Bayarkan Denda\" onclick=\"goFreeSkors('".$data['idanggota']."','".$data['denda']."')\" class=\"link\">
					<img src=\"images/freeskors.gif\">bayarkan</u>";
		}
						
		$info = "<div>
					<table width=\"600\" cellspacing=\"0\" cellpadding=\"0\"  border=\"0\" style=\"background:#EBF2F9;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;\">
						<tr height=\"25\">
							<th colspan=\"2\">DATA PENGEMBALIAN</th>
						</tr>
						<tr height=\"22\">
							<td width=160 ><b>ID ANGGOTA</b></td>
							<td>".$data['idanggota']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>NAMA</b></td>
							<td>".$data['namaanggota']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>NO.SERI</b></td>
							<td>".$data['noseri']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>JUDUL</b></td>
							<td>".$data['judul']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>TGL PINJAM</b></td>
							<td>".$data['tgltransaksi']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>TGL KEMBALI</b></td>
							<td>".$data['tglpengembalian']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>PERPANJANGAN KE</b></td>
							<td>".($data['perpanjangke'] == null ? '0' : $data['perpanjangke'])."</td>
						</tr>
						<tr height=\"22\">
							<td><font color=\"red\"><b>Hutang Denda</b></td>
							<td>: <font color=\"red\">Rp. ".($data['denda'] == null ? '0' : $data['denda']).",- &nbsp;".$inp."</td>
							<td>&nbsp;</td>
						</tr>	
					</table/>
				</div>";
				
		return $info;
	}
	
	function getDataPerpanjangan($conn, $idanggota){
		$data = $conn->GetRow("SELECT * FROM (select inner_query.*, rownum rnum FROM (
				select a.idanggota, a.namaanggota, e.noseri, p.judul,
				to_char(t.tgltransaksi,'YYYY-mm-dd HH:mm:ss') tgltransaksi,
				to_char(t.tgltenggat,'YYYY-mm-dd HH:mm:ss') tgltenggat,
				a.denda, t.perpanjangke,
				to_char(t.tglperpanjang,'YYYY-mm-dd HH:mm:ss') tglperpanjang    
				from pp_transaksi t
				join ms_anggota a on a.idanggota = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka p on p.idpustaka = e.idpustaka 
				where a.idanggota = '$idanggota' and t.statustransaksi = '1'
				) inner_query WHERE rownum <= 1) order by tglperpanjang desc");
		
		if($data['denda']!=null or $data['denda'] != 0){
			$inp =  UI::createTextBox('bayar','','ControlStyleT',10,10,true,'onkeydown="return onlyNumber(event,this,false,true)"');
			$inp .= "<u title=\"Bayarkan Denda\" onclick=\"goFreeSkors('".$data['idanggota']."','".$data['denda']."')\" class=\"link\">
					<img src=\"images/freeskors.gif\">bayarkan</u>";
		}
						
		$info = "<div>
					<table width=\"600\" cellspacing=\"0\" cellpadding=\"0\"  border=\"0\" style=\"background:#EBF2F9;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;\">
						<tr height=\"25\">
							<th colspan=\"2\">DATA PERPANJANGAN</th>
						</tr>
						<tr height=\"22\">
							<td width=160 ><b>ID ANGGOTA</b></td>
							<td>".$data['idanggota']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>NAMA</b></td>
							<td>".$data['namaanggota']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>NO.SERI</b></td>
							<td>".$data['noseri']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>JUDUL</b></td>
							<td>".$data['judul']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>TGL PERPANJANGAN PINJAM</b></td>
							<td>".$data['tgltransaksi']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>TGL HARUS KEMBALI</b></td>
							<td>".$data['tgltenggat']."</td>
						</tr>
						<tr height=\"22\">
							<td><b>PERPANJANGAN KE</b></td>
							<td>".($data['perpanjangke'] == null ? '0' : $data['perpanjangke'])."</td>
						</tr>
						<tr height=\"22\">
							<td><font color=\"red\"><b>Hutang Denda</b></td>
							<td>: <font color=\"red\">Rp. ".($data['denda'] == null ? '0' : $data['denda']).",- &nbsp;".$inp."</td>
							<td>&nbsp;</td>
						</tr>	
					</table/>
				</div>";
				
		return $info;
	}
	
	function getLabel($klasifikasi,$noseri,$er,$minr,$nopanggil){
		#punggung
		
		if($klasifikasi == "R")
			$ref = "R";
		else
			$ref = '<span style="color: white">-</span>';
		
		$urut = explode(".",$noseri);
		if( (count(explode("-",$label[3]))) == 2 ){
			$eks = intval($urut[1]);
		}else{
			$akhir = substr($noseri,-1);
			$eks = Helper::getAngka(strtolower($akhir));
		}
		if($er == "R"){
			$label = explode(' ',$minr);
			$countlabel = count($label);
			if($countlabel==4){
				$label1 = $label[0].' '.$label[1];
				$label2 = $label[2];
				if( (count(explode("-",$label[3]))) == 2 ){
					$lab3 = explode("-",$label[3]);
					$label3 = $lab3.($eks? "-".$eks: "");
				}else{
					$label3 = $label[3].($eks? "-".$eks: "");
				}
									
			}elseif($countlabel==3){
				$label1 = $label[0];
				$label2 = $label[1];
				if( (count(explode("-",$label[2]))) == 2 ){
					$lab3 = explode("-",$label[2]);
					$label3 = $lab3.($eks? "-".$eks: "");
				}else{
					$label3 = $label[2].($eks? "-".$eks: "");
				}
			}elseif($countlabel==2){
				$label1 = $label[0];
				if( (count(explode("-",$label[1]))) == 2 ){
					$lab2 = explode("-",$label[1]);
					$label2 = $lab2.($eks? "-".$eks: "");
				}else{
					$label2 = $label[1].($eks? "-".$eks: "");
				}
				$label3 = '<span style="color: white">-</span>';					
			}
		}else{
			$label = explode(' ',$nopanggil);
			$countlabel = count($label);
			if($countlabel==4){
				$label1 = $label[0].' '.$label[1];
				$label2 = $label[2];
				if( (count(explode("-",$label[3]))) == 2 ){
					$lab3 = explode("-",$label[3]);
					$label3 = $lab3.($eks? "-".$eks: "");
				}else{
					$label3 = $label[3].($eks? "-".$eks: "");
				}
									
			}elseif($countlabel==3){
				$label1 = $label[0];
				$label2 = $label[1];
				if( (count(explode("-",$label[2]))) == 2 ){
					$lab3 = explode("-",$label[2]);
					$label3 = $lab3.($eks? "-".$eks: "");
				}else{
					$label3 = $label[2].($eks? "-".$eks: "");
				}
			}elseif($countlabel==2){
				$label1 = $label[0];
				if( (count(explode("-",$label[1]))) == 2 ){
					$lab2 = explode("-",$label[1]);
					$label2 = $lab2.($eks? "-".$eks: "");
				}else{
					$label2 = $label[1].($eks? "-".$eks: "");
				}
				$label3 = '<span style="color: white">-</span>';					
			}
		}
		
		$punggung = "	<strong>PERPUSTAKAAN PT PJB SERVICES</strong><hr>
				<strong>".$ref."</strong><hr>
				<strong>".$label1."</strong><hr>
				<strong>".$label2."</strong><hr>
				<strong>".$label3."</strong>";
		return $punggung;
	}
	
	function getBarcode($klasifikasi,$noseri,$er,$minr,$nopanggil){
		#barcode
		if($klasifikasi == "R"){
			if($er == "R"){
				$code1 = "R ".$minr;
			}else{
				$code1 = "R ".$nopanggil;
			}
		}else{
			if($er == "R"){
				$code1 = $minr;
			}else{
				$code1 = $nopanggil;
			}
		}
		
		$barcode = '<div align="center" style="margin-top:-13px;margin-bottom:0px;background:#fff;position:relative;z-index:1;"><font size=1><b>'.$code1.'</b></font></div>
			<div align="center" style="margin:5px;"><img src="index.php?page=image&key='.$noseri.'">
			<div style="margin-top:-13px;background:#fff;position:relative;z-index:1;">
				<font size=1><b>'.Helper::AlfSpace($noseri,2).'</b></font></div>
				<font size="1">Perpustakaan PT PJB SERVICES</font><br>
			</div>';
			
		return $barcode;
	}
	
	function getLabelCode($kdjenispustaka,$noseri,$insjudul,$insauth,$key){
				
			
		$punggung = '
			<table align="center" class="title-label" "width="100%">
				<tr>
					<td align="center" style="width:15%;"><img src="images/favicon.ico" height="40"></td>
					<td align="center" style="width:65%;">
						<strong style="width:100%;margin-left:15%; font-size:7pt">'.Helper::AlfSpace('PERPUSTAKAAN',1).'</strong>
						<strong style="width:100%;margin-left:15%; font-size:7pt">'.Helper::AlfSpace('PT PJB',1).'</strong>
					</td>
					<td align="center" style="width:20%;"></td>
				</tr>
			</table>
			<hr>
			<div align="center" style="width:100%; margin-bottom:2pt; font-size:7pt"><strong>'.Helper::AlfSpace(str_replace($kdjenispustaka,$kdjenispustaka.'.',$noseri),1).'</strong></div>
			<div align="center" style="width:100%; margin-bottom:2pt; font-size:7pt"><strong>'.Helper::AlfSpace($insauth,1).'</strong></div>
			<div align="center" style="width:100%; margin-bottom:2pt; font-size:7pt"><strong>'.Helper::AlfSpace($insjudul,1).'</strong></div>
			';
				
		$barcode = '
			<table align="center" style="width:100%;margin-top:-13px;background:#fff;">
				<tr>
					<td align="center" style="width:25%;"><img src="images/favicon.ico" height="40"></td>
					<td align="center" style="width:65%;"><font size="1"><b>PERPUSTAKAAN<br>PT PJB</b></font></td>
					<td align="center" style="width:10%;"></td>
				</tr>
			</table>
			<hr>
			<div align="center" style="margin:10px;"><img src="index.php?page=image&key='.$noseri.'">
			<div style="margin-top:-13px;background:#fff;position:relative;z-index:1;">
				<font size=1><b>'.Helper::AlfSpace($noseri,2).'</b></font></div>
			</div>';
			
		if($key == "P"){
			$cetak = $punggung;
		}elseif($key == "B"){
			$cetak = $barcode;
		}
		return $cetak;
	}
	
	
	function generateNoseriDigitalContent($conn, $kdjenispustaka, $jmljur, $jur1){
		#Tugas Akhir, Skripsi, Disertasi, Tesis
		if($kdjenispustaka == "KT" or $kdjenispustaka == "SR" or $kdjenispustaka == "DS" or $kdjenispustaka == "TS"){
			if($jur1){
				$singkatan = $conn->GetOne("select namapendek from lv_jurusan where kdjurusan = '$jur1' ");
			}
			
			
			
			$depan = $kdjenispustaka."-".$singkatan."-".substr(date('Y'),2,2);
		}else{
			$depan = $kdjenispustaka;
		}
		
		$getseri=$conn->GetRow("select max(replace(noseri,'$depan','')) as maxseri
					from ms_pustaka p
					where noseri like '$depan%' ");	

		$noseribaru = $depan.str_pad($getseri['maxseri']+1,4,'0',STR_PAD_LEFT);
		
		return $noseribaru;
	}
	
	function getFoto($idanggota, $idpegawai, $kdjenisanggota){
		
		if($kdjenisanggota == "U"){#Umum
			$dirfoto = Config::dirFoto."anggota/".trim($idanggota).'.jpg';
			$viewfoto = Config::fotoUrl."anggota/".trim($idanggota).'.jpg';	
		}elseif($kdjenisanggota == "A" or $kdjenisanggota == "M"){#Alumni, Mahasiswa
			// koneksi ke akademik
			$conna = Factory::getConnAkad();
			#akad
			$mhs = $conna->GetRow("select substring(periodemasuk,1,4) angkatan, mhsid, nim from akademik.ms_mahasiswa where nim = '$idanggota'");
			$fold = $mhs['angkatan']."/".trim($idanggota).'.jpg';
			$dirfoto = Config::dirFotoMhs.$fold;

			if(!(is_file($dirfoto))){
				$fold = "mahasiswa/".md5($mhs['mhsid'].'asdf1234').'.jpg';
				$dirfoto = Config::dirFotoMhs.$fold;
			}
			$viewfoto = Config::fotoMhsUrl.$fold;
		}elseif($kdjenisanggota == "D" or $kdjenisanggota == "T"){#Dosen, Karyawan
			$dirfoto = Config::dirFotoPeg.trim($idpegawai).'.jpg';
			$viewfoto = Config::fotoPegUrl.trim($idpegawai).'.jpg';
		}

		if(is_file($dirfoto)){
			$v_foto = $viewfoto;
		}else{
			$v_foto = Config::fotoUrl."default1.jpg";
		}
		
		return $v_foto;
	}
	
	function getFoto2($conn,$idanggota){
		$a = $conn->GetRow("select idpegawai, kdjenisanggota from ms_anggota where idanggota = '$idanggota' ");
		$idpegawai = $a['idpegawai'];
		$kdjenisanggota = $a['kdjenisanggota']; 
		
		if($kdjenisanggota == "U"){#Umum
			$dirfoto = Config::dirFoto."anggota/".trim($idanggota).'.jpg';
			$viewfoto = Config::fotoUrl."anggota/".trim($idanggota).'.jpg';	
		}elseif($kdjenisanggota == "A" or $kdjenisanggota == "M"){#Alumni, Mahasiswa
			// koneksi ke akademik
			$conna = Factory::getConnAkad();
			#akad
			$mhs = $conna->GetRow("select substring(periodemasuk,1,4) angkatan, mhsid, nim from akademik.ms_mahasiswa where nim = '$idanggota'");
			$fold = $mhs['angkatan']."/".trim($idanggota).'.jpg';
			$dirfoto = Config::dirFotoMhs.$fold;

			if(!(is_file($dirfoto))){
				$fold = "mahasiswa/".md5($mhs['mhsid'].'asdf1234').'.jpg';
				$dirfoto = Config::dirFotoMhs.$fold;
			}
			$viewfoto = Config::fotoMhsUrl.$fold;
		}elseif($kdjenisanggota == "D" or $kdjenisanggota == "T"){#Dosen, Karyawan
			$dirfoto = Config::dirFotoPeg.trim($idpegawai).'.jpg';
			$viewfoto = Config::fotoPegUrl.trim($idpegawai).'.jpg';
		}

		if(is_file($dirfoto)){
			$v_foto = $viewfoto;
		}else{
			$v_foto = Config::fotoUrl."default1.jpg";
		}
		
		return $v_foto;
	}
	
	function isLibur($conn,$tgl) {
		$libur = $conn->GetOne("select count(*) from ms_libur where tgllibur = '$tgl' ");
		if($libur)
			return 1;
		else
			return 0;
	}
	
	function getLibur($conn,$bulan,$tahun){
		$libur = $conn->GetArray("select to_char(tgllibur,'dd') tgl, namaliburan from ms_libur where to_char(tgllibur,'mm') = '$bulan' and  to_char(tgllibur,'YYYY') = '$tahun' group by to_char(tgllibur,'dd'), namaliburan order by tgl ");
		return $libur;
	}
	
	function tglTenggat($conn, $tgltenggat){
		$tottgl = strtotime($tgltenggat);
		$tglrange = date("Y-m-d",($tottgl+(86400*30)));  

		$p_libur=$conn->GetArray("select tgllibur
					from ms_libur
					where to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') >= to_date('$tgltenggat','YYYY-mm-dd')
						and to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') <= to_date('$tglrange','YYYY-mm-dd')
					and to_char(tgllibur, 'D')!=0 and to_char(tgllibur, 'D')!=6
					order by tgllibur ");
		
		$thk=0;
		$tgllibur = $tgltenggat;
		$libur = array();
		if(count($p_libur)>0){
			foreach($p_libur as $r_lib){
				if($r_lib['tgllibur']==$tgllibur){
					$thk +=1;
					$tgllibur=date("Y-m-d",(strtotime($tgllibur)+86400));
					if(date("w",strtotime($tgllibur))==0){
						$thk +=1;
						$tgllibur=date("Y-m-d",(strtotime($tgllibur)+86400));
					}
				}else
					break;		
			}
			
			foreach($p_libur as $key => $value){
				foreach($value as $k => $v){
					$libur[] = $v;
				}
			}
		}else
			$thk=0;
			
		$tglt=(($tottgl)+(86400*$thk)); 
		$jum=0;
		$i=$tglt;
		
		$h=date("w",$i);
		if($h==0){ #minggu
				$jum +=1;
			}
			
		if($h==6){ #sabtu
				$jum +=2;
			}
		$tglh=($tglt+($jum*86400)); //waktu max pinjm 14 + jml minggu 2 = 16
		$tglhasil=date("Y-m-d",$tglh);

		if(count($p_libur)>0){
			if(in_array($tglhasil,$libur)){
				return sipus::tglTenggat($conn, $tglhasil);
			}else
				return $tglhasil;
		}
		
		
		return $tglhasil;
	}
	
	#check unit eksemplar & peminjam/user
	function checkUnit($conn, $idanggota, $eksemplar){
		$satker = $conn->GetOne("select idunit from ms_anggota where idanggota = '$idanggota' ");
		$unit = $conn->GetOne("select l.idunit from pp_eksemplar e
				join lv_lokasi l on l.kdlokasi = e.kdlokasi
				where e.ideksemplar = $eksemplar ");
		if($satker == $unit)
			return true;
		else
			return false;
	}
	
	function unita(){
		$unita = explode("_##_",Config::acceptUnit);
		return $unita;
	}
		
	function checkUnit_PJBS($conn, $idanggota, $eksemplar){
		$satker = $conn->GetOne("select idunit from ms_anggota where idanggota = '$idanggota' ");
		$unit = Sipus::unita();
		if(in_array($satker,$unit))
			return true;
		else
			return false;
	}
	
}
?>
