<? 
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

require_once('phpmailer/class.phpmailer.php');

class Mail {

	function srtTagih($conn, $key) {
		$data = $conn->Execute("select *
				       from v_tagih
				       where idanggota='$key' and kdjenistransaksi='PJN'
				       and to_date(TO_CHAR(tgltenggat,'dd-mm-yyyy'),'dd-mm-yyyy') < to_date(TO_CHAR(current_date,'dd-mm-yyyy'),'dd-mm-yyyy') 
				       and tglpengembalian is null  ");
		$tujuan = $data->fields['email'];
		$toname = $data->fields['namaanggota'];

		$isi="	<table cellpadding='0' cellspacing='0' width=''.$p_tbwidth.''>";
		$isi.="<tr>";
		$isi.="	<td><font size='+3'><strong>PERPUSTAKAAN PJB</strong></font></td>";
		$isi.="</tr>";
		$isi.="</table><br>";
		$isi.="<hr style='width:'.$p_tbwidth.'' align='left'>";
		$isi.="<table cellpadding='0' cellspacing='0' width=''.$p_tbwidth.''>";
		$isi.="</tr>";
		$isi.="<tr>";
		$isi.="	<td>Hal.</td>";
		$isi.="	<td>: </td>";
		$isi.="	<td>Tagihan Buku ";
		$isi.="	</td>";
		$isi.="</tr>";
		$isi.="</table><br>";
		$isi.="<table cellpadding='0' cellspacing='0' width='650'>
				<tr>
					<td width='100'>Kepada. Yth :</td>
				</tr>
				<tr>
					<td>Nama</td>
					<td>:  ".$data->fields['email']." </td>
				</tr>
				<tr>
					<td>NRP</td>
					<td>: ".$data->fields['idanggota']." </td>
				</tr>
				</table>
				<br><br>";
		$isi.="<table cellpadding='0' cellspacing='0' width='<?= $p_tbwidth?>'>";
		$isi.="	<tr>";
		$isi.="		<td>";
		$isi.="		Dengan Hormat, <br>";
		$isi.="		Untuk mendata kembali Administrasi buku-buku pinjaman yang telah dipinjam oleh pemakai Perpustakaan";
		$isi.="		PJB, perkenankanlah kami mengingatkan, bahwa menurut database yang ada pada kami,";
		$isi.="		Saudara masih mempunyai pinjaman buku sebagai berikut : <br><br>";
		$isi.="		<table border='1' style='border-collapse:collapse' width='100%'>";
		$isi.="			<tr>";
		$isi.="				<td rowspan='2' align='center'><strong>NO.</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>JUDUL</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>PENGARANG</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>REG.COMP</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>CALL NUMBER</strong></td>";
		$isi.="				<td colspan='2' align='center'><strong>TANGGAL</strong></td>";
		$isi.="			</tr>";
		$isi.="			<tr>";
		$isi.="				<td align='center'><strong>PINJAM</strong></td>";
		$isi.="				<td align='center'><strong>TENGGAT</strong></td>";
		$isi.="			</tr>";
						$i=1; while ($row = $data->FetchRow()){
		$isi.="			<tr>";
		$isi.="				<td>".$i."</td>";
		$isi.="				<td>".$row['judul']."</td>";
		$isi.="				<td>".$row['authorfirst1']." ".$row['authorlast1']."</td>";
		$isi.="				<td>".$row['noseri']."</td>";
		$isi.="				<td>".$row['nopanggil']."</td>";
		$isi.="				<td>". Helper::tglEngTime($row['tgltransaksi']) ."</td>";
		$isi.="				<td>". Helper::tglEngTime($row['tgltenggat']) ."</td>";
		$isi.="			</tr>";
						$i++; }
		$isi.="		</table>";
		$isi.="		<br><br>";
		$isi.="		Kami mengharap Saudara dapat meluangkan waktu untuk memenuhi kewajiban mengembalikan buku/memberikan";
		$isi.="		informasi ke Perpustakaan melalui telepon atau email.<br><br>";
		$isi.="		Terima Kasih<br><br><br><b>NB : Apabila Anda sudah mengembalikan buku tersebut, mohon email ini diabaikan</b>";
		$isi.="	</td>";
		$isi.="</tr>";
		$isi.="</table>";
		
		$subject = "Surat Penagihan Pengembalian Pustaka - PJB Library";
		
		// to check email was send
		Mail::sendMail('mfahruddinabdillah11@gmail.com','fachruddin_abdillah',$subject,$isi);
		
		Mail::sendMail($tujuan,$toname,$subject,$isi);
	}
	
	function mailPerpanjangan($conn,$status,$idperpanjangan){
		$dataTo = $conn->GetRow("select u.email, u.nama, u.nid, m.judul, m.noseri   
				from pp_perpanjangan p
				join pp_transaksi t on t.idtransaksi = p.idtransaksi
				join um.users u on u.nid = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka m on m.idpustaka = e.idpustaka 
				where p.idperpanjangan = '$idperpanjangan' ");

		if($dataTo['email']){
			Mail::sendMail($dataTo['email'],$dataTo['nama'],"Konfirmasi Perpanjangan","Pengajuan Perpanjangan peminjaman atas buku ".$dataTo['judul']." $status.");
		}
	}
	
	function statuspesan($conn, $ideksemplar, $email, $status){
		$sql = "select p.judul, e.noseri 
			from pp_eksemplar e 
			join ms_pustaka p on p.idpustaka = e.idpustaka 
			where e.ideksemplar = $ideksemplar ";
		$pustaka = $conn->GetRow($sql);
		Mail::sendMail($email,$dataTo['nama'],"Konfirmasi Pemesanan Pustaka","Pemesanan peminjaman atas buku ".$pustaka['judul']." $status.");
	}
	
	function alertTenggat($conn, $idtransaksi){
		$sql = "select a.email, a.namaanggota, to_char(t.tgltenggat,'dd-mm-yyyy') tglbatas, to_char(t.tgltransaksi,'dd-mm-yyyy') tglpinjam,
				p.judul, e.noseri, a.idanggota  
			from pp_transaksi t 
			join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
			join ms_pustaka p on p.idpustaka = e.idpustaka
			join ms_anggota a on a.idanggota = t.idanggota 
			where e.ideksemplar = $ideksemplar ";
		$pustaka = $conn->GetRow($sql);

		$isi="	<table cellpadding='0' cellspacing='0' width=''.$p_tbwidth.''>";
		$isi.="<tr>";
		$isi.="	<td><font size='+3'><strong>PERPUSTAKAAN PJB</strong></font></td>";
		$isi.="</tr>";
		$isi.="</table><br>";
		$isi.="<hr style='width:'.$p_tbwidth.'' align='left'>";
		$isi.="<table cellpadding='0' cellspacing='0' width=''.$p_tbwidth.''>";
		$isi.="</tr>";
		$isi.="<tr>";
		$isi.="	<td>Hal.</td>";
		$isi.="	<td>: </td>";
		$isi.="	<td>Notifikasi Pengembalian Pustaka Pinjam ";
		$isi.="	</td>";
		$isi.="</tr>";
		$isi.="</table><br>";
		$isi.="<table cellpadding='0' cellspacing='0' width='650'>
				<tr>
					<td width='100'>Kepada. Yth :</td>
				</tr>
				<tr>
					<td>Nama</td>
					<td>:  ".$row['namaanggota']." </td>
				</tr>
				<tr>
					<td>NRP</td>
					<td>: ".$row['idanggota']." </td>
				</tr>
				</table>
				<br><br>";
		$isi.="<table cellpadding='0' cellspacing='0' width='<?= $p_tbwidth?>'>";
		$isi.="	<tr>";
		$isi.="		<td>";
		$isi.="		Dengan Hormat, <br>";
		$isi.="		Untuk mendata kembali Administrasi buku-buku pinjaman yang telah dipinjam oleh pemakai Perpustakaan";
		$isi.="		PJB, perkenankanlah kami mengingatkan, bahwa menurut database yang ada pada kami,";
		$isi.="		Saudara masih mempunyai pinjaman buku sebagai berikut : <br><br>";
		$isi.="		<table border='1' style='border-collapse:collapse' width='100%'>";
		$isi.="			<tr>";
		$isi.="				<td rowspan='2' align='center'><strong>JUDUL</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>REG.COMP</strong></td>";
		$isi.="				<td rowspan='2' align='center'><strong>CALL NUMBER</strong></td>";
		$isi.="				<td colspan='2' align='center'><strong>TANGGAL</strong></td>";
		$isi.="			</tr>";
		$isi.="			<tr>";
		$isi.="				<td align='center'><strong>PINJAM</strong></td>";
		$isi.="				<td align='center'><strong>TENGGAT</strong></td>";
		$isi.="			</tr>";
		$isi.="			<tr>";
		$isi.="				<td>".$row['judul']."</td>";
		$isi.="				<td>".$row['noseri']."</td>";
		$isi.="				<td>".$row['nopanggil']."</td>";
		$isi.="				<td>". Helper::tglEngTime($row['tgltransaksi']) ."</td>";
		$isi.="				<td>". Helper::tglEngTime($row['tgltenggat']) ."</td>";
		$isi.="			</tr>";
		$isi.="		</table>";
		$isi.="		<br><br>";
		$isi.="		Kami mengharap Saudara dapat meluangkan waktu untuk memenuhi kewajiban mengembalikan buku/memberikan";
		$isi.="		informasi ke Perpustakaan melalui telepon atau email.<br><br>";
		$isi.="		Terima Kasih<br><br><br><b>NB : Apabila Anda sudah mengembalikan buku tersebut, mohon email ini diabaikan</b>";
		$isi.="	</td>";
		$isi.="</tr>";
		$isi.="</table>";
		
		$subject = "Notifikasi Pengembalian Pustaka - PJB Library";
		Mail::sendMail($row['email'],$row['namaanggota'],$subject,$isi);
	}
	
	function alertAvailable(){
		
	}
	

	function sendMail($mailto,$toname,$subject,$body) {
		$mail = new PHPMailer;
		$mail->IsSMTP();  // send via SMTP
		try {
			$mail->IsHTML(true);
			$mail->SMTPAuth = true;
			
			$mail->Host		= Config::SMTPHost;
			$mail->Port		= Config::SMTPPort;
			$mail->Username		= Config::SMTPUser;
			$mail->Password		= Config::SMTPPass;
			
			$mail->SMTPSecure = 'tls';
			$mail->ClearAddresses();
			$mail->AddAddress($mailto,$toname);
			$mail->From = Config::AdminEmail;
			$mail->FromName = Config::AdminName;
			$mail->Subject = $subject;
			$mail->Body = $body;			
			$mail->Send();
			
			
			
			$err = false;
		} catch (phpmailerException $e) {
			$err = true;
		} catch (Exception $e) {
			$err = true;
		}
		
		return $err;
	}

}
?>
