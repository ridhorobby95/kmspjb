<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

//class untuk barcode
require('class/BCGFont.php');
require('class/BCGColor.php');
require('class/BCGDrawing.php');
require('class/BCGisbn.barcode.php');
require('class/BCGcode39.barcode.php');
require('class/BCGcode128.barcode.php');

// Class untuk barcode
class BarCode {
	
	# generate barCODE
	function MakeCode($idseri) {
		$font = new BCGFont('classes/class/font/Arial.ttf', 7);
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255);

		$code = new BCGisbn();
		$code->setScale(1);
		$code->setThickness(25);
		$code->setForegroundColor($color_black);
		$code->setBackgroundColor($color_white);
		$code->setFont($font);
		$code->parse($idseri);

		// Drawing Part
		$drawing = new BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		//header('Content-Type: image/png');

		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}	
	
	function MakeCode39($idseri) {
		$font = new BCGFont('classes/class/font/Arial.ttf', 9);
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255);

		$code = new BCGcode39();
		$code->setScale(1);
		$code->setThickness(35);
		$code->setForegroundColor($color_black);
		$code->setBackgroundColor($color_white);
		$code->setFont($font);
		$code->setChecksum(false);
		$code->parse($idseri);

		// Drawing Part
		$drawing = new BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		//header('Content-Type: image/png');

		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	
	}
	
	function MakeCodeBaru($idseri){
		// Buat ukuran 300 x 70
		$width = strlen('*'.$idseri.'*');
		$im = imagecreatetruecolor(($width*17), 70);
		$white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);
		$black = imagecolorallocate($im, 0x00, 0x00, 0x00);

		// Make the background red
		imagefilledrectangle($im, 0, 0, 299, 99, $white);

		// Path to our ttf font file
		$font_file = 'classes/backfont.ttf';

		// Draw the text 'PHP Manual' using font size 13
		imagefttext($im, 13, 0, 0, 55, $black, $font_file, '*'.$idseri.'*');

		// Output image to the browser
		header('Content-Type: image/png');

		imagepng($im);
		imagedestroy($im);
	}
	
	function MakeCode128($idseri) {
		$font = new BCGFont('classes/class/font/Arial.ttf', 0);
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255);

		$code = new BCGcode128();
		$code->setScale(1);
		$code->setThickness(30);
		$code->setForegroundColor($color_black);
		$code->setBackgroundColor($color_white);
		$code->setFont($font);
		//$code->setChecksum(false);
		$code->parse($idseri);

		// Drawing Part
		$drawing = new BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		//header('Content-Type: image/png');

		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	
	}
}
?>