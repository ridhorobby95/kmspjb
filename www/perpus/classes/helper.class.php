<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

require_once('config.class.php');

$menu_access = $menu_unserialize = '';

class Helper {
	/* FUNGSI SISTEM */
	
	/* BARU */
	
	// mengambil data session
	function getSessionDataBaru($conn,$data) {
		global $conf;
		
		$xml = self::decryptSessionData($data);
		
		$p = xml_parser_create();
		xml_parse_into_struct($p,$xml,$data,$idx);
		xml_parser_free($p);
		
		// data
		$session = array();
		$session['loginuid'] = $data[$idx['USERNAME'][0]]['value'];
		$session['authenticated'] = 1;
		$session['userid'] = $data[$idx['USERID'][0]]['value'];
		$session['username'] = $data[$idx['USERNAME'][0]]['value'];
		$session['userdesc'] = $data[$idx['USERDESC'][0]]['value'];
		$session['lastlogin'] = $data[$idx['LASTLOGIN'][0]]['value'];
		$session['logintime'] = date('Y-m-d H:i:s');
		$session['idmodul'] = $data[$idx['KODEMODUL'][0]]['value'];
		$session['idrole'] = $data[$idx['KODEROLE'][0]]['value'];
		$session['namarole'] = $data[$idx['NAMAROLE'][0]]['value'];
		$session['idsatker'] = $data[$idx['KODEUNIT'][0]]['value'];
		$session['namasatker'] = $data[$idx['NAMAUNIT'][0]]['value'];
		$session['satkerleft'] = $data[$idx['INFOLEFT'][0]]['value'];
		$session['satkerright'] = $data[$idx['INFORIGHT'][0]]['value'];
		
		// menu
		$session['menu'] = self::getMenuRole($conn,$session['idmodul'],$session['idrole']);
		
		$_SESSION['menupage'] = Config::gateUrl;
		$_SESSION['gate'] = $session;
		
		//var_dump($_SESSION['gate']);die();
		
	}
	
	// dekripsi data session
	function decryptSessionData($data) {
		$encryptKey = 'SEVIMA'; // key untuk dekripsi
		
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $encryptKey, $data, MCRYPT_MODE_ECB, $iv);
		
		return $dec;
	}
	
	function xIsLoggedIn($conn) {

		if ($_SESSION[G_SESSION]['remote_addr'] == $_SERVER['REMOTE_ADDR']
			&& $_SESSION[G_SESSION]['user_agent'] == $_SERVER['HTTP_USER_AGENT']
			&& $_SESSION[G_SESSION]['iduser']) {
			
			$idx = $conn->GetRow("select  a.idanggota, a.kdjenisanggota, a.namaanggota, a.idunit, s.namasatker, s.kdsatker,   
						u.iduser, u.nid, to_char(u.lastlogintime,'dd-mm-YYYY HH:mm:dd ') lastlogintime, u.lastloginip
					from ms_anggota a
					left join ms_satker s on s.kdsatker = a.idunit 
					left join um.users u on u.nid = a.idanggota 
					where u.iduser = ".$_SESSION[G_SESSION]['iduser'] );
			
			$role = $_SESSION[G_SESSION]['idrole'];
			
			$session = array();
			$session['loginuid'] = $idx['idanggota'];
			$session['authenticated'] = 1;
			$session['userid'] = $idx['iduser'];
			$session['username'] = $idx['idanggota'];
			$session['userdesc'] = $idx['namaanggota'];
			$session['lastlogin'] = $idx['lastlogintime'];
			$session['logintime'] = date('Y-m-d H:i:s');
			$session['idmodul'] = 2;
			$session['idsatker'] = $idx['idunit'];
			$session['namasatker'] = $idx['namasatker'];
			
			$_SESSION['gate'] = $session;
			$_SESSION['PERPUS_USER'] = $_SESSION['gate']['username'];
			$_SESSION['PERPUS_NAME'] = $_SESSION['gate']['userdesc'];
			$_SESSION['PERPUS_LASTLOG'] = substr($_SESSION['gate']['lastlogin'],0,19);
			$_SESSION['PERPUS_ROLE'] = $role;
			$_SESSION['PERPUS_GROLE'] = $_SESSION['gate']['idrole'];
			$_SESSION['PERPUS_NAMAROLE'] = $_SESSION['gate']['namarole'];
			$_SESSION['PERPUS_MODUL'] = $_SESSION['gate']['idmodul'];
			$_SESSION['PERPUS_SATKER'] = $_SESSION['gate']['idsatker'];
			$_SESSION['PERPUS_NAMASATKER'] = $_SESSION['gate']['namasatker'];
			$_SESSION['PERPUS_SATKER_L'] = $_SESSION['gate']['satkerleft'];
			$_SESSION['PERPUS_SATKER_R'] = $_SESSION['gate']['satkerright'];
			$_SESSION['PERPUS_PERIODEAKAD'] = '20091'; 
			$_SESSION['PERPUS_INITPAGE'] = $page;
			$_SESSION['PERPUS_ITEMAUTH'] =  $_SESSION['gate']['roleitem'];
			$_SESSION['PERPUS_MENU'] = $_SESSION['gate']['menu'];
			
			if($role == '1' )
				$_SESSION['PERPUS_ADMIN'] = 1;
	
			unset($_SESSION['gate']);
			
			return true;
		}

		return false;
	}	
	function getMenuRole($conn) {
		global $menu_unserialize, $menu_access;

		$idrole = $_SESSION[G_SESSION]['idrole'];
		$iduser = $_SESSION[G_SESSION]['iduser'];
 
 		$temp = unserialize(file_get_contents(Config::NilaiPerpus)); 
		foreach ($temp as $menu) {
			$menu_unserialize[] = array_change_key_case($menu);
		}
		$_SESSION['PERPUS_MENU'] = $menu_unserialize;

		$rows = $conn->GetArray("select * from um.roleitem where idrole='$idrole'" );
		$roleitems = array();	
		foreach ($rows as $row) {
			$roleitems[$row['iditem']] = $row;
		}

		$rows = $conn->GetArray("select r.iditem, iscreate, isread, isupdate, isdelete
					from um.roleitem r
					join um.item i on r.iditem=i.iditem 
					where i.isactive=1 and idrole=$idrole");
		if($rows){
			foreach ($rows as $row) {
				$item_access[$row['iditem']] = $row;
			}
		}
		
		$rows = $conn->GetArray("select idmenu, nama, idparent, iditem, itemaccess
					from um.menu where isactive=1 ");
		foreach ($rows as $row) {
			$menu_access[$row['idmenu']] = $roleitems[$row['iditem']];
		}		

		$menus = "<ul class=\"nav navbar-nav\">\n" . Helper::_drawMenu() . "</ul>\n";

		return $menus;
	}
	
	function _drawMenu($idparent=false) {
		global $menu_unserialize, $menu_access;
		$ret = '';
		
		foreach ($menu_unserialize as $menu) {
			
			if ($menu['idparent'] != $idparent)
				continue;
			
			if (!$menu['isactive'])
				continue;
			
			if ($menu['nama'] == '---') {
				$ret .= '<li><hr/></li>';
				continue;
			}
			
			
			$has_child = Helper::_menuHasChild($menu['idmenu']);
			
			/*if (!$has_child && !$menu['iditem']) {
				continue;
			}*/
			
			if (!$menu_access[$menu['idmenu']]['isread'] && !$has_child) {
				continue;
			}
			
			$class_li = '';
			$tabindex = '';
			$data_toggle = '';
			$dropdown_toggle = '';
			$class_dropdown_toggle = '';
			if (!$menu['idparent']) {
				$class_li = 'class="dropdown"';
				$data_toggle = 'data-toggle="dropdown"';
				$class_dropdown_toggle = 'class="dropdown-toggle"';
			}
			else {
				if ($has_child and !$menu['idparent']) {
					$tabindex = 'tabindex="-1"';
					$class_li = 'class="dropdown-submenu"';
				}elseif($has_child and $menu['idparent']){
					//$tabindex = 'tabindex="-1"';
					//$class_li = 'class="dropdown-submenu"';
					$class_li = 'class="dropdown-submenu"';
					$data_toggle = 'data-toggle="dropdown"';
					$class_dropdown_toggle = 'class="dropdown-toggle"';
				}
			}
			
			$ret .= "<li $class_li>";
			
			if ($has_child) {
				$ahref = '';
				if ($menu['namafile'])
					$ahref = 'href="' . $menu['namafile'] . '"';
					
				$ret .= "<a $tabindex $data_toggle $class_dropdown_toggle $ahref>{$menu['nama']} ";
					
				if(empty($menu['idparent']))
					$ret .= "<b class=\"caret\"></b>";
				$ret .= "</a>";
			}
			else {
				if ($menu['namafile'])
					$ret .= "<a href=\"".$menu['namafile']."\">{$menu['nama']}</a>";
				else
					$ret .= $menu['nama'];
			}
			
			if ($has_child)
				$ret .= "\n<ul class=\"dropdown-menu\" role=\"menu\">\n";

			$ret .= Helper::_drawMenu($menu['idmenu']);

			if ($has_child)
				$ret .= "</ul>\n";

			$ret .= "</li>\n";
		}
		
		return $ret;
	}
	
	function _menuHasChild($idmenu) {
		global $menu_unserialize, $menu_access;
		$ret = 0;
		foreach ($menu_unserialize as $menu) {
			if ($menu['idparent'] == $idmenu) {
				if ($menu['isactive'] && $menu_access[$menu['idmenu']]) {
					$ret += 1;
                    //return $ret;
				}

				$ret += Helper::_menuHasChild($menu['idmenu']);
			}
		}
        
		return $ret;
	}
		
	// mendapatkan hak akses sebuah file
	function getFileAuth($conn,$modul,$file,$role) {
		$sql = "select r.caninsert, r.canupdate, r.candelete, r.aksesmenu from gate.sc_menurole r
				join gate.sc_menufile f on r.idmenu = f.idmenu and f.filemenu = '$file'
				join gate.sc_menu m on r.idmenu = m.idmenu and m.kodemodul = '$modul'
				where r.koderole = '$role'";
		
		return $conn->GetArray($sql);
	}
	
	/* END OF BARU */
	
	// dekripsi untuk pengiriman data
	function decrypt($c_t, $key) {
		$c_t = base64_decode(strtr($c_t,'-_~', '+/='));
		$iv = substr(md5($key), 0,mcrypt_get_iv_size (MCRYPT_CAST_256,MCRYPT_MODE_CFB));
		$p_t = mcrypt_cfb (MCRYPT_CAST_256, $key, $c_t, MCRYPT_DECRYPT, $iv);
		return trim($p_t);
	}
	
	// enkripsi untuk pengiriman data
	function encrypt($plain_text, $key) {
		$plain_text = trim($plain_text);
		$iv = substr(md5($key), 0,mcrypt_get_iv_size (MCRYPT_CAST_256,MCRYPT_MODE_CFB));
		$c_t = mcrypt_cfb (MCRYPT_CAST_256, $key, $plain_text, MCRYPT_ENCRYPT, $iv);
		return strtr(base64_encode($c_t),'+/=', '-_~');
	}
	
	// mendapatkan data session
	function getSessionData($data) {
		$data = urldecode($data);
		$data = substr($data, 0, strlen($data) - 1);
		$arr_data = explode(";",$data);
		
		$return = array();
		while (list ($key, $val) = each ($arr_data)) {
			$arr_str = explode("|", $val);
	
			$val3 = $arr_str[1];
			if (!preg_match("/a:/i", $val3)) {
				$val3 = str_replace(array("{", "}"),"",$val3);
				if (preg_match("/s:/i", $val3)) {
					$val3 = unserialize($val3);
				}
				else if (preg_match("/i:/i", $val3)) {
					$val3 = (int)substr($val3,2);
				}
			}
			
			$return[$arr_str[0]] = $val3;
		}
		
		return $return;
	}
	
	// membentuk url dengan satu pintu
	function navAddress($url) {
		if(strcasecmp(substr($url,-4),'.php') == 0)
			$url = substr($url,0,strlen($url)-4);
		return Config::webUrl.'?page='.$url;
	}
	
	// menuju suatu url dengan satu pintu
	function navigate($url) {
		Helper::redirect(Config::webUrl.'?page='.$url);
		exit;
	}
	
	// menuju gate
	function navigateOut() {
		Helper::redirect(Config::gateUrl);
		exit;
	}
	
	// normalisasi request
	function normalizeRequest() {
		if(get_magic_quotes_gpc()) {
			if(isset($_GET))
				$_GET=Helper::stripSlashes($_GET);
			if(isset($_POST))
				$_POST=Helper::stripSlashes($_POST);
			if(isset($_REQUEST))
				$_REQUEST=Helper::stripSlashes($_REQUEST);
			if(isset($_COOKIE))
				$_COOKIE=Helper::stripSlashes($_COOKIE);
		}
	}
	
	// this page, halaman ini
	function recentURL($includeParams=false)
	{
		if ($includeParams)
			$http = $_SERVER["REQUEST_URI"];
		else
			$http = $_SERVER["PHP_SELF"];
		return htmlentities(strip_tags($http));
	}
	
	// redirect ke halaman lain
	function redirect($url=NULL) {
	if (!$url) {
			$currentFile = $_SERVER['PHP_SELF'];
			$parts = Explode('/', $currentFile);
			$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'];
		}
   		header("Location: $url");
    	exit;
	}
	
	// melakukan stripslashes
	function stripSlashes(&$data) {
		return is_array($data)?array_map(array('Helper','stripSlashes'),$data):stripslashes($data);
	}
	
	/*FUNGSI MENGOLAH TANGGAL */
	/* Get count of years between dates */
	function yearsBetween($dateStart, $dateEnd) {
	   return date('Y', $dateEnd) - date('Y', $dateStart);
	}
	
	function monthsBetween($dateStart, $dateEnd) {
	   return date('n', $dateEnd) + (Helper::yearsBetween($dateStart, $dateEnd) * 12) - date('n', $dateStart);
	}
	
	
	/* FUNGSI MANIPULASI STRING */
	
	// strip selain alfanumerik
	function cAlphaNum($item,$allow='') {
		return preg_replace('/[^a-zA-Z0-9'.$allow.']/', '$1', $item);
	}
	
	// strip selain numerik
	function cNum($item) {
		return (float)preg_replace('/[^0-9]/', '$1', $item);
	}
	
	// untuk elemen array, lakukan cStrNull
	function cStrFill($src,$elem='',$idx=false) {
		if(is_array($elem)) {
			$retarr = array();
			for($i=0;$i<count($elem);$i++) {
				if(is_array($src[$elem[$i]])) {
					if($idx === false)
						$retarr[$elem[$i]] = Helper::cStrFill($src[$elem[$i]]);
					else
						$retarr[$elem[$i]] = Helper::cStrNull($src[$elem[$i]][$idx]);
				}
				else
					$retarr[$elem[$i]] = Helper::cStrNull($src[$elem[$i]]);
			}
		}
		else {
			$retarr = array();
			foreach($src as $key => $val) {
				if(is_array($val)) {
					if($idx === false) 
						$retarr[$key] = Helper::cStrFill($val);
					else 
						$retarr[$key] = Helper::cStrNull($val[$idx]);
				}
				else
					$retarr[$key] = Helper::cStrNull($val);
			}
		}
		
		return $retarr;
	}
	
	// format penulisan bilangan juga uang
	function formatNumber($num,$ndec='',$ismoney=false,$format=true) {
		if($num == '')
			return $num;
		
		if($ndec == '') {
			if($ismoney)
				$ndec = 2;
			else
				$ndec = 0;
		}
		
		if($num < 0) {
			$num = abs($num);
			$bracket = true;
		}
		
		// untuk misalnya 100.000 jadi 100000, tapi desimal disederhanakan dulu
		$num = round($num,2);
		$num = preg_replace('/\.(\d{3})/','${1}',$num);
		
		if($format)
			$num = number_format($num,$ndec,',','.');
		
		if($ismoney)
			$num = 'Rp. '.$num;
		
		if($bracket)
			return '('.$num.')';
		else
			return $num;
	}
	
	// format penulisan bilangan juga uang pada laporan
	function formatNumberRep($num,$format='html',$ndec='') {
		if($format == 'xls')
			return Helper::formatNumber($num,$ndec,false,false);
		else
			return Helper::formatNumber($num,$ndec,false);
	}
	
	// format menjadi bentuk bilangan
	function cStrDec($str,$ndec='',$convpoint=true) {
		if($str == '' or $str == 'null')
			return $str;
		
		if($convpoint)
			$str = str_replace('.','',$str);
		$str = str_replace(',','.',$str);
		
		if($ndec == '')
			return (float)$str;
		else
			return round($str,$ndec);
	}
	
	// bila $item kosong, set null, bila tidak, strip
	function cStrNull($item) {
		if($item == '')
			return 'null';
		else
			return Helper::removeSpecial($item);
	}
	
	// digunakan untuk laporan rtf supaya tidak ada kata2 null
	function cStrEmpty($item) {
		if($item == '')
			return '';
		else
			return Helper::removeSpecial($item);
	}
	
	// untuk elemen array, lakukan cStrEmpty
	function cStrFillEmpty($src,$elem='',$idx=false) {
		if(is_array($elem)) {
			$retarr = array();
			for($i=0;$i<count($elem);$i++) {
				if(is_array($src[$elem[$i]])) {
					if($idx === false)
						$retarr[$elem[$i]] = Helper::cStrFillEmpty($src[$elem[$i]]);
					else
						$retarr[$elem[$i]] = Helper::cStrEmpty($src[$elem[$i]][$idx]);
				}
				else
					$retarr[$elem[$i]] = Helper::cStrEmpty($src[$elem[$i]]);
			}
		}
		else {
			$retarr = array();
			foreach($src as $key => $val) {
				if(is_array($val)) {
					if($idx === false) 
						$retarr[$key] = Helper::cStrFillEmpty($val);
					else 
						$retarr[$key] = Helper::cStrEmpty($val[$idx]);
				}
				else
					$retarr[$key] = Helper::cStrEmpty($val);
			}
		}
		
		return $retarr;
	}
	
	// strip selain numerik, bila kosong null
	function cNumNull($item) {
		$item = preg_replace('/[^0-9]/', '$1', $item);
		
		if($item == '')
			return 'null';
		else
			return (float)$item;
	}
	
	// bila $item kosong, return 0, bila tidak, strip
	function cNumZero($item) {
		if($item == '')
			return 0;
		else
			return Helper::removeSpecial($item);
	}
	
	// mengubah format tanggal dari yyyy-mm-dd menjadi format indonesia
	function formatDateInd($ymd,$full=true,$dmy=false,$delim='-') {
		if($ymd == '')
			return '';
		if($ymd == 'null')
			return 'null';
		
		if($dmy)
			list($d,$m,$y) = explode($delim,substr($ymd,0,10));
		else
			list($y,$m,$d) = explode($delim,substr($ymd,0,10));
		
		return (int)$d.' '.Helper::indoMonth($m,$full).' '.$y;
	}
	
	// mengubah format tanggal dari dd-mm-yyyy menjadi yyyy-mm-dd dan sebaliknya
	function formatDate($dmy,$delim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		list($y,$m,$d) = explode($delim,substr($dmy,0,10));
		return $d.$delim.$m.$delim.$y;
	}
	
	// mengubah format tanggal, tapi ada time dibelakangnya
	function formatDateTime($dmy,$delim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		return Helper::formatDate(substr($dmy,0,10)).substr($dmy,10);
	}
	
	// cek apakah suatu tanggal termasuk di array interval tanggal
	function cekTanggal($tgl,$arrint) {
		if(empty($arrint))
			return false;
		
		foreach($arrint as $int) {
			$mulai = $int['mulai'];
			$selesai = $int['selesai'];
			
			if($tgl >= $mulai and $tgl <= $selesai)
				return true;
		}
		
		return false;
	}
	
	// mengubah format hhii menjadi hh:ii
	function formatTime($hi,$delim=':') {
		if($hi == '')
			return '';
		
		$hi = str_pad($hi,2,'0',STR_PAD_LEFT);
		return substr($hi,0,2).':'.substr($hi,2,2);
	}
	
	// melakukan string stripping (untuk masalah sekuritas)
	
	function removeSpecial($mystr,$stripapp = false) {
		// $pattern = '/[%&;()\"';
		$pattern = '/[%&;\"';
		if($stripapp == false) // tidak stripping ', tapi direplace jadi '', biasanya dipakai di nama
			$mystr = str_replace("'","''",$mystr);
		else
			$pattern .= "\'";
		if(preg_match('/[A-Za-z%\/]/',$mystr)) // kalau ada alfabet, %, atau /, strip <>
			$pattern .= '<>';
		$pattern .= ']|--/';
		
		return preg_replace($pattern, '$1', $mystr);
	}
	
	// mengucapkan bilangan ratusan
	function spellNumP($num) {
		$num = strval($num);
		if(!ctype_digit($num))
			return '';
		
		// hanya untuk bilangan sampai ratusan
		$len = strlen($num);
		if($len > 3) {
			$num = substr($num,0,3);
			$len = 3;
		}
		
		// membaca dari belakang
		$spell = '';
		$clen = 1;
		$belas = false;
		for($i=($len-1);$i>=0;$i--) {
			// jika pernah belasan maka continue
			if($belas === true) {
				$clen++;
				$belas = false;
				continue;
			}
			
			$cnum = $num[$i]; // bilangan yang dianalisa
			
			// menyebut bilangan
			$spellb = '';
			if($cnum == '1') {
				if($clen > 1 or ($clen == 1 and $num[$i-1] == 1))
					$spellb = 'se';
				else
					$spellb = 'satu ';
			}
			else if($cnum == '2') $spellb = 'dua ';
			else if($cnum == '3') $spellb = 'tiga ';
			else if($cnum == '4') $spellb = 'empat ';
			else if($cnum == '5') $spellb = 'lima ';
			else if($cnum == '6') $spellb = 'enam ';
			else if($cnum == '7') $spellb = 'tujuh ';
			else if($cnum == '8') $spellb = 'delapan ';
			else if($cnum == '9') $spellb = 'sembilan ';
			
			// jika bukan kosong
			if($spellb != '') {
				// jika belasan
				if($clen == 1 and $num[$i-1] == 1) {
					$spellb .= 'belas ';
					$belas = true;
				}
				
				// puluhan atau ratusan
				if($clen > 2)
					$spellb .= 'ratus ';
				else if($clen > 1)
					$spellb .= 'puluh ';
			}
			
			$spell = $spellb.$spell;
			$clen++;
		}
		
		return trim($spell);
	}
	
	// mengucapkan bilangan secara utuh
	function spellNum($num) {
		$num = strval($num);
		if(!ctype_digit($num))
			return '';
		
		$len = strlen($num);
		$spell = '';
		$clen = 1;
		
		// bilangan yang panjang diproses setiap ratusan dari akhir
		$cnum = $num;
		while($cnum != '') {
			$clen = strlen($cnum);
			if(strlen($cnum) >=3) {
				$pnum = substr($cnum,-3);
				$cnum = substr($cnum,0,($clen-3));
			}
			else {
				$pnum = $cnum;
				$cnum = '';
			}
			
			$spella[] = Helper::spellNumP($pnum);
		}
		
		$n = count($spella);
		for($i=0;$i<$n;$i++) {
			$j = $i;
			
			$spellb = '';
			while($j > 0) {
				if($j >= 4) {
					$spellb = ' triliun'.$spellb;
					$j -= 4;
				}
				else if($j == 3) {
					$spellb = ' miliar'.$spellb;
					$j -= 3;
				}
				else if($j == 2) {
					$spellb = ' juta'.$spellb;
					$j -= 2;
				}
				else if($j == 1) {
					$spellb = ' ribu'.$spellb;
					$j--;
				}
			}
			
			// kalau seribu bukan satu ribu
			if($spella[$i] == 'satu' and substr($spellb,0,5) == ' ribu')
				$spell = ' se'.trim($spellb).$spell;
			else if($spella[$i] != '')
				$spell = ' '.$spella[$i].$spellb.$spell;
		}
		
		return trim($spell);
	}
	
	// mengucapkan bilangan yang disertai koma dan titik
	function spellNumC($num) {
		// penghilangan titik
		$trans = array('.' => '');
		$num = strtr($num,$trans);
		
		// dipilah-pilah sesuai koma
		$a_num = explode(',',$num);
		$n = count($a_num);
		
		$spell = '';
		for($i=0;$i<$n;$i++) {
			$bag = $a_num[$i];
			$spell .= Helper::spellNum($bag);
			
			if($i < ($n-1))
				$spell .= 'Koma ';
		}
		
		return $spell;
	}
	
	function xlsHTMLArray($html) {
		$data = array();
		
		$i = 1;
		$arrtr = explode('</tr>',$html);
		
		foreach($arrtr as $tr) {
			$j = 1;
			$arrtd = explode('</td>',$tr);
			
			foreach($arrtd as $td) {
				$posopn = strpos($td,'<td');
				if($posopn === false)
					continue;
				
				$poscls = strpos(substr($td,$posopn),'>');
				if($poscls === false)
					continue;
				
				$td = substr($td,$posopn+$poscls+1);
				
				$data[$i][$j] = $td;
				$j++;
			}
			
			$i++;
		}
		
		return $data;
	}
	
	function pembulatan100($num) {
		$num = round($num);
		
		$bulat = (int)substr($num,-2);
		$bulat = 100 - $bulat;
		
		if ($bulat != 100)
			return ($num + $bulat);
		else
			return $num;
	}
	
	/* FUNGSI PENANGGALAN */
	
	// nama hari di bahasa indonesia
	function indoDay($nhari,$full=true) {
		if($full) {
			switch($nhari) {
				case 0: return "Minggu";
				case 1: return "Senin";
				case 2: return "Selasa";
				case 3: return "Rabu";
				case 4: return "Kamis";
				case 5: return "Jumat";
				case 6: return "Sabtu";
			}
		}
		else {
			switch($nhari) {
				case 0: return "Min";
				case 1: return "Sen";
				case 2: return "Sel";
				case 3: return "Rab";
				case 4: return "Kam";
				case 5: return "Jum";
				case 6: return "Sab";
			}
		}
	}
	
	// nama bulan di bahasa indonesia
	function indoMonth($nbulan,$full=true) {
		if($full) {
			switch($nbulan) {
				case 1: return "Januari";
				case 2: return "Pebruari";
				case 3: return "Maret";
				case 4: return "April";
				case 5: return "Mei";
				case 6: return "Juni";
				case 7: return "Juli";
				case 8: return "Agustus";
				case 9: return "September";
				case 10: return "Oktober";
				case 11: return "Nopember";
				case 12: return "Desember";
			}
		}
		else {
			switch($nbulan) {
				case 1: return "Jan";
				case 2: return "Peb";
				case 3: return "Mar";
				case 4: return "Apr";
				case 5: return "Mei";
				case 6: return "Jun";
				case 7: return "Jul";
				case 8: return "Agu";
				case 9: return "Sep";
				case 10: return "Okt";
				case 11: return "Nop";
				case 12: return "Des";
			}
		}
	}
	
	//*DB HELPER *//
		
	function checkRoleAuth($conn,$var=null) {
		if(!$_SESSION[G_SESSION]['iduser']){
			Helper::navigateOut();
			exit();
		}
		
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$page = explode("&",strtolower($_SERVER['QUERY_STRING']));
		if($var==null)
			$item = 'perpus:'.$page[0];
		else
			$item = 'perpus:page='.$var;
			
		$url = Config::umUrl . rawurlencode($item) . '/' . $idrole;
		$url = Config::umUrl . "authperpus/" . rawurlencode($item) . "/{$idrole}";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data) {
			Helper::navigateOut();
			exit();
		}

		return unserialize($data);
	}
	
	function checkRoleAuthAjax($conn,$var=null) {
		if(!$_SESSION[G_SESSION]['iduser']){
			Helper::navigateOut();
			exit();
		}
		
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$page = explode("&",strtolower($_SERVER['QUERY_STRING']));
		if($var==null)
			$item = 'perpus:'.$page[0];
		else
			$item = 'perpus:page='.$var;
			
		$url = Config::umUrl . rawurlencode($item) . '/' . $idrole;
		$url = Config::umUrl . "authperpus/" . rawurlencode($item) . "/{$idrole}";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);

		if(!$data) {
			exit();
		}

		return unserialize($data);
	}
	
	function getMenu($conn) {
		return Helper::getMenuRole($conn);
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$iduser = $_SESSION[G_SESSION]['iduser'];

		$url = Config::umUrl . "/getmenu/{$iduser}/{$idrole}";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);

		return unserialize(rawurldecode($data));
	}
	
	// cek otorisasi unit self dan child utk SDM
	function rideUnit($conn,$target) {
		if($target == '')
			return true;
		
		$sql = "select 1 from ms_satker where idsatker = '$target' and info_lft >= '".$_SESSION['PERPUS_SATKER_L']."'
				and info_rgt <= '".$_SESSION['PERPUS_SATKER_R']."'";
		$isride = $conn->GetOne($sql);
		
		if($isride == 1)
			return true;
		else
			return false;
	}
	
	// cek otorisasi unit self dan child utk SDM
	function rideUnitAkad($connakad,$target) {$connakad = Factory::getConnAkad();
		/*$cekfk = substr($target,0,1);
		
		if ($cekfk == 'f'){
			$idfk = substr($target,1,strlen($target)-1);
			$sql = "select 1 from fakultas where idfakultas = '$idfk'";
			$isride = $connakad->GetOne($sql);
		}*/
		
		if($target == '')
			return true;
		
		$sql = "select 1 from Fakultas where idFakultas = '$target'";
		$isride = $connakad->GetOne($sql);
		
		if($isride == 1)
			return true;
		else
			return false;
	}
	
	// tampilkan pesan error
	function pgError($conn,$tipe='simpan') {
		$errno = $conn->ErrorNo();
		$errmsg = $conn->ErrorMsg();
		
		if($fkey = strpos($errmsg,'DBSERROR:')) { // pesan tidak perlu diterjemahkan
			$start = $fkey + 9;
			$end = (strpos($errmsg,'CONTEXT')) - $start;
			$errview = trim(substr($errmsg,$start,$end));
			return UI::message("ERROR: ".$errview,true);
		}
		
		switch($errno) {
			case -1:
				if($tipe == "hapus")
					return UI::message("ERROR: Data masih dijadikan referensi.",true);
				else
					return UI::message("ERROR: Terjadi kesalahan pada referensi data.",true);
			case -5: return UI::message("ERROR: Terjadi duplikasi data.",true);
			case 0: 
				if($tipe == "simpan")
					return UI::message("Penyimpanan data berhasil.");
				else if($tipe == "hapus")
					return UI::message("Penghapusan data berhasil.");
				else
					return UI::message("Operasi data berhasil.");
			default: return UI::message("ERROR: Terjadi kesalahan pada operasi data.",true);
		}
	}
	
	// mendapatkan semester akademik dari periode yyyymm
	function getSemesterAkad($periode) {
		$thn = (int)substr($periode,0,4);
		$bln = (int)substr($periode,-2);
		
		if($bln <= 2 or $bln >= 9)
			$semester = $thn.'1';
		else
			$semester = ($thn-1).'2';
		
		return $semester;
	}
	
	// memformat kode kegiatan di penilaian
	function formatKodeKegiatan($str) {
		$str = ' '.$str; // padding untuk karakter pertama
		$a_str = str_split($str,2);
		$a_str[0] = $a_str[0][1]; // strip elemen pertama
		return implode('.',$a_str);
	}
	
	// membuat gradiasi warna
	function gradient() {
		$args = func_get_args();
		
		if(empty($args[0]))
			$scale = 5;
		else
			$scale = $args[0];
		
		$cols = array_slice($args,1);
		if(empty($cols)) {
			$cols = array();
			$cols[0] = '000000';
		}
		if(empty($cols[1])) {
			$cols[1] = 'ffffff';
		}
		
		$ncol = count($cols);
		
		// menghilangkan #
		for($i=0;$i<$ncol;$i++)
			if($cols[$i][0] == '#')
				$cols[$i] = substr($cols[$i],1);
		
		// menghitung nilai desimal dari heksadesimal
		for($i=0;$i<$ncol;$i++) {
			$r[$i] = hexdec(substr($cols[$i],0,2));
			$g[$i] = hexdec(substr($cols[$i],2,2));
			$b[$i] = hexdec(substr($cols[$i],4,2));
		}
		
		// menghitung variabel pelengkap
		if($scale <= $ncol) {
			$sint = 0;
		}
		else {
			$sint = ceil(($scale-$ncol)/($ncol-1));
			$sind = ($scale-$ncol)%($ncol-1);
			if($sind == 0)
				$sind = $ncol-1;
		}
		
		$j = 0; // untuk indeks array warna
		$k = ($sint > 0 ? 0 : -1); // untuk penanda saat init warna utk gradiasi
		$l = 1; // counter batas warna
		$retc = array();
		
		if($k == -1)
			// tanpa gradiasi karena $scale <= $ncol
			for($i=0;$i<$scale;$i++)
				$retc[$i] = $cols[$j++];
		else {
			// dengan gradiasi karena $scale > $ncol
			for($i=0;$i<$scale;$i++) {
				if($i == $k) {
					$nextc = $sint + ($l > $sind ? 0 : 1) + 1;
					$k += $nextc; // saat ganti batas warna
					$m = 0; // counter gradiasi utk setiap batas warna
					
					$col1 = $cols[$j];
					$col2 = $cols[$j+1];
					$r1 = $r[$j]; $g1 = $g[$j]; $b1 = $b[$j];
					$r2 = $r[$j+1]; $g2 = $g[$j+1]; $b2 = $b[$j+1];
					$j++;
					
					if($r1 > $r2) {
						$rd = $r1-$r2;
						$rs = -1;
					}
					else {
						$rd = $r2-$r1;
						$rs = 1;
					}
					
					if($g1 > $g2) {
						$gd = $g1-$g2;
						$gs = -1;
					}
					else {
						$gd = $g2-$g1;
						$gs = 1;
					}
					
					if($b1 > $b2) {
						$bd = $b1-$b2;
						$bs = -1;
					}
					else {
						$bd = $b2-$b1;
						$bs = 1;
					}
					
					$re = ($rd/$nextc)*$rs;
					$ge = ($gd/$nextc)*$gs;
					$be = ($bd/$nextc)*$bs;
					
					$l++;
				}
				
				$retc[$i] = '#'.dechex($r1+($m*$re)).dechex($g1+($m*$ge)).dechex($b1+($m*$be));
				$m++;
			}
		}
		
		return $retc;
	}
	
		
	/* FUNGSI ARRAY */
	
	function setTableArray(&$arr,$label,$tipe,$lebar=0) {
		$arr['label'] = $label;
		$arr['tipe'] = $tipe;
		$arr['lebar'] = $lebar;
	}
	
	// menjumlahkan elemen array
	function arrayAdd($src,$add) {
		if(!empty($add)) {
			foreach($add as $key => $val) {
				if(empty($src[$key]))
					$src[$key] = $add[$key];
				else if(is_array($add[$key]))
					$src[$key] = Helper::arrayAdd($src[$key],$add[$key]);
				else
					$src[$key] += $add[$key];
			}
		}
		return $src;
	}
	
	// jika $item kosong, return $sub
	function cEmChg($item,$sub) {
		if($item == '')
			return $sub;
		else
			return $item;
	}
	
	function getExtension($fileName) {
		$pos = strrchr($fileName, '.');

		if ($pos !== FALSE) {
			return strtolower(substr($pos, 0));
		} else {
			return '';
		}
	}
	
	//simpan 3 field pasti time,remote,ip
	
	function Identitas(&$record){
		$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
		$record['t_updatetime']=date('Y-m-d H:i:s');
		$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
	}
	
	function IdentitasBaca(&$record,$user){
		$record['t_user']=$user;
		$record['t_updatetime']=date('Y-m-d H:i:s');
		$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
	}
	
	
	# flash data untuk notifikasi setelah POST
	function setFlashData($name, $value) {
		$_SESSION['PERPUS_FLASHDATA'][$name] = $value;
	}
	
	function getFlashData($name) {
		if (isset($_SESSION['PERPUS_FLASHDATA'][$name]))
			return $_SESSION['PERPUS_FLASHDATA'][$name];
		return false;
	}

	function clearFlashData() {
		unset($_SESSION['PERPUS_FLASHDATA']);
	}
	
	function clearTrans() {
		unset($_SESSION['keyword']);

	}
	
	function tglEng($tglnya) { //format harus Y-m-d
		$bulan=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		
		$format=explode("-",$tglnya);
		$tgl=substr($format[2],0,2);
		$bln=$bulan[$format[1]-1];
		$thn=$format[0];
		
		$val=$tgl." ".$bln." ".$thn;	
		return $val;
	}
	
	function tglEngToInd($tglnya) { //format harus Y-m-d
		$bulan=array('jan'=>'01','feb'=>'02','mar'=>'03','apr'=>'04','may'=>'05','jun'=>'06','jul'=>'07','aug'=>'08','sep'=>'09','oct'=>10,'nov'=>11,'dec'=>12);
		
		$format=explode("-",$tglnya);
		$tgl=substr($format[2],0,2);
		$bln=$bulan[strtolower($format[1])];
		$thn=$format[0];
		
		$val=$thn."-".$bln."-".$tgl;	
		return $val;
	}
	
	function tglEngStamp($tglstamp) { //format default Y-m-d time
		$datetime=explode(" ",$tglstamp);
		$date=Helper::tglEng($datetime[0]);
		$time=$datetime[1];
		
		$val=$date." ".$time;
		return $val;
	
	}
	
	function tglEngTime($tglstamp) { //format default Y-m-d time
		$datetime=explode(" ",$tglstamp);
		$date=$datetime[0];
		$time=$datetime[1];
		
		$val=$date;
		return $val;
	
	}
	
	function JustTime($tglstamp){
	$datetime=explode(" ",$tglstamp);
	$time=$datetime[1];
	
	$val=$time;
	return $val;
	}
	
	function bulanInd($val){ //format angka : 8,9,10
	$bulan=array('januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
	
	$dadie=$bulan[$val-1];
	return $dadie;
	}
	
	function limitS($str,$i=55){
		$len=strlen($str);
		$string=substr($str,0,$i);
		if($len>$i)
			$string.=" - ";
		return $string;
	}
	
	function strPad($str,$val,$var,$c){
		if($c=='l')
			$string=str_pad($str,$val,$var,STR_PAD_LEFT);
		else if($c=='b')
			$string=str_pad($str,$val,$var,STR_PAD_BOTH);
		else
			$string=str_pad($str,$val,$var);
				
		return $string;
	}
	
	function getPass() {
    $length = 7;
    $characters = '01X234Y567Z89aAbBRcCdDeEfFghGiSjHklImQnoJpTqrVsUtKuvMwxNyWOzP';
    $pass ='';    

    for ($p = 0; $p < $length; $p++) {
        $pass .= $characters[mt_rand(0, strlen($characters))];
    }

    return $pass;
	}
	
	function bulanRomawi($date) { // format tanggal dd-mm-Y [ yg penting bulan ditengah
		$romawi=array ('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
		
		$slice=explode("-",$date);
		
		$toromawi=$romawi[$slice[1]-1];
		
		return $toromawi;
	
	}
	
	function getArrParam($arparam,$parkey){
		$paritem = array();
		$recdet = array();

		if(is_array($arparam)){
			foreach($arparam as $kparam => $vparam){
				if(!empty($_POST[$vparam]))
					$paritem[$vparam] = Helper::cStrFill($_POST[$vparam]);
			}
			if(count($paritem[$parkey])>0){
				foreach($paritem[$parkey] as $i => $val){
					foreach($paritem as $paramname => $paramval){
						$recdet[$i][$paramname] = $paramval[$i];
					}						
				}
			}
		}
		return $recdet;
	}
	function dateKurangi($dformat, $endDate, $beginDate)
	{
		$date_parts1=explode($dformat, date('Y-m-d',strtotime($beginDate)));
		$date_parts2=explode($dformat, date('Y-m-d',strtotime($endDate)));
		$start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
		$end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
		return $end_date - $start_date;
	}
	function arrStatusST() {
		return array('1'=>'Disetujui','0' =>'Ditolak');
	}
	
	function arrStatusP() {
		return array('A' => 'Diajukan','S' => 'Selesai', 'B' => 'Dibatalkan', 'PP' => 'Di Proses Perpustakaan', 'PK' => 'Di Proses Keuangan', 'V' => 'Verifikasi Supplier');
	}
	
	function getArrStatusP($value) {
		$vararr = array('A' => 'Diajukan','S' => 'Disetujui', 'B' => 'Dibatalkan', 'PP' => 'Di Proses Perpustakaan', 'PK' => 'Di Proses Keuangan', 'V' => 'Verifikasi Supplier');
		
		return $vararr[$value];
	}
	
	function strOnly($str){
		return str_replace(array('-', '.', '(', ')', '!', '@', '#', '$', '%', '^', '&', '*', '_', '=', '+',':','?'), '',$str);
	} 
	
	function RegNew($str){
		$nol = substr($str,0,1); //0010168
		if(strlen($str)==8 and $nol==0)
			$strnew = substr($str,1);
		else
			$strnew = $str;
			
		return $strnew;
	}
	
	function GetPath($str){
		$stre = explode("/",$str);
		
		$jstr = count($stre)-1;
		
		$sstr = $stre[$jstr];
		
		return $sstr;
	}
	
	function HapusFolder($foldere) {
	   if (is_dir($foldere))
			$dir_handle = opendir($foldere);
			if (!$dir_handle)
				return false;
				while($file = readdir($dir_handle)) {
					if ($file != "." && $file != "..") {
						if (!is_dir($foldere."/".$file))
						unlink($foldere."/".$file);
					else
						HapusFolder($foldere.'/'.$file);    
					}
				}
			closedir($dir_handle);
			rmdir($foldere);
			return true;
	}
	
	function AlfSpace($str,$space='2'){
		$var = strlen($str);
		$sp = '';
		
		for($u=0;$u<$space;$u++){
			$sp = $sp.'&nbsp;';
		}
		
		for($i=0;$i<$var;$i++){
			if($sstr=='')
				$sstr = substr($str,$i,1);
			else
				$sstr = $sstr.$sp.substr($str,$i,1);
		}
		
		return $sstr;
	}
	
	function periodeISO($tgl){
		global $conn;
		
		$percent = $conn->GetOne("select percent from lv_periodeiso where '$tgl' >= periodemulai order by periodemulai desc limit 1");
		
		if(!$percent)
			$percent = '90';
		
		return $percent;
	}
	
	function getAngka($huruf){
		$ha = array(
		  'a' => 1,
		  'b' => 2,
		  'c' => 3,
		  'd' => 4,
		  'e' => 5,
		  'f' => 6,
		  'g' => 7,
		  'h' => 8,
		  'i' => 9,
		  'j' => 10,
		  'k' => 11,
		  'l' => 12,
		  'm' => 13,
		  'n' => 14,
		  'o' => 15,
		  'p' => 16,
		  'q' => 17,
		  'r' => 18,
		  's' => 19,
		  't' => 20,
		  'u' => 21,
		  'v' => 22,
		  'w' => 23,
		  'x' => 24,
		  'y' => 25,
		  'z' => 26
		);
		
		return $ha[$huruf];
	}
	
	#4 SESSION
		// menyimpan request, untuk combo
		function setRequest($req,$key='') {
			if(isset($req)) {
				$req = Helper::removeSpecial($req);
				if(!empty($key))
					$_SESSION[SITE_ID]['VAR'][$key] = $req;
				
				return $req;
			}
			else if(!empty($key))
				return Helper::getRequest($key);
		}
		
		function getRequest($key) {
			return $_SESSION[SITE_ID]['VAR'][$key];
		}

		// nama halaman ini
		function thisPage($src=false) {
			$uri = $_SERVER['REQUEST_URI'];
			list(,$aftpage) = explode('page=',$uri);
			
			$famp = strpos($aftpage,'&');
			if($famp !== false and !$src)
				return substr($aftpage,0,$famp);
			else
				return $aftpage;
		}
		
		function refreshList() {
			unset($_SESSION[SITE_ID]['EX'][Helper::thisPage()]);
		}
		
		function plusNol($i){
			if($i<10){
				return "0".$i;
			}else
				return $i;
		}
		
		function xIPAddress() {
			if(array_key_exists('HTTP_X_FORWARDED_FOR',$_SERVER))
				return array_pop(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']));
			else
				return $_SERVER['REMOTE_ADDR'];
		}
		
		function unitpustaka($alias){
			if(empty($_SESSION['PERPUS_SATKER'])){
				$lokasi = " and 1=0 ";
			}elseif($_SESSION['PERPUS_SATKER'] != "KP"){
				$lokasi = " and $alias.kdlokasi = '".$_SESSION['PERPUS_SATKER']."' ";
			}
			
			return $lokasi;
		}
		
		function getMonthYear($start,$end){
			$date1=date_create($start);
			$date2=date_create($end);
			$diff=date_diff($date1,$date2);
			$h = intval(str_replace("+","",$diff->format("%R%a")));
			$bulan = array();
			for($i=0;$i<=$h;$i++){
				$date = new DateTime($start);
				$date->modify('+'.$i.' days');
				$date = $date->format('Y-m-d');
				$d = explode("-",$date);
				if($b == $d[1] and $t == $d[0]){
					continue;
				}else{
					$b = $d[1];$t = $d[0];
					$bulan[] = $d[1]."-".$d[0];
				}
			}
			
			return $bulan; 
		}

	// apakah null atau kosong
	function isYes($str) {
		if($str == '' or $str == 'null' or $str == '#N/A' or $str == 'N/A' or empty($str) or $str == '42')
			return false;
		else
			return true;
	}
	
	function getNamaUnit($conn, $idunit){
		return $conn->GetOne("select namasatker from ms_satker where kdsatker = '$idunit'");
	}
	
	function isAdminPusat(){
		$conn = Factory::getConn();
		//$unitTeratas = $conn->GetOne("select kdsatker from ms_satker where level = '1'");
		$unitTeratas = "P";
		if($unitTeratas == $_SESSION['PERPUS_SATKER'] and $_SESSION['PERPUS_NAMAROLE'] == "Administrator"){
			return true;
		}else{
			return false;
		}
	}
	
	function getUnits(){
		$conn = Factory::getConn();
		$idunit = $_SESSION['PERPUS_SATKER'];
		$info = $conn->GetRow("select info_lft, info_rgt from ms_satker where kdsatker = '$idunit' ");
		$left = $info['info_lft']; $right = $info['info_rgt'];

		$sql = $conn->Execute("select * from ms_satker where info_lft between $left and $right ");
		$units = array();
		while($row = $sql->FetchRow()){
			$units[] = $row['kdsatker'];
		}
		$jurusan = "'".implode("','",$units)."'";

		return $jurusan;
	}

}

?>
