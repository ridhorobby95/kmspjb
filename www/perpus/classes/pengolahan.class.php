<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	require_once('includes/sipus.class.php');
	
	class Pengolahan {
		
		function insertInventaris($conn,$record,$recdetail,$num,$au,$r_key,$kdklas,$kdperoleh,$tglperolehan,$noinduk) {
			$conn->BeginTrans();
			$recusul = array();
			Helper::Identitas($record);
			Helper::Identitas($recdetail);
			Helper::Identitas($recusul);
			$noseripus = Pengolahan::getSeri($conn,$record['kdjenispustaka']);
			$record['noseri'] = $noseripus;
			$record['idpustaka'] = Pengolahan::getIdPustaka($conn);
			$err = Query::recInsert($conn,$record,'ms_pustaka');
			$recdetail['idpustaka'] = $record['idpustaka'];
			$pengarang=$au;
			
			//insersi ke tabel pp_author
			Sipus::InsertRef($conn,pp_author,idauthor,idpustaka,$pengarang,$recdetail['idpustaka']);
			$recusul['idorderpustakattb'] = $r_key;
			for ($i=0;$i<$num;$i++){
				$iscount=str_pad($i+1,2,'0',STR_PAD_LEFT);
				$recdetail['noseri'] = $noseripus.'.'.$iscount;				
				if($i==0){
					$recdetail['kdklasifikasi'] = $kdklas[$i];
					$recdetail['kdperolehan'] = $kdperoleh[$i];
					$recdetail['tglperolehan'] = $tglperolehan[$i];
				}
				else{
					//cari ideksemplar lanjutannya jika sdh looping kedua, looping pertama ideksemplar dari data_inventaris.php
					$lastideks=$conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
					$ideks2 = $lastideks['maxid'] + 1;
					$recdetail['ideksemplar'] = $ideks2;
					
					//mendapatkan detail eksemplar 2 dst jika ada khususnya kdklasifikasi dan kdperolehan
					$recdetail['kdklasifikasi'] = $kdklas[$i];
					$recdetail['kdperolehan'] = $kdperoleh[$i];
					$recdetail['tglperolehan'] = $tglperolehan[$i];
				}
				
				//insersi ke eksemplar
				$err = Query::recInsert($conn,$recdetail,'pp_eksemplar');
				$sql_id = $conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
				$recusul['ideksemplar'] = $sql_id['maxid'];
				
				$sql_idolah = $conn->GetRow("select max(ideksemplarolah) as maxid from pp_eksemplarolah");
				$recusul['ideksemplarolah'] = $sql_idolah['maxid']+1;
				if ($err == 0)
					Query::recInsert($conn,$recusul,'pp_eksemplarolah');					
			}
			
			if($err == 0){
				$recttb = array();
				$recttb['idpustaka'] = $record['idpustaka'];
				$recttb['stsinventaris'] = 1;
				$recttb['npkinventaris'] = $_SESSION['PERPUS_USER'];
				$recttb['tglinventaris'] = date('Y-m-d H:m:s');
				Query::recUpdate($conn,$recttb,'pp_orderpustakattb',"idorderpustakattb = $r_key");
				$conn->CommitTrans();
			}else
				$conn->RollbackTrans();	
					
			return array($err,$recdetail['idpustaka']);
		}
		
		function updateInventaris($conn,$record,$recdetail,$num,$au,$r_key,$kdklas,$kdperoleh) {
			$conn->BeginTrans();
			
			$recusul = array();
			Helper::Identitas($record);
			Helper::Identitas($recdetail);
			Helper::Identitas($recusul);
			$noseripus = $conn->GetOne("select noseri from ms_pustaka where idpustaka=$record[idpustaka]");
			$record['noseri'] = $noseripus;
			$recdetail['idpustaka'] = $record['idpustaka'];
									
			$recusul['idorderpustakattb'] = $r_key;
			
			$countP=Sipus::GetCount($conn,$record['idpustaka']);
			for ($i=0;$i<$num;$i++){
				$iscount=str_pad($countP+$i,2,'0',STR_PAD_LEFT);
				$recdetail['noseri'] = $noseripus.'.'.$iscount;				
				if($i==0){
					$recdetail['kdklasifikasi'] = $kdklas[$i];
					$recdetail['kdperolehan'] = $kdperoleh[$i];
				}
				else{
					//cari ideksemplar lanjutannya jika sdh looping kedua, looping pertama ideksemplar dari data_inventaris.php
					$lastideks=$conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
					$ideks2 = $lastideks['maxid'] + 1;
					$recdetail['ideksemplar'] = $ideks2;
					
					//mendapatkan detail eksemplar 2 dst jika ada khususnya kdklasifikasi dan kdperolehan
					$recdetail['kdklasifikasi'] = $kdklas[$i];
					$recdetail['kdperolehan'] = $kdperoleh[$i];
				}
				
				//insersi ke eksemplar
				$err = Query::recInsert($conn,$recdetail,'pp_eksemplar'); //echo $err."<br/>";
				$sql_id = $conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
				$recusul['ideksemplar'] = $sql_id['maxid'];
				if ($err == 0)
					Query::recInsert($conn,$recusul,'pp_eksemplarolah');					
			}

			if($err == 0){
				$recttb = array();
				$recttb['idpustaka'] = $record['idpustaka'];
				$recttb['stsinventaris'] = 1;
				$recttb['npkinventaris'] = $_SESSION['PERPUS_USER'];
				$recttb['tglinventaris'] = date('Y-m-d H:m:s');
				Query::recUpdate($conn,$recttb,'pp_orderpustakattb',"idorderpustakattb = $r_key");
				$conn->CommitTrans();
			}else
				$conn->RollbackTrans();	
					
			return array($err,$recdetail['idpustaka']);
		}
		
		function &getIdPustaka($conn){
			$id = $conn->GetOne("select max(idpustaka) as maxid from ms_pustaka");
			return $id+1;
		}
				
		function &getSeri_old($conn, $kdjenispustaka){ 
			$getseri=$conn->GetRow("select max(noseri) as maxseri from ms_pustaka p where p.kdjenispustaka = '$kdjenispustaka' and p.noseri like '$kdjenispustaka%'");	
			$noseribaru = $kdjenispustaka.str_pad(str_replace($kdjenispustaka,'',$getseri['maxseri'])+1,5,'0',STR_PAD_LEFT);
	
			return $noseribaru;
		}
		
		function &getSeri($conn, $kdjenispustaka){ 
			$noseribaru = Sipus::GetSeri($conn, $kdjenispustaka);
	
			return $noseribaru;
		}
		
		function &createSeri($conn,$noseri,$r_id){
			$countP=Sipus::GetCount($conn,$r_id);
			$iscount=str_pad($countP,2,'0',STR_PAD_LEFT);
			$serie = $noseri.'.'.$iscount;
			
			return $serie;
		}
		
		function sentPengolahan($conn,$record,$key){
			$err= Query::recUpdate($conn,$record,'pp_eksemplarolah',"ideksemplarolah in ('$key')");
			
			return $err;			
		}
		
		function sentSirkulasi($conn,$record,$arrkey){
			$sql = "select ideksemplarolah,ideksemplar from pp_eksemplarolah where ideksemplarolah in ('$arrkey') and stspengolahan=1 order by ideksemplar";
			$rs = $conn->Execute($sql);
			$conn->StartTrans();
			while ($row = $rs->FetchRow()){
				Query::recUpdate($conn,$record,'pp_eksemplarolah',"ideksemplarolah=$row[ideksemplarolah]");
				$rec['kdkondisi']='V';
				$rec['statuseksemplar']='ADA';
				Query::recUpdate($conn,$rec,'pp_eksemplar',"ideksemplar=$row[ideksemplar]");
				
			}
			$conn->CompleteTrans();
			
			return $conn->ErrorNo();
		}
		
				
		//insersi ke history pengiriman untuk inventaris 
		function insHistory($conn,$record,$arrkey){
			$conn->BeginTrans();
						
			$record['tglhistory'] = date('Y-m-d H:m:s');
			$record['t_inserttime'] = date('Y-m-d H:m:s');
			$record['t_user'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['t_host'] = Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
			
			$sql = "select ideksemplar,idorderpustakattb from pp_eksemplarolah where ideksemplarolah in ('$arrkey') order by ideksemplar";
			$rs = $conn->Execute($sql); 
			
			while ($row = $rs->FetchRow()){
				$record['ideksemplar'] = $row['ideksemplar'];
				$record['idttb'] = $row['idorderpustakattb'];
				$err = Query::recInsert($conn,$record,'pp_historymaintaining');			
			}
			
			if($err == 0){
				$conn->CommitTrans();
			}else
				$conn->RollbackTrans();	
					
			return $err;
		}
		
		//insersi untuk pengiriman dari pengolahan ke lokasi
		function insSentLocation($conn,$record,$arrkey){
			$conn->BeginTrans();
			
			$record['tglhistory'] = date('Y-m-d H:m:s');
			$record['t_inserttime'] = date('Y-m-d H:m:s');
			$record['t_user'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['t_host'] = Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
			
			$sql = "select eo.ideksemplar,idorderpustakattb,eo.ideksemplarolah from pp_eksemplarolah eo 
					left join pp_eksemplar e on e.ideksemplar=eo.ideksemplar
					where e.noseri in ($arrkey) order by e.ideksemplar";
			$rs = $conn->Execute($sql); 
			$recupdate = array();
			$recupdate['tglkirim'] = date('Y-m-d H:m:s');
			$recupdate['npkkirim'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
			$recupdate['stskirimeksternal'] = 1;
			while ($row = $rs->FetchRow()){
				
				Sipus::UpdateComplete($conn,$recupdate,"pp_eksemplarolah"," ideksemplarolah=$row[ideksemplarolah]");
				
				$record['ideksemplar'] = $row['ideksemplar'];
				$record['idttb'] = $row['idorderpustakattb'];
				$err = Query::recInsert($conn,$record,'pp_historymaintaining');			
			}
			
			if($err == 0){
				$conn->CommitTrans();
			}else
				$conn->RollbackTrans();	
					
			return $err;
		}
		
		function getJenisPustakaByUnitJenjang($unit){
			$conna = Factory::getConnAkad();
			//$conna->debug = true;	
			$jenjang = $conna->GetOne("select kode_jenjang_studi from ak_prodi where kodeunit = '$unit' ");
			if($jenjang == "D3"){
				$kdjenispustaka = "KT"; #tugas akhir
			}elseif($jenjang == "D4" or $jenjang == "S1"){
				$kdjenispustaka = "SR"; #skripsi
			}elseif($jenjang == "S2"){
				$kdjenispustaka = "TS"; #tesis
			}elseif($jenjang == "S3"){
				$kdjenispustaka = "DS"; #disertasi
			}
			
			return $kdjenispustaka;
		}
		
		function getNamaPenerbitUnusa($conn,$idpenerbit){
			$namapenerbit = $conn->GetOne("select namapenerbit from ms_penerbit where idpenerbit = '$idpenerbit' ");
			return $namapenerbit;
		}
		
	}

?>