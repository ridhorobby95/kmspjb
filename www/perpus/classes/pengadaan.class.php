<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	
	class Pengadaan {
		
		function insertPengadaan($conn,$record,$recdetail) {
			$conn->BeginTrans();
		
			$err = Query::recInsert($conn,$record,'pp_pengadaan');
			$key = $conn->GetOne('select max(idpengadaan) from pp_pengadaan');
					
			if(count($recdetail) > 0){
				foreach($recdetail as $kdet => $vdet){
					$detail = $vdet;
					$detail['idpengadaan'] = $key;
					$err = Pengadaan::insertDetailPengadaan($conn,$detail,$detail['idorderpustaka']);
				}
			}
			if($err == 0)
				$conn->CommitTrans();
			else
				$conn->RollbackTrans();	
					
			return array($err,$key);
		}
		
		function updatePengadaan($conn,$record,$recdetail,$key) {
			$conn->BeginTrans();
			$err = Query::recUpdate($conn,$record,'pp_orderpustaka',"idpengadaan = '$key'");
			if(count($recdetail) > 0){
				foreach($recdetail as $kdet => $vdet){
					$detail = $vdet;
					$detail['idpengadaan'] = $key;
					$err = Pengadaan::insertDetailPengadaan($conn,$detail,$detail['idorderpustaka']);
				}
			}		
			if($err == 0)
				$conn->CommitTrans();
			else
				$conn->RollbackTrans();
				
			return array($err,$key);
		}
		
		function insertDetailPengadaan($conn,$record,$key) {
			return Query::recUpdate($conn,$record,'pp_orderpustaka',"idorderpustaka=$key");
		}
	}

?>