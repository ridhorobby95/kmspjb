<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	
	class Ajuanpengadaan {
		
		function insertPengadaan($conn,$record,$recdetail) {
			$conn->BeginTrans();
		
			$err = Query::recInsert($conn,$record,'pp_pengadaan');
			$key = $conn->GetOne('select max(idpengadaan) from pp_pengadaan');
					
			if(count($recdetail) > 0){
					$err = Ajuanpengadaan::insertDetailPengadaan($conn,$recdetail,$key);
			}
			if($err == 0)
				$conn->CommitTrans();
			else
				$conn->RollbackTrans();	
					
			return array($err,$key);
		}
		
		function updatePengadaan($conn,$record,$recdetail,$key) {
			$conn->BeginTrans();
			$err = Query::recUpdate($conn,$record,'pp_pengadaan',"idpengadaan = '$key'");
			if(count($recdetail) > 0){
					$err = Ajuanpengadaan::insertDetailPengadaan($conn,$recdetail,$key);
			}		
			if($err == 0)
				$conn->CommitTrans();
			else
				$conn->RollbackTrans();
				
			return array($err,$key);
		}
		
		function insertDetailPengadaan($conn,$record,$key) {
			return Query::recUpdate($conn,$record,'pp_orderpustaka',"idpengadaan=$key");
		}
	}

?>