<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	class FTS {
	
		function cariPustaka($r_keyword, $r_tipe='', $r_bahasa='', $r_tahun1='', $r_tahun2='') {
			$p_sqlstr = "select * from ms_pustaka p ";
			$r_word = explode(' ',$r_keyword);
			$word_key = '%';
			$sql_join_options = '';
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x].'%';
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'%';
					}else
						$word_key .= '%'.$r_word[$x];
				}
			}
		
			if ($r_tipe == 'T'){ //untuk kategori pencarian topik atau subjek
				$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
									left join lv_topik t on t.idtopik=pt.idtopik ";
				$keyvector = 't.namatopik';
			}
			else if ($r_tipe == 'J'){ //pencarian berdasarkan jenis pustaka
				$keyvector = 'jp.namajenispustaka';
			}
			else if ($r_tipe == 'P'){ //pencarian berdasarkan pengarang
				$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
									left join ms_author ma on ma.idauthor=pa.idauthor ";
				$keyvector = "ma.namadepan";
				$keyvector2 = " or upper(ma.namabelakang) like upper('$word_key')";
			}
			else if ($r_tipe == 'K'){ //pencarian berdasarkan keyword
				$keyvector = 'keywords';
			}
			else //default adalah pencarian berdasarkan judul 
				$keyvector = 'to_char(judul)';
			
			// $sql_where_options = "where to_tsvector($keyvector) @@ to_tsquery('$word_key')";
			if($keyvector2 == null)
				$sql_where_options = "where upper($keyvector) like upper('$word_key')";
			else
				$sql_where_options = "where upper($keyvector) like upper('$word_key')".$keyvector2;
				
			if (!empty($r_bahasa))
				$sql_where_options .= " and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
		function getIdPustaka($r_keyword, $r_tipe='') {
			$p_sqlstr = "select p.idpustaka from ms_pustaka p ";
			$r_word = explode(' ',$r_keyword);
			$word_key = '%';
			$sql_join_options = '';
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x].'%';
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'%';
					}else
						$word_key .= '%'.$r_word[$x];
				}
			}
						
			if ($r_tipe == 'T'){ //untuk kategori pencarian topik atau subjek
				$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
									left join lv_topik t on t.idtopik=pt.idtopik ";
				$keyvector = 't.namatopik';
			}
			else if ($r_tipe == 'J'){ //pencarian berdasarkan jenis pustaka
				$keyvector = 'jp.namajenispustaka';
			}
			else if ($r_tipe == 'P'){ //pencarian berdasarkan pengarang
				$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
									left join ms_author ma on ma.idauthor=pa.idauthor ";
				$keyvector = "ma.namadepan||' '||ma.namabelakang";
			}
			else if ($r_tipe == 'K'){ //pencarian berdasarkan keyword
				$keyvector = 'keywords';
			}
			else //default adalah pencarian berdasarkan judul 
				$keyvector = 'to_char(judul)';
			
			// $sql_where_options = "where to_tsvector($keyvector) @@ to_tsquery('$word_key')";
			$sql_where_options = "where upper($keyvector) like upper('$word_key')";
			
			if (!empty($r_bahasa))
				$sql_where_options .= " and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
	}
?>