<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	class Pdf {
		
		function ghostFile($file,$seri){
			/*
			require : ghostscript, imagemagick
			*/
		
			//list($nama,$ext) = explode('.',Helper::GetPath($file));
			list($nama,$ext) = explode('.',Helper::GetPath($file));
			
			$pathpdf = $file;

			$directori = 'ghostfile/'.$seri;

			//cek apakah directori sudah ada
			if (!is_dir($directori)){
				mkdir($directori,0777);
			}
			$directori = $directori.'/'.Helper::removeSpecial($nama);
			if(!is_dir($directori)){
				mkdir($directori,0777);
			}
			
			$pathoutput = $directori.'/'.Helper::removeSpecial($nama).'.jpg';
			
			exec("convert \"{$pathpdf}\" -colorspace RGB -geometry 1024 \"{$pathoutput}\"");
		}
	}

?>