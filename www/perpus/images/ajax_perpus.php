<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	define( '__PERPUS_ROW_LIMIT', 10 );
	
	//$conn->debug = false;
	
	error_reporting(0);

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng, 'home');

    if ($_GET['act']) {
		$act = Helper::removeSpecial($_GET['act']);
		
		switch ($act) {
			case 'aktivitas':
			case 'ttbinventaris':
			case 'bahasa':
			case 'pengarang':
			case 'supplier':
			case 'eksemplarolah':
			case 'usulan':
				$act($act);
				break;
			case 'order':
				$act($act);
				break;
			case 'newpengaranglov':
				_newPengarangLOV();
				break;
			case 'newsupplierlov':
				_newSupplierLOV();
				break;
			default:
				if (preg_match('/prev_/', $act) || preg_match('/next_/', $act) || preg_match('/reset_/', $act)) {
					_getNavigation($act);
				}
				else
					echo "Invalid Request";
		}
    }
    exit;

	function _getNavigation($act) {
		$prefix = explode('_', $act);
		$navigation = $prefix[0];
		$fungsi = $prefix[1];
		if ($prefix[0] === 'prev') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] - 10;
	
			if ($_SESSION["perpus_{$fungsi}_offset"] < 0)
				$_SESSION["perpus_{$fungsi}_offset"] = 0;			
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'next') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] + 10;
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'reset') {
			unset($_SESSION["perpus_{$fungsi}_offset"]);
			unset($_SESSION["perpus_{$fungsi}_keyword"]);
	
			return $fungsi($fungsi);
		}
	}

	function _retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}
	
	function bahasa($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from lv_bahasa ";
				
		if ($keyword != '') {
			$sql .= " where lower(kdbahasa) like '%{$keyword}%' or lower(namabahasa) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select * from lv_bahasa";
		if ($keyword != '') {
			$sql .= " where lower(kdbahasa) like '%{$keyword}%' or lower(namabahasa) like '%{$keyword}%' ";
		}
		$sql .=  " order by namabahasa ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>BAHASA</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['kdbahasa']}</td>";
					$ret .= "<td>{$row['namabahasa']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['kdbahasa']}','{$row['namabahasa']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function pengarang($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ms_author ";
				
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select * from ms_author";
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$sql .=  " order by namadepan,namabelakang ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>PENGARANG</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idauthor']}</td>";
					$ret .= "<td>{$row['namadepan']}&nbsp;{$row['namabelakang']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idauthor']}','".$row['namadepan'].' '.$row['namabelakang']."');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$extra_function = array('nama'=>'newPengarang', 'label'=>'Buat baru');
		
		$ret = _headerLOV($act, '', $extra_function) . $ret;
		
        echo $ret;
	}
	
	function supplier($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		list($id,$num) = explode('|',$_POST['add']);
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ms_supplier ";
				
		if ($keyword != '') {
			$sql .= " where lower(namasupplier) like '%{$keyword}%' or lower(alamat) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select * from ms_supplier";
		if ($keyword != '') {
			$sql .= " where lower(namasupplier) like '%{$keyword}%' or lower(alamat) like '%{$keyword}%' ";
		}
		$sql .=  " order by namasupplier ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>NAMA SUPPLIER</th><th>ALAMAT</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['kdsupplier']}</td>";
					$ret .= "<td>{$row['namasupplier']}</td>";
					$ret .= "<td>{$row['alamat']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['kdsupplier']}','{$row['namasupplier']}','{$id}','{$num}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$extra_function = array('nama'=>"newSupplier('$id','$num')", 'label'=>'Buat baru');
		
		$ret = _headerLOV($act, $_POST['add'] ,$extra_function) . $ret;
		
        echo $ret;
	}
	
   	function usulan($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from pp_orderpustaka o left join pp_usul u on u.idusulan=o.idusulan 
				where stsusulan='1' and idpengadaan is null";
				
		if ($keyword != '') {
			$sql .= " and lower(o.judul) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select * from pp_orderpustaka o left join pp_usul u on u.idusulan=o.idusulan 
				where stsusulan='1' and idpengadaan is null";
		if ($keyword != '') {
			$sql .= " and lower(o.judul) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$sql .=  " order by tglusulan ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">ID USULAN</th><th>TGL.USULAN</th><th>JUDUL</th><th>PENGARANG</th><th>PENGUSUL</th><th>HARGA</th><th>QTY</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idusulan']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglusulan'])."</td>";
					$ret .= "<td>{$row['judul']}</td>";
					$ret .= "<td>".$row['authorfirst1']." ".$row['authorlast1']."</td>";
					$ret .= "<td>{$row['namapengusul']}</td>";
					$ret .= "<td>{$row['hargausulan']}</td>";
					$ret .= "<td>{$row['qtyusulan']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idusulan']}','".Helper::formatDate($row['tglusulan'])."','{$row['judul']}','".$row['authorfirst1']." ".$row['authorlast1']."','{$row['namapengusul']}','{$row['hargausulan']}','{$row['qtyusulan']}','{$row['idorderpustaka']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function order($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from pp_orderpustaka o 
				join pp_usul u on o.idusulan=u.idusulan
				join pp_orderpustakattb tt on tt.idorderpustaka=o.idorderpustaka
				join pp_ttb tb on tt.idttb=tb.idttb
				where o.ststtb is null or o.ststtb=0 and o.qtypo<>o.qtyttb";
				
		if ($keyword != '') {
			$sql .= " and lower(o.judul) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select o.idorderpustaka,o.judul,u.tglusulan,u.namapengusul,o.namasupplier1,o.namasupplier2,o.namasupplier3,o.supplierdipilih,s.namasupplier,po.nopo,sum(o.qtypo-o.qtyttb) as qty from pp_orderpustaka o 
				join pp_usul u on o.idusulan=u.idusulan
				left join pp_po po on o.idpo=po.idpo
				left join ms_supplier s on o.supplierdipilih=s.kdsupplier
				where o.ststtb=0 and o.stspo=1 and o.qtypo<>o.qtyttb";
		if ($keyword != '') {
			$sql .= " and lower(o.judul) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$sql .=  " group by o.idorderpustaka,o.judul,u.tglusulan,u.namapengusul,o.namasupplier1,o.namasupplier2,o.namasupplier3,o.supplierdipilih,s.namasupplier,po.nopo order by o.idorderpustaka ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">ID ORDER</th><th>NO PO</th><th>TGL.USULAN</th><th>JUDUL</th><th>PENGUSUL</th><th>SUPPLIER</th><th>QTY</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				//$supp = $row['supplierdipilih'];
				if ($ret_function) {
					$ret .= "<td align='center'>{$row['idorderpustaka']}</td>";
					$ret .= "<td>{$row['nopo']}</td>";					
					$ret .= "<td>".Helper::formatDate($row['tglusulan'])."</td>";
					$ret .= "<td>{$row['judul']}</td>";
					$ret .= "<td>{$row['namapengusul']}</td>";
					$ret .= "<td>{$row['namasupplier']}</td>";
					$ret .= "<td>{$row['qty']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idorderpustaka']}','{$row['nopo']}','".Helper::formatDate($row['tglusulan'])."','{$row['judul']}','".trim($row['namapengusul'])."','{$row['namasupplier']}','{$row['qty']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"7\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function eksemplarolah($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
		
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		$arrkey = $_POST['add'];
		
		$arrplo = explode(',',$arrkey);
		
		$arrkey = implode("','",$arrplo);
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from pp_eksemplarolah p 
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
				left join ms_pustaka m on m.idpustaka=e.idpustaka 
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
				where stskirimpengolahan=1 and stskirimeksternal=0 
				and m.kdjenispustaka in ($_SESSION[roleakses]) and p.stspengolahan=1";
				
		if ($keyword != '') {
			$sql .= " and lower(m.judul) like '%{$keyword}%' or lower(e.noseri) like '%{$keyword}%' ";
		}
		
		if ($arrkey != '')
			$sql .= " and e.noseri not in ('$arrkey')";
		
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select *, e.noseri as seri from pp_eksemplarolah p 
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
				left join ms_pustaka m on m.idpustaka=e.idpustaka 
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
				where stskirimpengolahan=1 and stskirimeksternal=0 
				and m.kdjenispustaka in ($_SESSION[roleakses]) and p.stspengolahan=1 ";
		if ($keyword != '') {
			$sql .= " and lower(m.judul) like '%{$keyword}%' or lower(e.noseri) like '%{$keyword}%' ";
		}
				
		if ($arrkey != '')
			$sql .= " and e.noseri not in ('$arrkey')";
		
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">REG COMP</th><th>JUDUL</th><th>PENGARANG</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				//$supp = $row['supplierdipilih'];
				if ($ret_function) {
					$ret .= "<td align='center'>{$row['seri']}</td>";
					$ret .= "<td>{$row['judul']}</td>";
					$ret .= "<td>{$row['authorfirst1']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['seri']}','{$row['judul']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"4\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function aktivitas($act){
		$fungsi = $act;
        $connf = Factory::getConnFin();
		
		$nowday = date('Ym');
		$r_thang = $connf->GetOne("select thang from ms_thnang where periodeawal <= '$nowday' and periodeakhir >= '$nowday'");
		
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ke_anggarandetail s
				right join ke_subanggaran g on g.kodeunit = s.kodeunit and g.thang = s.thang and 
				g.kodeaktivitas = s.kodeaktivitas and g.nosubanggaran = s.nosubanggaran
				right join ke_anggaran a on a.kodeunit = s.kodeunit and a.thang = s.thang and a.kodeaktivitas = s.kodeaktivitas
				where s.thang = '$r_thang' and s.statusanggaran = 'S'";
		/*if ($unit != '')
			$sql .= " and s.kodeunit = '$unit'";*/
		if ($keyword != '') {
			$sql .= " and lower(a.kodeaktivitas) like '%{$keyword}%' or lower(a.namaaktivitas) like '%{$keyword}%' ";
		}
		$tot = $connf->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select a.kodeaktivitas, g.nosubanggaran, s.nosubaktivitas, a.namaaktivitas, 
				g.namasubanggaran, s.namasubaktivitas, s.kodeanggaran
				from ke_anggarandetail s
				right join ke_subanggaran g on g.kodeunit = s.kodeunit and g.thang = s.thang and 
				g.kodeaktivitas = s.kodeaktivitas and g.nosubanggaran = s.nosubanggaran
				right join ke_anggaran a on a.kodeunit = s.kodeunit and a.thang = s.thang and a.kodeaktivitas = s.kodeaktivitas
				where s.thang = '$r_thang' and s.statusanggaran = 'S'";
		/*if ($unit != '')
			$sql .= " and s.kodeunit = '$unit'";*/
			
		if ($keyword != '') {
			$sql .= " and lower(a.kodeaktivitas) like '%{$keyword}%' or lower(a.namaaktivitas) like '%{$keyword}%'";
		}
		$sql .=  " order by a.kodeaktivitas, s.nosubaktivitas";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th>AKTIVITAS</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $connf->Execute($sql);
		while($rows = $rs->FetchRow()) {
			if($t_kodeaktivitas != $rows['kodeaktivitas']) {
				$a_temp['kode'] = $rows['kodeaktivitas'];
				$a_temp['nama'] = $rows['namaaktivitas'];
				$a_temp['level'] = 1;
		
				if($rows['nosubanggaran'] != '')
					$a_temp['haschild'] = 1;
				else
					$a_temp['haschild'] = 0;
		
				$a_sub[] = $a_temp;
		
				$t_kodeaktivitas = $rows['kodeaktivitas'];
			}
		
			if($t_nosubanggaran != $rows['nosubanggaran']) {
				$a_temp['kode'] = $rows['nosubanggaran'];
				$a_temp['nama'] = $rows['namasubanggaran'];
				$a_temp['level'] = 2;
		
				if($rows['nosubaktivitas'] != '')
					$a_temp['haschild'] = 1;
				else
					$a_temp['haschild'] = 0;
		
				$a_sub[] = $a_temp;
		
				$t_nosubanggaran = $rows['nosubanggaran'];
			}
			if($rows['nosubaktivitas'] != '') {
				$a_temp['kode'] = $rows['kodeaktivitas'].'.'.$rows['nosubaktivitas'];
				$a_temp['nama'] = $rows['kodeanggaran'].' - '.$rows['namasubaktivitas'];
				$a_temp['haschild'] = 0;
				$a_temp['level'] = 3;
		
				$a_sub[] = $a_temp;
			}
		}	
		
		$i = 0;
		foreach($a_sub as $k=>$v){
			$i++;
			$class = ($i%2===1)?"class=\"odd\"":"";
			$ret .= "<tr {$class}>";
			if ($ret_function) {
				$ret .= "<td>".str_repeat('..',$v['level']-1).$v['nama']."</td>";
				$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$v['kode']}','{$r_thang}');closePop();resetLOV('$act');\"></td>";
			}
			$ret .= "</tr>";
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function ttbinventaris($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from pp_orderpustakattb o left join pp_ttb t on t.idttb=o.idttb 
				where stsinventaris=0 and stspengolahan <> 1";
				
		if ($keyword != '') {
			$sql .= " and lower(nottb) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "select * from pp_orderpustakattb o left join pp_ttb t on t.idttb=o.idttb 
				where stsinventaris=0 and stspengolahan <> 1";
				
		if ($keyword != '') {
			$sql .= " and lower(nottb) like '%{$keyword}%' ";
		}
		$sql .=  " order by tglttb ";
		$sql .=  " limit " . __PERPUS_ROW_LIMIT . " offset {$offset}";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">No TTP</th><th>TGL.TTP</th><th>JUMLAH</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['nottb']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglttb'])."</td>";
					$ret .= "<td>{$row['qtyttbdetail']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['nottb']}','{$row['idorderpustaka']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"4\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}

	function _headerLOV($act, $add, $extra_function) {
		$keyword = $_SESSION["perpus_{$act}_keyword"];
		
		$ret = "<table class=\"popWindowtr\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".
		$ret .= "<form style=\"float:left\">&nbsp;<input type=\"text\" id=\"sevimalov_keyword\" name=\"sevimalov_keyword\" value=\"$keyword\" size=\"25\" />
			<input type=\"submit\" style=\"cursor:pointer\" onclick=\"searchLOV('$act','$add');return false;\" value=\"Cari\" />
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('reset_'+'$act','$add')\" title=\"Reset\" value=\"Reset\"> &nbsp; 
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('prev_'+'$act','$add')\" title=\"Previous\" class=\"btn_prev\">
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('next_'+'$act','$add')\" title=\"Next\" class=\"btn_next\">";
		if ($extra_function) {
			$ret .= " &nbsp; <input type=\"button\" id=\"btn_{$extra_function['nama']}\" onclick=\"{$extra_function['nama']}()\" class=\"ControlStyle\" title=\"{$extra_function['label']}\" value=\"{$extra_function['label']}\">";
		}
		$ret .=	"</form>";
		$ret .= "<span style=\"float:right\">".
				"<input type=\"button\" class=\"btn_close\" style=\"cursor:pointer\" onclick=\"closePop()\"/>".
				"</span>";
		$ret .= "</td></tr></table>";
		return $ret;
	}

	function _newPengarangLOV() {
		global $conn;
		
		$record = array();
		$record['namadepan'] = pg_escape_string($_POST['namadepan']);
		$record['namabelakang'] = pg_escape_string($_POST['namabelakang']);
		Helper::Identitas($record);
		
		$col = $conn->Execute("select * from ms_author where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
	function _newSupplierLOV() {
		global $conn;
		
		$record = array();
		$record['kdsupplier'] = $conn->GetOne("select max(kdsupplier) from ms_supplier")+1;
		$record['namasupplier'] = pg_escape_string($_POST['namasupplier']);
		$record['alamat'] = pg_escape_string($_POST['alamat']);
		Helper::Identitas($record);
		
		$col = $conn->Execute("select * from ms_supplier where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
	
?>