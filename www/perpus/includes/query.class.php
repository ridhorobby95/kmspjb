<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

// Class untuk kueri umum
class Query {
	// Insert dari data array
	function recInsert($conn,$record,$table) {
		$col = $conn->Execute("select * from $table where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record,true);
		$conn->Execute($sql);
		
		return $conn->ErrorNo();
	}
	
	// Update dari data array
	function recUpdate($conn,$record,$table,$condition) {
		$col = $conn->Execute("select * from $table where $condition");
		$sql = $conn->GetUpdateSQL($col,$record,false,true);

		if($sql != '')
			$conn->Execute($sql);
		
		return $conn->ErrorNo();
	}
	
	// Insert/Update/Delete dari data array
	function recSave($conn,$record,$table,$condition,$chkcol='') {
		$col = $conn->Execute("select * from $table where $condition");
		
		// diproses bila tidak ada kolom cek atau ada kolom cek tetapi isinya kosong
		if($chkcol == '' or $col->fields[$chkcol] == '') {
			if($col->EOF)
				$sql = $conn->GetInsertSQL($col,$record,true);
			else
				$sql = $conn->GetUpdateSQL($col,$record,false,true);
			
			if($sql != '')
				$conn->Execute($sql);
			
			return $conn->ErrorNo();
		}
		else
			return 0; // kolom cek sudah ada isinya, dilewati, asumsinya tidak error
	}
	
	// Delete secara umum
	function qDelete($conn,$table,$condition) {
		$sql = "delete from $table where $condition";
		$conn->Execute($sql);
		
		return $conn->ErrorNo();
	}
	
	// Ambil nilai terakhir dari sequence
	function getLastValue($conn,$seq) {
		return $conn->GetOne("select last_value from $seq");
	}

	function getCount($conn, $field, $table){
		return $conn->GetOne("select max({$field}) as max from $table");
	}
}

?>