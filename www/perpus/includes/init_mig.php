<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	session_start();
	
	// gunakan buffer agar kalau ada tulisan masih bisa redirect
	ob_start();
	
	require_once('config.class.php');
	require_once('factory.class.php');
	require_once('ui.class.php');
	require_once('helper.class.php');
	require_once('sipus.class.php');
	require_once('query.class.php');


	Helper::normalizeRequest();
	$cono = Factory::getConnPerpusY();
	
	//$conn->debug = true;
	
	if(substr($_SERVER['REMOTE_ADDR'],-3) == ".10" or substr($_SERVER['REMOTE_ADDR'],-3) == ".53" or substr($_SERVER['REMOTE_ADDR'],-3) == ".23" or substr($_SERVER['REMOTE_ADDR'],-3) == ".69" or substr($_SERVER['REMOTE_ADDR'],-3) == "111"){		
		$conn->debug = true;
	}
	

?>