<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

class UI {
	
	function createTextArea($nameid,$value='',$class='',$rows='',$cols='',$edit=true,$add='') {
		if(!empty($edit)) {
			$ta = '<textarea wrap="soft" name="'.$nameid.'" id="'.$nameid.'"';
			if($class != '') $ta .= ' class="'.$class.'"';
			if($rows != '') $ta .= ' rows="'.$rows.'"';
			if($cols != '') $ta .= ' cols="'.$cols.'"';
			if($add != '') $ta .= ' '.$add;
			$ta .= '>';
			if($value != '') $ta .= $value;
			$ta .= '</textarea>';
		}
		/* else if($value == '')
			$ta = 'N/A'; */
		else
			$ta = $value;
		
		return $ta;
	}
	
	// membuat textbox
	function createTextBox($nameid,$value='',$class='',$maxlength='',$size='',$edit=true,$add='') {
		if(!empty($edit)) {
			$tb = '<input type="text" name="'.$nameid.'" id="'.$nameid.'"';
			if($value != '') $tb .= ' value="'.$value.'"';
			if($class != '') $tb .= ' class="'.$class.'"';
			if($maxlength != '') $tb .= ' maxlength="'.$maxlength.'"';
			if($size != '') $tb .= ' size="'.$size.'"';
			if($add != '') $tb .= ' '.$add;
			$tb .= '>';
		}
		/* else if($value == '')
			$tb = 'N/A'; */
		else
			$tb = $value;
		
		return $tb;
	}
	
	// membuat combo box
	function createSelect($nameid,$arrval='',$value='',$class='',$edit=true,$add='') {
		if(!empty($edit)) {
			$slc = '<select name="'.$nameid.'" id="'.$nameid.'"';
			if($class != '') $slc .= ' class="'.$class.'"';
			if($add != '') $slc .= ' '.$add;
			$slc .= ">\n";
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					$slc .= '<option value="'.$key.'"'.(!strcasecmp($value,$key) ? ' selected' : '').'>';
					$slc .= $val.'</option>'."\n";
				}
			}
			$slc .= '</select>';
		}
		else {
			if (is_array($arrval)) {
				foreach ($arrval as $key => $val) {
					if (!strcasecmp($value,$key)) {
						$slc = $val;
						break;
					}
				}
			}
			/* if(!isset($slc))
				$slc = 'N/A'; */
		}
		
		return $slc;
	}

	// membuat radio button
	function createRadio($nameid,$arrval='',$value='',$edit=true,$br=false,$add='') {
		$radio = '';
		if(!empty($edit)) {
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					$radio .= '<input type="radio" name="'.$nameid.'" id="'.$nameid.'_'.$key.'" value="'.$key.'"'.(!strcasecmp($value,$key) ? ' checked' : '').' '.$add.'>';
					$radio .= '<label for="'.$name.'_'.$key.'">'.$val.'</label>'.($br ? '<br>' : '')."\n";
				}
			}
		}
		else {
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					if(!strcasecmp($value,$key)) {
						$radio = $val;
						break;
					}
				}
			}
		}
		
		return $radio;
	}
	
	// membuat div tree untuk popup
	function divPopUp($treeid,$treename,$arrdata,$idxid,$idxkode,$idxnama,$idxkodealt='') {
		$tree = '<ul id="'.$treeid.'"><li><span style="font-weight:bold;">'.$treename.'</span><ul>';
		$n = count($arrdata);
		for($i=0;$i<$n;$i++) {
			$row = $arrdata[$i];
			$tree .= '<li>';
			
			if($row['haschild'] == 1)
				$tree .= strtoupper(formatKodeKegiatan($row[$idxkode]).(($idxkodealt != '' and $row[$idxkodealt] != '') ? ' ('.$row[$idxkodealt].')' : '').' - '.$row[$idxnama]);
			else
				$tree .= '<a id="menulink" name="'.$row[$idxid].'">'.strtoupper(formatKodeKegiatan($row[$idxkode]).(($idxkodealt != '' and $row[$idxkodealt] != '') ? ' ('.$row[$idxkodealt].')' : '').' - '.$row[$idxnama]).'</a>';
			
			if($i == ($n-1)) {
				$tree .= str_repeat('</ul></li>',$row['level']);
			}
			else {
				$t_selisih = $row['level'] - $arrdata[$i+1]['level'];
				
				if($t_selisih >= 0)
					$tree .= '</li>';
				else if($t_selisih < 0)
					$tree .= '<ul>';
				
				if($t_selisih > 0)
					$tree .= str_repeat('</ul></li>',$t_selisih);
			}
		}
		$tree .= '</ul></li></ul>';
		
		return $tree;
	}
	
	// membuat foto
	function createFoto($src,$dest,$xw=0,$xh=0) {
		if(($rsize = getimagesize($src)) === false)
			return -1; // bukan image
		$rw = $rsize[0]; $rh = $rsize[1];
		
		if($rw > $rh or ($rw == $rh and $xw < $xh)) { // lebih kecil max width atau sama
			$nw = $xw;
			$nh = round(($nw*$rh)/$rw);
		}
		else if($rw < $rh or ($rw == $rh and $xw > $xh)) { // lebih kecil max height atau sama
			$nh = $xh;
			$nw = round(($nh*$rw)/$rh);
		}
		else { // semua parameter max ukuran 0, disamakan
			$nw = $xw;
			$nh = $xh;
		}
		
		switch($rsize[2]) {
			case IMAGETYPE_GIF: $rimg= imagecreatefromgif($src); break;
			case IMAGETYPE_JPEG: $rimg= imagecreatefromjpeg($src); break;
			case IMAGETYPE_PNG: $rimg= imagecreatefrompng($src); break;
			default: return -2; // format image tidak dikenali
		}
		$nimg= imagecreatetruecolor($nw,$nh);
		
		imagecopyresized($nimg, $rimg, 0, 0, 0, 0, $nw, $nh, $rw, $rh);
		$return= imagejpeg($nimg,$dest);
		imagedestroy($rimg);
		imagedestroy($nimg);
		
		if($return === true)
			return 1;
		else
			return -3; // tidak bisa menulis image tujuan
	}
	
	// membandingkan menu session dan menu item
	function inMenu($item) {
		$sess = $_SESSION['PERPUS_MENU'];
		
		$return = array();
		for($i=0;$i<count($item);$i++) {
			if($sess[$item[$i]] === true)
				$return[$i] = true;
		}
		
		if(empty($return))
			return false;
		else
			return $return;
	}
	
	// tampilkan pesan (misalnya error)
	function message($str,$error=false) {
		if($error)
			return '<strong><font color="#FF0000">'.$str.'</font></strong>';
		else
			return '<strong><font color="#00AA00">'.$str.'</font></strong>';
	}
	
	// ber di dalam tabel dengan tr
	function trbr() {
		return '<tr><td height="1">&nbsp;</td></tr>';
	}
	
	// membuat gradiasi warna
	function gradient() {
		$args = func_get_args();
		
		if(empty($args[0]))
			$scale = 5;
		else
			$scale = $args[0];
		
		$cols = array_slice($args,1);
		if(empty($cols)) {
			$cols = array();
			$cols[0] = '000000';
		}
		if(empty($cols[1])) {
			$cols[1] = 'ffffff';
		}
		
		$ncol = count($cols);
		
		// menghilangkan #
		for($i=0;$i<$ncol;$i++)
			if($cols[$i][0] == '#')
				$cols[$i] = substr($cols[$i],1);
		
		// menghitung nilai desimal dari heksadesimal
		for($i=0;$i<$ncol;$i++) {
			$r[$i] = hexdec(substr($cols[$i],0,2));
			$g[$i] = hexdec(substr($cols[$i],2,2));
			$b[$i] = hexdec(substr($cols[$i],4,2));
		}
		
		// menghitung variabel pelengkap
		if($scale <= $ncol) {
			$sint = 0;
		}
		else {
			$sint = floor(($scale-$ncol)/($ncol-1));
			$sind = ($scale-$ncol)%($ncol-1);
			if($sind == 0)
				$sind = $ncol-1;
		}
		
		$j = 0; // untuk indeks array warna
		$k = ($sint > 0 ? 0 : -1); // untuk penanda saat init warna utk gradiasi
		$l = 1; // counter batas warna
		$retc = array();
		
		if($k == -1)
			// tanpa gradiasi karena $scale <= $ncol
			for($i=0;$i<$scale;$i++)
				$retc[$i] = $cols[$j++];
		else {
			// dengan gradiasi karena $scale > $ncol
			for($i=0;$i<$scale;$i++) {
				if($i == $k) {
					$nextc = $sint + ($l > $sind ? 0 : 1) + 1;
					$k += $nextc; // saat ganti batas warna
					$m = 0; // counter gradiasi utk setiap batas warna
					
					$col1 = $cols[$j];
					$col2 = $cols[$j+1];
					$r1 = $r[$j]; $g1 = $g[$j]; $b1 = $b[$j];
					$r2 = $r[$j+1]; $g2 = $g[$j+1]; $b2 = $b[$j+1];
					$j++;
					
					if($r1 > $r2) {
						$rd = $r1-$r2;
						$rs = -1;
					}
					else {
						$rd = $r2-$r1;
						$rs = 1;
					}
					
					if($g1 > $g2) {
						$gd = $g1-$g2;
						$gs = -1;
					}
					else {
						$gd = $g2-$g1;
						$gs = 1;
					}
					
					if($b1 > $b2) {
						$bd = $b1-$b2;
						$bs = -1;
					}
					else {
						$bd = $b2-$b1;
						$bs = 1;
					}
					
					$re = ($rd/$nextc)*$rs;
					$ge = ($gd/$nextc)*$gs;
					$be = ($bd/$nextc)*$bs;
					
					$l++;
				}
				
				$retc[$i] = '#'.dechex($r1+($m*$re)).dechex($g1+($m*$ge)).dechex($b1+($m*$be));
				$m++;
			}
		}
		
		return $retc;
	}
	
	function combotahun($name,$size,$sett) {
		echo "<select name='".$name."' id='".$name."' style='width:$size' class='ControlStyle'>";
		$t=2000; $x=2000;
		while($t<2020) {
		$x=$t+1;
		$s='false';
		if ($x==$sett)
			echo "<option value=".$x." selected>".$t."-".$x."</option>";	
		else
		echo "<option value=".$x." >".$t."-".$x."</option>";	
		$t++; }
		echo "</select>";
	}
	
	function combobulan($name,$size) {
		echo "<select name='".$name."' id='".$name."' style='width:$size' class='ControlStyle'>";
		echo "<option value='1'>Januari</option>";	
		echo "<option value='2'>Februari</option>";	
		echo "<option value='3'>maret</option>";	
		echo "<option value='4'>April</option>";
		echo "<option value='5'>Mei</option>";	
		echo "<option value='6'>Juni</option>";	
		echo "<option value='7'>Juli</option>";	
		echo "<option value='8'>Agustus</option>";	
		echo "<option value='9'>September</option>";	
		echo "<option value='10'>Oktober</option>";	
		echo "<option value='11'>November</option>";	
		echo "<option value='12'>Desember</option>";	
		echo "</select>";
	}
	
	function CreateCombo($conn,$name,$koneksi,$val,$value,$default){
		$str=$conn->Execute($koneksi);
		echo "<select name='".$name."' id='".$name."' class='ControlStyle' style='width:120'>";
		while ($row=$str->FetchRow()){
		if($row[$val]==$default)
		echo "<option value=".$row[$val]." selected>".$row[$value]."</option>";
		else
		echo "<option value='".$row['kdkondisi']."'>".$row['namakondisi']."</option>";
		}
		echo "</select>";

	}
	

}

?>