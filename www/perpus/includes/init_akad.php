<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//session_start();
	
	// gunakan buffer agar kalau ada tulisan masih bisa redirect
	//ob_start();
	
	require_once('config.class.php');
	require_once('factory.class.php');
	require_once('ui.class.php');
	require_once('helper.class.php');
	
	Helper::normalizeRequest();
	
	$connakad = Factory::getConnAkad();
	$connakad->debug = true;
	if(substr($_SERVER['REMOTE_ADDR'],-3) == ".43"/* or substr($_SERVER['REMOTE_ADDR'],-3) == ".10"*/){
		$connakad->debug = false;
	}
?>