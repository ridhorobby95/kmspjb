<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

class Config {
	// Koneksi ke Server Perpustakaan
	const server = 'localhost';//'172.16.30.215';
	const driver = 'oci8po';
	const database = '';
	const schema = 'perpus';
	const user = 'perpus';
	const password = 'perpus';

	const serverExcel = 'localhost';
	const driverExcel = 'oci8po';
	const databaseExcel = '';
	const schemaExcel = 'perpus';
	const userExcel = 'perpus';
	const passwordExcel = 'perpus';

	// Koneksi ke Server Perpustakaan
	const serverLamp = 'localhost';
	const driverLamp = 'oci8po';
	const databaseLamp = '';
	const schemaLamp = 'lp';
	const userLamp = 'lp';
	const passwordLamp = 'lp';

	const serverGate = 'localhost';//'172.16.30.215';
	const driverGate = 'oci8po';
	const databaseGate = '';
	const schemaGate = 'um';
	const userGate = 'um';
	const passwordGate = 'pjb123';

	const acceptUnit = 'KP_##_KO_##_JP';

	const pageTitle = "Sistem Perpustakaan PJB";
	const webUrl = "index.php";
	//const webUrl = "http://http://172.16.30.36//perpus/index.php";
	const gateUrl = "../";
	const logoutUrl = "../index.php/logout";
	const pagePath = "html";
	const pageDef = "home";
	const imgUrl = "images/";
	const fotoUrl = "images/perpustakaan/";
	const dirFoto = "images/perpustakaan/";
	const dirTAMandiri = "D:/abu/git/pjb/www/digilib/upl_link/uploadtamandiri";

	const taMandiriUrl = "../digilib/upl_link/uploadtamandiri";
	const dirDigi = "";
	const pageErr = "error404";
	const pathSeparator = "/";
	const dirFotoMhs = "D:/abu/git/pjb/www/siakad/siakad/uploads/fotomhs/";

	const fotoMhsUrl = "../siakad/siakad/uploads/fotomhs/";
	const dirFotoPeg = "D:/abu/git/pjb/www/sdm/sdm/up_l04ds/fotopeg/";

	const fotoPegUrl = "../sdm/sdm/up_l04ds/fotopeg/";
	const rpcsalt = "5mE0t6wrLCndHEOu5bQ1jJJ332PyJ2B9"; //setiap client harus menyamakan nilainya dengan nilai di server ini
	const setAuto = '2000';

	#email
	const email = "lib@pt.PJBervices.com";

	const SMTPHost = 'ssl://smtp.gmail.com';
	const SMTPUser = 'lib@PJBervices.com';
	const SMTPPass = 'PJB1414';
	const SMTPPort = '465';

	const AdminEmail = 'lib@pt.PJBervices.com';
	const AdminName = "Admin Perpustakaan-PJB";

	const G_SESSION = 'PJB';
	const kmUrl = 'http://localhost/git/pjb/www/index.php/home';
	const umUrl = 'http://localhost/git/pjb/www/index.php/publ1c/';
	const NilaiPerpus = "D:/abu/git/pjb/www/_static/nilaiperpus.txt";
	const digilib_url = "http://localhost/git/pjb/www/digilib/index.php?page=functionx&s=logbykm";

	const pictProf = "http://localhost/git/pjb/www/index.php/publ1c/profile?q=";
}

?>
