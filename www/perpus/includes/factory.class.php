<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

require_once('config.class.php');

class Factory
{
	// Koneksi database utama
	function &getConn() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConn();

		return $instance;
	}
	
	function &getConnLamp() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConnLamp();

		return $instance;
	}
	
	function &getConnGate() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConnGate();

		return $instance;
	}

	function &createConn() {
		define('ADODB_ASSOC_CASE', 0);
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driver);

		if (!$conn->Connect(Config::server,Config::user,Config::password,Config::database))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database " . $dest . "</p>");
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql =  $conn->Execute("SET search_path TO 'public' ");		
			
		return $conn;
	}
	
	function &createConnLamp() {
		define('ADODB_ASSOC_CASE', 0);
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driverLamp);

		if (!$conn->Connect(Config::serverLamp,Config::userLamp,Config::passwordLamp,Config::databaseLamp))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database " . $dest . "</p>");
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql =  $conn->Execute("SET search_path TO 'public' ");		
			
		return $conn;
	}
		
	function &createConnGate() {
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driverGate);

		if (!$conn->Connect(Config::serverGate,Config::userGate,Config::passwordGate,Config::databaseGate))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database " . $dest . "</p>");
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
		
		//$sql =  $conn->Execute("SET search_path TO 'gate' ");		
			
		return $conn;
	}
	
	//excel migrasi
	function &getConnExcel() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConnExcel();

		return $instance;
	}
	function &createConnExcel() {
		define('ADODB_ASSOC_CASE', 0);
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driverExcel);

		if (!$conn->Connect(Config::serverExcel,Config::userExcel,Config::passwordExcel,Config::databaseExcel))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database " . $dest . "</p>");
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql =  $conn->Execute("SET search_path TO 'public' ");		
			
		return $conn;
	}
}

?>