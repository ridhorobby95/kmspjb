<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = 1;
	$c_edit = 1;
	$c_readlist = 1;
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Jenis Pustaka';

	//$p_filerep = 'repp_jenispustaka';
	$p_filerep = 'repp_jenispustaka1';
	$p_tbwidth = 420;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
	$l_kondisi = $rs_cb->GetMenu2('kdkondisi','',true,false,0,'id="kdkondisi" class="ControlStyle" style="width:187"');
	$l_kondisi = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_kondisi);
	
	?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Rekap Jenis Pustaka</td></tr>
	<tr>
		<td class="LeftColumnBG">Kondisi Pustaka</td>
		<td><?= $l_kondisi ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">

function goPreSubmit() {
		goSubmit();
}

</script>
</html>