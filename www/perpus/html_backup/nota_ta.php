<?php
	// $conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	//print_r($_REQUEST);
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	$p_filelist=Helper::navAddress('dbebas_sumbangan.php');
	$r_id=Helper::removeSpecial($_GET['id']);
	
	if($r_id =='') {
		header('Location: '.$p_filelist);
		exit();
	}
	
	$ta=$conn->GetRow("select t.*,a.namaanggota,t.idanggota noika,j.namajurusan,f.namafakultas from pp_ta t
					   join ms_anggota a on t.idanggota=a.idanggota
					   left join lv_jurusan j on a.idunit=j.kdjurusan
					   left join lv_fakultas f on j.kdfakultas=f.kdfakultas
					   where iddaftarta='$r_id'");
	$petugas=$conng->GetOne("select userdesc from sc_user where username='".$ta['npkdaftarta']."'");
	
	$buku=$conn->Execute("select o.*,t.nottb from pp_orderpustaka o
						  join pp_orderpustakattb tb on o.idorderpustaka=tb.idorderpustaka
						  join pp_ttb t on tb.idttb=t.idttb
						  where o.iddaftarta='$r_id'");
?>

<html>
<head>
<title>Surat Bebas Pinjam</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="printpage()">
<table cellpadding="0" cellspacing="0" border="0" width="800">
	<tr>
		<td rowspan="5"><img src="images/perpustakaan/kop.png" width="800" height="95" style="border-bottom:1px;"></td>
	</tr>
</table>
<table cellpadding="4" cellspacing="0" width="800" border="0">
	<!--<tr>
		<td style="border-top:double #000000"  colspan="2">&nbsp;</td>
	</tr>-->
	<tr>
		<td align="center" colspan="2"><b><u><font size="3" face="Bodoni MT">K E T E R A N G A N</font></u></b></td>
	</tr>
	<tr>
		<td align="center" colspan="2">No : <?= $buku->fields['nottb'] ?><br><br><br></td>
	</tr>
	<tr>
		<td>Menerangkan bahwa :</td>
	</tr>
	<tr>
		<td height="70">
		<table>
		<tr><td width="25">&nbsp;</td>
			<td width="150">NIM MAHASISWA</td>
			<td>: </td><td><b><?= $ta['noika'] ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="120">NAMA</td>
			<td>: </td><td><b><?= $ta['namaanggota'] ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="170">FAKULTAS/JURUSAN</td>
			<td>: </td><td><b><?= $ta['namafakultas']." / ".$ta['namajurusan'] ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="170">JUDUL TA/SKRIPSI/THESIS</td>
			<td>: </td><td><b><?= $buku->fields['judul'] ?></b></td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>Telah menyerahkan Buku Tugas Akhir/Skripsi/Thesis/Disertasi pada Perpustakaan PT Pembangkitan Jawa Bali.</td>
	</tr>	
</table><br><br><br>
<table>
	<tr height="30">
		<td width="400"></td>
		<td><br>
		Surabaya, <?= Helper::formatDateInd($ta['tgldaftarta'] ) ?><br>Petugas,<br><br><br><br><?= $petugas ?>
		<td>
	</tr>
</table>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }


</script>
</html>