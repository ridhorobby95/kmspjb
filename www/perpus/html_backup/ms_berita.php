<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	$connLamp = Factory::getConnLamp();
	//$connLamp->debug = true;
	$cek = $connLamp->IsConnected();

	// definisi variabel halaman
	$p_dbtable = 'pp_berita';
	$p_window = '[PJB LIBRARY] Berita Perpustakaan';
	$p_title = 'Berita Perpustakaan';
	$p_tbwidth = 800;
	$p_filelist = Helper::navAddress('list_berita.php');
	
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['petugas'] = $_SESSION['PERPUS_NAME'];
			$record['tglberita']=Helper::formatDate($_POST['tglberita']);
			$record['tglexpired']=Helper::formatDate($_POST['tglexpired']);
			$record['isiberita']=$_POST['isiberita'];
			$record['isiberitaen']=Helper::removeSpecial($_POST['isiberitaen']);
			
			Helper::Identitas($record);
			
			if($r_key == '') { // insert record	
			
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
			}
			else { // update record
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idberita,$r_key);
							
			}
			
			if($_FILES['foto']['tmp_name']){
				if($_FILES['foto']['error'] == UPLOAD_ERR_OK)
					$sfok = UI::createFoto($_FILES['foto']['tmp_name'],$p_foto,500,500);
				else if($_FILES['foto']['error'] == UPLOAD_ERR_INI_SIZE or $_FILES['foto']['error'] == UPLOAD_ERR_FORM_SIZE)
					$sfok = -4; // ukuran file melebihi batas
				else if($_FILES['foto']['error'] == UPLOAD_ERR_NO_FILE)
					$sfok = -5; // tidak ada file yang diupload
					
				$path = $_FILES['foto']['tmp_name'];
				if($sfok>0){
					$id_poto = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 4 and idlampiran = $r_key ");
					$record = array();
					$record['idlampiran'] = $r_key;
					$record['jenis'] = '4'; # 1= cover, 2=sinopsis, 3=file pustaka, 4= foto
					$record['tiduser'] = '1'; #test
					$record['filename'] = $_FILES['foto']['name']; 
					if($id_poto){
						$err = Sipus::UpdateBlob($connLamp,$record,'lampiranperpus',$path,'idlampiranperpus','isifile',$id_poto);
					}else{
						$err = Sipus::InsertBlob($connLamp,$record,'lampiranperpus',$path,'idlampiranperpus','isifile');
					}
				}
				
				@unlink($_FILES['foto']['tmp_name']); // hapus dan jangan tampilkan pesan error bila error
				
				switch($sfok) {
					case -1: $uploadmsg = 'Format file foto tidak dikenali.'; break;
					case -2: $uploadmsg = 'Format file foto harus GIF, JPEG, atau PNG.'; break;
					case -3: $uploadmsg = 'File foto tidak bisa diupload.'; break;
					case -4: $uploadmsg = 'Ukuran file foto melebihi batas maksimal.'; break;
					case -5: $uploadmsg = 'Tidak ada file foto yang di-upload.'; break;
					//default: $uploadmsg = 'Upload foto berhasil.'; break;
				}
			}
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';
					$errdb .= $uploadmsg;
					Helper::setFlashData('errdb', $errdb);
					$row=$_POST; //rollback
					}
				else {
					if($uploadmsg){
						$errdb = $uploadmsg;
						Helper::setFlashData('errdb', $errdb);
					}else{
						$sucdb = 'Penyimpanan data berhasil.';	
						Helper::setFlashData('sucdb', $sucdb);
					}
				}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					$r_key = $conn->GetOne("select last_value from pp_berita_idberita_seq");
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
			
			
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where idberita = '$r_key'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	if(!$p_errdb) {
		if($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where idberita = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);
		
		$sql3 = "select isifile from lp.lampiranperpus where jenis = 4 and idlampiran = $r_key ";
		$rsLOB = $conn->GetOne($sql3);
		}
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/tinymcpuk/tiny_mce.js"></script>
	
	<script type="text/javascript">
	tinyMCE.init({
		mode: "textareas",
		theme: "advanced",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,hr,removeformat,visualaid,|,charmap,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor",
		theme_advanced_buttons3 : ""
	});
	</script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Berita </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan Berita</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset Isian</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus Berita</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br><? include_once('_notifikasi.php'); ?>
		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<header style="width:800px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
			<table width="<?= $p_tbwidth ?>" cellpadding="4" cellspacing="0" align="center" class="GridStyle">	
				<tr> 
					<td class="LeftColumnBG thLeft" width=150>Judul Berita *</td>
					<td class="RightColumnBG"><?= UI::createTextBox('judulberita',$row['judulberita'],'ControlStyle',255,70,$c_edit); ?>
					</td>
					<td valign="top" align="center" rowspan="3">
						<div id="uploadmsg"></div>
						<?php
							$file = Sipus::loadBlobFile($rsLOB, 'jpg');
						?>
						<img border="1" id="imgfoto" src="<?= (is_null($file)) ? Config::fotoUrl.'default.jpg' : $file ?>" width="120" height="150"><br>
						<input type="file" name="foto" class="ControlStyle" size="15" style="max-width:220px;">
					</td>
				</tr>
				<tr> 
					<td class="LeftColumnBG thLeft">Tanggal Pasang</td>
					<td class="RightColumnBG"><?= UI::createTextBox('tglberita',empty($row['tglberita']) ? date('d-m-Y') : Helper::formatDate($row['tglberita']),'ControlStyle',10,10,$c_edit,'readOnly="readOnly"'); ?>
					<? if($c_edit) { ?>
					<img src="images/cal.png" id="tglberita1" style="cursor:pointer;" title="Pilih tanggal berita">
					&nbsp;
					<script type="text/javascript">
					Calendar.setup({
						inputField     :    "tglberita",
						ifFormat       :    "%d-%m-%Y",
						button         :    "tglberita1",
						align          :    "Br",
						singleClick    :    true
					});
					</script>
					<? } ?>
					[ Format : dd-mm-yyyy ]
					</td>
				</tr>
				<tr> 
					<td class="LeftColumnBG thLeft">Tanggal Expired</td>
					<td class="RightColumnBG"><?= UI::createTextBox('tglexpired',Helper::formatDate($row['tglexpired']),'ControlStyle',10,10,$c_edit,'readOnly="readOnly"'); ?>
					<? if($c_edit) { ?>
					<img src="images/cal.png" id="tglexpired1" style="cursor:pointer;" title="Pilih tanggal expired">
					&nbsp;
					<script type="text/javascript">
					Calendar.setup({
						inputField     :    "tglexpired",
						ifFormat       :    "%d-%m-%Y",
						button         :    "tglexpired1",
						align          :    "Br",
						singleClick    :    true
					});
					</script>
					<? } ?>
					[ Format : dd-mm-yyyy ]
					</td>
				</tr>
				<tr> 
					<td class="LeftColumnBG thLeft">Isi Berita</td>
					<td colspan=2><?= UI::createTextArea('isiberita',$row['isiberita'],'ControlStyle',20,75,$c_edit); ?></td>
				</tr>
				<tr>
					<td colspan="3" class="footBG">&nbsp;</td>
				</tr>
			</table>



		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tglberita").mask("99-99-9999");
	   $("#tglexpired").mask("99-99-9999");
});
</script>

<script language="javascript">

function saveData() {
	if(cfHighlight("judulberita"))
		goSave();
}
function aSavePhoto(formid) {
	//if(!formid)
		formid = "cacform";
	$("#" + formid).attr("target","upload_iframe");
	$("#" + formid + " #act").val("simpanfoto");
	$("#" + formid + " #imgfoto").waitload();
	goSubmit();
}
</script>
</html>