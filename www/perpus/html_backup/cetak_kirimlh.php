<?php
	$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarolah';
	$p_window = '[PJB LIBRARY] Cetak Pengantar Pengiriman';
	$p_title = 'PENGANTAR PENGIRIMAN';
	$p_tbheader = '.: PENGANTAR PENGIRIMAN :.';
	
	$r_id = Helper::removeSpecial($_GET['key']);
	$r_type = Helper::removeSpecial($_GET['type']);
	$r_lokasi = Helper::removeSpecial($_GET['lokasi']);
	$r_date = Helper::formatDate($_GET['date']);
	
	$psqlstr = "select e.noseri,p.judul,p.authorfirst1,p.authorlast1,p.nopanggil,e.harga from pp_transaksi t
				join pp_eksemplar e on t.ideksemplar=e.ideksemplar
				join ms_pustaka p on e.idpustaka=p.idpustaka
				where kdjenistransaksi='$r_type' and t.idanggota='$r_id'";
	if($r_date=='')
	    $psqlstr .= " and t.tgltransaksi=current_date ";
	else
	    $psqlstr .= " and t.tgltransaksi='$r_date' ";
	    
	if($r_lokasi!='')
		$psqlstr .=" and t.kdlokasi='$r_lokasi' ";
	
	$rs = $conn->Execute($psqlstr);
	$anggota=$conn->GetOne("select namaanggota from ms_anggota where idanggota='$r_id'");
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="90" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">
<table cellpadding=0 cellspacing=0 border=0 width="1100" height="537" background="images/print/kirimlh2.png">
	<tr>
		<td valign="top">
		<div style="margin-top:90;margin-left:130"><?= $anggota ?></div>
		<div style="margin-top:55;margin-left:0">
		<table cellpadding=0 cellspacing=0 border=0>
			<? 
			$no=0;
			while ($row=$rs->FetchRow()){ $no++ ?>
			<tr height=20>
				<td width="40" valign="top"><?= $no ?></td>
				<td width="900"><?= Helper::limitS($row['judul']). " - ". $row['authorfirst1']." ". $row['authorlast1'] ?></td>
				<td width="100"><?= $row['noseri'] ?></td>
				<td width="200" align="center"><?= $row['nopanggil']=='' ? ' - ' : $row['nopanggil'] ?></td>
				<td width="200" align="right"><?= Helper::formatNumber($row['harga'],'0',true,true) ?></td>
			</tr>
			<? } ?>
		</table>
		</div>
		</td>
	</tr>
</table>
<div style="margin-top:-130;margin-left:800"><?= Helper::FormatDateInd(date('Y-m-d')) ?></div>
<div style="margin-top:50;margin-left:50">
<table>
	<tr>
		<td width="600"><b><?//= $anggota ?></b></td>
		<td><b><?//= Sipus::GetOperator($conng) ?></b></td>
	</tr>
</table>
</div>

</body>
</html>
