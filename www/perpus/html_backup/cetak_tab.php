<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	session_start();
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_edit = $a_auth['canedit'];
	
	// bila belum login redirect ke index
	if(!isset($_SESSION['PERPUS_USER'])) {
		Helper::navigateOut();
		exit();
	}
	
	$tab = $_REQUEST['tab'];
	$width = "65%";
	if($tab=="peminjam"){
		#peminjam
		$anggota = $conn->GetArray("select count(*) jml, j.namajenisanggota
					from pp_transaksi t 
					join ms_anggota a on a.idanggota = t.idanggota 
					join lv_jenisanggota j on j.kdjenisanggota = a.kdjenisanggota 
					where to_char(t.tgltransaksi, 'YYYY') = '".date('Y')."' 
					group by j.namajenisanggota 
					order by j.namajenisanggota ");
		$dat_ang = array();
		foreach($anggota as $da){
			$dat_ang[] = "['".$da['namajenisanggota']."',".$da['jml']."]";
			$total = $total + $da['jml'];
		}
		
		#pengunjung mahasiswa
		$peminjam_mhs = $conn->GetArray("select count(*) jml, j.namajurusan
					from pp_transaksi b 
					join ms_anggota a on a.idanggota = b.idanggota 
					join lv_jurusan j on j.kdjurusan = a.idunit 
					where to_char(b.tgltransaksi, 'YYYY') = '".date('Y')."' and a.kdjenisanggota = 'M'
					group by j.namajurusan 
					order by j.namajurusan");
	}elseif($tab=="pengunjung"){
		#pengunjung
		$pengunjung = $conn->GetArray("select count(*) jml, j.namajenisanggota
					from pp_bukutamu b 
					join ms_anggota a on a.idanggota = b.idanggota 
					join lv_jenisanggota j on j.kdjenisanggota = a.kdjenisanggota 
					where to_char(tanggal, 'YYYY') = '".date('Y')."' 
					group by j.namajenisanggota 
					order by j.namajenisanggota");
		$dat_peng = array();
		foreach($pengunjung as $pg){
			$dat_peng[] = "['".$pg['namajenisanggota']."',".$pg['jml']."]";
			$t_pengunjung = $t_pengunjung + $pg['jml'];
		}
		
		#pengunjung mahasiswa
		$pengunjung_mhs = $conn->GetArray("select count(*) jml, j.namajurusan
					from pp_bukutamu b 
					join ms_anggota a on a.idanggota = b.idanggota 
					join lv_jurusan j on j.kdjurusan = b.kdjurusan 
					where to_char(tanggal, 'YYYY') = '".date('Y')."' and a.kdjenisanggota = 'M'
					group by j.namajurusan 
					order by j.namajurusan");
	}elseif($tab=="pusjur"){
		#koleksi
		$koleksi = $conn->GetArray("select count(*) jml, j.namajurusan
					from pp_bidangjur b 
					join lv_jurusan j on j.kdjurusan = b.kdjurusan 
					group by j.namajurusan 
					order by j.namajurusan ");
		$dat_kol = array();
		foreach($koleksi as $da){
			$dat_kol[] = "['".str_replace('Prodi ','',$da['namajurusan'])."',".$da['jml']."]";
			$t_koleksi = $t_koleksi + $da['jml'];
		}
	}elseif($tab=="eksjur"){
		#koleksi eksemplar
		$koleksie = $conn->GetArray("select count(e.ideksemplar) jml, j.namajurusan
					from pp_bidangjur b 
					join pp_eksemplar e on e.idpustaka = b.idpustaka 
					join lv_jurusan j on j.kdjurusan = b.kdjurusan 
					group by j.namajurusan 
					order by j.namajurusan ");
		$dat_kole = array();
		foreach($koleksie as $da){
			$dat_kole[] = "['".str_replace('Prodi ','',$da['namajurusan'])."',".$da['jml']."]";
			$t_koleksie = $t_koleksie + $da['jml'];
		}
	}elseif($tab=="terpinjam"){
		#koleksi terpinjam per bulan
		$koleksi_t = $conn->GetArray("select count(*) jml, to_char(b.tgltransaksi, 'mm') bulan 
					from pp_transaksi b 
					where to_char(b.tgltransaksi, 'YYYY') = '".date('Y')."'
					group by to_char(b.tgltransaksi, 'mm') 
					order by to_char(b.tgltransaksi, 'mm') ");
		$dat_kolt = array();
		foreach($koleksi_t as $dat){
			$dat_kolt[] = $dat['jml'];
			#get bulan aktif terakhir
			if($bul){
				if(intval($dat['bulan']) > $bul){
					$bul = intval($dat['bulan']);
					$data['jumlahbulan'] = $bul;
				}else{
					continue;
				}
			}else{
				$bul = intval($dat['bulan']);
				$data['jumlahbulan'] = $bul;
			}
		}
		$jumlahbulan = $data['jumlahbulan'];
		
		$categorie =  array();
		for($i=1; $i<=$jumlahbulan; $i++) {
			$categorie[] = Helper::bulanInd($i);
		}
		$categories = "'".implode("','",$categorie)."'";
		$koljem = implode(",",$dat_kolt);
	}elseif($tab=="pustaka"){
		#koleksi pustaka eksemplar per tahun
		$y2 = intval(date('Y'));
		$y1 = intval(date('Y')) - 4;
		$koleksi_p = $conn->GetArray("select count(distinct e.idpustaka) jmlpustaka, count(e.ideksemplar) jmleksemplar, p.tahunterbit
					from pp_eksemplar e 
					join ms_pustaka p on p.idpustaka = e.idpustaka 
					where 1=1 and coalesce(p.tahunterbit,'0') between '$y1' and '$y2' 
					group by p.tahunterbit ");
		$pustakas = array();
		$eksemplars = array();
		$tahuns = array();
		foreach($koleksi_p as $dap){
			$pustakas[] = $dap['jmlpustaka'];
			$eksemplars[] = $dap['jmleksemplar'];
			$tahuns[] = $dap['tahunterbit'];
		}
	}

?>
<html>
<head>
<title>[PJB LIBRARY] Selamat Datang</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link rel="stylesheet" href="style/pager.css">

<link href="style/tabpane.css" rel="stylesheet" type="text/css">
	
<script language="JavaScript" type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/highcharts/highcharts.js"></script>
<script type="text/javascript" src="scripts/highcharts/modules/exporting.js"></script>

<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery.dimensions.js"></script>
<script type="text/javascript" src="scripts/jquery.positionBy.js"></script>
<script type="text/javascript" src="scripts/jquery.jdMenu.js"></script>
<link rel="stylesheet" href="style/jquery.jdMenu.css" type="text/css">
<link rel="stylesheet" href="style/style.css">
<link rel="icon" type="image/x-icon" href="images/favicon.png">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="printpage();" >
	
<div id="wrapper">
	<div class="SideItem" id="SideItem" style="width:80%;">
<?php if($tab=="peminjam"){?>
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td colspan="2" align="center" class="SideSubTitle" style="font-size: 14px;">
						Data Peminjam Perpustakaan PJB Tahun <?=date('Y');?>
					</td>
				</tr>
				<tr>
					<td width="50%" valign="top">
						<table cellpadding="4" cellspacing="0" border=1>
							<tr class="DataBG" align="center">
								<td width="70%">Jenis Anggota</td>
								<td>Jumlah</td>
							</tr>
							<?
								foreach($anggota as $a){
									if(trim($a['namajenisanggota']) == "Mahasiswa"){
							?>
							<tr>
								<td colspan="2">
									<table cellpadding="4" cellspacing="0" border=1>
										<tr><td colspan="2"><strong><?=$a['namajenisanggota'];?></td></strong></tr>
										<?
											foreach($peminjam_mhs as $mhs){
										?>
										<tr>
											<td width="71.8%"><?=$mhs['namajurusan'];?></td>
											<td align="right" style="padding-right: 30px;"><?=number_format($mhs['jml'],0, ',', '.');?></td>
										</tr>
										<?
											}
										?>
										<tr>
											<td align="right"><strong>Subtotal Mahasiswa</strong></td>
											<td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
										</tr>
									</table>
								</td>
							</tr>
							<?
									}else{
							?>
							<tr>
								<td><strong><?=$a['namajenisanggota'];?></strong></td>
								<td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
							</tr>
							<?
									}
								}
							?>
							<tr>
								<td align="center" class="FootBG"><b>Total</b></td>
								<td align="right" class="FootBG"><b><?= number_format($total,0, ',', '.');?></b></td>
							</tr>
						</table>
					</td>
					<td width="50%" valign="top" align="center">
						<div id="container_peminjam" style="width: 500px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
			</table>
		</div>
<?php }elseif($tab=="pengunjung"){?>
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td colspan="2" align="center" class="SideSubTitle" style="font-size: 14px;">
						Data Pengunjung Perpustakaan PJB Tahun <?=date('Y');?>
					</td>
				</tr>
				<tr>
					<td width="50%" valign="top">
					<table cellpadding="4" cellspacing="0" border=1>
						<tr class="DataBG" align="center">
							<td width="70%">Jenis Anggota</td>
							<td>Jumlah</td>
						</tr>
						<?
							foreach($pengunjung as $a){
								if(trim($a['namajenisanggota']) == "Mahasiswa"){
						?>
							<tr>
								<td colspan="2">
									<table cellpadding="4" cellspacing="0" border=1>
										<tr><td colspan="2"><strong><?=$a['namajenisanggota'];?></td></strong></tr>
										<?
											foreach($pengunjung_mhs as $mhs){
										?>
										<tr>
											<td width="71.8%"><?=$mhs['namajurusan'];?></td>
											<td align="right" style="padding-right: 30px;"><?=number_format($mhs['jml'],0, ',', '.');?></td>
										</tr>
										<?
											}
										?>
										<tr>
											<td align="right"><strong>Subtotal Mahasiswa</strong></td>
											<td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
										</tr>
									</table>
								</td>
							</tr>
							<?
									}else{
							?>
							<tr>
								<td><strong><?=$a['namajenisanggota'];?></strong></td>
								<td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
							</tr>
							<?
									}
							}
						?>
						<tr>
							<td align="center" class="FootBG"><b>Total</b></td>
							<td align="right" class="FootBG"><b><?= number_format($t_pengunjung,0, ',', '.');?></b></td>
						</tr>
					</table>
					</td>
					<td width="50%" valign="top" align="center">
						<div id="container_pengunjung" style="width: 500px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
			</table>
		</div>
<?php }elseif($tab=="pusjur"){?>
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td width="50%" valign="top" align="center">
						<span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Perpustakaan PJB Hingga Tahun <?=date('Y');?></span><br/>
						<div id="container_koleksi" style="width: 750px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
				<tr>
					<td width="50%" valign="top">
						<table cellpadding="4" cellspacing="0" border=1 style="width: 50%">
							<tr class="DataBG" align="center">
								<td>Jurusan</td>
								<td>Jumlah Judul Pustaka</td>
							</tr>
							<?
								foreach($koleksi as $a){
							?>
							<tr>
								<td><?=$a['namajurusan'];?></td>
								<td align="right"><?=number_format($a['jml'],0, ',', '.');?></td>
							</tr>
							<?
								}
							?>
							<tr>
								<td align="center" class="FootBG"><b>Total</b></td>
								<td align="right" class="FootBG"><b><?= number_format($t_koleksi,0, ',', '.');?></b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<?php }elseif($tab=="eksjur"){?>
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td width="50%" valign="top" align="center">
						<span class="SideSubTitle" style="font-size: 14px;">Data Eksemplar Perpustakaan PJB Hingga Tahun <?=date('Y');?></span><br/>
						<div id="container_koleksie" style="width: 750px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
				<tr>
					<td width="50%" valign="top">
						<table cellpadding="4" cellspacing="0" border=1 style="width: 50%">
							<tr class="DataBG" align="center">
								<td>Jurusan</td>
								<td>Jumlah Eksemplar</td>
							</tr>
							<?
								foreach($koleksie as $a){
							?>
							<tr>
								<td><?=$a['namajurusan'];?></td>
								<td align="right"><?=number_format($a['jml'],0, ',', '.');?></td>
							</tr>
							<?
								}
							?>
							<tr>
								<td align="center" class="FootBG"><b>Total</b></td>
								<td align="right" class="FootBG"><b><?= number_format($t_koleksie,0, ',', '.');?></b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<?php }elseif($tab=="terpinjam"){?>	
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td width="50%" valign="top" align="center">
						<span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Terpinjam Perpustakaan PJB Tahun <?=date('Y');?></span><br/>
						<div id="container_koleksit" style="width: 750px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
			</table>
		</div>
<?php }elseif($tab=="pustaka"){?>
		<div>
			<table width="100%" cellspacing="0" cellpadding="4">
				<tr>
					<td width="50%" valign="top" align="center">
						<span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Pustaka Perpustakaan PJB <br/>Tahun <?=$y1." s/d ".$y2;?></span><br/>
						<div id="container_kolekpus" style="width: 750px; height: 300px; margin: 0 auto"></div>
					</td>
				</tr>
			</table>
		</div>
<?php }?>		
	</div>
</div>

</body>

<script type="text/javascript">
$(function () {
    var pie_peminjam, pie_pengunjung, pie_koleksi, pie_koleksie, bar_koleksi, bar_kolekpus;
    
    $(document).ready(function() {
<?php if($tab=="peminjam"){?>
	pie_peminjam = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_peminjam',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		exporting: {
			enabled: false
		},
		colors: ['#FE9A2E', '#64E572', '#ED561B', '#DDDF00', '#50B432', '#FF9655', '#24CBE5', '#FFF263', '#6AF9C4'],
		series: [{
		    type: 'pie',
		    name: 'Data Anggota',
		    data: [<?=implode(",",$dat_ang);?>]
		}]
	});

<?php }elseif($tab=="pengunjung"){?>
	
	pie_pengunjung = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_pengunjung',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		exporting: {
			enabled: false
		},
		series: [{
		    type: 'pie',
		    name: 'Data Anggota',
		    data: [<?=implode(",",$dat_peng);?>]
		}]
	});

<?php }elseif($tab=="pusjur"){?>
	
	pie_koleksi = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_koleksi',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		exporting: {
			enabled: false
		},
		series: [{
		    type: 'pie',
		    name: 'Data Koleksi',
		    data: [<?=implode(",",$dat_kol);?>]
		}]
	});

<?php }elseif($tab=="eksjur"){?>
	
	pie_koleksie = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_koleksie',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		exporting: {
			enabled: false
		},
		series: [{
		    type: 'pie',
		    name: 'Data Koleksi',
		    data: [<?=implode(",",$dat_kole);?>]
		}]
	});

<?php }elseif($tab=="terpinjam"){?>
	
	bar_koleksi = new Highcharts.Chart({
            chart: {
                renderTo: 'container_koleksit',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {	
				title: {
                    text: 'Bulan'
                },
                categories: [<?=$categories;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Koleksi (Eksemplar)'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
						}
					}
                }
            },
		exporting: {
			enabled: false
		},
	    legend: {
				enabled: false
			},
            series: [
				{
					name: 'Jumlah Eksemplar',
					data: [<?=$koljem;?>]
				},
		]
        });

<?php }elseif($tab=="pustaka"){?>

	bar_kolekpus = new Highcharts.Chart({
            chart: {
                renderTo: 'container_kolekpus',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {	
				title: {
                    text: 'Tahun'
                },
                categories: [<?=implode(",",$tahuns)?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Koleksi'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
						}
					}
                }
            },
		exporting: {
			enabled: false
		},
	    legend: {
				enabled: false
			},
            series: [
			{
				name: 'Pustaka',
				data:[<?=implode(",",$pustakas)?>]
			},
			{
				name: 'Eksemplar',
				data:[<?=implode(",",$eksemplars)?>]
			
			}
		]
        });

<?php }?>
    });
        
});
</script>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>

