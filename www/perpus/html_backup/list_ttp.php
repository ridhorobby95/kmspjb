<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	require_once('includes/sipus.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Daftar Tanda Terima Pustaka';
	$p_title = 'Daftar Tanda Terima Pustaka';
	$p_tbheader = '.: Daftar TTP :.';
	$p_col = 9;
	$p_tbwidth = 860;
	$p_id = "dafttb";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idorderpustaka';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from pp_orderpustaka where ststtb=0";
  	
	// pengaturan ex
	if (!empty($_POST)){
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['id']);
		if($r_aksi=='proses'){
			$record = array();
			$record['ststtb'] = 1; 
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey");
			$conn->CompleteTrans();	
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Proses Penerimaan Pustaka Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Proses Penerimaan Pustaka Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}		
		else if ($r_aksi=='filter') {
			$filt=Helper::removeSpecial($_POST['txtid']);
			if($filt!='')
			$p_sqlstr.=" and nottb='$filt'";
		}
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
	}
	else{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') {
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	//group by
	//$p_sqlstr .=" group by tgldelivery,nodelivery,npkpetugasdelivery ";	
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
<tr>
	<td colspan=9 align="center">
<table width="100%">
	<tr height="30">
		<td align="center" valign="top" class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td align="center"><? include_once('_notifikasi.php') ?></td>
	<tr>
	<tr>
		<td align="center" valign="bottom">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_add) { ?>
				<td width="140">
				<a href="javascript:goNew('<?= $p_filelist; ?>')" class="buttonshort"><span class="new">
				TTP baru</span></a></td>
				<? } ?>
				<td width="140">
				<a href="javascript:goRef()" class="buttonshort"><span class="refresh">
				Refresh</span></a></td>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
	<td colspan=9>
	<table width="100%">
		<tr>
			<td width="150">Masukkan No TTP </td>
			<td>: &nbsp; <input type="text" name="txtid" id="txtid" maxlength="20" value="<?= $filt ?>"  onkeydown="etrFil(event);"></td>
			<td  align="right"><a href="javascript:goFilt()" class="buttonshort"><span class="filter">Filter</span></a></td>
		</tr>
	</table>
	</td>
</tr>
	<tr height="20"> 
		<td width="280" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','tglttb:D');">Judul Pustaka<?= $p_xsort['tglttb']; ?></td>		
		<td width="200" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','nottb:C');">Pengarang <?= $p_xsort['nottb']; ?></td>		
		<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','npkttb:C');">Penerbit <?= $p_xsort['npkttb']; ?></td>		
		<td width="50" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','npkttb:C');">Tahun<?= $p_xsort['npkttb']; ?></td>		
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['nodelivery'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height=20> 
		<td align="left"><?= Helper::tglEng($row['judul']); ?></td>
		<td align="left"><?= $row['authorfirst1'].' '.$row['authorlast1']; ?></td>
		<td nowrap align="left"><?= $row['penerbit']; ?></td>
		<td align="left"><?= $row['tahunterbit']; ?></td>
		<td align="center">
			<?if($row['ststtb']==0){?>
				<u onClick="goProses('<?= $row['idorderpustaka'] ?>')"  title="Tandai Sudah Diterima" class="link"><img src="images/proses.png"></u>
			<?}else{?>
				<u title="Tandai Sudah Diterima" class="link"><img src="images/centang.png"></u>
			<?}?>
		</td>
		
		</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
		</td>
	</tr>
</table><br>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="id" id="id">
<input type="hidden" name="idanggota" id="idanggota">
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>

<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>
</body>

<script type="text/javascript">
var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function etrFil(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goFilt();
}

function goRef(){
	document.getElementById('txtid').value='';
	goSubmit();
}

function goFilt(){
	document.getElementById('act').value='filter';
	goSubmit();

}

function goProses(key){
	var conf=confirm("Apakah Anda yakin akan melakukan penerimaan pustaka ini ?");
	if(conf){
	document.getElementById("act").value="proses";
	document.getElementById('id').value=key;
	goSubmit();
	}
}
</script>
</html>