<?php
	defined('__VALID_ENTRANCE') or die('Akses terbatas');
	
	// otentikasi user
	Helper::checkRoleAuth($conng, FALSE);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_xpage = Helper::navAddress('xlist_unitusul.php');
	$p_xmlmenu = Helper::navAddress('xml_satker.php');
	
	$p_window = '[PJB LIBRARY] Pengusulan Unit';
	
	// unset filter bila refresh
	if($_REQUEST['r'] == 1)
		unset($_SESSION['PERPUS_UNITFILTER']);
	
	// untuk back to list
	if(!isset($_SESSION['PERPUS_UNITFILTER']))
		$_SESSION['PERPUS_UNITFILTER'] = '';
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/jquery.treeview.css">
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<?php include('inc_menu.php'); ?>
	<div class="container">
		<div class="SideItem" id="SideItem">
          <div class="table-responsive">  
			<table cellpadding="0" cellspacing="0" align="center" width="100%" style="border-collapse:collapse">
			<tr>
				<td id="leftcolhead" style="background:url(images/tab_header_gray.gif) no-repeat; font-size:12px;" height="25" valign="middle">
					<a href="javascript:void(0);" id="minibutton"><img id="tabbutton" align="absmiddle" border="0" src="images/tab_minimize.gif"/></a>
					<font color="#FFFFFF"><strong>Filter</strong></font></td>
				<td style="background:url(images/tab_header_gray.gif) no-repeat; padding-left: 30px; font-size:12px;" height="25">
					<font color="#FFFFFF"><strong>Pengusulan Unit</strong></font></td>
			</tr>
			<tr>
				<td id="leftcolmenu" valign="top" style="border: #a3b3c9 1px solid; width:200px;" bgcolor="#f5f5f5">
				<div id="menufilter" style="height:100%;"></div>
				</td>
				<td valign="top" align="left" id="contents" style="border: #a3b3c9 1px solid"></td>
			</tr>
			</table>
            </div>
			<div id="progressbar" style="position:absolute;visibility:hidden;left:0px;top:0px;">
				<table bgcolor="#FFFFFF" border="1" style="border-collapse:collapse;"><tr><td align="center">
				Mohon tunggu...<br><br><img src="images/progressbar.gif">
				</td></tr></table>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.treeview.js"></script>
<script type="text/javascript" src="scripts/jquery.xtree.js"></script>
<script type="text/javascript" src="scripts/forpagerx.js"></script>
<script type="text/javascript">

// untuk ajaxing list
xlist = "<?= $p_xpage; ?>";
xtdid = "contents";
expanded = true;

$(document).ready(function(){
	$("#menufilter").filtercTreeFromXML({xml: "<?= $p_xmlmenu ?>", ctid: "contents", page: "<?= $p_xpage ?>", init: "<?= $_SESSION['PERPUS_UNITFILTER'] ?>"});
	
	$("#minibutton").click(function() {
		if(expanded) {
			$("#menufilter").css("display","none");
			$("#leftcolhead").attr("style","width:20px;");
			$("#leftcolmenu").attr("style","width:20px;");
			$("#tabbutton").attr("src","images/tab_expand.gif");
			expanded = false;
		}
		else {
			$("#menufilter").css("display","block");
			$("#leftcolhead").attr("style","width:200px;");
			$("#leftcolmenu").attr("style","width:200px;");
			$("#leftcolhead").css("background","url(images/tab_header_gray.gif) no-repeat");
			$("#tabbutton").attr("src","images/tab_minimize.gif");
			expanded = true;
		}
	});
	
	
});

</script>
</html>