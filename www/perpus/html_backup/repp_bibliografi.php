<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Bibliografi Koleksi Pustaka';
	
	$p_namafile = 'bibliografi_'.Helper::indoMonth(substr($r_tgl1,3,2)).substr($r_tgl1,6,4).'-'.Helper::indoMonth(substr($r_tgl2,3,2)).substr($r_tgl2,6,4);
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$peng = $conn->GetArray("select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor   ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	$sql = "select p.kodeddc,p.tglperolehan,to_char(p.judul) judul,p.edisi,p.nopanggil,p.authorfirst1,p.authorlast1,p.kota,p.namapenerbit,p.tahunterbit,
			jmlhalromawi,jmlhalaman,dimensipustaka,kdklasifikasi, p.idpustaka,p.kdjenispustaka,p.noseri 
		from ms_pustaka p 
		left join pp_eksemplar e on e.idpustaka=p.idpustaka
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
		where to_date(to_char(p.tglperolehan,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')
			$sqlAdminUnit ";
			
	if($r_lokasi)
		$sql .=" and l.kdlokasi = '$r_lokasi' ";
			
	$sql .= "group by kdklasifikasi,p.kodeddc,p.tglperolehan,to_char(p.judul),p.edisi,p.nopanggil,p.authorfirst1,p.authorlast1,p.kota,p.namapenerbit,
			p.tahunterbit,jmlhalromawi,jmlhalaman,dimensipustaka, p.idpustaka,p.kdjenispustaka,p.noseri
		order by to_char(judul) asc";	
	$row = $conn->Execute($sql);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11pt;
	
	}
	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h4>BIBLIOGRAFI KOLEKSI PERPUSTAKAAN PJB<br/>
		<?= Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4).
		' - '.Helper::indoMonth((int)substr($r_tgl2,5,2))." ".substr($r_tgl2,0,4)?>
	</h4>
  	</strong></td>
  </tr>
 
</table>
<br><br>
<table width="900" border="0" cellpadding="2" cellspacing="0">
	<?php
		$no=1;
		while($rs=$row->FetchRow()) 
		{  
			if(!empty($rs['edisi'])) //cek edisi
				$edisi = " ed. ".$rs['edisi']."./";
			else
				$edisi = ". ";
				
			$auth = array();
			if(!empty($pengarang[$rs['idpustaka']])){
				foreach($pengarang[$rs['idpustaka']] as $a){
					$auth[] = $a;
				}
				$auhtor =  implode(", ",$auth);
			}else
				$auhtor = "";
							
			if(!empty($rs['kota'])) //cek kota terbit
				$kota = " ".$rs['kota']." :";
			else
				$kota = " - :";
				
			if(!empty($rs['namapenerbit'])) // cek penerbit
				$penerbit = " ".$rs['namapenerbit'].",";
			else
				$penerbit = " -,";
				
			if(!empty($rs['tahunterbit'])) //cek tahun terbit
				$tahun = " ".$rs['tahunterbit'].".";
			else
				$tahun = " -.";
			// ===========================KOLASI============================	
			if(!empty($rs['jmlhalromawi'])) //cek hal romawi
				$romawi = $rs['jmlhalromawi'].",";
			else
				$romawi = " -,";
			
			if(!empty($rs['jmlhalaman'])) //cek halaman
				$halaman = " ".$rs['jmlhalaman'].".";
			else
				$halaman = "";
			
			if(!empty($rs['dimensipustaka'])) //cek dimensi
				$dimensi = "; ".$rs['dimensipustaka'].".";
			else
				$dimensi = "";
	
			$namabelakang = strtoupper(substr($auhtor,0,3));
			$satuhurufjudul = strtolower(substr($rs['judul'],0,1));
	?>
	<tr height=25>
		<td width="1%" nowrap align="left" valign="top"><?=$no.". ";?></td>
		<td width="20" nowrap align="left"><?=$rs['judul'].$edisi.$auhtor.$kota.$penerbit.$tahun?>
		<br><br><b><?= str_repeat("&nbsp;",10)?><?= $rs['kdklasifikasi']." / ".str_replace($rs['kdjenispustaka'],$rs['kdjenispustaka'].".",$rs['noseri'])." / ".$namabelakang." / ".$satuhurufjudul?></b><br><br>
		</td>
	</tr>
	<? 		$no++;
		} ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
</table>


</div>
</body>
</html>