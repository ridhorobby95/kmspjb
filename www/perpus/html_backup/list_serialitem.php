<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
 
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];

	// require tambahan
	
	
	// definisi variabel halaman

	$p_window = '[PJB LIBRARY] Daftar Serial Item Tak Tergenerate Eksemplar';
	$p_title = 'Daftar Serial Item Tak Tergenerate Eksemplar';
	$p_tbheader = '.: Daftar Serial Item Tak Tergenerate Eksemplar:.';
	$p_col = 9;
	$p_tbwidth = 950;
	$p_filelist = Helper::navAddress('list_pustakaberseri.php');
	$p_filedetail = Helper::navAddress('ms_serialitemx.php');
	$p_id = "pp_serialitem";
	$p_eksemplar = Helper::navAddress('ms_eksemplar.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'noseri';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select p.idpustaka, p.keywords, c.periode, p.noseri, p.judul ,s.nomor, s.volume, s.tahun, s.keterangan ket, r.keterangan keteks,
		s.idserial, s.idserialitem, s.tgldatang, s.tglperkiraan, s.jumlah, coalesce(s.tgldatang,r.tglperolehan) as tgll, e.namasupplier,
		(case when (count(r.*) = 0) then 'Tidak generate eksemplar' else count(r.*) end) as jumlah_eksemplar 
		from ms_pustaka p 
		left join pp_serialcontrol c on c.idpustaka = p.idpustaka 
		left join pp_serialitem s on s.idserial= c.idserial 
		left join pp_eksemplar r on p.idpustaka = r.idpustaka and s.idserialitem = r.idserialitem
		left join ms_supplier e on e.kdsupplier = c.kdsupplier 
		where isserial = 1 and (r.idserialitem is null)";
	
	
	//variabel filtering
	$f2=Helper::removeSpecial($_POST['carikeyword']);
	$f4=Helper::removeSpecial($_POST['carijudul']);
	$jenis = Helper::removeSpecial($_POST['kdjenispustaka']);
	
	$p_topiklv="select t.idpustaka from pp_topikpustaka t join lv_topik l on t.idtopik=l.idtopik where upper(l.namatopik) like upper('%$f2%') ";
	if($f2!=''){
	$p_sqlstr.=" and upper(p.keywords) like upper('%$f2%') or p.idpustaka in ($p_topiklv)";
	}
	if($f4!='') {
	$p_sqlstr.=" and upper(p.judul) like upper('%$f4%') ";
	}
	if($jenis!=''){
	$p_sqlstr .=" and p.kdjenispustaka = '$jenis' ";
	}
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '')
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
			
		} // end for

	} 
	$p_sqlstr .= " group by p.idpustaka, p.keywords, c.periode, p.noseri, p.judul ,s.nomor, s.volume, s.tahun, s.keterangan, r.keterangan,
		s.idserial, s.idserialitem, s.tgldatang, s.tglperkiraan, s.jumlah, coalesce(s.tgldatang,r.tglperolehan), e.namasupplier ";

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	//tambahan groupby
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);

	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal<>1 and isserial=1 and kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$jenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);	


?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link rel="shortcut icon" href="images/favicon.ico" />

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);document.getElementById('carikeyword').focus();" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	<table width="95%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
		<tr height="30">
			<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
		</tr>
		<tr>
			<td width="100%"  colspan="<?= $p_col ?>" align="center">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td align="left" width="130">
						<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
						Refresh</span></a></td>
						<? if($c_readlist) { ?>
						<td align="left" width="70">
							<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Pustaka Berseri</span></a>
						</td>
						<? }?>
						<td align="right" valign="middle">
						<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
						&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%" colspan="5">
				<table cellpadding=0 cellspacing="2" border=0 width="100%">
					<tr>
						<td>Keyword</td>
						<td colspan="4">: <input type="text" id="carikeyword" name="carikeyword" size="35" value="<?= $_POST['carikeyword'] ?>" onKeyDown="etrCari(event);"></td>
					</tr>
					<tr>
						<td>Judul Pustaka</td>
						<td >: <input type="text" id="carijudul" name="carijudul" size="35" value="<?= $_POST['carijudul'] ?>" onKeyDown="etrCari(event);"></td>
						<td>Jenis Pustaka</td>
						<td>: <?= $l_jenis ?></td>
						<td align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr height="20"> 
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.noseri:C');">REG COMP <?= $p_xsort['p.noseri']; ?></td>			
			<td width="30%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.judul:C');">Judul Pustaka  <?= $p_xsort['p.judul']; ?></td>		
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Periode</td>
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Terbitan</td>
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Tgl Perkiraan</td>
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Tgl Datang</td>
			<td width="5%" nowrap align="center" class="SubHeaderBGAlt">Jumlah</td>
			<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Supplier</td>
			<td width="5%" nowrap align="center" class="SubHeaderBGAlt"><? if ($c_edit) echo "Edit"; else echo "Lihat"; ?></td>
		</tr>	
		<?php
		$i = 0;

		if($showlist) {
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="20" valign="middle"> 
		<td><?= $row['noseri'] ?></td>	
		<td align="left"><u title="Lihat Detail" style="cursor:pointer;color:blue" onclick="popup('index.php?page=show_pustaka&id=<?= $row['idpustaka'] ?>',550,600)"><?= Helper::limitS($row['judul']).($row['judulseri']!='' ? ' : '.$row['judulseri'] : '').($row['edisi']!='' ? ' / '.$row['edisi'] : '') ?></u></td>
		<td align="left"><?=$row['periode'];?></td>
		<td><?= $row['ket']; ?></td>
		<td align="left"><?=$row['tglperkiraan'];?></td>
		<td><?= $row['tgldatang']; ?></td>
		<td align="left"><?=$row['jumlah'];?></td>
		<td><?= $row['namasupplier']; ?></td>
		<td align="center">
		<!--<u title="Generate Eksemplar" onclick="goDet('<?= $p_filedetail ?>','<?= $row['idserial'] ?>','<?= $row['idserialitem'] ?>');" style="cursor:pointer;color:#3300FF;"><img src="images/edited.gif" width=18 height=18></u>-->
		<u title="Generate Eksemplar" onclick="popup('index.php?page=ms_serialitemx&key2=<?= $row['idserialitem'] ?>&key=<?= $row['idserial'] ?>',620,500);" style="cursor:pointer"><img src="images/edited.gif" width=18 height=18></u>
		</td>
	</tr>
	<?php
	$nomer++;		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr>
	
</table><br>



<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="act" id="act">
<input type="hidden" name="npwd" id="npwd">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">

<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>


</form>
</div>


</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xauto.js"></script>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">


function goClear(){
document.getElementById("carikeyword").value='';
document.getElementById("carijudul").value='';
//goSubmit();
}
</script>
</html>