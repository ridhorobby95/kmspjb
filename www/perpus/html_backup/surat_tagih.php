<?php
	// $conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	
	$p_window = '[PJB LIBRARY] Surat Tagih Anggota';
	$p_title = 'Surat Tagih Anggota';
	$p_tbheader = '.: Surat Tagih Anggota :.';
	
	$idtagihan=Helper::removeSpecial($_GET['idtagihan']);
	
	$p_sql = "select p.idtagihan, p.idanggota, a.namaanggota,a.alamat,p.tagihanke, d.judul, d.authorfirst1, d.authorlast1, d.noseri,d.nopanggil
			  ,d.tgltransaksi,d.tgltenggat from pp_tagihan p
			  join pp_tagihandetail d on d.idtagihan=p.idtagihan
			  join ms_anggota a on p.idanggota = a.idanggota
			  where p.idtagihan=$idtagihan";
	$row = $conn->Execute($p_sql);
	if($idtagihan=='' or empty($row)) {
		echo "<script>window.close()</script>";
	}
?>	
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
	<style>
	body,td {
	font-family: Times New Roman;
	font-size: 11pt;
	}
	</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">
<div align="center">
<table cellpadding="0" cellspacing="0" width="800">
	<tr>
		<td><img src="images/logosim.jpg" width="300" height="95" style="border-bottom:1px;"></td>
	</tr>
</table><br>
<table cellpadding="0" cellspacing="0" width="800">
	<?/*?><tr>
		<td width="100">No.</td>
		<td>: </td>
		<td><input type="text" name="depan" class="ControlBlank" style="width:40px" maxlength=5/>/ Ptk-2 / <input type="text" name="tengah" class="ControlBlank" style="width:40px" maxlength=5 /> / <input type="text" name="belakang" class="ControlBlank" style="width:40px" maxlength=5 /></td>
		<td align="right"><?= Helper::formatDateInd(date('Y-m-d')) ?></td>
	</tr>
	<tr>
		<td>Lamp.</td>
		<td>:</td>
	</tr><?*/?>
	<tr>
		<td valign="top">Hal.</td>
		<td  valign="top" width="10">: </td>
		<td>Tagihan Buku ke <?= $row->fields['tagihanke'] ?> <br>
			Milik Perpustakaan
		</td>
	</tr>
</table><br>
<table cellpadding="0" cellspacing="0" width="800">
	<tr>
		<td width="100">Kepada. Yth :</td>
	</tr>
	<tr>
		<td>Nama</td>
		<td>: <?= $row->fields['namaanggota'] ?></td>
	</tr>
	<tr>
		<td>NRP</td>
		<td>: <?= $row->fields['idanggota'] ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>: <?= $row->fields['alamat'] ?></td>
	</tr>
</table>

<br><br>
<table cellpadding="0" cellspacing="0" width="800">
	<tr>
		<td align="justify">
			Dengan Hormat, <br>
			Untuk mendata kembali Administrasi buku-buku pinjaman yang telah dipinjam oleh pemakai Perpustakaan
			PJB, perkenankanlah kami mengingatkan, bahwa menurut database yang ada pada kami,
			Saudara masih mempunyai pinjaman buku sebagai berikut : <br><br>
			<table border="1" style="border-collapse:collapse" width="100%">
				<tr>
					<td rowspan="2" width="40" align="center"><strong>NO.</strong></td>
					<td rowspan="2" width="25%" align="center"><strong>JUDUL</strong></td>
					<td rowspan="2" width="15%" align="center"><strong>PENGARANG</strong></td>
					<td rowspan="2" width="70" align="center"><strong>REG.COMP</strong></td>
					<td rowspan="2" width="80" align="center"><strong>CALL NUMBER</strong></td>
					<td colspan="2" width="100" align="center"><strong>TANGGAL</strong></td>
				</tr>
				<tr>
					<td align="center"><strong>PINJAM</strong></td>
					<td align="center"><strong>KEMBALI</strong></td>
				</tr>
				<? 
					$no=0;
					while ($rs=$row->FetchRow()){
					$no++; ?>
				<tr height="20">
					<td align="center"><?= $no ?></td>
					<td><?= $rs['judul'] ?></td>
					<td><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
					<td><?= $rs['noseri'] ?></td>
					<td><?= $rs['nopanggil'] ?></td>
					<td align="center"><?= Helper::tglEng($rs['tgltransaksi']) ?></td>
					<td align="center"><?= Helper::tglEng($rs['tgltenggat']) ?> </td>
				</tr>
				<? } ?>
			</table>
			<br><br>
			Kami mengharap Saudara dapat meluangkan waktu untuk memenuhi kewajiban mengembalikan buku/memberikan
			informasi ke Perpustakaan melalui telepon atau email.<br><br>
			Terima Kasih.
		</td>
	</tr>
</table><br><br><br>
<table cellpadding="0" cellspacing="0" width="700">
	<tr>
		<td width="67%">&nbsp;</td>
		<td>Hormat Kami,<br>
		<?=$kepala['jabatan'];?><br><br><br><br><br><br>
		<?=$kepala['namalengkap'];?><br>
		NIP. <?=$kepala['nik'];?>
		</td>
	</tr>
</table>
</div>
</body>
</html>