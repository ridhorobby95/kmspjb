<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// print_r($_REQUEST);
	// pengecekan tipe session user
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Grafik Peminjam';
	
	$r_tahun = Helper::removeSpecial($_REQUEST['thn']);
	$sql = "select count(b.idanggota) jmlanggota, to_char(b.tgltransaksi, 'dd-mm-YYYY') tgl, to_char(b.tgltransaksi, 'dd') hari, to_char(b.tgltransaksi, 'mm') bulan, j.namasatker, a.idunit kdjurusan  
		from pp_transaksi b 
		join ms_anggota a on a.idanggota = b.idanggota
		left join ms_satker j on j.kdsatker = a.idunit  
		where to_char(b.tgltransaksi, 'YYYY') = '$r_tahun'
		group by to_char(b.tgltransaksi, 'dd-mm-YYYY'), to_char(b.tgltransaksi, 'dd'), to_char(b.tgltransaksi, 'mm'), j.namasatker, a.idunit
		order by a.idunit, j.namasatker, bulan, tgl asc";
	$rs = $conn->Execute($sql);
	$data = array();
	while($row = $rs->FetchRow()){
		if($jur == ($row['kdjurusan']?$row['kdjurusan']:"Tidak Ada Unit")){
			#rekap
			$data['jurusan'][$jur][intval($row['bulan'])]['jml'] = $data['jurusan'][$jur][intval($row['bulan'])]['jml'] + $row['jmlanggota'];
		}else{
			$jur = ($row['kdjurusan']?$row['kdjurusan']:"Tidak Ada Unit");
			$data['jurusan'][$jur]['satker'] = ($row['namasatker']?$row['namasatker']:"Kosong");
			#rekap
			$data['jurusan'][$jur][intval($row['bulan'])]['jml'] = $row['jmlanggota'];
		}
	}
	
	$categorie =  array();
	for($i=1; $i<=12; $i++) {
		$categorie[] = Helper::bulanInd($i);
	}
	$categories = "'".implode("','",$categorie)."'";
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
			<div class="left title">
				<h3>Laporan Data Peminjam Perpustakaan PT Pembangkitan Jawa Bali<br>Tahun <?= $r_tahun; ?></h3>
			</div>
			<div id="wrapper">
				<div class="SideItem" id="SideItem">
					<center>
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/highcharts/highcharts.js"></script>
<script type="text/javascript" src="scripts/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    var chart_test;
	
    $(document).ready(function() {
		
	//testing
	Highcharts.setOptions({
		colors: ['#4AA02C', '#F88017']
	});
	
        chart_tes = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {	
				title: {
                    text: 'Bulan'
                },
                categories: [<?=$categories;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Peminjam'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
							
								//return '<b>'+ this.series.name +'</b>: '+ this.y;
						}
					}
                }
            },
            series: [
			<?
			foreach($data['jurusan'] as $jur => $value){
				$bar = array();
				for($i=1; $i<=12; $i++) {
					$bar[] = number_format($data['jurusan'][$jur][$i]['jml'],0, ',', '.');
				}
			?>
				{
					name: '<?=$jur." - ".$data['jurusan'][$jur]['satker'];?>',
					data: [<?=implode(",",$bar)?>]
				},
			<?
			}
			?>
		]
        });


    });    
});

</script>
</html>