<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']); //ideksemplar
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Data Pengolahan Eksemplar';
	$p_title = 'Data Pengolahan Eksemplar';
	$p_tbwidth = 900;
	$p_filelist = Helper::navAddress('list_pengolahan.php');
	
	if($r_key==''){
		header('Location: '.$p_filelist);
		exit();
	}
	
	$sql = "select idpustaka from $p_dbtable where ideksemplar=$r_key";
	$rkey = $conn->GetOne($sql);
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$conn->StartTrans();
			
			//=====start penyimpanan ke pustaka ======
			$auth1=Helper::removeSpecial($_POST['addauthor'][0]);
			if ($auth1!='')
			$v_auth1=$conn->GetRow("select namadepan,namabelakang from ms_author where idauthor=$auth1");
			
			$auth2=Helper::removeSpecial($_POST['addauthor'][1]);
			if($auth2!='')
			$v_auth2=$conn->GetRow("select namadepan,namabelakang from ms_author where idauthor=$auth2");
			
			$auth3=Helper::removeSpecial($_POST['addauthor'][2]);
			if($auth3!='')
			$v_auth3=$conn->GetRow("select namadepan,namabelakang from ms_author where idauthor=$auth3");
			
			$record = Helper::cStrFill($_POST);
			$record['tglperolehan'] = Helper::formatDate($_POST['tglperolehan']);
			
			$record['judul']=Helper::removeSpecial($_POST['judul']);
			$record['judulseri']=Helper::removeSpecial($_POST['judulseri']);
			$record['edisi']=Helper::removeSpecial($_POST['edisi']);
			if($auth1!=''){
			$record['authorfirst1']=Helper::cStrNull($v_auth1['namadepan']);
			$record['authorlast1']=Helper::cStrNull($v_auth1['namabelakang']);
			$record['authorfirst2']=Helper::cStrNull($v_auth2['namadepan']);
			$record['authorlast2']=Helper::cStrNull($v_auth2['namabelakang']);
			$record['authorfirst3']=Helper::cStrNull($v_auth3['namadepan']);
			$record['authorlast3']=Helper::cStrNull($v_auth3['namabelakang']);
			}
			$record['namapenerbit']=Helper::removeSpecial($_POST['namapenerbit']);
			$record['keterangan']=Helper::removeSpecial($_POST['keterangan1']);
			Helper::Identitas($record);
				
			Sipus::UpdateBiasa($conn,$record,'ms_pustaka','idpustaka',$rkey);
			
			$author=$conn->GetRow("select idauthor from pp_author where idpustaka=$rkey");
			if ($author['idauthor']!='')
				Sipus::DeleteBiasa($conn,'pp_author','idpustaka',$rkey);
				
			$pengarang1=Helper::removeSpecial($_POST['addauthor']);
			if($pengarang1!='')
			Sipus::InsertAuthor($conn,'pp_author','idauthor','idpustaka','ext',$pengarang1,$rkey);
			
			$topik=$conn->GetRow("select idtopik from pp_topikpustaka where idpustaka=$rkey");
			if ($topik['idtopik']!='')
				Sipus::DeleteBiasa($conn,'pp_topikpustaka','idpustaka',$rkey);
				
			$topik1=Helper::removeSpecial($_POST['addtopik']);
			if($topik1!='')
			Sipus::InsertRef($conn,'pp_topikpustaka','idtopik','idpustaka',$topik1,$rkey);
						
			//=====start penyimpanan ke eksemplar ======
			$receks['kdklasifikasi']=Helper::removeSpecial($_POST['kdklasifikasi']);
			$receks['kdkondisi']=Helper::removeSpecial($_POST['kdkondisi']);
			$receks['kdperolehan']=Helper::removeSpecial($_POST['kdperolehan']);
			$receks['tglperolehan']=Helper::formatDate($_POST['tgloleh']);
			$receks['kdrak']=Helper::removeSpecial($_POST['kdrak']);
			$receks['kdsupplier']=Helper::removeSpecial($_POST['kdsupplier']);
			$receks['kdlokasi']=Helper::removeSpecial($_POST['kdlokasi']);
			$receks['harga']=Helper::removeSpecial($_POST['harga']);
			$receks['keterangan']=Helper::removeSpecial($_POST['keterangan']);
			$receks['noinventaris']=Helper::removeSpecial($_POST['noinventaris']);
			
			Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,ideksemplar,$r_key);
			
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				$row = $_POST;
			}
			else {
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);				
			}
			
			$conn->CompleteTrans();
				
			if ($conn->ErrorNo() == 0){
				$rkey = $rkey;
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
			} 
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,'ms_pustaka','idpustaka',$rkey);
			
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			} else {
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			
		}		
	}
	if(!$p_errdb) {
		if ($r_key !='') {
			$p_sqlstr = "select p.noseri as seri,p.*,to_char(p.tglperolehan, 'dd-mm-YYYY') as tgloleh,l.namalokasi,p.keterangan as ket2,m.keterangan as keterangan1,j.islokal,
					j.namajenispustaka,m.* , p.noinventaris
						from $p_dbtable p 
						left join ms_pustaka m on m.idpustaka=p.idpustaka
						left join lv_jenispustaka j on m.kdjenispustaka=j.kdjenispustaka
						left join lv_lokasi l on p.kdlokasi=l.kdlokasi
						where p.ideksemplar = $r_key";
			$row = $conn->GetRow($p_sqlstr);
			if($row['idpenerbit'] !='')
			$p_terbit=$conn->GetRow("select namapenerbit from ms_penerbit where idpenerbit=".$row['idpenerbit']."");		
	
			// data select
			$sql = "select b.idauthor, b.namadepan, b.namabelakang from ms_author b join
					pp_author a on b.idauthor = a.idauthor where a.idpustaka = '$rkey' order by ext";
			$rsd = $conn->Execute($sql);
			
			$sql2 = "select b.idtopik, b.namatopik from lv_topik b join
					pp_topikpustaka a on b.idtopik = a.idtopik where a.idpustaka = '$rkey'";
			$rsx = $conn->Execute($sql2);
		}
	}
		
	// daftar combo box
	if($r_key != '') {
		$rs_cb = $conn->Execute("select namatopik, idtopik from lv_topik order by namatopik");
		$l_topik = $rs_cb->GetMenu2('idtopik',$row['idtopik'],true,false,0,'id="idtopik" class="ControlStyle" style="width:250"');
		
		$rs_cb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by kdbahasa");
		$l_bahasa = $rs_cb->GetMenu2('kdbahasa',$row['kdbahasa'],true,false,0,'id="kdbahasa" class="ControlStyle" style="width:255"');
		
		$cb_pus="select namajenispustaka, kdjenispustaka from lv_jenispustaka where 1=1 ";
		
		if($row['islokal']!='1'){
		$cb_pus .=" and islokal <>1";
		$cb_pus .=" order by kdjenispustaka";
		$rs_cb = $conn->Execute($cb_pus);
		$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:140"');
		} else{
		$cb_pus .=" and islokal = 1";
		$cb_pus .="order by kdjenispustaka";
		$rs_cb = $conn->Execute($cb_pus);
		$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:140" disabled');
		}
				
		$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");
		$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$row['kdklasifikasi'],true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$row['kdkondisi'],true,false,0,'id="kdkondisi" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by kdperolehan");
		$l_perolehan = $rs_cb->GetMenu2('kdperolehan',$row['kdperolehan'],true,false,0,'id="kdperolehan" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namarak, kdrak from ms_rak order by kdrak");
		$l_rak = $rs_cb->GetMenu2('kdrak',$row['kdrak'],true,false,0,'id="kdrak" class="ControlStyle" style="width:200"');
	
		$rs_cb = $conn->Execute("select namasupplier, kdsupplier from ms_supplier order by namasupplier");
		$l_suplier = $rs_cb->GetMenu2('kdsupplier',$row['kdsupplier'],true,false,0,'id="kdsupplier" class="ControlStyle" style="width:200"');
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/foreditx.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/combo.js"></script>

	<script type="text/javascript" >
	function CreateTextbox()
	{

		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type=file name='userfile[]' size='40' /> </p>";
		}

	}
	
	function callnumber(){
		var ddc=document.getElementById("kodeddc").value;
		var judul=document.getElementById("judul").value;
		
		var author = $("[name='namaauth[]']").eq(0).val();
		if($("[name='nambel[]']").eq(0).val())
			var auth = $("[name='nambel[]']").eq(0).val();
		else
			var auth = $("[name='namdep[]']").eq(0).val();
		
		var print=(auth.substring(0,3)); 
		var title=judul.substr(0,1);
		document.getElementById("nopanggil").value=ddc+" "+print.substr(0, 1).toUpperCase() + print.substr(1).toLowerCase()+" "+title.toLowerCase();
	}


	</script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
	<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><h1 style="font-weight:normal;color:#015593;"><?= $p_title ?></h1></td>
			</tr>
		</table>
		<?php include_once('_notifikasi.php'); ?>
	
	<table width="100">
		<tr>
		<? if($c_readlist) { ?>
		<td align="center">
			<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Pustaka </span></a>
		</td>
		<? } if($c_edit) { ?>
		<td align="center">
			<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
		</td>
		<td align="center">
			<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
		</td>
		<? } if($c_delete and $r_key) { ?>
		<td align="center">
			<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
		</td>
		<? } ?>
		</tr>
	</table><br>
	
	<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
	<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
	<table width="930" border="0" cellspacing=0 cellpadding="0">
	<tr><td>
	<div class="tabs" style="width:;">
		<ul>
			<li><a id="tablink" href="javascript:void(0)">Identitas Pustaka</a></li>
			<li><a id="tablink" href="javascript:void(0)">Referensi</a></li>
			<li><a id="tablink" href="javascript:void(0)">Identitas Eksemplar</a></li>
					
		</ul><br>
		<div style="width:96.8%;height:1px;background:#20915e;position:relative;top:-2px;margin-bottom:10px;"></div>
	<div id="items" style="position:relative;top:-2px">
		<header style="width:<?= $p_tbwidth ?>;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Identitas Pustaka</h1>
				</div>
			</div>
		</header>
	<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
		<tr   height=20> 
			<td class="LeftColumnBG thLeft" width=150>KODE PUSTAKA *</td>
			<td class="RightColumnBG" ><?= $row['noseri']?>
			
			</td>
		</tr>
		<tr    height=20> 
			<td class="LeftColumnBG thLeft">Jenis Pustaka *</td>
			<td class="RightColumnBG"><?= $c_edit ? $l_pustaka : $row['namajenispustaka']; ?></td>
			
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft"  width="150">Judul *</td>
			<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',1000,55,$c_edit); ?></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Judul Seri </td>
			<td class="RightColumnBG"><?= UI::createTextBox('judulseri',$row['judulseri'],'ControlStyle',200,55,$c_edit); ?></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Bahasa *</td>
			<td class="RightColumnBG"><?= $l_bahasa ?></td>
		</tr>
		<tr   height=20>
		<td class="LeftColumnBG thLeft">Kode DDC *</td>
			<td class="RightColumnBG"><?= UI::createTextBox('kodeddc',$row['kodeddc'],'ControlStyle',50,15,$c_edit); 
			 
			 ?>&nbsp; 
			 <? if($c_edit) { ?>
			 <img src="images/popup.png" onClick="window.open('http://en.wikipedia.org/wiki/List_of_Dewey_Decimal_classes')" title="DDC List" style="cursor:pointer;">&nbsp;<u onclick="window.open('http://en.wikipedia.org/wiki/List_of_Dewey_Decimal_classes')" title="DDC List" style="cursor:pointer;">DDC List...</u>
			 <? } ?>
			 </td>
		</tr>
		
		<tr   height=20> 
			<td class="LeftColumnBG thLeft" valign="top" style="padding-top:10px">Topik Pustaka</td>
			<td class="RightColumnBG" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="4">
					<? if($c_edit) { ?>
					<tr id="tr_tambahtp"> 
						<td bgcolor="#EEEEEE" colspan="2">
							<!--<span id="spantopik"><?= $l_topik ?></span>-->
							<input type="text" readonly id="namatopik" class="ControlStyle" size="38" maxlength="50">
							<input type="hidden" id="idtopik" class="ControlStyle" size="30" maxlength="50">
							<input type="button" class="ControlStyle" value="Tambah" onClick="addTopik()">&nbsp;&nbsp;
							<img src="images/popup.png" style="cursor:pointer;" title="Topik baru" onClick="popup('index.php?page=pop_topik',400,430);">
							<span id="span_posisi"><u title="Topik Baru" onclick="popup('index.php?page=pop_topik',400,430);" style="cursor:pointer;" class="Link">Lihat Topik...</u></span>
							<? if ($c_delete) { ?>
							<img src="images/add.png" style="cursor:pointer;" title="Insert Topik" onClick="popup('index.php?page=instopik',430,120);">
							<span id="span_posisi"><u title="Insert Topik" onclick="popup('index.php?page=instopik',430,120);" style="cursor:pointer;" class="Link">Topik Baru...</u></span>
							<? } ?>
							<br> <span id="span_error3" style="display:none"><font color="#FF0000">Topik tersebut telah ditambahkan</font></span>
							 <span id="span_error4" style="display:none"><font color="#FF0000">Topik tidak sesuai</font></span>
						</td>
					</tr>
				  <?	} ?>
					<tr id="tr_tpkosong"<? if(!$rsx->EOF) { ?> style="display:none" <? } ?>> 
						<td bgcolor="#EEEEEE" align="center" colspan="2"><strong>Belum memiliki topik</strong></td>
					</tr><? if($r_key !=''){
							  while($rowx = $rsx->FetchRow()) { ?>
					<tr> 
						<td>
							<input type="hidden" name="addtopik[]" id="addtopik" value="<?= $rowx['idtopik'] ?>">
							<?= $rowx['namatopik'] ?>
						</td>
						<td width="20" align="center">
						<? if($c_edit) { ?><img src="images/delete.png" onClick="delTopik(this)" style="cursor:pointer"> <? } ?>
						</td>
					</tr>
					<?	}}  ?>
						
				</table>
				<table id="table_templatetp" style="display:none">
					<tr> 
						<td bgcolor="#EEEEEE"><input type="hidden" name="addtopik[]" id="addtopik" disabled></td>
						<td bgcolor="#EEEEEE" width="20" align="center"><img src="images/delete.png" onClick="delTopik(this)" style="cursor:pointer"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td class="LeftColumnBG thLeft" valign="top">Pengarang</td>
			<td class="RightColumnBG" colspan="2">	
			<? if($row['islokal']!=1) {?>
				<table width="100%" cellspacing="0" cellpadding="4">
					<? if($c_edit) { ?>
					<tr id="tr_tambahbu"> 
						<td bgcolor="#EEEEEE" colspan="2">
							
							<input type="text" id="namaauthor" class="ControlStyle" size="38" maxlength="50">
							<input type="hidden" id="idauthor" class="ControlStyle" size="30" maxlength="50">
							<input type="hidden" name="namapeng" id="namapeng">
							<img id="imgauthor_c" src="images/posting.gif" style="display:none">
							<img id="imgauthor_u" src="images/nonposting.gif" style="display:none">
							<u title="Lihat Author" onclick="popup('index.php?page=pop_author',700,500);" style="cursor:pointer;" class="Link">Lihat Author...</u>
						
							<input type="button" class="ControlStyle" value="Tambah" onClick="addAuthor()">&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Insert Author" onClick="popup('index.php?page=insauthor',430,150);">
							<span id="span_posisi"><u title="Insert Author" onclick="popup('index.php?page=insauthor',430,150);" style="cursor:pointer;" class="Link">Author Baru...</u></span>
							<br> <span id="span_error" style="display:none"><font color="#FF0000">author tersebut telah ditambahkan</font></span>
							 <span id="span_error2" style="display:none"><font color="#FF0000">Data Author tidak sesuai</font></span>
	
						</td>
					</tr>
					 
				  <?	} ?>
					<tr id="tr_bukosong"<? if(!$rsd->EOF) { ?> style="display:none" <? } ?>> 
						<td bgcolor="#EEEEEE" align="center" colspan="2"><strong>Belum memiliki author</strong></td>
					</tr><? if($r_key !=''){
							  while($rowd = $rsd->FetchRow()) { ?>
					<tr> 
						<td >
							<input type="hidden" name="addauthor[]" id="addauthor" value="<?= $rowd['idauthor'] ?>">
							<?= $rowd['namadepan']." ". $rowd['namabelakang'] ?>
							<input type="hidden" name="namaauth[]" id="namaauth" value="<?= $rowd['namadepan']." ".$rowd['namabelakang'] ?>">
							<input type="hidden" name="namdep[]" id="namdep" disabled value="<?= $rowd['namadepan'] ?>">
							<input type="hidden" name="nambel[]" id="nambel" disabled value="<?= $rowd['namabelakang'] ?>">
	
						</td>
						<td width="20" align="center">
						<? if($c_edit) { ?><img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer"><? } ?></td>
					</tr>
					<?	} } ?>
					<tr id="tr_authorhidden" style="display:none"> 
						<td colspan="2">
						</td>
					</tr>	
				</table>
	
				<table id="table_templatebu" style="display:none">
					<tr> 
						<td bgcolor="#EEEEEE"><input type="hidden" name="addauthor[]" id="addauthor" disabled>
						<input type="hidden" name="namaauth[]" id="namaauth" disabled>
						</td>
						<td bgcolor="#EEEEEE" width="20" align="center"><img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer"></td>
					</tr>
				</table>
			<? } else {
					echo "1. ".$row['authorfirst1']. " " .$row['authorlast1']; 
					echo "<input type='hidden' name='namaauth[]' id='namaauth' value='".$row['authorfirst1']." ".$row['authorlast1']."'>";
					echo "<input type='hidden' name='namdep[]' id='namdep' value='".$row['authorfirst1']."'>";
					echo "<input type='hidden' name='nambel[]' id='nambel' value='".$row['authorlast1']."'>";

					if ($row['authorfirst2']) 
					{
						echo " <br> ";
						echo "2. ".$row['authorfirst2']. " " .$row['authorlast2'];
						echo "<input type='hidden' name='namaauth[]' id='namaauth' value='".$row['authorfirst2']." ".$row['authorlast2']."'>";
					
					}
					if ($row['authorfirst3']) 
					{
						echo "<br>";
						echo "3. ".$row['authorfirst3']. " " .$row['authorlast3'];
						echo "<input type='hidden' name='namaauth[]' id='namaauth' value='".$row['authorfirst3']." ".$row['authorlast3']."'>";
					
					}
		}
		?>
		</td>
		</tr>
		
		<tr> 
			<td width="140" class="LeftColumnBG thLeft">No panggil *</td>
			<td class="RightColumnBG" colspan="2">
			<?= UI::createTextBox('nopanggil',$row['nopanggil'],'ControlStyle',50,15,$c_edit,'onfocus="callnumber()"'); ?>
			</td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft" width="150">Penerbit *</td>
			<td class="RightColumnBG" colspan="2">
				<? if($c_edit) { ?>
				<input type="text" id="namapenerbit" name="namapenerbit" size="35" class="ControlRead" readonly value="<?= $row['namapenerbit'] ?>">
				<input type="hidden" id="idpenerbit" name="idpenerbit" size="10" value="<?= $row['idpenerbit'] ?>">
				<? } else  echo $p_terbit['namapenerbit'] ?>
				
				<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit',570,500);">
					<span id="span_posisi"><u title="Lihat Penerbit" onclick="popup('index.php?page=pop_penerbit',560,450);" style="cursor:pointer;" class="Link">Lihat Penerbit...</u></span>
					<? } ?>
					<? if ($c_delete) { ?>
					<img src="images/add.png" style="cursor:pointer;" title="Insert Penerbit" onClick="popup('index.php?page=inspenerbit',500,300);">
					<span id="span_posisi"><u title="Insert Penerbit" onclick="popup('index.php?page=inspenerbit',500,300);" style="cursor:pointer;" class="Link">Penerbit Baru...</u></span>
					<? } ?>
			</td>
		</tr>
		</td>
	</tr>
	</table>
	</div>
	<div id="items" style="position:relative;top:-2px"> <!-- ============= REFERENSI PUSTAKA ============================= -->
		<header style="width:<?= $p_tbwidth ?>;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Referensi Pustaka</h1>
				</div>
			</div>
		</header>
	<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
		<tr    height=20> 
			<td class="LeftColumnBG thLeft" width=150>ISBN </td>
			<td class="RightColumnBG"><?= UI::createTextBox('isbn',$row['isbn'],'ControlStyle',30,30,$c_edit); ?></td>
		</tr>
		
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Kota Terbit </td>
			<td class="RightColumnBG"><?= UI::createTextBox('kota',$row['kota'],'ControlStyle',20,20,$c_edit); ?></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Tahun Terbit</td>
			<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		</tr>
		
		<tr height=20> 
			<td class="LeftColumnBG thLeft">Edisi</td>
			<td class="RightColumnBG"><?= UI::createTextBox('edisi',$row['edisi'],'ControlStyle',60,30,$c_edit); ?></td>
		</tr>
		<tr    height=20> 
			<td class="LeftColumnBG thLeft">Tanggal Perolehan *</td>
			<td class="RightColumnBG"><?= UI::createTextBox('tglperolehan',Helper::formatDate($row['tglperolehan']),'ControlStyle',10,10,$c_edit); ?>
			<? if($c_edit) { ?>
			<img src="images/cal.png" id="tgleperolehan" style="cursor:pointer;" title="Pilih tanggal perolehan">
			&nbsp;
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "tglperolehan",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tgleperolehan",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			
			[ Format : dd-mm-yyyy ]
			<? } ?>
			</td>
		</tr>
		
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Jumlah Hal. Romawi </td>
			<td class="RightColumnBG"><?= UI::createTextBox('jmlhalromawi',$row['jmlhalromawi'],'ControlStyle',6,10,$c_edit); ?><em>Misal: iv</em></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Jumlah Halaman </td>
			<td class="RightColumnBG"><?= UI::createTextBox('jmlhalaman',$row['jmlhalaman'],'ControlStyle',10,15,$c_edit); ?><em>Misal: 215 hal (tanpa titik)</td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Ilustrasi? </td>
			<td class="RightColumnBG"><?= UI::createTextBox('ilustrasi',$row['ilustrasi'],'ControlStyle',10,15,$c_edit); ?> <em>Misal: ilus (tanpa titik)</em></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Index? </td>
			<td class="RightColumnBG"><?= UI::createTextBox('index_buku',$row['index_buku'],'ControlStyle',10,15,$c_edit); ?> <em>Misal: ind (tanpa titik)</em></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Dimensi Pustaka </td>
			<td class="RightColumnBG"><?= UI::createTextBox('dimensipustaka',$row['dimensipustaka'],'ControlStyle',30,15,$c_edit); ?><em>Tinggi Buku, Misal: 28 cm (tanpa titik)</em></td>
		</tr>
			<tr   height=20> 
			<td class="LeftColumnBG thLeft">Keywords</td>
			<td class="RightColumnBG"><?= UI::createTextBox('keywords',($row['keywords']),'ControlStyle',100,40,$c_edit); ?>
			<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Daftar Keywords" onClick="popup('index.php?page=pop_keywords',450,400);">
				<span id="span_key"><u title="Daftar Keywords" onclick="popup('index.php?page=pop_keywords',450,400);" style="cursor:pointer;" class="Link">History Keywords...</u></span>
			<? } ?>
			</td>
		</tr>
	
			<tr   height=20> 
			<td class="LeftColumnBG thLeft">Links Pustaka</td>
			<td class="RightColumnBG"><?= UI::createTextBox('linkpustaka',($row['linkpustaka']),'ControlStyle',200,40,$c_edit); ?></td>
		</tr>
		<?/*?><tr   height=20> 
			<td class="LeftColumnBG thLeft">Keterangan</td>
			<td class="RightColumnBG"><?= UI::createTextArea('keterangan1',$row['keterangan1'],'ControlStyle',5,55,$c_edit); ?></td>
		</tr><?*/?>
		</table>
	</div>
	<div id="items" style="position:relative;top:-2px"> <!-- ============= IDENTITAS EKSEMPLAR ============================= -->
		<header style="width:<?= $p_tbwidth ?>;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Identitas Eksemplar</h1>
				</div>
			</div>
		</header>
	<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
		<!--<tr> -->
		<!--	<td class="SubHeaderBGAlt" colspan="2" align="center">Identitas Eksemplar</td>-->
		<!--</tr>-->
		<tr   height=20> 
			<td class="LeftColumnBG thLeft" width=150>No. Induk *</td>
			<td class="RightColumnBG" ><?= $row['seri']?>
			
			</td>
		</tr>
		<tr    height=20> 
			<td class="LeftColumnBG thLeft" width=150>Label</td>
			<td class="RightColumnBG"><?= $l_klasifikasi ?></td>
		</tr>
		
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Kondisi</td>
			<td class="RightColumnBG"><?= $l_kondisi ?></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Asal Perolehan</td>
			<td class="RightColumnBG"><?= $l_perolehan ?></td>
		</tr>
		<tr height=20>
		<td class="LeftColumnBG thLeft">Tanggal Perolehan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tgloleh',Helper::formatDate($row['tgloleh']),'ControlStyle',10,10,$c_edit) ?>
		<? if($c_edit) { ?>
			<img src="images/cal.png" id="tgleoleh" style="cursor:pointer;" title="Pilih tanggal perolehan">
			<script type="text/javascript">
			Calendar.setup({
			inputField     :    "tgloleh",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleoleh",
			align          :    "Br",
			singleClick    :    true
			});
			</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
		</tr>
		
		<tr height=20> 
			<td class="LeftColumnBG thLeft">Tempat/Rak Pustaka</td>
			<td class="RightColumnBG"><?= $l_rak ?></td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Supplier</td>
			<td class="RightColumnBG"><?= $l_suplier ?></td>
		</tr>
		<tr height=20> 
			<td class="LeftColumnBG thLeft">Lokasi *</td>
			<td class="RightColumnBG">
			<? if($c_edit) { ?>
			<input type="text" id="namalokasi" name="namalokasi" size="35" disabled value="<?= $row['namalokasi'] ?>">
			<input type="hidden" id="kdlokasi" name="kdlokasi" size="10" value="<?= $row['kdlokasi'] ?>">
			
			<img src="images/delete.png" style="cursor:pointer;" title="Hapus Lokasi" onClick="goClearLokasi()">
			&nbsp;
			<img src="images/popup.png" style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi',500,500);">
				<span id="span_posisi"><u title="Lihat Lokasi" onclick="popup('index.php?page=pop_lokasi',450,500);" style="cursor:pointer;" class="Link">Lihat Lokasi...</u></span>
			<? } else echo $row['namalokasi']?>
			</td>
		</tr>
		<tr   height=20> 
			<td class="LeftColumnBG thLeft">Harga</td>
			<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',14,14,$c_edit); ?></td>
		</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">No. Inventaris</td>
				<td class="RightColumnBG">
				<? if($c_edit) { ?>
				<input type="text" id="noinventaris" name="noinventaris" size="35" value="<?= $row['noinventaris'] ?>">
				<? } else echo $row['noinventaris']?>
				</td>
			</tr>
			<tr   height=20> 
			<td class="LeftColumnBG thLeft">Keterangan</td>
			<td class="RightColumnBG"><?= UI::createTextBox('keterangan',($row['ket2']),'ControlStyle',100,40,$c_edit); ?>
			</td>
		</tr>
	</table>
	</div>
	</div>
	</td>
	</tr>
	</table>
	<iframe name="upload_iframe" style="display:none;"></iframe>
	<input type="hidden" name="act" id="act">
	<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
	<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
	<input type="hidden" name="delfile" id="delfile">
	<input type="hidden" name="delkey" id="delkey">
	</form>
	</div>
	
	
	<div id="div_author" style="display:none;">
	</div>
	
	<div align="left" id="div_autocomplete" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;">
		<table bgcolor="#FFFFFF" id="tab_autocomplete" cellpadding="3" cellspacing="0"></table>
	</div>
	</div>
</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xautox.js"></script>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
//$(function(){
//	   $("#tglperolehan").mask("99-99-9999");
//	   $("#namaauthor").xautox ({ajaxpage: "<?= Helper::navAddress('ajax') ?>", strpost: "f=pengarang", targetid: "idauthor", imgchkid: "imgauthor", posset: "2"});
//});

$(document).ready(function() {

	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	chooseTab(0);
});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
function goDelSin(key){
	var delsin=confirm("Apakah Anda yakin akan menghapus file sinopsis ?");
	if (delsin){
		document.getElementById('act').value="delsin";
		document.getElementById('delfile').value=key;
		goSubmit();
	}

}

function goAll () {
	var aa= document.getElementById('perpusform');
	var a=document.getElementsByName("addjurusan[]");
	//var x = aa.all.checked=true;
	if (aa.all.checked==true){
		checked = true
	}else{
		checked = false
	}
	
	for (var i = 0; i < a.length; i++) {
	 	//aa.pilih[i].checked = checked;
		a[i].checked = checked;
	}
	
}

function goDelFile(key1,key2){
	var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
	if(delfile){
		document.getElementById('act').value="delfile";
		document.getElementById('delkey').value=key1;
		document.getElementById('delfile').value=key2;
		goSubmit();
	}
}

function saveData() {
	if(cfHighlight("idpustaka,judul,kdbahasa,kdjenispustaka,namalokasi,namapenerbit,tglperolehan,tgloleh")){
		goSave();
	}
	
}
function addAuthor() {
	var idauthor = $("#idauthor").val();
	if(idauthor=='') {
		$("#span_error2").show();
		setTimeout('$("#span_error2").hide()',1000);
		return false; 
	} else {
		if($("[name='addauthor[]'][value='"+idauthor+"']").length > 0) {
			$("#span_error").show();
			setTimeout('$("#span_error").hide()',1000);
			return false;
		}
	}
	if($("#tr_bukosong:visible").length > 0)
		$("#tr_bukosong").hide();
	
	var newtab = $($("#table_templatebu tbody").html()).insertBefore("#tr_authorhidden");
	var newtdf = newtab.find("td").eq(0);
	var addauthor = newtdf.find("[name='addauthor[]']");
	var namaauth = newtdf.find("[name='namaauth[]']");
	var namaauthor = $("#namaauthor").val();
	newtdf.prepend(namaauthor);
	addauthor.val(idauthor);
	namaauth.val(namaauthor);
	addauthor.attr("disabled",false);
	document.getElementById("namapeng").value=namaauthor
}

function delAuthor(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
	
	if($("#tr_tambahbu").prev().is("#tr_bukosong"))
		$("#tr_bukosong").show();
}

function addTopik() {
	var idtopik = $("#idtopik").val();
	if (idtopik==''){
		$("#span_error4").show();
		setTimeout('$("#span_error4").hide()',1000);
		return false;
	} else {
		if($("[name='addtopik[]'][value='"+idtopik+"']").length > 0) {
			$("#span_error3").show();
			setTimeout('$("#span_error3").hide()',1000);
			return false;
		}
	}
	
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_tpkosong:visible").length > 0)
		$("#tr_tpkosong").hide();
	
	var newtab = $($("#table_templatetp tbody").html()).insertAfter("#tr_tambahtp");
	var newtdf = newtab.find("td").eq(0);
	var addtopik = newtdf.find("[name='addtopik[]']");
	var namatopik = $("#idtopik option[value='"+idtopik+"']").text();
	
	newtdf.prepend(namatopik);
	addtopik.val(idtopik);
	addtopik.attr("disabled",false);
}

function delTopik(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
	
	if($("#tr_tambahtp").prev().is("#tr_tpkosong"))
		$("#tr_tpkosong").show();
}

</script>
</html>