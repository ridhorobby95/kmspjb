<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengadaan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
		
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Tanda Terima Pustaka (TTP)';
	$p_title = '.: Tanda Terima Pustaka (TTP) :.';
	$p_title1 = 'Data Pengadaan';
	$p_tvalidasi = 'Validasi Perpus';
	$p_tvalidasi2 = 'Validasi Keuangan';
	$p_titlelist = '.: Daftar Pengadaan :.';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_pengadaan.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$recdetail = array();
			$record = array();
			$record['tglpengadaan'] = Helper::formatDate($_POST['tglpengadaan']);
			$record['kodeaktivitas'] = Helper::cStrNull($_POST['kodeaktivitas']);
			$record['thang'] = Helper::cStrNull($_POST['thang']);
			$record['keterangan'] = Helper::cStrNull($_POST['keterangan']);
			Helper::Identitas($record);
			
			$arparam = array('idorderpustaka','qtypengadaan','hargausulan');
			$recdetail = Helper::getArrParam($arparam,'idorderpustaka');
			
			if ($r_key == ''){
				$record['statuspengadaan'] = 'A';
				$err = Pengadaan::insertPengadaan($conn,$record,$recdetail);		
			}else{
				$err = Pengadaan::updatePengadaan($conn,$record,$recdetail,$r_key);
			}
			if($err[0] == 0) {
				$r_key = $err[1];
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
			else{
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'savedetail' and $c_edit) {	
			$record = array();
			$record['hargausulan'] = Helper::cStrNull($_POST['u_hrg']);
			$record['qtypengadaan'] = Helper::cStrNull($_POST['u_qty']);
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey and idpengadaan=$r_key");
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Update Detail Pengadaan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Update Detail Pengadaan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if($r_aksi == 'hapusdetail' and $c_edit) {
			$record = array();
			$record['idpengadaan'] = 'null';
			Helper::Identitas($record);
			$conn->StartTrans(); 
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey and idpengadaan=$r_key");
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan Detail Pengadaan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan Detail Pengadaan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $rkey;
		}
		else if($r_aksi == 'proses' and $c_edit) {
			$record = array();
			$record['statuspengadaan'] = 'PP';
			Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
		}
	}
	
  	if ($r_key == ''){
		$regcomp = Query::getCount($conn, 'idpengadaan', 'pp_pengadaan');
		$regcomp = $regcomp == '' ? 1 : $regcomp+1; 
	}else{
		$sql = "select * from pp_pengadaan  
				where idpengadaan=$r_key";	
		$row = $conn->GetRow($sql);
		$regcomp = $row['idpengadaan'];
				
		// sql untuk mendapatkan isi list
		$p_sqlstr="select * from pp_orderpustaka op
				left join pp_usul u on u.idusulan=op.idusulan
			   where idpengadaan = $regcomp order by tglusulan";
		$rs = $conn->Execute($p_sqlstr);
  	
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table border="0" cellpadding="4" cellspacing="0">
	<tr height="20">
		<td align="center" colspan=4 class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
	<td align="center">
		<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar</span></a>
	</td>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goRef();" class="buttonshort"><span class="reset">Reset</span></a>
	</td>
	<? if (trim($row['statuspengadaan']) == 'A'){?>
	<td align="center">
		<a href="javascript:goProses();" class="buttonshort"><span class="validasi">Proses</span></a>
	</td>
	<? }} if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
	<tr>
		<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
	</tr>
</table>
<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth+50 ?>" class="GridStyle">
	<tr>
		<td valign="top">
			<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth ?>">
				<tr>
					<td align="center" class="SubHeaderBGAlt" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1; ?></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">No. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?=  $regcomp; ?>&nbsp;&nbsp;&nbsp;&nbsp;<em>[Digenerate Oleh System]</em></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">Tgl. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><input type="text" name="tglpengadaan" size="10" id="tglpengadaan" value="<?= $row['tglpengadaan']=='' ? date('d-m-Y') : Helper::formatDate($row['tglpengadaan']) ?>">
						<? if($c_edit) { ?>
						<img src="images/cal.png" id="tglada" style="cursor:pointer;" title="Pilih tanggal pengadaan">
						&nbsp;
						<script type="text/javascript">
						Calendar.setup({
							inputField     :    "tglpengadaan",
							ifFormat       :    "%d-%m-%Y",
							button         :    "tglada",
							align          :    "Br",
							singleClick    :    true
						});
						</script>
						
						<? } ?>
					</td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Kode Aktivitas</td>
        			<td class="RightColumnBG" colspan="2">
						<?= UI::createTextBox('kodeaktivitas',$row['kodeaktivitas'],'ControlRead',20,20,$c_edit,'readonly'); ?>
						<input type="hidden" name="thang" id="thang">
						<img src="images/tombol/breakdown.png" id="btnaktivitas" title="Cari Kode Anggaran" style="cursor:pointer" onClick="openLOV('btnaktivitas', 'aktivitas',-130, 40,'addAktivitas',500)">
					</td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Jumlah</td>
					<td class="RightColumnBG" colspan="2"><div id="totqty">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Total Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><div id="totprice">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Status Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?= Helper::getArrStatusP(trim($row['statuspengadaan']))?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Keterangan</td>
					<td class="RightColumnBG" colspan="2"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',3,60,$c_edit); ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table cellpadding="4" cellspacing="0" width="<?= p_tbwidth; ?>">
	<tr>
		<td align="center" class="PageTitle"><?= $p_titlelist; ?></td>
	</tr>
</table>
<br>
<table width="800" cellpadding="4" cellspacing="0" style="border-collapse:collapse" border="1">
	<tr>
		<td class="SubHeaderBGAlt" colspan="2" align="center" width="10%">Id Usulan</td>
		<td class="SubHeaderBGAlt" align="center">Tgl Usulan</td>
		<td class="SubHeaderBGAlt" align="center" nowrap>Judul</td>
		<td class="SubHeaderBGAlt" align="center">Pengarang</td>
		<td class="SubHeaderBGAlt" align="center">Pengusul</td>
		<td class="SubHeaderBGAlt" align="center">Harga</td>
		<td class="SubHeaderBGAlt" align="center">Qty</td>
		<td class="SubHeaderBGAlt" align="center">Aksi</td>
	</tr>
    <? if (!empty($r_key)){ 
    		$i=0;
			while ($rows = $rs->FetchRow()){
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				$hrg += $rows['hargausulan'];
				$qty += $rows['qtypengadaan'];
				
				if(strcasecmp($rows['idorderpustaka'],$p_editkey)) {
	?>
    <tr class="<?= $rowstyle ?>"> 
    	<td colspan="2" align="center"><?= $rows['idusulan']; ?></td>
        <td align="center"><?= Helper::formatDate($rows['tglusulan']); ?></td>
    	<td align="left"><?= $rows['judul']; ?></td>
        <td align="left"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
    	<td><?= $rows['namapengusul']; ?></td>
    	<td align="right"><?= $rows['hargausulan']; ?></td>
    	<td align="right"><?= $rows['qtypengadaan']; ?></td>
    	<td align="center"><img src="images/tombol/edited.gif" width="16" title="Edit Detail" onClick="editRow('<?= $rows['idorderpustaka'] ?>')" class="link"/>&nbsp;&nbsp;<img src="images/delete.png" width="16" title="Hapus Detail" onClick="delRow('<?= $rows['idorderpustaka']?>')" class="link"/></td>
    </tr>
    <? 	}else{ ?>
    <tr class="<?= $rowstyle ?>"> 
    	<td colspan="2" align="center"><?= $rows['idusulan']; ?></td>
        <td align="center"><?= Helper::formatDate($rows['tglusulan']); ?></td>
    	<td align="center"><?= $rows['judul']; ?></td>
        <td align="center"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
    	<td><?= $rows['namapengusul']; ?></td>
		<td><?= UI::createTextBox('u_hrg',$rows['hargausulan'],'ControlStyle',10,10,$c_edit); ?></td>
		<td><?= UI::createTextBox('u_qty',$rows['qtypengadaan'],'ControlStyle',3,3,$c_edit); ?></td>
    	<td align="center"><img src="images/tombol/file.png" width="16" title="Save Detail" onClick="saveRow('<?= $rows['idorderpustaka'] ?>')" class="link"/>&nbsp;&nbsp;<img src="images/delete.png" width="16" title="Hapus Detail" onClick="delRow('<?= $rows['idorderpustaka']?>')" class="link"/></td>
    </tr>
	<? }}} ?>
	<tr id="tr_add">
		<td><?= UI::createTextBox('usulan','','ControlRead',10,10,$c_edit,'readonly'); ?><input type="hidden" name="idorder" id="idorder"></td>
		<td><img src="images/tombol/breakdown.png" id="btnusulan" title="Cari Usulan" style="cursor:pointer" onClick="openLOV('btnusulan', 'usulan',-100, 20,'addUsulan',800)"></td>
		<td><?= UI::createTextBox('tgl','','ControlRead',10,10,$c_edit,'readonly'); ?></td>
		<td><?= UI::createTextBox('jdl','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
		<td><?= UI::createTextBox('pengarang','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
		<td><?= UI::createTextBox('pengusul','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
		<td><?= UI::createTextBox('hrg','','ControlStyle',10,10,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		<td><?= UI::createTextBox('qty','','ControlStyle',3,3,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		<td><input type="button" name="tambah" id="tambah" value="Tambah" onClick="addUsul();"></td>
	</tr>
</table>
<input type="hidden" name="key" id="key" value="<?= $r_key; ?>">
<input type="hidden" name="rkey" id="rkey">
<input type="hidden" name="act" id="act">
</form>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

$(function(){
	hitTotal();
});

function saveData(){
	if(cfHighlight("tglpengadaan")){
		$("#act").val("simpan");
		goSubmit();
	}
}

function onlyNumber(e,elem,dec) {
	var code = e.keyCode || e.which;
	if ((code > 57 && code < 96) || code > 105 || code == 32) {
		if(code == 190 && dec) {
			if(elem.value == "") // belum ada isinya, titik tidak boleh didepan
				return false;
			if(elem.value.indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
				return false;
			return true;
		}
		return false;
	}
}

function addUsulan(idusulan, tglusulan, judul, pengarang, pengusul, harga, qty, idorder) {
		$("#usulan").val(idusulan);
		$("#tgl").val(tglusulan);
		$("#jdl").val(judul);
		$("#pengarang").val(pengarang);
		$("#pengusul").val(pengusul);
		$("#hrg").val(harga);
		$("#qty").val(qty);
		$("#idorder").val(idorder);
}

function addAktivitas(kode,thang) {
		$("#kodeaktivitas").val(kode);
		$("#thang").val(thang);
}


function addUsul(val)
{
	var n = 0;
	if(cfHighlight("usulan,hrg,qty")){
		$("input[name^=idorderpustaka]").each(function(i){
			if(parseInt($("#idorder").val()) == parseInt($("input[name^=idorderpustaka]").eq(i).val())){
				$(this).addClass("ControlErr");
				n++;
			}else{
				$(this).removeClass("ControlErr");
			}
		});
		if(n > 0){
			alert("Detail Pengadaan Sudah Ada");
		}else{
			var baris = '';		
			var numdet = 1;
			var tmp = 'te';
			var namabarang = 's';
			numdet += 1;
			
			baris = '<tr valign="top" id="tr_detail'+numdet+'">'  + "\n" +
					'	<td align="center" colspan="2">' + $("#usulan").val() + '</td>'  + "\n" +
					'	<td align="center">' + $("#tgl").val() + '</td>'  + "\n" +
					'	<td>' + $("#jdl").val() + '</td>'  + "\n" +
					'	<td>' + $("#pengarang").val() + '</td>'  + "\n" +
					'	<td>' + $("#pengusul").val() + '</td>'  + "\n" +
					'	<td align="right">' + $("#hrg").val() + '</td>'  + "\n" +
					'	<td align="right">' + $("#qty").val() + '</td>'  + "\n" +
					'	<td align="center">'  + "\n" +
					'		<input type="hidden" name="idorderpustaka[]" id="idorderpustaka[]" value="' + $("#idorder").val() + '">' + "\n" +
					'       <input type="hidden" name="idusulan[]" id="idusulan[]" value="' + $("#usulan").val() + '">' + "\n" +
					'       <input type="hidden" name="hargausulan[]" id="hargausulan[]" value="' + $("#hrg").val() + '">' + "\n" +
					'		<input type="hidden" name="qtypengadaan[]" id="qtypengadaan[]" value="' + $("#qty").val() + '">' + "\n" +
					'		<img src="images/delete.png" width="16" title="Hapus order" onClick="delDetail(\''+numdet+'\')" class="link"/>'  + "\n" +     
					'	</td>'  + "\n" +
					'</tr>'  + "\n";
			$("#tr_add").before(baris);
			$("#usulan").val('');
			$("#tgl").val('');
			$("#jdl").val('');
			$("#pengarang").val('');
			$("#hrg").val('');
			$("#qty").val('');
			$("#idorder").val('');
			$("#aktivitas").val('');
			hitTotal();
		}
	}
}

function delDetail(row){
	$("#numdetail").val(parseInt($("#numdetail").val())-1);  //decrement
	$("#tr_detail"+row).remove(); //hapus row
	hitTotal();
}

function showSatKerPU() {
	win = window.open("<?= Helper::navAddress('pop_satker.php'); ?>&nama=namasatker&id=idunitaktivitas","popup_satker","width=450,height=400,scrollbars=1");
	win.focus();
}

function deleteSatKer() {
	document.getElementById("namasatker").value = "";
	document.getElementById("idunitaktivitas").value = "";
}

function editRow(id){
	$("#rkey").val(id);
	$("#act").val("sunting");
	goSubmit();
}

function 0(id){
	$("#rkey").val(id);
	$("#act").val("savedetail");
	goSubmit();
}

function delRow(id){
	$("#rkey").val(id);
	$("#act").val("hapusdetail");
	goSubmit();
}

function goProses(id){
	$("#rkey").val(id);
	$("#act").val("proses");
	goSubmit();
}


function hitTotal(){
	var total = 0;
	var totprice = 0;
	$("input[name^=qtypengadaan]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		total += qty;
	});
	
	$("input[name^=hargausulan]").each(function(i){
		var hrg = parseInt($("input[name^=hargausulan]").eq(i).val());
		totprice += hrg;	
	});
	
	total += <?= $qty != '' ? $qty : 0; ?>;
	totprice += <?= $hrg != '' ? $hrg : 0;; ?>;
	$("#totqty").html(total);
	$("#totprice").html(totprice);
}
</script>
</html>