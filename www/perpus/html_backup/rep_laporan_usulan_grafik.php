<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pengusulan Pustaka Grafik';

	$p_filerep = 'repp_laporan_usulan_grafik';
	$p_tbwidth = 420;
	
	// combo box
	//$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	//$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
		
	//$a_jenis = array('x' => 'Biasa', '0' => 'Unit Kerja');
	//$l_jenis = UI::createSelect('jenis',$a_jenis,'','ControlStyle',true,'style="width:150"');
        
        $a_kategori = array('vote' => 'Vote', 'harga'=>'Harga');
        $l_kategori = UI::createSelect('kategori',$a_kategori,'','ControlStyle',true,'onChange="goTampil()"');
	
	$a_teratas = array('3' => '3 Teratas', '5'=>'5 Teratas', '7'=>'7 Teratas');
        $l_teratas = UI::createSelect('teratas',$a_teratas,'','ControlStyle',true);
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan Pengusulan Pustaka Biasa (Grafik)</td></tr>
	<!-- <tr>
		<td class="LeftColumnBG">Jenis Usulan</td>
		<td><?= $l_jenis ?></td>
	</tr>

	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>-->
	
	<tr> 
		<td class="LeftColumnBG" width="120">Periode</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		</td>
	</tr>
	
        <tr>
		<td class="LeftColumnBG">Kategori Grafik</td>
		<td><?= $l_kategori ?> <?= $l_teratas ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
		goSubmit();
}

function goTampil(){
	if($("#kategori").val() == 'vote')
		$("#teratas").show();
	if($("#kategori").val() == 'harga')
		$("#teratas").hide();
}
</script>
</html>