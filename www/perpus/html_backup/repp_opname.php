<?php
	$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_kdkondisi = Helper::removeSpecial($_POST['kdkondisi']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	if($r_kdkondisi!='')
		$kondisi = $conn->GetOne("select namakondisi from lv_kondisi where kdkondisi = '$r_kdkondisi' ");
	else
		$kondisi = '';
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Opname Pustaka '.$kondisi;
	
	$p_namafile = 'laporan_opname'.$r_tgl1.'_'.$r_tgl2.'_'.$kondisi;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select d.idopname,o.namaopname, count(*) as jumlah ";
	$sql .= " from pp_detailopname d ";
	$sql .= " join pp_opname o on d.idopname=o.idopname ";
	$sql .= " join pp_eksemplar e on e.ideksemplar=d.ideksemplar ";
	$sql .= " left join lv_lokasi l on e.kdlokasi=l.kdlokasi ";
	$sql .= " where 1=1 $sqlAdminUnit ";
	if($r_kdkondisi != "")
		$sql .= " and d.kondisiopname = '$r_kdkondisi' ";
		
	if($r_lokasi != "")
		$sql .= " and e.kdlokasi = '$r_lokasi' ";
		
	$sql .= " and d.tglopname between '$r_tgl1' and '$r_tgl2' ";
	$sql .= " group by d.idopname,o.namaopname ";
	$sql .= " order by d.idopname "; 
	
	$p_detop=$conn->Execute($sql);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Opname Pustaka <?php echo ($kondisi?'-'.$kondisi:'')?></h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">
	<?
	$i=0;
	while($rowd=$p_detop->FetchRow())
	{
	?>
	<tr height=25>
		<th colspan=6 align="left"><b>Nama Opname : <?= $rowd['namaopname'] ?></b></td>
	</tr>
  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Id Eksemplar</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
	<th width="100" align="center"><strong>Tanggal Opname</strong></th>
	<th width="100" align="center"><strong>Kondisi Awal</strong></th>
	<th width="100" align="center"><strong>Kondisi Opname</strong></th>
  </tr>
  <?php
	$sql = "select o.*, k.namakondisi as kondisiawal, ko.namakondisi as kondisiopname, p.judul, e.noseri from pp_detailopname o 
			join pp_eksemplar e on o.ideksemplar=e.ideksemplar
			join ms_pustaka p on e.idpustaka=p.idpustaka
			join lv_kondisi k on o.kondisiawal=k.kdkondisi
			join lv_kondisi ko on o.kondisiopname=ko.kdkondisi
			where o.idopname='".$rowd['idopname']."' and o.tglopname between '$r_tgl1' and '$r_tgl2' ";
	if($r_kdkondisi != "")
		$sql .= " and o.kondisiopname = '$r_kdkondisi' ";
		
	$sql .= " order by o.idopname ";
			
	$row = $conn->Execute($sql);
	
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= $rs['noseri'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglopname']) ?></td>
	<td align="center"><?= $rs['kondisiawal'] ?></td>
	<td align="center"><?= $rs['kondisiopname'] ?></td>	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada Opname</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=6><b>Jumlah Opname : <?= $rowd['jumlah'] ?></b></td></tr>
   <? $i++; } ?>
   <? if($i==0){ ?>
   <tr>
		<td align="center">Tidak ada Opname</td>
	</tr>
	<? } ?>
</table>


</div>
</body>
</html>