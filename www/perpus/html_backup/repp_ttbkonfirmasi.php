<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Konfirmasi Pemesanan/Kedatangan Periode';
	
	$p_namafile = 'rekap_konfirmasi_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql = "select ot.*,u.namapengusul,t.*,o.*,s.namasupplier,p.tglpo from pp_orderpustakattb ot
			join pp_ttb t on ot.idttb=t.idttb and t.jnsttb not in(2,3)
			join pp_orderpustaka o on ot.idorderpustaka=o.idorderpustaka
			join pp_usul u on o.idusulan=u.idusulan
			join pp_po p on o.idpo=p.idpo
			left join ms_supplier s on o.supplierdipilih=s.kdsupplier
			where t.tglttb between '$r_tgl1' and '$r_tgl2' ";
	
	if($r_jenis==0)
	$sql .=" and u.isdenganpagu=0 and u.idunit is null ";
	elseif($r_jenis==1)
	$sql .=" and u.isdenganpagu=0 and u.idunit is not null ";
	elseif($r_jenis==2)
	$sql .=" and u.isdenganpagu=1 and u.idunit is null ";
	
	$sql .="  order by t.tglttb";
	$row = $conn->Execute($sql);
	$rsj = $row->RowCount();
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=900>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=900 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Konfirmasi Pemesanan/Kedatangan</h2>
  	</strong></td>
  </tr>
    <tr>
	<td>Periode : <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="900" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
    <th width="130" align="center"><strong>Pengarang</strong></th>
	<th width="110" align="center"><strong>Pemesan</strong></th>
	<th width="150" align="center"><strong>Supplier</strong></th>
	<th width="100" align="center"><strong>Tanggal Pesan</strong></th>
	<th width="100" align="center"><strong>Tanggal Datang</strong></th>

  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>   
	<td align="left"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?></td>
	<td align="left"><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
	<td align="left"><?= $rs['namapengusul'] ?></td>
	<td align="left"><?= $rs['namasupplier'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglpo'])?></td>
	<td align="center"><?= Helper::tglEng($rs['tglttb']) ?></td>

  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah Buku: <?= $rsj ?><b></td></tr>
</table>


</div>
</body>
</html>