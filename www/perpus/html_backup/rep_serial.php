<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Eksemplar';

	$p_filerep = 'repp_serialp';
	$p_filerep2 = 'repp_seriale';
	$p_tbwidth = 420;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');

	$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by namaperolehan");
	$l_asal = $rs_cb->GetMenu2('kdperolehan','',true,false,0,'id="kdperolehan" class="ControlStyle" style="width:187"');
	$l_asal = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_asal);
	
	$rsj = $conn->Execute("select * from lv_jenispustaka where islokal=0 order by namajenispustaka");
	
	$rs_cb = $conn->Execute("select kdfakultas ||'-'|| namajurusan, rtrim(kdjurusan) from lv_jurusan order by kdfakultas");
	$l_jurusan = $rs_cb->GetMenu2('kdjurusan',$row['kdjurusan'],true,false,0,'id="kdjurusan" class="ControlStyle" style="width:250"');
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);
	?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Data Serial</td></tr>
	<tr>
		<td class="LeftColumnBG">Jenis Pustaka</td>
		<td>
			<table>
			<tr>
			<? $i=0; while($row = $rsj->FetchRow()){ $i++?>
				<? if($i%2==1) echo "<tr>" ?>
				<td width="150">
				<label>
				  <input type="checkbox" name="jenis" value="<?= $row['kdjenispustaka'] ?>" />
				<?= $row['namajenispustaka'] ?></label>
				</td>
				<? if($i%2==2) echo "</tr>"; ?>
			<? } ?>
			<tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Judul Pustaka</td>
		<td><input type="text" id='idpustaka' name='idpustaka' maxlength="30" onChange="goChange()">&nbsp;
		<img src="images/popup.png" title="Pilih judul" style="cursor:pointer" onClick="popup('index.php?page=pop_title',550,550)">
		<br><input type="text" name="judul" id="judul" class="ControlBlank" style="width:250px;color:gray" readonly />
		
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Asal Perolehan</td>
		<td><?= $l_asal ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Jurusan</td>
		<td class="RightColumnBG"><?= $l_jurusan ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="120">Tanggal</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2,subyek")){
		var act = document.getElementById('idpustaka').value;
		if(act!='')
			document.perpusform.action='<?= Helper::navAddress($p_filerep) ?>';
		else
			document.perpusform.action='<?= Helper::navAddress($p_filerep2) ?>';
			
		goSubmit();
		
	}
}

function goChange(){
	if(document.getElementById('idpustaka').value=='')
		document.getElementById('judul').value=''

}

</script>
</html>