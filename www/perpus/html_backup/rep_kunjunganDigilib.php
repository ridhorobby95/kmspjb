<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and kdsatker in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Kunjungan Digilib';
	$p_filerep = 'repp_kunjunganDigilib';
	
	$p_tbwidth = 100;
		
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_tahun = $conn->Execute("select distinct to_char(tanggal, 'YYYY') tahun, to_char(tanggal, 'YYYY') thn from pp_bukutamu order by tahun asc");
	$l_tahun = $rs_tahun->GetMenu2('tahun',date('Y'),true,false,0,'id="tahun" class="ControlStyle" style="width:140"');
	
	$a_jenislap = array('0' => 'Detil Laporan', '1' => 'Rekap Laporan');
	$l_jenislap = UI::createSelect('jenislap',$a_jenislap,'','ControlStyle',true,'style="width:155" id="jenislap" onchange="goJenisLap()"');
	
	$rs_cb = $conn->Execute("select namasatker, kdsatker from ms_satker where 1=1 $sqlAdminUnit order by namasatker");
	$l_lokasi = $rs_cb->GetMenu2('kdunit','',true,false,0,'id="kdunit" class="ControlStyle" style="width:187"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Kunjungan Digilib</h1>
				</div>
			</div>
		</header>
        <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr id="tr_tahun">
				<td class="LeftColumnBG thLeft">Jenis Laporan</td>
				<td class="RightColumnBG"><?= $l_jenislap; ?> </td>
			</tr>
			<tr id="tr_tahun">
				<td class="LeftColumnBG thLeft">Tahun</td>
				<td class="RightColumnBG"><?= $l_tahun; ?> </td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Unit Pengunjung</td>
				<td><?= $l_lokasi ?></td>
			</tr>
			<tr id="tr_format">
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		* untuk format Word dan Excel tidak akan ikut ditampilkan grafik *
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
function goPreSubmit() {
	if(cfHighlight("tahun"))
		goSubmit();
}

</script>
</html>