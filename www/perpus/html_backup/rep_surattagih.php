<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Surat Tagih';

	$p_filerep = 'repp_surattagih';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi where 1=1 $sqlAdminUnit order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('kdlokasi','',true,false,0,'id="kdlokasi" class="ControlStyle" style="width:187"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('tgl1').focus()">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Rekap Tagihan</h1>
				</div>
			</div>
		</header>
         <div class="table-responsive">   
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr> 
				<td class="LeftColumnBG thLeft" width="120">Tanggal</td>
				<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
				<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl1",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				s/d &nbsp;
				<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
				<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl2",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Lokasi Pustaka</td>
				<td><?= $l_lokasi ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
		</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2"))
		goSubmit();
}

</script>
</html>