<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengadaan.class.php');
	require_once('includes/query.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$r_key = Helper::removeSpecial($_REQUEST['key']);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Pengadaan';
	$p_title = '.: Pengadaan :.';
	$p_title1 = 'Data Pengadaan';
	$p_tvalidasi = 'Validasi Perpus';
	$p_tvalidasi2 = 'Validasi Keuangan';
	$p_titlelist = '.: Daftar Pengadaan :.';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_pengadaanperpus.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$recdetail = array();
			$record = array();
			$record['tglpengadaan'] = Helper::formatDate($_POST['tglpengadaan']);
			$record['thang'] = Helper::cStrNull($_POST['thang']);
			$record['keterangan'] = Helper::cStrNull($_POST['keterangan']);
			Helper::Identitas($record);
			
			$arrparam = array('idorderpustaka','qtypengadaan','hargausulan');
			$recdetail = Helper::getArrParam($arrparam,'idorderpustaka');
			
			if ($r_key == ''){
				$record['statuspengadaan'] = 'A';
				$record['nopengadaan']=Helper::removeSpecial($_POST['nopeng']);
				$rs_cek = $conn->GetRow("select nopengadaan from pp_pengadaan where nopengadaan=".$_POST['nopeng']);
				if(!$rs_cek)
					$err = Pengadaan::insertPengadaan($conn,$record,$recdetail);		
				else{
					$errdb = 'No Pengadaan Telah Digunakan, Refresh dan Masukan Kembali Usulan.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}else{
				$err = Pengadaan::updatePengadaan($conn,$record,$recdetail,$r_key);
			}
			if($err[0] == 0) {
				$r_key = $err[1];
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
			}
			else{
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'savedetail' and $c_edit) {	
			$record = array();
			$record['hargausulan'] = Helper::cStrNull($_POST['u_hrg']);
			$record['qtypengadaan'] = Helper::cStrNull($_POST['u_qty']);
			$record['statusvalperpus'] = Helper::cStrNull($_POST['issetuju']);
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey and idpengadaan=$r_key");
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Update Detail Pengadaan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Update Detail Pengadaan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if ($r_aksi == 'hapus' and $c_delete){
			$record = array();
			$conn->StartTrans(); 
			// Hapus pp_orderpustaka berdsrkn id_pengadaan
			Query::qDelete($conn,'pp_paguusulan',"idorderpustaka=".$_POST['idorderp']);
			Query::qDelete($conn,'pp_orderpustaka',"idpengadaan=".$_POST['key']);
			Query::qDelete($conn,'pp_pengadaan',"idpengadaan=".$_POST['key']);
	
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan Pengadaan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan Pengadaan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if($r_aksi == 'hapusdetail' and $c_edit) {
			$record = array();
			$record['idpengadaan'] = 'null';
			Helper::Identitas($record);
			$conn->StartTrans(); 
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey and idpengadaan=$r_key");
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan Detail Pengadaan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan Detail Pengadaan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $rkey;
		}
		else if($r_aksi == 'proses' and $c_edit) {
			$record = array();
			$record['statuspengadaan'] = 'PP';
			Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
		}
		else if($r_aksi == 'setujui' and $c_edit) {
			$record = array();
			$record['statusvalperpus'] = '1';
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=".$_POST['idorderp']." and idpengadaan=$r_key");
		}
		else if($r_aksi == 'batalkan' and $c_edit) {
			$record = array();
			$record['statusvalperpus'] = '0';
			Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=".$_POST['idorderp']." and idpengadaan=$r_key");
		}
	}
	
  	if ($r_key == ''){
		$regcomp = Query::getCount($conn, 'idpengadaan', 'pp_pengadaan');
		$regcomp = $regcomp == '' ? str_pad(1,4,'0',STR_PAD_LEFT).'/'.Helper::bulanRomawi(date('d-m-Y')).'/'.date('Y') : str_pad($regcomp+1,4,'0',STR_PAD_LEFT).'/'.Helper::bulanRomawi(date('d-m-Y')).'/'.date('Y');
	}else{
		$sql = "select pp.*,po.idorderpustaka from pp_pengadaan pp left join pp_orderpustaka po on po.idpengadaan=pp.idpengadaan
				where pp.idpengadaan=$r_key";	
		$row = $conn->GetRow($sql);
		$regcomp = $row['nopengadaan'];
				
		// sql untuk mendapatkan isi list
		
		// if (trim($row['statuspengadaan']) == 'S')
			// $p_sqlstr="select *,s1.namasupplier as supp1,s2.namasupplier as supp2,s3.namasupplier as supp3  
					// from pp_orderpustaka op 
					// left join pp_usul u on u.idusulan=op.idusulan 
					// left join ms_supplier s1 on s1.kdsupplier=op.namasupplier1::integer
					// left join ms_supplier s2 on s2.kdsupplier=op.namasupplier2::integer
					// left join ms_supplier s3 on s3.kdsupplier=op.namasupplier3::integer
				   // where idpengadaan = $row[idpengadaan] order by tglusulan, idorderpustaka";
		// else
			$p_sqlstr="select * from pp_orderpustaka op
					left join pp_usul u on u.idusulan=op.idusulan
				   where idpengadaan = $r_key order by tglusulan";
				   
			$p_sqlstr2="select count(*) as jml from pp_orderpustaka op
					left join pp_usul u on u.idusulan=op.idusulan
				   where idpengadaan = $r_key ";
		$rs = $conn->Execute($p_sqlstr);
		$rs2 = $conn->GetRow($p_sqlstr2);
		$isItemEdit = true;  	
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<table  border="0" cellpadding="5" cellspacing="0">
			<!--tr height="20">
				<td align="center" colspan=7 class="PageTitle"><?= $p_title; ?></td>
			</tr-->
			<tr>
			<td align="center" style="text-align:center;">
				<a href="<?= $p_filelist ?>" class="buttonshort"><span title="Kembali Ke Daftar Pengadaan" class="list">Daftar</span></a>
			</td>
			<? if ($c_edit){
			if($r_key == '' or ((trim($row['statuspengadaan']) != 'S' and trim($row['statuspengadaan']) == 'A'))) { ?>
				<td align="center">
					<a href="javascript:saveData();" class="buttonshort"><span title="Simpan Pengadaan Beserta Detail Usulan" class="save">Simpan</span></a>
				</td>
				<? if (trim($row['statuspengadaan']) == 'A' and $rs2['jml']>0){?>
				<td align="center">
					<a href="javascript:goProses('<?= $row['idorderpustaka']?>');" class="buttonshort"><span title="Mengubah Status Menjadi 'Selesai (S)'" class="validasi">Proses</span></a>
				</td>
			<?}} if($c_delete and $r_key != '') { ?>
			<td align="center">
				<a href="javascript:goDeletePengadaan('<?= $row['idorderpustaka']?>');" class="buttonshort"><span title="Hapus Pengadaan Beserta Detail Usulan"  class="delete">Hapus</span></a>
			</td>
			<td>
				<a href="index.php?page=repp_listpengadaan&idpengadaan=<?= $r_key ?>" target="_blank" class="buttonshort"><span class="print">Cetak</span></a>
			</td>
			<? }} ?>
			</tr>
			<tr>
				<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
			</tr>
		</table>
		<br />
		<header style="width:650px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table cellpadding="0" cellspacing="0" width="<?= $p_tbwidth+50 ?>" border="0" class="GridStyle">
			<tr>
				<td style="text-align:center;font-size:14px;" class="thLeft" style="border:0 none;" align="center" class="SubHeaderBGAlt" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1; ?></td>
			</tr>
			<tr height="30">
				<td width="150" class="LeftColumnBG thLeft">No. Pengadaan</td>
				<td class="RightColumnBG" colspan="2"><?=  $regcomp; ?>&nbsp;&nbsp;&nbsp;&nbsp;<em>[Digenerate Oleh System]</em>
				<input type="hidden" name="nopeng" id="nopeng" value="<?= $regcomp ?>"></td>
			</tr>
			<tr height="30">
				<td width="150" class="LeftColumnBG thLeft">Tgl. Pengadaan</td>
				<td class="RightColumnBG" colspan="2"><input type="text" name="tglpengadaan" size="10" id="tglpengadaan" value="<?= $row['tglpengadaan']=='' ? date('d-m-Y') : Helper::formatDate($row['tglpengadaan']) ?>">
					<? if($c_edit) { ?>
					<img src="images/cal.png" id="tglada" style="cursor:pointer;" title="Pilih tanggal pengadaan">
					&nbsp;
					<script type="text/javascript">
					Calendar.setup({
						inputField     :    "tglpengadaan",
						ifFormat       :    "%d-%m-%Y",
						button         :    "tglada",
						align          :    "Br",
						singleClick    :    true
					});
					</script>
					
					<? } ?>
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jumlah</td>
				<td class="RightColumnBG" colspan="2"><div id="totqty">0</div></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Total Pengadaan</td>
				<td class="RightColumnBG" colspan="2"><div id="totprice">0</div></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Status Pengadaan</td>
				<td class="RightColumnBG" colspan="2"><?= Helper::getArrStatusP(trim($row['statuspengadaan']))?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Keterangan</td>
				<td class="RightColumnBG" colspan="2"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',3,60,$c_edit); ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<header style="margin:0 auto; width:100%">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_titlelist ?></h1>
				</div>
			</div>
		</header>
		<!--table cellpadding="4" cellspacing="0" width="<?//= p_tbwidth; ?>">
			<tr>
				<td align="center" class="PageTitle"><?//= $p_titlelist; ?></td>
			</tr>
		</table-->
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="GridStyle">
			<tr>
				<td class="SubHeaderBGAlt thLeft" colspan="2" align="center" width="10%">Id Usulan</td>
				<td width="70" class="SubHeaderBGAlt thLeft" align="center">Tgl Usulan</td>
				<td width="170" class="SubHeaderBGAlt thLeft" align="center" nowrap>Judul</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Pengarang</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Pengusul</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Harga</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Qty</td>
				<? if(trim($row['statuspengadaan']) == 'PP' or trim($row['statuspengadaan']) == 'S'){?>
					<td class="SubHeaderBGAlt thLeft" align="center">Validasi Perpustakaan</td>
				<? }if(trim($row['statuspengadaan']) == 'A'){?>
					<td width="50" class="SubHeaderBGAlt thLeft" align="center">Aksi</td>
				<? }?>

			</tr>
			<? if (!empty($r_key)){ 
					$i=0;
					while ($rows = $rs->FetchRow()){
						$z=0;
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
						$hrg += $rows['hargausulan'];
						$qty += $rows['qtypengadaan'];
						if (trim($row['statuspengadaan']) == 'S')
							if (empty($rows['supp1'])) $z=0; else if (empty($rows['supp2'])) $z=2; else if (empty($rows['supp3'])) $z=3; else $z=3;
						
						if(strcasecmp($rows['idorderpustaka'],$p_editkey)) {
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td colspan="2" align="left"><?= $rows['idusulan']; ?></td>
				<td align="left"><?= Helper::formatDate($rows['tglusulan']); ?></td>
				<td align="left"><?= $rows['judul']; ?></td>
				<td align="left"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
				<td><?= $rows['namapengusul']; ?></td>
				<td align="right"><?= $rows['hargausulan']; ?></td>
				<td align="right"><?= $rows['qtypengadaan']; ?></td>
				<?if(trim($row['statuspengadaan']) == 'PP' or trim($row['statuspengadaan']) == 'S'){?>
					<td nowrap align="center" class="RightColumnBG"><?= $rows['statusvalperpus'] == 1 ? '<strong>Disetujui</strong>' : ($rows['statusvalperpus'] == '0' ? '<strong>Ditolak</strong>' : '<strong>Belum</strong>') ; ?></td>
				<?}if(trim($row['statuspengadaan']) == 'A'){?>
					<td align="center"><img src="images/tombol/edited.gif" width="16" title="Edit Detail" onClick="editRow('<?= $rows['idorderpustaka'] ?>')" class="link"/>&nbsp;&nbsp;<img src="images/delete.png" width="16" title="Hapus Detail" onClick="delRow('<?= $rows['idorderpustaka']?>')" class="link"/></td>
				<?}?>
			</tr>
			<? if(trim($row['statuspengadaan']) == 'S') { ?>
			<!--<tr id="tr_item<?= $i; ?>" style="display:<?= $isItemEdit?"":"none"; ?>">
				<td>&nbsp;</td>
				<td colspan="10">
					<table cellpadding="4" cellspacing="0" width="100%" style="border-collapse:collapse" border="1">
						<tr>
							<td class="SubHeaderBGAlt" align="center">Supplier</td>
							<td class="SubHeaderBGAlt" align="center">Harga</td>
							<td class="SubHeaderBGAlt" align="center">Alasan Dipilih</td>
						</tr>
						<? if ($z != 0 ){
							for($y=1;$y<=$z;$y++){?>
						<tr  valign="top">
							<? if (!empty($rows['supp'.$y])){ 
								if (strcasecmp(trim($rows['supplierdipilih']),trim($rows['namasupplier'.$y]))) 
									$tdstyle = 'NormalBG';  
								else 
									$tdstyle = 'AlternateBG';
							?>
								<td class="<?= $tdstyle; ?>"><?= $rows['supp'.$y] ?></td>
								<td class="<?= $tdstyle; ?>"><?= $rows['hargasupplier'.$y] ?></td>
								<td class="<?= $tdstyle; ?>"><?= strcasecmp(trim($rows['supplierdipilih']),trim($rows['namasupplier'.$y])) ? '' : $rows['alasandipilih']; ?></td>
							<? } ?>
						</tr>
						<? }
						}else{ ?>
						<tr valign="top">
							<td colspan="3" align="center">Data Tidak Ditemukan</td>
						</tr>
						<? } ?>
					</table>
				</td>
			</tr> -->
			<? 	}}else{ ?>
			<tr class="<?= $rowstyle ?>"> 
				<td colspan="2" align="center"><?= $rows['idusulan']; ?></td>
				<td align="center"><?= Helper::formatDate($rows['tglusulan']); ?></td>
				<td align="center"><?= $rows['judul']; ?></td>
				<td align="center"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
				<td><?= $rows['namapengusul']; ?></td>
				<td><?= UI::createTextBox('u_hrg',$rows['hargausulan'],'ControlStyle',10,10,$c_edit); ?></td>
				<td align="right"><?= UI::createTextBox('u_qty',$rows['qtypengadaan'],'ControlStyle',3,3,$c_edit); ?></td>
				<?if((trim($row['statuspengadaan']) == 'PP' or trim($row['statuspengadaan']) == 'S') and $_SESSION['PERPUS_USER'] == 'admin'){?>
					<td align="center"><?= $rows['statusvalperpus'] == 1 ? '<strong>Disetujui</strong>' : ($rows['statusvalperpus'] == '0' ? '<strong>Ditolak</strong>' : '<strong>Belum</strong>') ; ?></td>
				<?}?>
				<?if(trim($row['statuspengadaan']) == 'A'){?>
					<td align="center"><img src="images/tombol/file.png" width="16" title="Save Detail" onClick="saveRow('<?= $rows['idorderpustaka'] ?>')" class="link"/>&nbsp;&nbsp;<img src="images/delete.png" width="16" title="Hapus Detail" onClick="delRow('<?= $rows['idorderpustaka']?>')" class="link"/></td>
				<?}?>
			</tr>
			<? }}} if ((trim($row['statuspengadaan']) != 'PP' and trim($row['statuspengadaan']) == 'A') or $r_key == '') {?>
			<tr id="tr_add">
				<td><?= UI::createTextBox('usulan','','ControlRead',10,10,$c_edit,'readonly'); ?><input type="hidden" name="idorder" id="idorder"></td>
				<td><img src="images/tombol/breakdown.png" id="btnusulan" title="Cari Usulan" style="cursor:pointer" onClick="openLOV('btnusulan', 'usulan',-100, 20,'addUsulan',800)"></td>
				<td><?= UI::createTextBox('tgl','','ControlRead',10,10,$c_edit,'readonly'); ?></td>
				<td><?= UI::createTextBox('jdl','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
				<td><?= UI::createTextBox('pengarang','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
				<td><?= UI::createTextBox('pengusul','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
				<td><?= UI::createTextBox('hrg','','ControlStyle',10,10,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
				<td><?= UI::createTextBox('qty','','ControlStyle',3,3,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
				
				<td><input type="button" name="tambah" id="tambah" value="Tambah" onClick="addUsul();"></td>
				<? //if (trim($row['statuspengadaan']) == 'S' and $_SESSION['PERPUS_USER']=='admin'){?>
					<!--<td nowrap>
						<input type="radio" name="issetuju" id="issetuju1" value="1" >Ya <!--onclick="goSetujui()"
						<input type="radio" name="issetuju" id="issetuju0" value="0" >Tidak <!-- onclick="goBatalkan()
					</td>-->
				<?//}?>
			</tr>
			<? } ?>
						<tr>
							<td colspan="9" class="footBG">&nbsp;</td>
						</tr>
		</table>
		<br>
		<? if(trim($row['statuspengadaan']) == 'S') { ?>
		<!--<table width="900" cellpadding="4" cellspacing="0" style="border-collapse:collapse;background:#FFCC66" border="0">
			<tr>
				<td><strong>Keterangan</strong></td>
			</tr>
			<tr>
				<td><strong>* Warna kuning pada supplier merupakan supllier yang dipilih</strong></td>
			</tr>
		</table> -->
		<? } ?>
		<input type="hidden" name="key" id="key" value="<?= $r_key; ?>">
		<input type="hidden" name="rkey" id="rkey">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="idorderp" id="idorderp">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

$(function(){
	hitTotal();
});

function goSetujui(idorderp){
	var setujui = confirm("Apakah anda yakin menyutujui data ini?");
	if(setujui) {
		document.getElementById("act").value = "setujui";
		document.getElementById("idorderp").value = idorderp;
		goSubmit();
	}
}

function goBatalkan(idorderp){
	var batalkan = confirm("Apakah anda yakin membatalkan pengadaan untuk data ini?");
	if(batalkan) {
		document.getElementById("act").value = "batalkan";
		document.getElementById("idorderp").value = idorderp;
		goSubmit();
	}
}

function goDeletePengadaan(idorderp) {
	var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
	if(hapus) {
		document.getElementById("act").value = "hapus";
		document.getElementById("idorderp").value = idorderp;
		goSubmit();
	}
}

function saveData(){
	if(cfHighlight("tglpengadaan")){
		$("#act").val("simpan");
		goSubmit();
	}
}

function onlyNumber(e,elem,dec) {
	var code = e.keyCode || e.which;
	if ((code > 57 && code < 96) || code > 105 || code == 32) {
		if(code == 190 && dec) {
			if(elem.value == "") // belum ada isinya, titik tidak boleh didepan
				return false;
			if(elem.value.indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
				return false;
			return true;
		}
		return false;
	}
}

function addUsulan(idusulan, tglusulan, judul, pengarang, pengusul, harga, qty, idorder) {
		$("#usulan").val(idusulan);
		$("#tgl").val(tglusulan);
		$("#jdl").val(judul);
		$("#pengarang").val(pengarang);
		$("#pengusul").val(pengusul);
		$("#hrg").val(harga);
		$("#qty").val(qty);
		$("#idorder").val(idorder);
}

function addAktivitas(kode,thang) {
		$("#kodeaktivitas").val(kode);
		$("#thang").val(thang);
}


function addUsul(val)
{
	var n = 0;
	if(cfHighlight("usulan,hrg,qty")){
		$("input[name^=idorderpustaka]").each(function(i){
			if(parseInt($("#idorder").val()) == parseInt($("input[name^=idorderpustaka]").eq(i).val())){
				$(this).addClass("ControlErr");
				n++;
			}else{
				$(this).removeClass("ControlErr");
			}
		});
		if(n > 0){
			alert("Detail Pengadaan Sudah Ada");
		}else{
			var baris = '';		
			var numdet = 1;
			var tmp = 'te';
			var namabarang = 's';
			numdet += 1;
			
			baris = '<tr valign="top" id="tr_detail'+numdet+'">'  + "\n" +
					'	<td align="center" colspan="2">' + $("#usulan").val() + '</td>'  + "\n" +
					'	<td align="center">' + $("#tgl").val() + '</td>'  + "\n" +
					'	<td>' + $("#jdl").val() + '</td>'  + "\n" +
					'	<td>' + $("#pengarang").val() + '</td>'  + "\n" +
					'	<td>' + $("#pengusul").val() + '</td>'  + "\n" +
					'	<td align="right">' + $("#hrg").val() + '</td>'  + "\n" +
					'	<td align="right">' + $("#qty").val() + '</td>'  + "\n" +
					'	<td align="center">'  + "\n" +
					'		<input type="hidden" name="idorderpustaka[]" id="idorderpustaka[]" value="' + $("#idorder").val() + '">' + "\n" +
					'       <input type="hidden" name="idusulan[]" id="idusulan[]" value="' + $("#usulan").val() + '">' + "\n" +
					'       <input type="hidden" name="hargausulan[]" id="hargausulan[]" value="' + $("#hrg").val() + '">' + "\n" +
					'		<input type="hidden" name="qtypengadaan[]" id="qtypengadaan[]" value="' + $("#qty").val() + '">' + "\n" +
					'		<img src="images/delete.png" width="16" title="Hapus order" onClick="delDetail(\''+numdet+'\')" class="link"/>'  + "\n" +     
					'	</td>'  + "\n" +
					'</tr>'  + "\n";
			$("#tr_add").before(baris);
			$("#usulan").val('');
			$("#tgl").val('');
			$("#jdl").val('');
			$("#pengusul").val('');
			$("#pengarang").val('');
			$("#hrg").val('');
			$("#qty").val('');
			$("#idorder").val('');
			$("#aktivitas").val('');
			hitTotal();
		}
	}
}

function delDetail(row){
	$("#numdetail").val(parseInt($("#numdetail").val())-1);  //decrement
	$("#tr_detail"+row).remove(); //hapus row
	hitTotal();
}

function showSatKerPU() {
	win = window.open("<?= Helper::navAddress('pop_satker.php'); ?>&nama=namasatker&id=idunitaktivitas","popup_satker","width=450,height=400,scrollbars=1");
	win.focus();
}

function deleteSatKer() {
	document.getElementById("namasatker").value = "";
	document.getElementById("idunitaktivitas").value = "";
}

function editRow(id){
	$("#rkey").val(id);
	$("#act").val("sunting");
	goSubmit();
}

function saveRow(id){
	$("#rkey").val(id);
	$("#act").val("savedetail");
	goSubmit();
}

function delRow(id){
	$("#rkey").val(id);
	$("#act").val("hapusdetail");
	goSubmit();
}

function goProses(idorderp){
	$("#idorderp").val(idorderp);
	$("#act").val("proses");
	goSubmit();
}


function hitTotal(){
	var total = 0;
	var totprice = 0;
	$("input[name^=qtypengadaan]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		total += qty;
	});
	
	$("input[name^=hargausulan]").each(function(i){
		var hrg = parseInt($("input[name^=hargausulan]").eq(i).val());
		totprice += hrg;	
	});
	
	total += <?= $qty != '' ? $qty : 0; ?>;
	totprice += <?= $hrg != '' ? $hrg : 0;; ?>;
	$("#totqty").html(total);
	$("#totprice").html(totprice);
}

function showSupp(img,id){
	if($(img).attr("src") == "images/tree/add.png"){
		$(img).attr("src","images/tree/minus.png")
	
		$("#tr_item"+id).show();
		
	}else{
		$(img).attr("src","images/tree/add.png")
		$("#tr_item"+id).hide();
	}

}
</script>
</html>