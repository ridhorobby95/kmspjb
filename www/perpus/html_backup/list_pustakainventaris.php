<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	$r_key = Helper::removeSpecial(trim($_REQUEST['key']));
		
	// definisi variabel halaman
	$p_dbtable = 'ms_pustaka';
	$p_window = '[PJB LIBRARY] Daftar Pustaka yang sesuai';
	$p_title = 'Daftar Pustaka Yang Sudah Ada';
	$p_tbheader = '.: Daftar Pustaka  Yang Sudah Ada :.';
	$p_col = 6;
	$p_tbwidth = 800;
	$p_filelist = Helper::navAddress('list_inventaris.php');
	$p_id = "pinventaris";
	
	
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$rows = $conn->GetRow("select op.*, ot.*, t.jnsttb
			      from pp_orderpustaka op
			      left join pp_orderpustakattb ot on ot.idorderpustaka=op.idorderpustaka
			      left join pp_ttb t on t.idttb=ot.idttb
			      where idorderpustakattb=$r_key");
	
	if($rows['jnsttb']!=3){
		$p_filedetail = Helper::navAddress('data_inventaris.php');		
	}else{
		$p_filedetail = Helper::navAddress('data_inventarista.php');	
	}
	
	$r_keyword = Helper::removeSpecial($rows['judul']);
	if($rows['authorfirst1']!='' and $rows['authorlast1']!='')
		$r_pengarang = " or authorfirst1||' '||authorlast1 like '%".$rows['authorfirst1']." ".$rows['authorlast1']."%' ";
	else
		$r_pengarang = '';
	if($rows['namapenerbit']!='')
		$r_penerbit = " or namapenerbit like '%".$rows['namapenerbit']."%' ";
	else
		$r_penerbit = '';
	
	$p_defsort = "idpustaka asc";
	
	$p_sqlstr = "select p.*,j.namajenispustaka from $p_dbtable p join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
				 where 1=1 ";
							

	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial(trim($_POST['carijudul']));
		if($keyjudul!=''){
			$p_sqlstr.=" and to_char(judul)||' '||coalesce(judulseri,'') like '%$keyjudul%'  ";	
		}else
			$p_sqlstr .=" and to_char(judul)||' '||coalesce(judulseri,'') like '%".$rows['judul']."%' {$r_pengarang} {$r_penerbit} ";
		
		$p_page 	= Helper::removeSpecial($_REQUEST['numpage']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$row2 = $conn->GetRow("select * from pp_orderpustaka op left join pp_orderpustakattb ot on ot.idorderpustaka=op.idorderpustaka left join pp_ttb t on t.idttb=ot.idttb where idorderpustakattb=$r_key");
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	<table align="center" width="836px" cellpadding="0"  border="0" style="background:#E0FFF3;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
		<tr height="25">
			<th colspan="4">Data Inventaris</td>
		</tr>
		<tr height="10">
			<td>&nbsp;</td>
		</tr>
		<tr height="22">
			<td width=160 ><b>Judul</b></td>
			<td width="600">: <?= $row2['judul'] ?></td>
		</tr>
		<tr height="22">
			<td width=160 ><b>Pengarang</b></td>
			<td width="600">: <?= $row2['authorfirst1']." ".$row2['authorlast1'] ?></td>
		</tr>
		<tr height="22">
			<td width=160 ><b>Penerbit</b></td>
			<td width="600">: <?= $row2['penerbit'] ?></td>
		</tr>
		<tr height="22">
			<td width=160 ><b>No. TTb</b></td>
			<td width="220">: <?= $row2['nottb'] ?></td>
		</tr>
		<tr height="22">
			<td ><b>Tanggal TTP </b></td>
			<td width="300"> : <b><?= Helper::tglEngTime($row2['tglttb'],false) ?></b></td>
		</tr>
	</table><br />
	<div class="filterTable">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="150">Judul</td>
					<td colspan="3">:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $_POST['carijudul'] ?>" onKeyDown="etrCari(event);"></td> 
					<td  align="right"><input type="button" value="Filter" class="ControlStyle" onClick="goFilt()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onClick="goClear();goFilter(false);" /></td>
				</tr>
			</table>
	</div>
	<br />
	<header style="width:836px;margin:0 auto;">
		<div class="inner">
			<div class="left title">
				<h1><?= $p_tbheader ?></h1>
			</div>
			<div class="right">
			  <?	if($c_edit) { ?>
			  <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data" onClick="javascript:void(0)" id="litacreate">+</div>
			  <?	} ?>
			</div>
		</div>
	</header>
	<table width="836px" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
	<tr height="20"> 
		<td width="10%" align="center" class="SubHeaderBGAlt thLeft">Eksemplar</td>
		<td width="35%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul  <?= $p_xsort['judul']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','nopanggil:C');">No. Panggil <?= $p_xsort['nopanggil']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;">Pengarang</td>				
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','edisi:C');">Penerbit<?= $p_xsort['namapenerbit']; ?></td>
		<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','namajenispustaka:C');">Jenis Pustaka<?= $p_xsort['namajenispustaka']; ?></td>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
		<td align="center"><img src="images/tambah.png" onClick="goPostX('<?= $p_filedetail; ?>','key=<?= $r_key; ?>&rkey=<?= $row['idpustaka'] ?>');"  title="Tambah Eksemplar" style="cursor:pointer;"></td>
		<td><u onClick="goPostX('<?= $p_filedetail; ?>','key=<?= $r_key; ?>&rkey=<?= $row['idpustaka'] ?>');"  title="Tambah Eksemplar" class="link"><?= $row['judul'] . ' '. $row['judulseri']; ?></u></td>
		<td align="center"><?= $row['nopanggil']==''?'-':$row['nopanggil'] ?></td>
		<td><?php
			echo $row['authorfirst1']. " " .$row['authorlast1']; 
			if ($row['authorfirst2']) 
			{
				echo " <strong><em>,</em></strong> ";
				echo $row['authorfirst2']. " " .$row['authorlast2'];
			}
			if ($row['authorfirst3']) 
			{
				echo " <strong><em>,</em></strong> ";
				echo $row['authorfirst3']. " " .$row['authorlast3'];
			}
		?>
		</td>
		<td align="left"><?= $row['namapenerbit']."&nbsp;"; ?></td>
		<td align="left"><?= $row['namajenispustaka']."&nbsp;"; ?></td>
	</tr>
	<?php
		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
			<tr>
				<td style="background:#015593;color:#fff;" class="footBG" colspan="<?= $p_col; ?>" align="right">
					Halaman <?= $p_page ?> / <?= $p_lastpage ?>
				</td>
			</tr>
</table>
 <?php require_once('inc_listnav.php'); ?><br>
<input type="hidden" name="numpage" id="numpage" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="act" id="act">


<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/foreditx.js"></script>
<script type="text/javascript">


var phpself = "<?= $i_phpfile; ?>";

function etrCari(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		sent = "key=<?= $r_key; ?>&carijudul="+$("#carijudul").val();
		goPostX('<?= $i_phpfile ?>',sent);
	}
}

function goFilt(){
	sent = "key=<?= $r_key; ?>&carijudul="+$("#carijudul").val();
	goPostX('<?= $i_phpfile ?>',sent);
}


function goClear(){
	sent = "key=<?= $r_key; ?>";
	goPostX('<?= $i_phpfile ?>',sent);
}
$(function() {
	$("#litacreate").click(function() {
		var x="<?= $rows['jnsttb'] ?>";
		if(x!='3')
		goPostX('<?= Helper::navAddress('data_inventaris')?>', 'key=<?= $r_key; ?>');
		else
		goPostX('<?= Helper::navAddress('data_inventarista')?>', 'key=<?= $r_key; ?>');
	});
});
</script>

</html>