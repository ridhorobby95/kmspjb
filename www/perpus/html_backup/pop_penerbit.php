	<?php
		defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

		// pengecekan tipe session user
		$a_auth = Helper::checkRoleAuth($conng,false);

		// definisi variabel halaman
		$p_dbtable = 'ms_penerbit';
		$p_window = '[PJB LIBRARY] Daftar Penerbit';
		$p_title = 'Daftar Penerbit';
		$p_tbheader = '.: Daftar Penerbit:.';
		$p_col = 3;
		$p_tbwidth = 100;
		$p_id = "ms_penerb";
		$p_row = 10;
		
		$code=Helper::removeSpecial($_GET['code']);
		
		// sql untuk mendapatkan isi list
		$p_sqlstr = "select * from $p_dbtable where 1=1";
		// pengaturan ex
		if (!empty($_POST)) {
			$r_aksi = Helper::removeSpecial($_POST['act']);
			$r_key = Helper::removeSpecial($_POST['key']);
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
		//filtering
		$keypenerbit=Helper::removeSpecial($_POST['caripenerbit']);
		if($keypenerbit!='')
			$p_sqlstr.=" and upper(namapenerbit) like upper('%$keypenerbit%')";
		
		}
		else
		{
			// dapatkan nilai ex dari session
			if ($_SESSION[$p_id.'.page'])
				$p_page = $_SESSION[$p_id.'.page'];
		}
		if (!$p_page)
			$p_page = 1; // halaman default adalah 1
			
		// eksekusi sql list
	if($keypenerbit)
		$p_sqlstr .= " order by case when upper(namapenerbit) = upper('".$keypenerbit."') then 0 else 1 end, namapenerbit ";
	else
		$p_sqlstr .= " order by namapenerbit";
	
		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		if ($rs->EOF) {
			// tidak ditemukan record atau ada kesalahan
			$p_atfirst = true;
			$p_atlast = true;
			$p_lastpage = 0;
			$p_page = 0;
		}
		else {
			// ditemukan record
			$p_atfirst = $rs->AtFirstPage();
			$p_atlast = $rs->AtLastPage();
			$p_lastpage = $rs->LastPageNo();
			$showlist = true;
		}
	?>
	<html>
	<head>
		<title><?= $p_window ?></title>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<link href="style/style.css" type="text/css" rel="stylesheet">
		<link href="style/pager.css" type="text/css" rel="stylesheet">
		<link href="style/button.css" type="text/css" rel="stylesheet">
		
		<script type="text/javascript" src="scripts/forinplace.js"></script>
		<script type="text/javascript" src="scripts/forpager.js"></script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('caripenerbit').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
	<div id="wrapper" style="width:auto;margin:0;padding:0;">
		<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
			<div align="center">
			<form name="perpusform" id="perpusform" method="post" action="">
			<div class="filterTable">
				<table border="0"  cellpadding="0" cellspacing="0" width="100%">
					<tr>
					<td>
						Nama Penerbit : &nbsp;<input type="text" name="caripenerbit" id="caripenerbit" size="25" value="<?= $keypenerbit ?>" onKeyDown="etrCari(event);">
					</td>
					<td align="right" >
						<input type="button" value="Filter" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRefresh();goSubmit(false);" />
					</td>
					</tr>
				</table>
			</div><br />
			<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
			<? include_once('_notifikasi.php'); ?>
			<table width="100%" border="1" cellpadding="3" cellspacing=0 class="GridStyle">
				<tr height="20"> 
					<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Kode</td>
					<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Penerbit</td>
					<td width="60" nowrap align="center" class="SubHeaderBGAlt thLeft">Pilih</td>
				</tr>
				<?php
					$i = 0;
					while ($row = $rs->FetchRow()) 
					{
						if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					
				?>
				<tr class="<?= $rowstyle ?>">
					<td align="center">	
						<?= $row['idpenerbit']; ?>
					</td>
					<td>
					<? if($code=='u') { ?>
						<u title="Pilih Penerbit" class="link" onclick="goSend2('<?= $row['idpenerbit'] ?>','<?= $row['namapenerbit']; ?>')"><?= $row['namapenerbit']; ?></u>
					<? } else { ?>
					<u title="Pilih Penerbit" class="link" onclick="goSend('<?= $row['idpenerbit'] ?>','<?= $row['namapenerbit']; ?>')"><?= $row['namapenerbit']; ?></u>
					<? } ?>
					</td>
					<td align="center">
					<img title="Pilih Penerbit" src="images/centang.gif" style="cursor:pointer" onclick="goSend('<?= $row['idpenerbit'] ?>','<?= $row['namapenerbit']; ?>')">
					</td>
				</tr>
				<? } ?>
				<tr> 
					<td class="PagerBG footBG" align="right" colspan="4"> 
						Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
					</td>
					
				</tr>
			</table>
				<?php require_once('inc_listnav.php'); ?><br>
			<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="key" id="key">
			<input type="hidden" name="scroll" id="scroll">
			</form>
			</div>
		</div>
	</div>
	</body>
	<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

	</script>
	<script type="text/javascript">
	function goRefresh(){
	document.getElementById("caripenerbit").value='';
	goSubmit();

	}

	function goSend(id,nama) {
		window.opener.document.getElementById("idpenerbit").value = id;
		window.opener.document.getElementById("namapenerbit").value = nama;
		
		window.close();
	}
	function goSend2(id,nama) {
		window.opener.document.getElementById("idpenerbit").value = id;
		window.opener.document.getElementById("penerbit").value = nama;
		
		window.close();
	}
	</script>
	</html>