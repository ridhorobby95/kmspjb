<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_ttb = Helper::removeSpecial($_GET['nottb']);
	
	if($r_ttb=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Tanda Terima Pustaka';
	
	
	$sql = "select o.*,t.*,tb.qtyttbdetail,u.namapengusul from pp_orderpustaka o
			join pp_usul u on o.idusulan=u.idusulan
			join pp_orderpustakattb tb on tb.idorderpustaka=o.idorderpustaka
			join pp_ttb t on tb.idttb=t.idttb
			where t.nottb='$r_ttb'";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Tanda Terima Pustaka</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> Nomor TTP</td>
	<td>: <?= $r_ttb ?>
  </tr>    
  <tr>
	<td width=150> Tanggal TTP</td>
	<td>: <?= Helper::tglEngTime($row->fields['tglttb']) ?>
  </tr>

</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Id Usulan</strong></th>
    <th width="200" align="center"><strong>Judul</strong></th>
	<th width="100" align="center"><strong>Pengusul</strong></th>
	<th width="100" align="center"><strong>Jumlah</strong></th>
	<th width="100" align="center"><strong>Harga Pengadaan</strong></th>
	<th width="100" align="center"><strong>Total Harga Pengadaan</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= $rs['idusulan'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td ><?= $rs['namapengusul'] ?></td>
	<td align="center"><?= $rs['qtyttbdetail'] ?></td>
	<td align="center"><?= "Rp. ".number_format($rs['hargadipilih'],0,",",".") ?></td>
	<td align="center"><?= "Rp. ".number_format((intval($rs['hargadipilih'])*intval($rs['qtyttbdetail'])),0,",",".") ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=7 >Tidak ada pengusulan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=7><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>
<br>
	<table>
	<tr>
		<td width="400">&nbsp;</td>
		<td>
		Surabaya, <?= Helper::formatDateInd($row->fields['tglttb']) ?><br>
		Penerima,<br><br><br><br>
		<?= $row->fields['npkttb']; ?>
		</td>
	</tr>
	</table>

</div>
</body>
</html>
