<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'ms_penerbit';
	$p_window = '[PJB LIBRARY] Data Penerbit Pustaka';
	$p_title = 'Data Penerbit Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_penerbit.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$namapenerbit = Helper::removeSpecial($_POST['namapenerbit']);
			if ($namapenerbit!=''){
				if($r_key == ''){
					$ada = $conn->GetOne("select count(*) from ms_penerbit where upper(namapenerbit) = upper('$namapenerbit') ");
				}else{
					$ada = $conn->GetOne("select count(*) from ms_penerbit where upper(namapenerbit) = upper('$namapenerbit') and idpenerbit != '$r_key' ");
				}
				if($ada > 0){
					$errdb = 'Penyimpanan data gagal. <br/>Data Penerbit sudah ada.<br/>';	
					Helper::setFlashData('errdb', $errdb);
				}elseif($ada == 0){			
					$record = array();
					$record = Helper::cStrFill($_POST);//,array tabel
					
					if($r_key == '') { // insert record	
						Helper::Identitas($record);
						$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
										
						if($err != 0){
							$errdb = 'Penyimpanan data gagal.';	
							Helper::setFlashData('errdb', $errdb);
							}
						else {
							$sucdb = 'Penyimpanan data berhasil.';	
							Helper::setFlashData('sucdb', $sucdb);
							Helper::redirect(); }
						
					}
					else { // update record
						Helper::Identitas($record);
						$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idpenerbit",$r_key);
						
						if($err != 0){
							$errdb = 'Update data gagal.';	
							Helper::setFlashData('errdb', $errdb);
							
							}
						else {
							$sucdb = 'Update data berhasil.';	
							Helper::setFlashData('sucdb', $sucdb);
							header('Location: '.$p_filelist);
							exit();
							}
					}	
						
					
					if($err == 0) {
						if($r_key == '')
							$r_key = $record['idpenerbit'];
					}
					else {
						$row = $_POST; // direstore
						$p_errdb = true;
					}
				}
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			if($r_key == "201"){
				$errdb = 'Penghapusan data unusa gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}else{
				$err=Sipus::DeleteComplete($conn,$p_dbtable," where idpenerbit = '$r_key' and idpenerbit != '201' ");
				
				if($err==0) {
					header('Location: '.$p_filelist);
					exit();
				}else{
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
			
			
		}
		
	}
	if(!$p_errdb) {
	if($r_key !=''){
		$p_sqlstr = "select * from $p_dbtable 
					where idpenerbit = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		}
	}
	
	#autocomplete
	$sqlpenerbit="select idpenerbit, namapenerbit from $p_dbtable order by namapenerbit";
	$rspenerbit = $conn->Execute($sqlpenerbit);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body>
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
<div class="SideItem" id="SideItem">
<div class="LeftRibbon">
<?= $p_title ?>
</div>
<div align="center">
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Penerbit</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key!='') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><? include('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">

	<tr> 
		<td width="150" class="LeftColumnBG"><strong>Nama Penerbit *</strong> </td>
		<td class="RightColumnBG">
			<?= UI::createTextBox('namapenerbit',$row['namapenerbit'],'ControlStyle',50,40,$c_edit); ?>
			<input type="hidden" name="penerbit" id="penerbit" />
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG"><strong>Alamat</strong> </td>
		<td class="RightColumnBG"><?= UI::createTextBox('alamat',$row['alamat'],'ControlStyle',50,50,$c_edit); ?></td>
	</tr>
		<tr> 
		<td class="LeftColumnBG"><strong>Kota</strong> </td>
		<td class="RightColumnBG"><?= UI::createTextBox('kota',$row['kota'],'ControlStyle',50,30,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG"><strong>Kode Pos</strong></td>
		<td class="RightColumnBG"><?= UI::createTextBox('kodepos',$row['kodepos'],'ControlStyle',5,5,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG"><strong>Telepon</strong></td>
		<td class="RightColumnBG"><?= UI::createTextBox('telp',$row['telp'],'ControlStyle',20,15,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG"><strong>Fax</strong></td>
		<td class="RightColumnBG"><?= UI::createTextBox('fax',$row['fax'],'ControlStyle',30,15,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG"><strong>Email</strong></td>
		<td class="RightColumnBG"><?= UI::createTextBox('email',$row['email'],'ControlStyle',50,20,$c_edit); ?></td>
	</tr>
</table>



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</div>
</div>

<div id="div_penerbit" style="display:none;">
<?	while($row = $rspenerbit->FetchRow()) { ?>
	<p id="<?= $row['idpenerbit'] ?>"><?= $row['namapenerbit'] ?></p>
<?	} ?>
</div>



<div align="left" id="div_autocomplete" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;">
	<table bgcolor="#FFFFFF" id="tab_autocomplete" cellpadding="3" cellspacing="0"></table>
</div>

</body>

<script type="text/javascript" src="scripts/jquery.xauto.js"></script>
<script type="text/javascript">

$(function() {
	$("#namapenerbit").xauto({targetid: "penerbit", srcdivid: "div_penerbit", imgchkid: "imgusulan"});

});

function saveData() {
	if(cfHighlight("namapenerbit")){
		goSave();
	}
}

</script>
</html>