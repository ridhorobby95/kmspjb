<?php
	// defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	session_unset();
	
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
	}
	unset($_COOKIE[session_name()]);
	
	session_destroy();
	
	header('Location: '. Config::logoutUrl);
?>