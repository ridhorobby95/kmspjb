<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Keuangan Pelayanan Umum';

	$p_filerep = 'rep_dinamikpustakaxml';
	$p_tbwidth = 520;
	
	// combo box
	$a_format = array('xml' => 'XML');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
		
	// list jenis pustaka
	$sql = "select namajenispustaka,kdjenispustaka from lv_jenispustaka order by kdjenispustaka";
	$rsc = $conn->Execute($sql);
	
	$a_jenispustaka = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['jenispustaka'][$rowc['kdjenispustaka']] = $rowc['namajenispustaka'];
	$l_jenispustaka = UI::createSelect('s_jenispustaka',$a_kparam['jenispustaka'],'','ControlStyle',true);
	
	// list jurusan
	$sql = "select kdjurusan,namafakultas||' - '||namajurusan as jurusan from lv_jurusan j 
			left join lv_fakultas f on j.kdfakultas=f.kdfakultas order by namafakultas,namajurusan";
	$rsc = $conn->Execute($sql);
	
	$a_jurusan = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['jurusan'][$rowc['kdjurusan']] = $rowc['jurusan'];
	$l_jurusan = UI::createSelect('s_jurusan',$a_kparam['jurusan'],'','ControlStyle',true);
	
	// list topik
	// $sql = "select idtopik,namatopik from lv_topik order by namatopik";
	// $rsc = $conn->Execute($sql);
	
	// $a_topik = array();
	// while($rowc = $rsc->FetchRow())
		// $a_kparam['topik'][$rowc['idtopik']] = $rowc['namatopik'];
	// $l_topik = UI::createSelect('s_topik',$a_kparam['topik'],'','ControlStyle',true);
	
	// list bahasa
	$sql = "select kdbahasa,namabahasa from lv_bahasa order by namabahasa";
	$rsc = $conn->Execute($sql);
	
	$a_bahasa = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['bahasa'][$rowc['kdbahasa']] = $rowc['namabahasa'];
	$l_bahasa = UI::createSelect('s_bahasa',$a_kparam['bahasa'],'','ControlStyle',true);
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('tgl1').focus()">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan Keuangan Pelayanan Umum</td></tr>
	<tr> 
		<td width="150" class="LeftColumnBG">No. Seri Pustaka</td>
		<td class="RightColumnBG"> <input type="text" name="noseri" id="noseri" size="20"></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Jenis Pustaka</td>
		<td class="RightColumnBG"><?= $l_jenispustaka;?> </td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Jurusan</td>
		<td class="RightColumnBG"><?= $l_jurusan;?> </td>
	</tr>
	<!--<tr> 
		<td class="LeftColumnBG">Topik</td>
		<td class="RightColumnBG"><?= $l_topik;?> </td>
	</tr>-->
	<tr> 
		<td class="LeftColumnBG">Bahasa</td>
		<td class="RightColumnBG"><?= $l_bahasa;?> </td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Nama Penerbit</td>
		<td class="RightColumnBG"><input type="text" name="t_penerbit" id="t_penerbit" size="50"></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Pengarang</td>
		<td class="RightColumnBG"><input type="text" name="t_pengarang" id="t_pengarang" size="50"></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="250">Tanggal Perolehan</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		</td>
	</tr>
	<!--<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>-->
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2")){
		goSubmit();
	}
}

</script>
</html>