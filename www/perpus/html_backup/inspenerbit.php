<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// otentiksi user
	$a_auth = Helper::checkRoleAuth($conng);
	

	if(!empty($_POST)){
		$namapenerbit = Helper::removeSpecial($_POST['namapenerbit']);
		if ($namapenerbit!=''){
			$ada = $conn->GetOne("select count(*) from ms_penerbit where upper(namapenerbit) = upper('$namapenerbit') ");
			if($ada > 0){
				$errdb = 'Penyimpanan data gagal. <br/>Data Penerbit sudah ada.<br/>';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){			

				$record=array();
				$record = Helper::cStrFill($_POST);
				Helper::Identitas($record);
				
				$err=Sipus::InsertBiasa($conn,$record,ms_penerbit);
				if($err !=0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$checks=$conn->GetOne("select max(idpenerbit) as maxid from ms_penerbit");
					$id = $checks;
					$new = 1;
				}
			}
		}
	}
	$auth = $conn->GetRow("select namapenerbit from ms_penerbit where idpenerbit=$id");
?>

<html>
<head>
<title>Shortcut Author Baru</title>
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">

</head>
<body>
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
<form  method="post" name="insertauthor">
<div class="filterTable">
<table border="0" width="100%">
<tr height=25>
	<th colspan="2"><? include_once('_notifikasi.php'); ?><strong>Shortcut Penerbit Baru</strong></th>
</tr>
	<tr> 
		<td width="150"><strong>Nama Penerbit</strong> </td>
		<td><?= UI::createTextBox('namapenerbit','','ControlStyle',50,40,true); ?></td>
	</tr>
	<tr> 
		<td><strong>Alamat</strong> </td>
		<td><?= UI::createTextBox('alamat','','ControlStyle',50,50,true); ?></td>
	</tr>
		<tr> 
		<td><strong>Kota</strong> </td>
		<td><?= UI::createTextBox('kota','','ControlStyle',50,30,true); ?></td>
	</tr>
	<tr> 
		<td><strong>Kode Pos</strong></td>
		<td><?= UI::createTextBox('kodepos','','ControlStyle',5,5,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr> 
		<td><strong>Telepon</strong></td>
		<td><?= UI::createTextBox('telp','','ControlStyle',20,15,true); ?></td>
	</tr>
	<tr> 
		<td><strong>Fax</strong></td>
		<td><?= UI::createTextBox('fax','','ControlStyle',30,15,true); ?></td>
	</tr>
	<tr> 
		<td><strong>Email</strong></td>
		<td><?= UI::createTextBox('email','','ControlStyle',50,20,true); ?></td>
	</tr>
	<tr> 
		<td colspan="2"><input type="button" value="Simpan" onClick="tambah()" class="ControlStyle"></td>
	</tr>
</table>
</div>
<input type="hidden" id="idnya" name="idnya" value="<?= $id ?>">
</form> 
</div>
</div>
</body>

<script>
	function tambah(){
	
		var nmpenerbit=document.getElementById("namapenerbit").value;
		if(nmpenerbit!=''){
			document.insertauthor.submit();
		}
		else {
			alert('Masukkan Nama Penerbit dengan benar');
			document.getElementById("namapenerbit").focus;
		}
	}
	
	function etrInsert(e) {
		var ev= (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			tambah();
	}
	function goSend2(key,key2) {
		window.opener.document.getElementById("namapenerbit").value = key;
		window.opener.document.getElementById("idpenerbit").value = key2;
	}
</script>
</html>

<?php
	if($new==1) { ?>
		<script>
		window.opener.loadauthor();
		goSend2('<?= $auth['namapenerbit']; ?>','<?= $id ?>');
		window.close();
		</script>
<? } ?>