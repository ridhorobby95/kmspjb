<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_ke = Helper::removeSpecial($_POST['ke']);
	$r_pustaka = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_anggota = Helper::removeSpecial($_POST['kdjenisanggota']);
	$r_jenis = Helper::removeSpecial($_POST['jenis']);
	$r_tagih = Helper::removeSpecial($_POST['tagih']);
	$r_jurusan = Helper::removeSpecial($_POST['kdjurusan']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_jenis=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Penagihan';
	
	$p_namafile = 'rekap_penagihan'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select t.idanggota, a.namaanggota,j.namasatker,p.judul,p.authorfirst1,p.authorlast1,p.kodeddc,p.nopanggil,e.noseri,p.edisi,
			to_char(t.tgltransaksi,'YYYY-mm-dd') tgltransaksi, to_char(t.tgltenggat,'YYYY-mm-dd') tgltenggat
			from pp_transaksi t
			join ms_anggota a on t.idanggota=a.idanggota
			join pp_eksemplar e on t.ideksemplar=e.ideksemplar
			join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			join ms_pustaka p on e.idpustaka=p.idpustaka
			left join ms_satker j on j.idunit=a.idunit
			where to_date(to_char(t.tgltenggat,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') and  a.idunit='$r_jurusan' and 
				t.tglpengembalian is null and t.statustransaksi='1' and t.kdjenistransaksi='PJN' $sqlAdminUnit ";
	if($r_pustaka!='')
	$sql .=" and p.kdjenispustaka='$r_pustaka'";
	
	if($r_anggota!='')
	$sql .=" and a.kdjenisanggota = '$r_anggota'";
	
	if($r_lokasi)
		$sql .=" and l.kdlokasi = '$r_lokasi' ";
	
	if ($r_tagih=='semua')
		$sql .="";
	elseif($r_tagih=='0')
		$sql .=" and t.tgltransaksi<>t.tgltenggat ";
	elseif($r_tagih=='1')
		$sql .=" and t.tgltransaksi=t.tgltenggat";
	
	if($r_jenis==0)
		$sql .=" order by a.idanggota";
	else
		$sql .=" order by p.kodeddc,to_char(p.judul)";	
			
	$rs = $conn->Execute($sql);
	$id='';
	while($row = $rs->FetchRow()){
		$ArId[] = $row['idanggota'];
		$ArNama[] = $row['namaanggota'];
		$ArFak[] = $row['namasatker'];
		$ArJudul[] = $row['judul'];
		$ArAuthor[] = $row['authorfirst1'].' '.$row['authorlast1'];
		$ArNoPang[] = $row['nopanggil'];
		$ArReg[] = $row['noseri'];
		$ArEdisi[] = $row['edisi'];
		$ArTgl[] = $row['tgltransaksi'];
		$ArTeng[] = $row['tgltenggat'];
		$ArKet[] = $row['keterangan'];
		
		if($row['idanggota']==$id){
			$ArRow[$row['idanggota']]++;
			$id = $row['idanggota'];
		}else{
			$ArRow[$row['idanggota']]++;
			$id = $row['idanggota'];
		}
	}
	$rsc=$rs->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" >

<div align="center">
<table width=1000>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=1000 cellpadding="2" cellspacing="0" border=0>
	<tr>
		<td align="center" colspan=3>
		<strong>
			<h2>
			<? if ($r_jenis==0) { ?>PENAGIHAN KE- <?= $r_ke ?>
			<? } else echo "CEK KE RAK"; ?>
			</h2>Periode : <?= Helper::formatDateInd($r_tgl1)." S/d ".Helper::formatDateInd($r_tgl2); ?></td>
		</strong>
	</tr>
	<tr>
		<td align="right">Tanggal Cetak : <?= Helper::tglEng(date('Y-m-d')) ?></td>
	</tr>
  
</table>
<table width="1000" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<? if($r_jenis==0) { ?>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="100" align="center"><strong>Id Anggota</strong></th>
    <th width="150" align="center"><strong>Nama</strong></th>
    <th width="100" align="center"><strong>Bagian</strong></th>
    <th width="170" align="center"><strong>Judul</strong></th>
	<th width="120" align="center"><strong>Author</strong></th>
	<th width="150" align="center"><strong>No Panggil</strong></th>
	<th width="100" align="center"><strong>No. Induk</strong></th>
	<th width="100" align="center"><strong>Tgl Pinjam</strong></th>
	<th width="100" align="center"><strong>Tgl Kembali</strong></th>
	<th width="50" align="center"><strong>Telat (hari)</strong></th>
	<? } else { ?>
	<th width="80" align="center"><strong>No Panggil</strong></th>
	<th width="100" align="center"><strong>No. Induk</strong></th>
	<th width="200" align="center"><strong>Judul</strong></th>
	<th width="140" align="center"><strong>Last Author1</strong></th>
	<? } ?>
   </tr>
  <?php
	$n=0;
	$rowspan=1;
	for($i=0;$i<count($ArId);$i++) 
	{  	
	$ket=explode("]",$ArKet[$i]);
		
	?>
    <tr height=25>
	<? if($r_jenis==0) { ?>
		
		<? if($ArRow[$ArId[$i]] > 1) { 
			if($ArId[$i]!=$xid) { $n++;?>
		<td align="center" rowspan="<?= $ArRow[$ArId[$i]] ?>"><?= $n ?></td>
		<td rowspan="<?= $ArRow[$ArId[$i]]?>"><?= $ArId[$i] ?></td>
		<td rowspan="<?= $ArRow[$ArId[$i]] ?>"><?= $ArNama[$i] ?></td>
		<td rowspan="<?= $ArRow[$ArId[$i]] ?>"><?= $ArFak[$i] ?></td>
		<?
		
		$xid = $ArId[$i];
		}else{
		$xid = $ArId[$i];
		//$n++;
		}
		}else { $n++;
		?>
		<td align="center" ><?= $n ?></td>
		<td><?= $ArId[$i] ?></td>
		<td><?= $ArNama[$i] ?></td>
		<td><?= $ArFak[$i] ?></td>
		<? } ?>
		<td><?= $ArJudul[$i] ?></td>
		<td><?= $ArAuthor[$i] ?></td>
		<td><?= $ArNoPang[$i] ?></td>
		<td><?= $ArReg[$i] ?></td>
		<td><?= Helper::tglEng($ArTgl[$i]) ?></td>
		<td><?= Helper::tglEng($ArTeng[$i]) ?></td>
		<td align="center"><?= Sipus::hitung_telat($conn,$ArTeng[$i],date("Y-m-d")) ?></td>
	<? } else {?>
		<td><?= $ArNoPang[$i] ?></td>
		<td><?= $ArReg[$i] ?></td>
		<td><?= $ArJudul[$i] ?></td>
		<td><?= $ArAuthor[$i] ?></td>
	<? } ?>
  </tr>
	<?  } ?>
	<? if($rsc==0) { ?>
	<tr height=25>
		<td align="center" colspan="11" >Tidak Ada Data Tagihan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=11><b>Jumlah Tagihan : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>