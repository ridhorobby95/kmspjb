<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$c_read = $a_auth['canread'];
	
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);

	
		
	// definisi variabel halaman
	$p_dbtable = 'pp_opname';
	$p_window = '[PJB LIBRARY] Sirkulasi Opname Pustaka';
	$p_title = 'Sirkulasi Opname Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_opname.php');
	$p_cetak = Helper::navAddress('rep_tempopname.php');
	if($r_key==''){
		header('Location: '.$p_filelist);
		exit();
	}
	
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
			$ideks=Helper::removeSpecial($_POST['keyid']);
			$kondisibaru=Helper::removeSpecial($_POST['keykon']);
			
			$lokasi=Helper::removeSpecial($_POST['kdlokasi']);
			$kondisi=Helper::removeSpecial($_POST['kdkondisi']);
		if ($r_aksi=='upopname' and $c_edit) {
			$record=array();
			$sqlcheck=$conn->GetRow("select idopname from pp_detailopname where idopname=$r_key and ideksemplar=$ideks");
			$sqlcheck2=$conn->GetRow("select kdkondisi from pp_eksemplar where ideksemplar=$ideks");
			
			$record['idopname']=$r_key;
			$record['kdkondisi']=$kondisibaru;
			$record['tglopname']=date("Y-m-d");
			Helper::Identitas($record);
			
			$rec=array();
			$rec['idopname']=$r_key;
			$rec['ideksemplar']=$ideks;
			$rec['tglopname']=date("Y-m-d");
			$rec['kondisiopname']=$kondisibaru;
			$conn->StartTrans();
			if(!$sqlcheck){
				$rec['kondisiawal']=$sqlcheck2['kdkondisi'];
				Sipus::InsertBiasa($conn,$rec,pp_detailopname);
				
			}else{
				$col = $conn->Execute("select * from pp_detailopname where idopname = $r_key and ideksemplar=$ideks");
				$sql = $conn->GetUpdateSQL($col,$rec);
				if($sql !='')
					$conn->Execute($sql);
			}			
			
			Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$ideks);
		
			$conn->CompleteTrans();
			if($conn->ErrorNo() != 0){
				$errdb = 'Opname pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Opname Pustaka berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//Helper::redirect();
				}
			
			
		}
	}	
	
	

		if($lokasi!='' or $kondisi!='') {
			$SQLOpname=$conn->Execute("select e.ideksemplar,e.noseri, p.judul, k.namakondisi from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka
									join lv_kondisi k on e.kdkondisi=k.kdkondisi where e.statuseksemplar='ADA' and  e.kdlokasi='$lokasi' and e.kdkondisi='$kondisi' order by e.ideksemplar limit 15 ");
		}else
		$SQLOpname=$conn->Execute("select e.ideksemplar,e.noseri, p.judul, k.namakondisi from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka
									join lv_kondisi k on e.kdkondisi=k.kdkondisi where e.kdlokasi=null and e.kdkondisi=null");
		
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$lokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150"');
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$kondisi,true,false,0,'id="kdkondisi" class="ControlStyle" style="width:150"');
		
		$rs_cb2 = $conn->Execute("select namakondisi, kdkondisi  from lv_kondisi order by kdkondisi");
		$l_kondisi2 = $rs_cb2->GetMenu2('kdkondisi2',$kondisibaru,true,false,0,'id="kdkondisi2" class="ControlStyle" style="width:150" ');
		
		$coba="select namakondisi, kdkondisi from lv_kondisi order by kdkondisi";
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		while($rowc=$rs_cb->FetchRow()){
		$Arkondisi[$rowc['kdkondisi']]=$rowc['namakondisi'];
		}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<!--table width="<?//= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><?//= $p_title ?></td>
			</tr>
		</table-->
		<table width="100">
			<tr>
			<? if($c_read) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Opname </span></a>
			</td>
			<td align="center">
				<a href="javascript:goRefreshOp();" class="button"><span class="refresh">Refresh</span></a>
			</td>
			<td align="center">
				<a href="<?= $p_cetak."&location=".$lokasi."&condition=".$kondisi ?>" target="_blank" class="button"><span class="print">Cetak Eksemplar</span></a>
			</td>
			<? } ?>
			</tr>
		</table><? include_once('_notifikasi.php'); ?>
		
		<form name="perpusform" id="perpusform" method="post" enctype="multipart/form-data">
			<div class="filterTable">
				<table width="100%">
					<tr height="40">
					<td colspan="4">Pilih Lokasi Opname : 
					<?= $l_lokasi ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi&code=op',500,500);">
					&nbsp; Kondisi Awal : <?= $l_kondisi ?>
					&nbsp;
					<? if($c_edit) { ?>
					<!--<input type="button" name="btneks" id="btneks" value="Mulai Opname" style="cursor:pointer" onclick="goSubmit()"> -->
					<input type="button" class="buttonSmall" onclick="goSubmit()" id="btneks" name="btneks" value="Mulai Opname" style="cursor:pointer;height:25;width:120">
					&nbsp; 
					<input type="button" class="buttonSmall" onclick="window.open('index.php?page=spesial_opname&id=<?= $r_key ?>',name='_self')" id="btneks" name="btneks" value="Spesial Opname" style="cursor:pointer;height:25;width:120">
					
					<? } ?>
					<br></td>
					</tr>
				</table>
			</div>
			<br />
			<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
				<table width="100%" cellpadding=0 class="GridStyle">
				
				<tr>
					<td width="100" align="center" class="SubHeaderBGAlt thLeft" width="700"><b>NO INDUK</b></td>
					<td width="380" align="center" class="SubHeaderBGAlt thLeft" width="700"><b>Judul Pustaka</b></td>
					<td width="150" align="center" class="SubHeaderBGAlt thLeft" width="120"><b>Kondisi Awal</b></td>
					<td width="210" align="center"  class="SubHeaderBGAlt thLeft" width="80"><b>Kondisi Opname</b></td>
				</tr>
				<? 
				$i=0;

					while ($row = $SQLOpname->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
					?>
					
				<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
					<td align="center">&nbsp;<?= $row['noseri']; ?></td>
					<td align="left">&nbsp;<?= $row['judul']; ?></td>
					<td align="left">&nbsp;<?= $row['namakondisi']; ?></td>
					<td align="center">
					
					<?= UI::createSelect('kddkondisi'.$row['ideksemplar'],$Arkondisi,'','ControlStyle',$c_edit,'id="kddkondisi"'.$row['ideksemplar'].' style="width:160"'); ?>
					<?//= UI::CreateCombo($conn,kddkondisi.$row['ideksemplar'],$coba,'kdkondisi','namakondisi','') ?>
					
					<input type="button" name="btnop" id="btnop" value="OK" style="cursor:pointer" onclick="UpOpname('<?= $row['ideksemplar'] ?>')"></td>
				</tr>
				<?php
					} 
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="4"><b>Data tidak ditemukan.</b></td>
					</tr>
					<tr>
						<td colspan="4" class="footBG">&nbsp;</td>
					</tr><?php } else { ?>
					<tr>
						<td colspan="4">
						<br>
						<table align="center" cellspacing="3" cellpadding="2" width="370" border=0 bgcolor="#f1f2d1">
							<tr>
								<td width=270><u><b>Keterangan : </b></u></td>
							</tr>
							<tr>
								<td>
								Maksimal daftar pustaka opname diatas adalah 15 Eksemplar
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<? } ?>
					</table>
					



		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
		<input type="hidden" name="keyid" id="keyid">
		<input type="hidden" name="keykon" id="keykon">
		</form>
		</div>
	</div>
</div>
</body>

<script language="javascript">


function goRefreshOp(id) {
	document.getElementById("act").value='';
	document.getElementById("kdlokasi").value='';
	document.getElementById("kdkondisi").value='';
	goSubmit();
}


function UpOpname(id){
	document.getElementById("act").value='upopname';
	document.getElementById("keyid").value=id;	
	var namee='kddkondisi'+id;
	document.getElementById("keykon").value=document.getElementById(namee).value;
	goSubmit();
}
</script>
</html>

	
	