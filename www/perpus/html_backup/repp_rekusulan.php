<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);

	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lap = Helper::removeSpecial($_POST['j_lap']);
	
	if($r_lap=='po')
		$var='Laporan Purchase Order';
	else
		$var='Laporan Tanda Terima Pustaka';
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] '.$var;
	
	$p_namafile = 'laporan_'.$r_lap.'_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){ // or $r_lap==''
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql="select o.*,u.*,p.*,t.* from pp_orderpustaka o
		  join pp_usul u on o.idusulan=u.idusulan
		  left join pp_po p on o.idpo=o.idpo
		  left join pp_ttb t on o.idttb=t.idttb
		  where 1=1";
	if($r_lap=='po')
		$sql .=" and o.stspo='1' and o.idpo is not null and o.idttb is null and p.tglpo";
	else
		$sql .=" and o.ststtb='1' and o.idttb is not null and t.tglttb";
	
	$sql.=" between '$r_tgl1' and '$r_tgl2' order by idorderpustaka";
	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h3><?= $var ?></h3>
  	</strong></td>
  </tr>
  <tr>
	<td width=150> Periode</td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?>
  </tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">
  
  <tr>
	<th width="12" align="center"><strong>No.</strong></th>
    <th width="10" align="center"><strong>Id <br>Usulan</strong></th>
    <th width="130" align="center"><strong>Judul</strong></th>
	<th width="10" align="center"><strong>Pengusul</strong></th>
	
	<? if($r_lap=='po') {?>
	<th width="60" align="center"><strong>Tanggal PO</strong></th>
	<th width="60" align="center"><strong>Jumlah PO</strong></th>
	<th width="60" align="center"><strong>Petugas PO</strong></th>
	
	<? } ?>
	<? if($r_lap=='ttb') {?>
	<th width="60" align="center"><strong>Tanggal TTP</strong></th>
	<th width="60" align="center"><strong>Jumlah TTP</strong></th>
	<th width="60" align="center"><strong>Petugas TTP</strong></th>
	<? } ?>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= $rs['idusulan'] ?></td>
    <td align="left"><?= Helper::limitS($rs['judul'],70) ?></td>
	<td><?= $rs['namapengusul'] ?></td>
	
	<? if($r_lap=='po') {?>
	<td align="center"><?= Helper::tglEng($rs['tglpo']) ?></td>
	<td align="center"><?= $rs['qtypo'] ?></td>
	<td align="center"><?= $rs['npkpo'] ?></td>
	
	<? } if($r_lap=='ttb') {?>
	<td align="center"><?= Helper::tglEng($rs['tglttb']) ?></td>
	<td align="center"><?= $rs['qtyttb'] ?></td>
	<td align="center"><?= $rs['npkttb'] ?></td>
	<? } ?>
  </tr>
	<? $no++; } ?>
	<? if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=7 >Tidak ada transaksi verifikasi</td>
	</tr>
	<? } ?>
    <tr>
    <td align="left" height=25 colspan=7><strong>Total Jumlah <?= $rsc ?></strong></td>

  </tr>
</table>


</div>
</body>
</html>