<?php
	$conn->debug=false;
	define('BASEPATH',1);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
	require_once('../application/helpers/x_helper.php');
	
	// require tambahan
	$kepala = Sipus::getHeadPerpus();
	$r_id=Helper::removeSpecial($_GET['id']);
	$r_dari=Helper::removeSpecial($_GET['dari']);
	
	$connGate = Factory::getConnGate();
	$connGate->debug = false;
	$cek = $connGate->IsConnected();
	
	//if($r_dari == "perpus"){
		$sql = "select idanggota, a.namaanggota, alamat, asal, noika, tglexpired, tgldaftar, lj.namasatker, lj.singkatan, a.kdjenisanggota, a.idpegawai 
			from ms_anggota a 
			left join ms_satker lj on lj.kdsatker=a.idunit
			left join lv_jenisanggota ja on ja.kdjenisanggota=a.kdjenisanggota
			where idanggota = '$r_id' ";
	/*}else{
		$sql = "select t.nid as idanggota, to_char(t.iduser), t.nama as namaanggota, 'T' as kdjenisanggota, 'Karyawan' as namajenisanggota, t.alamat, t.kota,
			'' as tgldaftar, '' as tglexpired, '' as noika, '' as asal, '' as namasatker, t.iduser idpegawai  
			from um.users t
			where t.nid = '$r_id' ";
	}*/
	
	$anggota=$conn->GetRow($sql);
	
	$user= $conng->GetRow("select userdesc from gate.sc_user where username='".$_SESSION['PERPUS_USER']."'");
	
	$p_foto = Config::dirFoto.'anggota/'.trim($anggota['idanggota']).'.jpg';
	$p_hfoto = Config::fotoUrl.'anggota/'.trim($anggota['idanggota']).'.jpg';
	
	$sqlPhoto = "select foto from users where nid = '$r_id'";
	$rsPhoto = $connGate->GetOne($sqlPhoto);
	
	if($anggota['kdjenisanggota'] == 'M')
		$labelanggota = $anggota['singkatan'].'/'.$anggota['namasatker'];
	else
		$labelanggota = $anggota['namasatker'];
?>

<html>
<head>
<title>Kartu Anggota</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="printpage()"> <!--onload="printpage()"-->
	<table cellpadding="1" border=0>
	<tr><td>
		<table width="300" cellpadding="1" cellspacing="0" border="0" style="background:#FFF;border:1pt solid #000;-moz-border-radius:8px;padding:2px;margin-top:2px;" > <!-- style="background:#FFF;border:1pt solid #d2d2d2 double black;-moz-border-radius:5px;padding:10px;margin-top:10px;"-->
		<tr>
		<td>
			<table height="150" width="320" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<div><img src="images/perpustakaan/logo_pt_warna.png" width="50" height="50"></div>
					<div style="float:left;margin-top:-50px;margin-left:60px;font-size:8.2px"><b>PERPUSTAKAAN</b><br><b>PJB</div>
					<div style="float:left;margin-top:-25px;margin-left:60px;font-size:7px;color:007FFF"><b>Jl Ketintang Baru No.11 Surabaya 60231<br>Telp. (031) 8283 180, (031) 8283 183</b></div>
				</tr>
				<tr><td colspan="2"><hr></td></tr>
				<tr>
					<td width="100" nowrap align="center"><div style="float:left;margin-left:7px;">
						<div style="position:relative;width:80px;height:80px;overflow: hidden;font-size:8px;padding:1px;border:1px solid #021a40;">
							<img style="position:absolute;left:-12px;" src="<?= Config::pictProf.xEncrypt($anggota['idpegawai']).'&s=300'; ?>" width="100" height="80"></div></div>
						<div style="float:left;margin-top:3px;margin-left:0px; width: 100px; height: 15px; overflow: hidden">
							<img width="100" nowrap src="index.php?page=image_anggota&key=<?= $anggota['idanggota'] ;?>">
						</div>
					</td>
					<td align="left" style="vertical-align:top;font-size:10px;">
					<br/><div style="padding-top:0px;padding-left:0px;padding-top:5px;font-family:'Times New Roman',Georgia,Serif;font-size:11px">
						<b><?= $anggota['idanggota']?></b><br/>
						<b><?= $anggota['namaanggota']?></b><br>
						<b><?= $labelanggota ?></b><br>
						<b><?= $anggota['alamat']?></b>
						</div>
					</td>
				</tr>
			</table>
		</td></tr>
		</table>
	</td>
	<td>&nbsp;</td>
	<td>
		<table  width="300" cellpadding="1" cellspacing="0" border="0" style="background:#FFF;border:1pt solid #000;-moz-border-radius:8px;padding:2px;margin-top:2px;" > <!-- style="background:#FFF;border:1pt solid #d2d2d2 double black;-moz-border-radius:5px;padding:10px;margin-top:10px;"-->
		<tr><td valign="top">
			<table cellpadding="0" cellspacing="0" height="200" width="320" border="0" align="center" >
				<tr>
					<td>
						<div style="float:left;margin-top:0px;margin-left:5px"><strong><font size="1" color="#FB0808"><u> UNTUK DIPERHATIKAN </u></font></strong><br><br></div>
						<div style="float:left;margin-top:-10px;margin-left:5px;line-height:200%;font-family:'Times New Roman',Georgia,Serif;font-size:6.35pt">
						1. Kartu ini hanya berlaku bagi pemiliknya.<br>
						2. Penyalahgunaan terhadap kartu ini bukan menjadi tanggung jawab Petugas.<br>
						3. Mentaati tata tertib yang berlaku.<br>
						4. Kartu ini harus ditunjukkan kepada Petugas saat memanfaatkan Perpustakaan.<br>
						5. Jika kartu hilang, segera melaporkan ke Petugas.<br>
						</div>
						<div style="float:left;margin-left:150px;font-family:'Times New Roman',Georgia,Serif;font-size:8.45px">
							<span>Surabaya, <?php echo Helper::formatDateInd(date("Y-m-d"))?></span><br>
							<div style="background-size:50px 50px;background-repeat:no-repeat;">
							<span><b><?=$kepala['jabatan'];?> PJB,</b></span><br><br><br><br>
							<span><b><?=$kepala['namalengkap'];?></b></span><br>
							<span><b>NIP. <?=$kepala['nik'];?></b></span>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
		</tr>
		</table>
	</td></tr>
	<tr><td><div style="height:5px"></div></td></tr>
	</table>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>