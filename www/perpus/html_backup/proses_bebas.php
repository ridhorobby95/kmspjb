
	<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	 // otorisasi user
	 $c_add = $a_auth['cancreate'];
	 $c_edit = $a_auth['canedit'];
	 $c_readlist = $a_auth['canlist'];

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	if(!empty($_POST)){
		$x=Helper::cStrNull($_POST['x']);
		$r_aksi=Helper::removeSpecial($_POST['act']);
		if($r_aksi=='simpan'){
			$idpel=$conn->GetRow("select max(idpelayanan) as idmax from pp_pelayanan");
			$idok=$idpel['idmax']+1;
			
			$record=array();
			// $record['idpelayanan']=$idok;
			$record['idanggota']=Helper::removeSpecial($_POST['idanggota']);
			$record['kdpelayanan']='B';
			$record['tglpelayanan']=date('Y-m-d');
			$record['petugas']=Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['keperluan']=Helper::removeSpecial($_POST['perlu']);
			$record['jenis']=Helper::removeSpecial($_POST['keterangan']);
			$record['nosurat']=Helper::removeSpecial($_POST['nosurat']);
			
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,pp_pelayanan);
			
			// update status anggota menjadi non aktif
			$recordnon['statusanggota'] = '0'; //non aktif
			Sipus::UpdateBiasa($conn,$recordnon,ms_anggota,'idanggota',$record['idanggota']);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$cetak=true;
					}
		}
	}	

	//validasi
	$idanggota=Helper::removeSpecial($_POST['idanggota']);

	unset($_SESSION['idbebaspinjaman']);
	if ($idanggota=='') {
		echo "<font color='red'>Masukkan Id Anggota dengan benar.</font>";
	} else {
		$cekangg=$conn->GetRow("select a.idanggota, a.namaanggota, a.idunit 
				       from ms_anggota a
				       where a.idanggota='$idanggota'  ");
		if (!$cekangg){
			echo "Data Anggota tidak ditemukan.";
		}
		else {
			if($cekangg['idunit'] != $idunit){
				echo "<font color='red'>Anggota bukan berasal dari unit $unitlogin </font>";
				$cekangg = array(); // digunakan agar tidak tampil data anggota nya
			}else{
				$cek=$conn->Execute("select ideksemplar,noseri,judul,tgltransaksi,tgltenggat from v_trans_list where idanggota='$idanggota' and /*tglpengembalian is null*/ statustransaksi='1' and kdjenistransaksi<>'PJL'");
				$row=$cek->RowCount();
				
				$cek_denda = $conn->GetRow("select denda from ms_anggota where idanggota='$idanggota'");
				
				if($row>0){
					echo "<font color='red'>Anggota tersebut masih mempunyai tanggungan pinjaman pustaka<br></font>";
				} else if($cek_denda['denda'] > 0){
					echo "<font color='red'>Anggota tersebut masih mempunyai hutang denda sebesar Rp.".$cek_denda['denda'].",-<br></font>
						<br><u title='Lunasi Hutang Denda' onclick='goFreeSkors(\"$idanggota\")' style='cursor:pointer;color:#3300FF'><img src='images/freeskors.gif'>&nbsp;Bayarkan";
				} else {
					$getNomer=$conn->GetRow("select max(nosurat) as maxsurat from pp_pelayanan where kdpelayanan='B' and to_char(tglpelayanan,'Year')=to_char(current_date,'Year')");
					$slice=explode("/",$getNomer['maxsurat']);
					$isNomer=$slice['0']+1;
				}
			}
		}
	}
	$a_jenis = array('PS' => 'Pensiun [PS]', 'KL' => 'Keluar [KL]', 'BH' => 'Berhenti [BH]');
	$l_jenis = UI::createSelect('jenis',$a_jenis,$_POST['keterangan'],'ControlStyle',true,'id="jenis" onchange="goKet(this.value)"');
	
?>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
<body>	
<? if($row==0 and $cekangg and ($cek_denda['denda'] == 0 or $cek_denda['denda'] == null)){ ?>
<form name="perpusbebas" id="perpusbebas" method="post">
<table width='530' border=0 align='center' cellpadding='4' cellspacing=0 class="filterTable">
<tr>
	<td><b>Nama Anggota</b></td>
	<td><b>: &nbsp;&nbsp;<?= $cekangg['namaanggota'] ?></b></td>
</tr>
<tr>
	<td width='123'>Nomor Surat</td>
	<td>: &nbsp;&nbsp;<input type='text' size='25' maxlength="25" name='txtnomer' id='txtnomer' value="<?= $_POST['nosurat']=='' ? Helper::strPad($isNomer,4,0,'l')."/P.PJB/BP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y') : $_POST['nosurat'] ?>" readonly="readonly"></td>
	</tr>
	<tr>
	<td>Tanggal Surat</td>
	<td>: &nbsp;&nbsp;<input type='text' size='10' maxlength="10" name='txttgl' id='txttgl' value="<?= date("d-m-Y")?>" readonly="readonly">

		<img src="images/cal.png" id="tgle" style="cursor:pointer;" title="Pilih tanggal surat">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "txttgl",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
	</td>
	</tr>
	<tr>
	<td>Keperluan</td>
	<td>: &nbsp; <?= $l_jenis ?>
	</td>
	</tr>
	<tr>
		<td>Keterangan</td>
		<td>: &nbsp; <input type="text" size="40" name="txtperlu" id="txtperlu" value="<?= $_POST['perlu']=='' ? "Pensiun" : $_POST['perlu'] ?>">
	</tr>
	<tr>
			<td>&nbsp;</td>		
	<td>
	&nbsp;&nbsp;&nbsp;
	<? if($cetak==true){ ?>
	<input type='button' name='btnprint' id='btnprint' value='Cetak Surat' class="buttonSmall" onclick="goPrintBebas()" style="cursor:pointer;height:25;width:100">&nbsp;
	<input type='button' name='btnselesai' id='btnselesai' value='Selesai' class="buttonSmall" onclick="goSubmit()" style="cursor:pointer;height:25;width:100">
	<? } else { ?>
	<input type='button' name='btnbebas' id='btnbebas' value='Simpan' class="buttonSmall" onclick="goBebas()" style="cursor:pointer;height:25;width:100"> &nbsp;
	<? } ?>
	</td>
</tr>
</table>
</form>
<br><span id="span_error" style="display:none"><font color="#FF0000">Masukkan data dengan benar</font></span>
<script type="text/javascript">
function goPrintBebas(){
	if (document.getElementById("txtnomer").value==''){
		$("#span_error").show();
		setTimeout('$("#span_error").hide()',2000);
		return false;
	}else {
	popup('index.php?page=pop_notabebas&id=<?= $cekangg['idanggota'] ?>&no='+document.getElementById("txtnomer").value+'&date='+document.getElementById("txttgl").value+'&something='+document.getElementById("txtperlu").value,530,450);
	}
		
}
</script>	
<? } else if($row>0 and $cekangg) { ?>
<br>
<table  width='700' border=0 align='center' cellpadding='4' cellspacing='0'>
<tr>
	<td width="100"><b>Nama Anggota</b></td>
	<td><b>: &nbsp;<?= $cekangg['namaanggota'] ?></b></td>
</tr>
</table>

<table  width='700' border=0 align='center' cellpadding='4' cellspacing='0' class="GridStyle">
<tr>
	<td width="100" class="SubHeaderBGAlt" align="center">NO INDUK</td>
	<td  class="SubHeaderBGAlt" align="center">Judul Pustaka </td>
	<td width="120" class="SubHeaderBGAlt" align="center">Tanggal Pinjam</td>
	<td width="120" class="SubHeaderBGAlt" align="center">Tanggal Kembali</td>
</tr>

	<?php
		$i = 0;
			// mulai iterasi
			while ($rs = $cek->FetchRow()) 
			{				
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
	?>
	<tr class="<?= $rowstyle ?>" height="25">
	<td align="center"><?= $rs['noseri'] ?></td>
	<td><?= $rs['judul'] ?></td>
	<td  align="center"><?= Helper::tglEngTime($rs['tgltransaksi']) ?> </td>
	<td  align="center"><?= Helper::tglEngTime($rs['tgltenggat']) ?> </td>
	</tr>
	<? } if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="4"><b>Tidak Ditemukan.</b></td>
	</tr>
	<?php } ?>
</table>
<?} ?>
</body>
<script>
function goKet(key){
	var nomer=document.getElementById('txtnomer').value;
	if(key=='BH'){
		document.getElementById('txtperlu').value='Berhenti';
	}
	else if(key=='KL'){
		document.getElementById('txtperlu').value='Keluar';
	}
	else if(key=='PS'){
		document.getElementById('txtperlu').value='Pensiun';
	}
}
function goBebas(){
	if(cfHighlight("txtnomer,txttgl,txtperlu")){
		goSimpan();
	}
}

function goFreeSkors(key) {
	goFreeSkors2(key);
}


</script>
