<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_thn = Helper::removeSpecial(Helper::formatDate($_POST['tahun']));
	$r_unit = Helper::removeSpecial(Helper::formatDate($_POST['kdjurusan']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Per Pengunjung';
	
	$p_namafile = 'rekap_pengunjung_'.$r_thn.'_'.$r_unit;
	
	if($r_format=='' or $r_thn==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(*) jml, idanggota, to_char(t_updatetime,'mm') bulan
		from pp_bukutamu 
		where to_char(t_updatetime,'YYYY') = '$r_thn' ";
		
	if(!empty($r_unit)){
		$sql .= " and idunit = '$r_unit' ";
	}
		
	$sql .=" group by idanggota, to_char(t_updatetime,'mm')";
	$rs = $conn->Execute($sql);
	
	while($row=$rs->FetchRow()){
		$data[$row['idanggota']][$row['bulan']] = $row['jml'];
		$data[$row['bulan']] = (int) $data[$row['bulan']] + (int) $row['jml'];		
	}
	
	$sql_u = "select * 
			from um.users 
			where 1=1 ";
	if(!empty($r_unit)){
		$sql_u .= " and kodeunit = '$r_unit' ";
	}
	
	$sql_u .= "order by kodeunit, nama ";
	$users = $conn->Execute($sql_u);
	$satker = "";
	if(!empty($r_unit)){
		$satker .= $conn->GetOne("select namasatker from ms_satker where kdsatker = '$r_unit' ");
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=995>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Rekap Per Pengunjung
	<br>Periode <?= $r_thn; ?>
	<?if(!empty($r_unit)){?>
	<br>Unit <?= $satker; ?>
	<?}?>
	</h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
	<tr height=30>
		<th nowrap width="10%" rowspan=2 align="center"><b>No.</b></th>
		<th nowrap width="50%" rowspan=2 align="center"><b>Nama</b></th>
		<th nowrap width="50%" rowspan=2 align="center"><b>NID</b></th>
		<th nowrap width="30%" colspan=12 align="center"><b><?=$r_thn;?></b></th>
	</tr>
	<tr>
	<? for($i=1;$i<=12;$i++){?>
		<th nowrap width="50%" align="center"><b><?=Helper::indoMonth($i,false);?></b></th>
	<? } ?>
	</tr>
	<? 
	$k=0;
	while($rowf=$users->FetchRow()){ $k++;?>
	<tr>
		<td><?=$k;?></td>
		<td><?=$rowf['nama'];?></td>
		<td><?=$rowf['nid'];?></td>
		<? $total = array(); 
		for($i=1;$i<=12;$i++){?>
		<td align="right"><?=number_format($data[$rowf['nid']][$i],0, ',', '.');?></td>
		<? } ?>
		</tr>
	<? 
	} ?>
	<tr height=30>
		<td align="right" colspan=3><b>JUMLAH TOTAL</b></td>
		<? for($i=1;$i<=12;$i++){?>
		<td align="right"><b><?=number_format($data[$i],0, ',', '.');?></b></td>
		<? } ?>
	</tr>
</table>
<br/><br/><br/>
</div>
</body>
</html>