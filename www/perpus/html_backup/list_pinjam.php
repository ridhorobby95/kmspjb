<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
			
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Daftar Riwayat Peminjaman Pustaka';
	$p_title = 'Daftar Riwayat Peminjaman Pustaka';
	$p_tbheader = '.: Daftar Peminjaman Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_id = "pp_transaksi";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 't.tgltransaksi desc ';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
		
	$p_sqlstr = "SELECT t.idtransaksi, t.kdlokasi, t.idanggota, t.kdjenistransaksi,
			t.ideksemplar, p.kdjenispustaka, e.kdklasifikasi, e.noseri,
			vj.namajenispustaka, p.judul, p.judulseri, p.edisi, p.idpustaka,
			p.nopanggil, t.tgltransaksi, t.tgltenggat, t.tglpengembalian,
			t.statustransaksi, t.perpanjangke, t.tglperpanjang, t.fix_status,
			p.authorfirst1, p.authorlast1, l.namalokasi,
			to_char(t.tgltenggat,'YYYY-mm-dd') as tgl_tenggat,
			to_char(t.tgltransaksi,'YYYY-mm-dd') as tgl_transaksi,
			u.nama, p.keywords  
		FROM pp_transaksi t
		join um.users u on u.nid = t.idanggota 
		JOIN pp_eksemplar e ON t.ideksemplar = e.ideksemplar
		JOIN ms_pustaka p ON e.idpustaka = p.idpustaka
		LEFT JOIN lv_lokasi l ON l.kdlokasi = e.kdlokasi
		JOIN lv_jenispustaka vj ON vj.kdjenispustaka = p.kdjenispustaka
		where 1=1 $sqlAdminUnit ";
	
	if($_POST){
		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$keynohal = Helper::removeSpecial($_POST['nohalaman']);
		$filtersearch = Helper::removeSpecial($_POST['filtersearch']);
		$carifilter = Helper::removeSpecial($_POST['carifilter']);
		$tglrawal = Helper::removeSpecial($_POST['tglrawal']);
		$tglrakhir = Helper::removeSpecial($_POST['tglrakhir']);
		$tgleawal = Helper::removeSpecial($_POST['tgleawal']);
		$tgleakhir = Helper::removeSpecial($_POST['tgleakhir']);
		$statuseks = Helper::removeSpecial($_POST['statuseks']);
		
		#session
		$_SESSION['listreservasi']['lokasi']=$keylokasi;
		$_SESSION['listreservasi']['nohalaman']=$keynohal;

		$_SESSION['listreservasi']['filtersearch']=$filtersearch;
		$_SESSION['listreservasi']['carifilter']=$carifilter;
		$_SESSION['listreservasi']['tglrawal']=$tglrawal;
		$_SESSION['listreservasi']['tglrakhir']=$tglrakhir;
		$_SESSION['listreservasi']['tgleawal']=$tgleawal;
		$_SESSION['listreservasi']['tgleakhir']=$tgleakhir;
		$_SESSION['listreservasi']['statuseks']=$statuseks;
		
		##
		if($keynohal!=null or $keynohal!='')
			$p_page 	= $keynohal;
		else
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
	}else{
		$keylokasi=$_SESSION['listreservasi']['lokasi'];
		$keynohal=$_SESSION['listreservasi']['nohalaman'];

		$filtersearch=$_SESSION['listreservasi']['filtersearch'];
		$carifilter=$_SESSION['listreservasi']['carifilter'];
		$tglrawal=$_SESSION['listreservasi']['tglrawal'];
		$tglrakhir=$_SESSION['listreservasi']['tglrakhir'];
		$tgleawal=$_SESSION['listreservasi']['tgleawal'];
		$tgleakhir=$_SESSION['listreservasi']['tgleakhir'];
		$statuseks=$_SESSION['listreservasi']['statuseks'];
		
		
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
	
	
	if($filtersearch == "noseri"){	
		$p_sqlstr.=" and (upper(e.noseri) like upper('%$carifilter%')) ";
	}elseif($filtersearch == "judul"){	
		$p_sqlstr.=" and (upper(to_char(p.judul)) like upper('%$carifilter%')) ";
	}elseif($filtersearch == "keywords"){	
		$p_sqlstr.=" and (upper(p.keywords) like upper('%$carifilter%')) ";
	}elseif($filtersearch == "idanggota"){
		$p_sqlstr.=" and (upper(t.idanggota) like upper('%$carifilter%')) ";
	}elseif($filtersearch == "nama"){	
		$p_sqlstr.=" and (upper(u.nama) like upper('%$carifilter%')) ";
	}
	
	if($tglrawal and $tglrakhir){
		$p_sqlstr.=" and (to_date(to_char(t.tgltransaksi,'dd-mm-YYYY'),'dd-mm-YYYY') between to_date('$tglrawal','dd-mm-YYYY') and to_date('$tglrakhir','dd-mm-YYYY') )";
	}else{
		if($tglrawal){
			$p_sqlstr.=" and (to_date(to_char(t.tgltransaksi,'dd-mm-YYYY'),'dd-mm-YYYY') = to_date('$tglrawal','dd-mm-YYYY') )";
		}elseif($tglrakhir){
			$p_sqlstr.=" and (to_date(to_char(t.tgltransaksi,'dd-mm-YYYY'),'dd-mm-YYYY') = to_date('$tglrakhir','dd-mm-YYYY') )";
		}
	}
	
	if($tgleawal and $tgleakhir){
		$p_sqlstr.=" and (to_date(to_char(t.tglpengembalian,'dd-mm-YYYY'),'dd-mm-YYYY') between to_date('$tgleawal','dd-mm-YYYY') and to_date('$tgleakhir','dd-mm-YYYY') )";
	}else{
		if($tgleawal){
			$p_sqlstr.=" and (to_date(to_char(t.tglpengembalian,'dd-mm-YYYY'),'dd-mm-YYYY') = to_date('$tgleawal','dd-mm-YYYY') )";
		}elseif($tgleakhir){
			$p_sqlstr.=" and (to_date(to_char(t.tglpengembalian,'dd-mm-YYYY'),'dd-mm-YYYY') = to_date('$tgleakhir','dd-mm-YYYY') )";
		}
	}
	
	if($keylokasi!=''){
		$p_sqlstr .= " and e.kdlokasi='$keylokasi' ";
	}
			
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}

	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_lokasi = $conn->Execute("select l.namalokasi, l.kdlokasi from lv_lokasi l where 1=1 $sqlAdminUnit ");
	$l_lokasi = $rs_lokasi->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150" onchange="goFilterEx()" ');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
	
	$a_filter = array('' => '- Pilih Filter Pencarian -', 'noseri' => 'No. Induk', 'judul' => 'Judul', 'keyword' => 'Keyword', 'idanggota' => 'ID Anggota Pinjam', 'nama' => 'Nama Anggota Pinjam');
	$l_filter = UI::createSelect('filtersearch',$a_filter,$filtersearch,'ControlStyle');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<style>

	/* tooltip styling. by default the element to be styled is .tooltip  */
	#tooltip {
		display:none;
		background:transparent url('images/black_arrow.png');
		font-size:8px;
		height:70px;
		width:160px;
		padding:25px;
		color:#fff;	
		border :1;
		
		position:absolute;
		top:5px;
		z-index: 3000;
		opacity: 0.85;
	}

	/* style the trigger elements */
	#test u {
		border:0;
		cursor:pointer;
		margin:0 8px;
		
	}
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div class="filterTable table-responsive">
          <table border=0 width="100%">
            <tr>
              <td><strong><?=$l_filter;?></strong></td>
              <td>:</td>
              <td><input type="text" id="carifilter" name="carifilter" size="40" value="<?= $carifilter; ?>" onKeyDown="etrCari(event);">
	      </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Tanggal Peminjaman</strong></td>
              <td>:</td>
              <td>
		<?= UI::createTextBox('tglrawal',$tglrawal,'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tglrawale" style="cursor:pointer;" title="Pilih tanggal Peminjaman">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglrawal",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglrawale",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		s/d
		
		<?= UI::createTextBox('tglrakhir',$tglrakhir,'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tglrakhire" style="cursor:pointer;" title="Pilih tanggal Peminjaman">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglrakhir",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglrakhire",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
	      </td>
	                    <td>&nbsp;</td>
              <td><strong>Lokasi</strong></td>
              <td>:</td>
              <td>
		<?= $l_lokasi ?>
	      </td>
            </tr>
            <tr>
              <td><strong>Tanggal Expired</strong></td>
              <td>:</td>
              <td>
		<?= UI::createTextBox('tgleawal',$tgleawal,'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tgleawale" style="cursor:pointer;" title="Pilih tanggal Expired">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgleawal",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleawale",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		s/d
		
		<?= UI::createTextBox('tgleakhir',$tgleakhir,'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tgleakhire" style="cursor:pointer;" title="Pilih tanggal Expired">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgleakhir",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleakhire",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
	      </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td rowspan="2"><input type="button" value="Filter" class="ControlStyle" onClick="goFilterEx()">
                <input type="button" value="Refresh" class="ControlStyle" onClick="goClear(); goFilter(false);">
	      </td>
            </tr>
          </table>
        </div>
	<div class="a" align="center" valign="center"><? include_once('_notifikasi_trans.php'); unset($_SESSION['reserve']) ?><br></div>	
        <br/>
	<header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
          </div>
        </header>
            <div class="table-responsive">
		<table width="100%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO. INDUK  <?= $p_xsort['e.noseri']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.judul:C');">Judul Pustaka  <?= $p_xsort['p.judul']; ?></th>		
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi <?= $p_xsort['l.namalokasi']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','t.idanggota:C');">ID Peminjam <?= $p_xsort['t.idanggota']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.nama:C');">Peminjam <?= $p_xsort['u.nama']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Tanggal Pinjam</th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Tanggal Harus Kembali</th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Tanggal Kembali</th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Perpanjang Ke</th>
			<?php
			$i = 0;
			if($showlist) {
				// mulai iterasi
				while ($row = $rs->FetchRow()) 
				{
					if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
					if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
		?>
		<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
			
			<td><?= $row['noseri']; ?></td>
			<td nowrap align="left"><p class="link" title="Lihat Detail" onclick="popup('index.php?page=show_eksemplar&id=<?= $row['idpustaka'] ?>&eks=<?= $row['ideksemplar'] ?>',850,600)"><?= Helper::limitS($row['judul']); ?></p></td>
			<td align="left"><?= $row['namalokasi']; ?></td>
			<td align="left"><?= $row['idanggota']; ?></td>
			<td align="left"><?= $row['nama']; ?></td>
			<td align="left">
				<?= $row['tgltransaksi']==$row['tgltenggat'] ? "<font color='blue'>".(Helper::tglEngTime($row['tgltransaksi']))."</font>" : Helper::tglEngTime($row['tgltransaksi']); ?>
			</td>
			<td align="left">
				<?= $row['tgltransaksi']==$row['tgltenggat'] ? "<font color='blue'>".($row['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($row['tgltenggat']))."</font>" : ($row['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($row['tgltenggat'])); ?>
			</td>
			<td align="center">
				<? if($row['tgltransaksi']==$row['tgltenggat'])
						echo "<font color='blue'>";

				   if ($row['statustransaksi']=='1')
					echo "Belum kembali";
					else
					echo  ($row['statustransaksi']==2 ? Helper::tglEngTime($row['tglpengembalian'])." *" : Helper::tglEngTime($row['tglpengembalian'])); 
				
					if($row['tgltransaksi']==$row['tgltenggat'])
						echo "</font>";
				?>			</td>
			<td align="center">
				<?= $row['tgltransaksi']==$row['tgltenggat'] ? "<font color='blue'>".$row['perpanjangke']."</font>" : $row['perpanjangke']; ?>
			</td>
		</tr>
		<?php
			}
			}
			if ($i==0) {
		?>
		<tr height="20">
			<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
		</tr>
		<?php } ?>
		<tr> 
			<td colspan="11" align="right" class="FootBG">
				<div style="float:left">
					Menuju ke halaman : <?= UI::createTextBox('nohalaman','','ControlStyle',6,6); ?> <input type="submit" name="halaman" id="halaman" onClick="goHalaman()" value="Go">
					&nbsp;&nbsp;Menampilkan <?= Helper::formatNumber($rsc)?> Data Peminjaman
				</div>
				<div style="float:right">
					Halaman
					<?= $p_page ?>
					/
					<?= $p_lastpage ?>
					<?= $p_status ?>						
				</div>
			</td>
		</tr>
	</table> 
            </div>
		<?php require_once('inc_listnav.php'); ?>
        <br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="ideks" id="ideks">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="nohal" id="nohal">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>
			<br />
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="350">
				<tr>
					<td style="border:0 none;" width=80><u>Keterangan :</u></td>
					<td style="border:0 none;" width=270> <span>-</span> <img src="images/pinjam.png" alt="" /> = Peminjaman Dibatalkan </td>
				</tr>
				
				<tr><td style="border:0 none;">&nbsp;</td>
					<td style="border:0 none;"> <span>-</span> <img src="images/ada.png" alt="" /> = Peminjaman Disetujui</td>
				</tr>
				<tr><td style="border:0 none;">&nbsp;</td>
					<td style="border:0 none;"> <span>-</span> <img src="images/Gear_32.png"/> = Peminjaman masih tahap proses</td>
				</tr>
			</table>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
			<table width=100  class="menu-body">
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
					<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
				</tr>
				<tr>
					<td class="separator"><div class="separator-line"></div></td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
				</tr>
			</table>

		</form>
		</div>
	</div>
</div>



</body>
<script src="scripts/jquery.tooltip.js"></script>
<script>
$('#test u').tooltip();
</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("filtersearch").value='';
	document.getElementById("carifilter").value='';
	document.getElementById("tglrawal").value='';
	document.getElementById("tglrakhir").value='';
	document.getElementById("tgleawal").value='';
	document.getElementById("tgleakhir").value='';
	document.getElementById("kdlokasi").value='';
	//goSubmit();
}

function goHalaman() {
	document.getElementById("nohal").value = $("#nohalaman").val();
	goSubmit();
}

function goFilterEx(){
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

function goBatalRes(ideks){
	var batal=confirm("Apakah Anda yakin akan membatalkan reservasi pustaka ini ?")
	if(batal){
		document.getElementById("act").value='batal';
		document.getElementById("ideks").value=ideks;
		goSubmit();
	}

}

function goPinjam(ideks) {
	var pinjam=confirm("Apakah Anda yakin akan menambahkan peminjaman pustaka atas reservasi pustaka ini ?")
	if(pinjam){
		document.getElementById("act").value='pinjam';
		document.getElementById("ideks").value=ideks;
		goSubmit();
	}
		
}


</script>

</html>
