<?php
	define('BASEPATH',1);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('../application/helpers/x_helper.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$salt = $Csalt;
	

	// otorisasi user
	$c_delete = false;
	$c_edit = false;
	$c_edit2 =$a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_nim = Helper::removeSpecial($_REQUEST['nim']);
	// variabel untuk upload foto anggota
	
	// definisi variabel halaman
	$p_dbtable = 'um.users';
	$p_window = '[PJB LIBRARY] Data Anggota Pustaka';
	$p_title = 'Data Anggota Pustaka';
	$p_tbwidth = 600;
	$w_tbl = 550;
	$w2 = 600;
	$p_filelist = Helper::navAddress('list_anggota.php');
			
	$p_sqlstr = "select u.*, j.nama as namajenisanggota, n.nama as namaunit  
		from um.users u 
		left join um.unit n on u.kodeunit = n.kodeunit 
		join um.status j on j.kodestatus = u.kodestatus
		where u.nid = '$r_key'";
	$row = $conn->GetRow($p_sqlstr);
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/foreditx.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr>
				<td align="center">
					<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Anggota</span></a>
				</td>
			</tr>
		</table>
		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="100%" border="0" cellspacing=0 cellpadding="0" class="GridStyle" style="border:0 none;">

		<tr><td style="border:0 none;">

			<table width="100%" border="0" cellspacing=0 cellpadding="0" class="GridStyle">
				<tr> 
					<th class="SubHeaderBGAlt thLeft" colspan="4" align="center" style="padding: 10px;background: #DBEEFF;">Identitas Anggota</th>
				</tr>
				<tr height=20> 
					<td class="LeftColumnBG thLeft">NID</td>
					<td class="RightColumnBG"><?= $row['nid']; ?></td>
					<td rowspan="8" width="200" align="center" valign="top">
						<img border="1" id="imgfoto" src="<?= Config::pictProf.xEncrypt($row['iduser']).'&s=300'; ?>" width="150" height="200"><br>
					</td>
				</tr>
				<tr height=20> 
					<td class="LeftColumnBG thLeft" width="150">Nama</td>
					<td><?= $row['nama']; ?> </td>
				</tr>
				<tr height=20>
				<td class="LeftColumnBG thLeft">Jenis Anggota</td>
				<td class="RightColumnBG"><?= $row['namajenisanggota'] ?></td>
				</tr>
				<tr height=20>
				<td class="LeftColumnBG thLeft">Status Anggota</td>
				<td class="RightColumnBG"><?= ($row['isactive']==1?"Aktif":"<font color='red'>Non Aktif</font>") ?></td>
				</tr>
				<tr height=20>
					<td class="LeftColumnBG thLeft">Unit</td>
					<td class="RightColumnBG"><?= $row['namaunit']?></td>
				</tr>
				
				<tr height=20> 
						<td class="LeftColumnBG thLeft">Jenis Kelamin</td>
						<td><?= $row['jk']=='P'?'Wanita':'Pria'?></td>
				</tr>
				<tr height=20><td class="LeftColumnBG thLeft">Alamat Sekarang</td>
					<td class="RightColumnBG">
					<?= $row['alamat']; ?>
					</td>
				</tr>
				<tr height=20> 
					<td class="LeftColumnBG thLeft">Email</td>
					<td class="RightColumnBG"><?= $row['email']; ?></td>
				</tr>
				
				<tr height=20>
					<td class="LeftColumnBG thLeft" >Telepon</td>
					<td class="RightColumnBG">	
						<?= $row['telp']; ?>
					</td>
				</tr>
				<tr height=20>
				<td class="LeftColumnBG thLeft" >Kota</td>
					<td colspan="3" class="RightColumnBG">	
						<?= $row['kota']; ?>
					</td>
				</tr>
				<tr height=20>
				<td class="LeftColumnBG thLeft" >Kode Pos</td>
					<td colspan="3" class="RightColumnBG">	
						<?= $row['kodepos']; ?>
					</td>
				</tr>			
			</table>
		</td>
		</tr>
		<tr>
			<td class="footBG">&nbsp;</td>
		</tr>
		</table>
		</div>
	</div>
</div>
</form>


</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>

</html>