<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_validasi = $a_auth['canedit'];
	
	// koneksi ke akademik
	$conna = Factory::getConnAkad();
	
	// require tambahan
	require_once('classes/honor.class.php');
	
	// definisi variabel halaman
	$p_dbtable = 'pe_ajars2';
	$p_window = '[PERPUS] Validasi Data Realisasi Ajar S2';
	$p_title = 'Validasi Data Realisasi Ajar S2';
	$p_col = 8;
	$p_tbwidth = 750;
	
	// variabel post
	$r_act = $_POST['act'];
	
	if($r_act == 'refresh')
		unset($_REQUEST['fkodeunit'],$_REQUEST['fvalid'],$_REQUEST['tglawal'],$_REQUEST['tglakhir']);
	
	if($r_act == 'filter')
		$r_kodeunit = Helper::removeSpecial($_POST['fkodeunit']);
	else
		$r_kodeunit = Helper::removeSpecial($_POST['kodeunit']);
	
	if($r_kodeunit == '' or ($r_kodeunit != '' and !Helper::rideUnit($conn,$r_kodeunit))) {
		if($_SESSION['PERPUS_CB_SATKER'] != '')
			$r_kodeunit = $_SESSION['PERPUS_CB_SATKER'];
		else {
			$r_kodeunit = $_SESSION['PERPUS_SATKER'];
			$_SESSION['PERPUS_CB_SATKER'] = $r_kodeunit;
		}
	}
	else
		$_SESSION['PERPUS_CB_SATKER'] = $r_kodeunit;
	
	if(isset($_REQUEST['valid'])) { // pasti fvalid juga set
		if($r_act == 'filter')
			$r_valid = Helper::removeSpecial($_REQUEST['fvalid']);
		else
			$r_valid = Helper::removeSpecial($_REQUEST['valid']);
		
		$_SESSION['PERPUS_CB_STVALIDS2'] = $r_valid;
	}
	else if($_SESSION['PERPUS_CB_STVALIDS2'] != '')
		$r_valid = $_SESSION['PERPUS_CB_STVALIDS2'];
	
	if($r_act == 'filter') {
		if(!empty($_REQUEST['ftglawal'])) {
			$r_ftglawal = Helper::removeSpecial($_REQUEST['ftglawal']);
			$r_tglawal = Helper::formatDate($r_ftglawal);
		}
		
		if(!empty($_REQUEST['ftglakhir'])) {
			$r_ftglakhir = Helper::removeSpecial($_REQUEST['ftglakhir']);
			$r_tglakhir = Helper::formatDate($r_ftglakhir);
		}
	}
	else {
		if(!empty($_REQUEST['tglawal'])) {
			$r_ftglawal = Helper::removeSpecial($_REQUEST['tglawal']);
			$r_tglawal = Helper::formatDate($r_ftglawal);
		}
		
		if(!empty($_REQUEST['tglakhir'])) {
			$r_ftglakhir = Helper::removeSpecial($_REQUEST['tglakhir']);
			$r_tglakhir = Helper::formatDate($r_ftglakhir);
		}
	}
	
	if($r_act == 'simpan') {
		$a_valid = array();
		$a_invalid = array();
		
		foreach($_POST['idajar'] as $t_idajar) {
			$t_stval = $_POST['cekvalid_'.$t_idajar];
			
			if(empty($t_stval))
				$a_invalid[] = $t_idajar; // kumpulkan id yang akan diset non valid
			else
				$a_valid[] = $t_idajar; // kumpulkan id yang akan diset valid
		}
		
		// proses yang non valid
		$err = 0;
		if(!empty($a_invalid))
			$err = Honor::validasiAjarS2($conn,$a_invalid,0);
		
		// proses yang valid
		if($err == 0 and !empty($a_valid))
			$err = Honor::validasiAjarS2($conn,$a_valid);
		
		if($err == 0) {
			$p_sucdb = true;
			$msgsuccess = '<font color="green"><strong>Proses set validasi berhasil.</strong></font>';
		}
		else {
			$p_errdb = true;
			$msgerror = '<font color="red"><strong>Proses set validasi gagal, mohon diulang sekali lagi.</strong></font>';
		}
	}
	
	// definisi variabel untuk paging
	$p_id = 'set_validasiajars2';
	$p_row = 10;
	
	// sql untuk mendapatkan isi list
	$infounit = $conn->GetRow("select info_lft, info_rgt from ms_satker where idsatker = '$r_kodeunit'");
	
	$p_sqlstr = "select idajar, a.npk, f_namalengkap(gelardepan,nama,gelarbelakang) as namalengkap, tglajar, kodemk, kp, skshadir, isvalid, tglvalidasi
					from $p_dbtable a join ms_satker s on s.idsatker = a.idsatker join ms_pegawai p on p.npk = a.npk
					where info_lft >= '".$infounit['info_lft']."' and info_rgt <= '".$infounit['info_rgt']."'";
	if($r_valid != '')
		$p_sqlstr .= " and isvalid = '$r_valid'";
	if($r_tglawal != '')
		$p_sqlstr .= " and tglajar >= '$r_tglawal'";
	if($r_tglakhir != '')
		$p_sqlstr .= " and tglajar <= '$r_tglakhir'";
	$p_sqlstr .= " order by tglajar, kodemk";
	
	if (isset($_POST['page'])) {
		$p_page = floor($_POST['page']);
		$_SESSION[$p_id.'.page'] = $p_page;
	}
	else if($_SESSION[$p_id.'.page'])
		$p_page = $_SESSION[$p_id.'.page'];
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		// $p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$showlist = true;
		
		// entah kenapa AtLastPage kadang salah
		if($p_page == $p_lastpage)
			$p_atlast = true;
		else
			$p_atlast = false;
	}
	
	// mendapatkan kode mk untuk mengambil nama mk
	$rsm = $conn->SelectLimit($p_sqlstr,$p_row);
	
	while($rowm = $rsm->FetchRow())
		$a_kodemk[] = $rowm['kodemk'];
	
	$in_kodemk = implode("','",$a_kodemk);
	
	// mengambil namamk
	$sql = "select KodeMK, Nama from MataKuliah where KodeMK in ('$in_kodemk')";
	$rsm = $conna->Execute($sql);
	
	while($rowm = $rsm->FetchRow())
		$a_namamk[$rowm['KodeMK']] = $rowm['Nama'];
	
	// combo unit
	$l_unit = UI::cbUnit($conn,'fkodeunit',$r_kodeunit,'class="ControlStyle"',false,'200000');
	
	// combo status validasi
	$a_stvalid = array('' => '', '0' => 'Tidak/Belum', '1' => 'Ya/Sudah');
	$l_stvalid = UI::createSelect('fvalid',$a_stvalid,$r_valid,'ControlStyle');
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth; ?>" cellpadding="4" cellspacing="0">
	<tr>
		<td align="center" colspan="6" class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td align="center" valign="bottom" colspan="6">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_validasi) { ?>
				<td width="200">
				<a href="javascript:goSave()" class="button"><span class="save">
				Set Status Validasi <?= $p_namatipe ?></span></a></td>
				<? } ?>
				<td width="200">
				<a href="javascript:goNoFilter()" class="button"><span class="refresh">
				Refresh</span></a></td>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="85">Unit</td>
		<td align="center" width="5">:</td>
		<td><?= $l_unit ?></td>
	</tr>
	<tr>
		<td>Disetujui</td>
		<td align="center">:</td>
		<td><?= $l_stvalid ?></td>
	</tr>
	<tr>
		<td>Tanggal Ajar</td>
		<td align="center">:</td>
		<td>
			<?= UI::createTextBox('ftglawal',$r_ftglawal,'ControlStyle',10,10); ?>
			<img src="images/cal.png" id="ftglawal_trg" style="cursor:pointer;" title="Pilih tanggal ujian">
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "ftglawal",
				ifFormat       :    "%d-%m-%Y",
				button         :    "ftglawal_trg",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			&nbsp; sampai &nbsp;
			<?= UI::createTextBox('ftglakhir',$r_ftglakhir,'ControlStyle',10,10); ?>
			<img src="images/cal.png" id="ftglakhir_trg" style="cursor:pointer;" title="Pilih tanggal ujian">
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "ftglakhir",
				ifFormat       :    "%d-%m-%Y",
				button         :    "ftglakhir_trg",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			&nbsp; <input type="button" value="Filter" class="ControlStyle" onClick="goFilter()">
		</td>
	</tr>
</table>
<br>
<table border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr height="20">
		<td nowrap width="70" align="center" class="SubHeaderBGAlt">Tanggal</td>
		<td nowrap width="190" align="center" class="SubHeaderBGAlt">Mata Kuliah</td>
		<td nowrap width="30" align="center" class="SubHeaderBGAlt">KP</td>
		<td nowrap width="230" align="center" class="SubHeaderBGAlt">Dosen</td>
		<td nowrap width="30" align="center" class="SubHeaderBGAlt">SKS</td>
		<td nowrap width="30" align="center" class="SubHeaderBGAlt">Sts</td>
		<td nowrap width="30" align="center" class="SubHeaderBGAlt">Ya</td>
		<td nowrap width="30" align="center" class="SubHeaderBGAlt">Tdk</td>
	</tr>
	<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
	?>
	<tr class="<?= $rowstyle ?>">
		<td align="center">
			<input type="hidden" name="idajar[]" value="<?= $row['idajar']; ?>">
			<?= Helper::formatDate($row['tglajar']); ?>
		</td>
		<td><?= $row['kodemk']; ?> - <?= $a_namamk[$row['kodemk']]; ?></td>
		<td align="center"><?= $row['kp']; ?></td>
		<td><?= $row['npk']; ?> - <?= $row['namalengkap']; ?></td>
		<td align="center"><?= $row['skshadir']; ?></td>
		<td align="center"><?= empty($row['isvalid']) ? 'T' : 'Y' ?></td>
		<? if($c_validasi) { ?>
		<td align="center"><input type="radio" name="cekvalid_<?= $row['idajar']; ?>" value="1"<?= empty($row['isvalid']) ? '' : ' checked' ?>></td>
		<td align="center"><input type="radio" name="cekvalid_<?= $row['idajar']; ?>" value="0"<?= empty($row['isvalid']) ? ' checked' : '' ?>></td>
		<? } else { ?>
		<td align="center">
			<? if(!empty($row['isvalid'])) { ?>
			<img src="images/centang.gif" border="0">
			<? } ?>
		</td>
		<td align="center">
			<? if(empty($row['isvalid'])) { ?>
			<img src="images/centang.gif" border="0">
			<? } ?>
		</td>
		<? } ?>
	</tr>
	<?php	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
	</tr>
</table>

	<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
	<input type="hidden" name="sort" id="sort">
	<input type="hidden" name="filter" id="filter">
	<input type="hidden" name="kodeunit" value="<?= $r_kodeunit ?>">
	<input type="hidden" name="valid" value="<?= $r_valid ?>">
	<input type="hidden" name="tglawal" value="<?= $r_ftglawal ?>">
	<input type="hidden" name="tglakhir" value="<?= $r_ftglakhir ?>">
	<input type="hidden" name="act" id="act">
</form>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened = false;
var menuSelObj = null;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

function goFilter() {
	document.getElementById("act").value = "filter";
	goSubmit();
}

function goNoFilter() {
	document.getElementById("act").value = "refresh";
	goRefresh();
}

function goSave() {
	document.getElementById("act").value = "simpan";
	goSubmit();
}
	
</script>

</html>