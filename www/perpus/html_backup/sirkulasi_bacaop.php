<?php
	//$conn->debug=true;
	define('BASEPATH',1);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('../application/helpers/x_helper.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Transaksi Tugas Akhir';
	$p_title = 'Transaksi Tugas Akhir';
	
	if(isset($_POST['btnbaca'])){
		$idlogin=Helper::removeSpecial($_POST['idanggotalogin']);
		
		$isLogin=$conn->GetRow("select idanggota,tglexpired, idunit from ms_anggota where idanggota='$idlogin'");
		if ($isLogin){
			if($isLogin['idunit'] != $idunit){
				echo "Anggota bukan berasal dari unit $unitlogin.";
			}else{
				if($isLogin['tglexpired']>= date('Y-m-d') or $isLogin['tglexpired']==''){
					$_SESSION['idanggotata']=$idlogin;
					Helper::redirect();
				}else{
					$errdb = 'Anggota Telah Expired.';
					Helper::setFlashData('errdb', $errdb); 	
					Helper::redirect();
				}
			}
		}else {
			$errdb = 'Masukkan Id Anggota dengan benar.';
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}
		
	
	}
	if($_SESSION['idanggotata']!=''){
		$anggota=$conn->GetRow("select * from ms_anggota a
				join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
				where a.idanggota='".$_SESSION['idanggotata']."'");
		$p_foto = Config::dirFoto.'anggota/'.trim($_SESSION['idanggotata']).'.jpg';
		$p_hfoto = Config::fotoUrl.'anggota/'.trim($_SESSION['idanggotata']).'.jpg';
		$p_mhsfoto = Config::dirFotoMhs.trim($_SESSION['idanggotata']).'.jpg';
		$p_pegfoto = Config::dirFotoPeg.trim($_SESSION['idanggotata']).'.jpg';
		
		$transnow=$conn->Execute("select * from v_trans_list where tglpengembalian is null 
								  and idanggota='".$_SESSION['idanggotata']."' and tgl_tenggat=tgl_transaksi");
		$e_sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM (";
		$e_sql .= "select *
				       from v_trans_list
				       where idanggota='".$_SESSION['idanggotata']."' and tgl_tenggat=tgl_transaksi";
		$e_sql .= ")  inner_query WHERE rownum <= 15) order by idtransaksi";
		$transe=$conn->Execute($e_sql);
		
		$p_cektenggat=$conn->GetRow("select idtransaksi from pp_transaksi where idanggota='".$_SESSION['idanggotata']."' and to_char(tgltenggat,'YYYY-mm-dd') < to_char(current_date,'YYYY-mm-dd') and tglpengembalian is null");
		$p_skors = $conn->GetRow("select keterangan from pp_skorsing where idanggota='".$_SESSION['idanggotata']."' order by idskorsing desc");
	
		$gambar = Sipus::getFoto2($conn,$_SESSION['idanggotata']);
	}

	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_seri=Helper::removeSpecial($_POST['txtpustaka']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		$r_key3=Helper::removeSpecial($_POST['key3']);
		
		if($r_aksi=='pinjambaca'){
			$getEks=$conn->GetRow("select e.ideksemplar,e.statuseksemplar,p.kdjenispustaka from pp_eksemplar e
								   join ms_pustaka p on e.idpustaka=p.idpustaka
								   where upper(e.noseri)=upper('$r_seri') and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
			if(!$getEks){
				$errdb = 'Data Pustaka tidak ditemukan.';
				Helper::setFlashData('errdb', $errdb); 	
			} else {
				if ($getEks['statuseksemplar']!='ADA') {
					$errdb = 'Pustaka dalam status terpinjam atau berstatus proses';
					Helper::setFlashData('errdb', $errdb); 	
					Helper::redirect();
				}else {
					$perjenis=$conn->GetRow("select namajenispustaka,isbaca from lv_jenispustaka where kdjenispustaka='".$getEks['kdjenispustaka']."'");
					$trans_count=$conn->Execute("select idtransaksi
								    from v_trans_list
								    where idanggota ='".$_SESSION['idanggotata']."'
									and kdjenispustaka='".$getEks['kdjenispustaka']."'
									and tgltransaksi=current_date
									and tgltransaksi=tgltenggat");
					$t_count=$trans_count->RowCount();
					if($t_count>=$perjenis['isbaca'] and $perjenis['isbaca']!=0) {
						$errdb = 'Transaksi Baca '.$perjenis['namajenispustaka'].' pada hari ini telah maksimal.';
						Helper::setFlashData('errdb', $errdb); 	
					}else{
						$record['kdjenistransaksi']='PJN';
						$record['ideksemplar']=$getEks['ideksemplar'];
						
						$record['idanggota']=$_SESSION['idanggotata'];
						$record['fix_status']=0;
						
						$record['tgltransaksi']=date('Y-m-d');
						$record['tgltenggat']=date('Y-m-d');
						$record['statustransaksi']=1;
						$record['fix_status']=0;
						Helper::IdentitasBaca($record,$_SESSION['idanggotata']);
						
						$conn->StartTrans();
						Sipus::InsertBiasa($conn,$record,pp_transaksi);
						
						$receks['statuseksemplar']='PJM';
						Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,ideksemplar,$getEks['ideksemplar']);
						
						$conn->CompleteTrans();
					
						if($conn->ErrorNo() != 0){
							$errdb = 'Transaksi Baca Pustaka Gagal.';	
							Helper::setFlashData('errdb', $errdb);
							}
						else {
							if($_SESSION['idanggotata']!=$_SESSION['PERPUS_USER'] or $r_anggota=='')
							$sucdb = 'Transakai Baca Pustaka Berhasil.';
							else
							$sucdb = 'Transaksi Baca Pustaka untuk Anggota '.$record['idanggota'].' Berhasil';
							Helper::setFlashData('sucdb', $sucdb);
							Helper::redirect();
						}
					}
				}
			}
		}
		else if($r_aksi=='delsession') {
		
			$conn->Execute("update pp_transaksi set fix_status='1' where idanggota = '".$_SESSION['idanggotata']."'");
			unset($_SESSION['idanggotata']);
			$sucdb = 'Proses transaksi telah selesai';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		
		}
		else if ($r_aksi=='batal') {
			$p_eks=$conn->GetRow("select max(ideksemplar) as ideksemplar from pp_eksemplar where noseri='$r_key2'");
			$ideksemplar=$p_eks['ideksemplar'];
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_transaksi,idtransaksi,$r_key);
			//update status
			$rec['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);

			$conn->CompleteTrans();
			
				if($conn->ErrorNo() != 0){
					$errdb = 'Transaksi gagal dibatalkan.';	
					Helper::setFlashData('errdb', $errdb);
					}
					else {
					$sucdb = 'Transaksi sukses dibatalkan.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect();
					}
			}
		else if ($r_aksi=='kembali') {
			$hariini=date("Y-m-d H:i:s");
			//$err=Sipus::fastback($conn,$r_key2,$hariini,'nolog');
			$p_eks=$conn->GetRow("select max(ideksemplar) as ideksemplar from pp_eksemplar where noseri='$r_key2'");
			$ideksemplar=$p_eks['ideksemplar'];
			$err=Sipus::fastback($conn,$r_key2,$hariini,$ideksemplar);
			 
			if($err != 0){
				$errdb = 'Pengembalian peminjaman gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Pengembalian peminjaman berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
		}
	
	
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<meta http-equiv="refresh" content="60" > 
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="<?= $_SESSION['idanggotata']=='' ? "document.getElementById('idanggotalogin').focus()" : "document.getElementById('txtpustaka').focus()" ?>">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon">SIRKULASI BACA</div>
		<? if($_SESSION['idanggotata']!='') { ?>
		
		<div><span class="out" name="btnselesai" id="btnselesai" value="Selesai" onClick="goClear()" style="padding-right:0px;cursor:pointer;"></span><span class="buttonSmall" style="cursor:pointer;background-color: #F4FA58;" onClick="goClear()">Selesai</span></div>
		<? } ?>
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
            <div class="table-responsive">
			<table cellpadding="0" cellspacing="0" align="center" width="600"  style="background:#EBF2F9;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
				<tr height="25">
					<th colspan="4">Sirkulasi Baca</td>
				</tr>
				<tr height="10">
					<td>&nbsp;</td>
				</tr>
				<tr height="22">
					<td width=160 ><b>Id Anggota</b></td>
					<td width=10>:</td>
					<td><input type="text" maxlength="20" name="idanggotalogin" class="ControlStyleT" id="idanggotalogin" onKeyDown="etrBaca(event);" value="<?= $anggota['idanggota'];?>">
					<div style="display:none"><input type="submit" name="btnbaca" class="buttonSmall" id="btnbaca"></div>
					</td>
					<td rowspan=5 align="center">
					<img border="1" id="imgfoto" src="<?= Config::pictProf.xEncrypt($anggota['idpegawai']).'&s=300'; ?>" width="110" height="110">
				</tr>
				<tr height="22">
					<td ><b>Nama Anggota </b></td>
					 <td width="10">: </td>
					 <td width="300"><b><?= $anggota['namaanggota'] ?></b></td>
					
				</tr>
				<tr height="22">
					<td><b>Jenis Keanggotaan</b></td>
					<td width="10">: </td>
					<td> <?= $anggota['namajenisanggota'] ?></td>
				</tr>		
				<tr height="22">
					<td valign="top"><b>Alamat</b></td>
					<td width="10" valign="top">: </td>
					<td > <?= $anggota['alamat'] ?></td>
				</tr>
				<tr height="22">
					<td><b>Tanggal Registrasi</b></td>
					<td width="10">: </td>
					<td> <?= Helper::formatDateInd($anggota['tgldaftar']) ?></td>
				</tr>
				<? if($anggota['tglexpired']!='') {?>
				<tr height="22">
					<td><b>Tanggal Expired</b></td>
					<td width="10">: </td>
					<td> <?= Helper::formatDateInd($anggota['tglexpired']) ?></td>
					<td>&nbsp;</td>
				</tr>	
				<? } ?>
				<? if($_SESSION['idanggotata']!='') { ?>
				<? if ($anggota['statusanggota'] !=1){ ?>
				<tr height="22">
					<td colspan="4" align="center"><font color="red"><b>
					<script>
					var sts="Anggota tersebut telah nonaktif !!!";
					document.write(sts.blink());
					</script>
					<? $aktive='block' ?>
					</b></font></td>
				</tr>
				<? }else if($anggota['statusanggota']==2){ ?>
					<tr height="22">
					<td colspan="4" align="center"><font color="red"><b>
					<script>
					var sts="Anggota tersebut telah terblokir !!!";
					document.write(sts.blink());
					</script>
					<? $aktive='block' ?>
					</b></font></td>
				</tr>	
				<? }else { ?>
				<? if ($anggota['tglselesaiskors'] >= date("Y-m-d")){ ?>
				<tr height="22">
					<td colspan="4" align="center"><font color="red"><b>
					<script>
					var skor="Anggota terkena masa skors [ <?= $p_skors['keterangan'] ?> ] sampai tanggal <?= Helper::formatDateInd($anggota['tglselesaiskors']) ?>.";
					document.write(skor.blink());
					</script>
					<? //$aktive='block' ?>
					</b></font></td>
				</tr>
				<? } ?>
				<? if($p_cektenggat) { ?>
				<tr height="22">
					<td colspan="4" align="center"><font color="red"><b>
					<script>
					var tenggat="Terdapat pustaka belum dikembalikan yang melewati masa tenggat.";
					document.write(tenggat.blink());
					</script>
					<? //$aktive='block' ?>
					</b></font></td>
				</tr>
				<? } }?>
				<? } ?>
			</table>
            </div>
            <br />
				<br>
			<table width="99%">
			<tr>	
				<td>
				<div class="tabs" style="width:100%">
				<ul>
					<li><a id="tablink" href="javascript:void(0)" onClick="goTab(0)">Peminjaman</a></li>
					
					<li><a id="tablink" href="javascript:void(0)"  onclick="goTab(3)">Sejarah Peminjaman</a></li>
					
					<div class="a" align="right" valign="center"><? include_once('_notifikasi_trans.php'); ?><br></div>
				
				</ul>
					<div style="background:#015593;width:100%;height:1px;margin-bottom:10px;"></div>

		<!-- ================================= transaksi peminjaman umum============================ -->

			<div id="items" style="position:relative;top:-2px;">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Peminjaman</h1>
						</div>
					</div>
				</header>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" class="GridStyle" style="border-bottom:0 none;border-top:1px solid #ccc;">
					<tr height="35">
						<td width="150" class="LeftColumnBG thLeft" style="border:none;">Masukkan NO INDUK</td>
						<td width="170" class="LeftColumnBG thLeft" style="border:none;">: 
						<? if($aktive=='block') { ?>
						<input type="text" name="txtpustakad" id="txtpustakad" maxlength="20" size="22" class="ControlRead" disabled>
						<? } elseif($_SESSION['idanggotata'])  { ?>
						<input type="text" name="txtpustaka" id="txtpustaka" maxlength="20" size="22" onKeyDown="etrPinjamTA(event);">
						<? } ?>
						</td>
						<td class="LeftColumnBG thLeft"  style="border:none;">
						<? if($aktive=='block') { ?>
						<input type="button" name="btnta" id="btnta" value="Pinjam Baca" class="buttonSmallDis" style="height:23px;width:130px;font-size:12px;" disabled>
						<? } elseif($_SESSION['idanggotata']) { ?>
						<input type="button" name="btnta" id="btnta" value="Pinjam Baca" onClick="goPinjamTA()" class="buttonSmall" style="height:23px;width:130px;font-size:12px;">
						&nbsp;
						<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam',700,500);">
						<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
			
						<? } ?>
						</td>
						
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="100%" border="0" class="GridStyle">
					<tr>
						<td width="6%" nowrap align="center" class="SubHeaderBGAlt thLeft">Batal</td>
						<td width="6%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kembali</td>
						<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft">NO INDUK</td>
						<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka</td>
						<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft">Jenis Pustaka</td>
						<td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>
					</tr>
					<? $i=0;
					if($_SESSION['idanggotata']){
					
					while ($row=$transnow->FetchRow()){
					if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idtransaksi'] == '0') $rowstyle = 'YellowBG';
					?>
					<tr class="<?= $rowstyle ?>" height="35">
						<td align="center">
						<? if ($row['fix_status']==0) { ?>
						<u title="Batal" onclick="goBatal('<?= $row['idtransaksi'] ?>','<?= $row['noseri'] ?>');" class="link"><img title="Batal meminjam" src="images/batal.png"></u>
						<? } else { ?>
						<u title="Batal" ><img title="Batal meminjam" src="images/batal2.png"></u>
						<? } ?></td>
						<td align="center">
						<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $row['idtransaksi']; ?>','<?= $row['noseri'] ?>');" class="link"><img src="images/kembalikan.png"></u>
						</td>
						<td>&nbsp; <?= $row['noseri'] ?></td>
						<td><?= $row['judul'].($row['edisi']!='' ? " / ".$row['edisi'] : "") ?></td>
						<td><?= $row['namajenispustaka'] ?></td>
						<td><?= $row['authorfirst1']." ".$row['authorlast1'] ?></td>
					</tr>
					<? } if ($i==0) { ?>
					<tr height="20">
						<td align="center" colspan="6"><b>Belum ada peminjaman.</b></td>
					</tr>
					<?php }
					}?>
					<tr>
						<td class="footBG" colspan="8">&nbsp;</td>
					</tr>
				</table>
			</div>
				
			<div id="items" style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Sejarah Peminjaman</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle" style="border-top:1px solid #ccc;">
					<tr height="20"> 
						<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">NO INDUK</td>
						<td width="42%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Harus Kembali</td>
						<td width="13%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Kembali</td>
						
						<?php
						$a = 0;
						if($_SESSION['idanggotata']){
							// mulai iterasi
							while ($rows = $transe->FetchRow()) 
							{
								if ($a % 2) $rowstyle3 = 'NormalBG';  else $rowstyle3 = ''; $a++; 
								if ($rows['idtransaksi'] == '0') $rowstyle3 = 'YellowBG';
					?>
					<tr class="<?= $rowstyle3 ?>" height="20" valign="middle"> 
						<td><?= $rows['noseri']; ?></u></td>
						<td align="left"><?=  Helper::limitS($rows['judul'],53).($rows['edisi']!='' ? " / ".$rows['edisi'] : "") ?></td>
						<td align="center"><?= Helper::tglEngTime($rows['tgltransaksi']); ?></td>
						<td align="center"><?= $rows['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($rows['tgltenggat']); ?></td>
						<td >&nbsp;
						<? if ($rows['tglpengembalian']=='')
							echo "Belum kembali";
							else
							echo Helper::tglEngTime($rows['tglpengembalian']);
						?></td>			
					</tr>
					<?php
						}
						if ($a==0) {
					?>
					<tr height="20">
						<td align="center" colspan="5"><b>Belum ada transaksi.</b></td>
					</tr>
					<?php } } ?>
					<tr>
						<td colspan="5" class="footBG">&nbsp;</td>
					</tr>
				</table>
				<br>
				<table align="center" cellspacing="3" cellpadding="2" width="370" border="0">
					<tr>
						<td width=270 align="center"><u>
						<script>
						var text="Sejarah transaksi diatas merupakan 15 transaksi terakhir";
						document.write(text.blink());
						</script>
						</u></td>
					</tr>
				</table>
				
				</div>
				</div>

				</td>
			</tr>
			</table>
			
		<? //} ?>
		<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key2" id="key2">
		<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">
		</form>
		</div>
	</div>
</div>
</body>
<script language="javascript">
$(document).ready(function() {

	
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	var key=document.getElementById("key3").value;
	
	if (key=='' || key==0)
	chooseTab(0);

	else if (key==1)
	chooseTab(1);
	else if(key==2)
	chooseTab(2);
	else
	chooseTab(3);

});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>
<script type="text/javascript">
function etrBaca(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnbaca").click();
}
function goPinjamTA(){
	if(cfHighlight("txtpustaka")){
		document.getElementById('act').value="pinjambaca";
		goSubmit();
	}
}
function etrPinjamTA(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById('btnta').onclick();
}
function goClear() {
	var conf=confirm("Apakah Anda yakin telah menyelesaikan peminjaman ?");
	if(conf){
	document.getElementById("act").value='delsession';
	goSubmit();
	}
}
function goBatal($key,$eks) {
		var batalno=confirm("Apakah Anda yakin akan membatalkan transaksi ini ?");
		if(batalno){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goKembali($key,$eks) {
		var kembalikan=confirm("Apakah Anda yakin akan melakukan proses pengembalian ?");
		if(kembalikan){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goTab($key3) {
		document.getElementById("key3").value=$key3;
		//document.perpusform.submit();
		
}
</script>
</html>