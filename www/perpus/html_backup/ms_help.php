<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'pp_helpdesk';
	$p_window = '[PJB LIBRARY] Digilib Helpdesk';
	$p_title = 'Digilib Guide';
	$p_tbwidth = 800;
	$p_filelist = Helper::navAddress('list_help.php');
	
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			
			
			$record = Helper::cStrFill($_POST);//,array('userid','nama','usertype','hints','statususer'));
			$record['keterangan'] = $_POST['keterangan'];
			$record['keteranganen'] = Helper::removeSpecial($_POST['keteranganen']);
			$record['tampil'] = Helper::removeSpecial($_POST['tampil']);

			if($r_key == '') { // insert record	
			
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
			}
			else { // update record
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idhelp,$r_key);
							
			}
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					$row=$_POST; //rollback
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					//Helper::redirect();
					}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					$r_key = $conn->GetOne("select last_value from pp_helpdesk_idhelp_seq");
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
			
			
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where idhelp = '$r_key'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	if(!$p_errdb) {
		if($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where idhelp = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		}
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/tinymcpuk/tiny_mce.js"></script>
	
	<script type="text/javascript">
	tinyMCE.init({
		mode: "textareas",
		theme: "advanced",
		theme_advanced_toolbar_location : "top",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,hr,removeformat,visualaid,|,charmap,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor",
		theme_advanced_buttons3 : ""
	});
	</script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Guide </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan Guide</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset Isian</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br><? include_once('_notifikasi.php'); ?>
		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data" class="GridStyle">
		<header style="width:800px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
					<table width="<?= $p_tbwidth ?>" cellpadding="4" cellspacing="0" align="center" class="GridStyle">	
						<tr> 
							<td class="LeftColumnBG thLeft">Nama Guide *</td>
							<td class="RightColumnBG"><?= UI::createTextBox('namahelp',$row['namahelp'],'ControlStyle',50,50,$c_edit); ?>
							</td>
						</tr>
						<tr> 
							<td class="LeftColumnBG thLeft">Tampilkan ?</td>
							<td><input type="checkbox" name="tampil" id="tampil" value="1" <?= $row['tampil']==1 ? 'checked' : '' ?>></td>
						</tr>
						<tr> 
							<td class="LeftColumnBG thLeft">Isi Berita<br><span style="font-weight:normal"></span></td>
							<td><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',20,75,$c_edit); ?></td>
						</tr>
					</table>



		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

		</form>
		</div>
	</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("namahelp"))
		goSave();
}

</script>
</html>