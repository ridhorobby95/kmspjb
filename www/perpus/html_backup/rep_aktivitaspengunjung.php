<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// print_r($_REQUEST);
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Sirkulasi Per Bulan';
	$p_filerep = 'repp_logsirkulasiuser';
	
	$p_tbwidth = 450;
	
	$bulan = array();
	$bulan[1] = "Januari";
	$bulan[2] = "Februari";
	$bulan[3] = "Maret";
	$bulan[4] = "April";
	$bulan[5] = "Mei";
	$bulan[6] = "Juni";
	$bulan[7] = "Juli";
	$bulan[8] = "Agustus";
	$bulan[9] = "September";
	$bulan[10] = "Oktober";
	$bulan[11] = "November";
	$bulan[12] = "Desember";
	$l_bulan = UI::createSelect('bulan',$bulan,'','ControlStyle');
		
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namasatker, kdsatker from ms_satker where kdsatker not in ('***') order by namasatker");
	$l_jurusan= $rs_cb->GetMenu2('kdjurusan','',true,false,0,'id="kdjurusan" class="ControlStyle" style="width:156"');
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);

	$rs_tahun = $conn->Execute("select distinct to_char(tanggal, 'YYYY') tahun, to_char(tanggal, 'YYYY') thn from pp_bukutamu order by tahun asc");
	$l_tahun = $rs_tahun->GetMenu2('tahun',date('Y'),true,false,0,'id="tahun" class="ControlStyle" style="width:140"');	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:450px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Sirkulasi Per Bulan</h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">Unit</td>
				<td class="RightColumnBG"><?= $l_jurusan; ?></td>
			</tr>
			<tr id="tr_tahun">
				<td class="LeftColumnBG thLeft">Periode</td>
				<td class="RightColumnBG"><?= $l_bulan; ?> - <?= $l_tahun; ?> </td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		* untuk format Word dan Excel tidak akan ikut ditampilkan grafik *
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
function goPreSubmit() {
	if(cfHighlight("tahun"))
		goSubmit();
}

</script>
</html>