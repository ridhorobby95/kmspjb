<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tahun = Helper::removeSpecial($_REQUEST['tahun']);
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	$r_jenislap = Helper::removeSpecial($_REQUEST['jenislap']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tahun=='') {
		header("location: index.php?page=home");
	}

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Data Peminjam Perpustakaan PJB <br>Tahun '.$r_tahun ;
	
	$p_namafile = 'rekap_peminjam_'.$r_tahun;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(b.idanggota) jmlanggota, to_char(b.tgltransaksi, 'dd-mm-YYYY') tgl, to_char(b.tgltransaksi, 'dd') hari, to_char(b.tgltransaksi, 'mm') bulan, j.namasatker, a.idunit kdjurusan  
		from pp_transaksi b
		join pp_eksemplar e on e.ideksemplar = b.ideksemplar 
		join ms_anggota a on a.idanggota = b.idanggota
		left join ms_satker j on j.kdsatker = a.idunit  
		where to_char(b.tgltransaksi, 'YYYY') = '$r_tahun' ";
		
	if($r_lokasi)
		$sql .=" and e.kdlokasi = '$r_lokasi' ";
		
	$sql .="group by to_char(b.tgltransaksi, 'dd-mm-YYYY'), to_char(b.tgltransaksi, 'dd'), to_char(b.tgltransaksi, 'mm'), j.namasatker, a.idunit
		order by a.idunit, j.namasatker, bulan, tgl asc";

	$rs = $conn->Execute($sql);

	$bulan = array();
	$bulan[1] = "JAN";
	$bulan[2] = "FEB";
	$bulan[3] = "MAR";
	$bulan[4] = "APR";
	$bulan[5] = "MEI";
	$bulan[6] = "JUN";
	$bulan[7] = "JUL";
	$bulan[8] = "AGTS";
	$bulan[9] = "SEP";
	$bulan[10] = "OKT";
	$bulan[11] = "NOV";
	$bulan[12] = "DES";
	
	$data = array();
	$awal = array();
	$selanjutnya = array();
	while($row = $rs->FetchRow()){
		#total detail per tanggal ke bawah
		$data['total']['jumlah']['tgl'][intval($row['bulan'])][intval($row['hari'])] = $data['total']['jumlah']['tgl'][intval($row['bulan'])][intval($row['hari'])] + $row['jmlanggota'];
		
		#total rekap per bulan kebawah
		$data['total']['jumlah']['bulan'][intval($row['bulan'])] = $data['total']['jumlah']['bulan'][intval($row['bulan'])] + $row['jmlanggota'];
		if($jur == ($row['kdjurusan']?$row['kdjurusan']:"Tidak Ada Unit")){
			#rekap
			$data['jurusan'][$jur][intval($row['bulan'])]['jml'] = $data['jurusan'][$jur][intval($row['bulan'])]['jml'] + $row['jmlanggota'];
			$data['jurusan'][$jur]['13']['jml'] = $data['jurusan'][$jur]['13']['jml'] + $row['jmlanggota'];
			#detail
			$data['jurusan'][$jur][intval($row['bulan'])][intval($row['hari'])] = $row['jmlanggota'];
		}else{
			$jur = ($row['kdjurusan']?$row['kdjurusan']:"Tidak Ada Unit");
			$data['jurusan'][$jur]['satker'] = ($row['namasatker']?$row['namasatker']:"Kosong");
			#rekap
			$data['jurusan'][$jur][intval($row['bulan'])]['jml'] = $row['jmlanggota'];
			$data['jurusan'][$jur]['13']['jml'] = $row['jmlanggota'];
			#detail
			$data['jurusan'][$jur][intval($row['bulan'])][intval($row['hari'])] = $row['jmlanggota'];
		}
		
		#total rekap per bulan total kebawah
		$data['total']['jumlah']['bulan']['13'] = $data['total']['jumlah']['bulan']['13'] + $row['jmlanggota'];
		
		#get tanggal aktif terakhir setiap bulannya
		if($awal[intval($row['bulan'])]['tgl']){
			$selanjutnya[intval($row['bulan'])]['tgl'] = intval($row['hari']);
			if($selanjutnya[intval($row['bulan'])]['tgl'] > $awal[intval($row['bulan'])]['tgl']){
				$awal[intval($row['bulan'])]['tgl'] = intval($row['hari']);
				$data['jumlahtgl'][intval($row['bulan'])] = intval($row['hari']);
			}else{
				continue;
			}
		}else{
			$awal[intval($row['bulan'])]['tgl'] = intval($row['hari']);
			$data['jumlahtgl'][intval($row['bulan'])] = intval($row['hari']);
		}

		#get bulan aktif terakhir
		if($bul){
			if(intval($row['bulan']) > $bul){
				$bul = intval($row['bulan']);
				$data['jumlahbulan'] = $bul;
			}else{
				continue;
			}
		}else{
			$bul = intval($row['bulan']);
			$data['jumlahbulan'] = $bul;
		}
		
		
	}
	
	$jumlahbulan = $data['jumlahbulan'];
	
	$categorie =  array();
	for($i=1; $i<=$jumlahbulan; $i++) {
		$categorie[] = Helper::bulanInd($i);
	}
	$categories = "'".implode("','",$categorie)."'";

?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<br><br>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Data Peminjam Perpustakaan PT Pembangkit Jawa Bali<br>Tahun <?= $r_tahun; ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="1000" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="left"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>
<?if($r_jenislap){?>
<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
	<!-- <tr><td class="SubHeaderBGAlt" colspan=2 align="center">Laporan Buku yang Dipinjam</td></tr> -->
	
	<tr>
		<th width="20%" rowspan="2">JURUSAN</th>
		<th colspan="12" align="center"> BULAN </th>
		<th rowspan="2"> JUMLAH </th>
	</tr>
	
	<!-- membuat kolom bulan sesuai semester beserta kolom judul/eksemplar -->
	<tr height="25">
		<? for ($i=1;$i<=12;$i++){?>
			<th width="130" align="center"><strong> <?= $bulan[$i] ?> </strong></th>
		<?}?>
	</tr>

	<!-- mengisikan jumlah peminjam untuk semester ganjil --> 
	<? foreach($data['jurusan'] as $jur => $value){?>
	<tr height="25">
		<td><?= $jur." - ".$data['jurusan'][$jur]['satker']; ?></td>
		<? for($i=1; $i<=12; $i++) { ?>
		<td align="right"><?= ($i<=$jumlahbulan ?number_format($data['jurusan'][$jur][$i]['jml'],0, ',', '.'):""); ?></td>
		<? } ?>
		<td align="right"><?= number_format($data['jurusan'][$jur]['13']['jml'],0, ',', '.'); ?></td>
	</tr>
	<? } ?>
	<!-- untuk SUM per Bulan tiap Judul dan Eksemplar -->
	<tr>
		<td align="right"><b>Jumlah</b> </td>
		
		<? for($i=1; $i<=12; $i++){?>
		<td align="right"><?= ($i<=$jumlahbulan ?number_format($data['total']['jumlah']['bulan'][$i],0, ',', '.'):""); ?></td>
		<? } ?>
		<td align="right"><?= number_format($data['total']['jumlah']['bulan']['13'],0,',', '.'); ?></td>
	</tr>
</table>
<br>
<table width="1150" border="1">
	<tr>
		<td align="center">
			<div class="left title">
				<h3>Grafik Peminjam Tahun <?= $r_tahun; ?></h3>
			</div>
			<div id="wrapper">
				<div class="SideItem" id="SideItem">
					<center>
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					</center>
				</div>
			</div>
		</td>
		
	</tr>	
</table>
<?}else{?>
	<? for($i=1; $i<=$jumlahbulan; $i++){
	$at = date('t', strtotime($r_tahun.'-'.$i.'-1'));	
	?>
	<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
		<tr>
			<th><?=strtoupper(Helper::bulanInd($i));?></th>
			<? for($t=1; $t<=$at; $t++){?>
			<th><?=$t;?></th>
			<? } ?>
			<th>JUMLAH</th>
		</tr>
		<? foreach($data['jurusan'] as $jur => $value){?>
		<tr height="25">
			<td width="200"><?= $jur." - ".$data['jurusan'][$jur]['satker']; ?></td>
			<? for($t=1; $t<=$at; $t++) {
				$date = $r_tahun.'-'.Helper::plusNol($i).'-'.Helper::plusNol($t);
				$h=date("w",strtotime($date));
				if($h==0 or $h==6){
					$bdc = '#F5A8A8';
					$nilai = '';
				}elseif(Sipus::isLibur($conn,$date)){
					$bdc = '#9DE6A5';
					$nilai = '';
				}else{
					$bdc = '#FFFFFF';
					$nilai = ($t<=$data['jumlahtgl'][$i]?number_format($data['jurusan'][$jur][$i][$t],0, ',', '.'):"");
				}
			?>
			<td style="background-color: <?=$bdc;?>" width="30" align="right"><?= $nilai; ?></td>
			<? } ?>
			<td width="30" align="right"><?= number_format($data['jurusan'][$jur][$i]['jml'],0, ',', '.'); ?></td>
		</tr>
		<? } ?>
		<tr>
			<td align="right"><b>Jumlah</b> </td>
			
			<? for($t=1; $t<=$at; $t++){
				$date = $r_tahun.'-'.Helper::plusNol($i).'-'.Helper::plusNol($t);
				$h=date("w",strtotime($date));
				if($h==0 or $h==6){
					$bdc = '#F5A8A8';
					$nilai = '';
				}elseif(Sipus::isLibur($conn,$date)){
					$bdc = '#9DE6A5';
					$nilai = '';
				}else{
					$bdc = '#FFFFFF';
					$nilai = ($t<=$data['jumlahtgl'][$i]?number_format($data['total']['jumlah']['tgl'][$i][$t],0, ',', '.'):"");
					$jmlhariaktif = $jmlhariaktif + $i;
				}
			?>
			<td style="background-color: <?=$bdc;?>" align="right"><?= $nilai; ?></td>
			<? } ?>
			<td align="right"><?= number_format($data['total']['jumlah']['bulan'][$i],0, ',', '.'); ?></td>
		</tr>
	</table><br/>
		<table width="1000" cellspacing="0" cellpadding="4"><tr><td align="left">
		<table width="200" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
			<tr>
				<th colspan="2">KETERANGAN</th>
			</tr>
			<tr>
				<td style="background-color: #F5A8A8" width="30%"></td>
				<td>Sabtu/Minggu</td>
			</tr>
				<?
					$liburan = Sipus::getLibur($conn,Helper::plusNol($i),$r_tahun);
					if(count($liburan)>0){
						foreach($liburan as $libur){
				?>
			<tr>
				<td valign="top" align="center" style="background-color: #9DE6A5" width="30%"><?=$libur['tgl'];?></td>
				<td><?=$libur['namaliburan'];?></td>
			</tr>
				<?
						}
					}
				?>
		</table>
		</td>
		<td align="left" valign="top"><strong>Rata-rata peminjam bulan <?=Helper::bulanInd($i).' '.$r_tahun;?> = <?=ceil($data['total']['jumlah']['bulan'][$i]/$jmlhariaktif);?> Per hari </strong></td>
		</tr></table>
	<br/><br/>
	<? } ?>
<?}?>
<br><br>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?></b></td></tr>
	<tr valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
<br/><br/><br/>
</div>
</body>
<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/highcharts/highcharts.js"></script>
<script type="text/javascript" src="scripts/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    var chart_test;
	
    $(document).ready(function() {
		
	//testing
	Highcharts.setOptions({
		colors: ['#4AA02C', '#F88017']
	});
	
        chart_tes = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {	
				title: {
                    text: 'Bulan'
                },
                categories: [<?=$categories;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Peminjam'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
							
								//return '<b>'+ this.series.name +'</b>: '+ this.y;
						}
					}
                }
            },
            series: [
			<?
			foreach($data['jurusan'] as $jur => $value){
				$bar = array();
				for($i=1; $i<=$jumlahbulan; $i++) {
					$bar[] = number_format($data['jurusan'][$jur][$i]['jml'],0, ',', '.');
				}
			?>
				{
					name: '<?=$jur." - ".$data['jurusan'][$jur]['satker'];?>',
					data: [<?=implode(",",$bar)?>]
				},
			<?
			}
			?>
		]
        });


    });    
});

</script>
</html>
