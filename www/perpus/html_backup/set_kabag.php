<?php
	//$conn->debug=true;
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$c_delete = $a_auth['candelete'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	//if(!$isAdminPusat)	
		$sqlAdminUnit = " and k.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'kabag_perpus';
	$p_window = '[PJB LIBRARY] Setting Kepala Perpustakaan';
	$p_title = 'SETTING KEPALA PERPUSTAKAAN';
	$p_tbheader = '.: SETTING KEPALA PERPUSTAKAAN:.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filelist =  Helper::navAddress('set_kabag.php');
	
	if(!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		if($r_aksi == 'simpan' and $c_add) {
			$ada = $conn->GetOne("select count(*) from kabag_perpus where idunit = '$idunit' ");
			$record = array();
			$record['iduser']=Helper::cStrNull($_POST['iduser']);
			$record['nid']=Helper::cStrNull($_POST['nid']);
			$record['nama']=Helper::cStrNull($_POST['nama']);
			$record['jabatan']=Helper::cStrNull($_POST['jabatan']);
			Helper::Identitas($record);
			if($ada>0)
				Sipus::UpdateBiasa($conn,$record,kabag_perpus,idunit,$idunit);
			else{
				$record['idunit']=$idunit;
				$record['t_inserttime']=date('Y-m-d H:i:s');;
				Sipus::InsertBiasa($conn,$record,kabag_perpus);
			}
			
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//Helper::redirect();
			}
		
		}
	}
	
	$pejabat=$conn->GetRow("select k.iduser, k.nid, k.nama, k.jabatan, k.idunit, u.namasatker
			       from kabag_perpus k
			       left join ms_satker u on u.kdsatker = k.idunit
			       where 1=1 $sqlAdminUnit
			       order by u.namasatker ");
	
?>
	
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage()">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
    <div class="LeftRibbon"> Setting Kepala Perpustakaan </div>
		<div align="center">
			<? include_once('_notifikasi.php'); ?>
			<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			  <div class="table-responsive">
                <table width="100%" border="0" align='center' cellpadding='4' cellspacing=0 class="filterTable">
				<tr   height=20> 
					<td class="LeftColumnBG thLeft">NID</td>
					<td class="RightColumnBG">
						<?= UI::createTextBox('nidd',$pejabat['nid'],'ControlStyle',200,20,$c_edit,"readonly"); ?>
						<img src="images/popup.png" style="cursor:pointer;" title="Data Pegawai" onClick="popup('index.php?page=pop_pegawai',580,430);">
						<input type="hidden" id="nid" name="nid" value="<?=$pejabat['nid'];?>" />
						<input type="hidden" id="iduser" name="iduser" value="<?=$pejabat['iduser'];?>" />
					</td>
				</tr>
				<tr   height=20> 
					<td class="LeftColumnBG thLeft">Nama</td>
					<td class="RightColumnBG">
						<?= UI::createTextBox('named',$pejabat['nama'],'ControlStyle',200,55,$c_edit,"readonly"); ?>
						<input type="hidden" id="nama" name="nama" value="<?=$pejabat['nama'];?>" />
					</td>
				</tr>
				<tr   height=20> 
					<td class="LeftColumnBG thLeft">Jabatan</td>
					<td class="RightColumnBG">
						<?= UI::createTextBox('jabatan',$pejabat['jabatan'],'ControlStyle',200,55,$c_edit); ?>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
					<input type='button' name='btnsimpan' id='btnsimpan' value='Simpan' class="buttonSmall" onClick="goSimpan()" style="cursor:pointer;height:25;width:110"> &nbsp;
					</td>
				</tr>
			</table>
                </div>
			<br>
			<br>
			<input type="hidden" name="key" id="key">	
			<input type="hidden" name="keyid" id="keyid">	
			
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="scroll" id="scroll">
			<input type="hidden" name="idsumb" id="idsumb" value="<?= $recsumb['idsumbangan']!='' ? $recsumb['idsumbangan'] : $idsumbangan ?>">
		</div>
		</div>
		</div>

</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript">
	function goSimpan(){
		if(cfHighlight("nidd,named,jabatan")) {
			document.getElementById("act").value="simpan";
			goSubmit();
		}
	}
		
</script>
<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

</script>
</html>
	