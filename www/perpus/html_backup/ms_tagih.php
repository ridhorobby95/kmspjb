<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_edit = Helper::removeSpecial($_REQUEST['edit']);

	// definisi variabel halaman
	$p_dbtable = 'pp_tagihan';
	$p_window = '[PJB LIBRARY] Data Surat tagih';
	$p_title = 'Data Surat Tagih';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('surattagih.php');
	
	 if ($r_key==''){
		 header('Location: '.$p_filelist);
		 exit();
	}
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$record = array();
		$record = Helper::cStrFill($_POST);//,array tabel
		$record['tgltagihan'] = Helper::formatDate($record['tgltagihan']);
		$record['tglexpired'] = Helper::formatDate($record['tglexpired']);
		
		
		if($r_aksi == 'simpan' and $c_edit) {
						
			if($r_edit == '') { // insert record	
				Helper::Identitas($record);
				$record['keterangan']=Helper::removeSpecial($_POST['keterangan']);
				$record['petugas'] = $_SESSION['PERPUS_USER'];
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				//penyimpanan untuk petugas dilakukan pada saat insert saja tidak bisa update
								
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				
			}
			else { // update record
				Helper::Identitas($record);
				$record['keterangan']=Helper::removeSpecial($_POST['keterangan']);
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idtagihan",$r_edit);
				
				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
			}	
				
			
		
			if($err == 0) {
				if($r_edit == ''){
					$idmax=$conn->GetRow("select max(idtagihan) as idmaxi from pp_tagihan");
					$r_edit = $idmax['idmaxi'];
					}
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key.'&edit='.$r_edit;
				Helper::redirect($url);
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"idtagihan",$r_edit);
			
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	
	if(!$p_errdb) {
		if($r_key !='' and $r_edit){
			$p_sqlstr = "select *
				from $p_dbtable 
				where idtagihan = '$r_edit'";
			$row = $conn->GetRow($p_sqlstr);	
		}
	}
	
	if ($r_key != '' or $r_edit != ''){
		$sql = "select a.idanggota, a.namaanggota from ms_anggota a where a.idanggota='$r_key' $sqlAdminUnit ";
		$rowang = $conn->GetRow($sql);
	}
	if($r_edit==''){
		$tagihan=$conn->Execute("select judul,noseri,nopanggil,edisi,tgltransaksi,tgltenggat
					from v_trans_list
					where idanggota='$r_key' and kdjenistransaksi='PJN'
						and tgltenggat<current_date and tglpengembalian is null");
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr height="40">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Tagihan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_edit!='') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><? include('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">

	<tr> 
		<td width="150" class="LeftColumnBG">Id Anggota  *</td>
		<td class="RightColumnBG"><b><?= $rowang['idanggota'] ?></b><input type="hidden" name="idanggota" id="idanggota" value="<?= $rowang['idanggota'] ?>"></td>
	</tr>	
	<tr> 
		<td class="LeftColumnBG">Nama Anggota</td>
		<td class="RightColumnBG"><b><?= $rowang['namaanggota'] ?></b></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tanggal Tagihan </td>
		<td class="RightColumnBG"><?= UI::createTextBox('tgltagihan',empty($row['tgltagihan']) ? date('d-m-Y') : Helper::formatDate($row['tgltagihan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgletagihan" style="cursor:pointer;" title="Pilih tanggal tagihan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgltagihan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgletagihan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>
		[ Format : dd-mm-yyyy ]
		</td>
	</tr>
		<tr> 
		<td class="LeftColumnBG">Tanggal Expired</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglexpired',Helper::formatDate($row['tglexpired']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleexpired" style="cursor:pointer;" title="Pilih tanggal expired">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglexpired",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleexpired",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>
		
		[ Format : dd-mm-yyyy ]
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tagihan Ke</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tagihanke',$row['tagihanke'],'ControlStyle',2,2,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG">
		<? if ($r_edit==''){ ?>
		<textarea name="keterangan" id="keterangan" cols="60" rows="5" class="ControlStyle">[ Isi pesan keterangan ]
		
<?
		$i = 1;
		while ($rowt = $tagihan->FetchRow()) 
			{
				echo $i.". ".$rowt['noseri']." | ".$rowt['nopanggil']." | ".$rowt['judul']." | ".$rowt['edisi']." | ".$rowt['tgltransaksi']." | ".$rowt['tgltenggat']." * ";
			$i++; }
		?></textarea><? } else { ?>
		<textarea name="keterangan" id="keterangan" cols="60" rows="5" class="ControlStyle"><?= $row['keterangan'] ?></textarea>
		<? } ?>
		</td>
	</tr>	
</table>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="edit" id="edit" value="<?= $r_edit ?>">

</form>
</div>
</body>

<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/forinplace.js"></script>
<script language="javascript">

$(function(){
	   $("#tgltagihan").mask("99-99-9999");
	   $("#tglexpired").mask("99-99-9999");
});

function saveData() {
	if(cfHighlight("tgltagihan"))
		goSave();
}

</script>
</html>