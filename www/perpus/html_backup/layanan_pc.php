<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Pemakaian Komputer';
	$p_title = 'Pemakaian Komputer';
	

	//hardcode jam
	$arrjam = array();
	$arrjam[] = '08:30';
	$arrjam[] = '09:30';
	$arrjam[] = '10:30';
	$arrjam[] = '11:30';
	$arrjam[] = '12:30';
	$arrjam[] = '13:30';
	$arrjam[] = '14:30';
	$arrjam[] = '15:30';
	$arrjam[] = '16:30';
	$arrjam[] = '17:30';
	$arrjam[] = '18:30';
	/*
	$sql = $conn->Execute("select idkomputer from pp_pelayanan
							where tglpelayanan=current_date and kdpelayanan='K'
							and replace('".date('H:i')."',':','') between replace(jamawal,':','') and replace(jamselesai,':','') 
							and aktifpc=1");
	*/						
	$sql = $conn->Execute("select idanggota,max(jamawal),jenis as jam,idkomputer from pp_pelayanan where kdpelayanan='K'
						   and tglpelayanan=current_date and aktifpc=1 group by jenis,idanggota,idkomputer order by max(jamawal)");
							//and replace('".date('H:i')."',':','') between replace(jamawal,':','') and replace(jamselesai,':','') 
	
	$arrpakai=array();
	while($row = $sql->FetchRow()){
		$arrpakai['idkomputer'][] = $row['idkomputer'];
		$arrpakai['jam'][] = $row['jam'];
		$arrpakai[$row['idkomputer']][$row['jam']] = $row['idanggota'];
	}
		
	if(isset($_POST['btnpc'])){
		$idlogin=Helper::removeSpecial($_POST['idanggotalogin']);
		$passwd=Helper::removeSpecial($_POST['password']);
		//$salt = $Csalt;
		//$pass=md5(md5($passwd.$salt).$salt);
		$isLogin=$conn->GetRow("select idanggota,namaanggota from ms_anggota where idanggota='$idlogin'");
		
		if ($isLogin){
			$_SESSION['idanggota']=$isLogin['idanggota'];
			$_SESSION['namaanggota']=$isLogin['namaanggota'];
			
			Helper::redirect();
		}else {
			$errdb = 'Masukkan Id Anggota dan Password dengan benar.';
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}
	
	}
	
	if (!empty($_POST)) {
		$r_aksi=Helper::removeSpecial($_POST['act']);
		
		if($r_aksi=='pinjam'){
				
				$idpel=Sipus::GetLast($conn,pp_pelayanan,idpelayanan);
				$comp=Helper::removeSpecial($_POST['keyid']);
				$jenis=Helper::removeSpecial($_POST['keytime']);
				$record=array();
				$record['idpelayanan']=$idpel;
				$record['idkomputer']=$comp;
				$record['jamawal']=date('H:i');
				$record['jamselesai']=date('H:i',(strtotime($record['jamawal']))+3600);
				$record['petugas']=$record['idanggota'];
				$record['kdpelayanan']='K';
				$record['tglpelayanan']=date('Y-m-d');
				$record['jenis']=$jenis;
				Helper::Identitas($record);
				
				if ($_SESSION['idanggota']==$_SESSION['PERPUS_USER'])
					$record['idanggota']=Helper::removeSpecial($_POST['idtemp']);
				else
					$record['idanggota']=$_SESSION['idanggota'];
				
				$cek=$conn->GetOne("select idanggota from ms_anggota where idanggota='".$record['idanggota']."'");
				if($cek){
				$err=Sipus::InsertBiasa($conn,$record,pp_pelayanan);
				if($err != 0){
					$errdb = 'Peminjaman Komputer gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Peminjaman Komputer berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect();
				}
				}else{
					$errdb = 'Data Anggota tidak ditemukan.';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				}

		}
		else if($r_aksi=='free'){
			$comp=Helper::removeSpecial($_POST['keyid']);
			$jenis=Helper::removeSpecial($_POST['keytime']);
			
			$recpel['aktifpc']=0;
			$err=Sipus::UpdateComplete($conn,$recpel,pp_pelayanan,"idkomputer='$comp' and jenis='$jenis' and tglpelayanan=current_date and aktifpc=1");
			
			if($err != 0){
				$errdb = 'Pembebasan Komputer gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembebasan Komputer berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
			
		}
		else if($r_aksi=='clear'){
			unset($_SESSION['idanggota']);
			unset($_SESSION['namaanggota']);
			unset($_SESSION['lokasi']);
			$sucdb = 'Anda telah berhasil log out';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		}
		else if($r_aksi=='ganti'){
			$lokasi = Helper::removeSpecial($_POST['lokasi']);
			$_SESSION['lokasi']=$lokasi;
			Helper::redirect();
		}
	
	}
	$cekA=$conn->GetOne("select idanggota from pp_pelayanan where idanggota='".$_SESSION['idanggota']."' and tglpelayanan=current_date");
	
	
	
	//membuat array untuk komputer
	$psql = "select * from pp_komputer where 1=1";
	if($_SESSION['lokasi']=='')
		$_SESSION['lokasi']='semua';
	
	if($_SESSION['lokasi']!='')
	$psql .=" and flag=$_SESSION[lokasi]";
	
	$psql .=" order by idkomputer";
	
	$rs = $conn->Execute($psql);
	
	
	
	$arrkomp = array();
	while ($row = $rs->FetchRow()){
		$arridkomp[] = $row['idkomputer'];
		$arrnmkomp[] = $row['namakomputer'];
	}	
	
	$a_pilih = array('semua'=>'Semua','0' => 'Perpustakaan Tenggilis', '1' => 'Perpustakaan Ngagel');
	$l_pilih = UI::createSelect('lokasi',$a_pilih,$_SESSION['lokasi'],'ControlStyle',true,'id="lokasi" onchange="goChange(this.value)"');
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<meta http-equiv="refresh" content="60" > 
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('idanggotalogin').focus()">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile ?>"  enctype="multipart/form-data">
<? if($_SESSION['idanggota']=='' and $_SESSION['ispetugas']==''){ ?>
		<table width="400" border=0 align="center" cellpadding="6" cellspacing=0 style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
			<tr height="25">
				<th colspan="2">LAYANAN PEMAKAIAN KOMPUTER</th>
			</tr>
			
			<tr>
				<td width="130"><b>ID ANGGOTA</b></td>
				<td><input type="text" maxlength="20" name="idanggotalogin" class="ControlStyleT" id="idanggotalogin"></td>
			</tr>
			<!--
			<tr>
				<td><b>PASSWORD</b></td>
				<td><input type="password" name="password" id="password" class="ControlStyleT"></td>
			</tr>
			-->
			<tr>
				<td align="center" colspan=2><input type="submit" name="btnpc" id="btnpc" value="Mulai Transaksi" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:150px;font-size:12px;"></td>
			</tr>
		</table>
<? include_once('_notifikasi.php'); ?>
<? } else { ?>
	<table>
		<tr height="20">
			<td align="center" class="PageTitle"><?= $p_title ?></td>
		</tr>
	</table>
	<? include_once('_notifikasi.php') ?>
	<table cellpadding="0" cellspacing="0" align="center" width="98%" style="border-collapse:collapse">
		<tr>
			<td id="leftcolhead" style="background:url(images/tab_header_gray.gif) no-repeat; font-size:10px;" height="25" valign="middle">
				<a href="javascript:void(0);" id="minibutton"><img id="tabbutton" align="absmiddle" border="0" src="images/tab_minimize.gif"/></a>
				<font color="#FFFFFF"><strong>Pelayanan</strong></font></td>
			<td style="background:url(images/tab_header_gray.gif) no-repeat; padding-left: 35px; font-size:10px;" height="25">
				<font color="#FFFFFF"><strong>Client Control</strong></font></td>
		</tr>
		<tr>
			<td id="leftcolmenu" valign="top" style="border: #a3b3c9 1px solid; width:300px;padding-bottom:15px" bgcolor="#f5f5f5">
			<div id="pemakaian" style="padding-top:10px">
			<? if($_SESSION['idanggota']!=''){?>
			<table width="95%" border=0 align="center" cellpadding="4" cellspacing=0 style="border:1pt solid #d2d2d2;">
				<tr height="25">
				<td colspan="2"><b><u>Identitas Anggota</u></b></td>
			  </tr>
				<tr height="20">
				<td  width="110" ><b>Id Anggota</b></td>
				<td> :&nbsp;<b><?= $_SESSION['idanggota'] ?></b></td>
				</tr>
				<tr height="20">
				<td  width="110" ><b>Nama Anggota</b></td>
				<td> :&nbsp;<b><?= $_SESSION['namaanggota'] ?></b></td>
				</tr>
				<tr>
					<td>
					<input type="button" name="btnselesai" id="btnselesai" onClick="goClear()" value="Selesai" style="padding-bottom:3px;cursor:pointer;height:23px;width:100px;font-size:12px;">
					</td>
				</tr>

			</table><br>
			<table width="95%" border=0 align="center" cellpadding="4" cellspacing=0 style="border:1pt solid #d2d2d2;">
				<tr height="25">
					<td ><b><u>Keterangan</u></b></td>
				</tr>
				<tr height="20">
					<td>
					<ol>
					  <li>Pilih Icon untuk Pemakaian Komputer</li>
					  <li>Warna Merah merupakan komputer yang sedang dipakai</li>
					  <li>Batas waktu pemakaian adalah 1 jam</li>
					</ol>
					</td>
				</tr>

			</table><br>
			<table width="95%" border=0 align="center" cellpadding="4" cellspacing=0 style="border:1pt solid #d2d2d2;">
				<tr height="25">
					<td ><b><u>Setting Lokasi</u></b></td>
				</tr>
				<tr height="20">
					<td>Pilih Lokasi : <?= $l_pilih ?></td>
				</tr>

			</table>
			<? } ?>	
			</div>
			<!--<div id="showcomp">
			 
			</div>-->
			</td>
			<td valign="top" align="left" id="contents" style="border: #a3b3c9 1px solid">
			<!-- yang ta pindah sementaraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa -->
			<br>
			<table width="650" border=1 cellpadding="4" cellspacing="0" align="center" style="border-collapse:collapse">
			<tr height="25">
				<td width="100">Komputer</td>
				<? for($i=0;$i<count($arrjam);$i++){?> 
				<td align="center"><?= $arrjam[$i]; ?></td>
				<? } ?>
			</tr>
			<? for($i=0;$i < count($arridkomp); $i++) {?>
			<tr>
				<td><?= $arrnmkomp[$i]; ?></td>
				<? for($x=0;$x < count($arrjam);$x++){ 
					if($_SESSION['idanggota']!=$_SESSION['PERPUS_USER']) {
					
				?>	
					<? if(empty($arrpakai[$arridkomp[$i]][$arrjam[$x]])){ 
					if($cekA=='') { ?>
					<td align="center" <?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? 'style="cursor:pointer"' : 'style="cursor:pointer;background-color:#FF0000"' ?> onclick="goPakai2('<?= $arridkomp[$i] ?>','<?= $arrjam[$x] ?>')" title="Pakai">
						<?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '<img src="images/tombol/user_empty.png" style="cursor:pointer">' : '<img src="images/tombol/user_in.png" title="Dipakai" style="cursor:pointer"><br>'.$arrpakai[$arridkomp[$i]][$arrjam[$x]]; ?>
					</td>
					<? } else { ?>
						<td align="center" <?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '' : 'style="background-color:#FF0000"' ?>>
						<?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '<img src="images/tombol/user_empty.png">' : '<img src="images/tombol/user_in.png" title="Dipakai"><br>'.$arrpakai[$arridkomp[$i]][$arrjam[$x]]; ?>
						</td>
					<? }
					} else { ?>
						<td align="center" <?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '' : 'style="background-color:#FF0000"' ?>>
						<?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '<img src="images/tombol/user_empty.png">' : '<img src="images/tombol/user_in.png" title="Dipakai"><br>'.$arrpakai[$arridkomp[$i]][$arrjam[$x]]; ?>
						</td>
						<?
						} ?>
					
				<? }else { ?>
					<td align="center" <?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? 'style="cursor:pointer"' : 'style="cursor:pointer;background-color:#FF0000"' ?> onclick="goPakai3('<?= $arridkomp[$i] ?>','<?= $arrjam[$x] ?>')" title="Bebas Pakai">
						<?= empty($arrpakai[$arridkomp[$i]][$arrjam[$x]]) ? '<img src="images/tombol/user_empty.png" style="cursor:pointer">' : '<img src="images/tombol/user_in.png" title="Dipakai" style="cursor:pointer"><br>'.$arrpakai[$arridkomp[$i]][$arrjam[$x]]; ?>
					</td>
				<?
				}
				} ?>
			</tr>
			<? }  ?>
			</table><br>
			</td>
		</tr>
		</table>
<? } ?>
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="keyid" id="keyid">
<input type="hidden" name="keytime" id="keytime">
<input type="hidden" name="idtemp" id="idtemp">
<input type="hidden" name="idsess" id="idsess">

<input type="hidden" name="act" id="act">

</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>

<script type="text/javascript">

function goPakai2(keyid,keytime){
	var pakai=confirm('Apakah Anda yakin akan menggunakan komputer ini?');
	if(pakai){
		document.getElementById('keyid').value=keyid;
		document.getElementById('keytime').value=keytime;
		document.getElementById('act').value="pinjam";
		goSubmit();
	}
}

function goPakai3(keyid,keytime){
	var bebas=confirm('Apakah Anda akan membebas pakaikan komputer ini?');
	if(bebas){
	var pakai=confirm('Apakah melakukan peminjaman?');
	if(pakai){
		var idtemp=prompt("Masukkan Id Anggota");
		if (idtemp!='' || idtemp!=null){
		document.getElementById('idtemp').value=idtemp;
		document.getElementById('keyid').value=keyid;
		document.getElementById('keytime').value=keytime;
		document.getElementById('act').value="pinjam";
		goSubmit();
		}else
		alert('Peminjaman dibatalkan');
	}else {
		document.getElementById('keyid').value=keyid;
		document.getElementById('keytime').value=keytime;
		document.getElementById('act').value="free";
		goSubmit();
		}
	}
}


function goClear(){
	var clear=confirm('Apakah Anda yakin akan keluar ?');
	if(clear){
		document.getElementById('act').value="clear";
		goSubmit();
	}
}

function goChange(x){
	document.getElementById('idsess').value=x;
	document.getElementById('act').value="ganti";
	goSubmit();
}
</script>
</html>