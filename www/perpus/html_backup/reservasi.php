<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	session_start();
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

#note : cek reservasi koleksi unit lain diperbolehkan seharusnya 
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Reservasi Pustaka';
	$p_title = 'RESERVASI PUSTAKA';
	$p_tbheader = '.: RESERVASI PUSTAKA :.';
	$p_col = 9;
	$p_tbwidth = 100;
	
	if (!empty($_POST)){
		$noseri=Helper::removeSpecial($_POST['txtcari']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_ideks=Helper::removeSpecial($_POST['ideks']);
		
		if($noseri!=''){
			$noseri2 = Helper::RegNew($noseri);
			$p_eks=$conn->GetRow("select t.ideksemplarpesan eksemplar, l.idunit 
					     from pp_reservasi t
					     left join pp_eksemplar e on t.ideksemplarpesan=e.ideksemplar
					     left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
					     where upper(e.noseri)=upper('$noseri') or upper(e.noseri)=upper('$noseri2')
						and t.tglexpired >= '".date('Y-m-d')."' ");
			$key=$p_eks['eksemplar'];
		
			if ($r_key=='periksa') {
				$sqlcheck=$conn->GetRow("select t.ideksemplarpesan, t.idanggotapesan, a.namaanggota, t.tglreservasi,e.kdlokasi,l.namalokasi,
								t.tglexpired, l.idunit 
							from pp_reservasi t 
							left join ms_anggota a on t.idanggotapesan=a.idanggota
							left join pp_eksemplar e on t.ideksemplarpesan=e.ideksemplar
							left join ms_pustaka m on m.idpustaka=e.idpustaka
							left join lv_lokasi l on e.kdlokasi=l.kdlokasi
							where t.ideksemplarpesan='$key' 
								and m.kdjenispustaka in(".$_SESSION['roleakses'].")");
				if($sqlcheck){
					$sqlval=$conn->GetRow("select e.idpustaka, p.judul,p.judulseri,p.edisi
							      from pp_eksemplar e
							      join ms_pustaka p on e.idpustaka=p.idpustaka
							      where e.ideksemplar='$key'");
					
					$rstrans=$conn->Execute("select t.idreservasi, t.idanggotapesan, a.namaanggota, t.tglreservasi, t.tglexpired, t.statusreservasi,
								case when t.statusreservasi = '0' 
										then 'Disetujui'
									when t.statusreservasi = '1' 
										then 'Proses' 
									when t.statusreservasi = '3' 
										then 'Dibatalkan' end as status
								from pp_reservasi t
								left join ms_anggota a on t.idanggotapesan=a.idanggota
								join pp_eksemplar e on t.ideksemplarpesan=e.ideksemplar
								join ms_pustaka p on e.idpustaka=p.idpustaka
								where t.ideksemplarpesan='$key'
								order by tglreservasi, tglexpired desc, t.statusreservasi asc ");
				}else {
					$errdb = 'Reservasi untuk pustaka tersebut tidak ditemukan.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
			elseif($r_key=='batal'){
				$rectrans = array();
				Helper::Identitas($rectrans);
				$rectrans['statusreservasi']=3;
				$err=Sipus::UpdateBiasa($conn,$rectrans,pp_reservasi,idreservasi,$r_ideks);
				
					if($err != 0){
						$errdb = 'Pembatalan Reservasi gagal.';	
						Helper::setFlashData('errdb', $errdb);
					}
					else {
						$sucdb = "Pembatalan Reservasi berhasil.";
						Helper::setFlashData('sucdb', $sucdb);
						Helper::redirect();
					}
			}
		}
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
		<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon">RESERVASI PUSTAKA</div>
		<div align="center">
		<form name="perpusform" id="perpusform" method="post">
            <div class="table-responsive">
			<table width="450" border=0 align="center" cellpadding="4" cellspacing=0 class="filterTable">
			  <tr height="50">
				<td valign="bottom" width="170"><h3>Masukkan Nomor Seri</h3></td>
				<td>
				<? if($sqlcheck){ ?>
				<input type="text" name="txtcari" id="txtcari" size="15" maxlength="15" class="ControlStyleT" value="<?= $noseri ?>" readonly>
				<? } else { ?>	
				<input type="text" name="txtcari" id="txtcari" size="15" maxlength="15" class="ControlStyleT" value="<?= $noseri ?>" onKeyDown="etrTrans(event);">
				<? }?>
				</td>
			  </tr>
			  <? if($sqlcheck){ ?>	
			<tr>
				<td colspan="2">
			
			<table width="430" border=0>
			<tr>
				<td colspan=2><b><u>Informasi Pustaka</u> :</b></td>
			</tr>
			<tr>
				<td width="160" valign="top">Judul Pustaka</td>
				<td width="10" valign="top">: </td>
				<td width="250"><?=  Helper::limitS($sqlval['judul'],53).($sqlval['judulseri']!='' ? " : ".$sqlval['judulseri'] : "").($sqlval['edisi']!='' ? " / ".$sqlval['edisi'] : "") ?></td>
			</tr>
			<tr>
				<td>Nama Lokasi</td>
				<td width="10">: </td>
				<td><?= $sqlcheck['kdlokasi']." - " .$sqlcheck['namalokasi']; ?></td>
			</tr>

			</table>
				</td>
			</tr>
			  <? } ?>
			  <tr  height="35">
				<td colspan="20" align="center">
					<? if(!$sqlcheck){ ?>	
					<input type="button" name="btntrans" id="btntrans" value="Periksa" onclick="goCheck()" class="buttonSmall" style="cursor:pointer;height:23px;width:130px;font-size:12px;padding-bottom:3px;">
					<?} else {?>
					<input type="button" name="btnbatal" id="btnbatal" value="Selesai" onclick="goSubmit();" class="buttonSmall" style="cursor:pointer;height:23px;width:130px;font-size:12px;padding-bottom:3px;">
					<? } ?>
				</td>
			  </tr>
			</table>
            </div>
                <br>
			<center><? include_once('_notifikasi_trans.php'); ?>
			<? if ($rstrans){ ?>
			<header style="width:600;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1>Daftar Reservasi Pustaka</h1>
					</div>
				</div>
			</header>
			<table width="600" border="0"  cellpadding="0" cellspacing="0" class="GridStyle">
			<tr>
				<td width="90" class="SubHeaderBGAlt thLeft" align="center">No. Anggota</td>
				<td width="120" class="SubHeaderBGAlt thLeft" align="center">Anggota Pemesan</td>
				<td width="120" class="SubHeaderBGAlt thLeft" align="center">Tanggal Reservasi </td>
				<td width="120" class="SubHeaderBGAlt thLeft" align="center">Tanggal Expired </td>
				<td width="160" class="SubHeaderBGAlt thLeft" align="center">Status Reservasi </td>
				<td width="40" class="SubHeaderBGAlt thLeft" align="center">Aksi </td>
			</tr>
			
			<? $i = 0;
				while($row=$rstrans->FetchRow())
			{				
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
			?>
			
			<tr class="<?= $rowstyle ?>" height="25">
				<td align="center"><?= $row['idanggotapesan']; ?></td>
				<td><?= $row['namaanggota']; ?></td>
				<td  align="center"><?= Helper::tglEng($row['tglreservasi']) ?> </td>
				<td  align="center"><?= ($row['tglexpired']< date('Y-m-d') ? "<blink><font color='red'>".Helper::tglEng($row['tglexpired'])."</font></blink>" : Helper::tglEng($row['tglexpired'])) ?> </td>
				<td align="center"><?= $row['status']; ?></td>
				<td align="center"><?= $row['statusreservasi'] == "1" ? "<img src=\"images/delete.png\" title=\"Pembatalan Reservasi\" onclick=\"goBatalRes('".$row['idreservasi']."');\" style=\"cursor:pointer\">" : " - " ?></td>
			</tr>
			<? } ?>
			</table>
			<? } ?>
			</center>

		<input type="hidden" name="key" id="key">
		<input type="hidden" name="ideks" id="ideks">
		<input type="text" name="xxx" id="xxx" style="visibility:hidden">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function initPage() {
		document.getElementById('txtcari').focus();
}
function goCheck(){
	var cek = document.getElementById("txtcari").value;
	if(cek!=''){
		document.getElementById("key").value='periksa';
		goSubmit();
	}else{
		alert('Isikan NO INDUK dengan benar')
		document.getElementById("txtcari").focus();
	}
}


function etrTrans(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btntrans").click();
}

function goBatalRes(ideks){
	var batal=confirm("Apakah Anda yakin akan membatalkan reservasi pustaka ini ?")
	if(batal){
		document.getElementById("key").value='batal';
		document.getElementById("ideks").value=ideks;
		goSubmit();
	}

}

</script>

</html>
