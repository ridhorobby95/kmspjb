<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	// Auth::checkRoleAuth($conng);
	
	// include tambahan
	include_once('includes/pChart/pData.class');
	include_once('includes/pChart/pChart.class');
	
	$cw = 800;
	$ch = 260;
	
	$a_data = $_SESSION['_DATA_BARVOTE'];
	
	$n_data = 0;
	foreach($a_data as $t_jmlvote)
		$n_data += $t_jmlvote;
	
	// $a_judulStatistik = array('A','B+','B','C+','C','D','E');
	$a_judulStatistik = $_SESSION['_DATA_JUDUL'];
	
	$a_jmlvote = array();
	foreach($a_judulStatistik as $t_Judul)
		$a_jmlvote[] = Helper::cEmChg($a_data[$t_Judul],'0');
	
	// definisi data set
	$DataSet = new pData;
	$DataSet->AddPoint($a_jmlvote,'SerieVote');
	$DataSet->AddPoint($a_judulStatistik,'Judul Buku');
	$DataSet->AddSerie('SerieVote');
	$DataSet->SetAbsciseLabelSerie('Judul Buku');
	
	// Initialise the graph   
	$Chart = new pChart($cw,$ch);
	$Chart->drawFilledRoundedRectangle(7,7,$cw-3,$ch-3,5,240,240,240);   
	$Chart->drawRoundedRectangle(5,5,$cw-1,$ch-1,5,230,230,230);
	
	// gambar grafik
	$Chart->setFontProperties('style/tahoma.ttf',8);
	$Chart->loadColorPalette('style/palette.txt');
	$Chart->drawPieLegend($cw-550,25,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);
	
	// sebelumnya yang votenya 0 ditiadakan :D
	$DataSet->RemoveSerie('SerieVote');
	
	$a_jmlobjekfix = array();
	$a_newindex = array();
	foreach($a_judulStatistik as $t_idx => $t_Judul) {
		$t_jumlah = $a_data[$t_Judul];
		
		if(!empty($t_jumlah)) {
			$a_jmlobjekfix[] = $t_jumlah;
			$a_newindex[] = $t_idx;
		}
	}
	
	if(empty($a_jmlobjekfix))
		$a_jmlobjekfix[] = 1;
	
	$DataSet->AddPoint($a_jmlobjekfix,'SerieObjekFix');
	$DataSet->AddSerie('SerieObjekFix');
	
	$Chart->ArrangeColorPalette($a_newindex);
	$Chart->drawPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),130,70,85,PIE_PERCENTAGE,TRUE,50,20,5);
	
	// tampilkan chart
	$Chart->Stroke();
?>