<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_seri = trim(Helper::removeSpecial($_POST['noseri']));
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Sirkulasi Pustaka';
	
	$p_namafile = 'rekap_sirkulasi'.$r_seri;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.*,a.namaanggota,namasatker, e.noseri series 
		from pp_transaksi r 
		join ms_anggota a on r.idanggota=a.idanggota
		left join ms_satker j on j.kdsatker=a.idunit
		left join pp_eksemplar e on e.ideksemplar=r.ideksemplar
		left join ms_pustaka p on p.idpustaka=e.idpustaka
		where (lower(e.noseri) like lower('%$r_seri%') or lower(p.noseri) like lower('%$r_seri%'))
		and to_date(to_char(r.tgltransaksi, 'YYYY-mm-dd'), 'YYYY-mm-dd') between to_date('$r_tgl1', 'YYYY-mm-dd') and to_date('$r_tgl2', 'YYYY-mm-dd')";
			

	$sql .=" order by r.tgltransaksi desc ";	
	$rs = $conn->Execute($sql);
	$rsc=$rs->RowCount();
	$judul=$conn->GetOne("select judul||' '|| coalesce(edisi,'') from pp_eksemplar e left join ms_pustaka p on p.idpustaka=e.idpustaka where lower(e.noseri) like lower('%$r_seri%') or lower(p.noseri) like lower('%$r_seri%') ");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Sirkulasi Pustaka</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> Kode Pustaka / No. Induk</td>
	<td>: <?= $r_seri?>
  </tr>
  <tr>
	<td> Judul</td>
	<td>: <?= $judul ?></td>
	</tr>

</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Id Anggota</strong></th>
    <th width="200" align="center"><strong>Nama Anggota</strong></th>
    <th width="200" align="center"><strong>Unit</strong></th>
    <th width="200" align="center"><strong>No. Induk</strong></th>
	<th width="100" align="center"><strong>Tanggal Transaksi</strong></th>
	<th width="100" align="center"><strong>Tanggal Kembali</strong></th>
  </tr>
  <?php
	$no=1;
	$harga = 0;
	while($row=$rs->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $row['idanggota'] ?></td>
	<td ><?= $row['namaanggota'] ?></td>
	<td ><?= $row['namasatker'];?></td>
	<td align="center"><?= $row['series']; ?></td>
	<td align="center"><?= Helper::tglEngTime($row['tgltransaksi']) ?></td>
	<td align="right"><?= $row['statustransaksi']=='0' || $row['statustransaksi']=='2' ? "<center>".Helper::tglEngTime($row['tglpengembalian'])."</center>" : "<center>Belum Kembali</center>" ?></td>
  </tr>
	<? $no++ ;} ?>
	<? if($no==1) { ?>
	<tr height=25>
		<td align="center" colspan='7' >Tidak ada sirkulasi</td>
	</tr>
	<?}else { ?>
   <tr height=25>
   <td colspan='7'><b>Jumlah : <?= $rsc ?></b></td>
   </tr>
   <? } ?>
</table>


</div>
</body>
</html>
