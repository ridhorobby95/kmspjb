<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$conn->debug = false;
	
	//variabel post 
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	
	//jika mahasiswa
	if($r_jenis == 'ALB'){	
		$tgl_expired_alb = strtotime(date("Y-m-d"))+(86400*62);
		$tgl_expired = date("Y-m-d",$tgl_expired_alb);
	}else{
		if(date('Y-m-d') > date('Y').'-07-31'){
			$tahunbaru = date('Y')+1;
			$tgl_expired =  ($tahunbaru."-07-31");
		}else
			$tgl_expired =  (date('Y')."-07-31");
	}
	
	echo Helper::formatDate($tgl_expired);
?>