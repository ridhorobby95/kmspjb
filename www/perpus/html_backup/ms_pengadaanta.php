<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	// print_r($_REQUEST);
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		$conn->debug=true;
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'pp_ta';
	$p_window = '[PJB LIBRARY] Pengadaan Skripsi/Thesis dll';
	$p_title = 'Pengadaan Skripsi/Thesis dll';
	$p_tbwidth = 700;
	$p_filelist = Helper::navAddress('list_pengadaanta.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']); 
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		if($r_aksi == 'simpan' and $c_edit) {
		
				$record = array();
				$record = Helper::cStrFill($_POST);
				$conn->StartTrans();
						
				$record['tgladministrasita']=Helper::formatDate($_POST['tgldaftarta'])." ".date('H:i:s');
				$record['qtyttb']=1;
				$record['ststtb']=1;
				$record['stsadministrasita']=1;
				$record['tgldaftarta']=Helper::formatDate($_POST['tgldaftarta']);
				$record['npkadministrasita']=$_SESSION['PERPUS_USER'];
				// $record['nrp1']=Helper::removeSpecial($_POST['idanggota']);
				$record['nrp1']=Helper::removeSpecial($_POST['noika']);
				$record['judul'] = Helper::removeSpecial($_POST['judul']);
				$record['pembimbing'] = Helper::removeSpecial($_POST['pembimbing']);
				
				Helper::Identitas($record);
				if($r_key == '') { // insert record	
					$rs_cek_mhs = $conn->GetRow("select count(*) as jml from pp_ta where idanggota='".$_POST['idanggota']."'");
					if($rs_cek_mhs['jml'] >0){
						$errdb = 'Mahasiswa dengan id anggota '.$_POST['idanggota'].' telah menyerahkan Buku Skripsi/Thesis nya.';	
						Helper::setFlashData('errdb', $errdb);
					}else{
						//$idorderpustaka=$conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq"); //Sipus::GetLast($conn,pp_orderpustaka,idorderpustaka);
						$iddaftarta=Sipus::GetLast($conn,pp_ta,iddaftarta);
						//$record['idorderpustaka']=$idorderpustaka;
						$record['iddaftarta']=$iddaftarta;	
						$record['npkdaftarta']=$_SESSION['PERPUS_USER'];
						
						// input pp_ta
						Sipus::InsertBiasa($conn,$record,$p_dbtable);
						// input pp order
						Sipus::InsertBiasa($conn,$record,pp_orderpustaka);
						
						//input pp_orderttb n ttb
						$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
						$slice=explode("/",$getttb['ttb']);
						$isttb=$slice[0]+1;
						$nottb=Helper::strPad($isttb,4,0,'l')."/TTP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y');
						$idttb=Sipus::GetLast($conn,pp_ttb,idttb);
						
						$recttb['idttb']=$idttb;
						$recttb['nottb']=$nottb;
						$recttb['tglttb']=date('Y-m-d');
						$recttb['npkttb']=$_SESSION['PERPUS_USER'];
						$recttb['jnsttb']=3;
						
						Sipus::InsertBiasa($conn,$recttb,pp_ttb);
										
						$ttb=$recttb['idttb'];
						
						$recdttb['idorderpustaka']=$conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq");
						$recdttb['idttb']=$ttb;
						$recdttb['qtyttbdetail']=1;
						$recdttb['ststtbdetail']=1;			
						Sipus::InsertBiasa($conn,$recdttb,pp_orderpustakattb);
					}
				}
				else { // update record
					$record['tgladministrasita']=Helper::formatDate($_POST['tgldaftarta'])." ".date('H:i:s');
					Sipus::UpdateBiasa($conn,$record,$p_dbtable,iddaftarta,$r_key);
					Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,iddaftarta,$r_key);
				}	
				
				$conn->CompleteTrans();
				
			if($conn->ErrorNo() != 0){				
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				$restore=1;
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				if($r_key=='')
				$r_key=$record['iddaftarta'];
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
			//print_r($record);
		}
		else if($r_aksi=='getnrp'){
			$nrp = Helper::removeSpecial(trim($_POST['nrp']));
			$nama = $conn->GetOne("select namaanggota from ms_anggota where idanggota='$nrp'");				
			
			$namae = explode(' ',$nama);
			$jnama = count($namae)-1;
		}

		else if($r_aksi == 'hapus' and $c_delete) {
			$rs_cek = $conn->GetRow("select * from pp_ta t left join pp_orderpustaka o on t.iddaftarta=o.iddaftarta
								left join pp_orderpustakattb ot on ot.idorderpustaka=o.idorderpustaka left join pp_ttb tb on tb.idttb=ot.idttb
								where t.iddaftarta=$r_key");
								
			//delete pp_orderpustakattb
			$err_ot=Sipus::DeleteBiasa($conn,'pp_orderpustakattb',idttb,$rs_cek['idttb']);
			if($err_ttb == 0){
				//delete pp_ttb
				$err_ttb=Sipus::DeleteBiasa($conn,'pp_ttb',idttb,$rs_cek['idttb']);
				if($err_ot == 0){
					//delete pp_orderpustaka
					$err_o=Sipus::DeleteBiasa($conn,'pp_orderpustaka',iddaftarta,$rs_cek['iddaftarta']);
					if($err_o == 0){
						//delete pp_ta
						$err_ta=Sipus::DeleteBiasa($conn,'pp_ta',iddaftarta,$rs_cek['iddaftarta']);
						header('Location: '.$p_filelist);
						exit();
					}else{
						$errdb = 'Penghapusan data gagal.';	
						Helper::setFlashData('errdb', $errdb);
					}
				}else{
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}else{
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			
			// if($err==0) {
				// header('Location: '.$p_filelist);
				// exit();
			// }
		}
	}
	if ($r_key !='') {
		$p_sqlstr = "select t.*,o.*,a.namaanggota from pp_ta t
					 join pp_orderpustaka o on o.iddaftarta=t.iddaftarta
					 join ms_anggota a on t.idanggota=a.idanggota
					 where t.iddaftarta=$r_key";
		$row = $conn->GetRow($p_sqlstr);		
	}
	if($restore==1)
		$row = $_POST;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('idanggota').focus()">
<div id="main_content">
  <?php include('inc_menu.php'); ?>
  <div id="wrapper">
    <div class="SideItem" id="SideItem">
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">

<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Terima</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<? if($r_key=='') { ?>
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
		<? } else { ?>
		<a href="javascript:goPrint('<?= $r_key ?>');" class="button"><span class="print">Cetak Bukti</span></a>
		<? } ?>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	
	</tr>
</table>
<br>
<?php include_once('_notifikasi.php'); ?>

        <br/>
        <header style="width:<?= $p_tbwidth ?>px">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
            
          </div>
        </header>
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr height=25> 
		<td class="LeftColumnBG" width="180">ID Anggota *</td>
		<td width="370"><b><?= UI::createTextBox('idanggota',$nama !='' ? $nrp : $row['idanggota'],'ControlStyle',20,20,$c_edit,'onKeyDown="etrNrp(event);"'); ?></b>
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=ta',620,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG" height="30">NIM Mahasiswa </td>
		<td><b><?= UI::createTextBox('noika',$row['nrp1'],'',35,35,$c_edit); ?></b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG" height="30">Nama Mahasiswa *</td>
		<td><b><?= UI::createTextBox('namaanggota',$nama !='' ? $nama : $row['namaanggota'],'ControlRead',50,50,$c_edit); ?></b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tanggal Daftar *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tgldaftarta',$row['tgldaftarta']!='' ? Helper::formatDate($row['tgldaftarta']) : date('d-m-Y'),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgldaftarta",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]<? } ?>
		</td>
	</tr>
	
	<tr height=25> 
		<td class="LeftColumnBG">Judul Pustaka *</td>
		<td class="RightColumnBG"><?= UI::createTextArea('judul',$row['judul'],'ControlStyle',2,60,$c_edit); ?></td>
		<!--<td width=10><img src="images/lookup.gif" title="Ambil Data Tugas Akhir" style="cursor:pointer" onclick="goJudul()"></td>-->
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 1 *</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst1',$r_key!='' ? $row['authorfirst1'] : str_ireplace(' '.$namae[$jnama],'',$nama),'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast1',$r_key!='' ? $row['authorlast1'] : $namae[$jnama] ,'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 2</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('nrp2',$row['nrp2'],'ControlStyle',20,20,$c_edit,'id="nrp2"'); ?> 
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=ta2',620,500);">
		<? } ?>
		[ NRP = 1040xxx ]<br>
		<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 3</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('nrp3',$row['nrp3'],'ControlStyle',20,20,$c_edit,'id="nrp3"'); ?> 
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=ta3',620,500);">
		<? } ?>
		[ NRP = 1040xxx ]<br>
		<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Pembimbing *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('pembimbing',$row['pembimbing'],'ControlStyle',300,50,$c_edit,'id="pembimbing"'); ?>
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Pembimbing" onClick="popup('index.php?page=pop_anggota&code=d',620,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? //if($c_edit) { ?><!--<img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">-->
			<? //} ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table><br>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="nrp" id="nrp" value="<?= $nrp ?>">
<input type="hidden" name="nrpne2" id="nrpne2" value="<?= $nrp2 ?>">
<input type="hidden" name="nrpne3" id="nrpne3" value="<?= $nrp3 ?>">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

</form>
</div>
</div>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tgldaftarta").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function etrNrp(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		goNrp();
	}
}


function saveData() {
	if(cfHighlight("idanggota,judul,pembimbing,penerbit,tgldaftarta,tahunterbit,authorfirst1"))
		goSave();
}

function goNrp(){
	var x = document.getElementById('idanggota').value;
	document.getElementById('act').value='getnrp';
	document.getElementById('nrp').value=x;
	
	goSubmit();

}


function goPrint(key){
	popup('index.php?page=nota_ta&id='+key,600,500);
}

function goJudul(){
	file = "<?= Helper::navAddress('sys_judulta.php'); ?>";
      $.ajax({
          type: "post",
          url: file,
          data: "idanggota="+ $("#idanggota").val(),
          cache: false,
          success: function(data){
			var hasil = data.split("*");
        		$("#judul").val(hasil[0]);
			$("#pembimbing").val(hasil['1']);
          }
      });
      return false;
}
</script>
</html>