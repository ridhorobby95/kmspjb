<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::formatDate($_REQUEST['tgl1']);
	$r_tgl2 = Helper::formatDate($_REQUEST['tgl2']);
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Statistik Koleksi Terpinjam per Fakultas di Perpustakaan PJB <br>Bulan '.Helper::indoMonth((int)substr($r_tgl1,5,2)).' '.substr($r_tgl1,0,4). ' - '.Helper::indoMonth((int)substr($r_tgl2,5,2)).' '.substr($r_tgl2,0,4) ;
	
	$p_namafile = 'rekap_buku_dipinjam_fakultas_'.Helper::formatDate($r_tgl1).'-'.Helper::formatDate($r_tgl2);
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select lj.kdjurusan,lj.namajurusan,count(distinct ppe.idpustaka) as jumlah_judul, count(ppt.ideksemplar) as jumlah_eksemplar from pp_transaksi ppt join pp_eksemplar ppe on ppe.ideksemplar = ppt.ideksemplar
			join ms_pustaka msp on msp.idpustaka = ppe.idpustaka
			join ms_anggota a on a.idanggota=ppt.idanggota
			left join lv_jurusan lj on lj.kdjurusan=a.idunit
			where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2'
			group by lj.kdjurusan,lj.namajurusan
			order by lj.kdjurusan asc";

	$rs = $conn->Execute($sql);
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<br><br>
<table width=900 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Statistik Koleksi Terpinjam di Perpustakaan PT Pembangkitan Jawa Bali<br>Bulan <?= Helper::indoMonth((int)substr($r_tgl1,5,2))==Helper::indoMonth((int)substr($r_tgl2,5,2)) ? Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4) : Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4)." - ".Helper::indoMonth((int)substr($r_tgl2,5,2))." ".substr($r_tgl2,0,4) ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="900" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="left"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>
<table width="900" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
	<!-- <tr><td class="SubHeaderBGAlt" colspan=2 align="center">Laporan Buku yang Dipinjam</td></tr> -->
	
	<tr>
		<th rowspan="2"> No. </th>
		<th rowspan="2"> Kode </th>
		<th rowspan="2"align="center"> Organisasi </th>
		<th colspan="2"> JUMLAH </th>
	</tr>
	<tr>
		<th>Jumlah Judul</th>
		<th>Jumlah Eksemplar</th>
	</tr>
	<? $i=1;$jml_judul=0;$jml_eks=0;while($row = $rs->FetchRow()){?>
	<tr>
		<td><?= $i?></td>
		<td><?= $row['kdjurusan']?></td>
		<td><?= $row['namajurusan']?></td>
		<td align="right"><?= $row['jumlah_judul']?></td>
		<td align="right"><?= $row['jumlah_eksemplar']?></td>
	</tr>
	<?$i++;$jml_judul=$jml_judul+$row['jumlah_judul']; $jml_eks=$jml_eks+$row['jumlah_eksemplar'];}?>
	<tr><td colspan=3 align="right"><b>Jumlah</b></td>
		<td align="right"><b><?= $jml_judul?></b></td>
		<td align="right"><b><?= $jml_eks?></b></td>
	</tr>
</table>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="900" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b>Kabag Perpustakaan PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>