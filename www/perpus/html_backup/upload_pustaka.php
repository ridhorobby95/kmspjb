<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	$targetDir = Config::dirMigrasi;
	$targetUrl = Config::migrasiUrl;
	$targetTemp = Config::dirTemp;
	
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);

	$nyear = date("Y");
//$conn->debug=true;
	// definisi variabel halaman
	$p_dbtable = 'ms_pustaka';
	$p_window = '[PJB LIBRARY] Data Migrasi Pustaka';
	$p_title = 'Migrasi Pustaka';
	$p_tbwidth = 600;
	$not = array('.','..');
	
	$connLamp = Factory::getConnLamp();
$connLamp->debug=true;
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);		
		
		if($r_aksi == 'simpan' and ($c_add or $c_edit)) {

/*echo "<pre>";
print_r($_FILES['fileexcel']);
echo "</pre>";
var_dump(UPLOAD_ERR_OK);
die($_FILES['fileexcel']['error']);*/

			$alert = "";
			
			if($_FILES['fileexcel']['error'] == UPLOAD_ERR_OK)
			{
				$filename = $_FILES['fileexcel']['name'];
				$tmp_file = $_FILES['fileexcel']['tmp_name'];
				$filetype = $_FILES['fileexcel']['type'];
				$filesize = $_FILES['fileexcel']['size'];
				$destination = $targetDir .'/'.$filename;				
				$r_file = array();
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$dirfname = str_replace(".".$ext,"",$filename);

				if ($filetype=='application/vnd.ms-excel') {  
					if (@move_uploaded_file($tmp_file, $destination))
					{					
						$alert_upload = true;
					}else{
						$alert_upload = false;
						$errdb = '<font color="red"><strong>File excel gagal terupload, silakan ulangi lagi.</strong></font>';
						Helper::setFlashData('errdb', $errdb);
						
						$parts = Explode('/', $_SERVER['PHP_SELF']);
						$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] ;
						Helper::redirect($url);
					}
				}else{
					$alert_upload = false;
					$p_errdb = true;
					$errdb = '<font color="red"><strong>Format File salah, silakan ulangi lagi.</strong></font>';
					Helper::setFlashData('errdb', $errdb);
					
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] ;
					Helper::redirect($url);
				}

				if($alert_upload){
					include 'excel_reader.php';     // include the class
					$excel = new PhpExcelReader;
					$excel->read($destination);
					$nr_sheets = count($excel->sheets);
					$jmlkolom = $excel->sheets[0]['numCols'];
					$jmldata = $excel->sheets[0]['numRows'];
					
					//echo "jumlah kolom : ".$jmlkolom."<br/>";
					//echo "jumlah data : ".$jmldata."<br/>";
					//die();
					
					$x = 6;
					$baris = 0;
					$data = array();
					while($x <= $jmldata) {	
						$baris++;
						$alert .= "<br/>----------Data Urutan $baris-----------";
						$y = 1;
						while($y <= $jmlkolom) {
							$data[$x][$y] = Helper::isYes($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : null ;
							$y++;
						}
						
						$upload = false;
						
						/*echo "<pre>";
						print_r($data[$x]);
						echo "</pre>";
						die();*/
						
						$penerbit = array();
						$penerbit['namapenerbit'] = ($data[$x][8]?$data[$x][8]:$data[$x][16]);
						$penerbit['alamat'] = $data[$x][17];
						$penerbit['kota'] = $data[$x][18];
						$penerbit['kodepos'] = $data[$x][19];
						$penerbit['telp'] = $data[$x][20];
						$penerbit['fax'] = $data[$x][21];
						$penerbit['email'] = $data[$x][22]; 
						$penerbit['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
						$penerbit['t_updatetime']=date('Y-m-d H:i:s');
						$penerbit['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
						$idpenerbit_ada = $conn->GetOne("select idpenerbit from perpus.ms_penerbit where trim(namapenerbit) = trim('".$penerbit['namapenerbit']."') ");
						if($idpenerbit_ada){
							$penerbit['idpenerbit'] = $idpenerbit_ada;
							$alert .= "<br/><span style='color:blue;'>Data Penerbit baris $x sudah ada.</span>";
						}else{
							$idpenerbit_new = $conn->GetOne("select coalesce(max(idpenerbit),0)+1 from perpus.ms_penerbit");
							$penerbit['idpenerbit'] = $idpenerbit_new;
							$err_penerbit = Sipus::InsertBiasa($conn,$penerbit,'perpus.ms_penerbit');
							if($err_penerbit > 0){
								$alert .= "<br/><span style='color:red;'>Data Penerbit baris $x Gagal dimigrasikan.</span>";
							}else{
								$alert .= "<br/><span style='color:green;'>Data Penerbit baris $x berhasil dimigrasikan.</span>";
							}
						}
						
						$pengarang = array();
						$pengarang['namadepan'] = $data[$x][25];
						$pengarang['namabelakang'] = $data[$x][26]; 
						$pengarang['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
						$pengarang['t_updatetime']=date('Y-m-d H:i:s');
						$pengarang['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
						$idauthor_ada = $conn->GetOne("select idauthor from perpus.ms_author where trim(namadepan) = trim('".$pengarang['namadepan']."') and  trim(namabelakang) = trim('".$pengarang['namabelakang']."') ");
						if($idpenerbit_ada){
							$pengarang['idauthor'] = $idauthor_ada;
							$alert .= "<br/><span style='color:blue;'>Data Pengarang baris $x sudah ada.</span>";
						}else{
							$idauthor_new = $conn->GetOne("select coalesce(max(idauthor),0)+1 from perpus.ms_author");
							$pengarang['idauthor'] = $idauthor_new;
							$err_pengarang = Sipus::InsertBiasa($conn,$pengarang,'perpus.ms_author');
							if($err_pengarang > 0){
								$alert .= "<br/><span style='color:red;'>Data Pengarang baris $x Gagal dimigrasikan.</span>";
							}else{
								$alert .= "<br/><span style='color:green;'>Data Pengarang baris $x Berhasil dimigrasikan.</span>";
							}
						}
						
						$pustaka = array();
						$pustaka['idpustaka'] = $conn->GetOne("select coalesce(max(idpustaka),0)+1 from perpus.ms_pustaka");
						$pustaka['kdbahasa'] = ($data[$x][12]?$data[$x][12]:"ID");
						$pustaka['kdjenispustaka'] = $data[$x][14];
						$pustaka['idpenerbit'] = $penerbit['idpenerbit'];
						$pustaka['noseri'] = $pustaka['idpustaka'];
						$pustaka['judul'] = $data[$x][3];
						//$pustaka['judulseri'] = null;
						//$pustaka['nopanggil'] = null;
						$pustaka['kodeddc'] = $data[$x][5];
						$pustaka['tglperolehan'] = $data[$x][6];
						$pustaka['kota'] = $data[$x][7];
						$pustaka['namapenerbit'] = $data[$x][8];
						$pustaka['edisi'] = $data[$x][9];
						$pustaka['isbn'] = $data[$x][10];
						$pustaka['sinopsis'] = $data[$x][11];
						$pustaka['kdjenisbuku'] = $data[$x][23];
						$pustaka['jenisbuku'] = $data[$x][24];
						$pustaka['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
						$pustaka['t_updatetime']=date('Y-m-d H:i:s');
						$pustaka['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
						$err_pustaka = Sipus::InsertBiasa($conn,$pustaka,'perpus.ms_pustaka');
						if($err_pustaka > 0){
							$alert .= "<br/><span style='color:red;'>Data pustaka baris $x Gagal dimigrasikan.</span>";
						}else{
							$alert .= "<br/><span style='color:green;'>Data pustaka baris $x Berhasil dimigrasikan.</span>";
							$upload = true;
						}
						
						$eksemplar = array();
						$eksemplar['ideksemplar'] = $conn->GetOne("select coalesce(max(ideksemplar),0)+1 from perpus.pp_eksemplar");
						$eksemplar['idpustaka'] = $pustaka['idpustaka'];
						$eksemplar['kdklasifikasi'] = "SR"; //sirkulasi
						$eksemplar['kdkondisi'] = "V"; //available
						$eksemplar['kdperolehan'] = "B"; //beli
						$eksemplar['kdlokasi'] = $data[$x][14];
						$eksemplar['statsueksemplar'] = "ADA";
						$eksemplar['t_entrytime'] = date('Y-m-d H:i:s');
						$eksemplar['noseri'] = $data[$x][4];
						$eksemplar['tglperolehan'] = $data[$x][6];
						$eksemplar['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
						$eksemplar['t_updatetime']=date('Y-m-d H:i:s');
						$eksemplar['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
						$err_eksemplar = Sipus::InsertBiasa($conn,$eksemplar,'perpus.pp_eksemplar');
						if($err_eksemplar > 0){
							$alert .= "<br/><span style='color:red;'>Data eksemplar baris $x Gagal dimigrasikan.</span>";
						}else{
							$alert .= "<br/><span style='color:green;'>Data eksemplar baris $x Berhasil dimigrasikan.</span>";
						}
						
						$aut_pustaka = array();
						$aut_pustaka['idauthor'] = $pengarang['idauthor'];
						$aut_pustaka['idpustaka'] = $pustaka['idpustaka'];
						if($aut_pustaka['idauthor'] and $aut_pustaka['idpustaka']){
							$err_aut_pustaka = Sipus::InsertBiasa($conn,$aut_pustaka,'perpus.pp_author');
							if($err_aut_pustaka > 0){
								$alert .= "<br/><span style='color:red;'>Data pengarang pustaka baris $x Gagal dimigrasikan.</span>";
							}else{
								$alert .= "<br/><span style='color:green;'>Data pengarang pustaka baris $x Berhasil dimigrasikan.</span>";
							}
						}else{
							$alert .= "<br/><span style='color:green;'>Data pengarang pustaka baris $x Gagal dimigrasikan. [idauthor : ".$pengarang['idauthor'].' idpustaka : '.$pustaka['idpustaka']."]</span>";
						}

//$dir = $targetTemp.$data[$x][2]."/".$dirfname."/".$data[$x][1]."/";die($dir);

						if($upload){
							$dir = $targetTemp.$data[$x][2]."/".$dirfname."/".$data[$x][1]."/";//die($dir);
							if (is_dir($dir)){
								if ($dh = opendir($dir)){
									while (($file = readdir($dh)) !== false){
										if (!in_array($file, $not)) {
											//echo "filename:" . $file . "<br>";
											$ext = pathinfo($file, PATHINFO_EXTENSION);
											//echo "extension:" . $ext . "<br>";
											$content = $dir.$file;
											$record = array();
											$record['idlampiran'] = $pustaka['idpustaka'];
											#jenis : 1= cover, 2=sinopsis, 3=file pustaka, 4= foto
											$record['tiduser'] = '1'; #test
											$record['filename'] = $file;
											#image cover
											if(trim(strtolower($file)) == "cover.jpg" ){
												$record['jenis'] = '1';
											}
											#sinopsis jika ada
											elseif(trim(strtolower($file)) == "sinopsis.pdf"){
												$record['jenis'] = '2';
											}
											#lampiran pustaka
											else{
												$record['jenis'] = '3';
											}
											$err = Sipus::InsertBlob($connLamp,$record,'lampiranperpus',$content,'idlampiranperpus','isifile');
											
											//#image cover
											//if(trim(strtolower($file)) == "cover.jpg" ){
											//	$ext = pathinfo($file, PATHINFO_EXTENSION);
											//	$content = file_get_contents($dir.$file);
											//	$sql = "insert into lampiranperpus (idlampiran, jenis, isifile, filename)
											//		values(".$pustaka['idpustaka'].",'1','$content','$file') ";
											//	$err = $connLamp->Execute($sql);
											//}
											//#sinopsis jika ada
											//elseif(trim(strtolower($file)) == "sinopsis.pdf"){
											//	$ext = pathinfo($file, PATHINFO_EXTENSION);
											//	$content = file_get_contents($dir.$file);
											//	$sql = "insert into lampiranperpus (idlampiran, jenis, isifile, filename)
											//		values(".$pustaka['idpustaka'].",'2','$content','$file') ";
											//	$err = $connLamp->Execute($sql);
											//}
											//#lampiran pustaka
											//else{
											//	$ext = pathinfo($file, PATHINFO_EXTENSION);
											//	$content = file_get_contents($dir.$file);
											//	$sql = "insert into lampiranperpus (idlampiran, jenis, isifile, filename)
											//		values(".$pustaka['idpustaka'].",'3','$content','$file') ";
											//	$err = $connLamp->Execute($sql);
											//}
											
											
											
											
											
											
										}
									}
									closedir($dh);
								}
							}
						}
						die();	
						$x++;
					}
					
				}else{
					echo "upload gagal";
					unlink($destination);
				}
					
					
					
			}else{
				$errdb = 'Data Excel Error.';	
				Helper::setFlashData('errdb', $errdb);
			}
			unlink($destination);
			
			/*if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}	*/

			
		}
		
	}
					
?>
<html>
<head>
	<title><?= $p_window ?></title>
<link rel="shortcut icon" href="images/favicon.ico"> 
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body>
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
<div class="SideItem" id="SideItem">
<div class="LeftRibbon">
<?= $p_title ?>
</div>
<div align="center">
<table width="100">
	<tr>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key!='') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><? include('_notifikasi.php'); ?>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">

	<tr> 
		<td width="150" class="LeftColumnBG"><strong>File Excel</strong> </td>
		<td class="RightColumnBG"><input type="file" name="fileexcel" id="fileexcel" class="ControlStyle" size="33" /></td>
	</tr>
	<tr>
		<td colspan="2"><em>* Extension excel => .xls</em></td>
	</tr>
	<?php if($alert){?>
	<tr>
		<td colspan="3"><?=$alert;?></td>
	</tr>
	<?php } ?>
</table>



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">

</form>
</div>
</div>
</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("fileexcel"))
		goSave();
}

</script>
</html>