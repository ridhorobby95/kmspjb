<?php
	if (!empty($_POST)){
		$key=Helper::removeSpecial($_POST['txtcari']);
		if($key==''){
		$errdb = 'Masukkan Id Eksemplar dengan benar.';	
		Helper::setFlashData('errdb', $errdb);
		Helper::redirect();
		}else{
		$sqlcheck=$conn->GetRow("select t.idanggota, a.namaanggota, t.tgltransaksi, t.tgltenggat, t.perpanjangke from pp_transaksi t join ms_anggota a on t.idanggota=a.idanggota where t.ideksemplar=$key and t.tglpengembalian is null");
		if($sqlcheck)	
		$sqlval=$conn->GetRow("select e.idpustaka, p.judul from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka where e.ideksemplar=$key");
		else {
		$errdb = 'Peminjaman untuk pustaka tersebut tidak ditemukan.';	
		Helper::setFlashData('errdb', $errdb);
		Helper::redirect();
		}
		} 
			}
	if(isset($_POST['btnfast'])){
		$key2=Helper::removeSpecial($_POST['txt']);
		Sipus::fastback($conn,$key2);
		 
			if($conn->ErrorNo() != 0){
				$errdb = 'Pengembalian Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Pengembalian Pustaka berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}}
if($sqlcheck) {		
?>
<form id="checkfast" name="checkfast" method="post">
<table width="530" border=0>
	<tr>
		<td colspan=2><b>Informasi Pustaka</b></td>
	</tr>
	<tr>
		<td>Judul Pustaka</td>
		<td>: <?= $sqlval['judul']; ?> <input type="text" name="txt" id="txt" value="<?= $key ?>"></td>
	</tr>
	
	<tr>
		<td>Peminjam</td>
		<td>: <?= $sqlcheck['idanggota']." - " .$sqlcheck['namaanggota']; ?></td>
	</tr>
	<tr>
		<td>Tanggal Terpinjam</td>
		<td>: <?= Helper::formatDateInd($sqlcheck['tgltransaksi']); ?> </td>
	</tr>
	<tr>
		<td>Tanggal Harus Kembali</td>
		<td>: <?= Helper::formatDateInd($sqlcheck['tgltenggat']); ?> </td>
	</tr>
	<tr>
		<td>Perpanjang Ke</td>
		<td>: <?= $sqlcheck['perpanjangke']; ?> </td>
	</tr>
	<tr>
		<td colspan=2 align="center"><input type="button" name="btnfast" id="btnfast" value="Pengembalian"  style="cursor:pointer;" onclick="document.checkfast.submit();"></td>
	</tr>
</table>
</form>
<? } else { ?>
<span style="color:red;">Peminjaman untuk pustaka tersebut tidak ditemukan.</span>
<? } ?>