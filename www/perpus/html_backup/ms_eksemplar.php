<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$p_eksemplar = Helper::navAddress('ms_eksemplar.php');
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Data Eksemplar';
	$p_title = 'Data Eksemplar';
	$p_tbwidth = 978;
	$p_filelist = Helper::navAddress('list_pustaka.php');
	$p_filelist2 = Helper::navAddress('list_pustakata.php');
	$p_ekslist = Helper::navAddress('list_eksemplar.php');
	
	// variabel esensial
	if(Helper::removeSpecial($_GET['idp'])!=''){
		$r_pus = Helper::removeSpecial($_GET['idp']); //insert
	} else{
		$r_pus = Helper::removeSpecial($_POST['key3']); //update
	}
	if(Helper::removeSpecial($_GET['ideks'])!=''){
		$r_eks = Helper::removeSpecial($_GET['ideks']);
	}
	else{
		$r_eks = Helper::removeSpecial($_POST['key']);//ideksemplar
		$r_key = Helper::removeSpecial($_POST['key']);;
	}
	
	if ($r_pus==''){
		header('Location: index.php?page=home');
		exit();
		
	}
	$sqlpustaka="select idpustaka, judul, kdjenispustaka, noseri as noseripustaka,
	(select islokal from lv_jenispustaka where kdjenispustaka=ms_pustaka.kdjenispustaka) as lokal from ms_pustaka where idpustaka=$r_pus";
	$rowp= $conn->GetRow($sqlpustaka);
	if($rowp['lokal']==0){
		$countP=Sipus::GetCount($conn,$rowp['idpustaka']);
		$iscount=str_pad($countP,2,'0',STR_PAD_LEFT);
		$seripustaka=Helper::removeSpecial($rowp['noseripustaka']);
		$serie = Sipus::GetESeri($conn, $rowp['idpustaka'], $seripustaka);
	}else{
		$countP=Sipus::GetCount($conn,$rowp['idpustaka']);
		$iscount=str_pad($countP,2,'0',STR_PAD_LEFT);
		$seripustaka=Helper::removeSpecial($rowp['noseripustaka']);
		$serie = Sipus::GetESeri($conn, $rowp['idpustaka'], $seripustaka);
	}
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
			
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record = Helper::cStrFill($_POST);
			
			if($r_eks == '') { // insert record	
				$lastid=Sipus::GetLast($conn,$p_dbtable,ideksemplar);
				$record['ideksemplar']=$lastid;
				$record['tglpengadaan']=date("Y-m-d");
				$record['statuseksemplar']='ADA';
				$record['tglperolehan']=Helper::formatDate($_POST['tglperolehan']);
				$record['tglterbit']=Helper::formatDate($_POST['tglterbit']);

				$rs_cek_noseri = $conn->GetRow("select count(*) as jml from pp_eksemplar where noseri='".$record['noseri']."'"); 
				
				Helper::Identitas($record);
				
				if($rs_cek_noseri['jml'] > 0){
					$record['noseri'] = Sipus::GetESeri($conn, $record['idpustaka'], $seripustaka);
				}
				
				Sipus::InsertBiasa($conn,$record,$p_dbtable);	
				if($conn->ErrorNo()!= 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				$r_pus=$record['idpustaka'];
				
				if ($conn->ErrorNo() == 0){
					$r_pus = $record['idpustaka'];
					
					$r_last=$conn->GetRow("select max(ideksemplar) as idmax from pp_eksemplar where idpustaka=$r_pus");
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&idp='.$r_pus.'&ideks='.$r_last['idmax'];
					Helper::redirect($url);
				}
			}
			else { // update record
				$record['noseri'] = Helper::removeSpecial($_POST['noseri']);
				$record['tglperolehan']=Helper::formatDate($_POST['tglperolehan']);
				$record['tglterbit']=Helper::formatDate($_POST['tglterbit']);
				Helper::Identitas($record);
				
					$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,ideksemplar,$r_eks);
					if($err != 0){
						$errdb = 'Update data gagal.';	
						Helper::setFlashData('errdb', $errdb);
					}else {
						$sucdb = 'Update data berhasil.';	
						Helper::setFlashData('sucdb', $sucdb);
					}
					$r_pus=$record['idpustaka'];
			}
			
			if($conn->ErrorNo() == 0 and empty($errdb)) {
				if($r_key == '')
					$r_key = $record['ideksemplar'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			} 
			
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where ideksemplar = '$r_eks'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_ekslist);
				exit();
			}else{
				$errdb = 'Penghapusan Data Gagal. Data sedang digunakan.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		
	}
	
	if($r_eks!='') { // update cari data eksmeplar
		$p_sqlstr = "select e.*, l.namalokasi, k.namaklasifikasi, d.namakondisi, pe.namaperolehan, r.namarak, l.idunit 
			from $p_dbtable e 
			left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			left join lv_klasifikasi k on e.kdklasifikasi=k.kdklasifikasi
			left join lv_kondisi d on e.kdkondisi = d.kdkondisi
			left join lv_perolehan pe on e.kdperolehan=pe.kdperolehan
			left join ms_rak r on e.kdrak=r.kdrak
			where e.ideksemplar = $r_eks";
		$row = $conn->GetRow($p_sqlstr);	
		
		if($row['idunit'] != $_SESSION['PERPUS_SATKER']){
			$c_delete = false;
			$c_edit = false;
		}
	}else{ // insert
		$rs_data_eks_pertama = "select * 
					from pp_eksemplar e
					left join lv_lokasi l on l.kdlokasi=e.kdlokasi
					where idpustaka='$r_pus'
					order by ideksemplar asc ";		
		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM (";
		$sql .= $rs_data_eks_pertama." )  inner_query WHERE rownum = 1) ";
		$row = $conn->GetRow($sql);
		if($row['idunit'] != $_SESSION['PERPUS_SATKER']){
			$row['kdlokasi'] = null;
			$row['namalokasi'] = null;
		}
	}

	if($c_edit) {
		$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");
		$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$row['kdklasifikasi'],true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$row['kdkondisi'],true,false,0,'id="kdkondisi" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by kdperolehan");
		$l_perolehan = $rs_cb->GetMenu2('kdperolehan',$row['kdperolehan'],true,false,0,'id="kdperolehan" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namarak, kdrak from ms_rak order by kdrak");
		$l_rak = $rs_cb->GetMenu2('kdrak',$row['kdrak'],true,false,0,'id="kdrak" class="ControlStyle" style="width:200"');
		
		$a_stateks = array('' => '-Pilih Status Eksemplar-', 'ADA' => 'Tersedia', 'PJM' => 'Terpinjam');
		$l_stateks = UI::createSelect('statuseksemplar',$a_stateks,'','ControlStyle');
	}	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
		<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center"><? include_once('_notifikasi.php'); ?>
		<table width="<?= $p_tbwidth ?>" border="0" style="border-bottom:0 none;">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<? if ($r_key=='') {
				if ($rowp['lokal']!='1') {	?>
				<a href="<?= $p_filelist; ?>" class="button" style="text-align:center;"><span class="list">Daftar Pustaka </span></a>
				<? } else { ?>
				<a href="<?= $p_filelist2; ?>" class="button"><span class="list">Daftar Digital Content </span></a>
				<? } } else { ?>
				<a href="<?= $p_ekslist; ?>" class="button"><span class="list">Daftar Eksemplar </span></a>
				<? } ?>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">
				<? if($r_eks=='') echo "Simpan Baru"; else echo "Simpan"; ?></span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_eks) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
				<? if ($r_pus!='') { ?>
				<div class="right">
				      <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data eksemplar" onClick="goEks('<?= $p_eksemplar."&idp=".$r_pus; ?>');">+</div>
				</div>
				<? } ?>			
			</div>
		</header>
		<table class="GridStyle" width="<?= $p_tbwidth ?>" style="border-bottom:0 none;border-top:0 none;"><tr><td class="thLeft" style="border:0 none;" align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<table class="GridStyle" width="<?= $p_tbwidth ?>" border="1" cellspacing="0" cellpadding="6" class="GridStyle">
			<tr height=20> 
				<td class="LeftColumnBG thLeft">NO. INDUK *</td>
				<td class="RightColumnBG">
				<? if($c_edit and $c_delete) { ?>
				<input type="text" name="noseri" id="noseri" value="<?= $r_eks=='' ? $serie : $row['noseri'] ?>" maxlength=20>
				<? } else {?>
				<?= UI::createTextBox('noseri',($r_eks=='' ? $serie : $row['noseri']),'ControlStyle',2,75,$c_edit,'readonly'); ?>
				<? } ?>
				</td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft" width="150">Judul Pustaka</td>
				<td class="RightColumnBG">
				<input type="hidden" name="idpustaka" id="idpustaka" value="<?= $rowp['idpustaka'] ?>">
				<?= UI::createTextArea('judule',$rowp['judul'],'ControlStyle',2,75,$c_edit,'readonly'); ?>
				<input type="hidden" name="noseripustaka" id="noseripustaka" value="<?= $rowp['noseripustaka'] ?>">
				</td>
			</tr>
			<? if($row['namaklasifikasi']=='LH'){ ?>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Peminjam</td>
				<td class="RightColumnBG"><?= $isPinjam['idanggota']!='' ? $isPinjam['idanggota'] .' - '. $isPinjam['namaanggota'] : '<i>Tersedia</i>' ?></td>
			</tr>
			<? } ?>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Label *</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_klasifikasi : $row['namaklasifikasi']; ?></td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Kondisi *</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_kondisi : $row['namakondisi']; ?></td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Asal Perolehan *</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_perolehan : $row['namaperolehan']; ?></td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Tanggal Perolehan *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tglperolehan',$r_key!='' ? Helper::formatDate($row['tglperolehan']) : Helper::formatDate($row['tglperolehan']),'ControlStyle',10,10,$c_edit); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleperolehan" style="cursor:pointer;" title="Pilih tanggal perolehan">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglperolehan",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleperolehan",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				[ Format : dd-mm-yyyy ]
				<? } ?>
				
				</td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Tanggal terbit</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tglterbit',$r_key!='' ? Helper::formatDate($row['tglterbit']) : Helper::formatDate($row['tglterbit']),'ControlStyle',10,10,$c_edit); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleterbit" style="cursor:pointer;" title="Pilih tanggal terbit">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglterbit",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleterbit",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				[ Format : dd-mm-yyyy ]
				<? } ?>
				
				</td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Tempat/Rak Pustaka</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_rak : $row['namarak']; ?></td>
				
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Lokasi *</td>
				<td class="RightColumnBG">
				<? if($c_edit) { ?>
				<input type="text" id="namalokasi" name="namalokasi" size="35" disabled value="<?= $row['namalokasi'] ?>">
				<input type="hidden" id="kdlokasi" name="kdlokasi" size="10" value="<?= $row['kdlokasi'] ?>">
				
				<img src="images/delete.png" style="cursor:pointer;" title="Hapus Lokasi" onClick="goClearLokasi()">
				&nbsp;
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi',500,500);">
					<span id="span_posisi"><u title="Lihat Lokasi" onclick="popup('index.php?page=pop_lokasi',450,440);" style="cursor:pointer;" class="Link">Lihat Lokasi...</u></span>
				<? } else echo $row['namalokasi']?>
				</td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Harga</td>
				<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',50,20,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">No. Inventaris</td>
				<td class="RightColumnBG">
				<? if($c_edit) { ?>
				<input type="text" id="noinventaris" name="noinventaris" size="35" value="<?= $row['noinventaris'] ?>">
				<? } else echo $row['noinventaris']?>
				</td>
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Keterangan</td>
				<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',5,75,$c_edit); ?></td>
			</tr>
			<tr>
				<td colspan="5" class="FootBG" align="right">&nbsp;</td>
			</tr>
			
		</table>



		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_eks ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
		<input type="hidden" name="key3" id="key3" value="<?= $r_pus ?>">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tglterbit").mask("99-99-9999");
	   $("#tglperolehan").mask("99-99-9999");
});
</script>
	<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

	</script>
<script language="javascript">

function saveData() {
	if(cfHighlight("ideksemplar,kdklasifikasi,kdkondisi,kdperolehan,tglperolehan,namalokasi")){
		goSave();
	}
}
function goClearLokasi() {
	document.getElementById("kdlokasi").value='';
	document.getElementById("namalokasi").value='';
	
}

function goEks(file3) {
	document.getElementById("perpusform").action = file3;
	document.getElementById("key").value = '';
	goSubmit();
}
</script>
</html>