<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	// definisi variabel halaman

	$p_window = '[PJB LIBRARY] Daftar Pustaka';
	$p_title = 'Daftar Pustaka';
	$p_tbheader = '.: Daftar Pustaka:.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_pustaka.php');
	$p_id = "ms_pustakata";
	$p_eksemplar = Helper::navAddress('ms_eksemplar.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'noseri';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	
	$reqresult = false;
	$sql ="select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka
                                from ms_author p
                                left join pp_author t on t.idauthor = p.idauthor"; 
	$peng = $conn->GetArray($sql);
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}

	// sql untuk mendapatkan isi list

	$p_sqlstr="select p.idpustaka, p.noseri, p.judul,p.judulseri, p.kdjenispustaka, p.nopanggil, p.authorfirst1, p.authorlast1, p.authorfirst2,
		p.authorlast2,p.authorfirst3,p.authorlast3, p.isbn, p.tglperolehan, p.kota, p.namapenerbit, p.edisi, p.jmlhalromawi, p.dimensipustaka,
		p.keywords, p.tahunterbit, p.pembimbing, p.nrp1, (select count(idpustaka) from pp_eksemplar where pp_eksemplar.idpustaka=p.idpustaka ) as copies
		from ms_pustaka p 
		where 1=1
			and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal<>1 )";
	
	if($sqlAdminUnit){
		$p_sqlstr .=" and p.idpustaka in (select e.idpustaka
							from pp_eksemplar e 
							join lv_lokasi l on l.kdlokasi = e.kdlokasi
							where 1=1 and l.idunit in ($units)) ";
	}
	
	$c_sqlstr="select count(*)
		from ms_pustaka p 
		where 1=1
		and p.idpustaka in (select e.idpustaka
					from pp_eksemplar e 
					join lv_lokasi l on l.kdlokasi = e.kdlokasi
					where 1=1 $sqlAdminUnit 
				) 
		and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal<>1 )";
	
	$e_sqlstr="select count(*)
		from pp_eksemplar e
		join lv_lokasi l on l.kdlokasi = e.kdlokasi
		join ms_pustaka p on e.idpustaka=p.idpustaka
		where 1=1 $sqlAdminUnit and p.kdjenispustaka
			in (select kdjenispustaka from lv_jenispustaka where islokal<>1 and kdjenispustaka in(".$_SESSION['roleakses']."))";
	
	
	if($_POST){
		$jenis=Helper::removeSpecial($_POST['kdjenispustaka']);
		$filtersearch = Helper::removeSpecial($_POST['filtersearch']);
		$carifilter = Helper::removeSpecial($_POST['carifilter']);
		$tglawal = Helper::removeSpecial($_POST['tglawal']);
		$tglakhir = Helper::removeSpecial($_POST['tglakhir']);
		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		
		#session
		$_SESSION['listdigitalcontent']['jenispustaka']=$jenis;
		$_SESSION['listdigitalcontent']['filtersearch']=$filtersearch;
		$_SESSION['listdigitalcontent']['carifilter']=$carifilter;
		$_SESSION['listdigitalcontent']['tglawal']=$tglawal;
		$_SESSION['listdigitalcontent']['tglakhir']=$tglakhir;
		$_SESSION['listdigitalcontent']['lokasi']=$keylokasi;
		
		##
		$keynohal = Helper::removeSpecial($_POST['nohalaman']);
		$reqresult = ($_POST['print'] == 'Y');
		
		if($keynohal!=null or $keynohal!='')
			$p_page 	= $keynohal;
		else
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
	}else{
		$jenis=$_SESSION['listdigitalcontent']['jenispustaka'];
		$filtersearch = $_SESSION['listdigitalcontent']['filtersearch'];
		$carifilter = $_SESSION['listdigitalcontent']['carifilter'];
		$tglawal = $_SESSION['listdigitalcontent']['tglawal'];
		$tglakhir = $_SESSION['listdigitalcontent']['tglakhir'];
		$keylokasi=$_SESSION['listdigitalcontent']['lokasi'];
		
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}

	if($filtersearch == "kdpustaka"){
		$p_sqlstr.=" and upper(p.noseri) like upper('%$carifilter%')";
		$c_sqlstr.=" and upper(p.noseri) like upper('%$carifilter%')";
		$e_sqlstr.=" and upper(p.noseri) like upper('%$carifilter%')";
	}elseif($filtersearch == "judul"){
		$p_sqlstr.=" and upper(p.judul) like upper('%$carifilter%') ";
		$c_sqlstr.=" and upper(p.judul) like upper('%$carifilter%') ";
		$e_sqlstr.=" and upper(p.judul) like upper('%$carifilter%') ";
	}elseif($filtersearch == "nopanggil"){
		$p_sqlstr.=" and upper(p.nopanggil) like upper('%$carifilter%')";
		$c_sqlstr.=" and upper(p.nopanggil) like upper('%$carifilter%')";
		$e_sqlstr.=" and upper(p.nopanggil) like upper('%$carifilter%')";
	}elseif($filtersearch == "pengarang"){
		$p_sqlstr.=" and( ";
		$p_sqlstr.=" (upper(p.authorfirst1||' '||p.authorlast1) like upper('%$carifilter%') or upper(p.authorfirst2||' '||p.authorlast2) like upper('%$carifilter%') or upper(p.authorfirst3||' '||p.authorlast3) like upper('%$carifilter%')) ";
		$p_sqlstr.=" or (upper(p.authorfirst1) like upper('%$carifilter%') or upper(p.authorfirst2) like upper('%$carifilter%') or upper(p.authorfirst3) like upper('%$carifilter%')) ";
		$p_sqlstr.=" or (upper(p.authorlast1) like upper('%$carifilter%') or upper(p.authorlast2) like upper('%$carifilter%') or upper(p.authorlast3) like upper('%$carifilter%')) ";
		$p_sqlstr.=" or (p.idpustaka in (select ta.idpustaka
						from pp_author ta
						left join ms_author a on a.idauthor = ta.idauthor
						where upper(a.namadepan) like upper('%$carifilter%')
							or upper(a.namabelakang) like upper('%$carifilter%')
							or upper(trim(a.namadepan)||' '||trim(a.namabelakang)) like upper('%$carifilter%') 
						)
				)
			)";

		$c_sqlstr.=" and( ";
		$c_sqlstr.=" (upper(p.authorfirst1||' '||p.authorlast1) like upper('%$carifilter%') or upper(p.authorfirst2||' '||p.authorlast2) like upper('%$carifilter%') or upper(p.authorfirst3||' '||p.authorlast3) like upper('%$carifilter%')) ";
		$c_sqlstr.=" or (upper(p.authorfirst1) like upper('%$carifilter%') or upper(p.authorfirst2) like upper('%$carifilter%') or upper(p.authorfirst3) like upper('%$carifilter%')) ";
		$c_sqlstr.=" or (upper(p.authorlast1) like upper('%$carifilter%') or upper(p.authorlast2) like upper('%$carifilter%') or upper(p.authorlast3) like upper('%$carifilter%')) ";
		$c_sqlstr.=" or (p.idpustaka in (select ta.idpustaka
						from pp_author ta
						left join ms_author a on a.idauthor = ta.idauthor
						where upper(a.namadepan) like upper('%$carifilter%')
							or upper(a.namabelakang) like upper('%$carifilter%')
							or upper(trim(a.namadepan)||' '||trim(a.namabelakang)) like upper('%$carifilter%') 
						)
				)
			)";

		$e_sqlstr.=" and( ";
		$e_sqlstr.=" (upper(p.authorfirst1||' '||p.authorlast1) like upper('%$carifilter%') or upper(p.authorfirst2||' '||p.authorlast2) like upper('%$carifilter%') or upper(p.authorfirst3||' '||p.authorlast3) like upper('%$carifilter%')) ";
		$e_sqlstr.=" or (upper(p.authorfirst1) like upper('%$carifilter%') or upper(p.authorfirst2) like upper('%$carifilter%') or upper(p.authorfirst3) like upper('%$carifilter%')) ";
		$e_sqlstr.=" or (upper(p.authorlast1) like upper('%$carifilter%') or upper(p.authorlast2) like upper('%$carifilter%') or upper(p.authorlast3) like upper('%$carifilter%'))";
	
		$e_sqlstr.=" or (p.idpustaka in (select ta.idpustaka
						from pp_author ta
						left join ms_author a on a.idauthor = ta.idauthor
						where upper(a.namadepan) like upper('%$carifilter%')
							or upper(a.namabelakang) like upper('%$carifilter%')
							or upper(trim(a.namadepan)||' '||trim(a.namabelakang)) like upper('%$carifilter%') 
						)
				)
			)";
	}elseif($filtersearch == "penerbit"){
		$p_sqlstr.=" and upper(p.namapenerbit) like upper('%$carifilter%')";
		$c_sqlstr.=" and upper(p.namapenerbit) like upper('%$carifilter%')";
		$e_sqlstr.=" and upper(p.namapenerbit) like upper('%$carifilter%')";
	}elseif($filtersearch == "keyword"){
		$p_sqlstr.=" and upper(p.keywords) like upper('%$carifilter%')";
		$c_sqlstr.=" and upper(p.keywords) like upper('%$carifilter%')";
		$e_sqlstr.=" and upper(p.keywords) like upper('%$carifilter%')";
	}elseif($filtersearch == "isbn"){
		$p_sqlstr.=" and upper(p.isbn) like upper('%$carifilter%')";
		$c_sqlstr.=" and upper(p.isbn) like upper('%$carifilter%')";
		$e_sqlstr.=" and upper(p.isbn) like upper('%$carifilter%')";
	}elseif($filtersearch == "topik"){
		$p_sqlstr.=" and p.idpustaka in (select idpustaka
						from pp_topikpustaka tp
						left join lv_topik t on t.idtopik = tp.idtopik
						where upper(t.namatopik) like upper('%$carifilter%')
						order by case when upper(t.namatopik) = upper('".$carifilter."') then 0 else 1 end 
						)";
		$c_sqlstr.=" and p.idpustaka in (select idpustaka
						from pp_topikpustaka tp
						left join lv_topik t on t.idtopik = tp.idtopik
						where upper(t.namatopik) like upper('%$carifilter%')
						order by case when upper(t.namatopik) = upper('".$carifilter."') then 0 else 1 end 
						)";
		$e_sqlstr.=" and p.idpustaka in (select idpustaka
						from pp_topikpustaka tp
						left join lv_topik t on t.idtopik = tp.idtopik
						where upper(t.namatopik) like upper('%$carifilter%')
						order by case when upper(t.namatopik) = upper('".$carifilter."') then 0 else 1 end 
						)";
	}
	
	if($tglawal and $tglakhir){
		$p_sqlstr.=" and (p.tglperolehan between to_date('$tglawal','dd-mm-yyyy') and to_date('$tglakhir','dd-mm-yyyy') + (86399/86400))";
		$c_sqlstr.=" and (p.tglperolehan between to_date('$tglawal','dd-mm-yyyy') and to_date('$tglakhir','dd-mm-yyyy') + (86399/86400))";
		$e_sqlstr.=" and (p.tglperolehan between to_date('$tglawal','dd-mm-yyyy') and to_date('$tglakhir','dd-mm-yyyy') + (86399/86400))";
	}else{
		if($tglawal){
			$p_sqlstr.=" and (p.tglperolehan = to_date('$tglawal','dd-mm-yyyy'))";
			$c_sqlstr.=" and (p.tglperolehan = to_date('$tglawal','dd-mm-yyyy'))";
			$e_sqlstr.=" and (p.tglperolehan = to_date('$tglawal','dd-mm-yyyy'))";
		}elseif($tglakhir){
			$p_sqlstr.=" and (p.tglperolehan = to_date('$tglakhir','dd-mm-yyyy'))";
			$c_sqlstr.=" and (p.tglperolehan = to_date('$tglakhir','dd-mm-yyyy'))";
			$e_sqlstr.=" and (p.tglperolehan = to_date('$tglakhir','dd-mm-yyyy'))";
		}
	}
	
	
	if($jenis!=''){
		$p_sqlstr .=" and p.kdjenispustaka = '$jenis' ";
		$c_sqlstr .=" and p.kdjenispustaka = '$jenis' ";
		$e_sqlstr .=" and p.kdjenispustaka = '$jenis' ";
	}
	if($keylokasi!=''){
		$p_sqlstr .=" and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$keylokasi') ";
		$c_sqlstr .=" and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$keylokasi') ";
		$e_sqlstr .=" and e.kdlokasi = '$keylokasi' ";
	}
	  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

$filter_field = "";
	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '')
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			$filter_field .= "kolom ".$filtercol." : ".$filterdata."<br/>";
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'CJudul' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '%".strtolower(trim($filterdata))."%'";							
							break;
				
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'Ncopies' : 	// numeric atau float
							$filterstr .= "(select count(idpustaka) from pp_eksemplar where pp_eksemplar.idpustaka=p.idpustaka ) = $filterdata";
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
			$c_sqlstr .= " and (" . $filterstr . ")";
			$e_sqlstr .= " and (" . $filterstr . ")";
			
		} // end for
	} else

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != ''){
		$filter_field .= "Urutan berdasarkan ".$p_sort."<br/>";
		$p_sqlstr .= " order by $p_sort";
	}
	else{
		$p_sqlstr .= " order by $p_defsort"; 
	}
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$sqlresult = $p_sqlstr;
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page, false, 0, $rsc);
	// $rsc=$conn->Execute($p_sqlstr)->RowCount();
	$rsc=$conn->GetOne($c_sqlstr);
	$sqlresult_e = $e_sqlstr; 
	$rs_e = $conn->GetOne($e_sqlstr);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal<>1 and kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$jenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goFilterEx();showJenisBuku()" ');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);
	
	$rs_cb = $conn->Execute("select jenisbuku, kdjenisbuku from lv_jenisbuku order by jenisbuku");
	$l_buku = $rs_cb->GetMenu2('kdjenisbuku',$f6,true,false,0,'id="kdjenisbuku" class="ControlStyle" style="width:80" onChange="goFilterEx()"');

	$a_filter = array('' => '- Pilih Filter Pencarian -', 'kdpustaka' => 'Kode Pustaka', 'judul' => 'Judul', 'nopanggil' => 'No. Panggil', 'pengarang' => 'Pengarang', 'penerbit' => 'Penerbit','keyword' => 'Keyword', 'isbn' => 'ISBN', 'topik' => 'Topik');
	$l_filter = UI::createSelect('filtersearch',$a_filter,$filtersearch,'ControlStyle');
	
	$rs_lokasi = $conn->Execute("select l.namalokasi, l.kdlokasi from lv_lokasi l where 1=1 $sqlAdminUnit ");
	$l_lokasi = $rs_lokasi->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150" onchange="goFilterEx()" ');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);	

$filter_field = "";


	if($reqresult) {
		header("Content-Type: application/msexcel");
		header('Content-Disposition: attachment; filename="'.'digitalcontent_searchresult'.'.xls"');
		$rsreq = $conn->Execute(mb_convert_encoding($sqlresult,"UTF-8"));
	?>
	<html>
	<body>
		<?php include_once('_notifikasi.php'); ?>
        <div class="table-responsive">
		<table width="100%" border="0">
			<tr height="20"> 
				<td width="80" nowrap align="center">KODE PUSTAKA </td>			
				<td width="42%" nowrap align="center">Judul Pustaka</td>		
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >NRP</td> 
				<td width="25%" nowrap align="center">Pengarang</td>
				<td width="15%" nowrap align="center">No. Panggil</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Pembimbing</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Tgl Perolehan</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Kota</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Nama Penerbit</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Tahun Terbit</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Edisi</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Jml Hal Romawi</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Dimensi</td> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt" >Keywords</td> 
				<td width="10%" nowrap align="center">Eksemplar</td>
			</tr>
		<?
			while($row = $rsreq->FetchRow()) {
		?>
			<tr height="20" valign="middle"> 
				<td><?= "'".$row['noseri'] ?></td>	
				<td align="left"><?= Helper::limitS($row['judul']).($row['judulseri']!='' ? ' : '.$row['judulseri'] : '').($row['edisi']!='' ? ' / '.$row['edisi'] : '') ?></td>
				<td><?= $row['nrp1']; ?></td>
				<td align="left">
						<?php 
							$auth = array();
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									$auth[] = $a;
								}
								echo implode(", ",$auth);
							}
						?>
				</td>
				<td><?= $row['nopanggil']; ?></td>
				<td><?= $row['pembimbing']; ?></td>
				<td><?= $row['tglperolehan']; ?></td>
				<td><?= $row['kota']; ?></td>
				<td><?= $row['namapenerbit']; ?></td>
				<td><?= $row['tahunterbit']; ?></td>
				<td><?= $row['edisi']; ?></td>
				<td><?= $row['jmlhalromawi']; ?></td>
				<td><?= $row['dimensipustaka']; ?></td>
				<td><?= $row['keywords']; ?></td>
				<td align="center"><?= $row['copies']; ?></td>
			</tr>
		<?
			}
		?>
		</table>
        </div>
	</body>
	</html>
	<? 
	}

	?>
	
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
<!--    <link rel="stylesheet" href="../application/assets/bootstrap/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="style/bootstrap.min.css">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);document.getElementById('carijudul').focus();" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
	<div class="SideItem" id="SideItem">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<div class="filterTable table-responsive">
				<table width="100%">
					<tr>
						<td><?=$l_filter;?></td>
						<td>:</td>
						<td><input type="text" id="carifilter" name="carifilter" size="40" value="<?= $carifilter; ?>" onKeyDown="etrCari(event);">
						</td>
						<td>&nbsp;</td>
						<td><strong>Jenis Pustaka</strong></td>
						<td>:</td>
						<td>
						  <?= $l_jenis ?>
						</td>
					      </tr>
					      <tr>
						<td><strong>Tanggal Perolehan</strong></td>
						<td>:</td>
						<td>
						  <?= UI::createTextBox('tglawal',$tglawal,'ControlStyle',10,10,true); ?>
						  <img src="images/cal.png" id="tglawale" style="cursor:pointer;" title="Pilih tanggal perolehan">
						  &nbsp;
						  <script type="text/javascript">
						  Calendar.setup({
							  inputField     :    "tglawal",
							  ifFormat       :    "%d-%m-%Y",
							  button         :    "tglawale",
							  align          :    "Br",
							  singleClick    :    true
						  });
						  </script>
						  
						  s/d
						  
						  <?= UI::createTextBox('tglakhir',$tglakhir,'ControlStyle',10,10,true); ?>
						  <img src="images/cal.png" id="tglakhire" style="cursor:pointer;" title="Pilih tanggal perolehan">
						  &nbsp;
						  <script type="text/javascript">
						  Calendar.setup({
							  inputField     :    "tglakhir",
							  ifFormat       :    "%d-%m-%Y",
							  button         :    "tglakhire",
							  align          :    "Br",
							  singleClick    :    true
						  });
						  </script>
						  
						  [ Format : dd-mm-yyyy ]
						</td>
							      <td>&nbsp;</td>
						<td><strong>Lokasi</strong></td>
						<td>:</td>
						<td>
						  <?= $l_lokasi ?>
						</td>
				  
						<td>&nbsp;</td>
						<td rowspan="2"><input type="button" value="Filter" class="ControlStyle" onClick="goFilterEx()">
						  <input type="button" value="Refresh" class="ControlStyle" onClick="goClear(); goFilter(false);">
						</td>
					      </tr>

				</table>
			</div>
			<br/>
			<small>
			<?php
				if($filter_field){
					echo "Filter : <br/>";
					echo $filter_field;
				}
			?>
			</small>
			<br/>
			<header style="width:100%">
				<div class="inner">
					<div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
						<h1>
						<?= $p_title ?>
						</h1>
					</div>
					<div class="right">
						<div class="addButton" style="float:left;margin-left:10px; margin:right:10px;" title="download excel" onClick='goPrint()'><img src="images/tombol/excel.png"/></div>
						<?	#if($c_add) { ?>
						<div class="addButton" style="float:left; margin:right:10px;"  title="tambah data" onClick="goNew('<?= $p_filedetail; ?>')">+</div>
						<?	#} ?>
					</div>
				</div>
			</header>
			
            <div class="table-responsive">
			<table width="100%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr> 
				<th width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.noseri:C');">KODE PUSTAKA <?= $p_xsort['p.noseri']; ?></th>			
				<th width="42%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','Judul:CJudul');">Judul Pustaka  <?= $p_xsort['p.judul']; ?></th>		
				<th width="25%" nowrap align="center" class="SubHeaderBGAlt">Pengarang</th>
				<th width="15%" nowrap align="center" class="SubHeaderBGAlt">No. Panggil</th>
				<th width="10%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','copies:Ncopies');">Eksemplar <?= $p_xsort['copies']; ?></th>
				
				<th width="5%" nowrap align="center" class="SubHeaderBGAlt">
				<? if ($c_edit) echo "Edit"; else echo "Lihat"; ?>
				</th>
			</tr>
				<?php
				$i = 0;

				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						//$rowc =  $rsc->FetchRow();
						
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="20" valign="middle"> 
				<td><?= $row['noseri'] ?></td>	
				<td align="left"><p title="Lihat Detail" class="link" onclick="popup('index.php?page=show_pustaka&id=<?= $row['idpustaka'] ?>',850,600)"><?= Helper::limitS($row['judul']).($row['judulseri']!='' ? ' : '.$row['judulseri'] : '').($row['edisi']!='' ? ' / '.$row['edisi'] : '') ?></p></td>
				<td align="left">
						<?php 
							$auth = array();
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									$auth[] = $a;
								}
								echo implode(", ",$auth);
							}else{
								echo $row['authorfirst1']. " " .$row['authorlast1']; 
								if ($row['authorfirst2']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo $row['authorfirst2']. " " .$row['authorlast2'];
								}
								if ($row['authorfirst3']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo $row['authorfirst3']. " " .$row['authorlast3'];
								}
							}
						?>
				</td>
				<td><?= $row['nopanggil']; ?></td>
				<td align="center"><?= $row['copies']; ?>&nbsp; 
				<? if($c_add) {?>
				<u title="Tambah Eksemplar" onclick="goEksemplar('<?= $p_eksemplar."&idp=".$row['idpustaka']; ?>');" class="link"><img src="images/add.png"></u>
				<? } ?>
				</td>	
				<? //if ($c_edit) {?>
				<td align="center">
				
				<u title="Detail Pustaka" onclick="goDetail('<?= $p_filedetail ?>','<?= $row['idpustaka']; ?>');" class="link"><img src="images/edited.gif" width=18 height=18></u>
				</td>
				<? //} ?>
			</tr>
			<?php
			$nomer++;		}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td colspan="11" align="right" class="FootBG">
						<div style="float:left">
							Menuju ke halaman : <?= UI::createTextBox('nohalaman','','ControlStyle',6,6); ?> <input type="submit" name="halaman" id="halaman" onClick="goHalaman()" value="Go">					
							&nbsp;&nbsp;Menampilkan <?= Helper::formatNumber($rsc)?> Data Pustaka
							&nbsp;-&nbsp;<?= Helper::formatNumber($rs_e)?> Data Eksemplar
						</div>
						<div style="float:right">
							Halaman
						  <?= $p_page ?>
						  /
						  <?= $p_lastpage ?>
						  <?= $p_status ?>						
						</div>
				</td>
			</tr>
		</table>
            </div>
		<?php require_once('inc_listnav.php'); ?>
		<br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="npwd" id="npwd">
		<input type="hidden" name="nohal" id="nohal">
		<input type="hidden" name="print" id="print">
		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">

		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>


		</form>
	</div>
</div>


</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xauto.js"></script>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">


function goClear(){
	document.getElementById("carifilter").value='';
	document.getElementById("kdjenispustaka").value='';
	document.getElementById("filtersearch").value='';
	document.getElementById("tglawal").value='';
	document.getElementById("tglakhir").value='';
	document.getElementById("kdlokasi").value='';
//goSubmit();
}

function goHalaman() {
	document.getElementById("nohal").value = $("#nohalaman").val();
	goSubmit();
}

function goFilterEx(){
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

function goPrint() {
	document.getElementById("perpusform").target= "_blank";
	document.getElementById("print").value = "Y";
	goSubmit();
	document.getElementById("perpusform").target= "_self";
	document.getElementById("print").value = "X";
}
</script>
</html>
