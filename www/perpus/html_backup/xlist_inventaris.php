<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
		
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_idx = Helper::cAlphaNum($_REQUEST['idx']);
	
	if ($_POST['fmenu'] != '') {
		if (isset($_SESSION['SIP_DUKFILTER'])) {
			$_SESSION['SIP_DUKFILTER'] = Helper::removeSpecial($_POST['fmenu']);
		} else {
			$_SESSION['SIP_PEGFILTER'] = Helper::removeSpecial($_POST['fmenu']);
		}
	}
	
	// otorisasi user
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_dbtable = 'ms_pustaka';
	$p_window = '[PJB LIBRARY] Daftar Pustaka yang sesuai';
	$p_title = 'Daftar Pustaka';
	$p_tbheader = '.: Daftar Pustaka :.';
	$p_col = 5;
	$p_tbwidth = 800;
	$p_filelist = Helper::navAddress('list_inventaris.php');
	
	$row = $conn->GetRow("select * from pp_orderpustaka op left join pp_orderpustakattb ot on ot.idorderpustaka=op.idorderpustaka left join pp_ttb t on t.idttb=ot.idttb where idorderpustakattb=$r_key");
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<style type="text/css" media="screen">@import "style/tabs.css";</style>
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>	
	<script type="text/javascript" src="scripts/combo.js"></script>	
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<?php include ('inc_menu.php'); ?>

<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<table align="center" width="<?= $p_tbwidth; ?>">
			<tr>
			<? if($c_readlist) { ?>
				<td><a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Inventaris</span></a></td>
			<? } ?>
			</tr>
		</table>
		<table align="center" width="<?= $p_tbwidth; ?>"><tr><td width="100%">
		<div id="header">
			<ul id="primary">
				<li><span>Inventaris</span>
					<ul id="secondary">
						<li id="chosen"><a href="<?= Helper::navAddress('list_pustakainventaris.php'); ?>" >Proses Eksemplar</a></li>
						<li><a href="<?= Helper::navAddress('xdata_kirimeksemplar.php'); ?>" >Pengiriman</a></li>
					</ul>
				</li>
				<li><span>Riwayat</span>
					<ul id="secondary">
						<li><a href="<?= Helper::navAddress('xdata_rwteksemplarinv.php'); ?>" >Eksemplar</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div id="main">
			<div id="contents">
			</div>
		</div>
		</td></tr></table>
		
		
		<div id="roller" style="position:absolute;visibility:hidden;left:0px;top:0px;">
			<img src="images/roller.gif">
		</div>
		<div id="progressbar" style="position:absolute;visibility:hidden;left:0px;top:0px;">
			<table bgcolor="#FFFFFF" border="1" style="border-collapse:collapse;"><tr><td align="center">
			Mohon tunggu...<br><br><img src="images/progressbar.gif">
			</td></tr></table>
		</div>
	</div>
</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xtabs.js"></script>
<script type="text/javascript" src="scripts/foreditx.js"></script>
<script type="text/javascript">
	var key = "<?= $r_key; ?>";
	var defsent = "key=" + key;
	
	$(function() {
		if(key == "") { // insert mode
			switchTab();
		}
		
		$("#header").xtabs({sent:defsent,deftab:"<?= $r_idx=='' ? 0 : $r_idx; ?>"});
		if(key == "") { // insert mode
			$("#header").showOnlyTab(1);
		}
	});
	
	function showRealTab() {
		$("#header").showrealtab(defsent);
	}
	
	function switchTab() {
		$("#primary").hide();
		$("#primary").attr("id","primary_t");
		$("#primary_t").attr("id","primary");
		$("#primary").show();
	}
</script>
</html>
