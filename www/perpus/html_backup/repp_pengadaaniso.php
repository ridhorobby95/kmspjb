<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_klasifikasi= Helper::removeSpecial(Helper::formatDate($_POST['kdklasifikasi']));
	$r_jenis = Helper::removeSpecial(Helper::formatDate($_POST['kdjenispustaka']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pengolahan Sasaran Mutu ISO';
	
	$p_namafile = 'laporan_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	$periode = Helper::periodeISO($r_tgl1);
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select o.*,e.noseri,e.kdklasifikasi,p.judul,p.edisi,p.authorfirst1,p.authorlast1 from pp_eksemplarolah o
			join pp_eksemplar e on o.ideksemplar=e.ideksemplar
			join ms_pustaka p on e.idpustaka=p.idpustaka
			where o.tglkirim between '$r_tgl1 00:00:00' and '$r_tgl2 23:59:00' ";
	
	if($r_jenis!='')
		$sql .= "and p.kdjenispustaka='$r_jenis' ";
	
	if($r_klasifikasi!='')
		$sql .= "and e.kdklasifikasi='$r_klasifikasi' ";
	
	
	$rs = $conn->Execute($sql);
	
	$ket=0;
	while($row=$rs->FetchRow()) 
	{
	$hari=Helper::dateKurangi('-',$row['tglkirim'],$row['tglpengolahan']);
	if ($row['kdklasifikasi']=='LH'){
		if ($hari>3){
			$ket +=1;
			$label='TIDAK TERCAPAI';
			}else
			$label='TERCAPAI';
			
	}else {
		if($hari>4){
			$ket +=1;
			$label='TIDAK TERCAPAI';
			}else
			$label='TERCAPAI';
	}
	
	$Aseri[]=$row['noseri'];
	$Ajudul[]=Helper::LimitS($row['judul']).' '.($row['edisi']=='' ? '' : ': '.$row['edisi']);
	$Aauthor[]=$row['authorfirst1'].' '. $row['authorlast1'];
	$Atgloleh[]=Helper::formatDate($row['tglpengolahan']);
	$Atglkirim[]=Helper::formatDate($row['tglkirim']);
	$Ahari[]=$hari;
	$Alabel[]=$label;
	}
	$rsc=$rs->RowCount();
	if($rsc!=0)
	$persen=(100)-($ket/$rsc*100);
	else
	$persen=0;

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=1000>
	<tr>
		<td width=100><img src="<?= $dirIcon.'logounusa.jpg' ?>" width=70 height=60></td>
		<td valign="center">Monitoring Sasaran Mutu<br>FM-MR-04/-/01/Rev 01<br>BDU: Perpustakaan</td>
	</tr>
</table>
<br>
<table width=1000 border=0 cellpadding="2" cellspacing="0">
  <tr>
	<td>Nomor Prosedur : PM-PERPUS-02<br>
		Nama Dokumen : Pengolahan Bahan Pustaka Perpustakaan<br>
		Sasaran Mutu : Proses pengolahan bahan pustaka<br>
		Kriteria Keberhasilan : <?= $periode ?> % waktu yang diperlukan untuk proses pengolahan
		buku label hijau tidak lebih dari 3 hari dan untuk buku non label hijau<br>
		tidak lebih dari 4 hari sejak diterima dari bagian pengadaan<br>
		Periode : <?= Helper::formatDate($r_tgl1) ." s/d ". Helper::formatDate($r_tgl2) ?><br>
		Keberhasilan : Tercapai <?= Helper::formatNumber($persen,'0',false,true) ?> % 
</table><br>
<table width="1000" border="1" cellpadding="2" cellspacing="0">
  
  <tr height="30">
	<th width="12" align="center"><strong>No.</strong></th>
	<th width="60" align="center"><strong>NO INDUK</strong></th>
    <th width="150" align="center"><strong>Judul - Edisi</strong></th>
    <th width="100" align="center"><strong>Pengarang</strong></th>
	<th width="60" align="center"><strong>Tgl Terima</strong></th>
	<th width="60" align="center"><strong>Tgl Kirim</strong></th>	
	<th width="70" align="center"><strong>Waktu Proses (Hari)</strong></th>	
	<th width="85" align="center"><strong>Keterangan</strong></th>	
	<th width="90" align="center"><strong>Nomor Registrasi MR</strong></th>
  </tr>
  <?php
	$no=1;
	for ($i=0;$i<$rsc;$i++) {
	?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $Aseri[$i] ?></td>
    <td align="left"><?= $Ajudul[$i] ?></td>
    <td align="left"><?= $Aauthor[$i] ?></td>
    <td align="center"><?= $Atgloleh[$i] ?></td>
    <td align="center"><?= $Atglkirim[$i] ?></td>
    <td align="center"><?= $Ahari[$i] ?></td>
    <td align="left"><?= $Alabel[$i] ?></td>
    <td>&nbsp;</td>
  </tr>
	<? $no++; } ?>
	<? if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=10 >Data tidak ditemukan</td>
	</tr>
	<? } ?>
    <tr height=25 >
	
    <td align="left" colspan='10'><strong>Total Jumlah  : <?= $rsc;?>

	</strong></td>
  </tr>
</table>


</div>
</body>
</html>