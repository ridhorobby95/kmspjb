<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	
	$r_kondisi = Helper::removeSpecial($_POST['kdkondisi']);
	
	if($r_format=='') { // or $r_kondisi==''
		header("location: index.php?page=home");
	}
	
	//$conn->debug = true;
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Jenis Pustaka';
	
	$p_namafile = 'rekapjpus_'.$r_kondisi;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//sql jumlah "judul" per "jenis pustaka"
	$sql = "select count(distinct(e.idpustaka)) as jml_judul, p.kdjenispustaka, s.namajenispustaka, e.kdkondisi, k.namakondisi
			from pp_eksemplar e
			JOIN ms_pustaka p ON p.idpustaka=e.idpustaka
			join lv_jenispustaka s on s.kdjenispustaka = p.kdjenispustaka
			join lv_kondisi k on k.kdkondisi = e.kdkondisi
			";	
			
	if ($r_kondisi!=''){
		$sql .=" where e.kdkondisi = '$r_kondisi'";
	}
	
	$sql .=" group by p.kdjenispustaka, s.namajenispustaka, e.kdkondisi, k.namakondisi
			 order by s.namajenispustaka";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	
	$rsj = $conn->getRow("select namakondisi from lv_kondisi where kdkondisi='$r_kondisi'");

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Rekap Jenis Pustaka</h2>
  	</strong></td>
  </tr>
	<tr>
	<td><b>Jenis Kondisi : <?= $r_kondisi=='semua' ? "Semua Kondisi" : $rsj['namakondisi'] ?></b></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="110" align="center" rowspan="2"><strong>Jenis Pustaka</strong></th>
	<th width="118" align="center" rowspan="2"><strong>Kondisi Pustaka</strong></th>
	<th width="118" align="center" rowspan="2"><strong>Jumlah Judul</strong></th>
	<th align="center" colspan="4"><strong>Jumlah Eksemplar</strong></th>
  </tr>
  <tr>
  	<th width="99">LH</th>
	<th width="103">LM</th>
	<th width="90">LT</th>
	<th width="117">Total</th>
  </tr>
  <?php
	while($rs=$row->FetchRow()) 
	{  
	?>
    <tr height=25>
		<td align="left"><?= $rs['namajenispustaka']; ?></td>
		<td align="left"><?= $rs['namakondisi']; ?></td>
		<td align="left"><?= number_format($rs['jml_judul'],0); ?></td>
	<?
		//sql jumlah "eksemplar" per "kondisi"
		$r_kdjenispustaka = $rs['kdjenispustaka'];
		$r_kdkondisi = $rs['kdkondisi'];
		
		$sql_1 = "select count(e.ideksemplar) as jml_eks1, s.namajenispustaka, p.kdjenispustaka, e.kdklasifikasi, e.kdkondisi
				from pp_eksemplar e
					join ms_pustaka p on e.idpustaka = p.idpustaka
					join lv_jenispustaka s on s.kdjenispustaka = p.kdjenispustaka
					join lv_kondisi k on k.kdkondisi = e.kdkondisi
				where p.kdjenispustaka = '$r_kdjenispustaka' ";	
		if ($r_kondisi!='')
			$sql_1 .="  and e.kdkondisi = '$r_kondisi'";
		else
			$sql_1 .="  and e.kdkondisi = '$r_kdkondisi'";
		
		$sql_1 .=" group by p.kdjenispustaka, e.kdkondisi, s.namajenispustaka, e.kdklasifikasi
				order by e.kdklasifikasi ";
				
		//sql jumlah "eksemplar" per "jenis pustaka"
		$sql_2 = "select count(e.ideksemplar) as jml_eks2, s.namajenispustaka, p.kdjenispustaka
				from pp_eksemplar e
					join ms_pustaka p on e.idpustaka = p.idpustaka
					join lv_jenispustaka s on s.kdjenispustaka = p.kdjenispustaka
					join lv_kondisi k on k.kdkondisi = e.kdkondisi
				where p.kdjenispustaka = '$r_kdjenispustaka' ";	
		if ($r_kondisi!='')
			$sql_2 .=" and e.kdkondisi = '$r_kondisi'";
		else
			$sql_2 .=" and e.kdkondisi = '$r_kdkondisi'";
			
		$sql_2 .=" group by p.kdjenispustaka, s.namajenispustaka
				order by s.namajenispustaka ";
		$row_2 = $conn->Execute($sql_2);
		$count_2 = $row_2->fields["jml_eks2"];
		//$rsc_2=$row_2->RowCount();
		//$rs_2=$row_2->FetchRow();
				
		$row_1 = $conn->Execute($sql_1);
		$rsc_1=$row_1->RowCount();
		if($rsc_1 == 3)
		{
			while($rs_1=$row_1->FetchRow()) 
			{
				if($rs_1['kdklasifikasi']=="LH")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LH" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				if ($rs_1['kdklasifikasi']=="LM")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LM" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				if ($rs_1['kdklasifikasi']=="LT")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LT" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				if ($rs_1['kdklasifikasi']=="")
				{
				?>
				<td align="left"></td>
				<?
				}
			} 
		}
		else if ($rsc_1 == 2)
		{
			$urut2 = 1 ;
			while($rs_1=$row_1->FetchRow()) 
			{
				if($rs_1['kdklasifikasi']=="LH")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LH" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				else
				{

					if ($rs_1['kdklasifikasi']=="LM")
					{
						if($urut2==1)
						{
							?>
							<td align="left"></td>
							<td align="left"><?= $rs_1['kdklasifikasi']=="LM" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
							<?
						}
						elseif($urut2==2)
						{
							?>
							<td align="left"><?= $rs_1['kdklasifikasi']=="LM" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
							<td align="left"></td>
							<?
						}
					}
					else 
					{
						if ($rs_1['kdklasifikasi']=="LT")
						{
							?>
							<td align="left"><?= $rs_1['kdklasifikasi']=="LT" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
							<?
						}
					}
				}
				$urut2++;
			} 
		}
		else if ($rsc_1 == 1)
		{
			while($rs_1=$row_1->FetchRow()) 
			{
				if($rs_1['kdklasifikasi']=="LH")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LH" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				else
				{
				?>
				<td align="left"></td>
				<?
				}

				if ($rs_1['kdklasifikasi']=="LM")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LM" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				else
				{
				?>
				<td align="left"></td>
				<?
				}

				if ($rs_1['kdklasifikasi']=="LT")
				{
				?>
				<td align="left"><?= $rs_1['kdklasifikasi']=="LT" ? number_format($rs_1['jml_eks1'],0) : "" ?></td>
				<?
				}
				else
				{
				?>
				<td align="left"></td>
				<?
				}
			} 
		}
	?>
	<td align="left"><?= number_format($count_2,0); ?></td>
  </tr>
<? } ?>
	
	<? if($rsc==0) { ?>
	<tr height=25>
		<td align="center" colspan="6" >Tidak ada pustaka</td>
	</tr>
	<? } ?>
</table>
<br>

</div>
</body>
</html>