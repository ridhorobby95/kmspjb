<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Buku Tamu Pengunjung';
	
	$p_namafile = 'laporan_bukutamu_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(*) as jumlah, idunit
		from pp_bukutamu
		where to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')";
	$sql .=" group by idunit";
	$rs = $conn->Execute($sql);
	
	while($row=$rs->FetchRow()){
		$ArJur[$row['idunit']] = $row['jumlah'];	
	}
	
	$s_fakultas = $conn->Execute("select * from ms_satker order by namasatker");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Laporan Kunjungan Per Unit
	<br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
	<tr height=30>
		<th nowrap width="50%" align="center"><b>Unit</b</th>
		<th nowrap width="30%" align="center"><b>Jumlah Pengunjung</b</th>
	</tr>
	<?php $total=0; while($rowf=$s_fakultas->FetchRow()){ ?>
	<tr>
		<td><?=$rowf['namasatker'];?></td>
		<td align="right"><?=number_format($ArJur[$rowf['kdsatker']],0, ',', '.');?></td>
	</tr>
	<?php $total +=$ArJur[$rowf['kdsatker']];
	} ?>
	<tr height=30>
		<td align="right"><b>JUMLAH TOTAL</b></td>
		<td align="right"><b><?= number_format($total,0, ',', '.') ?></b></td>
	</tr>
</table>
<br/><br/><br/>
</div>
</body>
</html>