<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengadaan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
		
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Data Pengadaan';
	$p_title = '.: Data Pengadaan Validasi Keuangan :.';
	$p_title1 = 'Data Pengadaan';
	$p_tvalidasi = 'Validasi Keuangan';
	$p_tvalidasi2 = 'Validasi Keuangan';
	$p_titlelist = '.: Daftar Detail Pengadaan :.';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_pgvalkeu.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$sql = "select * from pp_pengadaan  
			where idpengadaan=$r_key";	
	$row = $conn->GetRow($sql);
	
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record['npkvalidasikeu'] = $_SESSION['PERPUS_USER'];
			$record['tglvalidasikeu'] = date('Y-m-d H:i:s');
			Helper::Identitas($record);
			
			$sql = "select * from pp_orderpustaka op
					left join pp_usul u on u.idusulan=op.idusulan
		  			where idpengadaan = $r_key order by tglusulan";
			$rs = $conn->Execute($sql);
			
			while ($rss = $rs->FetchRow()){
				$idorder = $rss['idorderpustaka'];
				$record['statusvalkeu'] = Helper::cStrNull($_POST['statusvalkeu_'.$idorder]);
				$record['memokeu'] = Helper::cStrNull($_POST['memokeu_'.$idorder]);
				$err = Pengadaan::insertDetailPengadaan($conn,$record,$idorder);
				
			}

			$isproses = $conn->GetOne("select 1 from pp_orderpustaka where idpengadaan= $row[idpengadaan] and statusvalkeu is null");
			if (!$isproses){
				if($err[0] == 0) {
					$sucdb = 'Penyimpanan Berhasil. Tekan Proses jika ingin melanjutkan proses ke step berikutnya';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}else{
				if($err[0] == 0) {
					$sucdb = 'Penyimpanan Berhasil. Proses belum bisa dilanjutkan, ada data yang belum divalidasi';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}
		else if($r_aksi == 'proses' and $c_edit) {
			$record = array();
			$record['statuspengadaan'] = 'V';
			Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
			$err = Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
			if($err[0] == 0) {
					$sucdb = 'Data Berhasil di Proses.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
					Helper::redirect($url);
			}
			else{
					$errdb = 'Data Gagal di Proses.';	
					Helper::setFlashData('errdb', $errdb);
			}
		}
	}
	
	$isproses = $conn->GetOne("select 1 from pp_orderpustaka where idpengadaan= $row[idpengadaan] and statusvalkeu is null"); 
	
  	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from pp_orderpustaka op
			left join pp_usul u on u.idusulan=op.idusulan
		   where idpengadaan = $row[idpengadaan] order by tglusulan";
	$rs = $conn->Execute($p_sqlstr);
  	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table border="0" cellpadding="4" cellspacing="0">
	<tr height="20">
		<td align="center" colspan=4 class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
	<td align="center">
		<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar</span></a>
	</td>
	<? if($c_edit and trim($row['statuspengadaan']) == 'PK') { ?>
	<td align="center">
		<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goReset();" class="buttonshort"><span class="reset">Reset</span></a>
	</td>
	<? }if (trim($row['statuspengadaan']) == 'PK' and !$isproses){?>
	<td align="center">
		<a href="javascript:goProses();" class="buttonshort"><span class="validasi">Proses</span></a>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
	<tr>
		<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
	</tr>
</table>
<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth+50 ?>" class="instan">
	<tr>
		<td valign="top">
			<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth ?>" style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
				<tr>
					<td align="center" class="SubHeaderBGAlt" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1; ?></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">No. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?=  $row['idpengadaan']; ?></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">Tgl. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?= Helper::formatDate($row['tglpengadaan']) ?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Kode Aktivitas</td>
        			<td class="RightColumnBG" colspan="2"><?= $row['kodeaktivitas'] ?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Jumlah</td>
					<td class="RightColumnBG" colspan="2"><div id="totqty">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Total Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><div id="totprice">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Status Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?= Helper::getArrStatusP(trim($row['statuspengadaan']))?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Keterangan</td>
					<td class="RightColumnBG" colspan="2"><?= $row['keterangan'] ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table cellpadding="4" cellspacing="0" width="<?= p_tbwidth; ?>">
	<tr>
		<td align="center" class="PageTitle"><?= $p_titlelist; ?></td>
	</tr>
</table>
<br>
<table width="900" cellpadding="4" cellspacing="0" style="border-collapse:collapse" border="1">
	<tr>
		<td class="SubHeaderBGAlt" colspan="2" align="center" width="10%">Id Usulan</td>
		<td class="SubHeaderBGAlt" align="center" nowrap>Tgl Usulan</td>
		<td class="SubHeaderBGAlt" align="center" nowrap>Judul</td>
		<td class="SubHeaderBGAlt" align="center">Pengarang</td>
		<td class="SubHeaderBGAlt" align="center">Pengusul</td>
		<td class="SubHeaderBGAlt" align="center">Harga</td>
		<td class="SubHeaderBGAlt" align="center">Qty</td>
		<td class="SubHeaderBGAlt" align="center">Validasi Perpustakaan</td>
		<td class="SubHeaderBGAlt" align="center">Validasi</td>
		<td class="SubHeaderBGAlt" align="center">Memo</td>
	</tr>
    <? if (!empty($r_key)){ 
    		$i=0;
			while ($rows = $rs->FetchRow()){
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;				
	?>
    <tr class="<?= $rowstyle ?>"> 
    	<td colspan="2" align="center" valign="top"><?= $rows['idusulan']; ?></td>
        <td align="center" valign="top"><?= Helper::formatDate($rows['tglusulan']); ?></td>
    	<td align="left" valign="top"><?= $rows['judul']; ?></td>
        <td align="left" valign="top"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
    	<td valign="top"><?= $rows['namapengusul']; ?></td>
    	<td align="right" valign="top"><?= $rows['hargausulan']; ?><input type="hidden" name="hargausulan" id="hargausulan" value="<?= $rows['hargausulan']?>"></td>
    	<td align="right" valign="top"><?= $rows['qtypengadaan']; ?><input type="hidden" name="qtypengadaan" id="qtypengadaan" value="<?= $rows['qtypengadaan']?>"></td>
    	<td align="left" valign="top"><?= $rows['statusvalperpus'] == 1 ? '<strong>Disetujui</strong>' : ($rows['statusvalperpus'] == 0 ? '<strong>Ditolak</strong>' : '<strong>Belum</strong>') ; ?><?= $rows['memoperpus'] != '' ? ',&nbsp;<br>'.$rows['memoperpus'] : ''; ?></td>
		<? if (trim($row['statuspengadaan']) == 'PK' and $rows['statusvalperpus'] != 0) {?>
    	<td valign="top" width="80">
			<?= UI::createRadio('statusvalkeu_'.$rows['idorderpustaka'],Helper::arrStatusST(),Helper::cEmChg($rows['statusvalkeu'],''),$c_edit); ?> 
		</td>
		<td valign="top"><?= UI::createTextArea('memokeu_'.$rows['idorderpustaka'],$rows['memokeu'],'ControlStyle',5,40,$c_edit); ?></td>
		<? }else{ ?>
		<td align="left" valign="top"><?= $rows['statusvalkeu'] == '' ? '<strong>Belum</strong>' : ($rows['statusvalkeu'] == 0 ? '<strong>Ditolak</strong>' : '<strong>Disetujui</strong>') ; ?></td>
		<td valign="top"><?= $rows['memokeu'] != '' ? $rows['memokeu'] : ''; ?></td>
		<? } ?>
    </tr>
	<? }} ?>
</table>
<input type="hidden" name="key" id="key" value="<?= $r_key; ?>">
<input type="hidden" name="rkey" id="rkey">
<input type="hidden" name="act" id="act">
</form>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

$(function(){
	hitTotal();
});

function saveData(){
		$("#act").val("simpan");
		goSubmit();
}


function goProses(id){
	$("#rkey").val(id);
	$("#act").val("proses");
	goSubmit();
}

function hitTotal(){
	var total = 0;
	var totprice = 0;
	$("input[name^=qtypengadaan]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		total += qty;
	});
	
	$("input[name^=hargausulan]").each(function(i){
		var hrg = parseInt($("input[name^=hargausulan]").eq(i).val());
		totprice += hrg;	
	});
	
	total += <?= $qty != '' ? $qty : 0; ?>;
	totprice += <?= $hrg != '' ? $hrg : 0;; ?>;
	$("#totqty").html(total);
	$("#totprice").html(totprice);
}

function goReset(){
	$("textarea").val("");
	$("input[type='radio']").attr("checked",false); 
}
</script>
</html>