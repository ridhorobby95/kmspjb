<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//$conns->debug = false;
	$conn->debug = true;
	
	// koneksi ke sdm
	//$conns = Factory::getConnSdm();

	// otentiksi user
	Helper::checkRoleAuth($conng);
	
	// menu dengan struktur xml
	$menu = 
	"<response>
		<item>
			<category>Satuan Kerja</category>";
			
	function loopunit($conn, $parent) {
		$rs = $conn->Execute("select idunit, kodeunit idsatker, nama namasatker, null info_lft, null info_rgt, tingkat lvel, idparent parent, urutan
				      from um.unit
				      where idparent = '$parent'
				      order by nama");
		if($rs->EOF){
			while($row = $rs->FetchRow()) {
				$menu .="<linx>
						<label>".$row['idsatker']." - ".htmlspecialchars($row['namasatker'])."</label>
						<value>".$row['idsatker']."</value>
						<level>".$row['lvel']."</level>
						<parent>".$row['parent']."</parent>
					";
					loopunit($conn, $row['idunit']);
				$menu .="</linx>";
			}
		}
	}
	
	#posisi atas
	$rs = $conn->Execute('select idunit, kodeunit idsatker, nama namasatker, null info_lft, null info_rgt, tingkat lvel, idparent parent, urutan
			      from um.unit
			      where idparent is null
			      order by nama');
	
	while($row = $rs->FetchRow()) {
		$menu .="<linx>
				<label>".$row['idsatker']." - ".htmlspecialchars($row['namasatker'])."</label>
				<value>".$row['idsatker']."</value>
				<level>".$row['lvel']."</level>
				<parent>".$row['parent']."</parent>";
			loopunit($conn, $row['idunit']);
		$menu .="</linx>";
	}

	
	$menu .=
		"</item>
	</response>";

	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	echo $menu;
?>