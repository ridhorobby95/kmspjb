<?php
	// defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// // pengecekan tipe session user
	// $a_auth = Helper::checkRoleAuth($conng,false);

	// // otorisasi user
	// $c_add = $a_auth['cancreate'];
	// $c_edit = $a_auth['canedit'];
	// $c_delete = $a_auth['candelete'];

	// // pengecekan tipe session user
	 // $a_auth = Helper::checkRoleAuth($conng);

	// // otorisasi user
	// $c_add = $a_auth['cancreate'];
	include_once("includes/open_flash/php-ofc-library/open-flash-chart-object.php");
	
	// definisi variabel halaman
	$p_dbtable = 'lv_jenispelayanan';
	$p_window = '[PJB LIBRARY] Coba Graphic';
	$p_title = ' Coba Graphic';
	$p_tbheader = '.: Coba Graphic :.';
	$p_col = 3;
	$p_tbwidth = 430;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdpelayanan";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
	<script src="scripts/rgraph/RGraph.common.core.js"></script>
	<script src="scripts/rgraph/RGraph.common.annotate.js"></script>  <!-- Just needed for annotating -->
	<script src="scripts/rgraph/RGraph.common.context.js"></script>   <!-- Just needed for context menus -->
	<script src="scripts/rgraph/RGraph.common.effects.js"></script>   <!-- Just needed for visual effects -->
	<script src="scripts/rgraph/RGraph.common.key.js"></script>       <!-- Just needed for keys -->
	<script src="scripts/rgraph/RGraph.common.resizing.js"></script>  <!-- Just needed for resizing -->
	<script src="scripts/rgraph/RGraph.common.tooltips.js"></script>  <!-- Just needed for tooltips -->
	<script src="scripts/rgraph/RGraph.common.zoom.js"></script>      <!-- Just needed for zoom -->
	<script src="scripts/rgraph/RGraph.common.dynamic.js"></script>      <!-- Just needed for zoom -->
	
	<script src="scripts/rgraph/RGraph.bar.js"></script>              <!-- Just needed for Bar charts -->
	<script src="scripts/rgraph/RGraph.bipolar.js"></script>          <!-- Just needed for Bi-polar charts -->
	<script src="scripts/rgraph/RGraph.cornergauge.js"></script>      <!-- Just needed for CornerGauge charts -->
	<script src="scripts/rgraph/RGraph.fuel.js"></script>             <!-- Just needed for Fuel charts -->
	<script src="scripts/rgraph/RGraph.funnel.js"></script>           <!-- Just needed for Funnel charts -->
	<script src="scripts/rgraph/RGraph.gantt.js"></script>            <!-- Just needed for Gantt charts -->
	<script src="scripts/rgraph/RGraph.gauge.js"></script>            <!-- Just needed for Gauge charts -->
	<script src="scripts/rgraph/RGraph.hbar.js"></script>             <!-- Just needed for Horizontal Bar charts -->
	<script src="scripts/rgraph/RGraph.hprogress.js"></script>        <!-- Just needed for Porizontal Progress bars -->
	<script src="scripts/rgraph/RGraph.led.js"></script>              <!-- Just needed for LED charts -->
	<script src="scripts/rgraph/RGraph.line.js"></script>             <!-- Just needed for Line charts -->
	<script src="scripts/rgraph/RGraph.meter.js"></script>            <!-- Just needed for Meter charts -->
	<script src="scripts/rgraph/RGraph.odo.js"></script>              <!-- Just needed for Odometers -->
	<script src="scripts/rgraph/RGraph.pie.js"></script>              <!-- Just needed for Pie AND Donut charts -->
	<script src="scripts/rgraph/RGraph.radar.js"></script>            <!-- Just needed for Radar charts -->
	<script src="scripts/rgraph/RGraph.rose.js"></script>             <!-- Just needed for Rose charts -->
	<script src="scripts/rgraph/RGraph.rscatter.js"></script>         <!-- Just needed for Rscatter charts -->
	<script src="scripts/rgraph/RGraph.scatter.js"></script>          <!-- Just needed for Scatter charts -->
	<script src="scripts/rgraph/RGraph.thermometer.js"></script>      <!-- Just needed for Thermometer charts -->
	<script src="scripts/rgraph/RGraph.vprogress.js"></script>        <!-- Just needed for Vertical Progress bars -->
	<script src="scripts/rgraph/RGraph.waterfall.js"></script>        <!-- Just needed for Waterfall charts  -->
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<? include_once('_notifikasi.php'); ?>
<table width="700" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr height="400"> 
		<td width="110" nowrap align="center" >
		<canvas id="myCanvas" width="600" height="250">[No canvas support]</canvas>
		</td>
		
	</tr>
</table><br>
</body>

<script>
	//pie chart
	
	window.onload = function ()
	{
		var pie = new RGraph.Pie('myCanvas', [4,8,4,6,5,3,2,5]);
		pie.Set('chart.tooltips', ['Alvin-10%','Pete-20%','Hoolio-30%','Jack-40%','Kev-50%','Luis-60%','Lou-70%','Jesse-80%']);
		pie.Set('chart.exploded',[15,15,15,15,15,15,15,15]);
		
		pie.Set('chart.labels', ['Alvin','Pete','Hoolio','Jack','Kev','Luis','Lou','Jesse']);
		
		pie.Set('chart.shadow', true);
		pie.Set('chart.shadow.offsetx', 5);
		pie.Set('chart.shadow.offsety', 5);
		pie.Set('chart.radius', 100);
		pie.Draw();
	}
    
	// function CreateGradient (obj, color){
		// return RGraph.RadialGradient(obj, 200, 150, 95, 200, 150, 125, color, 'black')
	// }

	// window.onload = function (){
		// var pie = new RGraph.Pie('myCanvas', [4,3,2,6,8]);
		// pie.Set('chart.colors', [
								 // CreateGradient(pie, '#ABD874'),
								 // CreateGradient(pie, '#E18D87'),
								 // CreateGradient(pie, '#599FD9'),
								 // CreateGradient(pie, '#F4AD7C'),
								 // CreateGradient(pie, '#D5BBE5')
								// ]);
		// pie.Set('chart.shadow', true);
		// pie.Set('chart.shadow.offsetx', 5);
		// pie.Set('chart.shadow.offsety', 5);
		// pie.Set('chart.shadow.blur', 15);
		// pie.Set('chart.shadow.color', '#bbb');
		// pie.Set('chart.labels.sticks', true);
		// pie.Set('chart.labels.sticks.length', 15);
		// pie.Set('chart.tooltips', ['Robert','Louise','Peter','Kevin','Jack']);
		// pie.Set('chart.labels', ['Robert','Louise','Peter','Kevin','Jack']);
		// pie.Set('chart.radius', 100);
		// pie.Set('chart.strokestyle', 'rgba(0,0,0,0.1)');
		// pie.Draw();
	// }

	
	//Line Chart
	// window.onload = function ()
    // {
        // // The data for the Line chart. Multiple lines are specified as seperate arrays.
        // // var data = [10,4,17,50,25,19,20,25,30,29,30,29];
    
        // // Create the Line chart object. The arguments are the canvas ID and the data array.
        // // var line = new RGraph.Line("myCanvas", data);
        
        // // The way to specify multiple lines is by giving multiple arrays, like this:
        // var line = new RGraph.Line('myCanvas', [10,4,17], [50,25,19], [20,15,30]);
        
        // // Configure the chart to appear as you wish.
		// line.Set('chart.curvy', true);
        // line.Set('chart.background.barcolor1', 'white');
        // line.Set('chart.background.barcolor2', 'white');
        // line.Set('chart.background.grid.color', 'rgba(238,238,238,1)');
		// line.Set('chart.tooltips', ['<b>Winner!</b><br />John', 'Fred', 'Jane', 'Lou', 'Pete', 'Kev']);
        // line.Set('chart.colors', ['red']);
        // line.Set('chart.linewidth', 2);
        // // line.Set('chart.filled', false);
        // line.Set('chart.hmargin', 5);
        // line.Set('chart.labels', ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        // line.Set('chart.gutter.left', 40);
        
        // // Now call the .Draw() method to draw the chart.
        // line.Draw();
    // }
	
	// window.onload = function ()
	// {
		// var line = new RGraph.Line('myCanvas', [4,5,1,6,3,5,2], [4,8,6,5,3,2,5], [12,9,8,6,7,9,11], [4,5,1,2,6,4,3]);
		// line.Set('chart.curvy', true);
		// line.Set('chart.linewidth', 3);
		// line.Set('chart.hmargin', 5);
		// line.Set('chart.tooltips', ['<b>Winner!</b><br />John', 'Fred', 'Jane', 'Lou', 'Pete', 'Kev']);
		// line.Set('chart.labels', ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']);
		// line.Draw();
	// }
    
	
	// Bar Chart
	// window.onload = function ()
    // {
       // // jika datanya hanya satu per parameter (satu batang, satu parameter)
       // // var data = [280,45,133];
        
        
        // // An example of the data used by stacked and grouped charts
        // var data = [[1,5,6], [4,5,3], [7,8,9]]

        
        // // Create the br chart. The arguments are the ID of the canvas tag and the data
        // var bar = new RGraph.Bar('myCanvas', data);
        
        
        // // Now configure the chart to appear as wanted by using the .Set() method.
        // // All available properties are listed below.
        // bar.Set('chart.labels', ['Jan', 'Feb', 'Mar']);
		// bar.Set('chart.tooltips', ['Alvin-1','Pete-5','Hoolio-6','Alvin-4','Pete-5','Hoolio-3','Alvin-7','Pete-8','Hoolio-9']);
        // bar.Set('chart.gutter.left', 45);
        // bar.Set('chart.background.barcolor1', 'white');
        // bar.Set('chart.background.barcolor2', 'white');
        // bar.Set('chart.background.grid', true);
		// bar.Set('chart.variant', '3d');
        // bar.Set('chart.colors', ['#2A17B1', '#98ED00', '#000000']);
        
        
        // // Now call the .Draw() method to draw the chart
        // bar.Draw();
    // }

	// Horizontal Bar
	 // window.onload = function (){
        // // The data that is to be represented on the chart
        // // var data = [280,45,133];
        
        // // To show a stacked or grouped chart each element of the data array should itself
        // // be an array containing the data to be shown.
        // var data = [[120,80,60],[30,12,13],[50,50,33]];
    
        // // Create the HBar object giving it the canvas ID and the data just defined.
        // var hbar = new RGraph.HBar('myCanvas', data);
        
        // // Configure the chart to appear as wished.
        // hbar.Set('chart.labels', ['Richard', 'Alex', 'Nick']);
        // hbar.Set('chart.gutter.left', 45);
        // hbar.Set('chart.background.barcolor1', 'white');
        // hbar.Set('chart.background.barcolor2', 'white');
        // hbar.Set('chart.background.grid', true);
        // hbar.Set('chart.colors', ['red']);
        
        // // Now call the .Draw() method to draw the chart.
        // hbar.Draw();
    // }
	
	// Gauge Chart

	// window.onload = function ()
	// {
		// // var gauge = new RGraph.Gauge('myCanvas', 0, 200, [184,12]);
		// var gauge = new RGraph.Gauge('myCanvas', 0, 200, [184]);
		// // gauge.Set('chart.title.top', 'UMK');
		// gauge.Set('chart.title.top.size', 'Italic 22'); // Hmmmm
		// gauge.Set('chart.title.top.font', 'Impact');
		// gauge.Set('chart.title.top.color', 'white');
		// gauge.Set('chart.title.top', 'UMK');
		// gauge.Set('chart.title.bottom.size', 'Italic 14'); // Hmmmm
		// gauge.Set('chart.title.bottom.font', 'Impact');
		// gauge.Set('chart.title.bottom.color', '#ccc');
		// gauge.Set('chart.title.bottom', 'juta');
		// gauge.Set('chart.title.bottom.pos', 0.4);
		// gauge.Set('chart.background.color', 'black');
		// gauge.Set('chart.background.gradient', true);
		// gauge.Set('chart.centerpin.color', '#666');
		// // gauge.Set('chart.needle.colors', [RGraph.RadialGradient(gauge, 125, 125, 0, 125, 125, 25, 'transparent', 'white'),
										   // // RGraph.RadialGradient(gauge, 125, 125, 0, 125, 125, 25, 'transparent', '#d66')]);
		// gauge.Set('chart.needle.colors', [RGraph.RadialGradient(gauge, 125, 125, 0, 125, 125, 25, 'transparent', 'white')]);
		// // gauge.Set('chart.needle.size', [null, 50]);
		// gauge.Set('chart.needle.size', [null]);
		// gauge.Set('chart.text.color', 'white');
		// gauge.Set('chart.tickmarks.big.color', 'white');
		// gauge.Set('chart.tickmarks.medium.color', 'white');
		// gauge.Set('chart.tickmarks.small.color', 'white');
		// gauge.Set('chart.border.outer', '#666');
		// gauge.Set('chart.border.inner', '#333');
		// gauge.Set('chart.colors.ranges', []);
		// gauge.Set('chart.centerx', 150);
		// // RGraph.Effects.Gauge.Grow(obj);
		// gauge.Draw();
		
		// var gauge2 = new RGraph.Gauge('myCanvas', 0, 100, [50]);
		// // gauge.Set('chart.title.top', 'UMK');
		// gauge2.Set('chart.title.top.size', 'Italic 22'); // Hmmmm
		// gauge2.Set('chart.title.top.font', 'Impact');
		// gauge2.Set('chart.title.top.color', 'white');
		// gauge2.Set('chart.title.top', 'SPJ');
		// gauge2.Set('chart.title.bottom.size', 'Italic 14'); // Hmmmm
		// gauge2.Set('chart.title.bottom.font', 'Impact');
		// gauge2.Set('chart.title.bottom.color', '#ccc');
		// gauge2.Set('chart.title.bottom', '%');
		// gauge2.Set('chart.title.bottom.pos', 0.4);
		// gauge2.Set('chart.background.color', 'black');
		// gauge2.Set('chart.background.gradient', true);
		// gauge2.Set('chart.centerpin.color', '#666');
		// // gauge.Set('chart.needle.colors', [RGraph.RadialGradient(gauge, 125, 125, 0, 125, 125, 25, 'transparent', 'white'),
										   // // RGraph.RadialGradient(gauge, 125, 125, 0, 125, 125, 25, 'transparent', '#d66')]);
		// gauge2.Set('chart.needle.colors', [RGraph.RadialGradient(gauge2, 125, 125, 0, 125, 125, 25, 'transparent', 'white')]);
		// // gauge.Set('chart.needle.size', [null, 50]);
		// gauge2.Set('chart.needle.size', [null]);
		// gauge2.Set('chart.text.color', 'white');
		// gauge2.Set('chart.tickmarks.big.color', 'white');
		// gauge2.Set('chart.tickmarks.medium.color', 'white');
		// gauge2.Set('chart.tickmarks.small.color', 'white');
		// gauge2.Set('chart.border.outer', '#666');
		// gauge2.Set('chart.border.inner', '#333');
		// gauge2.Set('chart.colors.ranges', []);
		// gauge2.Set('chart.centerx', 440);
		// gauge2.Draw();
	// }

</script>
</html>