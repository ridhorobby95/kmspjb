<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// otentiksi user
	Helper::checkRoleAuth($conng);
	
	// definisi variabel halaman
	$p_xmlmenu = Helper::navAddress('xml_satker.php');
	
	$pcname=$_REQUEST['nama'];
	if($pcname=='')$pcname='namasatker';
	$pcid=$_REQUEST['id'];
	if($pcid=='')$pcid='idsatker';
	elseif($pcid=='u') $pcid='u_idsatker';
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/jquery.treeview.css">
	<script type="text/javascript" src="scripts/jquery.js"></script>
	<script type="text/javascript" src="scripts/jquery.treeview.js"></script>
	<script type="text/javascript" src="scripts/jquery.xtree.js"></script>
</head>
<body>
	<div id="menutree" style="height:100%;width:100%;"></div>
</body>

<script type="text/javascript">

$(document).ready(function(){
	$("#menutree").popupTreeFromXML({xml: "<?= $p_xmlmenu ?>", ctid: "contents", pcid: "<?= $pcid?>", pcname: "<?= $pcname?>"});
});

</script>

</html>