<?php	
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jurusan = Helper::removeSpecial($_POST['kdjurusan']);
	$r_bidang = Helper::removeSpecial(trim($_POST['bidang']));
	$r_bahasa = Helper::removeSpecial(trim($_POST['kdbahasa']));
	$r_op = Helper::removeSpecial($_POST['optgl']);
	$r_tgl1 = Helper::formatDate($_POST['tgl1']);
	$r_tgl2 = Helper::formatDate($_POST['tgl2']);
	
	
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Borang';
	
	$p_namafile = 'rekapeks_'.$r_kondisi.'_'.$r_jenis;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$rowb = $conn->GetRow("select b.*,namajurusan,namafakultas from pp_borang b join lv_jurusan j on b.kdfakultas=j.kdfakultas and b.kdjurusan=j.f_jur join lv_fakultas f on j.kdfakultas=f.kdfakultas where j.kdjurusan='$r_jurusan'");
	$rbahasa = $conn->GetOne("select namabahasa from lv_bahasa where kdbahasa='$r_bahasa'");
	
	$sql = "select p.kodeddc,e.harga, p.judul,coalesce(p.edisi,'') as edisi,coalesce(p.authorfirst1,'') as authorfirst1,coalesce(p.authorlast1,'') as authorlast1,coalesce(p.authorfirst2,'') as authorfirst2,coalesce(p.authorlast2,'') as authorlast2,coalesce(p.authorfirst3,'') as authorfirst3,coalesce(p.authorlast3,'') as authorlast3,namapenerbit as penerbit,e.tglperolehan as oleh ";
	
	// if($r_op==0)
	// $sql .= " ,e.tglperolehan ";
	// else
	// $sql .= " ,to_char(e.tglterbit,'yyyy') ";
	
	$sql .= " from pp_eksemplar e
			  left join ms_pustaka p on e.idpustaka = p.idpustaka where p.kdjenispustaka='B'";
	
	if($r_bahasa!='')
	$sql .=" and p.kdbahasa='$r_bahasa' ";
	
	
	$sql2 = "SELECT * FROM pp_borang b WHERE (p.kodeddc between b.kelas1 and b.kelas2)";
	if ($r_jurusan!='0') {
		$sql2 .=" and b.kdfakultas='$rowb[kdfakultas]' and b.kdjurusan='$rowb[kdjurusan]' ";
	}
	
	if($r_bidang!=0){
		$sql2 .=" and b.kode='$r_bidang' ";
	}
	
	$sql .= " and exists ($sql2) ";
	
	if($r_op==0)
	$sql .= " and e.tglperolehan between '$r_tgl1' and '$r_tgl2'";
	else{
	$r_tgl1 = date('Y',strtotime($r_tgl1));
	$r_tgl2 = date('Y',strtotime($r_tgl2));
	$sql .= " and to_char(e.tglperolehan,'yyyy') between '$r_tgl1' and '$r_tgl2' ";
	}
	
	//$sql .= " group by p.kodeddc,p.judul,coalesce(p.edisi,''),coalesce(p.authorfirst1,''),coalesce(p.authorlast1,''),coalesce(p.authorfirst2,''),coalesce(p.authorlast2,''),coalesce(p.authorfirst3,''),coalesce(p.authorlast3,'')";
	
	$sql .=" order by p.kodeddc ";
	$rs = $conn->Execute($sql);
	
	$jjud = 0;
	$jeks = 0;
	$jharga=0;
	while($row=$rs->FetchRow()){
	$Arddc[] = $row['kodeddc'];
	$Arjudul[] = Helper::limitS($row['judul'],100).' '. $row['edisi'];
	$Aredisi[] = $row['edisi'];
	$Arauth[] = $row['authorfirst1'].' ' .$row['authorlast1'];
	$Areks[] = $row['copies'];
	$ArP[] = $row['penerbit'].', '.date('Y',strtotime($row['oleh']));	
	$ArH[] = $row['harga'];
	$jharga += $row['harga'];
	$jjud += 1;
	$jeks += $row['copies'];
	}
	
	if($r_bidang==0)
		$bidang = 'Semua';
	elseif($r_bidang=='1')
		$bidang = 'MKK';
	elseif($r_bidang=='2')
		$bidang = 'MKDK';
	elseif($r_bidang=='3')
		$bidang = 'MKDU';
	elseif($r_bidang=='4')
		$bidang = 'REFERENSI';
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=1000>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=1000 cellpadding="2" cellspacing="0" border=0>
  <tr>
	<td align="center" colspan=4><strong>
  	<h2>LAPORAN BORANG</h2>
  	</strong></td>
  </tr>
  <tr>
	<td width="100" align="right"><b>Fakultas</b></td>
	<td width="400">: <?= strtoupper($rowb['namafakultas'])=='' ? 'Semua' : strtoupper($rowb['namafakultas']) ?></td> 
  </tr>
  <tr>
	<td align="right"><b>Jurusan</b></td>
	<td>: <?= strtoupper($rowb['namajurusan'])=='' ? 'Semua' : strtoupper($rowb['namajurusan']) ?></td>
  </tr>
  <tr>
	<td align="right"><b>Bidang</b></td>
	<td>: <?= $bidang ?>&nbsp; <b>Bahasa</b> : <?= $r_bahasa=='' ? 'Semua' : $rbahasa ?></td>
	<td align="right" width="150"><b>Jumlah Eksemplar</b></td>
	<td>: <?= $jjud ?></td>
  </tr>
  <tr>
	<td align="right"><b>Tahun </b></td>
	<td>: <?= $r_op==1 ? 'Terbit : '.$r_tgl1 ." s/d ". $r_tgl2 : 'Proses : '.Helper::formatDate($r_tgl1) ." s/d ". Helper::formatDate($r_tgl2) ?></td>
	<td align="right"><b>Total Harga.</b></td>
	<td>: <?= Helper::formatNumber($jharga,'0',true,true) ?> </td>
  </tr>
</table>
<table width="1000" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="10" align="center"><strong>Klasifikasi</strong></th>
    <th width="330" align="center"><strong>Judul</strong></th>
    <th width="100" align="center"><strong>Pengarang</strong></th>
    <th width="100" align="center"><strong>Penerbit</strong></th>
    <th width="70" align="center"><strong>Harga</strong></th>
  </tr>
  <?php
	$no=1;
	for($i=0;$i<$jjud;$i++)
	{ ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="left"><?= $Arddc[$i] ?></td>
    <td align="left"><?= $Arjudul[$i] ?></td>
	<td align="left"><?= $Arauth[$i] ?></td>
	<td align="left"><?= $ArP[$i] ?></td>
	<td align="right"><?= Helper::formatNumber($ArH[$i],'0',true,true) ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
	
</table>


</div>
</body>
</html>