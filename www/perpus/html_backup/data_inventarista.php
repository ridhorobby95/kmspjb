<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
$conn->debug = true;
	require_once('classes/pengolahan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	$r_key = Helper::removeSpecial($_REQUEST['key']); 
	$rkey = Helper::removeSpecial($_REQUEST['rkey']); 
		//echo "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_orderpustaka';
	$p_window = '[PJB LIBRARY] Data Inventaris Tugas Akhir';
	$p_title = '.: Data Inventaris Tugas Akhir:.';
	$p_tbwidth = 720;
	$p_filelist = Helper::navAddress('xdata_kirimeksemplar.php');

	#default unusa
	$namapenerbit = Pengolahan::getNamaPenerbitUnusa($conn,'201');
	
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_idfileupload = Helper::removeSpecial($_POST['iduploadta']); 
		$iscd=Helper::removeSpecial($_POST['iscd']);

		if($r_aksi == 'proses' and $c_edit) {
			$num = Helper::removeSpecial($_POST['qtyttbdetail']);
			$conn->StartTrans();
			if ($rkey==''){
				$jenis=Helper::removeSpecial($_POST['kdjenispustaka']);
				
				if($rkey==''){
					$seri_fix = sipus::generateNoseriDigitalContent($conn, $jenis, count($_POST['addjurusan']), $_POST['addjurusan'][0]);
				}else{
				    $seri_fix = $conn->GetOne("select noseri from ms_pustaka where idpustaka = $rkey ");
				}
				
				// insert pustaka
				$record = array();
				$idpustaka=Sipus::GetLast($conn,ms_pustaka,idpustaka);
				$record['idpustaka']=$idpustaka;
				$record['judul'] = Helper::cStrNull($_POST['judul']); 
				$record['judulseri'] = Helper::cStrNull($_POST['judulseri']); 
				$record['kdbahasa'] = Helper::cStrNull($_POST['kdbahasa']); 
				$record['edisi'] = Helper::cStrNull($_POST['edisi']);
				$record['idpenerbit']=Helper::cStrNull($_POST['idpenerbit']);
				$record['namapenerbit']=Helper::cStrNull($_POST['namapenerbit']);
				$record['tahunterbit']=Helper::cStrNull($_POST['tahunterbit']);
				$record['isbn'] = Helper::cStrNull($_POST['isbn']); 
				$record['kota'] = Helper::cStrNull($_POST['kota']); 
				$record['tglperolehan'] = Helper::formatDate($_POST['tglperolehan']);
				$record['kdjenispustaka'] = $jenis; 
				$record['authorfirst1']=Helper::cStrNull($_POST['authorfirst1']);			
				$record['authorlast1']=Helper::cStrNull($_POST['authorlast1']);
				$record['authorfirst2']=Helper::cStrNull($_POST['authorfirst2']);			
				$record['authorlast2']=Helper::cStrNull($_POST['authorlast2']);
				$record['authorfirst3']=Helper::cStrNull($_POST['authorfirst3']);			
				$record['authorlast3']=Helper::cStrNull($_POST['authorlast3']);
				$record['nrp1']=Helper::cStrNull($_POST['nrp1']);
				$record['nrp2']=Helper::cStrNull($_POST['nrp2']);
				$record['nrp3']=Helper::cStrNull($_POST['nrp3']);
				$record['noseri']=$seri_fix;
				$record['pembimbing']=Helper::cStrNull($_POST['pembimbing']);
				$record['nimpengarang']=Helper::cStrNull($_POST['nrp1']);

				// tambahan ganti jurusan
				Helper::Identitas($record);
				Sipus::InsertBiasa($conn,$record,ms_pustaka);
				
				#pengarang by penulis
				$r_author = array();
				$r_author['namadepan']=Helper::cStrNull($_POST['authorfirst1']);			
				$r_author['namabelakang']=Helper::cStrNull($_POST['authorlast1']);
				Helper::Identitas($r_author);
				Sipus::InsertBiasa($conn,$r_author,ms_author);
				$idauth = $conn->GetOne("select last_value from ms_author_idauthor_seq");
				
				#author pustaka
				$r_pauthor = array();
				$r_pauthor['idpustaka']=$idpustaka;			
				$r_pauthor['idauthor']=$idauth;
				Sipus::InsertBiasa($conn,$r_pauthor,pp_author);
				
				//jurusan
				$jurusan1=$_POST['addjurusan'];
				if($jurusan1!='')
					Sipus::InsertRef($conn,pp_bidangjur,kdjurusan,idpustaka,$jurusan1,$record['idpustaka']);
				
				if($r_idfileupload){
					$file = $conn->GetArray("select * from uploadta_file where iduploadta = '$r_idfileupload' order by iduploadtafile ");
					if($file){
						foreach($file as $f){
							$f_record = array();
							$f_record['idpustaka'] = $idpustaka;
							$f_record['file'] = $f['file'];
							$f_record['login'] = $f['login'];
							$f_record['download'] = $f['download'];
							Helper::Identitas($f_record);
							Sipus::InsertBiasa($conn,$f_record,pustaka_file);
						}
					}
				}
				
				
				//insert eksemplar
				$recdetail = array();
				$ideksemplar=Sipus::GetLast($conn,pp_eksemplar,ideksemplar);
				$recdetail['ideksemplar']=$ideksemplar;
				$recdetail['idpustaka']=$record['idpustaka'];
				$recdetail['kdklasifikasi'] = Helper::cStrNull($_POST['kdklasifikasi']);
				$recdetail['kdperolehan'] = 'L'; // Lain-lain  
				$recdetail['harga'] = Helper::cStrNull($_POST['harga']);  
				$recdetail['kdkondisi'] = 'V';
				
				$kdklasifikasicd = array();
				$kdklasifikasicd[] = 'M';
				
				$kdperolehan = array();
				$kdperolehan[] = $recdetail['kdperolehan'];
				
				//dapatkan reg comp
				$countP=Sipus::GetCount($conn,$record['idpustaka']);
				$recdetail['noseri']=$record['noseri'].".01";
				Helper::Identitas($recdetail);
				Sipus::InsertBiasa($conn,$recdetail,pp_eksemplar);
				
				//insert eksemplar olah
				$recolah['idorderpustakattb']=$r_key;
				$recolah['ideksemplar']=$ideksemplar;
				Helper::Identitas($recolah);
				Sipus::InsertBiasa($conn,$recolah,pp_eksemplarolah);
				
				//update orderttb
				$recorder['idpustaka']=$record['idpustaka'];
				$recorder['tglinventaris']=date('Y-m-d');
				$recorder['npkinventaris']=$_SESSION['PERPUS_USER'];
				$recorder['stsinventaris']=1;
				Sipus::UpdateBiasa($conn,$recorder,pp_orderpustakattb,idorderpustakattb,$r_key);
				
				if($iscd=='VCD'){
					$num = 1;
					$record['kdjenispustaka']='V';
					$lastid=$conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
					$ideks = $lastid['maxid'] + 1;
					$recdetail['ideksemplar'] = $ideks;
					
					$pengarang = array();
					$pengarang[] = $idauth;
					
					$err = Pengolahan::insertInventaris($conn,$record,$recdetail,$num,$pengarang,$r_key,$kdklasifikasicd,$kdperolehan,null);
					
					//jurusan
					$jurusan1=$_POST['addjurusan'];
					if($jurusan1!='' and $err[1])
						Sipus::InsertRef($conn,pp_bidangjur,kdjurusan,idpustaka,$jurusan1,$err[1]);
				}
			
			}else {   //hanya eksemplar
				//insert eksemplar
				$isSeri=Sipus::Search($conn,ms_pustaka,idpustaka,$rkey,noseri);
				$recdetail = array();
				$recdetail['idpustaka']=$rkey;
				$recdetail['kdklasifikasi'] = Helper::cStrNull($_POST['kdklasifikasi']);
				$recdetail['kdperolehan'] = 'S';  
				$recdetail['harga'] = Helper::cStrNull($_POST['harga']);  
				$recdetail['kdkondisi'] = 'A';
				//dapatkan reg comp
				$countP=Sipus::GetCount($conn,$rkey);
				$recdetail['noseri']=$isSeri.".".Helper::strPad($countP,2,0,'l');
				Helper::Identitas($recdetail);
				Sipus::InsertBiasa($conn,$recdetail,pp_eksemplar);
				
				//insert eksemplar olah
				$recolah['idorderpustakattb']=$r_key;
				$ideks = $conn->GetOne("select last_value from pp_eksemplar_ideksemplar_seq");
				
				$recolah['ideksemplar']=$ideks;
				Helper::Identitas($recolah);
				Sipus::InsertBiasa($conn,$recolah,pp_eksemplarolah);
				
				//update orderttb
				$recorder['idpustaka']=$r_key;
				$recorder['tglinventaris']=date('Y-m-d');
				$recorder['npkinventaris']=$_SESSION['PERPUS_USER'];
				$recorder['stsinventaris']=1;
				//Helper::Indentitas($recorder);
				Sipus::UpdateBiasa($conn,$recorder,pp_orderpustakattb,idorderpustakattb,$r_key);
			
			}
			$conn->CompleteTrans();
			if($conn->ErrorNo() == 0) {
				$rkey = $err[1];
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				
				echo '<script type="text/javascript">';
				echo 'var postdata = "key='.$r_key.'&rkey='.$rkey.'";';
				echo 'goPostX("'.$p_filelist.'", postdata)';
				echo '</script>';	
			}
			else{
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			
		}
		
		if ($r_key != ''){
			$p_sqlstr = "select *, a.idunit from pp_orderpustakattb ot 
						left join pp_orderpustaka o on o.idorderpustaka=ot.idorderpustaka
						join pp_ta ta on o.iddaftarta=ta.iddaftarta
						left join pp_ttb t on t.idttb=ot.idttb
						left join ms_anggota a on a.idanggota = o.nrp1 
						where ot.idorderpustakattb=$r_key";
			$row = $conn->GetRow($p_sqlstr);
			
			$iduploadta = $row['iduploadta'];
			
			if($row['idunit']){
				$jr = array();	
				$jr[$row['idunit']] = $row['idunit'];
			}
			
			$kdjenispustaka = Pengolahan::getJenisPustakaByUnitJenjang($row['idunit']);


			if ($rkey != ''){
				$p_sqlstr = "select * from ms_pustaka p
					left join pp_eksemplar e on e.idpustaka=p.idpustaka 
					left join pp_bidangjur b on p.idpustaka=b.idpustaka
					where e.idpustaka=$rkey";
				$rowp = $conn->GetRow($p_sqlstr);
				$sql3 = "select b.kdjurusan, b.namajurusan from lv_jurusan b join
						pp_bidangjur a on b.kdjurusan = a.kdjurusan where a.idpustaka = '$rkey'";
				$rsjur = $conn->GetArray($sql3);
			}
						
			$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");
			$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$rowp['kdklasifikasi'],true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:200"');

			$rs_cb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by kdbahasa");
			$l_bahasa= $rs_cb->GetMenu2('kdbahasa',$rowp['kdbahasa'],true,false,0,'id="kdbahasa" class="ControlStyle" style="width:150"');
			$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal=1 order by kdjenispustaka");

			$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'] ? $row['kdjenispustaka'] : $kdjenispustaka,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:250px"');
			$l_jurusan = UI::createSelect('kdjurusan','','','ControlStyle',true,'class="ControlStyle" style="width:250px"');
			
			$jurusan = $conn->GetArray("select kdjurusan ||' - '|| namajurusan jurusan, kdjurusan, namapendek from lv_jurusan order by kdjurusan");
	
		}
		
		if($rkey){
			$jr = array();	
			foreach($rsjur as $rowjur) {
				$jr[$rowjur['kdjurusan']] = $rowjur['kdjurusan'];
			}
		}
				
	}
		
?>
<html>
<head>
	<script type="text/javascript" src="scripts/combo.js"></script>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center"></div>
<table width="<?= $p_tbwidth ?>" border=0>
	<tr height="40">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="100" border="0">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<? if ($c_edit) {?>
		<a href="javascript:goPostX('<?= $p_filelist; ?>','key=<?= $r_key; ?>')" class="button"><span class="list">Daftar </span></a>
		<? } ?>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:goProses();" class="button"><span class="validasi">Proses</span></a>
	</td>
	<? } ?>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	</tr>
</table>
<div align="center"><? include_once('_notifikasi.php'); ?></div>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<table width="<?= $p_tbwidth ?>" border="1" cellspacing="0" cellpadding="6" class="GridStyle">
	<tr> 
		<td class="SubHeaderBGAlt" colspan="2" align="center">Data Pustaka</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="150">Judul Pustaka *</td>
		<td class="RightColumnBG">
		<?= UI::createTextArea('judul',$rkey != '' ? $rowp['judul'] : $row['judul'],'ControlStyle',2,75,$c_edit); ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Judul Seri </td>
		<td class="RightColumnBG"><?= UI::createTextBox('judulseri',$rkey != '' ? $rowp['judulseri'] : $row['judulseri'],'ControlStyle',200,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Edisi </td>
		<td class="RightColumnBG"><?= UI::createTextBox('edisi',$rkey != '' ? $rowp['edisi'] : $row['edisi'],'ControlStyle',60,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Jenis Pustaka * </td>
		<td class="RightColumnBG">
		<?= $l_pustaka; ?>
		  <label>
		  <input type="checkbox" name="iscd" id="idcd" value="VCD" />
		  Dengan VCD/DVD
		  </label>
		</td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Jurusan * </td>
		<td class="RightColumnBG">		
			<table width="100%" cellspacing="0" cellpadding="4" bgcolor="#EEEEEE">
				<tr>
					<td align="center"><input type="checkbox" name="all" id="all" value="all" onClick="goAll()"></td>
					<td align="center"><strong>Jurusan</strong></td>
				</tr>
				
				<?php
				foreach($jurusan as $jur){?>
				<tr>
					<td align="center"><input <?=($jr[$jur['kdjurusan']] == $jur['kdjurusan'] ? "checked" : "");?> type="checkbox" name="addjurusan[]" id="addjurusan" class="pilJur" value="<?= $jur['kdjurusan'] ?>"></td>
					<td align="left"><?= ($jr[$jur['kdjurusan']] == $jur['kdjurusan'] ? "<strong>".$jur['jurusan']." [".($jur['namapendek']?$jur['namapendek']:"-")."]</strong>" : $jur['jurusan']." [ ".($jur['namapendek']?$jur['namapendek']:"-")." ]" ); ?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Pembimbing * </td>
		<td class="RightColumnBG"><?= UI::createTextBox('pembimbing',$rkey=='' ? $row['pembimbing'] : $rowp['pembimbing'],'ControlStyle',500,100,$c_edit); ?></td>
	</tr>
	
	<tr> 
		<td class="LeftColumnBG">Bahasa *</td>
		<td class="RightColumnBG"><?= $l_bahasa ?>
        </td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Penulis</td>
		<td class="RightColumnBG">
			<?php
				echo "1. ".$row['authorfirst1']. " " .$row['authorlast1']; 
				echo "<input type='hidden' value='".$row['authorfirst1']."' name='authorfirst1' id='authorfirst1'>";
				echo "<input type='hidden' value='".$row['authorlast1']."' name='authorlast1' id='authorlast1'>";
				echo "<input type='hidden' value='".$row['nrp1']."' name='nrp1' id='nrp1'>";
				if ($row['authorfirst2']) 
				{
					echo " <br> ";
					echo "2. ".$row['authorfirst2']. " " .$row['authorlast2'];
					echo "<input type='hidden' value='".$row['authorfirst2']."' name='authorfirst2' id='authorfirst2'>";
					echo "<input type='hidden' value='".$row['authorlast2']."' name='authorlast2' id='authorlast2'>";
					echo "<input type='hidden' value='".$row['nrp2']."' name='nrp2' id='nrp2'>";
				}
				if ($row['authorfirst3']) 
				{
					echo "<br>";
					echo "3. ".$row['authorfirst3']. " " .$row['authorlast3'];
					echo "<input type='hidden' value='".$row['authorfirst3']."' name='authorfirst3' id='authorfirst3'>";
					echo "<input type='hidden' value='".$row['authorlast3']."' name='authorlast3' id='authorlast3'>";
					echo "<input type='hidden' value='".$row['nrp3']."' name='nrp3' id='nrp3'>";
				}
			?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Penerbit </td>
		<td class="RightColumnBG">
		<input type="text" id="namapenerbit1" name="namapenerbit1" size="35" class="ControlRead" readonly value="<?= ($rowp['namapenerbit']?$rowp['namapenerbit']:$namapenerbit); ?>">
		<?/*?><?= UI::createTextBox('namapenerbit1',($rowp['namapenerbit']?$rowp['namapenerbit']:$namapenerbit),'ControlStyle',200,55,$c_edit); ?>
		&nbsp;&nbsp;<img src="images/tombol/breakdown.png" id="btnpenerbit" title="Cari Penerbit" style="cursor:pointer" onClick="openLOV('btnpenerbit', 'penerbit',-190, 40,'addPenerbit',500)">
		<?*/?>
		<input type="hidden" name="namapenerbit" id="namapenerbit" value="<?=($rowp['namapenerbit']?$rowp['namapenerbit']:$namapenerbit);?>">
		<input type="hidden" name="idpenerbit" id="idpenerbit" value="<?=($rowp['idpenerbit']?$rowp['idpenerbit']:"201");?>">
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tahun Terbit </td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">ISBN </td>
		<td class="RightColumnBG"><?= UI::createTextBox('isbn',$rowp['isbn'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Kota Terbit </td>
		<td class="RightColumnBG"><?= UI::createTextBox('kota',$rowp['kota'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="SubHeaderBGAlt" colspan="2" align="center">Data Eksemplar</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="150">Label *</td>
		<td class="RightColumnBG">
		<?= $l_klasifikasi ?>
		</td>
	</tr> 
	<tr> 
		<td class="LeftColumnBG">Tanggal Perolehan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglperolehan',Helper::formatDate($row['tglttb']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleperolehan" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglperolehan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleperolehan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		[ Format : dd-mm-yyyy ]
		<? } ?>
		
		</td>
	</tr>
</table>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="rkey" id="rkey" value="<?= $rkey ?>">
<input type="hidden" name="iduploadta" id="iduploadta" value="<?= $iduploadta ?>">
</form>


</div>

<div id="div_jurusan" style="display:none;"></div>
<!--<div id="div_author" style="display:none;"></div>-->

<div align="left" id="div_autocomplete" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;">
	<table bgcolor="#FFFFFF" id="tab_autocomplete" cellpadding="3" cellspacing="0"></table>
</div>
<div id="newpenerbit_block" class="popWindow" style="position:absolute;">
	<form method="post" onSubmit="isnewPenerbit();return false;">
	<input type="hidden" name="pf" value="edit_disposisi" />
    <table cellspacing=1 cellpadding="4">
		<tr class="popWindowtr">
			<td colspan="2">
				<div style="float:left;font-weight:bold;padding:2px">Tambah Penerbit</div>
				<div style="float:right">
				<span id="loadingGif2" style="display:none"><img src="images/loading.gif" /></span>
				&nbsp;
				<input type="button" class="btn_close" value="" onClick="closePop()"/>
				</div>
			</td>
		</tr>
		<tr> 
			<td width="150"><strong>Nama Penerbit</strong> </td>
			<td><?= UI::createTextBox('auth_namapenerbit','','ControlStyle',50,40,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Alamat</strong> </td>
			<td><?= UI::createTextBox('auth_alamat','','ControlStyle',50,50,true); ?></td>
		</tr>
			<tr> 
			<td><strong>Kota</strong> </td>
			<td><?= UI::createTextBox('auth_kota','','ControlStyle',50,30,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Kode Pos</strong></td>
			<td><?= UI::createTextBox('auth_kodepos','','ControlStyle',5,5,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		</tr>
		<tr> 
			<td><strong>Telepon</strong></td>
			<td><?= UI::createTextBox('auth_telp','','ControlStyle',20,15,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Fax</strong></td>
			<td><?= UI::createTextBox('auth_fax','','ControlStyle',30,15,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Email</strong></td>
			<td><?= UI::createTextBox('auth_email','','ControlStyle',50,20,true); ?></td>
		</tr>
    </table>
    <div style="margin-top:5px;padding:5px;background-color:#FFD787;text-align:center;"><input type="submit" value="Tambah" class="btn" /></div>
	</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xautox.js"></script>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>


<script type="text/javascript">

	function goJenis(){
		$('#show_jenis').fadeOut();
		$.post("<?= Helper::navAddress('sys_combo.php') ?>", {
			parent_id: $('#kdjenispustaka').val(),
		}, function(response){
			
			setTimeout("finishAjax('show_jenis', '"+escape(response)+"')", 400);
		});
		return false;
	}

function finishAjax(id, response){
  $('#'+id).html(unescape(response));
  $('#'+id).fadeIn();
}

function goAll () {
	var aa= document.getElementById('perpusform');
	var a=document.getElementsByName("addjurusan[]");
	//var x = aa.all.checked=true;
	if (aa.all.checked==true){
		checked = true
	}else{
		checked = false
	}
	
	for (var i = 0; i < a.length; i++) {
	 	//aa.pilih[i].checked = checked;
		a[i].checked = checked;
	}
	
}
</script>
<script language="javascript">
$(function(){
	   $("#tglperolehan").mask("99-99-9999");
});
$(function(){
	   $("#namajurusan").xautox ({ajaxpage: "<?= Helper::navAddress('ajax_jurusan') ?>", strpost: "f=jurusan", targetid: "kdjurusan", imgchkid: "imgauthor", posset: "2"});
});
function goProses() {
	
	
	var pesan=confirm("Apakah Anda yakin akan melakukan proses ini ?");
	
	if(pesan){
		if(cfHighlight("judul,kdjenispustaka,kdklasifikasi,kdbahasa")){
		i = 0;
		$('.pilJur').each(function () {
			if ($(this).attr('checked')) {
				i++;
			}
		});
		
		if (i==0) {
			alert('Silakan dipilih terlebih dahulu Jurusan');
			exit();
		}
				sent = $("#perpusform").serialize();
				sent += "&act=proses";
				goPostX('<?= $i_phpfile; ?>',sent);
			
		}
	}
}
function addPenerbit(kode,nama) {
	$("#idpenerbit").val(kode);
	$("#namapenerbit1").val(nama);
	$("#namapenerbit").val(nama);
}
</script>
</html>