<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and l.idunit in ($units) ";
		$s_query = " and kdsatker in ($units) ";
		$s_query = " and kdsatker in ($units) ";
	}
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tahun = Helper::removeSpecial($_REQUEST['tahun']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Rekap Surat tagih';
	
	$p_namafile = 'laporan_rekap_surattagih';

	if($r_format =='' or $r_tahun=='')
		header("location: index.php?page=home");
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sqlfak = $conn->Execute("select kdsatker, idunit,namasatker from ms_satker where 1=1 $s_query order by namasatker");
	while ($rs=$sqlfak->FetchRow()){
		$fak[]=$rs['kdsatker'];
		$nmfak[$rs['kdsatker']]=$rs['namasatker'];
	}
	$bulan=array(1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'Nopember',12=>'Desember');
	
	#transaksi
	$jumP=$conn->Execute("select to_char(t.tgltransaksi,'mm') as bulan,count(*) as jumlah
			     from pp_transaksi t
			     left join pp_eksemplar e on e.ideksemplar = t.ideksemplar
			     left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			     where to_date(to_char(t.tgltransaksi,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY') $sqlAdminUnit
			     group by to_char(t.tgltransaksi,'mm')
			     order by to_char(t.tgltransaksi,'mm')");
	
	$sqlJP="select t.idanggota,to_char(t.tgltransaksi,'mm') as bulan,a.idunit
		from pp_transaksi t
		join ms_anggota a on t.idanggota=a.idanggota 
		join ms_satker j on a.idunit=j.kdsatker
		where to_date(to_char(t.tgltransaksi,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY')
		group by t.idanggota,to_char(t.tgltransaksi,'mm'),a.idunit
		order by to_char(t.tgltransaksi,'mm')";
	
	$p_exeJP=$conn->Execute($sqlJP);
	
	while ($perFak=$p_exeJP->FetchRow()){
		$arrJP[$perFak['idunit']][$perFak['bulan']][]=$perFak['idanggota'];
	}
	
	$sqlJB="select to_char(t.tgltransaksi,'mm') as bulan,a.idunit,count(t.idtransaksi) as jumlah
		from pp_transaksi t
		join ms_anggota a on t.idanggota=a.idanggota 
		join ms_satker j on a.idunit=j.kdsatker
		where to_date(to_char(t.tgltransaksi,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY') and t.kdjenistransaksi='PJN'
		group by to_char(t.tgltransaksi,'mm'),a.idunit
		order by to_char(t.tgltransaksi,'mm')";
	
	$p_exeJB=$conn->Execute($sqlJB);
	
	
	while ($row=$p_exeJB->FetchRow()){
		$arJumB[$row['idunit']][$row['bulan']]=$row['jumlah'];
	}
	
	$sqlKP="select t.idanggota,to_char(t.tglpengembalian,'mm') as bulan,a.idunit from pp_transaksi t
			join ms_anggota a on t.idanggota=a.idanggota 
			join ms_satker j on a.idunit=j.kdsatker
			where to_date(to_char(t.tglpengembalian,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY') and t.kdjenistransaksi='PJN' and t.statustransaksi='0'
			group by t.idanggota,to_char(t.tglpengembalian,'mm'),a.idunit
			order by a.idunit,to_char(t.tglpengembalian,'mm')";
	
	$p_exeKP = $conn->Execute($sqlKP);
	
	while ($row=$p_exeKP->FetchRow()){
		$arKemP[$row['idunit']][$row['bulan']][]=$row['idanggota'];
	}
	
	$sqlKB="select to_char(t.tglpengembalian,'mm') as bulan,a.idunit,count(t.idtransaksi) as jumlah
			from pp_transaksi t
			join ms_anggota a on t.idanggota=a.idanggota
			join ms_satker j on a.idunit=j.kdsatker
			where to_date(to_char(t.tglpengembalian,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY') and t.kdjenistransaksi='PJN' and t.statustransaksi='0'
			group by to_char(t.tglpengembalian,'mm'),a.idunit
			order by to_char(t.tglpengembalian,'mm')";
	
	$p_exeKB = $conn->Execute($sqlKB);
	
	while ($row=$p_exeKB->FetchRow()){
		$arKemB[$row['idunit']][$row['bulan']]=$row['jumlah'];
	}
	
	$sqlS="select to_char(t.tgltagihan,'mm') as bulan,a.idunit,count(t.idtagihan) as jumlah
			from pp_tagihan t
			join ms_anggota a on t.idanggota=a.idanggota /*and a.kdjenisanggota='M'*/
			left join ms_satker j on a.idunit=j.kdsatker
			where to_date(to_char(t.tgltagihan,'YYYY'),'YYYY')=to_date('$r_tahun','YYYY')
			group by to_char(t.tgltagihan,'mm'),a.idunit
			order by to_char(t.tgltagihan,'mm')";
	
	$p_exeS = $conn->Execute($sqlS);
	
	while ($row=$p_exeS->FetchRow()){
		$arS[$row['idunit']][$row['bulan']]=$row['jumlah'];
	}
?>        

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table cellpadding=0 cellspacing=0 width="1000">
	<tr>
		<td align="center"><font size="+1"><b>REKAP SURAT TAGIHAN TAHUN <u><?= $r_tahun ?></u></b></font></td>
	</tr>
</table><br>
<table border=1 cellpadding=0 cellspacing=0 width="1000" style="border-collapse:collapse">
	
	<tr height="20">
		<td rowspan=4 align="center">UNIT</td>
		<td colspan="30" align="center">BULAN</td>
		<td colspan=5 rowspan=2 align="center">TOTAL</td>
	</tr>
	
	<tr height="20">
		<? for ($a=1;$a<=6;$a++){ 
		?>
		
		<td colspan=5 align="center"><?= $bulan[$a] ?></td>
		<? 
		} ?>
	</tr>
	<tr height="20">
		<? for ($b=1;$b<=7;$b++){ ?>
		<td colspan=2 align="center">Jumlah</td>
		<td colspan=3 align="center">Kembali</td>
		<? } ?>
		 
	</tr>
	<tr height="20">
		<? for ($d=1;$d<=7;$d++){ ?>
		<td align="center">P</td>
		<td align="center">B</td>
		<td align="center">P</td>
		<td align="center">B</td>
		<td align="center">S</td>
		
		<? } ?>
	</tr>
	<? 
	for($e=1;$e<count($fak);$e++){ ?>
	<tr height="20">
		
		<td><?= $nmfak[$fak[$e]] ?></td>
		<? for ($f=1;$f<=6;$f++){ ?>
		<td align="center"><?= $r[$f]=count($arrJP[$fak[$e]][$f]); $rjum[$e] += $r[$f] ?></td>
		<td align="center"><?= $s[$f]=$arJumB[$fak[$e]][$f] =='' ? 0 : $arJumB[$fak[$e]][$f] ; $sjum[$e] += $s[$f]; ?></td>
		<td align="center"><?= $t[$f]=count($arKemP[$fak[$e]][$f]); $tjum[$e] += $t[$f] ?></td>
		<td align="center"><?= $u[$f]=$arKemB[$fak[$e]][$f] =='' ? 0 : $arKemB[$fak[$e]][$f]; $ujum[$e] += $u[$f] ?> </td>
		<td align="center"><?= $v[$f]=$arS[$fak[$e]][$f] =='' ? 0 : $arS[$fak[$e]][$f]; $vjum[$e] += $vjum[$f] ?></td>
		<? } ?>
		<td align="center"><?= $rjum[$e] ?></td>
		<td align="center"><?= $sjum[$e] ?></td>
		<td align="center"><?= $tjum[$e] ?></td>
		<td align="center"><?= $ujum[$e] ?></td>
		<td align="center"><?= $vjum[$e] ?></td>
	</tr>
	<? } ?>
	<? for ($ff=1;$ff<=6;$ff++){ 
		for($ee=1;$ee<count($fak);$ee++){
			$rr[$ee]=count($arrJP[$fak[$ee]][$ff]); $rrjum[$ff] += $rr[$ee];
			$ss[$ee]=$arJumB[$fak[$ee]][$ff]; $ssjum[$ff] += $ss[$ee];
			$tt[$ee]=count($arKemP[$fak[$ee]][$ff]); $ttjum[$ff] += $tt[$ee];
			$uu[$ee]=$arKemB[$fak[$ee]][$ff]; $uujum[$ff] += $uu[$ee];
			$vv[$ee]=$arS[$fak[$ee]][$ff]; $vvjum[$ff] += $vv[$ee];
		}
		$rrrjum +=$rrjum[$ff];
		$sssjum +=$ssjum[$ff];
		$tttjum +=$ttjum[$ff];
		$uuujum +=$uujum[$ff];
		$vvvjum +=$vvjum[$ff];
	}
	?>
	<tr height="20">
		<td  align="center">TOTAL</td>
		<? for ($ff=1;$ff<=6;$ff++){ ?>
		<td align="center"><?= $rrjum[$ff] ?></td>
		<td align="center"><?= $ssjum[$ff] ?></td>
		<td align="center"><?= $ttjum[$ff] ?></td>
		<td align="center"><?= $uujum[$ff] ?></td>
		<td align="center"><?= $vvjum[$ff] ?></td>
		<? } ?>
		<td align="center"><?= $rrrjum ?></td>
		<td align="center"><?= $sssjum ?></td>
		<td align="center"><?= $tttjum ?></td>
		<td align="center"><?= $uuujum ?></td>
		<td align="center"><?= $vvvjum ?></td>
	</tr>

</table>
<br>
<table border=1 cellpadding=0 cellspacing=0 width="1000" style="border-collapse:collapse">
	<tr height="20">
		<td rowspan=4 align="center">UNIT</td>
		<td colspan="30" align="center">BULAN</td>
		<td colspan=5 rowspan=2 align="center">TOTAL</td>
	</tr>
	
	<tr height="20">
		<? for ($g=7;$g<=12;$g++){ 
		?>
		
		<td colspan=5 align="center"><?= $bulan[$g] ?></td>
		<? 
		} ?>
	</tr>
	<tr height="20">
		<? for ($h=1;$h<=7;$h++){ ?>
		<td colspan=2 align="center">Jumlah</td>
		<td colspan=3 align="center">Kembali</td>
		<? } ?>
		 
	</tr>
	<tr height="20">
		<? for ($i=1;$i<=7;$i++){ ?>
		<td align="center">P</td>
		<td align="center">B</td>
		<td align="center">P</td>
		<td align="center">B</td>
		<td align="center">S</td>
		
		<? } ?>
	</tr>
	<? 
	for($j=1;$j<count($fak);$j++){ ?>
	<tr height="20">
		
		<td><?= $nmfak[$fak[$j]] ?></td>
		<? for ($k=7;$k<=12;$k++){ ?>
		<td align="center"><?= $x[$k]=count($arrJP[$fak[$j]][$k]); $xjum[$j] += $x[$k]; ?></td>
		<td align="center"><?= $y[$k]=$arJumB[$fak[$j]][$k] =='' ? 0 : $arJumB[$fak[$j]][$k] ; $yjum[$j] += $y[$k];?></td>
		<td align="center"><?= $z[$k]=count($arKemP[$fak[$j]][$k]); $zjum[$j] += $z[$k]; ?></td>
		<td align="center"><?= $w[$k]=$arKemB[$fak[$j]][$k] =='' ? 0 : $arKemB[$fak[$j]][$k]; $wjum[$j] += $w[$k] ?> </td>
		<td align="center"><?= $q[$k]=$arS[$fak[$j]][$k] =='' ? 0 : $arS[$fak[$j]][$k]; $qjum[$j] += $q[$k] ?></td>
		<? 
		
		
		} ?>
		<td align="center"><?= $xjum[$j] ?></td>
		<td align="center"><?= $yjum[$j] ?></td>
		<td align="center"><?= $zjum[$j] ?></td>
		<td align="center"><?= $wjum[$j] ?></td>
		<td align="center"><?= $qjum[$j] ?></td>
	</tr>
	<? }for ($kk=7;$kk<=12;$kk++){ 
		for($jj=1;$jj<count($fak);$jj++){
			$xx[$jj]=count($arrJP[$fak[$jj]][$kk]); $xxjum[$kk] += $xx[$jje];
			$yy[$jj]=$arJumB[$fak[$jj]][$kk]; $yyjum[$kk] += $yy[$jj];
			$zz[$jj]=count($arKemP[$fak[$jj]][$kk]); $zzjum[$kk] += $zz[$jj];
			$ww[$jj]=$arKemB[$fak[$jj]][$kk]; $wwjum[$kk] += $ww[$jj];
			$qq[$jj]=$arS[$fak[$jj]][$kk]; $qqjum[$kk] += $qq[$jj];
		}
		$xxxjum +=$xxjum[$kk];
		$yyyjum +=$yyjum[$kk];
		$zzzjum +=$zzjum[$kk];
		$wwwjum +=$wwjum[$kk];
		$qqqjum +=$qqjum[$kk];
	}
	?>
	<tr height="20">
		<td  align="center">TOTAL</td>
		<? for ($kk=7;$kk<=12;$kk++){ ?>
		<td align="center"><?= $xxjum[$kk] ?></td>
		<td align="center"><?= $yyjum[$kk] ?></td>
		<td align="center"><?= $zzjum[$kk] ?></td>
		<td align="center"><?= $wwjum[$kk] ?></td>
		<td align="center"><?= $qqjum[$kk] ?></td>
		<? } ?>
		<td align="center"><?= $xxxjum ?></td>
		<td align="center"><?= $yyyjum ?></td>
		<td align="center"><?= $zzzjum ?></td>
		<td align="center"><?= $wwwjum ?></td>
		<td align="center"><?= $qqqjum ?></td>
	</tr>

</table>
		
</div>

</body>
</html>