<? 
	//defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//generate metadata
	$p_namafile = 'metadat';
	header('Content-Type: text/xml');
	header('Content-Disposition: attachment; filename="'.$p_namafile.'.xml"');
	//break;
	
	$conn->debug = false;
	
	$sqlstr = "select *, coalesce(keywords,'') as keywords from ms_pustaka p 
				left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
				left join ms_penerbit pe on pe.idpenerbit=p.idpenerbit
				where j.islokal=1 order by judul";
	$rs = $conn->Execute($sqlstr);
	
	//start metadata
	echo '<?xml version="1.0"?>';
	echo '<metadata	
	xmlns="http://example.org/myapp/"
  	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  	xsi:schemaLocation="http://example.org/myapp/ http://example.org/myapp/schema.xsd"
  	xmlns:dc="http://purl.org/dc/elements/1.1/"
  	xmlns:dcterms="http://purl.org/dc/terms/">';
	while ($row = $rs->FetchRow()){
		echo '<dc:title>'.$row['judul'].'</dc:title>';
		echo '<dc:creator>'.$row['authorfirst1'].' '.$row['authorlast1'].'</dc:creator>';
		echo '<dc:subject>'.$row['keywords'].'</dc:subject>';
		echo '<dc:description>'.$row['sinopsis'].'</dc:description>';		
		echo '<dc:publisher>'.$row['namapenerbit'].'</dc:publisher>';
		echo '<dc:contributor>'.$row['keywords'].'</dc:contributor>';
		echo '<dc:date>'.$row['tahunterbit'].'</dc:date>';
		echo '<dc:type_material>'.$row['namajenispustaka'].'</dc:type_material>';
		echo '<dc:permalink>http://192.168.1.2/ubaya/www/ubayaperpus/webportal-2</dc:permalink>';
		echo '<dc:right>'.$row['namajenispustaka'].'</dc:right>';
	}
	echo '</metadata>';
?>