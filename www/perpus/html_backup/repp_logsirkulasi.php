<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	$periode = Helper::periodeISO($r_tgl1);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan ISO Sirkulasi';
	
	$p_namafile = 'rekap_lSOsirkulasi'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select l.*,p.nopanggil from pp_logtransaksi l
			join pp_eksemplar e on l.regcomp=e.noseri
			join ms_pustaka p on e.idpustaka=p.idpustaka
			where waktumulai between '$r_tgl1 00:00:00' and '$r_tgl2 23:59:59' and status=$r_jenis";
	
	$sql .=" order by waktumulai";	
	$rs = $conn->Execute($sql);
	$i=0;
	$ket=0;
	while($row=$rs->FetchRow()){
		$ArIdanggota[$i]=$row['idanggota'];
		$ArRegcomp[$i]=$row['regcomp'];
		$ArNopanggil[$i]=$row['nopanggil'];
		$ArWmulai[$i]=$row['waktumulai'];
		$ArWselesai[$i]=$row['waktuselesai'];
		$ArTuser[$i]=$row['t_user'];
		$time=strtotime($row['waktuselesai'])-strtotime($row['waktumulai']);
		
		if($time<=60){
			$ket += 1;
			$ArKet[$i] = 'Tercapai';
		}else
			$ArKet[$i] = 'Tidak tercapai';
		
		$i++;		
	}
	//echo $ket;
	$rsc=$rs->RowCount();
	if($rsc!=0)
	$persen=($ket/$rsc*100);
	else
	$persen=0;
	
	if($r_jenis=='1')
		$label="peminjaman";
	elseif($r_jenis=='2')
		$label="perpanjangan";
	elseif($r_jenis=='0')
		$label="pengembalian";

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=700>
	<tr>
		<td width=100><img src="<?= $dirIcon.'logounusa.jpg' ?>" width=70 height=60></td>
		<td valign="center">Monitoring Sasaran Mutu<br>FM-MR-04/-/01/Rev 01<br>BDU: Perpustakaan</td>
	</tr>
</table><br>
<table width=700 border=0 cellpadding="2" cellspacing="0">
  <tr>
	<td>Nomor Prosedur : PM-PERPUS-03<br>
	Nama Dokumen : Layanan Sirkulasi Buku Kepada Pelanggan<br>
		Sasaran Mutu : Peminjaman,perpanjangan,pengembalian buku kepada pelanggan<br>
		Kriteria Keberhasilan : <?= $periode ?> % waktu yang diperlukan untuk masing-masing<br>
		proses <?= $label ?> per buku (mulai entry nomer identitas pelanggan atau no regcomp
		buku sampai dengan <br>pencetakan struk)
		tidak lebih dari 1 (satu) menit<br>
		Periode : <?= Helper::formatDate($r_tgl1) ." s/d ". Helper::formatDate($r_tgl2) ?><br>
		Keberhasilan : Tercapai : <?= Helper::formatNumber($persen,'0',false,true) ?> %
</table><br>

<table width="700" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="70" align="center"><strong>Id Anggota</strong></th>
    <th width="70" align="center"><strong>NO INDUK</strong></th>
    <th width="100" align="center"><strong>Call Number</strong></th>
    <th width="80" align="center"><strong>Tanggal Pinjam</strong></th>
    <th width="80" align="center"><strong>Proses Awal</strong></th>
    <th width="80" align="center"><strong>Proses Akhir</strong></th>
    <th width="80" align="center"><strong>Petugas</strong></th>
    <th><strong>Keterangan</strong></th>
    <th><strong>Nomor Registrasi MR</strong></th>
	
  </tr>
  <?php
	$x=0;
	for($n=0;$n<$rsc;$n++)
	{ $x++		
	?>
    <tr height=25>
	<td align="center"><?= $x ?></td>
    <td align="left"><?= $ArIdanggota[$n]?></td>
    <td align="left"><?= $ArRegcomp[$n] ?></td>
    <td align="left"><?= $ArNopanggil[$n] ?></td>
    <td align="center"><?= Helper::tglEng($ArWmulai[$n]) ?></td>
	<td align="center"><?= Helper::JustTime($ArWmulai[$n]) ?></td>
	<td align="center"><?= Helper::JustTime($ArWselesai[$n]) ?></td>
	<td align="left"><?= $ArTuser[$n] //= floor($time/60)." Menit ".$time%60 . " detik" ?></td>
	<td><?= $ArKet[$n] ?></td>
    <td>&nbsp;</td>
  </tr>
	<?  } ?>
	<? if($rsc==0) { ?>
	<tr height=25>
		<td align="center" colspan=10 >Tidak Sirkulasi</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah sirkulasi <?= $label ?>: <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>