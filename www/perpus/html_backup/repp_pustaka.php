<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);

	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Jumlah Pustaka';
	
	$p_namafile = 'laporan_jumlah_pustaka';

	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//query's
	$p_pinjam = $conn->Execute("SELECT e.kdklasifikasi, k.namaklasifikasi, COALESCE(count(*), 0) AS jumlahpinjam
		FROM pp_eksemplar e 
		JOIN pp_transaksi t ON e.ideksemplar = t.ideksemplar
		join lv_klasifikasi k on k.kdklasifikasi = e.kdklasifikasi
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
		WHERE t.tglpengembalian IS NULL $sqlAdminUnit
		GROUP BY e.kdklasifikasi, k.namaklasifikasi ");
	while ($rowp=$p_pinjam->FetchRow()){
		$pinjam[$rowp['kdklasifikasi']]	= $rowp['jumlahpinjam'];
	}
	
		
	$p_eks = $conn->Execute("SELECT e.kdklasifikasi, k.namaklasifikasi, count(*) AS jumlaheks
		FROM pp_eksemplar e 
		join MS_PUSTAKA p on p.IDPUSTAKA = e.IDPUSTAKA 
		JOIN lv_klasifikasi k ON e.kdklasifikasi = k.kdklasifikasi
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi
		WHERE 1=1 $sqlAdminUnit
		GROUP BY e.kdklasifikasi, k.namaklasifikasi");
	
	$pin_count=$conn->GetRow("SELECT count(*) jumlah 
			FROM pp_eksemplar e
			left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			JOIN pp_transaksi t ON e.ideksemplar = t.ideksemplar
			WHERE t.tglpengembalian IS NULL $sqlAdminUnit ");
	
	$p_total=$conn->GetRow("SELECT count(*) jumlah 
			FROM pp_eksemplar e
			left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			WHERE 1=1 $sqlAdminUnit ");
	
	$p_kondisi=$conn->Execute("select k.namakondisi, count(*) as jum
				  from lv_kondisi k
				  left join pp_eksemplar e on k.kdkondisi=e.kdkondisi
				  left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				  join ms_pustaka p on p.idpustaka = e.idpustaka
				  where 1=1 $sqlAdminUnit 
				  group by k.kdkondisi,k.namakondisi
				  order by k.kdkondisi ");	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	

<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellspacing="0" cellpadding="0" border="0">
	<tr><td  colspan="2" align="center"><h2><strong>Laporan Jumlah Bahan Pustaka</strong></h2></td></tr>
</table>

<table width=675 border=1>
	<tr height=20>
		<th rowspan=2><strong>Klasifikasi Pustaka<strong></th>
		<th colspan=2>Jumlah</th>
		<th rowspan=2>Total</th>
	</tr>
	<tr height=20>
		<th>Terpinjam</th>
		<th>Tersedia</th>
	</tr>
	<? while ($rowe=$p_eks->FetchRow()){
	?>
	<tr height=20>
		<td>&nbsp;<?= $rowe['namaklasifikasi'] ?></td>
		<td align="center"><?= (int) $rowe['jumlaheks'] - (int) $pinjam[$rowe['kdklasifikasi']]; ?></td>
		<td align="center"><?= $pinjam[$rowe['kdklasifikasi']] ?> </td>
		<td align="center"><?= $rowe['jumlaheks'] ?></td>
	</tr>
		<? } ?>
	<tr>
		<td><strong>&nbsp;Jumlah</strong></td>
		<td align="center"><strong><?= ($p_total['jumlah']-$pin_count['jumlah']) ?></strong> </td>
		<td align="center"><strong><?= $pin_count['jumlah'] ?> </strong></td>
		<td align="center"><strong><?= $p_total['jumlah'] ?></strong> </td>
	</tr>
</table><br><br><br>
<table width=400 border=1>
	<tr height=35>
		<th>Kondisi Pustaka</th>
		<th>Jumlah</th>
	</tr>
	<? while ($rowk=$p_kondisi->FetchRow()){ ?>
	<tr height=20>
		<td>&nbsp;<?= $rowk['namakondisi'] ?> </td>
		<td align="center"><?= $rowk['jum'] ?></td>
	</tr>
	<? } ?>
</table>
</div>
</body>

</html>