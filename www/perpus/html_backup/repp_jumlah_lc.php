<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	//print_r($_REQUEST);
	//definisi hari,bulan,tahun
	$bulantahun = date('M Y');
	$sekarang = date('d F Y');
	
	$kepala = Sipus::getHeadPerpus();
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_grafik = 'batang';
	$r_kdjurusan = Helper::removeSpecial($_REQUEST['kdjurusan']);
	$r_kdjenispustaka = Helper::removeSpecial($_REQUEST['kdjenispustaka']);
	$r_tgl1 = Helper::formatDate($_REQUEST['tgl1']); //2012-01-30
	$r_tgl2 = Helper::formatDate($_REQUEST['tgl2']);
	// $r_thn1 = Helper::removeSpecial($_REQUEST['thnakademik1']);
	// $r_thn2 = Helper::removeSpecial($_REQUEST['thnakademik2']);
	// $r_semester = Helper::removeSpecial($_REQUEST['semester']);
	
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Statistik Karya Ilmiah Tahun '.$r_tgl1.'/'.$r_tgl2 ;
	
	$p_namafile = 'Laporan Statistik Karya Ilmiah_'.$r_tgl1.'-'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql_jnspustaka = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal=1 order by kdjenispustaka");
	$count = $sql_jnspustaka->RowCount();
	$loop=0;
	$str_jns = "(";
	while($row_jns = $sql_jnspustaka->FetchRow()){
		$str_jns .= "'".$row_jns['kdjenispustaka']."'";
		$loop++;
		if($loop!=$count)
			$str_jns .= ",";
	}
	$str_jns .= ")";
	
	// if($r_semester == 'ganjil'){ //juli-desember
		
		// $monstart = 7;
	
		$sql = "select p.idcounter as kd_jur, to_char(p.tglperolehan,'mm') as bulan,count(e.ideksemplar) as jml_ki from ms_pustaka p left join pp_eksemplar e on e.idpustaka=p.idpustaka 
				left join lv_jurusan lj on lj.kdjurusan::integer=p.idcounter
				where p.tglperolehan between '$r_tgl1' and '$r_tgl2' ";  
				
    // }
	// else {  //januari-juni
	
		// $monstart = 1;
	
		// $sql = "select p.idcounter as kd_jur, to_char(p.tglperolehan,'mm') as bulan,count(e.ideksemplar) as jml_ki from ms_pustaka p left join pp_eksemplar e on e.idpustaka=p.idpustaka 
				// left join lv_jurusan lj on lj.kdjurusan::integer=p.idcounter
				// where ((to_char (p.tglperolehan,'mm') between '01' and '06') and (to_char (p.tglperolehan,'yyyy') = '$r_thn2')) ";
				
		
	// }
	
	// if($r_kdjurusan == 'semua'){
		// $sql .= "";
	// }else{
		// // $sql .= " and kdjurusan='$r_kdjurusan' ";
		// $sql .= " and idcounter=$r_kdjurusan ";
	// }
	
	if($r_kdjenispustaka == 'semua'){
		$sql .= "";
	}else{
		$sql .= " and kdjenispustaka='$r_kdjenispustaka'";
	}	
	
	$sql .=" group by kd_jur,bulan order by bulan";

	$rs = $conn->Execute($sql);
	$jml_jur = $rs->RowCount();
	
	//membuat array Kode DDC dan Bulan (bersifat tetap);
	$bulan = array();
	
	// mencatat jurusan
	$sql_jur = $conn->Execute("select * from lv_jurusan order by kdjurusan asc");
	$jml_jur = $sql_jur->RowCount();
	
	$sql_jur2 = $conn->Execute("select * from lv_jurusan order by kdjurusan asc");
	
	$bulan[1] = "Januari";
	$bulan[2] = "Februari";
	$bulan[3] = "Maret";
	$bulan[4] = "April";
	$bulan[5] = "Mei";
	$bulan[6] = "Juni";
	$bulan[7] = "Juli";
	$bulan[8] = "Agustus";
	$bulan[9] = "September";
	$bulan[10] = "Oktober";
	$bulan[11] = "November";
	$bulan[12] = "Desember";
	
	$bulan_grafik = '[';
	$jml_bulan=0;for($k=(int)substr($r_tgl1,5,2); $k<=(int)substr($r_tgl2,5,2); $k++){
		$bulangrafik[$k] = $bulan[$k];
		$bulan_grafik .= "'".$bulan[$k]."',";
		$jml_bulan++;
	}
	$bulan_grafik .= ']';
	
	while($row = $rs->FetchRow())
	{
		$data[(int) $row['bulan']][$row['kd_jur']] = $row['jml_ki'];
	}
	

?>

<html>
<head>
<script type="text/javascript" src="includes/FusionCharts/JSClass/FusionCharts.js" ></script>
<script src="scripts/rgraph/RGraph.common.core.js"></script>
<script src="scripts/rgraph/RGraph.common.annotate.js"></script>  <!-- Just needed for annotating -->
<script src="scripts/rgraph/RGraph.common.context.js"></script>   <!-- Just needed for context menus -->
<script src="scripts/rgraph/RGraph.common.effects.js"></script>   <!-- Just needed for visual effects -->
<script src="scripts/rgraph/RGraph.common.key.js"></script>       <!-- Just needed for keys -->
<script src="scripts/rgraph/RGraph.common.resizing.js"></script>  <!-- Just needed for resizing -->
<script src="scripts/rgraph/RGraph.common.tooltips.js"></script>  <!-- Just needed for tooltips -->
<script src="scripts/rgraph/RGraph.common.zoom.js"></script>      <!-- Just needed for zoom -->
<script src="scripts/rgraph/RGraph.common.dynamic.js"></script>      <!-- Just needed for zoom -->
<script src="scripts/rgraph/RGraph.bar.js"></script>              <!-- Just needed for Bar charts -->

<title><?= $p_window ?></title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Statistik Karya Ilmiah <br>Bulan <?= Helper::indoMonth((int)substr($r_tgl1,5,2))==Helper::indoMonth((int)substr($r_tgl2,5,2)) ? Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4): Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4)." - ".Helper::indoMonth((int)substr($r_tgl2,5,2))." ".substr($r_tgl2,0,4) ?></h2>
  	</strong></td>
  </tr>
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()">Cetak Laporan</a></td>
	</tr>
	<? } ?>
</table>


<table width="800" border="1" cellpadding="2" cellspacing="0">

	<tr>
		<th rowspan="2"> Jurusan </th>
		<th colspan="<?= $jml_bulan?>" align="center"> BULAN </th>
		<th rowspan="2"> JUMLAH </th>
	</tr>
	<!-- membuat kolom bulan sesuai semester beserta kolom judul/eksemplar -->

	<tr height=25>
		<? for ($i=(int)substr($r_tgl1,5,2); $i<=(int)substr($r_tgl2,5,2);$i++) { ?>
		<th width="130" align="center"><strong> <?= $bulan[$i] ?> </strong></th>
		<? } ?>
	</tr>

	<!-- mengisikan jumlah peminjam untuk semester ganjil -->
	<?php 
		$total=0; 
		while ($row_jur = $sql_jur->FetchRow()){?>
			<tr height="25">
				<td nowrap><?= $row_jur['namajurusan']; ?></td>
				<? 
					$jumlah=0;
					for($j=(int)substr($r_tgl1,5,2); $j<=(int)substr($r_tgl2,5,2); $j++) { 
				
						if($data[$j][$row_jur['kdjurusan']] > 0) { 
						?> 
							<td align="center"> <?=$data[$j][$row_jur['kdjurusan']]?></td> 
						<?
						} else { 
						?>
							<td align="center">0</td>
						<? }
						$jumlah += $data[$j][$row_jur['kdjurusan']];
						$jml_bawah[$j] += $data[$j][$row_jur['kdjurusan']];
						$g_jumlah[$bulan[$j]] = $jml_bawah[$j];
					}?>
				<td align="center"> <?= $jumlah ?></td>
			</tr>
			<?php $total += $jumlah;?>
		<?
		}
		?>
			<tr>
				<td align="right"><strong>Jumlah</strong></td>
			<?
				$arr_jml = '[';
				$arr_tooltip = '[';
				for($j=(int)substr($r_tgl1,5,2); $j<=(int)substr($r_tgl2,5,2); $j++) { 
					$arr_jml .= $jml_bawah[$j].",";
					$arr_tooltip .= "'".$bulan[$j]." - ".$jml_bawah[$j]." eks',";
			?>
				<td align="center"> <?= $jml_bawah[$j] ?></td>
			<? } 
				$arr_jml .= "]";
				$arr_tooltip .= "]";
			?>
				<td align="center"><?= $total?></td>
			</tr> 
</table><br><br>


<!-- Menampilkan grafik -->
<? if($r_format == 'html') { ?>
<table>
	<tr>
		<td align="center"> Grafik Batang Jumlah Karya Ilmiah </td>
	</tr>
	<tr>
		<td width="210" nowrap align="center" ><canvas id="myCanvas" width="800" height="250">[No canvas support]</canvas></td>
	</tr>
</table>
<? } ?>
<br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
	</table>
</div>
</body>
</html>
<script>
// Bar Chart
	window.onload = function ()
    {
        var data = <?= $arr_jml;?> ;
		var bar = new RGraph.Bar('myCanvas', data);
        
         bar.Set('chart.labels', <?= $bulan_grafik?>);
		bar.Set('chart.tooltips', <?= $arr_tooltip?>);
        bar.Set('chart.gutter.left', 45);
        bar.Set('chart.background.barcolor1', 'white');
        bar.Set('chart.background.barcolor2', 'white');
        bar.Set('chart.background.grid', true);
		bar.Set('chart.variant', '3d');
        bar.Set('chart.colors', ['#2A17B1']);
		bar.Draw();
    }
</script>