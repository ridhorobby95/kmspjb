<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	//$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_po = Helper::removeSpecial($_GET['nopo']);
	
	if($r_po=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Purchase Order';
	
	$p_namafile = 'rekap_po'.$r_anggota.'_'.$r_tgl1.'_'.$r_tgl2;
	


	$sql = "select o.*,po.*,s.namasupplier,u.namapengusul from pp_orderpustaka o
			join pp_usul u on o.idusulan=u.idusulan
			join pp_po po on o.idpo=po.idpo
			left join ms_supplier s on o.supplierdipilih=s.kdsupplier
			where po.nopo='$r_po'";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Purchase Order</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> Nomor PO</td>
	<td>: <?= $r_po ?>
  </tr>    
  <tr>
	<td width=150> Tanggal PO</td>
	<td>: <?= Helper::tglEng($row->fields['tglpo']) ?>
  </tr>
  <tr>
	<td width=150> Nama Supplier</td>
	<td>: <?= $row->fields['namasupplier'] ?>
  </tr>

</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="200" align="center"><strong>Judul</strong></th>
	<th width="100" align="center"><strong>Pengusul</strong></th>
	<th width="100" align="center"><strong>Harga PO</strong></th>
	<th width="100" align="center"><strong>Jumlah PO</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td ><?= $rs['namapengusul'] ?></td>
	<td align="right">&nbsp;<?= Helper::formatNumber($rs['hargadipilih'],'0',true,true) ?></td>
	<td align="center"><?= $rs['qtypo'] ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pengusulan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=6><b>Jumlah : <?= $rsc ?></b></td></tr>
   
</table>
<br>
	<table>
	<tr>
		<td width="400">&nbsp;</td>
		<td>
		Surabaya, <?= Helper::formatDateInd($row->fields['tglpo']) ?><br>
		Penerima,<br><br><br><br>
		<?= Sipus::Search($conng,sc_user,username,$row->fields['npkpo'],userdesc); ?>
		</td>
	</tr>
	</table>


</div>
</body>
</html>