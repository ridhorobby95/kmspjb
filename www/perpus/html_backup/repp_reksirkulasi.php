<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Sirkulasi Pustaka';
	
	$p_namafile = 'rekap_sirkulasi'.$r_tgl1.'_'.$r_tgl2;
	$p_tbwidth = 950;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.statustransaksi,r.tgltransaksi, r.tgltenggat,e.noseri, p.judul,l.namalokasi from pp_transaksi r
			join um.users u on u.nid = r.idanggota 
			join pp_eksemplar e on r.ideksemplar=e.ideksemplar
			join ms_pustaka p on e.idpustaka = p.idpustaka
			left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			JOIN lv_jenispustaka vj ON vj.kdjenispustaka = p.kdjenispustaka
			where (to_date(to_char(r.tgltransaksi,'dd-mm-YYYY'),'dd-mm-YYYY') between '$r_tgl1' and '$r_tgl2')
			 $sqlAdminUnit ";
			
	if($r_lokasi)
		$sql .=" and l.kdlokasi = '$r_lokasi' ";
			
	$sql .=" order by r.tgltransaksi";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	
	if ($r_lokasi)
		$namalokasi = $conn->getOne("select namalokasi from lv_lokasi where kdlokasi = '$r_lokasi' ");
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=<?= $p_tbwidth?>>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=<?= $p_tbwidth?> cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Rekap Sirkulasi Pustaka <?php echo $namalokasi ? '<br>'.$namalokasi : ''?></h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
  </tr>
</table>
<table width="<?= $p_tbwidth?>" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>No. Induk</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
    <th width="200" align="center"><strong>Lokasi</strong></th>
	<th width="100" align="center"><strong>Tanggal Transaksi</strong></th>
	<th width="100" align="center"><strong>Tanggal Harus Kembali</strong></th>
	<th width="100" align="center"><strong>Status</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $rs['noseri'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td ><?= $rs['namalokasi'] ?></td>
	<td align="center"><?= Helper::tglEngTime($rs['tgltransaksi']) ?></td>
	<td align="center"><?= $rs['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($rs['tgltenggat']) ?></td>
	<td align="center"><?= $rs['statustransaksi']==0 ? "Kembali" : ($rs['statustransaksi']==1 ? "Pinjam" : "Perpanjang") ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=7><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>
