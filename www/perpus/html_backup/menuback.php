<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	session_start();
	
	// bila belum login redirect ke index
	if(!isset($_SESSION['PERPUS_USER'])) {
		Helper::navigateOut();
		exit();
	}
		
	header("Location: ".$_SESSION['menupage']);
	
?>