<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and (a.idunit in ($units) or u.idunit in ($units)) ";
		$sqlAdminUnit_b = " and a.idunit in ($units) ";
		$sqlAdminUnit_u = " and u.idunit in ($units) ";
	}
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_asal = Helper::removeSpecial($_POST['asal']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	$r_jenis = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1'])).' 00:00';
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2'])).' 23:59';
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Kirim/Terima Pustaka';
	
	$p_namafile = 'laporan_'.$r_asal.'_'.$r_jenis.'_'.$r_lokasi.'_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select lk.namaklasifikasi, ll.namalokasi,h.*, e.noseri, e.kdklasifikasi, p.judul, p.nopanggil, e.kdlokasi as lokasi, p.edisi, p.authorfirst1,
			p.authorlast1, p.namapenerbit,p.isbn
		from pp_historymaintaining h
		join pp_eksemplar e on h.ideksemplar=e.ideksemplar
		join ms_pustaka p on e.idpustaka=p.idpustaka
		left join lv_klasifikasi lk on lk.kdklasifikasi=e.kdklasifikasi
		left join lv_lokasi ll on ll.kdlokasi=e.kdlokasi
		where to_char(h.tglhistory, 'yyyy-mm-dd hh24:mi') between '$r_tgl1' and '$r_tgl2' ";
	
	if($r_jenis!='')
		$sql .=" and p.kdjenispustaka = '$r_jenis' ";
	
	if($r_asal==1)
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke Pengolahan' ";
	elseif($r_asal==2)
		$sql .=" and h.idttb is not null and h.tkhistory = 'T' and to_char(h.keterangan)='Terima Pengolahan' ";
	elseif($r_asal=='3a')
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke Sirkulasi' ";
	elseif($r_asal=='3b')
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke Referensi' ";
	elseif($r_asal=='3c')
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke Tandon' ";
	elseif($r_asal=='3d')
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke KKI' ";
	elseif($r_asal=='3e')
		$sql .=" and h.idttb is not null and h.tkhistory = 'K' and to_char(h.keterangan)='Kirim ke Terbitan Berkala' ";
	else
		$sql .=" and h.kdlokasi='$r_lokasi' and h.idttb is null ";

	$sql .=" order by p.nopanggil ";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=1000>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>LAPORAN 
	<?php if($r_asal == 1) echo "PENGIRIMAN PUSTAKA KE PENGOLAHAN"; else if($r_asal==2) echo "PENERIMAAN PUSTAKA di PENGOLAHAN"; 
		else if($r_asal=='3a') echo "PENGIRIMAN PUSTAKA KE SIRKULASI"; else if($r_asal=='3b') echo "PENGIRIMAN PUSTAKA KE REFERENSI"; else if($r_asal=='3c') echo "PENGIRIMAN PUSTAKA KE TANDON"; 
		else if($r_asal=='3d') echo "PENGIRIMAN PUSTAKA KE KKI"; else if($r_asal=='3e') echo "PENGIRIMAN PUSTAKA KE TERBITAN BERKALA"; else echo "PENGIRIMAN PUSTAKA"; ?><br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
	</strong></td>
  </tr>
  
</table>
<table width="1000" border="1" cellpadding="2" cellspacing="0">
  
  <tr height="30">
	<th width="12" align="center"><strong>No.</strong></th>
	<th width="70" align="center"><strong>No. Induk </strong></th>
    <th width="320" align="center"><strong>Judul</strong></th>
	<th width="70" align="center"><strong>No. Panggil</strong></th>
    <th width="100" align="center"><strong>Pengarang</strong></th>
	<th width="100" align="center"><strong>Penerbit</strong></th>
	<th width="70" align="center"><strong>ISBN</strong></th>
	<!--<th width="30" align="center"><strong>Koleksi/Ruang</strong></th>
	<th width="30" align="center"><strong>Lokasi</strong></th>	
	<th width="70" align="center"><strong>Keterangan</strong></th>	
	<th width="55" align="center"><strong>Tanggal</strong></th>	
	<th width="55" align="center"><strong>Petugas</strong></th>	-->
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="left"><?= $rs['noseri'] ?></td>
    <td align="left"><?= Helper::LimitS($rs['judul']) ?></td>
	<td align="left"><?= $rs['nopanggil'] ?></td>
    <td align="left"><?= $rs['authorfirst1'].' '. $rs['authorlast1'] ?></td>
	<td align="left"><?= $rs['namapenerbit'] ?></td>
	<td align="left"><?= $rs['isbn'] ?></td>
    <!--<td align="center"><?= $rs['namaklasifikasi'] ?></td>
    <td align="center"><?= $r_asal==2 ? $rs['kdlokasi'] : $rs['namalokasi'] ?></td>
    <td align="left"><?= $rs['keterangan'] ?></td>
    <td align="center"><?= Helper::formatDateInd($rs['tglhistory']) ?></td>
    <td align="left"><?= $rs['t_user'] ?></td>-->
  </tr>
	<? $no++; }

	?>
	<? if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=10 >Tidak ada <?= $r_jenis=='T' ? "Penerimaan Pustaka" : "Pengiriman Pustaka" ?></td>
	</tr>
	<? } ?>
    <tr height=25 >
	

    <td align="left" colspan='10'><strong>Total Jumlah  : <?= $rsc; ?></strong></td>
  </tr>
</table><br><br>
<table width="1000" >
	<tr>
		<? if($r_asal == 1 or $r_asal == 2) {?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengadaan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}else if($r_asal == '3a'){?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Sirkulasi,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}else if($r_asal == '3b'){?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Referensi,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}else if($r_asal == '3c'){?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Tandon,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}else if($r_asal == '3d'){?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian KKI,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}else if($r_asal == '3e'){?>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Pengolahan,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<td >Surabaya, <?= Helper::formatDateInd(date("Y-m-d"))?><br>Bagian Terbitan Berkala,<br><br><br><br><br>
		(<?= str_repeat('&nbsp;',30)?>)
		</td>
		<?}?>
	</tr>
</table>

</div>
</body>
</html>