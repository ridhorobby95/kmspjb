<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// otentiksi user
	Helper::checkRoleAuth($conng);
	
	if(!empty($_POST)){
		$namatopik = Helper::removeSpecial($_POST['namatopik']);
		if ($namatopik!=''){
			$ada = $conn->GetOne("select count(*) from lv_topik where upper(namatopik) = upper('$namatopik') ");
			if($ada > 0){
				$errdb = 'Penyimpanan data gagal. <br/>Data Topik sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){			
				$p_max = $conn->GetOne("select max(idtopik)+1 as maxid from lv_topik");
				$record=array();
				$record['idtopik']=$p_max;
				$record['namatopik']=Helper::removeSpecial($_POST['namatopik']);
				$err=Sipus::InsertBiasa($conn,$record,lv_topik);
				
				if($err !=0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$checks=$conn->GetOne("select max(idtopik) as maxid from lv_topik");
					$id = $checks;
					$new = 1;
				}
			}
		}
		
	}
	$auth = $conn->GetRow("select namatopik from lv_topik where idtopik=$id");
	
?>

<html>
<head>
<title>Shortcut Topik Baru</title>
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">

<script>
function tambah(){
	//document.inserttopik.submit();
	//window.opener.loadtopik();
	//window.close();
	
	var namatopik=document.getElementById("namatopik").value;
	if(namatopik!=''){
		document.inserttopik.submit();
	}
	else {
		alert('Masukkan Topik dengan benar');
		document.getElementById("namatopik").focus;
	}
}

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		tambah();
}

function goSend2(key,nm) {
		window.opener.document.getElementById("idtopik").value = key;
		window.opener.document.getElementById("namatopik").value = nm;
	}
</script>
</head>
<body>
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
<form  method="post" name="inserttopik">
	<input type="hidden" id="idnya" name="idnya" value="<?= $id ?>">
	<div class="filterTable">
<? include_once('_notifikasi.php'); ?>
<table border=0 width="100%">
<tr height=25>
<td colspan="2"><b><u> Shortcut Topik Baru </u></b></td>
</tr>
<tr> 
		<td align="Left" width="150">Nama Topik</td>
		<td align="center"><?= UI::createTextBox('namatopik','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
	</tr>
<tr> 
		<td colspan="2"><input type="button" value="Simpan" onClick="tambah()" class="ControlStyle"></td>
	</tr>
</table>
	</div>
</form> 
	</div>
</div>
</body>
</html>
<?php
	if($new==1) { ?>
		<script>
		window.opener.loadauthor();
		goSend2('<?= $id ?>','<?=$auth['namatopik']?>');
		window.close();
		</script>
<? } ?>