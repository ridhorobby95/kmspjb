<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Tanda Terima Pustaka';
	$p_title = 'Tanda Terima Pustaka';
	$p_tbheader = '.: Tanda Terima Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 560;
	$p_filelist = Helper::navAddress('list_ttb.php');
	
	//no ttb
	$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
	$slice=explode("/",$getttb['ttb']);
	$isttb=$slice[0]+1;
	$r_key = Helper::removeSpecial($_GET['ttb']);
	// sql untuk mendapatkan isi list
	
	$checkqty=$conn->Execute("select idorderpustaka, qtyttb,qtypo from pp_orderpustaka where stspo=1 and ststtb=0 and qtypo<>qtyttb");
	$qtypo=array();
	while ($rsc=$checkqty->FetchRow()){
		$qtypo[$rsc['idorderpustaka']]=$rsc['qtyttb'];
	}

	$checkqty=$conn->Execute("select idorderpustaka, qtyttb,qtypo from pp_orderpustaka where stspo=1 and ststtb=0 and qtypo<>qtyttb");
	while ($rspt=$checkqty->FetchRow()){
		$qtypottb[$rspt['idorderpustaka']]=$rspt['qtypo'];
	}
		
	
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		$idanggota=Helper::removeSpecial($_POST['idanggota']);
		$keyfilter=Helper::removeSpecial($_POST['nopo']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		$idheader=Helper::removeSpecial($_POST['idttb']);
		
		if($r_aksi=='save') {
			//no ttb
			$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
			$slice=explode("/",$getttb['ttb']);
			$isttb=$slice[0]+1;
			$newno = Helper::strPad($isttb,4,0,'l')."/TTP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y');
			
			if($r_key==''){
				$recttb=array();
				$recttb['idttb']=Sipus::GetLast($conn,pp_ttb,idttb);
				$recttb['nottb']=$newno;//Helper::removeSpecial($_POST['nottb']);
				$recttb['tglttb']=Helper::formatDate($_POST['tglttb']);
				$recttb['npkttb']=$_SESSION['PERPUS_USER'];
				$recttb['jnsttb']=1; // 1 = pengadaan, 2 = sumbangan, 3 = TA
				$recttb['idunit']=$idunit;
				Helper::Identitas($recttb);			
				Sipus::InsertBiasa($conn,$recttb,pp_ttb);
			}
			if($conn->ErrorNo() != 0){
					$errdb = 'Penyimpanan Data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
				$sucdb = 'Penyimpanan Data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
#kirim notifikasi : realisasi					
				if($r_key==''){
					$r_key=$recttb['nottb'];
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&ttb='.$r_key;
					Helper::redirect($url);
				}else
					Helper::redirect();
			}
		}else if($r_aksi=='proses') {
			$recttb=array();
			$recttb['sts']=1; 
			Helper::Identitas($recttb);			
			Sipus::UpdateBiasa($conn,$recttb,pp_ttb,idttb,$idheader);	

			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan Data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan Data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
		}else if($r_aksi == 'addOrder' and $c_edit) {	
			$arrid=$_POST['idorderpustaka'];
			$arrqty=$_POST['qtyorder'];
			$arrharga=$_POST['harga'];
			$idttb=$conn->GetOne("select idttb from pp_ttb where nottb='".$r_key."'");

			$conn->StartTrans();
			if($arrid!='' and $arrqty!=''){
				#detail ttb
				$recdet['idorderpustaka']=$arrid;
				$recdet['idttb']=$idttb;
				$recdet['qtyttbdetail']=$arrqty;
				$recdet['ststtbdetail'] = 1;
				$recdet['t_user']=$_SESSION['PERPUS_USER'];
				$recdet['t_updatetime']=date('Y-m-d H:i:s');
				$recdet['t_host'] = $_SERVER['REMOTE_ADDR'];
				Sipus::InsertBiasa($conn,$recdet,pp_orderpustakattb);							
			
				#update 
				$rec['qtyttb']=Helper::removeSpecial($arrqty);
				$rec['hargadipilih']=Helper::removeSpecial($arrharga);
				$rec['ststtb']=1;
				$rec['idttb'] = $idttb;
				Sipus::UpdateBiasa($conn,$rec,pp_orderpustaka,idorderpustaka,$arrid);			
			}
			 
			$conn->CompleteTrans();
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan Data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan Data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				
				if($r_key==''){
					$r_key=$recttb['nottb'];
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&ttb='.$r_key;
					Helper::redirect($url);
				}else
					Helper::redirect();
			}
		}else if($r_aksi == 'savedetail' and $c_edit) {	
			$record = array();
			$record['qtyttbdetail'] = Helper::cStrNull($_POST['u_qty']);
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::UpdateBiasa($conn,$record,pp_orderpustakattb,idorderpustakattb,$rkey);
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Update Detail TTP Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Update Detail TTP Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
			}
		}else if($r_aksi == 'hapusdetail' and $c_delete) {
			$rqty=Helper::removeSpecial($_POST['rqty']);
			$rorder=Helper::removeSpecial($_POST['rorder']);
			$conn->CompleteTrans();
			$qty=Sipus::Search($conn,pp_orderpustakattb,idorderpustaka,$rorder,qtyttb);
			$recorder['qtyttb']=$qty-$rqty;
			$recorder['ststtb']=0;

			Sipus::UpdateBiasa($conn,$recorder,pp_orderpustaka,idorderpustaka,$rorder);
			Sipus::DeleteBiasa($conn,pp_orderpustakattb,idorderpustakattb,$rkey);
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan Data Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan Data Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
			}
		}else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $rkey;
		}else if($r_aksi =="delete" and $c_delete){
			$conn->CompleteTrans();
			$sql = $conn->GetRow("select idttb from pp_ttb where nottb='".$r_key."'");
			
			Sipus::DeleteBiasa($conn,pp_orderpustakattb,idttb,$sql['idttb']);
			Sipus::DeleteBiasa($conn,pp_ttb,nottb,$r_key);
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan Data Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan Data Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist);
				exit();
			}
		
		}
	}
	
	$p_header = "select tb.*
		from pp_ttb tb 
		where tb.nottb='$r_key'";
	$header = $conn->GetRow($p_header);
	
	// eksekusi sql list
	$p_sqlstr = "select o.*, u.*, ot.qtyttbdetail, po.nopo, ot.idttb, tb.nottb, s.namasupplier, tb.tglttb, ot.idorderpustakattb, o.hargadipilih
		from pp_orderpustaka o
		join pp_usul u on o.idusulan = u.idusulan
		left join pp_po po on o.idpo = po.idpo
		left join pp_orderpustakattb ot on o.idorderpustaka = ot.idorderpustaka
		left join pp_ttb tb on ot.idttb = tb.idttb
		left join ms_supplier s on o.supplierdipilih = s.kdsupplier
		where tb.nottb='$r_key' ";
	
	if($r_key!='')
		$rs = $conn->Execute($p_sqlstr);

	$rs_cb = $conn->Execute("select po.nopo, po.idpo
				from pp_orderpustaka o 
				left join pp_po po on o.idpo=po.idpo
				where o.stspo='1' and o.stspengadaan='1' and o.ststtb is null");
	$l_nopo = $rs_cb->GetMenu2('nopo',$keyfilter,true,false,0,'id="nopo" class="ControlStyle" style="width:150" onchange="goPo()"');
	$l_nopo = str_replace('<option></option>','<option value="x">-- Semua--</option>',$l_nopo);	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
		<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="<?= $keyfilter !='' ? "initButton($p_atfirst ? '1' : '0',$p_atlast ? '1' : '0');" : "" ?>" onmousemove="checkS(event)" >
<div id="main_content">
  <?php include('inc_menu.php'); ?>
  <div id="wrapper">
    <div class="SideItem" id="SideItem">
<div align="center">
<form name="perpusform" id="perpusform" method="post">
<table border=0 align="center">
	
	<tr>
	<td align="center">
		<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">&nbsp;Daftar TTP</span></a>
	</td>
	<? if($header['sts'] ==0 and $c_edit) { ?>
	<td align="center">
		<span style="cursor:pointer" type="button" class="buttonshort" id="savebutton" onclick="saveData()"><span class="save">Simpan</span></span>
	</td>
	<? } if($header['sts'] ==0 and $c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDeleted();" class="buttonshort"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	<? if($header['sts'] ==0 and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goProses('<?= $header['idttb']?>');" class="buttonshort"><span title="Mengubah Status Menjadi 'Selesai (S)'" class="validasi">Proses</span></a>
	</td>
	<? } ?>
	<? if($header['sts'] ==1 and $r_key != '') { ?>
		<td class="thLeft" style="border:0 none;" width=10>
			<a href="index.php?page=repp_ttb&nottb=<?= $header['nottb'] ?>" target="_blank" class="button"><span class="print">Cetak TTP</span></a>
		</td>
	<? } ?>
	</tr>
	<tr>
		<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
	</tr>
</table>
<table border="0" cellpadding="2" cellspacing=0 class="GridStyle">
	<tr>
		<td width=150 class="LeftColumnBG">Nomor TTP</td>
		<td><?=$header['nottb'];?>
			<?php if(empty($r_key)){?>
				&nbsp;&nbsp;<em>[Digenerate Oleh System]</em>&nbsp;&nbsp;
			<?php }?>	
	</tr>
	<tr>
		<td class="LeftColumnBG">Tanggal TTP</td>
		<td>
		<?php if($r_key){
			echo Helper::formatDate($header['tglttb']);
		}else{ ?>
		<input type="text" name="tglttb" id="tglttb" maxlength=10 size=10 value="<?= $r_key=='' ? date('d-m-Y') : Helper::formatDate($header['tglttb']) ?>" onChange="EditDate()">
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglettb" style="cursor:pointer;" title="Pilih tanggal TTP">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglttb",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglettb",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>
		<? } ?>
		</td>
	</tr>
</table>
<?php if (!empty($r_key)){ ?>
<br/>
<header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
        </header>
	<table width="100%" cellpadding="4" cellspacing="0" class="GridStyle">
		<tr>
			<td width="70" class="SubHeaderBGAlt" nowrap align="center">Id Order</td>
			<td width="80" class="SubHeaderBGAlt" align="center">Tgl Usulan</td>
			<td width="200" class="SubHeaderBGAlt" align="center" nowrap>Judul</td>		
			<td width="180" class="SubHeaderBGAlt" align="center">Pengusul</td>
			<td width="70" class="SubHeaderBGAlt" align="center">Qty</td>
			<td width="50" class="SubHeaderBGAlt" align="center">Harga</td>
		<? if($header['sts'] ==0 and $r_key != '') { ?>
			<td width="70" class="SubHeaderBGAlt" align="center">Aksi</td>
		<?php } ?>
		</tr>
<?php if (!empty($r_key)){ 
	$i=0;
	while ($rows = $rs->FetchRow()){
		if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
		$hrg += $rows['hargausulan'];
		$qty += $rows['qtyttbdetail'];
		
?>
		<tr class="<?= $rowstyle ?>"> 
			<td align="center"><?= $rows['idorderpustaka']; ?></td>
			<td align="center"><?= Helper::formatDate($rows['tglusulan']); ?></td>
			<td><?= $rows['judul']; ?></td>
			<td><?= $rows['namapengusul']; ?></td>
			<td align="right"><?= $rows['qtyttbdetail']; ?></td>
			<td align="right"><?= $rows['hargadipilih']; ?></td>
		<? if($header['sts'] ==0 and $r_key != '') { ?>
			<td align="center">
				<img src="images/delete.png" width="16" title="Hapus Detail" onClick="delRow('<?= $rows['idorderpustakattb']?>','<?= $rows['qtyttbdetail'] ?>','<?= $rows['idorderpustaka'] ?>')" class="link"/>
			</td>
		<?php } ?>
		</tr>
<?php 	
	}
}?>
<? if ($r_key and $header['sts']==0) {?>
		<tr id="tr_add">
			<td nowrap>
				<input type="hidden" name="idorderpustaka" id="idorderpustaka">
				<?= UI::createTextBox('idorder','','ControlRead',10,5,$c_edit,'readonly'); ?>
				<img src="images/tombol/breakdown.png" id="btnusulan" title="Cari Usulan" style="cursor:pointer" onClick="openLOV('btnusulan', 'order',-65, 20,'addUsulan',800)"></td>
			<td><?= UI::createTextBox('tgl','','ControlRead',10,10,$c_edit,'readonly'); ?></td>
			<td><?= UI::createTextBox('jdl','','ControlRead',30,30,$c_edit,'readonly'); ?></td>
			<td><?= UI::createTextBox('pengusul','','ControlRead',30,20,$c_edit,'readonly'); ?></td>
			<td>
				<input type="hidden" name="qtyorder_" id="qtyorder_">
				<?= UI::createTextBox('qtyorder','','',3,3,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
			</td>
			<td>
				<?= UI::createTextBox('harga','','ControlStyle',10,10,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
			</td>
			<td><input type="button" name="tambah" id="tambah" value="Tambah" onClick="addUsul();"></td>
		</tr>
<?php } ?>
	</table><br>
<?php } # end of empty ?>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="id" id="id">
<input type="hidden" name="idttb" id="idttb" value="<?= $rs->fields['idttb'] ?>">
<input type="hidden" name="rkey" id="rkey">
<input type="hidden" name="rqty" id="rqty">
<input type="hidden" name="rorder" id="rorder">

<input type="hidden" name="idanggota" id="idanggota">
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>
</div>
</div>
</div>


</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>

<script type="text/javascript">
$(function(){
	   $("#tglttb").mask("99-99-9999");
});
</script>
<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">

function saveData(){

	if(cfHighlight("nottb,tglttb")){
		document.getElementById("act").value="save";
		$('#savebutton').hide();
		goSubmit();
	}

}

function addUsulan(idorder,nopo, tglusulan, judul, pengusul, supplierdipilih, qty, harga) {
		$("#idorder").val(idorder);
		$("#nopo").val(nopo);
		$("#tgl").val(tglusulan);
		$("#jdl").val(judul);
		$("#pengusul").val(pengusul);
		$("#supplierdipilih").val(supplierdipilih);
		$("#qty").val(qty);
		$("#harga_aktual").val(harga);
		
		$("#idorderpustaka").val(idorder);
		$("#idusulan").val(nopo);
		$("#qtyorder").val(qty);
		$("#harga").val(harga);
}

function addUsul()
{
	if(cfHighlight("idorderpustaka,qtyorder,harga_aktual")){
		$("#act").val("addOrder");
		goSubmit();
	}
}

function goDeleted(){
	var deleted=confirm("Apakah Anda yakin akan melakukan hapus data?");
	if (deleted){
	$("#act").val("delete");
	goSubmit();
	}
}

function saveRow(key){
	$("#rkey").val(key);
	$("#act").val("savedetail");
	goSubmit()
}

function EditDate(){
	var tgl=document.getElementById("tglttb").value;
	var slice=tgl.substring(3,5);
	
	var nomer=document.getElementById("nottb").value;
	var slice2=nomer.substring(9,11);
	
	nomer=nomer.replace(slice2,'<?= Helper::bulanRomawi(slice) ?>');

}
function delDetail(row){
	$("#numdetail").val(parseInt($("#numdetail").val())-1);  //decrement
	$("#tr_detail"+row).remove(); //hapus row
}
function goRef() {
	document.getElementById("act").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}
function editRow(id){
	$("#rkey").val(id);
	$("#act").val("sunting");
	goSubmit();
}
function delRow(id,qty,order){
	var del=confirm("Apakah Anda yakin akan melakukan hapus data ?");
	if(del){
	$("#rkey").val(id);
	$("#rqty").val(qty);
	$("#rorder").val(order);
	$("#act").val("hapusdetail");
	goSubmit();
	}
}
function goProses(idorderp){
	$("#idttb").val(idorderp);
	$("#act").val("proses");
	goSubmit();
}

</script>
</html>
