<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengadaan.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$r_key = Helper::removeSpecial($_REQUEST['key']);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Data Pengadaan';
	$p_title = '.: Data Pengadaan Validasi Perpustakaan :.';
	$p_title1 = 'Data Pengadaan';
	$p_tvalidasi = 'Validasi Perpus';
	$p_tvalidasi2 = 'Validasi Keuangan';
	$p_titlelist = '.: Daftar Detail Pengadaan :.';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_pgvalperpus.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// tambahan
	$jabatan = Helper::getJabatan();
	$kodeJabatanAc = 'AAB20000B';
	$namaRole = $_SESSION['PJB']['namarole'];
	
	$sql = "select pp.*,po.idorderpustaka from pp_pengadaan pp left join pp_orderpustaka po on po.idpengadaan=pp.idpengadaan
			where pp.idpengadaan=$r_key";	
	$row = $conn->GetRow($sql);
	
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record['npkvalidasiperpus'] = $_SESSION['PERPUS_USER'];
			$record['tglvalidasiperpus'] = date('Y-m-d H:i:s');
			Helper::Identitas($record);
			
			$sql = "select * from pp_orderpustaka op
					left join pp_usul u on u.idusulan=op.idusulan
		  			where idpengadaan = $r_key order by tglusulan";
			$rs = $conn->Execute($sql);
			
			while ($rss = $rs->FetchRow()){
				$idorder = $rss['idorderpustaka'];
				$record['statusvalperpus'] = Helper::cStrNull($_POST['statusvalperpus_'.$idorder]);
				$record['memoperpus'] = Helper::cStrNull($_POST['memoperpus_'.$idorder]);
				$err = Pengadaan::insertDetailPengadaan($conn,$record,$idorder);
			}
			
			$isproses = $conn->GetOne("select 1 from pp_orderpustaka where idpengadaan= $row[idpengadaan] and statusvalperpus is null");
			if (!$isproses){
				if($err[0] == 0) {
					$sucdb = 'Penyimpanan Berhasil. Tekan Proses jika ingin melanjutkan proses ke tanda Terima Pustaka';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}else{
				if($err[0] == 0) {
					$sucdb = 'Penyimpanan Berhasil. Proses belum bisa dilanjutkan, ada data yang belum divalidasi';	
					Helper::setFlashData('sucdb', $sucdb);
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}
		else if($r_aksi == 'proses' and $c_edit) {
			$record = array();
			$record['statuspengadaan'] = 'S';
			$err = Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
			
			//update pp_orderpustaka kolom stspengadaan menjadi 1
			$sql = "select * from pp_orderpustaka op
					left join pp_usul u on u.idusulan=op.idusulan
		  			where idpengadaan = $r_key order by tglusulan";
			$rs = $conn->Execute($sql);
			
			while ($rss = $rs->FetchRow()){
				$idorder = $rss['idorderpustaka'];
				if(Helper::cStrNull($_POST['statusvalperpus_'.$idorder]) == '1'){
					$record2['stspengadaan']=1;
					$err = Sipus::UpdateComplete($conn,$record2,'pp_orderpustaka',"idpengadaan=$r_key and idorderpustaka=".$idorder);
				}else{
					$record2['stspengadaan']=2;
					$err = Sipus::UpdateComplete($conn,$record2,'pp_orderpustaka',"idpengadaan=$r_key and idorderpustaka=".$idorder);
				}
			}
			
			if($err[0] == 0) {
#kirim notifikasi : persetujuan perpustakaan

					$sucdb = 'Data Berhasil di Proses.';	
					Helper::setFlashData('sucdb', $sucdb);
					
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
					Helper::redirect($url);
			}
			else{
					$errdb = 'Data Gagal di Proses.';	
					Helper::setFlashData('errdb', $errdb);
			}
		}
	}
	
	//pengecekkan tombol proses	
  	$isproses = $conn->GetOne("select 1 from pp_orderpustaka where idpengadaan= $row[idpengadaan] and statusvalperpus is null"); 
	
	
  	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from pp_orderpustaka op
			left join pp_usul u on u.idusulan=op.idusulan
		   where idpengadaan = $row[idpengadaan] order by tglusulan";
	$rs = $conn->Execute($p_sqlstr);
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<? include_once('_notifikasi.php'); ?>
		<table width="40%" border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td align="center">
				<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar</span></a>
			</td>
			<? if($c_edit and trim($row['statuspengadaan']) == 'PP') { ?>
			<td align="center">
				<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goReset();" class="buttonshort"><span class="reset">Reset</span></a>
			</td>
			<? } if (trim($row['statuspengadaan']) == 'PP' and !$isproses){?>
			<td align="center" style="border-left:0 none;">
				<a href="javascript:goProses('<?= $row['idorderpustaka']?>');" class="buttonshort"><span class="validasi">Proses</span></a>
			</td>
			<? } if($c_delete and $r_key != '') { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br />
		<header style="width:650px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
					<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth+50 ?>" class="GridStyle">
						<tr>
							<td style="text-align:center;font-weight:bold;" align="center" class="SubHeaderBGAlt thLeft" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1; ?></td>
						</tr>
						<tr height="30">
							<td width="150" class="LeftColumnBG thLeft">No. Pengadaan</td>
							<td class="RightColumnBG" colspan="2"><?=  $row['idpengadaan']; ?></td>
						</tr>
						<tr height="30">
							<td width="150" class="LeftColumnBG thLeft">Tgl. Pengadaan</td>
							<td class="RightColumnBG" colspan="2"><?= Helper::formatDateInd($row['tglpengadaan']) ?></td>
						</tr>
						<tr>
							<td class="LeftColumnBG thLeft">Jumlah</td>
							<td class="RightColumnBG" colspan="2"><div id="totqty">0</div></td>
						</tr>
						<tr>
							<td class="LeftColumnBG thLeft">Total Pengadaan</td>
							<td class="RightColumnBG" colspan="2"><div id="totprice">0</div></td>
						</tr>
						<tr>
							<td class="LeftColumnBG thLeft">Status Pengadaan</td>
							<td class="RightColumnBG" colspan="2"><?= Helper::getArrStatusP(trim($row['statuspengadaan']))?></td>
						</tr>
						<tr height="25">
							<td class="LeftColumnBG thLeft">Keterangan</td>
							<td class="RightColumnBG" colspan="2"><?= $row['keterangan'] ?></td>
						</tr>
						<tr>
							<td colspan="2" class="footBG">&nbsp;</td>
						</tr>
					</table>
		<br>
		<br>
		<header style="width:900px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_titlelist ?></h1>
				</div>
			</div>
		</header>
		<table width="900" cellpadding="4" cellspacing="0" style="border-collapse:collapse" border="0" class="GridStyle">
			<tr>
				<td class="SubHeaderBGAlt thLeft" colspan="2" align="center" width="10%">Id Usulan</td>
				<td class="SubHeaderBGAlt thLeft" align="center" nowrap>Tgl Usulan</td>
				<td class="SubHeaderBGAlt thLeft" align="center" nowrap>Judul</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Pengarang</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Pengusul</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Harga</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Qty</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Validasi</td>
				<td class="SubHeaderBGAlt thLeft" align="center">Memo</td>
			</tr>
			<? if (!empty($r_key)){ 
					$i=0;
					while ($rows = $rs->FetchRow()){
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;				
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td colspan="2" align="center" valign="top"><?= $rows['idusulan']; ?></td>
				<td align="center" valign="top"><?= Helper::formatDate($rows['tglusulan']); ?></td>
				<td align="left" valign="top"><?= $rows['judul']; ?></td>
				<td align="left" valign="top"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
				<td valign="top"><?= $rows['namapengusul']; ?></td>
				<td align="right" valign="top"><?= $rows['hargadipilih']; ?><input type="hidden" name="hargadipilih" id="hargadipilih" value="<?= $rows['hargadipilih']?>"></td>
				<td align="right" valign="top"><?= $rows['qtypengadaan']; ?><input type="hidden" name="qtypengadaan" id="qtypengadaan" value="<?= $rows['qtypengadaan']?>"></td>
				<? if (trim($row['statuspengadaan']) == 'PP') {?>
				<td valign="top" width="80">
					<? if($jabatan['kodejabatan'] == $kodeJabatanAc || $namaRole == 'Administrator' || $namaRole == 'PJB Academy') { ?>
					<?= UI::createRadio('statusvalperpus_'.$rows['idorderpustaka'],Helper::arrStatusST(),Helper::cEmChg($rows['statusvalperpus'],''),$c_edit); ?> 
					<? } ?>
				</td>
				<td valign="top"><?= UI::createTextArea('memoperpus_'.$rows['idorderpustaka'],$rows['memoperpus'],'ControlStyle',5,40,$c_edit); ?></td>
				<? }else{ ?>
				<td align="left" valign="top"><?= $rows['statusvalperpus'] == '' ? '<strong>Belum</strong>' : ($rows['statusvalperpus'] == 1 ? '<strong>Disetujui</strong>' : '<strong>Ditolak</strong>') ; ?></td>
				<td valign="top"><?= $rows['memoperpus'] != '' ? $rows['memoperpus'] : ''; ?></td>
				<? } ?>
			</tr>
			<? }} ?>
						<tr>
							<td colspan="10" class="footBG">&nbsp;</td>
						</tr>
		</table>
		<input type="hidden" name="key" id="key" value="<?= $r_key; ?>">
		<input type="hidden" name="rkey" id="rkey">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="idorderp" id="idorderp">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

$(function(){
	hitTotal();
});

function saveData(){
		$("#act").val("simpan");
		goSubmit();
}

function goProses(idorderp){
	$("#idorderp").val(idorderp);
	$("#act").val("proses");
	goSubmit();
}

function hitTotal(){
	var total = 0;
	var totprice = 0;
	$("input[name^=qtypengadaan]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		total += qty;
	});
	
	$("input[name^=hargadipilih]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		var hrg = parseInt($("input[name^=hargadipilih]").eq(i).val())*qty;
		totprice += hrg;	
	});
	
	total += <?= $qty != '' ? $qty : 0; ?>;
	totprice += <?= $hrg != '' ? ($hrg*$qty) : 0; ?>;
	$("#totqty").html(total);
	$("#totprice").html(totprice);
}

function goReset(){
	$("textarea").val("");
	$("input[type='radio']").attr("checked",false); 
}
</script>
</html>