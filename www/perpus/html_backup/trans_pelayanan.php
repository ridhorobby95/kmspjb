<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanan';
	$p_window = '[PJB LIBRARY] Data Pelayanan Umum';
	$p_title = 'Data Pelayanan Umum';
	$p_tbwidth = 450;
	$p_filelist = Helper::navAddress('list_pelayanan.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record = Helper::cStrFill($_POST);//,array tabel
			
			if($r_key == '') { // insert record	
				$idpel=Sipus::GetLast($conn,pp_pelayanan,idpelayanan);
				$record['idpelayanan']=$idpel;
				$record['tglpelayanan']=Helper::formatDate($_POST['tglpelayanan']);
				$record['petugas']=Helper::cStrNull($_SESSION['PERPUS_USER']);
				Helper::Identitas($record);
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
								
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
					Helper::redirect($url);
					}
				
			}
			else { // update record
				$record['tglpelayanan']=Helper::formatDate($_POST['tglpelayanan']);
				if ($record['petugas']=='')
					$record['petugas']=Helper::cStrNull($_SESSION['PERPUS_USER']);
				Helper::Identitas($record);
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idpelayanan",$r_key);
				
				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
					Helper::redirect($url);
					
					}
			}	
				
			
			if($err == 0) {
				if($r_key == '')
					$r_key = $record['idpelayanan'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"idpelayanan",$r_key);
			
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	if(!$p_errdb) {
	if($r_key !=''){
		$p_sqlstr = "select * from $p_dbtable 
					where idpelayanan = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		}
	}
	
	if($c_edit){
		$rs_cb = $conn->Execute("select kdpelayanan ||' - '|| namapelayanan, kdpelayanan from lv_jenispelayanan where kdpelayanan <> 'S' and kdpelayanan <> 'B'order by kdpelayanan");
		$l_kdpel = $rs_cb->GetMenu2('kdpelayanan',$row['kdpelayanan'],true,false,0,'id="kdpelayanan" class="ControlStyle" style="width:150"');
		}
	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div id="main_content">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem" align="center">
    
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Pelayanan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<? if($r_key==''){ ?>
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
		<? } else {?>
		<a onClick="popup('index.php?page=cetak_notap&id=<?= $r_key; ?>',450,400);" class="button" style="cursor:pointer"><span class="print">Cetak Nota</span></a>
		<? } ?>
	</td>
	<? } if($c_delete and $r_key!='') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><? include('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table><br>
<header style="width:<?= $p_tbwidth ?>px">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
            
          </div>
        </header>

<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">
	<tr> 
		<td class="LeftColumnBG" width="150">Id Anggota</td>
		<td class="RightColumnBG"><?= UI::createTextBox('idanggota',$row['idanggota'],'ControlStyle',20,20,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Kode Layanan</td>
		<td class="RightColumnBG"><?= $l_kdpel; ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tanggal</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglpelayanan',$r_key!='' ? Helper::formatDate($row['tglpelayanan']) : date('d-m-Y'),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglpela" style="cursor:pointer;" title="Pilih pelayanan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglpelayanan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglpela",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>
		[ Format :dd-mm-yyyy ]
		</td>
	</tr>
		<tr> 
		<td class="LeftColumnBG">Pembayaran</td>
		<td class="RightColumnBG"><?= UI::createTextBox('bayarpelayanan',$row['bayarpelayanan'],'ControlStyle',20,20,$c_edit); ?></td>
	</tr>
	<!--
	<tr> 
		<td class="LeftColumnBG">Denda</td>
		<td class="RightColumnBG"><?= UI::createTextBox('denda',$row['denda'],'ControlStyle',20,20,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	-->
	<tr> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('keperluan',$row['keperluan'],'ControlStyle',100,45,$c_edit); ?></td>
	</tr>

</table>



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tglpelayanan").mask("99-99-9999");
});
</script>
<script language="javascript">

function saveData() {
	if(cfHighlight("idanggota,kdpelayanan"))
		goSave();
}

</script>
</html>