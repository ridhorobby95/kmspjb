<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
		
	// variabel request
	$r_idpengadaan = Helper::removeSpecial($_GET['idpengadaan']);
	
	if($r_idpengadaan=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Daftar Pengadaan';
	
	//sql untuk pengadaan
	$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM (select pp.*,po.idorderpustaka from pp_pengadaan pp left join pp_orderpustaka po on po.idpengadaan=pp.idpengadaan where pp.idpengadaan=$r_idpengadaan )  inner_query WHERE rownum <= 1)";
	$rs = $conn->GetRow($sql);
	
	//sql untuk detail pengadaan
	$sql_detail = "select * from pp_orderpustaka op left join pp_usul u on u.idusulan=op.idusulan where idpengadaan = $r_idpengadaan order by tglusulan ";
	$rs_detail = $conn->Execute($sql_detail);
	$qty_pengadaan = 0; $tot_pengadaan=0;
	while($row_detail = $rs_detail->FetchRow()){
		$qty_pengadaan = $qty_pengadaan + $row_detail['qtypengadaan'];
		$tot_pengadaan = $tot_pengadaan + ($row_detail['qtypengadaan'] * $row_detail['hargadipilih']);
	}
	$rs_detail2 = $conn->Execute($sql_detail);
	$rsc=$rs_detail2->RowCount();
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table><br><br>
<table cellpadding="4" border=1 cellspacing="0" width="400">
	<tr>
		<th align="center" colspan="2" width="400"><strong><font size='4'>Laporan Daftar Pengadaan</font></strong></th>
	</tr>
	<tr height="30">
		<th align="left" width="150" class="LeftColumnBG">No. Pengadaan</th>
		<td class="RightColumnBG"><?=  $rs['nopengadaan']; ?></td>
	</tr>
	<tr height="30">
		<th align="left" width="150" class="LeftColumnBG">Tgl. Pengadaan</th>
		<td class="RightColumnBG" ><?= Helper::formatDate($rs['tglpengadaan'])?></td>
	</tr>
	<tr height="30">
		<th align="left" class="LeftColumnBG">Jumlah</th>
		<td class="RightColumnBG"><?= $qty_pengadaan?> Eksemplar</td>
	</tr>
	<tr height="30">
		<th align="left" class="LeftColumnBG">Total Pengadaan</th>
		<td class="RightColumnBG" >Rp. <?= Helper::formatNumber($tot_pengadaan)?>,-</td>
	</tr>
	<tr height="30">
		<th align="left" class="LeftColumnBG">Keterangan</th>
		<td class="RightColumnBG" ><?= $rs['keterangan']?></td>
	</tr>
</table><br><br>

<table width="900" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="200" align="center"><strong>Judul</strong></th>
	<th width="100" align="center"><strong>Pengarang</strong></th>
	<th width="100" align="center"><strong>Penerbit</strong></th>
	<th width="100" nowrap align="center"><strong>Tahun Terbit</strong></th>
	<th width="50" align="center"><strong>ISBN</strong></th>
	<th width="80" align="center"><strong>Harga</strong></th>
	<th width="30" align="center"><strong>Qty</strong></th>
  </tr>
  <?php
	$no=1;$loop=0;
	while($row_detail2=$rs_detail2->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td ><?= $row_detail2['judul'] ?></td>
	<td ><?= $row_detail2['authorfirst1']." ".$row_detail2['authorlast1'] ?></td>
	<td ><?= $row_detail2['penerbit'] ?></td>
	<td ><?= $row_detail2['tahunterbit'] ?></td>
	<td ><?= $row_detail2['isbn'] ?></td>
	<td align="center">Rp. <?= Helper::formatNumber($row_detail2['hargadipilih']) ?>,-</td>
	<td align="center"><?= $row_detail2['qtypengadaan'] ?></td>
  </tr>
	<? $no++; $loop++;} ?>
	<? if($loop==0) { ?>
	<tr height=25>
		<td align="center" colspan=10>Tidak ada pengusulan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>
<br><br>
	<table width="800" border=0 >
		<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= Helper::formatDateInd(date('Y-m-d')) ?></td></tr>
		<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
		<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
	</table>
</div>
</body>
</html>