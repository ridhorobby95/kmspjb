<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Histori Transaksi';
	$r_param = Helper::removeSpecial($_REQUEST['param']);
	if($r_param = 'noanggota')
		$p_filerep = 'repp_sirkulasianggota';
	else
		$p_filerep = 'repp_logpustaka';
	$p_tbwidth = 100;
	
	// combo box untuk pilih lbh dulu filter by anggota atau regcomp
	$a_filter = array('1' => 'No. anggota', '2' => 'No. Induk Koleksi');
	$l_filter = UI::createSelect('filterby',$a_filter,'','ControlStyle',true,'style="width:140"');
	
	// combo box untuk sirkulasi anggota
	$a_status = array('0' => 'Semua', '1' => 'Kembali', '2' => 'Belum Kembali');
	$l_status = UI::createSelect('statuse',$a_status,'','ControlStyle',true,'style="width:140"');
	
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by kdklasifikasi");
	$l_label = $rs_cb->GetMenu2('label','',true,false,0,'id="label" class="ControlStyle" style="width:140"');
	$l_label = str_replace('<option></option>','<option value="">Semua</option>',$l_label);
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi where 1=1 $sqlAdminUnit order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('kdlokasi','',true,false,0,'id="kdlokasi" class="ControlStyle" style="width:187"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
	//======================================================================================
	
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('txtanggota').focus()">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Histori Transaksi</h1>
				</div>
			</div>
		</header>
          <div class="table-responsive">  
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td width="150" class="LeftColumnBG thLeft">Pilih Filter</td>
				<td class="RightColumnBG">
					<label id="param_dev" ><input name="param" type="radio" id="param" value="noanggota" checked onChange="showanggota()" /> No. Anggota</label>
					<label id="param_dev" ><input name="param" type="radio" id="param" value="regcomp" onChange="showregcomp()" /> No. Induk Koleksi</label>
				</td>
			</tr>
			<tr  id="tr_idanggota">
				<td class="LeftColumnBG thLeft">Id Anggota</td>
				<td class="RightColumnBG"><input type="text" name="txtanggota" id="i_idanggota" size=20 maxlength=20>
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota',620,500);">
					
				</td>
			</tr>
			<tr id="tr_status">
				<td class="LeftColumnBG thLeft">Status Pinjam</td>
				<td class="RightColumnBG"><?= $l_status ?></td>
			</tr>
			<tr id="tr_label">
				<td class="LeftColumnBG thLeft">Label</td>
				<td class="RightColumnBG"><?= $l_label ?></td>
			</tr>
			<tr id="tr_regcomp" style="display:none">
				<td class="LeftColumnBG thLeft" width="150">No. Induk</td>
				<td class="RightColumnBG"><input type="text" name="noseri" id="noseri" size=20 maxlength=20>
				</td>
			</tr>	
			<tr id="tr_tgl"> 
				<td class="LeftColumnBG thLeft" width="120">Tanggal</td>
				<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
				<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl1",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				s/d &nbsp;
				<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>

				<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl2",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Lokasi Pustaka</td>
				<td><?= $l_lokasi ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if($('input:radio[name=param]:checked').val() == "noanggota"){
		if(cfHighlight("i_idanggota,tgl1,tgl2"))
			goSubmit();
	}else{
		if(cfHighlight("tgl1,tgl2"))
			goSubmit();
	}
}

function showregcomp(){
	$("#tr_regcomp").show();
	$("#tr_idanggota").hide();
	$("#tr_status").hide();
	$("#tr_label").hide();
	$("#tr_tgl").show();
	document.getElementById("perpusform").action = "index.php?page=repp_logpustaka";
}

function showanggota(){
	$("#tr_regcomp").hide();
	$("#tr_idanggota").show();
	$("#tr_status").show();
	$("#tr_label").show();
	$("#tr_tgl").show();
	document.getElementById("perpusform").action = "index.php?page=repp_sirkulasianggota";
}

</script>
</html>