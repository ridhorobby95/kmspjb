<?php
	$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	//$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanan';
	$p_window = '[PJB LIBRARY] History Pemakaian Komputer';
	$p_title = 'History Pemakaian Komputer';
	
	//var esensial
	$r_key=Helper::removeSpecial($_GET['key']);
	
	$p_comp=$conn->GetRow("select namakomputer from pp_komputer where idkomputer=$r_key");
	
	$p_sqlstr=$conn->Execute("select p.*,now(),to_char(now(),'HH24:MI'),case when jamselesai< to_char(now(),'HH24:MI') then 1 else 0 end as expired from pp_pelayanan p
							  join pp_komputer k on p.idkomputer=k.idkomputer
							  where
							  p.kdpelayanan='K' and p.tglpelayanan=current_date
							  and k.idkomputer=$r_key");
	
	if(!empty($_POST)){
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_key=Helper::removeSpecial($_POST['key']);
		
		if($r_aksi=='free'){
			$recfree['aktifpc']=0;
			Sipus::UpdateBiasa($conn,$recfree,pp_pelayanan,idpelayanan,$r_key);
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Pembebasan Komputer gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembebasan Komputer berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
	
	
	}
?>


<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<form name="perpusform1" id="perpusform1" method="post" enctype="multipart/form-data">
<table>
	<tr>
		<td><img src="images/pc.jpg" width="50" height="50"></td>
		<td align="left"><b><?= $p_comp['namakomputer'] ?></b>
		<br>
		<? include_once('_notifikasi.php') ?>
		</td>
	</tr>
</table>
			
<table width="270" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1pt solid #ccc;border-collapse:collapse;">
	<tr height="20">
		<td width="50" align="center" style="background-color:#a03a09;color:#fff;font-weight:bold;border:1pt solid #ccc;">No.</td>
		<td width="120" align="center" style="background-color:#a03a09;color:#fff;font-weight:bold;border:1pt solid #ccc;">Id Anggota</td>
		<td width="80" align="center" style="background-color:#a03a09;color:#fff;font-weight:bold;border:1pt solid #ccc;">Mulai</td>
		<td width="80" align="center" style="background-color:#a03a09;color:#fff;font-weight:bold;border:1pt solid #ccc;">Selesai</td>
		<td width="60" align="center" style="background-color:#a03a09;color:#fff;font-weight:bold;border:1pt solid #ccc;">Batal</td>
	</tr>
	<? $i=0;
	while($row=$p_sqlstr->FetchRow()){ 
	$i++;
	?>
	<tr>
		<td align="center"><?= $i ?></td>
		<td><?= $row['idanggota'] ?></td>
		<td align="center"><?= $row['jamawal'] ?></td>
		<td align="center"><?= $row['jamselesai'] ?> </td>
		<td align="center">
		<? if(($row['idanggota']==$_SESSION['idanggota']  and $row['aktifpc']==1 and $row['expired']==0) or ($_SESSION['ispetugas']!='' and $row['aktifpc']==1 and $row['expired']==0)){ ?>
		<u title="Bebaskan komputer" style="cursor:pointer" onclick="goFree('<?= $row['idpelayanan'] ?>')"><img src="images/delete.png"></u> </td>
		<?} else { ?>
		<img src="images/delete2.png">
		<? } ?>
	</tr>
	<? } ?>
	<? if($i==0){ ?>
	<tr>
		<td align="center" colspan="5">Belum ada pemakaian</td>
	</tr>
	<? } ?>
</table>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
</form>

</body>
<script type="text/javascript">
function goFree(id){
	var free=confirm('Apakah anda akan membatalkan pemakaian pada komputer tersebut?');
	if(free){
	document.getElementById('act').value='free';
	document.getElementById('key').value=id;
	document.perpusform1.submit();
	}
}
</script>
</html>
