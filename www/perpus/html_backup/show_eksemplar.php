<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$p_ekslist = Helper::navAddress('ms_eksemplar.php');
		
	// require tambahan
	
	
	$id = Helper::removeSpecial($_REQUEST['id']);
	$ideks = Helper::removeSpecial($_REQUEST['eks']);
	$sql = "select p.*, j.namajenispustaka, b.namabahasa, mp.namapenerbit 
			from ms_pustaka p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$id'";
	$row = $conn->GetRow($sql);	
	
	/*$sqljur = "select * from pp_bidangjur j 
				left join lv_jurusan lj on lj.kdjurusan=j.kdjurusan
				where idpustaka='$id'";
	$rsjur = $conn->Execute($sqljur);*/
	
	$sqltopik = "select t.* from pp_topikpustaka p join lv_topik t on p.idtopik=t.idtopik
		     where p.idpustaka='$id'";
	
	$rstopik = $conn->GetRow($sqltopik);
	
	$sql = "select * from pp_author a 
			left join ms_author m on a.idauthor=m.idauthor
			where a.idpustaka='$id'";
	$rsauth = $conn->Execute($sql);
	
	//untuk eksemplar
	$sql = "select e.*,l.namalokasi,t.tgltenggat,t.idanggota,a.namaanggota,r.namarak,s.keterangan as serial from pp_eksemplar e 
			left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			left join ms_rak r on r.kdrak=e.kdrak
			left join pp_transaksi t on e.ideksemplar=t.ideksemplar and /*t.tglpengembalian is null*/ t.statustransaksi='1'
			left join ms_anggota a on t.idanggota=a.idanggota
			left join pp_serialitem s on e.idserialitem=s.idserialitem
			where e.ideksemplar=$ideks";
	$rseks = $conn->Execute($sql);
?>

<html>
<head>
<title>Detail Eksemplar</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0>
<div id="wrapper" style="width:auto;">
	<div class="SideItem" id="SideItem" style="margin:0;width:auto;">
		<header style="width:800px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/BIODATA.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>DETAIL PUSTAKA</h1>
				</div>
			</div>
		</header>
		<form name="perpusform" id="perpusform" target="_blank" method="post" enctype="multipart/form-data">
		<table width="800px" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td valign="top">
				<table class="GridStyle" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="thLeft" width=120>KODE PUSTAKA</td>
						<td>&nbsp;<?= $row['noseri']?></td>
						<td width="105" valign="middle" align="center" rowspan="7">	
							<? 
								$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
								$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';	
							?>
							<img id="imgfoto" width="130" height="150" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'none.png') ?>?<?= mt_rand(1000,9999) ?>">
						</td>
					</tr>
					<tr>
						<td class="thLeft">Judul</td>
						<td>&nbsp;<?= $row['judul']?></td>
					</tr>
					<tr>
						<td class="thLeft">Edisi</td>
						<td>&nbsp;<?= $row['edisi']?></td>
					</tr>
					<tr>
						<td class="thLeft">No Panggil</td>
						<td>&nbsp;<?= $row['nopanggil']?></td>
					</tr>
					<tr>
						<td class="thLeft">Pengarang</td>
						<td>
							<? //while ($rowauth = $rsauth->FetchRow()){
								if($row['authorfirst1']!='')
								//echo "<a href='index.php?page=show_pauthor&author=".$rowauth['namadepan'].' '.$rowauth['namabelakang']."'>".$rowauth['namadepan'].' '.$rowauth['namabelakang']."</a><br>";
								echo "<a style='padding-left:4px' href='index.php?page=show_pauthor&author=".$row['authorfirst1'].' '.$row['authorlast1']."'>".$row['authorfirst1'].' '.$row['authorlast1']."</a><br>";
								//$rowauth['namadepan'].' '.$rowauth['namabelakang']
								if($row['authorfirst2']!='')
								echo "<a style='padding-left:4px;' href='index.php?page=show_pauthor&author=".$row['authorfirst2'].' '.$row['authorlast2']."'>".$row['authorfirst2'].' '.$row['authorlast2']."</a><br>";
								
								if($row['authorfirst3']!='')
								echo "<a style='padding-left:4px;' href='index.php?page=show_pauthor&author=".$row['authorfirst3'].' '.$row['authorlast3']."'>".$row['authorfirst3'].' '.$row['authorlast3']."</a><br>";
								?>
						</td>
					</tr>
					<tr>
						<td class="thLeft">Penerbit</td>
						<td>&nbsp;<?= $row['namapenerbit']?></td>
					</tr>
					<? if($row['pembimbing']!='') { ?>
					<tr>
						<td class="thLeft">Pembimbing</td>
						<td>&nbsp;<?= $row['pembimbing']?></td>
					</tr>
					<? } ?>
					<tr>
						<td class="thLeft">Kolasi</td>
						<td>&nbsp;<?= $row['jmlhalromawi'].", ".$row['jmlhalaman'].".: ".$row['ilustrasi'].".: ".$row['index_buku'].".; ".$row['dimensipustaka']."."?></td>
					</tr>
					<tr>
						<td class="thLeft">ISBN</td>
						<td colspan=2>&nbsp;<?= $row['isbn']?></td>
					</tr>
					<tr>
						<td class="thLeft">Bahasa</td>
						<td colspan="2">&nbsp;<?= $row['namabahasa']?></td>
					</tr>
					<tr>
						<td class="thLeft">Jenis Pustaka</td>
						<td colspan="2">&nbsp;<?= $row['namajenispustaka']?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Kode DDC</td>
						<td colspan="2">&nbsp;<?= $row['kodeddc']?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Topik Pustaka</td>
						<td colspan="2">&nbsp;<?= $rstopik['namatopik'] ?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Tahun Terbit</td>
						<td colspan="2">&nbsp;<?= $row['tahunterbit']?></td>
					</tr>
					<?/*tr>
						<td class="thLeft">Jurusan</td>
						<td colspan="2">
							<? while ($rowjur = $rsjur->FetchRow()){
									echo '<span style="padding-left:4px;">'.$rowjur['namajurusan'].'</span>'.'<br>';
								}
							?>
						</td>
					</tr*/?>
					<tr>
						<td class="thLeft">File(s)</td>
						<td colspan=2>
							<? for($i=1;$i<=10;$i++){ 
							$nama='file'.$i;
							if ($row[$nama]=='')
								continue;
							?>
							<?  if($c_edit) { ?>
							<a style="padding-left:4px;" href="<?= $row[$nama] ?>"><?= $row[$nama]!='' ? "<img src='images/attach.gif' border=0> ".Helper::GetPath($row[$nama]) : '' ?></a>
							<br>
							<?} else echo "<span style='padding-left:4px;'>".Helper::GetPath($row[$nama])."</span>"."<br>"; 
								
							} ?>
						</td>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<table class="GridStyle" width="100%">
					<tr>
						<th align="center" colspan="5" style="background:#015593;color:#fff;font-weight:normal;">EKSEMPLAR</th>
					</tr>
					<tr>
						<th width="70">NO. INDUK</th>
						<th width="200">Lokasi</th>
						<th width="100">Nama Rak</th>
						<th width="100">Ket.</th>
						<th width="150">Status</th>
					</tr>
					<? 
						while ($roweks = $rseks->FetchRow()){
						if($roweks['kdklasifikasi']=='LH')
							$warna='green';
						elseif($roweks['kdklasifikasi']=='LM')
							$warna='red';
						else
							$warna='black';
					?>
					<tr height=20>
						<td><u title="Edit Eksemplar" style="cursor:pointer;color:blue" onclick="javascript:goDetEks('<?= $p_ekslist; ?>','<?= $roweks['ideksemplar']; ?>','<?= $row['idpustaka'] ?>');"><?= $roweks['noseri']?></u></td>
						<td style="color:<?= $warna ?>"><?= $roweks['namalokasi']?></td>
						<td style="color:<?= $warna ?>"><?= $roweks['namarak']?></td>
						<? if($row['idperiode']!='') { ?>
						<td style="color:<?= $warna ?>"><?= $roweks['serial']?></td>
						<? } else { ?>
						<td style="color:<?= $warna ?>"><?= $roweks['keterangan']?></td>
						<? } ?>
						<td style="color:<?= $warna ?>"><?= $roweks['statuseksemplar']=='ADA' ? '<center>ADA</center>' : ($roweks['statuseksemplar']=='PJM' ? $roweks['idanggota'].'-'.$roweks['namaanggota'] : 'PROSES') ;?></td>
					</tr>
					<? } ?>
				</table>
			</td>
		</tr>
		</table>
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		</form>
	</div>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
</html>