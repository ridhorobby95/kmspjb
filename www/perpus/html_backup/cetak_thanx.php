<?php
$conn->debug=false;
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Cetak Ucapan Terimakasih';
	$kepala = Sipus::getHeadPerpus();
	// koneksi ke sdm
	$conns = Factory::getConnSdm();

	$code=Helper::removeSpecial($_GET['code']);
	$id=Helper::removeSpecial($_GET['id']);
	$sum=Helper::removeSpecial($_GET['sum']);
	$key=Helper::removeSpecial($_GET['key']);
	
	if($id!=''){
	$row=$conn->GetRow("select a.namaanggota,a.alamat,f.namafakultas,j.namajurusan,kota from ms_anggota a
						left join lv_jurusan j on a.idunit=j.kdjurusan
						left join lv_fakultas f on j.kdfakultas=f.kdfakultas
						where a.idanggota='$id'");
	$anggota = strtoupper($row['namaanggota']);
	$fakultas = strtoupper($row['namafakultas'].' - '.$row['namajurusan']);
	}
	else{
	$row = $conn->GetRow("select p.*,a.alamat,a.kota,a.idunit from pp_sumbangan p left join ms_anggota a on p.npksumbangan=a.idanggota where idsumbangan=$key");
	$anggota = strtoupper($row['namapenyumbang']);
	$fakultas = strtoupper(Sipus::getUnitIn($conns,$row['npksumbangan']));
        }
        //$jum = $conn->GetOne("select sum(qtysumbangan) as jumlah from pp_orderpustaka o join pp_sumbangan s on o.idsumbangan=s.idsumbangan where idanggota='$id'");
	if($code=='k'){
		$h='830';
		$w='610';
		$t='-18px';
		$l='-8px';
		$file="thaxp1.jpg";
	}else if($code=='a'){
		$h='610';
		$w='850';
		$t='5px';
		$l='255px';		
		$file="thaxp2.jpg";
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.print()">
<div style="background:url(<?= "images/perpustakaan/".$file ?>) <?= $code=='a' ? 'no-repeat -152px -33px' : '' ?>;height:<?= $h ?>;width:<?= $w ?>;margin-top:<?= $t ?>;margin-left:<?= $l ?>;">
	<? if ($code=='k') { ?>
	<div style="position:relative;top:370px;left:300px;font-size:12px;"><b><?= $anggota ?></b></div>
	<div id="fak" style="position:relative;top:390px;left:300px;font-size:12px;width:305px;"><b><?= $fakultas ?></b></div>
	<div id="a"></div>
	<div id="jumlah" style="position:relative;top:450px;left:335px;font-size:12px;width:235px;"><b><?= $sum ?></b></div>
	<div style="margin-top:510.5px;margin-left:310px;font-size:12px"><b><?= Helper::formatDateInd(date('Y-m-d')) ?></b></div>
	<div style="margin-top:130px;margin-left:230px;font-size:12px">
	<span><b><?=$kepala['namalengkap'];?></b></span><br>
	<span><b>NIP. <?=$kepala['nik'];?></b></span>
	</div>
	<? } else if ($code=='a') { ?>
	<div style="position:relative;top:370px;left:230px;font-size:14px;"><b><?= strtoupper($anggota) ?></b></div>
	<div style="position:relative;top:380px;left:230px;font-size:14px;"><b><?= strtoupper($row['alamat']) ?></b></div>
	<div style="position:relative;top:410px;left:470px;font-size:14px;"><b><?= strtoupper($row['kota']) ?></b></div>
	<? } ?>
</div>
</body>
<script type="text/javascript">
<? if($code=='k') { ?>
	var tinggi = document.getElementById('fak').offsetHeight;
	if (tinggi > 16)
	{
		document.getElementById('am').innerHTML = '<div style="position:relative;top:0px;left:300px;font-size:12px;"><b>'+<?= date('Y') ?>+'</b></div>';
	}
	else
	{
		document.getElementById('a').innerHTML = '<div style="position:relative;top:410px;left:300px;font-size:12px;"><b>'+<?= date('Y') ?>+'</b></div>';
	}
<? } ?>
</script>
</html>