<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and u.kodeunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanan';
	$p_window = '[PJB LIBRARY] Daftar Pelayanan Umum';
	$p_title = 'Daftar Pelayanan Umum';
	$p_tbheader = '.: Daftar Pelayanan Umum :.';
	$p_col = 8;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('trans_pelayanan.php');
	$p_bs=Helper::navAddress('dbebas_sumbangan.php');
	$p_id = "trans_pelayanan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idpelayanan';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select p.*, u.nama as namaanggota, n.nama as namaunit, b.stsinventaris, o.judul   
		from $p_dbtable p
		join um.users u on u.nid = p.idanggota
		left join um.unit n on n.kodeunit = u.kodeunit
		left join pp_sumbangan s on to_char(s.idsumbangan) = p.jenis
		left join pp_orderpustaka o on o.idsumbangan = s.idsumbangan
		left join pp_orderpustakattb b on b.idorderpustaka = o.idorderpustaka 
		where 1=1 $sqlAdminUnit ";

	// pengaturan ex
	if (!empty($_POST))
	{
		$r_jenis = Helper::removeSpecial($_POST['kdpelayanan']);
		$r_id = Helper::removeSpecial($_POST['txtid']);
		if($r_jenis!='')
			$p_sqlstr .=" and p.kdpelayanan='$r_jenis' ";
		
		if($r_id!='')
			$p_sqlstr .=" and lower(p.idanggota) like '%".strtolower($r_id)."%'";
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		if($r_aksi=='hapus') {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idpelayanan,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb); 
				Helper::redirect();
				}
		
		}
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							//$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							$filterstr .= 'lower('.$filtercol.") like '%".strtr(strtolower(trim($filterdata)),'*','%')."%'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$list=$conn->Execute("select namapelayanan,kdpelayanan from lv_jenispelayanan order by kdpelayanan");
	$l_list = $list->GetMenu2('kdpelayanan',$r_jenis,true,false,0,'id="kdpelayanan" class="ControlStyle" style="width:200" onchange="goSubmit()"');
	$l_list = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_list);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
        <div class="filterTable">
          <table width="100%">
            <tr>
              <td><strong>Id Anggota</strong></td>
              <td>:</td>
              <td><input type="text" name="txtid" id="txtid" maxlength=20 value="<?= $r_id ?>"></td>
              <td><strong>Jenis Pelayanan</strong></td>
              <td>:</td>
              <td><?= $l_list ?></td>
              <td><input type="button" value="Filter" class="ControlStyle" onClick="goSubmit()">
                <input type="button" value="Refresh" class="ControlStyle" onClick="document.getElementById('kdpelayanan').value='semua';document.getElementById('txtid').value='';javascript:goRefresh();"></td>
            </tr>
          </table>
        </div>
        <br/>
		<header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
          </div>
        </header>
            <div class="table-responsive">
		<table width="100%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			
			<tr> 
				<th style="text-align:center;" width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.nosurat:C');">Nomor  <?= $p_xsort['p.nosurat']; ?></td>		
				<th width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.idanggota:C');">Id Anggota  <?= $p_xsort['p.idanggota']; ?></th>
				<th width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.nama:C');">Nama Anggota  <?= $p_xsort['u.nama']; ?></th>
				<th width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','n.nama:C');">Unit  <?= $p_xsort['n.nama']; ?></th>
				<th width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.kdpelayanan:C');">Jenis Pelayanan  <?= $p_xsort['p.kdpelayanan']; ?></th>		
				<th style="text-align:center;" width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.tglpelayanan:C');">Tanggal <?= $p_xsort['p.tglpelayanan']; ?></th>
				<th width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.petugas:C');">Petugas <?= $p_xsort['p.petugas']; ?></th>
				<? if($c_edit or $c_delete) { ?>
				<th style="text-align:center;" width="5%" nowrap align="center" class="SubHeaderBGAlt thLeft">Hapus </th>
				<? } ?>
                </tr>
			<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
						if ($row['idpenerbit'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td>
				<? if($c_edit){ ?>
				<? if($row['kdpelayanan']=='S') { ?>
				<a href="<?= $p_bs.'&id='.$row['idanggota'].'&idp='.$row['idpelayanan'].'&idsumb='.$row['jenis'] ?>" target="_blank" title="Detail Sumbangan" ><?= $row['nosurat']; ?></a>
				<? } else if($row['kdpelayanan']=='B'){ ?>
				<u title="Detail Pelayanan" onclick="popup('index.php?page=pop_notabebas&id=<?= $row['idanggota'] ?>&no=<?= $row['nosurat'] ?>&date=<?= $row['tglpelayanan'] ?>&something=<?= $row['keperluan'] ?>',530,450)" class="link"><?= $row['nosurat']; ?></u>			
				<? } else { ?>
				<u title="Detail Pelayanan" onclick="goDetail('<?= $p_filedetail; ?>','<?= $row['idpelayanan']; ?>');" class="link"><?= $row['nosurat']; ?></u>	
				
				<? }}else{ 
				echo $row['nosurat'];
				} ?>	
				</td>	
				<td><?= $row['idanggota']; ?></td>
				<td><?= $row['namaanggota']; ?></td>
				<td><?= $row['namaunit']; ?></td>
				<? if($row['kdpelayanan']=="F"){?>
					<td>Fotocopy</td>
				<?}else if($row['kdpelayanan']=="S"){?>
					<td>Sumbangan
					<br/><fieldset>
							<legend>Judul pustaka : </legend>
							<?=str_replace("\n","<br/>",$row['judul']);?>
						</fieldset>
					</td>
				<?}else{?>
					<td>Nota Bebas</td>
				<?}?>
				<td align="center"><?= Helper::tglEng($row['tglpelayanan']); ?></td>
				<td align="left"><?= $row['petugas']; ?></td>
				<? if(($c_edit or $c_delete) and $row['stsinventaris'] == 0) { ?>
				<td align="center"><img src="images/delete.png" title="Hapus pelayanan" onClick="goHapus('<?= $row['idpelayanan'] ?>')" style="cursor:pointer"></td>
				<? } ?>
			</tr>
			
			<?php
					}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
			</tr>
		</table>
            </div>
        <?php require_once('inc_listnav.php'); ?>
		<br/>
        <table align="center" cellspacing="3" cellpadding="2" width="350" border=0 bgcolor="#f1f2d1">
		<?
		$i=1;
		while($row=$list->FetchRow()){ ?>
		<tr>
			<td width=80><? if($i==1) echo "<u>Keterangan :</u>"; ?></td>
			<td width=270> - <?= $row['kdpelayanan']." = ".$row['namapelayanan'] ?> </td>
		</tr>
		<? $i++; } ?>
	   </table>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="npwd" id="npwd">

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>
		</form>
		</div>
		</div>
		</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">
function goHapus(id) {
	var hapus=confirm("Apakah Anda yakin akan menghapus pelayanan ini?");
	if(hapus){
	document.getElementById("act").value='hapus';
	document.getElementById("key").value=id;
	goSubmit();
	}
}
</script>
</html>
