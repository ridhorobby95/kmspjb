<?php
	ob_start();
	// $conn->debug=false;
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	$c_edit = $a_auth['canedit'];	
	
	// variabel request
	$r_format = Helper::cAlphaNum($_REQUEST['format']);
	$r_tglawal = Helper::formatDate($_REQUEST['periode1']);
	$r_tglakhir = Helper::formatDate($_REQUEST['periode2']);
	$r_jenis = $_REQUEST['jenis'];
	
	if(empty($r_jenis))
		$sqlj = '';
	else if(count($r_jenis) == 1) {
		if(is_array($r_jenis)) $r_jenis = $r_jenis[0];
		$sqlj = "and a.kdjenisanggota = '".Helper::cAlphaNum($r_jenis)."' ";
	}
	else {
		for($i=0;$i<count($r_jenis);$i++)
			$r_jenis[$i] = Helper::cAlphaNum($r_jenis[$i]);
		$i_jenis = implode("','",$r_jenis);
		$sqlj = "and a.kdjenisanggota in ('$i_jenis') ";
	}
		
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Denda Keterlambatan';
	$p_title = 'Daftar Laporan Denda Keterlambatan';
	$p_dbtable = 'pp_denda';
	$p_namafile = 'Daftar_Denda_Keterlambatan';
	
	switch($r_format) {
		case "doc" :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'_'.$r_tglawal.'-'.$r_tglakhir.'.doc"');
			break;
		case "xls" :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'_'.$r_tglawal.'-'.$r_tglakhir.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$p_sqlstr = "select a.idanggota,a.namaanggota,j.namajenisanggota,k.namaklasifikasi,d.tglbayar, d.jml_denda, d.jmlhari
		from $p_dbtable d
		left join pp_transaksi t on t.idtransaksi=d.idtransaksi 
		left join pp_eksemplar e on e.ideksemplar=t.ideksemplar
		left join ms_pustaka p on p.idpustaka=e.idpustaka 
		left join ms_anggota a on a.idanggota=t.idanggota
		left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
		left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
		where tglbayar between '$r_tglawal' and '$r_tglakhir' {$sqlj}
		order by d.tglbayar";
	$rs = $conn->Execute($p_sqlstr);
	
	// $p_sqlstr = "select * from $p_dbtable p
				// left join ms_anggota m on m.idanggota=p.idanggota 
				// left join lv_jenisanggota j on j.kdjenisanggota=m.kdjenisanggota
				// where tglskorsing between '$r_tglawal' and '$r_tglakhir' {$sqlj}";
	
	
	$p_col = 11;
	$p_tbwidth = 800;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<style>
		td { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 12px;}
	</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=800>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table><br>
<table width="<?= $p_tbwidth; ?>">
	<tr>
		<td align="center" colspan="<?= $p_col; ?>"><strong><font size="3"><?= $p_title; ?></font></strong></td>
	</tr>
	<tr>
		<td align="center" colspan="<?= $p_col; ?>"><strong>Per Tgl <?= Helper::formatDate($r_tglawal).' s/d '.Helper::formatDate($r_tglakhir);?></strong></td>
	</tr>
</table>
<br>
<table width="<?= $p_tbwidth; ?>" cellpadding="4" border="1" style="border-collapse:collapse;">
	<tr>
		<td align="center" bgcolor='#CCCCCC'>ID ANGGOTA</td>
		<td align="center" bgcolor='#CCCCCC'>NAMA</td>
		<td align="center" bgcolor='#CCCCCC'>JENIS ANGGOTA</td>
		<td align="center" bgcolor='#CCCCCC'>KLASIFIKASI</td>
		<td align="center" bgcolor='#CCCCCC'>TGL. BAYAR</td>
		<td align="center" bgcolor='#CCCCCC'>KETERLAMBATAN</td>
		<td align="center" bgcolor='#CCCCCC'>JML. DENDA</td>
	</tr>
<?	$i = 0; $jml_denda=0;
	while($row = $rs->FetchRow()) { $i++;?>
	<tr>
		<td align="center"><?= $row['idanggota']; ?></td>
		<td><?= $row['namaanggota']; ?></td>
		<td><?= $row['namajenisanggota']; ?></td>
		<td><?= $row['namaklasifikasi']; ?></td>
		<td align="center"><?= Helper::formatDate($row['tglbayar']); ?></td>
		<td align="center"><?= $row['jmlhari']; ?> Hari</td>
		<td align="right"><?= Helper::formatNumber($row['jml_denda']); ?></td>
	</tr>
<?
	$jml_denda = $jml_denda+$row['jml_denda'];
	} 
	if($i == 0) { ?>
	<tr>
		<td colspan="<?= $p_col ?>" height="30" align="center">Data tidak ditemukan</td>
	</tr>
<?	} ?>
	<tr><td colspan="6" align="right"><strong>Jumlah</strong></td><td align="right">Rp. <?= Helper::formatNumber($jml_denda)?></td></tr>
</table>
</div>
</body>
</html>

<? ob_end_flush(); ?>