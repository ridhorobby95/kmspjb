<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	

	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarlh';
	$p_window = '[PJB LIBRARY] Pengusul Pustaka Label Hijau';
	$p_title = 'Pengusul Pustaka Label Hijau';
	$p_tbheader = '.: Pengusul Pustaka Label Hijau :.';
	$p_col = 4;
	$p_tbwidth = 500;
	
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable where ideksemplar is null";
  	
	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2 = Helper::removeSpecial($_POST['key2']);
		$r_seri=Helper::removeSpecial($_POST['idhijau']);
		$p_pagu = Helper::removeSpecial($_POST['key3']);
		
		$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where noseri='$r_seri'");
		$txteks=$p_eks['ideksemplar'];
		
		if($txteks!=''){
		$p_total = $conn->GetRow("select sum(sharepagu) as total from $p_dbtable where ideksemplar=$txteks");
	
		if($r_aksi == 'insersi' and $c_add) {
			$idhij=$txteks;
			$check=$conn->GetRow("select ideksemplar,harga,kdklasifikasi from pp_eksemplar where ideksemplar=$idhij");
			$check2=$conn->GetRow("select namaanggota,kdjenisanggota from ms_anggota where idanggota='".$_POST['i_idanggota']."'");
			$checkaktif=$conn->GetRow("select*from pp_settingpagu where isaktif=true");
			$checkpagu=$conn->GetRow("select sisapagu from pp_pagulh where idanggota='".$_POST['i_idanggota']."' and periodeakad='".$checkaktif['periodepagu']."'");
			
			if ($check['kdklasifikasi']=='LH' and $check2['kdjenisanggota']=='D'){
			$record = array();
			$pagu=Helper::removeSpecial($_POST['i_sharepagu']);
			$total=$p_total['total']+$pagu;
			$harga=$check['harga'];
			
			if($harga>=$total){				
			if ($checkpagu['sisapagu']>=$pagu){
			$record['ideksemplar'] = $txteks;
			$record['idanggota'] = Helper::cStrNull($_POST['i_idanggota']);
			$record['sharepagu']=Helper::cStrNull($pagu);
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::InsertBiasa($conn,$record,pp_eksemplarlh);
			
			
			$col = $conn->Execute("select * from pp_pagulh where idanggota = '".$_POST['i_idanggota']."' and periodeakad='".$checkaktif['periodepagu']."'");
			echo $rec['sisapagu']=$checkpagu['sisapagu']-$pagu;
			$p_svsql = $conn->GetUpdateSQL($col,$rec);
			if($p_svsql !='')
				$conn->Execute($p_svsql);
				
			$conn->CompleteTrans();

			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $errdb);
				//Helper::redirect(); 
				}
			}else {
				$errdb = 'Share pagu melebihi sisa pagu.';	
				Helper::setFlashData('errdb', $errdb);
				//Helper::redirect();
			}
			} else {
				$errdb = 'Share pagu melebihi harga pustaka.';	
				Helper::setFlashData('errdb', $errdb);
				//Helper::redirect();
			}
			} 
			else{
			$errdb = 'Data Eksemplar atau Anggota tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			//Helper::redirect();
			}
			
		} 
		//}
		else if($r_aksi == 'update' and $c_edit) {
			$check=$conn->GetRow("select ideksemplar,harga,kdklasifikasi from pp_eksemplar where ideksemplar='".$_POST['idhijau']."'");
			$check2=$conn->GetRow("select namaanggota,kdjenisanggota from ms_anggota where idanggota='".$_POST['u_idanggota']."'");
			$checkaktif=$conn->GetRow("select*from pp_settingpagu where isaktif=true");
			$checkpagu=$conn->GetRow("select sisapagu from pp_pagulh where idanggota='".$_POST['u_idanggota']."' and periodeakad='".$checkaktif['periodepagu']."'");
			$ce_edit=$conn->GetRow("select sharepagu from $p_dbtable where ideksemplar = '$r_key' and idanggota='$r_key2'");
			
			if ($check['kdklasifikasi']=='LH' and $check2['kdjenisanggota']=='D'){
			$record = array();
			$pagu=Helper::removeSpecial($_POST['u_sharepagu']);
			echo $total=$p_total['total']-$ce_edit['sharepagu']+$pagu;
			echo $harga=$check['harga'];
			if($harga >$total){
			
			if ($checkpagu['sisapagu']>=$pagu){
			$record['ideksemplar'] = $txteks;
			$record['idanggota'] = Helper::cStrNull($_POST['u_idanggota']);
			$record['sharepagu']=Helper::cStrNull($pagu);
			Helper::Identitas($record);
			$conn->StartTrans();
			$col = $conn->Execute("select * from $p_dbtable where ideksemplar = '$r_key' and idanggota='$r_key2'");
			$p_svsql = $conn->GetUpdateSQL($col,$record);
			if($p_svsql !='')
				$conn->Execute($p_svsql);
			
			
			$col2 = $conn->Execute("select * from pp_pagulh where idanggota = '".$_POST['u_idanggota']."' and periodeakad='".$checkaktif['periode_aktif']."'");
			$rec['sisapagu']=$checkpagu['sisapagu']+$col2->fields['sharepagu']-$pagu;
			$p_svsql2 = $conn->GetUpdateSQL($col2,$rec);
			if($p_svsql2 !='')
				$conn->Execute($p_svsql2);
				
			$conn->CompleteTrans();

			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $errdb);
				//Helper::redirect(); 
				}
			}else {
				$errdb = 'Share pagu melebihi sisa pagu.';	
				Helper::setFlashData('errdb', $errdb);
				//Helper::redirect();
			}
			}else {
				$errdb = 'Share pagu melebihi harga pustaka.';	
				Helper::setFlashData('errdb', $errdb);
				//Helper::redirect();
			}
			
			}else{
			$errdb = 'Data Eksemplar atau Anggota tidak ditemukan';	
			Helper::setFlashData('errdb', $errdb);
			//Helper::redirect();
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$checkaktif=$conn->GetRow("select*from pp_settingpagu where isaktif=true");
			$checkpagu=$conn->GetRow("select sisapagu from pp_pagulh where idanggota='$r_key2' and periodeakad='".$checkaktif['periodepagu']."'");
			$record['sisapagu']=$checkpagu['sisapagu']+$p_pagu;
			$conn->StartTrans();
			
			$col = $conn->Execute("select * from pp_pagulh where idanggota='$r_key2' and periodeakad='".$checkaktif['periodepagu']."'");
			$sql = $conn->GetUpdateSQL($col,$record);
						
			if($sql !='')
				$conn->Execute($sql);
				
			$p_delsql = "delete from $p_dbtable where ideksemplar = '$r_key'  and idanggota='$r_key2'";
			$conn->Execute($p_delsql);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $errdb);
				//Helper::redirect(); 
				}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
		// else if($r_aksi == 'cari' and $c_edit){
			// $txteks=$txteks;
			// $rowb=$conn->GetRow("select e.ideksemplar,e.kdklasifikasi, p.judul, e.harga from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka
								// where e.ideksemplar='$txteks'");
			// $p_sqlstr.=" or ideksemplar=$txteks";					
		// }
	}else{
		$errdb = 'Data eksemplar tidak ditemukan.';	
		Helper::setFlashData('errdb', $errdb);
		Helper::redirect();
		}
	
	}
	// eksekusi juga
	if($txteks!=''){
			$rowb=$conn->GetRow("select e.ideksemplar,e.kdklasifikasi, p.judul, e.harga from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka
								where e.ideksemplar='$txteks'");
			if ($rowb)
				$p_sqlstr.=" or ideksemplar=$txteks";
			else{
				$errdb = 'Data eksemplar tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
				}
	}
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
	//echo "txt eks ".$txteks;
	
	// echo "hargaa ".$rowb['harga'];
	// echo "<br>hargaa2 ".$total;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="initPage()">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth; ?>">
	<tr height="40">
		<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
	</tr>
</table>

<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr>
		<td colspan=3>
		<table border=0 width="100%" cellspacing=0>
			<tr>
				<td width="150">Masukkan Nomor Seri </td>
				<td>:</td> 
				<td><input type="text" name="idhijau" id="idhijau" value="<?= $r_seri ?>" onKeyDown="etrCari(event);"></td>
				<td align="right"><a href="javascript:goLihat()" class="buttonshort"><span class="cari" >Lihat</span></a></td>
			</tr>
			<? if($rowb['kdklasifikasi']=='LH'){ ?>
			<tr>
				<td valign="top">Judul Pustaka</td>
				<td valign="top">:</td>
				<td  colspan=2><?= Helper::limitS($rowb['judul']) ?> </td>
			</tr>
			<tr>
				<td>Harga Pustaka</td>
				<td>:</td>
				<td><?= Helper::formatNumber($rowb['harga'],'0',true) ?> </td>
				<td>&nbsp;</td>
			</tr>
			<? } elseif($rowb['kdklasifikasi']=='LT' or $rowb['kdklasifikasi']=='LM') { 
				$errdb = 'Data eksemplar bukan label hijau.';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
				} 
			?>
			<tr>
				<td colspan=4 align="center" valign="bottom"><?php include_once('_notifikasi.php'); ?></td>
				
			</tr>
		</table>
		</td>
	</tr>	
	<tr height="20"> 
		
		<td nowrap width="200" align="center" class="SubHeaderBGAlt">Id Anggota</td>
		<td nowrap width="180" align="center" class="SubHeaderBGAlt">sharepagu</td>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
	</tr>
	<?php
		$i = 0;
		while ($row = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($row['ideksemplar']."-".$row['idanggota'],$p_editkey."-".$r_key2)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>">
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Sunting Data" onclick="goEdIP('<?= $row['ideksemplar']; ?>','<?= $row['idanggota'] ?>');" class="link"><?= $row['idanggota']; ?></u>
		<? } else { ?>
			<?= $row['idanggota']; ?>
		<? } ?>
		</td>
		<td align="right"><?= Helper::formatNumber($row['sharepagu'],'0',true); ?>&nbsp;&nbsp;</td>
		<td align="center">
		<? if($c_delete) { ?>
			<u title="Hapus Data" onclick="goDelIP('<?= $row['ideksemplar']; ?>','<?= $row['idanggota']; ?>','<?= $row['sharepagu']; ?>','<?= $row['idanggota']; ?>');" class="link">[hapus]</u>
		<? } ?>
		</td>
	</tr>
	<?php
			} else { // row diupdate
	?>
	<tr class="<?= $rowstyle ?>"> 
		
		<td align="center"><?= UI::createTextBox('u_idanggota',$row['idanggota'],'ControlStyle',30,20,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('u_sharepagu',$row['sharepagu'],'ControlStyle',8,20,true,'onKeyDown="return onlyNumber(event,this,false,true);etrUpdate(event)"'); ?></td>
		
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Update Data" onclick="updateData('<?= $row['ideksemplar']; ?>','<?= $row['idanggota']; ?>');" class="link">[update]</u>
		<? } ?>
		</td>
	</tr>
	<?php 	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data Anggota tidak ditemukan.</b></td>
	</tr>
	<?php } if($c_add) { ?>
	<tr class="LiteSubHeaderBG"> 
		
		<td align="center"><?= UI::createTextBox('i_idanggota','','ControlStyle',30,20,true,'onKeyDown="etrInsert(event);"'); ?>
		<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onclick="popup('index.php?page=pop_anggota&code=p',600,540);">
		</td>
		<td align="center"><?= UI::createTextBox('i_sharepagu','','ControlStyle',8,20,true,'onKeyDown="return onlyNumber(event,this,false,true);etrInsert(event);"'); ?></td>
		<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
	</tr>
	<?php }  ?>
</table><br>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="scroll" id="scroll">
</form>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function etrCari(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goLihat();
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	var x = document.getElementById('idhijau').value;
	if(x=='') { 
	document.getElementById('idhijau').focus();
	} else {
	<? 
	if($p_editkey != '') { ?>
	document.getElementById('u_idanggota').focus();
	<? } else { ?>
	document.getElementById('i_idanggota').focus();
	<? }  ?>
	}
}

function insertData() {
	if(cfHighlight("idhijau,i_sharepagu,i_idanggota"))
		goInsertIP();
}

function updateData(key,key2) {
	if(cfHighlight("idhijau,u_sharepagu,u_idanggota")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		document.getElementById("key").value=key;
		document.getElementById("key2").value=key2;
		document.getElementById("act").value='update';
		goSubmit();
	}
}
function goDelIP(key,key2,key3,label) {
	var hapus = confirm('Apakah anda yakin akan menghapus data "' + label + '"?');
	if(hapus) {
		document.getElementById("key").value = key;
		document.getElementById("key2").value = key2;
		document.getElementById("key3").value = key3;
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}
function goEdIP(key,key2) {
	document.getElementById("key").value = key;
	document.getElementById("key2").value = key2;
	document.getElementById("act").value = "sunting";
	goSubmit();
}
function goLihat() {
	if(cfHighlight("idhijau")) {
	//document.getElementById("act").value = "cari";
	goSubmit();
	}else
	document.getElementById("idhijau").focus();
}
</script>
</html>