<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);


	
	// definisi variabel halaman
	$p_dbtable = 'lv_topik';
	$p_window = '[PJB LIBRARY] Daftar Topik Pustaka';
	$p_title = 'Daftar Topik Pustaka';
	$p_tbheader = '.: Daftar Topik Pustaka :.';
	$p_col = 3;
	$p_tbwidth = 100;
	$p_id = "ms_topik";
	$p_row = 15;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable";
	$p_max = $conn->GetRow("select max(idtopik) as maxid from $p_dbtable");
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['idtopik'] = Helper::cStrNull($_POST['i_idtopik']);
			$record['namatopik'] = Helper::cStrNull($_POST['i_topik']);
			
			$ada = $conn->GetOne("select count(*) from lv_topik where upper(namatopik) = upper('{$record['namatopik']}') ");
			if($ada > 0){
				$errdb = 'Penyimpanan data gagal. <br/>Data Topik sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
			}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['idtopik'] = Helper::cStrNull($_POST['u_idtopik']);
			$record['namatopik'] = Helper::cStrNull($_POST['u_topik']);

			$ada = $conn->GetOne("select count(*) from lv_topik where upper(namatopik) = upper('{$record['namatopik']}') ");
			if($ada > 0){
				$errdb = 'Penyimpanan data gagal. <br/>Data Topik sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idtopik",$r_key);
				
				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
			}			
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"idtopik",$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	//filtering
	$keytopik=Helper::removeSpecial($_POST['caritopik']);
	if($keytopik!='')
		$p_sqlstr.=" where upper(namatopik) like upper('%$keytopik%')";
	
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr.=" order by idtopik";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
<?php include('inc_menu.php'); ?>
<div class="container">
	
    <div class="SideItem" id="SideItem">
		<div align="center"><? include_once('_notifikasi.php'); ?>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		
        <div class="filterTable table-responsive">
        	 <table>
            <tr>
              <td><strong>Nama Topik</strong></td>
              <td>:</td>
              <td><input type="text" name="caritopik" id="caritopik" size="25" value="<?= $keytopik ?>" onKeyDown="etrCari(event);"></td>
              <td><input type="button" value="Filter" class="ControlStyle" onClick="goSubmit()">
                <input type="button" value="Refresh" class="ControlStyle" onClick="goRefresh()"></td>
            </tr>
          </table>
        </div>
        <br/>
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="20">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			
			<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt">Kode Topik</td>
				<td nowrap align="center" class="SubHeaderBGAlt">Topik Pustaka</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idtopik'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<a title="Sunting Data" onclick="goEditIP('<?= $row['idtopik']; ?>');" class="link"><?= $row['idtopik']; ?></a>
				<? } else { ?>
					<?= $row['idtopik']; ?>
				<? } ?>
				</td>
				<td><?= $row['namatopik']; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
                    <img src="images/delete.png" title="Hapus pelayanan" onclick="goDeleteIP('<?= $row['idtopik']; ?>','<?= $row['namatopik']; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= UI::createTextBox('u_idtopik',$row['idtopik'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_topik',$row['namatopik'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center">
				<? if($c_edit) { ?>
					<img src="images/edit.png" title="Update Data" onclick="updateData('<?= $row['idtopik']; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_idtopik',($p_max['maxid']+1),'ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_topik','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="4"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
            </div>
        <?php require_once('inc_listnav.php'); ?>
        <br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_idtopik').focus();
	<? } else { ?>
	document.getElementById('i_idtopik').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_topik"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_topik")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

function goRefresh(){
document.getElementById("caritopik").value='';
goSubmit();

}

</script>
</html>