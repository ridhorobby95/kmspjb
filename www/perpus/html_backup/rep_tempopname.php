<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user

	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$p_window="[PJB LIBRARY] Laporan Pustaka Opname";
	$r_lokasi = Helper::removeSpecial($_GET['location']);
	$r_kondisi = Helper::removeSpecial($_GET['condition']);
	
	if ($r_lokasi !='' and $r_kondisi !=''){
		$strsql=$conn->Execute("SELECT e.ideksemplar,e.noseri, e.kdkondisi, k.namakondisi, e.kdlokasi, l.namalokasi, p.judul, r.namarak, r.lantai
							   FROM pp_eksemplar e
							   JOIN ms_pustaka p ON e.idpustaka = p.idpustaka
							   JOIN lv_kondisi k ON e.kdkondisi = k.kdkondisi
							   JOIN lv_lokasi l ON e.kdlokasi = l.kdlokasi
							   JOIN ms_rak r on e.kdrak=r.kdrak 
							   where e.statuseksemplar='ADA' and e.kdlokasi='$r_lokasi' and e.kdkondisi='$r_kondisi'  order by e.ideksemplar");
		$strjum=$strsql->RowCount();
		$strpjm=$conn->Execute("select ideksemplar from pp_eksemplar where statuseksemplar='PJM' and kdlokasi='$r_lokasi' and kdkondisi='$r_kondisi'");
		$jumpjm=$strpjm->RowCount();
		
		$sqllocate=$conn->GetRow("select namalokasi from lv_lokasi where kdlokasi='$r_lokasi'");
		$sqlkondisi=$conn->GetRow("select namakondisi from lv_kondisi where kdkondisi='$r_kondisi'");
	}else
		echo"<script>javascript:window.close()</script>";
	
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/report.css" type="text/css" rel="stylesheet">
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="javascript:window.print();" >
<div align="center">
<table width="675" border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td align="center" colspan=2><strong><h2>LAPORAN EKSEMPLAR<br></h2></strong>

		</td>
	</tr>
	<tr height=20>
		<td width=150>Lokasi </td><td>: <?= $sqllocate['namalokasi'] ?></td>

	</tr>
	<tr height=20>
		<td>Kondisi </td><td>: <?= $sqlkondisi['namakondisi'] ?></td>
	</tr>
</table>

<table width="675" border="1" cellpadding="2" cellspacing="0">
	<tr>
		<th width="50" align="center" rowspan=2><strong>No.</strong></th>
		<th width="80" align="center" rowspan=2><strong>NO INDUK</strong></th>
		<th width="170" align="center" rowspan=2><strong>Judul Pustaka</strong></th>
		<th width="150" align="center" colspan=2><strong>Tempat</strong></th>
	</tr>
	<tr>
		<th width="100" align="center"><strong>Nama Rak</strong></th>
		<th width="100" align="center"><strong>Lantai</strong></th>
	</tr>
	<? 
		$no=0;
		while($row=$strsql->FetchRow()){ $no++?>
	<tr height=20>
		<td align="center"><?= $no ?></td>
		<td align="center"><?= $row['noseri'] ?></td>
		<td><?= $row['judul'] ?></td>
		<td>&nbsp;<?= $row['namarak'] ?></td>
		<td align="center"><?= $row['lantai'] ?></td>
	</tr>
	
	<? } ?>
	
	<? if ($no==0){ ?>
	<tr>
		<td colspan=5 align="center">Data tidak ditemukan.</td>
	</tr>
	<? }else { ?>
	<tr height=20>
		<td colspan=5><strong>JUMLAH : <?= $strjum ?> <br>
		JUMLAH TERPINJAM : <?= $jumpjm ?><br>
		JUMLAH TOTAL : <?= $strjum+$jumpjm ?> </strong></td>
	</tr>
	<? } ?>
	
</table>
</div>
</body>

</html>