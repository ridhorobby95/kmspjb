<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Parameter Daftar Pustaka';
	$p_title = 'Laporan Daftar Pustaka';
	$p_filerep = Helper::navAddress('rep_dinamikpustaka.php');
	
	// combo kolom
	$a_kolom['idpustaka'] = 'Kode Pustaka';
	$a_kolom['judul'] = 'Judul';
	$a_kolom['nopanggil'] = 'NO Panggil';
	$a_kolom['kdjenispustaka'] = 'Jenis Pustaka';
	$a_kolom['judulseri'] = 'Judul Seri';
	$a_kolom['kdbahasa'] = 'Bahasa';
	$a_kolom['kodeddc'] = 'Kode DDC';
	$a_kolom['idtopik'] = 'Topik Pustaka';
	$a_kolom['idpenerbit'] = 'Penerbit';
	$a_kolom['isbn'] = 'ISBN';
	$a_kolom['kota'] = 'Kota Terbit';
	$a_kolom['tahunterbit'] = 'Tahun Terbit';
	$a_kolom['edisi'] = 'Edisi';
	$a_kolom['tglperolehan'] = 'Tgl. Perolehan';
	$a_kolom['jmlhalromawi'] = 'Jml. Halaman Romawi';
	$a_kolom['jmlhalaman'] = 'Jml. Halaman';
	$a_kolom['dimensipustaka'] = 'Dimensi Pustaka';
	$a_kolom['keywords'] = 'Keyword';
	$a_kolom['linkpustaka'] = 'Link Pustaka';
	$a_kolom['keterangan'] = 'Keterangan';	
	$a_kolom['sumeks'] = 'Jumlah Eksemplar';	
	
	$lk_kolom = UI::createSelect('k_kolom',$a_kolom,'','ControlStyle');
	$lu_kolom = UI::createSelect('u_kolom',$a_kolom,'','ControlStyle');
	
	// combo urut
	$a_urut = array('A' => 'Asc', 'D' => 'Desc');
	$lu_urut = UI::createSelect('u_urut',$a_urut,'','ControlStyle');
	
	// combo kriteria
	$a_kriteria['jenispustaka'] = 'Jenis Pustaka';
	$a_kriteria['topik'] = 'Topik';
	$a_kriteria['bahasa'] = 'Bahasa';
	$a_kriteria['penerbit'] = 'Nama Penerbit';	
	$a_kriteria['kodeddc'] = 'Kelas';	
	$a_kriteria['tglperolehan'] = 'Periode';
	$l_kriteria = UI::createSelect('s_kriteria',$a_kriteria,'','ControlStyle',true,'onchange="gantiKriteria()"');
	
	// combo format
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	// list jenis pustaka
	$sql = "select namajenispustaka,kdjenispustaka from lv_jenispustaka order by kdjenispustaka";
	$rsc = $conn->Execute($sql);
	
	$a_jenispustaka = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['jenispustaka'][$rowc['kdjenispustaka']] = $rowc['namajenispustaka'];
	$l_jenispustaka = UI::createSelect('s_jenispustaka',$a_kparam['jenispustaka'],'','ControlStyle',true,'multiple size="10"');
	
	// list topik
	$sql = "select idtopik,namatopik from lv_topik order by namatopik";
	$rsc = $conn->Execute($sql);
	
	$a_topik = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['topik'][$rowc['idtopik']] = $rowc['namatopik'];
	$l_topik = UI::createSelect('s_topik',$a_kparam['topik'],'','ControlStyle',true,'multiple size="8"');
	
	// list bahasa
	$sql = "select kdbahasa,namabahasa from lv_bahasa order by namabahasa";
	$rsc = $conn->Execute($sql);
	
	$a_bahasa = array();
	while($rowc = $rsc->FetchRow())
		$a_kparam['bahasa'][$rowc['kdbahasa']] = $rowc['namabahasa'];
	$l_bahasa = UI::createSelect('s_bahasa',$a_kparam['bahasa'],'','ControlStyle',true,'multiple size="10"');
	
	
	if(!empty($_POST)) {
		$r_template = Helper::removeSpecial($_POST['template']);
		
		// masih menampilkan data ketika disubmit
		if(!empty($_POST['kolom']))
			$al_kolom = $_POST['kolom'];
		if(!empty($_POST['urutan']))
			$al_urut = $_POST['urutan'];
		
		if(!empty($_POST['kriteria'])) {
			$al_kriteria = array();
			$n = count($_POST['kriteria']);
			for($i=0;$i<$n;$i++)
				$al_kriteria[] = $_POST['kriteria'][$i].'|'.$_POST['paramkriteria'][$i];
		}
		
		// ada aksi
		$r_act = $_POST['act'];
		if($r_act == 'simpantemplat') {
			$record = array();
			$record['namatemplate'] = Helper::CStrNull($_POST['namatpbaru']);
			if(!empty($_POST['kolom']))
				$record['listkolom'] = Helper::CStrNull(implode(',',$_POST['kolom']));
			if(!empty($_POST['urutan']))
				$record['listurutan'] = Helper::CStrNull(implode(',',$_POST['urutan']));
			
			if(!empty($_POST['kriteria'])) {
				$a_listkriteria = array();
				$n = count($_POST['kriteria']);
				for($i=0;$i<$n;$i++)
					$a_listkriteria[] = $_POST['kriteria'][$i].'|'.$_POST['paramkriteria'][$i];
				$record['listkriteria'] = Helper::CStrNull(implode(',',$a_listkriteria));
			}
			
			$col = $conn->Execute("select * from pp_templatereport where namatemplate ='".$record['namatemplate']."'");
			
			if($col->EOF)
				$sql = $conn->GetInsertSQL($col,$record);
			else
				$sql = $conn->GetUpdateSQL($col,$record);
			
			if($sql != '')
				$conn->Execute($sql);
			
			$r_template = $record['namatemplate'];
		}
		else if($r_act == 'hapustemplat') {
			$sql = "delete from pp_templatereport where namatemplate = '$r_template'";
			$conn->Execute($sql);
			
			$r_template = '';
		}
		else if($r_act == 'muattemplat') {
			$rowt = $conn->GetRow("select * from pp_templatereport where namatemplate = '$r_template'");
			
			if(!empty($rowt['listkolom']))
				$al_kolom = explode(',',$rowt['listkolom']);
			else
				unset($al_kolom);
			if(!empty($rowt['listurutan']))
				$al_urut = explode(',',$rowt['listurutan']);
			else
				unset($al_urut);
			if(!empty($rowt['listkriteria']))
				$al_kriteria = explode(',',$rowt['listkriteria']);
			else
				unset($al_kriteria);
		}
	}
	
	// default kolom, NIP dan nama
	if(empty($al_kolom))
		$al_kolom = array('idpustaka','judul');
	
	// combo template
	$sql = "select namatemplate from pp_templatereport order by namatemplate";
	$rsc = $conn->Execute($sql);
	$l_template = $rsc->GetMenu('template',$r_template,true,false,0,'id="template" class="ControlStyle"');
?>
<html>
<head>
<title>
<?= $p_window ?>
</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<!--<style type="text/css" media="screen">
@import "style/basic.css";
</style>-->

<link href="style/pager.css" type="text/css" rel="stylesheet">
<link href="style/button.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="scripts/foreditx.js"></script>
<script type="text/javascript" src="scripts/calendar.js"></script>
<script type="text/javascript" src="scripts/calendar-id.js"></script>
<script type="text/javascript" src="scripts/calendar-setup.js"></script>
<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="gantiKriteria()">
<?php include ('inc_menu.php'); ?>
<div class="container">
  <div class="SideItem" id="SideItem">
    <div class="leftRibbon" style="width:250px;">
      <?= $p_title ?>
    </div>
    <div align="left" style="width:100%">
      <form name="perpusform" id="perpusform" method="post" action="<?= $p_filerep; ?>" target="_blank">
        <div class="filterTable table-reaponsive">
          <table width="100%" cellspacing="4" cellpadding="4">
            <tr>
              <td colspan="2"><strong>Nama Format</strong> &nbsp;
                <input type="text" name="namatpbaru" id="namatpbaru" value="<?= $r_template ?>" class="ControlStyle" size="40" maxlength="160" onKeyDown="etrSimpanTemplat(event)">
                <input type="text" style="display:none">
                <!-- biar tidak submit bila tekan enter -->
                
                <input type="button" class="ControlStyle" value="Simpan" onClick="simpanTemplat()">
                &nbsp; &nbsp; <strong>Daftar Format Laporan</strong> &nbsp;
                <?= $l_template ?>
                <input type="button" class="ControlStyle" value="Muat" onClick="muatTemplat()">
                <input type="button" class="ControlStyle" value="Hapus" onClick="hapusTemplat()"></td>
            </tr>
          </table>
        </div>
        <br/>
          <div class="table-responsive">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top"><header class="header-res header-res-left">
                <div class="inner inner-res inner-res-left">
                  <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
                    <h1>Daftar Kolom </h1>
                  </div>
                  <div class="right" style="padding:5px;">
                    <?= $lk_kolom ?>
                    <input type="button" class="ControlStyle" value="Tambah" onClick="tambahKolom()">
                  </div>
                </div>
              </header>
              <table id="tab_kolom" width="100%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
                <tr>
                  <th align="center" class="SubHeaderBGAlt"><strong>Nama Kolom</strong></th>
                  <th align="center" class="SubHeaderBGAlt"><strong>Aksi</strong></th>
                </tr>
                <?	if(!empty($al_kolom)) {
												$n = count($al_kolom);
												for($i=0;$i<$n;$i++) { ?>
                <tr>
                  <td><?= $a_kolom[$al_kolom[$i]] ?>
                    <input type="hidden" name="kolom[]" value="<?= $al_kolom[$i] ?>"></td>
                  <td align="center"><? if($i == ($n-1)) { ?>
                    <img id="down-dis" src="images/arrow-down-dis.png">
                    <? } else { ?>
                    <img id="down-en" src="images/arrow-down.png" style="cursor:pointer" onClick="turunBaris(this)">
                    <? } ?>
                    <? if($i == 0) { ?>
                    <img id="up-dis" src="images/arrow-up-dis.png">
                    <? } else { ?>
                    <img id="up-en" src="images/arrow-up.png" style="cursor:pointer" onClick="naikBaris(this)">
                    <? } ?>
                    <img src="images/delete.png" style="cursor:pointer" onClick="deleteBaris(this)"></td>
                </tr>
                <?		}
											} ?>
                                            </table>
                      <table width="100%" class="GridStyle">                      
                <tr>
                  <td colspan="2" align="center" class="FootBG"> Pilih kolom yang akan tampilkan di tabel </td>
                </tr>
              </table></td>
            <td width="5%">&nbsp;</td>
            <td valign="top"><header class="header-res header-res-right">
                <div class="inner inner-res inner-res-right">
                  <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
                    <h1>Daftar Urutan</h1>
                  </div>
                  <div class="right" style="padding:5px;">
                    <?= $lu_kolom ?>
                    <?= $lu_urut ?>
                    <input type="button" class="ControlStyle" value="Tambah" onClick="tambahUrutan()">
                  </div>
                </div>
              </header>
              <table id="tab_urut" border="0" cellpadding="4" cellspacing="0" class="GridStyle" width="100%">
                <tr>
                  <th align="center" class="SubHeaderBGAlt"><strong>Nama Kolom</strong></th>
                  <th align="center" class="SubHeaderBGAlt"><strong>Urutan</strong></th>
                  <th align="center" class="SubHeaderBGAlt"><strong>Aksi</strong></th>
                </tr>
                <?	if(!empty($al_urut)) {
				$n = count($al_urut);
				for($i=0;$i<$n;$i++) {
					list($c_kolom,$c_urut) = explode(':',$al_urut[$i]); ?>
                <tr>
                  <td><?= $a_kolom[$c_kolom] ?>
                    <input type="hidden" name="urutan[]" value="<?= $c_kolom ?>:<?= $c_urut ?>"></td>
                  <td><?= $a_urut[$c_urut] ?></td>
                  <td align="center"><? if($i == ($n-1)) { ?>
                    <img id="down-dis" src="images/arrow-down-dis.png">
                    <? } else { ?>
                    <img id="down-en" src="images/arrow-down.png" style="cursor:pointer" onClick="turunBaris(this)">
                    <? } ?>
                    <? if($i == 0) { ?>
                    <img id="up-dis" src="images/arrow-up-dis.png">
                    <? } else { ?>
                    <img id="up-en" src="images/arrow-up.png" style="cursor:pointer" onClick="naikBaris(this)">
                    <? } ?>
                    <img src="images/delete.png" style="cursor:pointer" onClick="deleteBaris(this)"></td>
                </tr>
                <?		}
					} ?>
                
              </table>
              <table width="100%" class="GridStyle">
              <tr>
                  <td colspan="3" align="center" class="FootBG"> Pilih untuk mengurutkan berdasarkan daftar kolom </td>
                </tr>
              </table>
              </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><header style="width:100%">
                <div class="inner">
                  <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
                    <h1>Daftar Kriteria</h1>
                  </div>
                  
                </div>
              </header>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
              <td>
              <?= $l_kriteria ?>
                    <input name="button" type="button" class="ControlStyle" onClick="tambahKriteria()" value="Tambah">
                    <div id="div_kriteria"> &nbsp; </div>
              </td>
              <td valign="top">
              <table id="tab_kriteria" border="0" cellpadding="4" cellspacing="0" class="GridStyle" width="100%">
              
                <tr>
                  <th align="center" class="SubHeaderBGAlt"><strong>Nama Kriteria</strong></th>
                  <th align="center" class="SubHeaderBGAlt"><strong>Nilai Kriteria</strong></th>
                  <th align="center" class="SubHeaderBGAlt"><strong>Aksi</strong></th>
                </tr>
                <?	if(!empty($al_kriteria)) {
				$n = count($al_kriteria);
				for($i=0;$i<$n;$i++) {
					list($c_kriteria,$c_param) = explode('|',$al_kriteria[$i]);
					$ac_param = explode(':',$c_param);
					
					if(empty($a_kparam[$c_kriteria])) {
						$c_paramlabel = $ac_param[0].' - '.$ac_param[1].' '.$ac_param[2];
					}
					else {
						$ac_paramlabel = array();
						
						$nonbsp = array('&nbsp;' => '');
						$m = count($ac_param);
						for($j=0;$j<$m;$j++)
							$ac_paramlabel[] = strtr($a_kparam[$c_kriteria][$ac_param[$j]],$nonbsp);
						
						$c_paramlabel = implode(', ',$ac_paramlabel);
					} ?>
                <tr>
                  <td><?= $a_kriteria[$c_kriteria] ?></td>
                  <td><?= $c_paramlabel ?></td>
                  <td align="center"><input type="hidden" name="kriteria[]" value="<?= $c_kriteria ?>">
                    <input type="hidden" name="paramkriteria[]" value="<?= $c_param ?>">
                    <img src="images/delete.png" style="cursor:pointer" onClick="deleteBaris(this)"></td>
                </tr>
                <?		}
												} ?>
              </table>
              <table width="100%" class="GridStyle">
          <tr>
            <td colspan="3" align="center" class="FootBG"> Pilih untuk menyeleksi kriteria-kriteria tertentu. </td>
          </tr>
          </table>
            </td>
          
            </tr>
          
        </table>
        </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td colspan="2" align="right">
        <span class="EditTabLabel">Format</span> &nbsp; <?= $l_format; ?></td> &nbsp; <td><a href="javascript:showData();" class="button"><span class="list">Tampilkan</span></a></td>	
        
        </td>
        </tr>
        </table>
      </div>
        <input type="hidden" name="act" id="act">
      </form>
      <div id="divk_jenispustaka" style="display:none">
        <?= $l_jenispustaka ?>
      </div>
      <div id="divk_bahasa" style="display:none">
        <?= $l_bahasa ?>
      </div>
      <div id="divk_topik" style="display:none">
        <?= $l_topik ?>
      </div>
      <div id="divk_author" style="display:none">
        <input type="text" name="t_nama" id="t_nama" size="50">
      </div>
      <div id="divk_penerbit" style="display:none">
        <input type="text" name="t_nama" id="t_nama" size="50">
      </div>
      <div id="divk_kodeddc" style="display:none">
        <input type="text" name="t_nama" id="t_nama" size="10" onKeyDown="return onlyNumber(event,this,false,true)">
        -
        <input type="text" name="t_nama2" id="t_nama2" size="10" onKeyDown="return onlyNumber(event,this,false,true)">
      </div>
      <div id="divk_tglperolehan" style="display:none">
        <input type="text" name="t_nama" id="t_nama" size=10 maxlength=10>
        <img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal"> &nbsp; 
        s/d &nbsp;
        <input type="text" name="t_nama2" id="t_nama2" size=10 maxlength=10>
        <img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal"> &nbsp; 
      </div>
    </div>
  </div>
</div>
</body>
<script type="text/javascript">

var nkbaris = <?= count($al_kolom) ?>;
var nubaris = <?= count($al_urut) ?>;
var kname, ktype;

function etrSimpanTemplat(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		simpanTemplat();
}

function simpanTemplat() {
	if(cfHighlight("namatpbaru")) {
		$("#perpusform").attr("action","<?= $i_phpfile ?>");
		$("#perpusform").removeAttr("target");
		$("#act").val("simpantemplat");
		$("#perpusform").submit();
	}
}

function muatTemplat() {
	$("#perpusform").attr("action","<?= $i_phpfile ?>");
	$("#perpusform").removeAttr("target");
	$("#act").val("muattemplat");
	$("#perpusform").submit();
}

function hapusTemplat() {
	if($("#template").val() != "") {
		var hapus = confirm('Anda yakin akan menghapus templat "'+$("#template option:selected").text()+'"?');
		if(hapus) {
			$("#perpusform").attr("action","<?= $i_phpfile ?>");
			$("#perpusform").removeAttr("target");
			$("#act").val("hapustemplat");
			$("#perpusform").submit();
		}
	}
}

function tambahKolom() {
	var baris, up, down;
	
	if($("#tab_kolom").find("input[value='"+$("#k_kolom").val()+"']").size() > 0)
		return false;
	
	if(nkbaris == 0)
		up = '<img id="up-dis" src="images/arrow-up-dis.png"> ';
	else
		up = '<img id="up-en" src="images/arrow-up.png" style="cursor:pointer" onclick="naikBaris(this)"> ';
	
	nkbaris++;
	baris = '<tr>' + "\n" +
				'<td>' + "\n" +
					$("#k_kolom option:selected").text() + "\n" +
					'<input type="hidden" name="kolom[]" value="'+$("#k_kolom").val()+'">' + "\n" +
				'</td>' + "\n" +
				'<td align="center">' + "\n" +
					'<img id="down-dis" src="images/arrow-down-dis.png"> ' + up + "\n" +
					'<img src="images/delete.png" style="cursor:pointer" onclick="deleteBaris(this)">' + "\n" +
				'</td>' + "\n" +
			'</tr>';
	
	down = $("#tab_kolom").find("tr:last").find("#down-dis");
	if(down.size() > 0)
		switchImg(down,true);
	
	$("#tab_kolom").append(baris);
}

function tambahUrutan() {
	var baris, up, down;
	
	if($("#tab_urut").find("input[value^='"+$("#u_kolom").val()+":']").size() > 0)
		return false;
	
	if(nubaris == 0)
		up = '<img id="up-dis" src="images/arrow-up-dis.png"> ';
	else
		up = '<img id="up-en" src="images/arrow-up.png" style="cursor:pointer" onclick="naikBaris(this)"> ';
	
	nubaris++;
	baris = '<tr>' + "\n" +
				'<td>' + "\n" +
					$("#u_kolom option:selected").text() + "\n" +
					'<input type="hidden" name="urutan[]" value="'+$("#u_kolom").val()+':'+$("#u_urut").val()+'">' + "\n" +
				'</td>' + "\n" +
				'<td>'+$("#u_urut option:selected").text()+'</td>' + "\n" +
				'<td align="center">' + "\n" +
					'<img id="down-dis" src="images/arrow-down-dis.png"> ' + up + "\n" +
					'<img src="images/delete.png" style="cursor:pointer" onclick="deleteBaris(this)">' + "\n" +
				'</td>' + "\n" +
			'</tr>';
	
	down = $("#tab_urut").find("tr:last").find("#down-dis");
	if(down.size() > 0)
		switchImg(down,true);
	
	$("#tab_urut").append(baris);
}

function deleteBaris(img) {
	var imgef;
	var tr = $(img).parent().parent();
	var table = tr.parent().parent();
	
	var ddis = tr.find("#down-dis");
	if(ddis.size() > 0) {
		imgef = tr.prev().find("#down-en");
		if(imgef.size() > 0)
			switchImg(imgef,false);
	}
	
	var udis = tr.find("#up-dis");
	if(udis.size() > 0) {
		imgef = tr.next().find("#up-en");
		if(imgef.size() > 0)
			switchImg(imgef,false);
	}
	
	if(table.attr("id") == "tab_kolom")
		nkbaris--;
	else if(table.attr("id") == "tab_kolom")
		nubaris--;
	
	$(img).parent().parent().replaceWith("");
}

function turunBaris(img) {
	var trr = $(img).parent().parent();
	var tre = trr.next();
	
	var tdr = $(img).parent();
	var tde = tre.children("td:last");
	
	tdrt = tdr.clone();
	tdet = tde.clone();
	
	tdr.replaceWith(tdet);
	tde.replaceWith(tdrt);
	
	trr.before(tre);
}

function naikBaris(img) {
	var trr = $(img).parent().parent();
	var tre = trr.prev();
	
	var tdr = $(img).parent();
	var tde = tre.children("td:last");
	
	tdrt = tdr.clone();
	tdet = tde.clone();
	
	tdr.replaceWith(tdet);
	tde.replaceWith(tdrt);
	
	trr.after(tre);
}

function switchImg(img,on) {
	if(img.attr("id").substr(0,2) == "do") {
		if(on) {
			img.attr("id","down-en");
			img.attr("src","images/arrow-down.png");
			img.css("cursor","pointer");
			img.attr("onclick","turunBaris(this)");
		}
		else {
			img.attr("id","down-dis");
			img.attr("src","images/arrow-down-dis.png");
			img.removeAttr("style");
			img.removeAttr("onclick");
		}
	}
	else if(img.attr("id").substr(0,2) == "up") {
		if(on) {
			img.attr("id","up-en");
			img.attr("src","images/arrow-up.png");
			img.css("cursor","pointer");
			img.attr("onclick","naikBaris(this)");
		}
		else {
			img.attr("id","up-dis");
			img.attr("src","images/arrow-up-dis.png");
			img.removeAttr("style");
			img.removeAttr("onclick");
		}
	}
}

function gantiKriteria() {
	kname = $("#s_kriteria").val();
	console.log("#divk_"+kname);
	var clone = $("#divk_"+kname).clone();
	clone.removeAttr("style");
	
	if(clone.find("select").size() > 0)
		ktype = "select";
	else
		ktype = "text";
	
	$("#div_kriteria").html(clone);
		if (kname == 'tglperolehan'){
			Calendar.setup({
				inputField     :    "t_nama",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tgle1",
				align          :    "Br",
				singleClick    :    true
			});			
			
			Calendar.setup({
				inputField     :    "t_nama2",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tgle2",
				align          :    "Br",
				singleClick    :    true
			});	
		}
}

function tambahKriteria() {
	var namakr, nilaikr, valkr, paramkr, baris;
	var arval = new Array();
	var arlabel = new Array();
	
	if($("#tab_kriteria").find("input[value='"+kname+"']").size() > 0)
		return false;
	
	if(ktype == 'select') {
		if($("#div_kriteria option:selected").size() == 0) {
			alert("Mohon pilih nilai kriteria yang diinginkan.");
			return false;
		}
		
		namakr = $("#s_kriteria option:selected").text();
		valkr = kname;
		
		$("#div_kriteria option:selected").each(function(i) {
			arval[i] = $(this).val();
			arlabel[i] = $(this).text().replace(/\u00a0/g,"");
		});
		
		nilaikr = arlabel.join(', ');
		paramkr = arval.join(':');
	}
	else {
		if(!cfHighlight("t_nama"))
			return false;
		
		namakr = $("#s_kriteria option:selected").text();
		nilaikr = $("#div_kriteria #t_nama").val();
		if($("#div_kriteria #t_nama").val() != '') 
			nilaikr = nilaikr;
		
		if ($("#div_kriteria #t_nama2").val() != null)
			nilaikr = nilaikr + ' - ' + $("#div_kriteria #t_nama2").val();
				
		valkr = kname;
		paramkr = $("#div_kriteria #t_nama").val();
		if($("#div_kriteria #t_nama2").val() != null) 
			paramkr = paramkr + ':' + $("#div_kriteria #t_nama2").val();
	}
	
	baris = '<tr>' + "\n" +
				'<td>'+namakr+'</td>' + "\n" +
				'<td>'+nilaikr+'</td>' + "\n" +
				'<td align="center">' + "\n" +
					'<input type="hidden" name="kriteria[]" value="'+valkr+'">' + "\n" +
					'<input type="hidden" name="paramkriteria[]" value="'+paramkr+'">' + "\n" +
					'<img src="images/delete.png" style="cursor:pointer" onclick="deleteBaris(this)">' + "\n" +
				'</td>' + "\n" +
			'</tr>';
	
	$("#tab_kriteria").append(baris);
}

function showData() {
	$("#perpusform").submit();
}

</script>
</html>
