<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$persen = Helper::removeSpecial(Helper::formatDate($_POST['persen']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	if($persen=='')
		$persen=100;
	
	//update nominal
	if(!empty($_POST)){
		$r_aksi = Helper::removeSpecial($_POST['act']);
			
		if($r_aksi=='edit'){
			$r_key = Helper::removeSpecial($_POST['key']);
			
			$record['rpregistrasi'] = Helper::removeSpecial($_POST['rp']);
			
			$err=Sipus::UpdateBiasa($conn,$record,pp_histregistrasi,idhistregistrasi,$r_key);
			
			
		}
	}
	
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Keuangan Anggota';
	
	$p_namafile = 'lap_keuanggota'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select a.idanggota,a.namaanggota,a.alamat,a.asal,h.rpregistrasi,h.idhistregistrasi from pp_histregistrasi h
			join ms_anggota a on h.idanggota=a.idanggota
			join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
			where j.biayakeanggotaan is not null and h.tgldaftar between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') 
			order by h.tgldaftar";
			
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" >

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Keuangan Registrasi Anggota Luar</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
	<tr>
	<td> Persentase</td>
	<td>: <?= $persen.' %' ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="90" align="center"><strong>Id Anggota</strong></th>
    <th width="150" align="center"><strong>Nama Anggota</strong></th>
    <th width="200" align="center"><strong>Alamat</strong></th>
	<th width="100" align="center"><strong>Asal</strong></th>
	<th width="110" align="center"><strong>Jumlah</strong></th>
  </tr>
  <?php
	$no=1;
	$rp=0;
	while($rs=$row->FetchRow()) 
	{  
	$jrp = $rs['rpregistrasi'] * $persen / 100;
	?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="center"><?= $rs['idanggota'] ?></td>
    <td align="left"><?= $rs['namaanggota'] ?></td>
	<td ><?= $rs['alamat'] ?></td>
	<td align="left"><?= $rs['asal'] ?></td>
	<? if ($c_delete){ ?>
	<td align="right" title="Edit Nominal" style="cursor:pointer" onclick="goEdit('<?= $rs['idhistregistrasi'] ?>','<?= Helper::formatNumber($rs['rpregistrasi'],'0',false,false) ?>')"><?= Helper::formatNumber($jrp,'0',true,true) ?>&nbsp;</td>
	<? } else { ?>
	<td align="right"><?= Helper::formatNumber($jrp,'0',true,true) ?>&nbsp;</td>
	<? } ?>
  </tr>
	<? $no++; $rp +=$jrp; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada histori registrasi</td>
	</tr>
	<? } ?>
   <tr height=25>
   <td colspan=4><b>Jumlah : <?= $rsc ?></b></td>
   <td><b>Total</b></td>
   <td align="right"><b><?= Helper::formatNumber($rp,'0',true,true) ?>&nbsp;</b></td>
   </tr>
</table>


</div>
<form name="perpusform" id="perpusform" method="post" action="">
<input type="hidden" name="format" id="format" value="<?= $r_format ?>">
<input type="hidden" name="tgl1" id="tgl1" value="<?= Helper::formatDate($r_tgl1) ?>">
<input type="hidden" name="tgl2" id="tgl2" value="<?= Helper::formatDate($r_tgl2) ?>">
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="rp" id="rp">
</form>
</body>
<script>
function goEdit(key,rp){
	var edit=prompt("Masukkan nilai Rupiah :",rp);
	
	if(edit!='' && edit!=null){
		document.getElementById('act').value='edit';
		document.getElementById('key').value=key;
		document.getElementById('rp').value=edit;
		document.perpusform.submit();
	}
}

</script>

</html>