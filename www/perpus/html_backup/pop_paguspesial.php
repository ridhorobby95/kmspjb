<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	$a_auth = Helper::checkRoleAuth($conng,false);
	// otentiksi user
	Helper::checkRoleAuth($conng);
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
		$c_readlist = $a_auth['canlist'];
	$p_filelist = Helper::navAddress('list_pagu.php');
	
	if(!empty($_POST)){
		$tahun=Helper::removeSpecial($_POST['periodeakad']);
		$jumlah=Helper::removeSpecial($_POST['jumlah']);
		$copi=Helper::removeSpecial($_POST['tahun']);
		
		if ($tahun!='' and $jumlah!=''){
			if($copi!='')
			$copies=$conn->Execute("select idanggota from pp_pagulh where periodeakad='$copi'");
			else
			$copies=$conn->Execute("select a.idanggota from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota where j.setpagu=1");
			
			if($copies){
				$conn->StartTrans();
				$record=array();
				
				$record['periodepagu']=$tahun;
				$record['bulanstart']=Helper::removeSpecial($_POST['bulan1']);
				$record['bulanend']=Helper::removeSpecial($_POST['bulan2']);
				$record['besarpagu']=$jumlah;
				$record['islast']=1;
				Helper::Identitas($record);
				
				Sipus::InsertBiasa($conn,$record,pp_settingpagu);
				
				while($rs=$copies->FetchRow()){
					$rec=array();
					$rec['idanggota']=$rs['idanggota'];
					$rec['periodeakad']=$tahun;
					$rec['pagulh']=$jumlah;
					$rec['sisapagu']=$jumlah;
					$rec['sisasementara']=$jumlah;
					Helper::Identitas($rec);
					
					Sipus::InsertBiasa($conn,$rec,pp_pagulh);
				
				}
				$conn->CompleteTrans();
				if($conn->ErrorNo() != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
				
			}
		}else{
		$errdb = 'Masukkan data dengan benar.';	
		Helper::setFlashData('errdb', $errdb);
		Helper::redirect();
		}
		
	}
	$rs_cb = $conn->Execute("select periodeakad-1||' - '||periodeakad as tahun,periodeakad from pp_pagulh group by periodeakad order by periodeakad desc");
	$l_akad = $rs_cb->GetMenu2('tahun',$row['periodeakad'],true,false,0,'id="tahun" class="ControlStyle" style="width:120"');
	$l_akad = str_replace('<option></option>','<option value="">Semua Daftar</option>',$l_akad);

	
?>

<html>
<head>
	<title>[PJB LIBRARY] Pagu Anggota</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_readlist) { ?>
				<td align="center">
					<a href="<?= $p_filelist; ?>" class="buttonshort"><span class="list">&nbsp;&nbsp;&nbsp;Daftar Pagu</span></a>
				</td>
				<?} if($c_edit) { ?>
				<td align="center">
					<a href="javascript:goSubmit();" class="buttonshort"><span class="save">Simpan</span></a>
				</td>
				<? } ?>
				<td align="center">
					<a href="javascript:goUndo();" class="buttonshort"><span class="reset">Reset</span></a>
				</td>
			</tr>
		</table><br>
		<div align="center"><? include_once('_notifikasi.php'); ?></div>
		<header style="width:435px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<h1>Pagu Anggota</h1>
				</div>
			</div>
		</header>
		<table width="435" cellspacing="1" cellpadding="4"  class="GridStyle" align="center">
		<!--tr>
			<td align="center" colspan="2"  class="PageTitle">Pagu Anggota</td>
		</tr-->
		<!--tr>
			<td colspan=2 align="center" width="100%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<? if($c_readlist) { ?>
						<td align="center">
							<a href="<?= $p_filelist; ?>" class="buttonshort"><span class="list">&nbsp;&nbsp;&nbsp;Daftar Pagu</span></a>
						</td>
						<?} if($c_edit) { ?>
						<td align="center">
							<a href="javascript:goSubmit();" class="buttonshort"><span class="save">Simpan</span></a>
						</td>
						<? } ?>
						<td align="center">
							<a href="javascript:goUndo();" class="buttonshort"><span class="reset">Reset</span></a>
						</td>
					</tr>
			</table><br>
			<div align="center"><? include_once('_notifikasi.php'); ?></div>
			</td>
		</tr-->
		<tr>
			<td class="LeftColumnBG thLeft" width="150">Periode Akademik</td>
			<td><?= UI::combotahun('periodeakad',120,date("Y")); ?>
			</td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft">Bulan Mulai</td>
			<td><?= UI::combobulan('bulan1',120); ?></td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft">Bulan Selesai</td>
			<td><?= UI::combobulan('bulan2',120); ?></td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft">Jumlah Pagu</td>
			<td>
			<input type="text" id="jumlah" name="jumlah" size="20"  maxlength=20 onkeydown="return onlyNumber(event,this,false,true)">
			</td>
		</tr>	
		<tr>
			<td class="LeftColumnBG thLeft">Salin Daftar Anggota</td>
			<td><?= $l_akad ?></td>
		</tr>
		<tr>
			<td class="footBG" colspan="2">&nbsp;</td>
		</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>
</html>