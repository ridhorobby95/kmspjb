<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	// require tambahan
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and u.kodeunit in ($units) ";
		$sqlAdminUnitL = " and kodeunit in ($units) ";
	}	
	// definisi variabel halaman
	$p_dbtable = 'um.users';
	$p_window = '[PJB LIBRARY] Daftar Anggota';
	$p_title = 'Daftar Anggota';
	$p_tbheader = '.: Daftar Anggota :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_anggota.php');
	$p_filedetaild = Helper::navAddress('ms_anggota.php');
	$p_filelist = Helper::navAddress('list_anggota.php');
	$p_id = "um.users";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'u.nid';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$reqresult = false;
	
	if($_POST['act'] == "hapusanggota"){
		$anggotaid = $_POST['key'];
		$err=Sipus::DeleteBiasa($conn,$p_dbtable,idanggota,$anggotaid);
		
		if($err != 0){
			$errdb = 'Penghapusan data gagal.';	
			Helper::setFlashData('errdb', $errdb);
		}
		else {
			$sucdb = 'Penghapusan data berhasil.';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		}
	}
	
	
	// sql untuk mendapatkan isi list  1		
	$p_sqlstr = "select u.*, j.nama as namajenisanggota, n.nama as namaunit  
		from um.users u 
		left join um.unit n on u.kodeunit = n.kodeunit 
		left join um.status j on j.kodestatus = u.kodestatus 
		where 1=1 $sqlAdminUnit ";
	
	// pengaturan ex
	if (!empty($_POST))
	{
		//set like dan masukkan ke dalam session untuk keperluan next dan prev paging
		$islike = Helper::removeSpecial($_POST['islike']);
		$_SESSION[$p_id.'.like'] = $islike;
		
		$keyid=Helper::removeSpecial($_POST['cariid']);
		$keynama=Helper::removeSpecial($_POST['carinama']);	
		$keyjenis=Helper::removeSpecial($_POST['kdjenisanggota']);
		$keynim = Helper::removeSpecial($_POST['noika']);
		$keynohal = Helper::removeSpecial($_POST['nohalaman']);
		$reqresult = ($_POST['print'] == 'Y');
		$statusanggota=Helper::removeSpecial($_POST['statusanggota']);
		$keyunit=Helper::removeSpecial($_POST['kdunit']);
		
		$_SESSION['listanggota']['cariid'] = $keyid;
		$_SESSION['listanggota']['carinama'] = $keynama;	
		$_SESSION['listanggota']['kdjenisanggota'] = $keyjenis;
		$_SESSION['listanggota']['noika'] = $keynim;
		$_SESSION['listanggota']['nohalaman'] = $keynohal;
		//$_SESSION['listanggota']['print'] = $reqresult;
		$_SESSION['listanggota']['statusanggota'] = $statusanggota;
		$_SESSION['listanggota']['kdunit'] = $keyunit;
		
		if($keynohal!=null or $keynohal!='')
			$p_page 	= $keynohal;
		else
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
	}
	else
	{
		$keyid = $_SESSION['listanggota']['cariid'];
		$keynama = $_SESSION['listanggota']['carinama'];	
		$keyjenis = $_SESSION['listanggota']['kdjenisanggota'];
		$keynim = $_SESSION['listanggota']['noika'];
		$keynohal = $_SESSION['listanggota']['nohalaman'];
		//$reqresult = $_SESSION['listanggota']['print'];
		$statusanggota = $_SESSION['listanggota']['statusanggota'];
		$keyunit = $_SESSION['listanggota']['kdunit'];
		
		
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
	
	if($keyid!=''){
	$idanggota_exp = explode(',',$keyid);
		$list_id = '';
		for($i=0;$i<count($idanggota_exp);$i++){
			if($i==0)
				$list_id = "'".trim($idanggota_exp[$i])."'";
			else
				$list_id = trim($list_id) . ",'".trim($idanggota_exp[$i])."'";
		}
		$p_sqlstr.=" and u.nid in ($list_id)";
	}
	
	if($statusanggota!=''){
		$p_sqlstr.=" and u.isactive='$statusanggota' ";
	}
	
	if($keyunit!=''){
		$p_sqlstr.=" and u.kodeunit='$keyunit' ";
	}
	
	if ($keynama!=''){
		$p_sqlstr.=" and upper(u.nama) like upper('%$keynama%')";
	}
	if ($keyjenis!='')
		$p_sqlstr.=" and u.kodestatus='$keyjenis' ";
	
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$sqlresult = $p_sqlstr;
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page, false, 0, $rsc);

	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	$rs_cb = $conn->Execute("select nama, kodestatus from um.status order by kodestatus");
	$l_jenis = $rs_cb->GetMenu2('kdjenisanggota',$keyjenis,true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:120" onChange="goFilterEx()"'); 
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_jenis);
	
	$a_statusanggota = array('' => '-Pilih-', '0' => 'Non Aktif', '1' => 'Aktif');
	$l_statusanggota = UI::createSelect('statusanggota',$a_statusanggota, $statusanggota,'ControlStyle',true,' onChange="goFilterEx()"');
	
	$rs_cb = $conn->Execute("select nama, kodeunit from um.unit where 1=1 $sqlAdminUnitL order by nama ");
	$l_lokasi = $rs_cb->GetMenu2('kdunit',$keyunit,true,false,0,'id="kdunit" class="ControlStyle" style="width:187" onChange="goFilterEx()"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
	
	if($reqresult) {
		header("Content-Type: application/msexcel");
		header('Content-Disposition: attachment; filename="'.'anggota_searchresult'.'.xls"');
		$rsreq = $conn->Execute(mb_convert_encoding($sqlresult,"UTF-8"));
	?>
	<html>
	<body>
        <div class="table-responsive">
		<table width="100%" border="0">
			<tr height="20">
				<td width="100" nowrap align="center" >ID Anggota</td>		
				<td width="270" nowrap align="center">Nama Anggota</td>
				<td width="100" nowrap align="center">Jenis Anggota</td>
				<td width="100" nowrap align="center">Jenis Kelamin</td>
				<td width="370" nowrap align="center">Alamat</td>
				<td width="100" nowrap align="center">Kota</td>
				<td width="100" nowrap align="center">Telp</td>
				<td width="150" nowrap align="center">Email</td>
				<td width="150" nowrap align="center">Unit</td>
			</tr>
		<? while($row = $rsreq->FetchRow()) { 
		?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle">
				<td align="left">
				<?= "'".$row['nid']; ?>
				</td>
				<td nowrap align="left"><?= $row['nama']; ?></td>
				<td ><?= $row['namajenisanggota']; ?></td>
				<td ><?= $row['jk']=='L'?'Laki-Laki':'Perempuan'; ?></td>
				<td align="left"><?= $row['alamat']; ?></td>
				<td align="left"><?= $row['kota']; ?></td>
				<td align="left"><?= $row['telp']; ?></td>
				<td align="left"><?= $row['email']; ?></td>
				<td ><?= $row['namaunit']; ?></td>
			</tr>
		<?
			}
		?>
		</table>
        </div>
	</body>
	</html>
	<? 
	}	
	?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);document.getElementById('cariid').focus()" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
		
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center"><? include_once('_notifikasi.php'); ?>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<div class="filterTable table-responsive">				
				<table cellpadding=0 cellspacing="4" border=0 width="100%" style="border:0 none;">
				<tr>
					<td style="border:0 none;" width="10%">Id Anggota</td>
					<td style="border:0 none;" width="40%">: <input type="text" id="cariid" size="20" name="cariid" value="<?= $keyid ?>" onKeyDown="etrCari(event);">&nbsp;&nbsp;<i>Pisahkan dengan tanda koma ( , )</i></td> 
					<td style="border:0 none;" width="10%">Status Anggota</td>
					<td style="border:0 none;" width="20%">: <?=$l_statusanggota;?></td>
					<td style="border:0 none;" rowspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td style="border:0 none;" nowrap>Nama Anggota</td>
					<td style="border:0 none;">: <input type="text" id="carinama" name="carinama" size="35" value="<?= $_POST['carinama'] ?>" onKeyDown="etrCari(event);">
					</td>
					<td style="border:0 none;" nowrap>Jenis Anggota</td>
					<td style="border:0 none;">: <?= $l_jenis ?> </td>
					<td style="border:0 none;" class="thLeft" rowspan=3>&nbsp;</td>
				</tr>
				<tr>
					<td style="border:0 none;" nowrap>Lokasi</td>
					<td style="border:0 none;">:&nbsp; <?=$l_lokasi;?></td>
					<td style="border:0 none;" nowrap>&nbsp;</td>
					<td style="border:0 none;">&nbsp;</td>
					<td style="border:0 none;" align="right"><input style="float:right;" type="button" value="Filter" class="ControlStyle" onclick="goFilterEx()"></td>
					<td style="border:0 none;">
						<input style="float:right;" type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
					</td>
					<td style="border:0 none;" class="thLeft">&nbsp;</td>
				</tr>
				</table>
			</div>
			<br />
			<header style="width:auto;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/keanggotaan.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
					<div class="right">
					  <div class="addButton" style="float:left; margin:right:10px;" title="download excel" onClick='goPrint()'><img src="images/tombol/excel.png"/></div>
					  <div class="addButton" style="float:left; margin:right:10px;padding:0px 2px;" title="cetak kartu" onClick='goCodePrint()'><img width=22 src="images/cetak-kartu.png"/></div>
					</div>
				</div>
			</header>
            <div class="table-responsive">
		<table width="100%" border="0" cellpadding="0" cellspacing=0 class="GridStyle">
			<tr height="20">
				<td width="5%" align="center" class="SubHeaderBGAlt thLeft">
					<u title="Pilih Semua" onclick="goAll()" style="cursor:pointer">
					<input type="checkbox" name="all" id="all" value="all" onClick="goAll()"></u>
				</td>
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.nid:C');">ID Anggota  <?= $p_xsort['u.nid']; ?></td>		
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.nama:C');">Nama Anggota <?= $p_xsort['u.nama']; ?></td>
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','n.nama:C');">Unit <?= $p_xsort['n.nama']; ?></td>
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.namajenisanggota:C');">Jenis Anggota<?= $p_xsort['j.namajenisanggota']; ?></td>
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.alamat:C');">Alamat<?= $p_xsort['u.alamat']; ?></td>
				
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
				<?php
				$i = 0;

				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			</tr>
			<tr class="<?= $rowstyle ?>" height="25" valign="top"> 
				<td align="center">
					<input type="checkbox" name="pilih[]" id="pilih" value="<?= $row['nid'] ?>">
				</td>
				<td align="left">
				<?= $row['nid']; ?>
				</td>
				<td nowrap align="left">
					<?= $row['nama']; ?>
				</td>
				<td ><?= $row['namaunit']; ?></td>
				<td ><?= $row['namajenisanggota']; ?></td>
				<td align="left"><?= $row['alamat']; ?></td>
				
				
				<td align="center">
				<u title="Detail Anggota" onclick="goDetail('<?= $p_filedetaild; ?>','<?= $row['nid']; ?>');" class="link"><img src="images/edited.gif"></u>
				<u title="Cetak kartu Anggota" onclick="popup('index.php?page=cetak_kartu&id=<?= $row['nid'] ?>',500,400)" class="link"><img src="images/printer.png"></u>
				</td>
				
				
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td colspan="11" align="right" class="FootBG" style="padding:4px;">
						<div style="float:left">
							Menuju ke halaman : <?= UI::createTextBox('nohalaman','','ControlStyle',6,6); ?> <input type="submit" name="halaman" id="halaman" onClick="goHalaman()" value="Go">
							&nbsp;&nbsp;Menampilkan <?= Helper::formatNumber($rsc)?> Data
						</div>
						<div style="float:right">
							Halaman
              <?= $p_page ?>
              /
              <?= $p_lastpage ?>
              <?= $p_status ?>						</div>
						</td>
			</tr>
		</table>
            </div>
        <?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="nohal" id="nohal">
		<input type="hidden" name="npwd" id="npwd">
		<input type="hidden" name="print" id="print">
		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">
function goClear(){
document.getElementById("cariid").value='';
document.getElementById("carinama").value='';
document.getElementById("kdjenisanggota").value='';
document.getElementById("statusanggota").value='';
document.getElementById("kdunit").value='';
}

function goAll () {
	var aa= document.getElementById('perpusform');
	//var x = aa.all.checked=true;
	 if (aa.all.checked==true)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i = 0; i < aa.pilih.length; i++) 
	{
	 	aa.pilih[i].checked = checked;
	}
}

function goCodePrint() {
	if($("input[id='pilih']:checked").val() == null) // tidak ada yang dicentang
		alert("Silakan pilih lebih dahulu anggota yang akan dicetak kartu anggotanya!");
	else {
		var a=document.getElementsByTagName("pilih[]");
		//var kd=document.getElementById("kdklasifikasi").value;
		var total="";
		for(var i=0; i < document.perpusform.pilih.length; i++){
		if(document.perpusform.pilih[i].checked)
		total +=document.perpusform.pilih[i].value+"|";
		}
		if(total=='')
			total=document.perpusform.pilih.value+"|";

		window.open('index.php?page=cetak_kartu_anggota&start='+total);
	}
}

function goHalaman() {
	document.getElementById("nohal").value = $("#nohalaman").val();
	goSubmit();
}

function goFilterEx(){
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

function goPrint() {
	document.getElementById("perpusform").target= "_blank";
	document.getElementById("print").value = "Y";
	goSubmit();
	document.getElementById("perpusform").target= "_self";
	document.getElementById("print").value = "X";
}

function goDeleteAnggota(idanggota){
	var del = confirm("Apakah anda yakin akan meghapus Id Anggota "+idanggota+" ?");
	if(del) {
		document.getElementById("act").value = 'hapusanggota';
		document.getElementById("key").value = idanggota;
		goSubmit();
	}
}
</script>
</html>
