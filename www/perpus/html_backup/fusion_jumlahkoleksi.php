<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
// pengecekan tipe session user
//$a_auth = Helper::checkRoleAuth($conng);

$ChartHeading = 'Jumlah Koleksi Buku Perpustakaan PJB';  
$XaxisName = 'Kode Koleksi';

//GET ini berfungsi untuk LinkedCharts.
$type = $_GET['type'] ;

//SESSION yang sebelumnya telah dipersiapkan dipanggil kembali kedalam variabel agar dapat diisi ke XML <chart>
$jumlahjudul = $_SESSION['jumlahjudul'];
$jumlaheks = $_SESSION['jumlaheksemplar'];

$gol = $_SESSION['_DATA_DDC'];
$a_jumlahjudul=$_SESSION['_DATA_KOLEKSIJUDUL'];
$a_jumlaheksemplar = $_SESSION['_DATA_KOLEKSIEKSEMPLAR'];

//Generate Chart XML: Head Part  

switch($type)
{
     default:
     case 'total':
     $Output = '<chart caption="'.$ChartHeading.'" xAxisName="'.$XaxisName.'" yAxisName="Jumlah Buku" showNames="1" bgColor="E6E6E6,F0F0F0" bgAlpha="100,50" bgRatio="50,100" bgAngle="270" showBorder="1" borderColor="AAAAAA" baseFontSize="12">';
     $Output .= '<set value="'.$jumlahjudul.'" name="Jumlah Judul" link="newchart-xmlurl-index.php?page=fusion_jumlahkoleksi&type=perkoleksijudul" />';
     $Output .= '<set value="'.$jumlaheks.'" name="Jumlah Eksemplar" link="newchart-xmlurl-index.php?page=fusion_jumlahkoleksi&type=perkoleksieksemplar" />';
     break;

     case 'perkoleksijudul':
     $Output = '<chart caption="'.$ChartHeading.'" xAxisName="'.$XaxisName.'" yAxisName="Jumlah Judul" showNames="1" bgColor="E6E6E6,F0F0F0" bgAlpha="100,50" bgRatio="50,100" bgAngle="270" showBorder="1" borderColor="AAAAAA" baseFontSize="12">';
	  for($i=0;$i<count($gol);$i++)
	  {
	  $Output .= ' <set value="'.(int)$a_jumlahjudul[$gol[$i]].'" name="'.$gol[$i].'" link = "javascript: loadKoleksi('.$i.' , \'buku\');" />';
	  }
     break;

     case 'perkoleksieksemplar':
     $Output = '<chart caption="'.$ChartHeading.'" xAxisName="'.$XaxisName.'" yAxisName="Jumlah Eksemplar" showNames="1" bgColor="E6E6E6,F0F0F0" bgAlpha="100,50" bgRatio="50,100" bgAngle="270" showBorder="1" borderColor="AAAAAA" baseFontSize="12">';
	  for($i=0;$i<10;$i++)
	  {
	  $Output .= ' <set value="'.(int)$a_jumlaheksemplar[$gol[$i]].'" name="'.$gol[$i].'" link = "javascript: loadKoleksi('.$i.' , \'eksemplar\');" />';
	  }
     break;
}

//Generate Chart XML: Last Part  
$Output .= ' </chart>';  
  
//Set the output header to XML  
header('Content-type: text/xml');  
  
//Send output  
echo $Output;
?>