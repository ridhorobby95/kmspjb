<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	
	// definisi variabel halaman
	$p_dbtable = 'ms_libur';
	$p_window = '[PJB LIBRARY] Daftar Hari Libur';
	$p_title = 'Daftar Hari Libur';
	$p_tbheader = '.: Daftar Hari Libur :.';
	$p_col = 4;
	$p_tbwidth = 100;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable where 1=1";

	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['namaliburan'] = Helper::cStrNull($_POST['i_namaliburan']);
			$record['tgllibur'] = Helper::formatDate($_POST['i_tgllibur']);
			$record['keterangan'] = Helper::cStrNull($_POST['i_keterangan']);
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
				
				
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['namaliburan'] = Helper::cStrNull($_POST['u_namaliburan']);
			$record['tgllibur'] = Helper::formatDate($_POST['u_tgllibur']);
			$record['keterangan'] = Helper::cStrNull($_POST['u_keterangan']);
			Helper::Identitas($record);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idliburan,$r_key);
			
			if($err != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
				$th=Helper::removeSpecial($_POST['tahun']);
				
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idliburan,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
				
				
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}

	}
	
	$th=Helper::removeSpecial($_POST['tahun']);
	  	
	//list tahun
	//$rs_cb = $conn->Execute("select date_part('year', tgllibur) as thlibur from ms_libur group by thlibur order by thlibur desc");
	$rs_cb = $conn->Execute("select EXTRACT(year FROM tgllibur) as thlibur from ms_libur group by EXTRACT(year FROM tgllibur) order by thlibur desc");
	if ($th=='')
		$th=date("Y");
	
	$l_libur = $rs_cb->GetMenu2('tahun',$th,false,false,0,'id="tahun" class="ControlStyle" style="width:100" onchange="javascript:goSubmit()"');

	
	//$p_sqlstr.=" and date_part('year', tgllibur) = '$th'";
	$p_sqlstr.=" and EXTRACT(year FROM tgllibur) = '$th'";
	
	// eksekusi sql list
	$p_sqlstr.=" order by tgllibur desc";
	$rs = $conn->Execute($p_sqlstr);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/jquery.common.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="30">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable table-responsive">
			<table width="100%">
				<tr>
					<td align="left">Filter Tahun : &nbsp;<?= $l_libur ?></td>
				</tr>
			</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<!--tr>
				<td class="thLeft" colspan="5">Filter Tahun : &nbsp;<?//= $l_libur ?></td>
			</tr-->
			<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Edit</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Hari Libur</td>
				<td width="170" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Hari Libur</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Keterangan</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idliburan'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['idliburan']; ?>');" class="link"><img src="images/edited.gif"></u>
				<? } else { ?>
				<img src="edited.gif">
				<? } ?>
				</td>
				<td width="150" align="center"><?= Helper::tglEng($row['tgllibur']); ?></td>
				<td width="250"><?= $row['namaliburan']; ?></td>
				
				<td width="200"><?= $row['keterangan']; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['idliburan']; ?>','<?= $row['namaliburan']; ?>');" class="link">[hapus]</u>
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><img src="images/edited.gif"></td>
				<td align="center"><?= UI::createTextBox('u_tgllibur',Helper::formatDate($row['tgllibur']),'ControlStyle',15,15,$c_edit); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgllibur1" style="cursor:pointer;" title="Pilih tanggal libur">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "u_tgllibur",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgllibur1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				<? } ?>
				
				</td>
				<td align="center"><?= UI::createTextBox('u_namaliburan',$row['namaliburan'],'ControlStyle',50,40,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_keterangan',$row['keterangan'],'ControlStyle',100,40,true,'onKeyDown="etrUpdate(event);"'); ?></td>

				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['idliburan']; ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><img src="images/popup.png" style="cursor:pointer;" title="Libur Spesial" onClick="popup('index.php?page=pop_libspesial',400,230);">
					<span id="span_posisi"><u title="Libur Spesial" onclick="popup('index.php?page=pop_libspesial',400,230);" style="cursor:pointer;" class="Link">Libur Spesial</u></span></td>
				
				<td align="center"><?= UI::createTextBox('i_tgllibur','','ControlStyle',15,15,$c_add); ?>
				<? if($c_add) { ?>
				<img src="images/cal.png" id="tgllibur" style="cursor:pointer;" title="Pilih tanggal libur">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "i_tgllibur",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgllibur",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				<? } ?>
				</td>
				<td align="center"><?= UI::createTextBox('i_namaliburan','','ControlStyle',50,40,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_keterangan','','ControlStyle',100,40,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr>
			<td colspan="5" class="footBG">&nbsp;
			</td>
		</table>
            </div>
                <br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namaliburan').focus();
	<? } else { ?>
	document.getElementById('i_namaliburan').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_namaliburan"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_namaliburan")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>