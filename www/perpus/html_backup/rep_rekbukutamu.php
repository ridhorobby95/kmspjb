<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Per Pengunjung';
	$p_filerep = 'repp_rekpengunjung';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namasatker, kdsatker from ms_satker where kdsatker not in ('***') order by namasatker");
	$l_jurusan= $rs_cb->GetMenu2('kdjurusan','',true,false,0,'id="kdjurusan" class="ControlStyle" style="width:156"');
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);

	$rs_tahun = $conn->Execute("select distinct to_char(tanggal, 'YYYY') tahun, to_char(tanggal, 'YYYY') thn from pp_bukutamu order by tahun asc");
	$l_tahun = $rs_tahun->GetMenu2('tahun',date('Y'),true,false,0,'id="tahun" class="ControlStyle" style="width:140"');
	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post"  target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Rekap Per Pengunjung</h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">Unit</td>
				<td class="RightColumnBG"><?= $l_jurusan; ?></td>
			</tr>
			<tr id="tr_tahun">
				<td class="LeftColumnBG thLeft">Tahun</td>
				<td class="RightColumnBG"><?= $l_tahun; ?> </td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("tahun")){
		document.perpusform.action='<?= Helper::navAddress($p_filerep) ?>';
			
		goSubmit();
	}
}


</script>
</html>