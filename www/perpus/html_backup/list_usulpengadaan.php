<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Daftar Usulan Pengadaan';
	$p_title = '.: Pengadaan :.';
	$p_title1 = 'Data Pengadaan';
	$p_titlelist = 'Daftar Pengusulan';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 900;
	$p_filelist = Helper::navAddress('list_pengadaan.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'op.idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from pp_orderpustaka op left join pp_usul u on u.idusulan=op.idusulan
			   where op.stsusulan=1 and op.idpengadaan is null";	
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton('<?= $p_atfirst ? '1' : '0' ?>','<?= $p_atlast ? '1' : '0' ?>');" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">

<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr>
		<td colspan="<?= $p_col ?>" class="PageTitle" align="center"><?= $p_titlelist; ?></td>
	</tr>
	<tr>
		<td align="center" valign="bottom" colspan="<?= $p_col ?>">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="right" valign="middle">
			<img title="first" id="firstButton" onClick="goFirst()" src="images/first.gif" style="cursor:pointer;">
			<img title="previous" id="prevButton" onClick="goPrev()" src="images/prev.gif" style="cursor:pointer;">
			<img title="next" id="nextButton" onClick="goNext(<?= $p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
			<img title="last" id="lastButton" onClick="goLast(<?= $p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
			&nbsp;&nbsp;&nbsp;</td>
		</tr>
		</table>	
		</td>
	</tr>
	<tr height="20"> 
		<td width="50" nowrap align="center" class="SubHeaderBGAlt"><u title="Pilih Semua" onclick="goAll()" style="cursor:pointer"><input type="checkbox" name="all" id="all" value="all" /></u></td>	
		<td nowrap width="70" align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','tglusulan:D');">Tgl. Usulan<?= $p_xsort['tglusulan']; ?></td>			
		<!--<td width="70"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','idusulan:C');">Id Usulan<?= $p_xsort['idusulan']; ?></td>-->	
		<td width="250"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>		
		<td width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','namaanggota:C');">Anggota Pengusul <?= $p_xsort['namaanggota']; ?></td>
		<td width="145" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','authorfirst1:C');">Pengarang <?= $p_xsort['authorfirst1']; ?></td>
		<td width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" >Jenis Usulan <?= $p_xsort['authorfirst1']; ?></td>
		<td width="50" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Aksi</td>
	</tr>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['idusulan'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height=20> 
		<td align="center">
		  <input type="checkbox" name="pilih[]" id="pilih" value="<?= $row['idusulan'] ?>" />
		</td>		
		<td align="center" id="tglusulan<?= $i?>"><?= Helper::formatDate($row['tglusulan']); ?></td>
		<!--<td align="left" id="idusulan<?= $i?>"><?= $row['idusulan']; ?></td>-->
		<td align="left" id="judul<?= $i?>"><?= $row['judul']; ?></td>
		<td align="left" id="namapengusul<?= $i?>"><?= $row['namapengusul']; ?></td>
		<td align="left" id="pengarang<?= $i?>"><?= $row['authorfirst1']." ".$row['authorlast1']; ?></td>
		<td align="center" id="jenisusulan<?= $i?>"><?= $row['idunit']== null ? 'Pengusulan Biasa' : 'Pengusulan Unit'; ?></td>
		<td align="center"><img src="images/add.png" title="Tambah" onClick="addUsul('<?= $i; ?>');" style="cursor:pointer"></td>
	</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Daftar usulan masih kosong.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
		</td>
	</tr>
</table>
		
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>



</body>

<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

</html>