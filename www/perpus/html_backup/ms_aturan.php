<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key =Helper::removeSpecial($_POST['key']);
	$pecah=explode(",",$r_key);

	// definisi variabel halaman
	$p_dbtable = 'pp_aturan';
	$p_window = '[PJB LIBRARY] Input Aturan Pustaka';
	$p_title = 'Input Aturan Pustaka';
	$p_tbwidth = 760;
	$p_filelist = Helper::navAddress('list_aturan.php');
	
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			
			$record = Helper::cStrFill($_POST);//,array('userid','nama','usertype','hints','statususer'));
			$record['satuanpinjam']=$_POST['sp']==1 ? 1 : 0;
			$record['isdownload']=$record['isdownload']==1 ? 1 : 0;
			
			if($r_key == '') { // insert record	
			
			$record['tglupdateterakhir']=date('Y-m-d');
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect();
				}
								
			}
			else { // update record
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,kdjenisanggota,$pecah[0]);		
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb); 
				}
			}
			
			if($conn->ErrorNo() == 0) {
				if($r_key =='')
					$pecah[0] = $record['kdjenisanggota']; 
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			} 
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where kdjenisanggota = '$pecah[0]'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	if(!$p_errdb) {
		if($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where kdjenisanggota = '$pecah[0]' ";
		$row = $conn->GetRow($p_sqlstr);	
		}
		
		
		if($row['satuanpinjam']=="1"){
		$op1="<label><input type=\"radio\" name=\"sp\" id=\"sp\" Value=\"1\" checked> Jam</label>";
		$op2="<label><input type=\"radio\" name=\"sp\" id=\"sp\" value=\"0\"> Hari</label>";
		} else {
		$op1="<label><input type=\"radio\" name=\"sp\" id=\"sp\" Value=\"1\"> Jam</label>";
		$op2="<label><input type=\"radio\" name=\"sp\" id=\"sp\" value=\"0\" checked> Hari</label>";
		}
	}
	
	// daftar combo box
	if($c_edit) {
		$rs_cb = $conn->Execute("select namajenisanggota, kdjenisanggota from lv_jenisanggota order by kdjenisanggota");
		$l_member = $rs_cb->GetMenu2('kdjenisanggota',$row['kdjenisanggota'],true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:150"');
		
		$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka order by kdjenispustaka");
		$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150"');
		
		$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by kdklasifikasi");
		$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$row['kdklasifikasi'],true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:150" onchange="LHaktif()"');
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<!--table width="<?//= $p_tbwidth ?>">
			<tr height="40">
				<td align="center" class="PageTitle"><?//= $p_title ?></td>
			</tr>
		</table-->
		<? include_once('_notifikasi.php'); ?>
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Aturan</span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>

		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<header style="width:760px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="7" class="GridStyle">
			<tr> 
				<td class="LeftColumnBG thLeft">Jenis Anggota  *</td>
				<td class="RightColumnBG"><?= $l_member; ?></td>
			</tr>
			<tr> 
				<td class="LeftColumnBG thLeft">Max. Waktu Pinjam</td>
				<td class="RightColumnBG"><?= UI::createTextBox('lamaperpanjangan',$row['lamaperpanjangan'],'ControlStyle',8,8,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
				<em>Nilai Nol, jika ingin pengembalian dihari yang sama.</em>
				</td>
				
			</tr>
			<tr>
			<td class="LeftColumnBG thLeft">Max. Perpanjangan </td>
				<td class="RightColumnBG"><?= UI::createTextBox('jumlahperpanjangan',$row['jumlahperpanjangan'],'ControlStyle',8,8,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
				 <em>Nilai Nol, jika tidak memperbolehkan melakukan perpanjangan.</em>
				 </td>
			</tr>
			<tr> 
				<td class="LeftColumnBG thLeft">Max. Jumlah Pinjam </td>
				<td class="RightColumnBG"><?= UI::createTextBox('batasmaxpinjam',$row['batasmaxpinjam'],'ControlStyle',8,8,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
				<em>Nilai Nol, jika tidak memperbolehkan melakukan peminjaman.</em>
				</td>
			</tr>
			<tr> 
				<td class="LeftColumnBG thLeft">Denda</td>
				<td class="RightColumnBG"><?= UI::createTextBox('denda',$row['denda'],'ControlStyle',8,8,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
			</tr>
			<tr height=20>
				<td class="LeftColumnBG thLeft">Satuan Pinjam</td>
				<td class="RightColumnBG"><? if ($c_edit) {
				     if($r_key!=''){ 
				    echo $op1." ".$op2;
				    } else { ?>
				  <label>
				    <input name="sp" type="radio" value="1" id="sp" checked />
				    Jam</label>
				  <label>
				    <input name="sp" type="radio" value="0" id="sp" />
				    Hari</label>
				  <? }
				    } else {
				    echo $row['satuanpinjam']=='1' ? "Jam" : "Hari";
				    }?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2>
				*) Harus diisi<br><br>
				Ket: <br>Nilai [ Null/Kosong ] merupakan tidak ada batas maksimal 
				</td>
			</tr>
			
		</table>



		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

		</form>
		</div>
	</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("kdjenisanggota"))
		goSave();
}

</script>
</html>