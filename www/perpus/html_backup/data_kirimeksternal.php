<?php
	//$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	
	// koneksi ke sdm
	$conns = Factory::getConnSdm();
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarolah';
	$p_window = '[PJB LIBRARY] Pengiriman Eksemplar';
	$p_title = 'PENGIRIMAN EKSEMPLAR / SIRKULASI';
	$p_tbheader = '.: PENGIRIMAN EKSEMPLAR :.';
	$p_col = 9;
	$p_tbwidth = 1135;
	$p_filedetail = Helper::navAddress('ms_anggota.php');
	$p_id = "datakirimeksternall";
	
	$p_defsort = 'ideksemplarolah';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$idunit=Helper::removeSpecial($_GET['lokasi']);
	$idanggota=Helper::removeSpecial($_GET['id']);
	$unitusul = Helper::removeSpecial($_GET['idunit']);
	
	
	$p_sqlstr = "select e.*,m.*, j.namajenispustaka,e.noseri as regcomp from $p_dbtable p
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar
				left join ms_pustaka m on m.idpustaka=e.idpustaka
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka
				left join pp_orderpustakattb ot on ot.idorderpustakattb=p.idorderpustakattb
				left join pp_orderpustaka op on op.idorderpustaka=ot.idorderpustaka
				left join pp_usul u on u.idusulan=op.idusulan
				where stskirimpengolahan=1 and stskirimeksternal=0 
				and m.kdjenispustaka in (".$_SESSION['roleakses'].") and isfinish=0 and (e.statuseksemplar is null or e.statuseksemplar='ADA')";
		
			
		if($idanggota!='' and $idunit!='' and $unitusul!=''){
			$p_sqlstr .=" and u.idunit='$unitusul' ";
		}else{
			$p_sqlstr .=" and u.idanggota='$idanggota' and u.isdenganpagu=1";
		}
		
		if($idanggota!=''){
		$p_anggota="select a.idanggota,a.namaanggota, a.kdjenisanggota from ms_anggota a 
					join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota 
					where a.idanggota='$idanggota'";
		$r_anggota=$conn->GetRow($p_anggota);
		
		if(!$r_anggota){
			$errdb = 'Data anggota tidak ditemukan.';
			Helper::setFlashData('errdb', $errdb); 	
			$idanggota='';
			$idunit='';
		}
		}
	
	if (!empty($_POST)) {
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		
		
		if ($r_aksi=='pinjamlab'){
				$conn->StartTrans();
				
				for($i=0;$i<count($_POST['cbSelect']);$i++){
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				//$r_strsent = implode("','",$_POST['cbSelect']);
				$record=array();
				$record = Helper::cStrFill($_POST);
				$record['kdlokasi']=$idunit;
				$record['idanggota']=$idanggota;
				$record['kdjenistransaksi']='PJL';
				$record['tgltransaksi']=date('Y-m-d');
				$record['ideksemplar']=Helper::removeSpecial($_POST['cbSelect'][$i]);
				$record['statustransaksi']=1;
				$record['fix_status']=1;
				Helper::Identitas($record);
				
				Sipus::InsertBiasa($conn,$record,pp_transaksi);
				
				$rec['statuseksemplar']='PJM';
				$rec['kdkondisi']='V';
				$rec['kdlokasi']=$idunit;
				Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,Helper::removeSpecial($_POST['cbSelect'][$i]));
				
				$recup['iskirimeksternal']=1;
				$recup['tglkirim']=date('Y-m-d H:i:s');
				$recup['npkkirim']=$_SESSION['PERPUS_USER'];
				$recup['tglsirkulasi']=date('Y-m-d H:i:s');
				$recup['npksirkulasi']=$_SESSION['PERPUS_USER'];
				$recup['stssirkulasi']=1;
				$recup['isfinish']=1;
				Sipus::UpdateBiasa($conn,$recup,pp_eksemplarolah,ideksemplar,Helper::removeSpecial($_POST['cbSelect'][$i]));
				
				}

				$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Peminjaman Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Peminjaman Pustaka berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
			
		}
		else if($r_aksi=='pinjamhijau'){
			for($i=0;$i<count($_POST['cbSelect']);$i++)
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				
				$r_strsent = implode("','",$_POST['cbSelect']);
			//$conn->StartTrans();
			$sqlsave=$conn->Execute("select noseri,ideksemplar from pp_eksemplar where ideksemplar in ('".$r_strsent."')");
			
			$recup['iskirimeksternal']=1;
			$recup['tglkirim']=date('Y-m-d H:i:s');
			$recup['npkkirim']=$_SESSION['PERPUS_USER'];
			$recup['tglsirkulasi']=date('Y-m-d H:i:s');
			$recup['npksirkulasi']=$_SESSION['PERPUS_USER'];
			$recup['stssirkulasi']=1;
			$recup['isfinish']=1;
			
			while($rsave=$sqlsave->FetchRow()){
				$rupdate['kdkondisi']='V';
				$rupdate['statuseksemplar']='ADA';
				$rupdate['kdlokasi']='LH';
				$rsave['noseri']."<br>";
				Sipus::UpdateBiasa($conn,$rupdate,pp_eksemplar,ideksemplar,$rsave['ideksemplar']);
				Sipus::UpdateBiasa($conn,$recup,pp_eksemplarolah,ideksemplar,$rsave['ideksemplar']);
				Sipus::newTransSpecial($conn,$rsave['noseri'],$idanggota,$r_anggota['kdjenisanggota'],false);
			}
			//$conn->CompleteTrans();
			
		}		
		else if ($r_aksi=='filter') {
		$filt=Helper::removeSpecial($_POST['carino']);
		if($filt!='')
		$p_sqlstr.=" and m.judul='$filt'";

		
		}

		
		
	}	
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}


	// list lokasi
	if($c_edit) {
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2("kdlokasi",$fil=='' ? $_POST['kdlokasi'] : $fil,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:140"');

		}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)"  >
<?php include('inc_menu.php'); ?>
<div align="center" style="position:relative;top:-11px;">
<form name="perpusform" id="perpusform" method="post">
<table width="99%" border="0" cellpadding="4" cellspacing=0>
	<tr>
	<td width="<?= $p_tbwidth ?>" align="center">
	
	<table width="530" border=0 align="center" cellpadding="4" cellspacing=0 class="instan">
      <? if($idanggota=='' and $idunit=='') { ?>
      <tr height="35">
	  <td>
	  
			<table width="400" border=0 align="center" cellpadding="6" cellspacing=0 style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
				<tr height="25">
					<th colspan="2">SIRKULASI KIRIM EKSTERNAL</th>
				</tr>
				<tr>
					<td><b>UNIT PENGUSUL</b></td>
					<td><?= UI::createTextBox('namapengusul',$row['namapengusul'],'ControlRead',50,30,true,'readonly'); ?>
						<img src="images/popup.png" style="cursor:pointer;" title="Pilih Unit kerja" onClick="showSatKerPU();">
						<input type="hidden" name="idunit" id="idunit" value="">
					</td>
				</tr>
				<tr>
					<td><b>LOKASI DIKIRIM</b></td>
					
					<td><?= $l_lokasi ?></td>
				</tr>
				<tr>
					<td width="130"><b>ID ANGGOTA</b></td>
					<td><input type="text" maxlength="20" name="i_idanggota" class="ControlStyle" id="i_idanggota" value="<?= $_POST['idanggota'] ?>" onKeyDown="etrTrans(event);">
					<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota"  onClick="popup('index.php?page=pop_anggota&code=pa',630,500);">
					
					</td>
				</tr>
				
				<tr>
					<td align="center" colspan=2><input type="button" name="btnkirim" id="btnkirim" value="Kirim Eksternal" onclick="goMulai()" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:150px;font-size:12px;"></td>
				</tr>
			</table>
	
	<center><br><? include_once('_notifikasi.php'); ?></center>
	  </td>
	  </tr>
	  <? } else {?>
	  <tr>
		<td>
			<table width="400" border=0 align="center" cellpadding="6" cellspacing=0 style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
				<tr height="25">
					<th colspan="3"><?= ($idanggota!='' and $idunit!='' and $unitusul!='') ? "SIRKULASI KIRIM UNIT" : "SIRKULASI KIRIM DOSEN" ?></th>
				</tr>
				<? if ($idanggota!='' and $idunit!='' and $unitusul!='') { ?>
				<tr>
					<td valign="top"><b>UNIT PENGUSUL</b></td>
					<td width="10" valign="top"><b>:</b></td>
					<td><b><?= Sipus::Search($conns,ms_satker,idsatker,$unitusul,namasatker) ?></b></td>
				</tr>
				<tr>
					<td valign="top"><b>LOKASI DIKIRIM</b></td>
					<td width="10" valign="top"><b>:</b></td>
					<td><b><?= Sipus::Search($conn,lv_lokasi,kdlokasi,$idunit,namalokasi) ?></b></td>
				</tr>
				<? } ?>
				<tr>
					<td width="130"><b>ID ANGGOTA</b></td>
					<td width="5"><b>:</b></td>
					<td><b><?= $idanggota ?></b></td>
				</tr>
				<tr>
					<td width="130"><b>NAMA ANGGOTA</b></td>
					<td width="5"><b>:</b></td>
					<td><b><?= $r_anggota['namaanggota'] ?></b></td>
				</tr>
			</table>
			<div align="center"><? include_once('_notifikasi.php') ?></div>
			<br>
			<table width="760" border=0 align="center" cellpadding="0" cellspacing=0 class="GridStyle">
				<tr>
					<td colspan=6>
						<table width="100%" cellpadding="4" cellspacing="0" border="0">
						<tr>	
							<td width="130">
								<a href="javascript:goKirimS();" class="buttonshort"><span class="new">
								&nbsp;&nbsp;&nbsp;Proses Kirim</span></a>
							</td>				
							<td width="100">
								<? if ($idanggota!='' and $idunit!='' and $unitusul!='') { ?>
								<u onclick="window.open('index.php?page=cetak_kirimlh&type=PJL&lokasi=<?= $idunit ?>&key=<?= $idanggota ?>&date='+document.getElementById('tglcetak').value,target='_blank')" style="cursor:pointer" class="buttonshort"><span class="print">
								Cetak</span></u>
								<? } else { ?>
								<u onclick="window.open('index.php?page=cetak_kirimlh&type=PJH&key=<?= $idanggota ?>&date='+document.getElementById('tglcetak').value,target='_blank')" class="buttonshort" style="cursor:pointer"><span class="print">
								Cetak</span></u>
								<? } ?>
							</td>				
							<td width="200">
								<a href="index.php?page=data_kirimeksternal" class="buttonshort"><span class="end">
								Selesai</span></a>
							</td>				
							<td align="right" valign="middle">
								<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
								<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
								<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
								<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
							</td>
						</tr>
						<tr>
							<td width="130"><strong>Tgl Cetak</strong></td>
							<td  width="350" colspan=2>:&nbsp;<input type="text" name="tglcetak" id="tglcetak" size="10" value="<?= date('d-m-Y') ?>" >
							<img src="images/cal.png" id="t_cetak" style="cursor:pointer;" title="Pilih tanggal Cetak">
							&nbsp;
							<script type="text/javascript">
							Calendar.setup({
							inputField     :    "tglcetak",
							ifFormat       :    "%d-%m-%Y",
							button         :    "t_cetak",
							align          :    "Br",
							singleClick    :    true
							});
							</script>		
							[ Format : dd-mm-yyyy ]
							</td> 
						</tr>
						<tr>
							<td width="130"><strong>Judul</strong></td>
							<td  width="350" colspan=2>:&nbsp;<input type="text" name="carino" id="carino" size="50" value="<?= $filt ?>" onKeyDown="etrCari(event);"></td> <td  align="right"><a href="javascript:goFilt()" class="buttonshort"><span class="filter">Filter</span></a></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr height="20"> 
					<td nowrap align="center" class="SubHeaderBGAlt" s><input type="checkbox" name="cbSelAll" id="cbSelAll" value="1" class="ControlStyle" onClick="checkedAll()"></td>
					<td width="100" align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">No. Induk  <?= $p_xsort['regcomp']; ?></td>
					<td width="300" align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul  <?= $p_xsort['judul']; ?></td>
					<td width="160" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','authorfirst1:C');">Pengarang  <?= $p_xsort['authorfirst1']; ?></td>
					<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','edisi:C');">Edisi <?= $p_xsort['edisi']; ?></td>		
					<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','kdjenispustaka:C');">Jenis  <?= $p_xsort['kdjenispustaka']; ?></td>
				</tr>	
				<?php
						$i = 0;
						if($showlist) {
							// mulai iterasi
							while ($row = $rs->FetchRow()) 
							{
								if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
								if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
							?>
							<tr class="<?= $rowstyle ?>" valign="middle"> 
								<td align="center">
								<input type="checkbox" id="cbSelect" name="cbSelect[]" value="<?= $row['ideksemplar']; ?>" class="ControlStyle">
								</td>
								<td><?= $row['regcomp']; ?></td>
								<td><?= $row['judul']; ?></td>
								<td><?php
													echo $row['authorfirst1']. " " .$row['authorlast1']; 
													if ($row['authorfirst2']) 
													{
														echo " <strong><em>,</em></strong> ";
														echo $row['authorfirst2']. " " .$row['authorlast2'];
													}
													if ($row['authorfirst3']) 
													{
														echo " <strong><em>,</em></strong> ";
														echo $row['authorfirst3']. " " .$row['authorlast3'];
													}
												?></td>
								<td align="center"><?= $row['edisi']; ?></td>
								<td align="center"><?= $row['namajenispustaka']; ?></td>
							</tr>	
							<? } } ?>
							<? if($i==0){?>
							<tr> 
								<td align="center" colspan="5"> 
									<b>Data tidak ditemukan</b>
								</td>
								
							</tr>
							
							<? } ?>
							<tr> 
								<td class="PagerBG" align="right" colspan="<?= $p_col ; ?>"> 
									Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
								</td>
								
							</tr>
			</table>
		</td>
	  </tr>
	  <? } ?>
	</table>

</div>


</td>
</tr>
</table>
<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">

<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
<input type="hidden" name="ranggota" id="ranggota" value="<?= $anggota!='' ? $anggota : $idanggota ?>">
<input type="hidden" name="runit" id="runit" value="<?= $unit!='' ? $unit : $idunit ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

<input type="hidden" name="act" id="act">

</form>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script language="javascript">
function etrTrans(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnkirim").click();
}

function goMulai(){
	if(cfHighlight("i_idanggota")){
	var x=document.getElementById('i_idanggota').value;
	var y=document.getElementById('kdlokasi').value;
	var u=document.getElementById('idunit').value;
	var z='';
	//document.getElementById('act').value='mulai';
	if(x!='')
		z='&id='+x;
	if(y!='' && u!='')
		z=z+'&lokasi='+y+'&idunit='+u;
		
	document.perpusform.action='index.php?page=data_kirimeksternal'+z;
	goSubmit();
	}
}
function showSatKerPU() {
	win = window.open("<?= Helper::navAddress('pop_satker.php'); ?>&nama=namapengusul&id=idunit","popup_satker","width=450,height=400,scrollbars=1");
	win.focus();
}
function goFilt(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function goKirimS(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih data yang akan dikirim ke eksternal/peminjaman.");
	else {
		var x='<?= $idanggota ?>';
		var y='<?= $idunit ?>';
		var u='<?= $unitusul ?>';
		
		if (x!='' && y!='' && u!=''){
			document.getElementById('act').value='pinjamlab'
			goSubmit();
		}else {
			document.getElementById('act').value="pinjamhijau";
			goSubmit();
		}
	}
}


checked=false;
function checkedAll () {
	var aa= document.getElementById('perpusform');
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i =0; i < aa.elements.length; i++) 
	{
	 	aa.elements[i].checked = checked;
	}
}
</script>
</html>