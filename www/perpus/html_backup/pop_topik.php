	<?php
//$conn->debug=true;
		defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

		// pengecekan tipe session user
		$a_auth = Helper::checkRoleAuth($conng,false);

		// otorisasi user
		$c_add = $a_auth['cancreate'];
		$c_edit = $a_auth['canedit'];
		$c_delete = $a_auth['candelete'];

		// pengecekan tipe session user
		 $a_auth = Helper::checkRoleAuth($conng);

		// otorisasi user
		$c_add = $a_auth['cancreate'];
		
		// definisi variabel halaman
		$p_dbtable = 'lv_topik';
		$p_window = '[PJB LIBRARY] Daftar Topik Pustaka';
		$p_title = 'Daftar Topik Pustaka';
		$p_tbheader = '.: Daftar Topik Pustaka :.';
		$p_col = 3;
		$p_tbwidth = 100;
		$p_id = "ms_topik";
		$p_row = 10;
		
		// sql untuk mendapatkan isi list
		$p_sqlstr = "select * from $p_dbtable";
		$id=Sipus::GetLast($conn,lv_topik,idtopik);
		// pengaturan ex
		if (!empty($_POST)) {
			$r_aksi = Helper::removeSpecial($_POST['act']);
			$r_key = Helper::removeSpecial($_POST['key']);
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
			if($r_aksi == 'insersi' and $c_add) {
				$record = array();
				$record['idtopik'] = $id;
				$record['namatopik'] = Helper::cStrNull($_POST['i_topik']);
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
					else {
					
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
				//echo '<script>goRefC();</script>';
			}
			
		//filtering
		$keytopik=Helper::removeSpecial($_POST['caritopik']);
		if($keytopik!='')
			$p_sqlstr.=" where upper(namatopik) like upper('%$keytopik%')";
		
		}
		else
		{
			// dapatkan nilai ex dari session
			if ($_SESSION[$p_id.'.page'])
				$p_page = $_SESSION[$p_id.'.page'];
		}
		if (!$p_page)
			$p_page = 1; // halaman default adalah 1

		// eksekusi sql list
		$p_sqlstr.=" order by case when upper(namatopik) = upper('".$keytopik."') then 0 else 1 end ";
		
		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		if ($rs->EOF) {
			// tidak ditemukan record atau ada kesalahan
			$p_atfirst = true;
			$p_atlast = true;
			$p_lastpage = 0;
			$p_page = 0;
		}
		else {
			// ditemukan record
			$p_atfirst = $rs->AtFirstPage();
			$p_atlast = $rs->AtLastPage();
			$p_lastpage = $rs->LastPageNo();
			$showlist = true;
		}
	?>
	<html>
	<head>
		<title><?= $p_window ?></title>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<link href="style/style.css" type="text/css" rel="stylesheet">
		<link href="style/pager.css" type="text/css" rel="stylesheet">
		<link href="style/button.css" type="text/css" rel="stylesheet">
		
		<script type="text/javascript" src="scripts/forinplace.js"></script>
		<script type="text/javascript" src="scripts/forpager.js"></script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);initPage();">
	<div id="wrapper" style="width:auto;margin:0;padding:0;">
		<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
			<div align="center">
			<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<div class="filterTable">
						<table border=0  cellpadding=0 cellspacing=0 width="100%">
						<tr>
							<td align="left">Nama Topik : &nbsp;<input type="text" name="caritopik" id="caritopik" size="18" value="<?= $keytopik ?>" onKeyDown="etrCari(event);"></td>
							<td align="right" >
								<input type="button" value="Cari" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRefresh();goFilter(false);" />
							</td>
						</tr>
						</table>
			</div><br/>
			<header style="width:420px;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<h1><?= $p_title; ?></h1>
					</div>
				</div>
			</header>
			<? include_once('_notifikasi.php'); ?>
			<table width="420px;" border="1" cellpadding="3" cellspacing=0 class="GridStyle">
				<tr height="20"> 
					<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Topik Pustaka</td>
					<td width="30" nowrap align="center" class="SubHeaderBGAlt thLeft">Pilih</td>
				</tr>
				<?php
					$i = 0;
					while ($row = $rs->FetchRow()) 
					{
						if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					
				?>
				<tr class="<?= $rowstyle ?>">
					<td><u title="Pilih Topik" class="link" onclick="goSend('<?= $row['idtopik'] ?>','<?= $row['namatopik']; ?>')"><?= $row['namatopik']; ?></u></td>
					<td align="center">
					<img title="Pilih Topik" src="images/centang.gif" style="cursor:pointer" onClick="goSend('<?= $row['idtopik'] ?>','<?= $row['namatopik']; ?>')">
					</td>
				</tr>
				<?php
						
					}
					if ($i==0) {
				?>
				<tr height="20">
					<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
				</tr>
				<?php } if($c_add) { ?>
				<tr class="LiteSubHeaderBG"> 
					<td><?= UI::createTextBox('i_topik','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
					<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
				</tr>
				<?php }  ?>
				<tr> 
					<td class="PagerBG footBG" align="right" colspan="4"> 
						Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
					</td>
					
				</tr>
			</table>
				<?php require_once('inc_listnav.php'); ?><br>
			<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="key" id="key">
			<input type="hidden" name="scroll" id="scroll">
			</form>
			</div>
		</div>
	</div>
	</body>
	<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

	</script>
	<script type="text/javascript">

	function etrInsert(e) {
		var ev= (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			insertData();
	}


	function initPage() {
		initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
		<? if($p_editkey != '') { ?>
		document.getElementById('u_topik').focus();
		<? } else { ?>
		document.getElementById('i_topik').focus();
		<? } ?>
		window.opener.loadtopik();
	}

	function insertData() {
		if(cfHighlight("i_topik")){
			goInsertIP();
			//window.opener.loadtopik();
			}
	}


	function goRefresh(){
	document.getElementById("caritopik").value='';
	goSubmit();

	}

	/*function goSend(key) {
		
		window.opener.document.getElementById("idtopik").value = key;
		window.close();
	}*/
	
	function goSend(key,nm) {
		
		window.opener.document.getElementById("idtopik").value = key;
		window.opener.document.getElementById("namatopik").value = nm;
		window.close();
	}
	</script>
	</html>
	
	
	
	
