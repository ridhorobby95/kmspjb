<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// include tambahan
	include_once('includes/pChart/pData.class');
	include_once('includes/pChart/pChart.class');
	
	$cw = 700;
	$ch = 250;
	
	$a_data = $_SESSION['_DATA_JUMLAHPEMINJAM'];

	$n_data = 0;
	foreach($a_data as $t_jmlobjek)
		$n_data += $t_jmlobjek;
	
	$a_judulStatistik = $_SESSION['_DATA_BULAN'];
	
	$maxy = 0;
	$a_jmlobjek = array();
	foreach($a_judulStatistik as $t_Judul) {
		$n_jmlobjek = Helper::cEmChg($a_data[$t_Judul],'0'); 
		
		$a_jmlobjek[] = $n_jmlobjek;
		
		if($n_jmlobjek > $maxy)
			$maxy = $n_jmlobjek;
	}

	if(!empty($maxy)) {
		// skala y maksimal-minimal
		$maxsc = 5;
		$minsc = 2;
		
		$mod = -1;
		$cursc = $maxsc;
		while($cursc >= $minsc) {
			$mod = $maxy%$cursc;
			if($mod == 0 or $mod == $cursc-1)
				break;
			
			$cursc--;
		}
		
		if(!empty($mod)) {
			if($cursc < $minsc)
				$cursc++; // ambil skala minimal
			$maxy += ($cursc-$mod);
		}
		$nscale = $cursc;
	}
	else {
		$maxy = 1;
		$nscale = 1;
	}

	$header = array();
	foreach($a_judulStatistik as $judul){
		$header[] = $judul;
	}

	// definisi data set
	$DataSet = new pData;
	$DataSet->AddPoint($a_jmlobjek,'SeriePeminjam');
	$DataSet->AddPoint($header,'Peminjam');
	
	//$DataSet->AddPoint(array(1,4,-3,2,-3,3,2,1,0,7,4,-3,2,-3,3,5,1,0,7),'SeriePeminjam');
	//$DataSet->AddPoint(array(0,3,-4,1,-2,2,1,0,-1,6,3,-4,1,-4,2,4,0,-1,6),'Peminjam');
	$DataSet->AddSerie('SeriePeminjam');
	$DataSet->SetAbsciseLabelSerie('Peminjam');
	$DataSet->SetSerieName('Peminjam','SeriePeminjam');
	$DataSet->SetXAxisName('Bulan');
	$DataSet->SetYAxisName('Jumlah Data: '.$n_data);

	// Initialise the graph   
	$Chart = new pChart($cw,$ch);   
	$Chart->setFontProperties('style/tahoma.ttf',8);   
	$Chart->setGraphArea(50,30,$cw-20,$ch-50);
	$Chart->setFixedScale(0,$maxy,$nscale);
	$Chart->drawFilledRoundedRectangle(7,7,$cw-3,$ch-13,5,240,240,240);   
	$Chart->drawRoundedRectangle(5,5,$cw-1,$ch-11,5,230,230,230);   
	$Chart->drawGraphArea(200,255,200,TRUE);
	$Chart->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2,TRUE);   
	$Chart->drawGrid(4,TRUE,230,230,230,50);
	
	// gambar garis 0
	$Chart->setFontProperties('style/tahoma.ttf',6);
	$Chart->loadColorPalette('style/palette.txt');
	$Chart->drawTreshold(0,143,55,72,TRUE,TRUE);   
		
	// gambar grafik
	$Chart->drawBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(),TRUE);
	$Chart->WriteValues($DataSet->GetData(),$DataSet->GetDataDescription(),'SeriePeminjam');
	
	// tampilkan chart
	$Chart->Stroke();
?>