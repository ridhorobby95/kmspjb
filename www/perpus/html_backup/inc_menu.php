<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	$conn->debug = false;
	$separator = " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ";
	
	// load menu langsung, untuk pengujian
	if($_SESSION['roleakses']==''){ 
		$roleakses=$conn->Execute("select kdjenispustaka from lv_jenispustaka");
		if ($roleakses) {
			$xrole=$roleakses->RowCount();
			while($rr=$roleakses->FetchRow()){
				$_SESSION['roleakses']=$_SESSION['roleakses'].($_SESSION['roleakses']=='' ? "'".$rr['kdjenispustaka']."'" : ",'".$rr['kdjenispustaka']."'");
			}
		}
	}
	
	if($_SESSION['userdesc']==''){
		$user = $conng->GetOne("select userdesc from sc_user where username='".$_SESSION['PERPUS_USER']."'");
		
		$_SESSION['userdesc']=$user;
	}
	
	$countUsulan = $conn->Getone("select count(*) from pp_usul u where u.idunit is null and statususulan = '0' ");
	#hubungi kami
	$countDigilib = $conn->Getone("select count(*) from pp_kontakonline p where p.idparentol is null and p.isaktif=1 and p.idkontak = 0
				      and (select count(*) from pp_kontakonline where idparentol = p.idkontakol) = 0  ");
	#kontak online
	$countKontak = $conn->Getone("select count(*) from pp_kontakonline p where p.idparentol is null and p.isaktif=1 and p.idkontak <> 0
				      and (select count(*) from pp_kontakonline where idparentol = p.idkontakol) = 0  ");
	$countReservasi = $conn->Getone("select count(*) from pp_reservasi where statusreservasi='1' and tglexpired > current_date ");
	$countPanjang = $conn->Getone("select count(*) from pp_perpanjangan where statusajuan='1' ");
	if($_REQUEST["nomenu"] != 1) {  
?>
<script src="../application/assets/js/jquery.js"></script> 
<script type="text/javascript" src="scripts/jquery-ui-1.10.1.min.js"></script>    
<link rel="stylesheet" href="style/jquery.ui-1.10.1.min.css" type="text/css" /> 
<script src="../application/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/jquery.jdMenu.js"></script> 
<script type="text/javascript" src="scripts/jquery.dimensions.js"></script>
<script type="text/javascript" src="scripts/jquery.positionBy.js"></script>
<link rel="stylesheet" href="../application/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="style/menu.css">
<link rel="stylesheet" href="style/button.css">
<link rel="stylesheet" href="style/style.css">
<link rel="stylesheet" href="style/jquery.jdMenu.css">
<link rel="stylesheet" href="../application/assets/css/font-awesome.min.css">
<link rel="icon" type="image/x-icon" href="images/favicon.ico">

<div class="menus navbar-fixed-top" style="background:#0b4c5f;">
    <div class="container">
<div class="box">
    <div class="row"> 
		<div class="">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="background:#fff;">
                <span class="icon-bar" style="background:#000033;"></span>
                <span class="icon-bar" style="background:#000033;"></span>
                <span class="icon-bar" style="background:#000033;"></span>                        
              </button>
                <ul class="jd_menu">
                        <li>
                            <a href="javascript:void(0)" style="padding:0px;height:25px;"><img src="images/gear.png" width="25" style="position:relative;bottom:5px; float:left;"></a>
                        <span></span>
                        <ul style="width:180px;padding:15px 5px;  border-bottom-left-radius:4px; border-bottom-right-radius:4px;">
                            <div style="float:left;margin-left:15px">
                                <div class="DivButton" style="float:left;margin-left:3px;margin-top:-3px" onclick="location.href='<?= Config::gateUrl; ?>'">
                                    <img src="images/menu.png" width="16"> <br>Menu KM
                                </div>
                                <div class="DivButton" style="float:left;margin-left:3px;margin-top:-3px" onclick="location.href='<?= Config::logoutUrl; ?>'">
                                    <img src="images/exit.png" width="16"> <br>Log Out
                                </div>
                            </div>
                        </ul>
                    </li>
                </ul>
            </div>
            
			<div class="collapse navbar-collapse" role="navigation" id="myNavbar">
                
				<?= Helper::getMenu($conn) ?>
                
                <table>
					<tr>
						<td valign="top" class="notif1-left">
							<a href="index.php?page=list_usulanbiasa" title="Pengusulan Pustaka Baru">
								<img width="25" src="images/aktivitas/DEFAULT.png">		
								<span id="notif1" class="SideSubTitle<?=($countUsulan>0?" blink":"");?>">
									<?=number_format($countUsulan,0,'','');?>
								</span>
							</a>
						</td>
						<td width="10%"></td>
						<td valign="top">
							<a href="index.php?page=list_answerkontak" title="Pesan dari Kontak Online">
								<img width="25" src="images/aktivitas/FORUM.png">
								<span id="notif4" class="SideSubTitle<?=($countKontak>0?" blink":"");?>">
									<?=number_format($countKontak,0,'','');?>
								</span>
							</a>
						</td>
						<td width="10%"></td>
						<td valign="top">
							<a href="index.php?page=list_answeronline" title="Pesan dari Hubungi Kami">
								<img width="25" src="images/aktivitas/MAIL.png">
								<span id="notif5" class="SideSubTitle<?=($countDigilib>0?" blink":"");?>">
									<?=number_format($countDigilib,0,'','');?>
								</span>
							</a>
						</td>
						<td width="10%"></td>
						<td valign="top">
							<a href="index.php?page=list_reservasi" title="Reservasi Pustaka">
								<img width="25" src="images/aktivitas/ABSENSI.png">
								<span id="notif2" class="SideSubTitle<?=($countReservasi>0?" blink":"");?>">
									<?=number_format($countReservasi,0,'','');?>
								</span>
							</a>
						</td>
						<td width="10%"></td>
						<td valign="top">
							<a href="index.php?page=list_perpanjangan" title="Perpanjangan Pustaka">
								<img width="25" src="images/aktivitas/KULIAH.png">
								<span id="notif3" class="SideSubTitle<?=($countPanjang>0?" blink":"");?>">
									<?=number_format($countPanjang,0,'','');?>
								</span>
							</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
    </div>
</div>
<?php } else { 
// Start Menu Section =======================================================================
?>

<table width="30" align="center" border=1 cellspacing=0 cellpadding="0" class="GridStyle" bgcolor="#999999">
  <tr> 
    <td width="20%" height="29" align="left" nowrap>
	<button onClick="window.close();" class="OuButton" onMouseOver="this.className='OvButton'" onMouseOut="this.className='OuButton'" style="cursor:pointer;height:30px;width:100px;font-size:14px;">
	<img src="images/bdel.gif"> Tutup</button>
	</td>
  </tr>
</table>
<br>

<? 
 // End No Menu Section ======================================================================
} ?>
<div class="Header" style="margin-top:60;">
    <div class="container">
        <img src="images/logosim.jpg">
	<div style="float:right;">
		<h3 style="color:#0366D0;"><?php echo $_SESSION['PJB']['namarole']." [ ".$_SESSION['PERPUS_NAMASATKER']." ]";?></h3>
	</div>
     </div>
    
</div>
<script type="text/javascript">
<?php  if($_SESSION[G_SESSION]['iduser']){?>
	$(document).ready(function() {
		var refreshId = setInterval(function() {getNotif()}, 5000);
	});
	
	function getNotif(){
		file = "<?= Helper::navAddress('ajax_perpus.php'); ?>";

		$.ajax({
		    type: "post",
		    url: file+"&act=getUsulanBaru", 
		    cache: false,
		    success: function(data){
				  document.getElementById('notif1').innerHTML = data;
		    }
		});
		$.ajax({
		    type: "post",
		    url: file+"&act=getPesanDigilib",
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notif4').innerHTML = data;
		    }
		});
		$.ajax({
		    type: "post",
		    url: file+"&act=getKontak",
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notif5').innerHTML = data;
		    }
		});
		$.ajax({
		    type: "post",
		    url: file+"&act=getReservasiBaru",
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notif2').innerHTML = data;
		    }
		});
		$.ajax({
		    type: "post",
		    url: file+"&act=getPanjangBaru",
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notif3').innerHTML = data;
		    }
		});
	}
<?php } ?>
</script>
