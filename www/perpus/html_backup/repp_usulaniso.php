<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	$periode = Helper::periodeISO($r_tgl1);
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Sasaran Mutu ISO';
	
	$p_namafile = 'rekap_bukudatang_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql = "select * from pp_orderpustakattb ot
			join pp_ttb t on ot.idttb=t.idttb and t.jnsttb not in(2,3)
			join pp_orderpustaka o on ot.idorderpustaka=o.idorderpustaka
			join pp_usul u on o.idusulan=u.idusulan
			left join ms_supplier s on o.supplierdipilih=s.kdsupplier
			where t.tglttb between '$r_tgl1' and '$r_tgl2' ";
	
	if($r_jenis==1)
	$sql .=" and u.isdenganpagu=0 and u.idunit is null ";
	elseif($r_jenis==2)
	$sql .=" and u.isdenganpagu=0 and u.idunit is not null ";
	elseif($r_jenis==3)
	$sql .=" and u.isdenganpagu=1 and u.idunit is null ";
	else
	$sql .="";
	
	$sql .=" order by t.tglttb ";
	$row = $conn->Execute($sql);
	$rsj = $row->RowCount();
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=700>
	<tr>
		<td width=100><img src="<?= $dirIcon.'logounusa.jpg' ?>" width=70 height=60></td>
		<td valign="center">Monitoring Sasaran Mutu<br>FM-MR-04/-/01/Rev 01<br>BDU: Perpustakaan</td>
	</tr>
</table><br>
<table width=700 border=0 cellpadding="2" cellspacing="0">
  <tr>
	<td>Nomor Prosedur : PM-PERPUS-01<br>
		Nama Dokumen : Pengadaan bahan Pustaka Perpustakaan<br>
		Sasaran Mutu : Pemenuhan bahan pustaka sesuai dengan permintaan / usulan pelanggan perpustakaan<br>
		Kriteria Keberhasilan : <?= $periode ?> % tingkat pemenuhan bahan pustaka sesuai dengan permintaan / usulan pelanggan perpustakaan <br>
		Periode : <?= Helper::formatDate($r_tgl1) ." s/d ". Helper::formatDate($r_tgl2) ?><br>
		Keberhasilan : Tercapai 100 % 
</table><br>
<table width="700" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
    <th width="200" align="center"><strong>Judul Pustaka Dipesan</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka Diterima</strong></th>
	<th width="90" align="center"><strong>Tanggal</strong></th>
	<th width="80" align="center"><strong>Keterangan</strong></th>
	<th align="center"><strong>Nomor Registrasi MR</strong></th>

  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>   
	<td align="left"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>	
	<td align="left"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglttb']) ?></td>
	<td align="left">Tercapai</td>
	<td>&nbsp;</td>

  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Data tidak ditemukan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah Buku: <?= $rsj ?><b></td></tr>
</table>


</div>
</body>
</html>