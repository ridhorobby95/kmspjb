<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	//$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_kategori = Helper::removeSpecial($_REQUEST['kategori']);
	$r_teratas = Helper::removeSpecial($_REQUEST['teratas']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pengusulan Pustaka Grafik';
	
	$p_namafile = 'rekap berdasarkan kategori_'.$r_kategori;
	
	$sql = "select u.judul,u.hargausulan,u.authorfirst1,u.authorlast1,u.penerbit,u.tahunterbit,u.vote_usulan,u.tglusulan from pp_usul u where u.tglusulan between '$r_tgl1' and '$r_tgl2' and idunit is null";
	
	$sqlharga1 = "select count(hargausulan) from pp_usul u where hargausulan < 50000 and u.tglusulan between '$r_tgl1' and '$r_tgl2' and idunit is null ";
	$sqlharga2 = "select count(hargausulan) from pp_usul u where hargausulan > 50000 and hargausulan < 100000 and u.tglusulan between '$r_tgl1' and '$r_tgl2' and idunit is null";
	$sqlharga3 = "select count(hargausulan) from pp_usul u where hargausulan > 100000 and u.tglusulan between '$r_tgl1' and '$r_tgl2' and idunit is null";
	
	
	$harga[1] = " Kurang dari Rp 50.000 ";
	$harga[2] = " Antara Rp 50.000 dan Rp 100.000 ";
	$harga[3] = " Lebih dari Rp 100.000 ";
	
	$countHarga[' Kurang dari Rp 50.000 '] = (int) $conn->GetOne($sqlharga1);
	$countHarga[' Antara Rp 50.000 dan Rp 100.000 '] = (int) $conn->GetOne($sqlharga2);
	$countHarga[' Lebih dari Rp 100.000 '] = (int) $conn->GetOne($sqlharga3);
	
	$sql1 =$sql." order by u.vote_usulan desc limit $r_teratas ";
	$row = $conn->Execute($sql1);
	
	$fullrow = $conn->Execute($sql);
	$rsj = $fullrow->RowCount();
	
	$a_judul = array();
	$a_vote = array();
	
	$i =0;
	
	while($row1 = $row->FetchRow()){
		$a_judul[$i]= $row1['judul'];
		$a_vote[$row1['judul']] = (int) $row1['vote_usulan'];
		$i++;
	}
	$_SESSION['_DATA_JUDUL'] = $a_judul;
	$_SESSION['_DATA_HARGA'] = $harga;
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Pengusulan Pustaka Grafik</h2>
  	</strong></td>
  </tr>
</table>

<a href="javascript:window.print()">Cetak Grafik</a>
<?php if($r_kategori =='vote') { ?>
<table>
	
	 <tr><td align="center"> Grafik Batang Voting <?=$r_teratas ?> terbesar periode <?= Helper::formatDateInd($r_tgl1) ?> sampai dengan <?= Helper::formatDateInd($r_tgl2) ?> <br>
	<?	$_SESSION['_DATA_BARVOTE'] = $a_vote; ?>
       <br><br>
	<img src="<?= Helper::navAddress('img_pieusulan_judul') ?>">
	</td></tr>
</table>
<? } ?>

<?php if($r_kategori =='harga') { ?>
<table>
	 <tr><td align="center"> Grafik Batang dan Pie Berdasarkan Harga periode <?= $r_tgl1 ?> sampai dengan <?= $r_tgl2 ?> <br>
	<?	$_SESSION['_DATA_BARHARGA'] = $countHarga; ?>
        <img src="<?= Helper::navAddress('img_barusulan_harga') ?>" /><br><br>
	<img src="<?= Helper::navAddress('img_pieusulan_harga') ?>"
	</td></tr>
</table>
<? } ?>

</div>
</body>
</html>