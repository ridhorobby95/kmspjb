<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pengguna Aktif';

	$p_filerep = 'repp_pengguna_aktif';
	$p_tbwidth = 420;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
		
	$rs_jenispustaka = $conn->Execute("select namajenispustaka,kdjenispustaka from lv_jenispustaka order by namajenispustaka");
	$l_jenispustaka = $rs_jenispustaka->GetMenu2('kdjenispustaka','',true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:120"');
	
	$a_grafik = array('batang'=>'Grafik Batang','pie'=>'Grafik Pie');
	$l_grafik = UI::createSelect('grafik',$a_grafik,'','ControlStyle');
	
	$a_limit = array('3'=>'3 Teratas','5'=>'5 Teratas','7'=>'7 Teratas','10'=>'10 Teratas');
	$l_limit = UI::createSelect('limit',$a_limit,'','ControlStyle');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan Pengguna Aktif</td></tr>
	<tr>
		<td class="LeftColumnBG">Jenis Sirkulasi</td>
		<td><?= $l_jenispustaka ?></td>
	</tr>

	<tr> 
		<td class="LeftColumnBG" width="120">Periode</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG"> Bentuk Grafik </td>
		<td class="RightColumnBG"><?= $l_grafik ?> </td>
	</tr>
	<tr>
		<td class="LeftColumnBG"> Teratas </td>
		<td class="RightColumnBG"><?= $l_limit ?> </td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>
</table>
<br>
	
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
* untuk format Word dan Excel tidak akan ikut ditampilkan grafik *
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2"))
		goSubmit();
}

</script>
</html>