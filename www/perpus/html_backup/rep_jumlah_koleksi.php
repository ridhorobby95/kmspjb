<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Jumlah Koleksi Buku';
	
	//definisi akan di-direct ke mana.
	$p_filerep = 'repp_jumlah_koleksi';
	$p_tbwidth = 600;
	
	// combo box
	$a_bulan = array('01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'Nopember', '12' => 'Desember');
	$l_bulan = UI::createSelect('r_bulan',$a_bulan,'','ControlStyle',true,'style="width:140"');
	
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$a_grafik = array('batang' => 'Grafik Batang','pie' => 'Grafik Pie');
	$l_grafik = UI::createSelect('grafik',$a_grafik,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal<>1 and kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka','',true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goFilterEx();showJenisBuku()" ');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:600px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Jumlah Koleksi</h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td width="150" class="LeftColumnBG thLeft">Pilih Filter</td>
				<td class="RightColumnBG">
					<label id="param" ><input name="param" type="radio" id="param1" value="semua" checked onChange="showSemua()" /> Rekapitulasi Koleksi</label>
					<label id="param" ><input name="param" type="radio" id="param2" value="perbulan" onChange="showBulan()" /> Rekapitulasi Koleksi Per Bulan</label>
				</td>
			</tr>
			<tr id="grafik">
				<td class="LeftColumnBG thLeft">Jenis Pustaka</td>
				<td class="RightColumnBG"><?= $l_jenis ?></td>
			</tr>
			<tr id="bulan" style="display:none">
				<td class="LeftColumnBG thLeft">Bulan</td>
				<td class="RightColumnBG"><?= $l_bulan ?></td>
			</tr>
			<tr id="tahun">
				<td class="LeftColumnBG thLeft">Tahun</td>
				<td class="RightColumnBG"><input type="text" name="tahun2" id="tahun2" value="<?= date("Y")?>" size=4 maxlength=4>
			</tr>
			<tr id="grafik">
				<td class="LeftColumnBG thLeft"> Grafik</td>
				<td class="RightColumnBG"><?= $l_grafik ?></td>
			</tr>
			<tr id="format">
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		<input type="hidden" id="parameter" name="parameter">
		</form>
		* untuk format Word dan Excel tidak akan ikut ditampilkan grafik *
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	//alert($("#tahun2").val());exit;
	
	if(document.getElementById("param2").checked){
		if(cfHighlight("tahun2"))
			goSubmit();
	}
	else
		goSubmit();
}

function showBulan(){
	$("#bulan").show();
	document.getElementById("perpusform").action = "index.php?page=repp_jumlah_koleksi";
}

function showSemua(){
	$("#bulan").hide();
	document.getElementById("perpusform").action = "index.php?page=repp_jumlah_koleksi";
}
</script>
</html>