<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];


	// definisi variabel halaman
	$p_dbtable = 'pp_borang';
	$p_window = '[PJB LIBRARY] Daftar Master Borang';
	$p_title = 'Daftar Master Borang';
	$p_tbheader = '.: Daftar Master Borang :.';
	$p_col = 3;
	$p_tbwidth = 870;
	$p_id = "ms_role";
	$p_row = 15;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select p.*,j.f_jur,j.namajurusan,j.kdjurusan as jur from pp_borang p 
				 join lv_jurusan j on p.kdjurusan=j.f_jur and p.kdfakultas=j.kdfakultas
				 where 1=1";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2 = Helper::removeSpecial($_POST['key2']);
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$jur = Helper::cStrNull($_POST['i_jurusan']);
			$cjur = $conn->GetRow("select * from lv_jurusan where kdjurusan='$jur'");
			
			$record = array();
			$record['kdjurusan'] = $cjur['f_jur'];
			$record['kdfakultas'] = $cjur['kdfakultas'];
			$record['kode'] = Helper::cStrNull($_POST['i_kode']);
			$record['kelas1'] = Helper::cStrNull($_POST['i_kelas1']);
			$record['kelas2'] = Helper::cStrNull($_POST['i_kelas2']);
			$record['keterangan'] = Helper::cStrNull($_POST['i_ket']);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$jur = Helper::cStrNull($_POST['u_jurusan']);
			$cjur = $conn->GetRow("select * from lv_jurusan where kdjurusan='$jur'");
			
			$record = array();
			$record['kdjurusan'] = $cjur['f_jur'];
			$record['kdfakultas'] = $cjur['kdfakultas'];
			$record['kode'] = Helper::cStrNull($_POST['u_kode']);
			$record['kelas1'] = Helper::cStrNull($_POST['u_kelas1']);
			$record['kelas2'] = Helper::cStrNull($_POST['u_kelas2']);
			$record['keterangan'] = Helper::cStrNull($_POST['u_ket']);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idborang,$r_key);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idborang,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
		
		$keyjur=Helper::removeSpecial($_POST['carijurusan']);
		if($keyjur !='')
		$p_sqlstr .=" and j.kdjurusan='$keyjur' ";
	
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr .=" order by idborang";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}

	if($c_edit){
		$rs_cb1 = $conn->Execute("select namajurusan, kdjurusan from lv_jurusan where f_jur is not null order by namajurusan");
		$l_jurusan = $rs_cb1->GetMenu2('i_jurusan','',true,false,0,'id="i_jurusan" class="ControlStyle" style="width:120"');
		
		$rs_cb1 = $conn->Execute("select namajurusan, kdjurusan from lv_jurusan where f_jur is not null order by namajurusan");
		$l_jurusan2 = $rs_cb1->GetMenu2('carijurusan',$keyjur,true,false,0,'id="carijurusan" class="ControlStyle" style="width:200"');
		
		$rs_cb = $conn->Execute("select namajurusan, kdjurusan from lv_jurusan where f_jur is not null order by namajurusan");
		
		$a_bidang = array('1' => 'MKK', '2' => 'MKDK', '3' => 'MKDU', '4' => 'REFERENSI');
		$l_bidang = UI::createSelect('i_kode',$a_bidang,'','ControlStyle',true,'id="i_kode"');
		
		$a_bidang2 = array('1' => 'MKK', '2' => 'MKDK', '3' => 'MKDU', '4' => 'REFERENSI');
		
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth; ?>">
	<tr height="40">
		<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
	</tr>
</table>
<? include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
	<tr height="20"> 
		<td align="left">
			<a href="javascript:goRefresh()" class="buttonshort"><span class="refresh">Refresh</span></a></td>
			<td align="right" colspan="4">
			<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
			<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
			<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
			<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
		</td>
	</tr>
	<tr>
		<td colspan="3">Pencarian Berdasarkan Jurusan : &nbsp;<?= $l_jurusan2 ?></td>
		<td colspan="2" align="right" ><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>
	</tr>
	<tr height="20"> 
		<td width="120" nowrap align="center" class="SubHeaderBGAlt">Jurusan</td>
		<td width ="70" nowrap align="center" class="SubHeaderBGAlt">Kode</td>
		<td width ="200" nowrap align="center" class="SubHeaderBGAlt">Kelas</td>
		<td width ="300" nowrap align="center" class="SubHeaderBGAlt">Keterangan</td>
		<? if ($c_edit or $c_delete) { ?>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
		<? } ?>
	</tr>
	<?php
		$i = 0;
		while ($row = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($row['idborang'],$p_editkey)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>">
		<td>
		<? if($c_edit) { ?>
			<u title="Sunting Data" onclick="goEditIP('<?= $row['idborang']; ?>');" class="link"><?= $row['namajurusan']; ?></u>
		<? } else { ?>
			<?= $row['namajurusan']; ?>
		<? } ?>
		</td>
		<td align="center"><?= $row['kode']; ?></td>
		<td><?= $row['kelas1'].' - '.$row['kelas2']; ?></td>
		<td><?= $row['keterangan']; ?></td>
		<? if($c_delete or $c_edit) { ?>
		<td align="center">
		
			<u title="Hapus Data" onclick="goDeleteIP('<?= $row['idborang']; ?>','<?= $row['namajurusan'].' ('.trim($row['kelas1']). ' - '.trim($row['kelas2']). ')' ?>');" class="link">[hapus]</u>
		
		</td>
		<? } ?>
	</tr>
	<?php
			} else { // row diupdate
	?>
	<tr class="<?= $rowstyle ?>"> 
		<td align="center"><?= $l_jurusan2 = $rs_cb->GetMenu2('u_jurusan',$row['jur'],true,false,0,'id="u_jurusan" class="ControlStyle" style="width:120"');  ?></td>
		<td align="center"><?= UI::createSelect('u_kode',$a_bidang,trim($row['kode']),'ControlStyle',true,'id="u_kode"'); //UI::createTextBox('u_kode',$row['kode'],'ControlStyle',2,5,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('u_kelas1',$row['kelas1'],'ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?> - <?= UI::createTextBox('u_kelas2',$row['kelas2'],'ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('u_ket',trim($row['keterangan']),'ControlStyle',100,50,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Update Data" onclick="updateData('<?= $row['idborang'] ?>');" class="link">[update]</u>
		<? } ?>
		</td>
	</tr>
	<?php 	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } if($c_add) { ?>
	<tr class="LiteSubHeaderBG"> 		
		<td align="center"><?= $l_jurusan ?></td>
		<td align="center"><?= $l_bidang //UI::createTextBox('i_kode','','ControlStyle',2,5,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('i_kelas1','','ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?> - <?= UI::createTextBox('i_kelas2','','ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('i_ket','','ControlStyle',100,50,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><input type="button" style="cursor:pointer" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
	</tr>
	<?php }  ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr>
</table><br>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="scroll" id="scroll">
</form>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_jurusan').focus();
	<? } else { ?>
	document.getElementById('i_jurusan').focus();
	<? } ?>
}


function insertData() {
	if(cfHighlight("i_jurusan,i_kelas1,i_kelas2")){
		goInsertIP();
		}
}

function updateData(key1,key2) {
	if(cfHighlight("u_jurusan,u_kelas1,u_kelas2")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		document.getElementById('key').value=key1;
		document.getElementById('key2').value=key2;
		document.getElementById('act').value='update';
		goSubmit();
	}
}

function goRefresh(){
document.getElementById("carijurusan").value='';
goSubmit();

}
</script>
</html>