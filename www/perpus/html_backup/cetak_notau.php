<?php
	//$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
		
	// require tambahan
	

	$r_id=Helper::removeSpecial($_GET['id']);
	$r_date=Helper::removeSpecial($_GET['date']);
	
	$pinjam=$conn->Execute("select ideksemplar,noseri,judul,tgltenggat,nopanggil from v_trans_list 
							where kdlokasi='$r_id' and tgltransaksi='$r_date' and tglpengembalian is null and tglperpanjang is null");
	
	$kembali=$conn->Execute("select ideksemplar,noseri,judul,tgltenggat,nopanggil from v_trans_list 
							where kdlokasi='$r_id' and tglpengembalian='$r_date' and statustransaksi='0'");
	
	
	//$reserve=$conn->Execute("select ideksemplarpesan,judul,tglexpired from v_reserve where idanggotapesan='$r_id' and tglreservasi='$r_date' and statusreservasi='1'");
	$unit=$conn->GetRow("select namalokasi from lv_lokasi where kdlokasi='$r_id'");
	
	$user= $conng->GetRow("select userdesc from gate.sc_user where username='".$_SESSION['PERPUS_USER']."'");
?>

<html>
<head>
<title>Nota Transaksi</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onLoad="printpage()">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>PERPUSTAKAAN PJB<br>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="430" border="0" >
	<tr>
		<td width="80">Id Unit</td>
		<td>: <?= $r_id ?></td>
	</tr>
	<tr>
		<td>Unit Kerja</td>
		<td>: <?= $unit['namalokasi'] ?></td>
	</tr><br>
	<tr>
		<td colspan="2"><br>
		<? if($pinjam->fields['ideksemplar']!='') {?>
		<u>PEMINJAMAN PUSTAKA</u>
		<table cellpadding="0" cellspacing="0" width="230" border=0 class="nota">

		<tr height="20">
			<td width="70" style="border-bottom:1px solid">NO SERI</td>
			<td width="80" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="80" style="border-bottom:1px solid">BTS KEMBALI</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rowp = $pinjam->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rowp['noseri']; ?></td>
			<td><?= $rowp['nopanggil'] ?></td>
			<td><?= $rowp['tgltenggat']!='' ? Helper::tglEng($rowp['tgltenggat']) : 'Maksimal' ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rowp['judul'],50) ?></td>
		</tr>
		<?php
		}}?>
		</table><br>
		<? if($kembali->fields['ideksemplar']){ ?>
		<u>PENGEMBALIAN PUSTAKA</u>
			<table cellpadding="0" cellspacing="0" width="230" border=0 class="nota">

		<tr height="20">
			<td width="70" style="border-bottom:1px solid">NO SERI</td>
			<td width="80" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="80" style="border-bottom:1px solid">BTS KEMBALI</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rowk = $kembali->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rowk['noseri']; ?></td>
			<td><?= $rowk['nopanggil'] ?></td>
			<td><?= Helper::tglEng($rowk['tgltenggat']) ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rowk['judul'],50) ?></td>
		</tr>
		<?php
		} }?>
		</table><br>
		</td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td>Terima Kasih | <?= Helper::tglEngStamp(date('d-m-Y H:i:s')) ?><br>
		<?= $user['userdesc'] ?></td>
	</tr>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>