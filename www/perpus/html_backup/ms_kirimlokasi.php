<?php
	//$conn->debug = true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'v_eksopname';
	$p_window = '[PJB LIBRARY] Kirim Ke Lokasi Eksemplar';
	$p_title = 'Kirim Ke Lokasi Eksemplar';
	$p_tbheader = '.: Kirim Ke Lokasi Eksemplar :.';
	$p_col = 9;
	$p_tbwidth = 1120;
	//$p_filelist = Helper::navAddress('list_opname.php');
	$p_id = "list_kirim";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'e.ideksemplar';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	// sql untuk mendapatkan isi list
	
	
	$p_sqlstr="select e.noseri,m.edisi,m.judul,m.nopanggil, k.namakondisi, e.kdlokasi, l.namalokasi
			   from pp_eksemplar e
			   join lv_kondisi k on e.kdkondisi=k.kdkondisi
			   join ms_pustaka m on e.idpustaka=m.idpustaka
			   join lv_lokasi l on e.kdlokasi=l.kdlokasi
			   where ideksemplar=null";
	

	// pengaturan ex
	if (!empty($_POST))
	{
		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$keykondisi=Helper::removeSpecial($_POST['kdkondisi']);
				
		if ($keylokasi!='')
			$p_sqlstr.=" or e.kdlokasi = '$keylokasi'";
	
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi=='kirim'){
			$ideks=Helper::removeSpecial($_POST['txtid']);
			$check=Sipus::Search($conn,pp_eksemplar,noseri,$ideks,ideksemplar);
			if($check!=''){
			$record=array();
			$record['kdlokasi']=$keylokasi;
			$record['kdkondisi']=$keykondisi;
			$err=Sipus::UpdateBiasa($conn,$record,pp_eksemplar,noseri,$ideks);
			
			if($err !=0){
			$errdb = 'Pengiriman pustaka gagal.';	
			Helper::setFlashData('errdb', $errdb);
			}
			else {
			$sucdb = 'Pengiriman Pustaka berhasil.';	
			Helper::setFlashData('sucdb', $sucdb);
			//Helper::redirect();
			}
			}else {
			$errdb = 'Eksemplar tidak ditemukan.';	
			Helper::setFlashData('errdb', $errdb);
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:250" onchange="goSubmit();"');
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$keykondisi,true,false,0,'id="kdkondisi" class="ControlStyle" style="width:250"');
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	
	<table width="95%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

	<tr height="30">
		<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td align="center" colspan="<?= $p_col ?>"><? include_once('_notifikasi.php') ?></td>
	</tr>
	<tr>
	<td width="100%"  colspan="<?= $p_col ?>" align="center">
	
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="170" align="left">
				<a href="javascript:goClear();goFilter(false)" class="buttonshort"><span class="refresh">
				Refresh</span></a></td>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			</table>
			</td>	
	</tr>		
	<!--
	<table cellpadding=0 cellspacing="0" border=0 width="100%">
	

		<td align="right" colspan=3><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>
	</tr>
	
	</table>
	-->
	<tr>
		<td class="LeftColumnBG" colspan=5>
			<table border=0>
				<tr >
					<td><b>Lokasi Di Kirim </b></td><td><b>:</b> <?= $l_lokasi ?>
						<img src="images/popup.png" style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi&code=op',470,450);">
					</td>
				</tr>
				<tr>
					<td><b>Kondisi Terkirim </b></td><td><b>:</b> <?= $l_kondisi ?></td>
					<td><b>NO INDUK </b></td><td><b>:</b> <input type="text" name="txtid" id="txtid" maxlength="20" size="25" onKeyDown="etrKirim(event);"></td>
					<td><input type="button" name="btnkirim" id="btnkirim" value="Kirim" onclick="goKirim()" class="buttonSmall" style="cursor:pointer;height:25;width:120"></td>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="20">
		<td width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO INDUK  <?= $p_xsort['noseri']; ?></td>		
		<td width="40%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.judul:C');">Judul Pustaka <?= $p_xsort['judul']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.nopanggil:C');">No Panggil <?= $p_xsort['nopanggil']; ?></td>
		<td width="20%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','k.namakondisi:C');">Kondisi<?= $p_xsort['namakondisi']; ?></td>
		<td width="35%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi<?= $p_xsort['namalokasi']; ?></td>

		<?php
		$i = 0;

		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
		<td align="left">
		<?= $row['noseri']; ?>
		</td>
		<td align="left"><?= $row['judul'].($row['edisi']!='' ? " / ".$row['edisi'] : ""); ?></td>
		<td ><?= $row['nopanggil']; ?></td>
		<td ><?= $row['namakondisi']; ?></td>
		<td align="left"><?= $row['namalokasi']; ?></td>

		
	</tr>
	<?php
		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr>
</table><br>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="act" id="act">


<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

</form>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function etrKirim(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnkirim").click();
}
function goClear(){
	document.getElementById("kdlokasi").value='';
	document.getElementById("kdkondisi").value='';
	document.getElementById("txtid").value='';
	//goSubmit();
}
function goKirim(){
	if(cfHighlight("kdlokasi,kdkondisi,txtid")) {
	document.getElementById('act').value="kirim";
	goSubmit();
	}
}
</script>
</html>