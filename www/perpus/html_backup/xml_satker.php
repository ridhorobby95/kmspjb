<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// otentiksi user
	Helper::checkRoleAuth($conng);

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and kodeunit in ($units) ";
	
	// menu dengan struktur xml
	$menu = 
	"<response>
		<item>
			<category>Satuan Kerja</category>";
	
	#posisi atas
	$rs = $conn->Execute("select kodeunit idsatker, nama namasatker 
			      from um.unit
			      where 1=1 $sqlAdminUnit 
			      order by nama");
	
	while($row = $rs->FetchRow()) {
		$menu .="<linx>
				<label>".$row['idsatker']." - ".htmlspecialchars($row['namasatker'])."</label>
				<value>".$row['idsatker']."</value>
				<level></level>
				<parent></parent>";
		$menu .="</linx>";
	}

	
	$menu .=
		"</item>
	</response>";

	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	echo $menu;
?>