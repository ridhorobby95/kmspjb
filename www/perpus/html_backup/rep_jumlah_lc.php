<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Statistik Karya Ilmiah';
	
	//definisi akan di-direct ke mana.
	$p_filerep = 'repp_jumlah_lc';
	$p_tbwidth = 520;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	// $a_grafik = array('batang' => 'Grafik Batang','pie' => 'Grafik Pie');
	// $l_grafik = UI::createSelect('grafik',$a_grafik,'','ControlStyle');
	
	$a_semester = array('ganjil' => "Ganjil", 'genap'=>'Genap');
	$l_semester = UI::createSelect('semester',$a_semester,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal=1 order by kdjenispustaka");
	$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'],true,false);
	$l_pustaka = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_pustaka);
	
	$rs_jur = $conn->Execute("select namajurusan,kdjurusan from lv_jurusan order by kdjurusan");
	$l_jurusan = $rs_jur->GetMenu2('kdjurusan','',true,false);
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:520px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Statistik Karya Ilmiah</h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
			<!--tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan Statistik Karya Ilmiah</td></tr-->
			<!--<tr>
				<td class="LeftColumnBG">Semester</td>
				<td class="RightColumnBG"><?//= $l_semester; ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG">Tahun</td>
				<td class="RightColumnBG"><?//= UI::createTextBox('thnakademik1','','ControlStyle',4,4,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?> / <?= UI::createTextBox('thnakademik2','','ControlStyle',4,4,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?> </td>
			</tr>-->
			<tr id="tr_tgl" > 
				<td class="LeftColumnBG thLeft" width="180">Tanggal</td>
				<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
				<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl1",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				s/d &nbsp;
				<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>

				<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl2",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Pustaka</td>
				<td class="RightColumnBG"><?= $l_pustaka; ?></td>
			</tr>
			<!--<tr>
				<td class="LeftColumnBG">Jurusan</td>
				<td class="RightColumnBG"><?= $l_jurusan; ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG"> Grafik</td>
				<td class="RightColumnBG"><?= $l_grafik ?></td>
			</tr>-->
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		* untuk format Word dan Excel tidak akan ikut ditampilkan grafik *
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	// alert ($("#tgl1").val().substr(6,4));
	if(cfHighlight("tgl1,tgl2")){
		if( $("#tgl1").val().substr(6,4) == $("#tgl2").val().substr(6,4))
			goSubmit();
		else
			alert ("Kedua tanggal harus pada tahun yang sama.");
	}
}
</script>
</html>