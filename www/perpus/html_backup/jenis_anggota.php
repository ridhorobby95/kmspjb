<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	
	// definisi variabel halaman
	$p_dbtable = 'lv_jenisanggota';
	$p_window = '[PJB LIBRARY] Daftar Jenis Anggota';
	$p_title = 'Daftar Jenis Anggota';
	$p_tbheader = '.: Daftar Jenis Anggota :.';
	$p_col = 4;
	if($c_edit)
	$p_tbwidth = 100;
	else
	$p_tbwidth = 100;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdjenisanggota";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
				
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['kdjenisanggota'] = Helper::cStrNull($_POST['i_kdjenisanggota']);
			$record['namajenisanggota'] = Helper::cStrNull($_POST['i_namajenisanggota']);
			$record['setpagu'] = Helper::cStrNull($_POST['i_setpagu']);
			$record['canlogin'] = Helper::cNumNull($_POST['i_canlogin']);
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['namajenisanggota'] = Helper::cStrNull($_POST['u_namajenisanggota']);
			$record['setpagu'] = Helper::cStrNull($_POST['u_setpagu']);
			$record['canlogin'] = Helper::cNumNull($_POST['u_canlogin']);
						
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,kdjenisanggota,$r_key);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,kdjenisanggota,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/keanggotaan.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Id Jenis</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Jenis Member</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Login Web</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['kdjenisanggota'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['kdjenisanggota']; ?>');" class="link"><?= $row['kdjenisanggota']; ?></u>
				<? } else { ?>
					<?= $row['kdjenisanggota']; ?>
				<? } ?>
				</td>
				<td><?= $row['namajenisanggota']; ?></td>
				<td align="center"><?= $row['canlogin'] == '1' ? '<img src="images/centang.gif">' : '' ; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['kdjenisanggota']; ?>','<?= $row['namajenisanggota']; ?>');" class="link"><img src="images/deletes.png" width="14" /></u>
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= $row['kdjenisanggota']; ?></td>
				<td align="center"><?= UI::createTextBox('u_namajenisanggota',$row['namajenisanggota'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="u_canlogin" id="u_canlogin" value="1" <?= $row['canlogin']=='1' ? 'checked':''; ?>></td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['kdjenisanggota']; ?>');" class="link"><img src="images/updates.png" /></u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_kdjenisanggota','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_namajenisanggota','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="i_canlogin" id="i_canlogin" value="1" <?= $row['canlogin'] ?> onkeydown="return onlyNumber(event,this,true,true)"></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php } ?>
			<tr>
				<td colspan="7" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div><br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namajenisanggota').focus();
	<? } else { ?>
	document.getElementById('i_kdjenisanggota').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_kdjenisanggota,i_namajenisanggota"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_namajenisanggota,u_namajenisanggota")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>