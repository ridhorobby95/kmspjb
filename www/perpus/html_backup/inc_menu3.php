<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$separator = " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ";
	
	if($_REQUEST["nomenu"] != 1) {  
?>
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery.dimensions.js"></script>
<script type="text/javascript" src="scripts/jquery.positionBy.js"></script>
<script type="text/javascript" src="scripts/jquery.jdMenu.js"></script>
<link rel="stylesheet" href="style/jquery.jdMenu.css" type="text/css">
<div id="bannerlong" style="background-image:url(images/header_fill.jpg); background-repeat:repeat-x">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="300px">
			<img src="images/header_left.jpg">
		</td>
		<td>&nbsp;
		 
		</td>
		<td width="100px">
			<img src="images/header_right.jpg">
		</td>
	</tr>
</table>
</div>
<form name="logform" id="logform" method="post" action="home.php">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr> 
    <td valign="top" colspan="2">
	<div>
		<ul class="jd_menu">
			<li><a href="index.php?page=home">Home</a></li>
			<?	if($_SESSION['PERPUS_ROLE'] == 'admin' or $_SESSION['PERPUS_ROLE'] == 'admsdm') { ?>
			<li>Kepegawaian <img src="images/down_m.gif">
				<ul>
					<li><a href="index.php?page=list_pegawai">Daftar Pegawai</a></li>
					<li><a href="index.php?page=list_angkakredit">Penghitungan Angka Kredit</a></li>
					<li><a href="index.php?page=list_gaji">Penghitungan Gaji</a></li>
					<li><a href="index.php?page=list_diklatns">Diklat NS Kolektif</a></li>
					<li>Daftar Pengajuan <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=list_cuti">Cuti</a></li>
							<li><a href="index.php?page=list_klaim">Klaim</a></li>
							<li><a href="index.php?page=list_perkeplek">Permohonan Keplek</a></li>	
							<li><a href="index.php?page=list_perknama">Permohonan K. Nama</a></li>		
							<li><a href="index.php?page=list_perseragam">Permohonan Seragam</a></li>	
							<li><a href="index.php?page=list_perlainlain">Permohonan Lain-lain</a></li>	
						</ul>
					</li>
					<li>LPPM <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=list_publikasi">Daftar Publikasi</a></li>
							<li><a href="index.php?page=ms_jenispublikasi">Jenis Publikasi</a></li>
							<li><a href="index.php?page=list_bidangkompetensi">Bidang Kompetensi</a></li>
							<li><a href="index.php?page=list_jenispenelitian">Jenis Penelitian</a></li>
							<li><a href="index.php?page=list_kegiatanppm">Kegiatan PPM</a></li>
						</ul>
					</li>
					<li>Rekrutmen <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=list_permintaan">Daftar Permintaan Pegawai</a></li>
							<li><a href="index.php?page=list_calon">Daftar Calon Pegawai</a></li>
							<li><a href="index.php?page=list_lamaran">Daftar Pelamar & Tahap Lamaran</a></li>
						</ul>
					</li>
										
				</ul>
			</li>
			<li>Penilaian Kinerja <img src="images/down_m.gif">
				<ul>
					<li>Karyawan <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=list_periodepa">Periode</a></li>
							<li><a href="index.php?page=list_soalpa">Soal</a></li>
							<li><a href="index.php?page=list_timpenilai">Tim Penilai</a></li>
							<li><a href="index.php?page=list_dinilai">Penilaian</a></li>
							<li><a href="index.php?page=list_hasilpa">Hasil</a></li>
						</ul>
					</li>
					<li>Dosen (QA) <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=ms_qabidang">Bidang Kinerja</a></li>
							<li><a href="index.php?page=ms_qagolongan">Gol. Kinerja</a></li>
							<li><a href="index.php?page=list_qaparameter">Parameter Penilaian</a></li>
							<li><a href="index.php?page=ms_qareward">Reward</a></li>
						</ul>
					</li>
				</ul>
			</li>
			<li>Laporan <img src="images/down_m.gif">
				<ul>
					<li>Angka Kredit <img src="images/left_m.gif">
						<ul>
							<li>Riwayat Akreditasi <img src="images/left_m.gif">
								<ul>
									<li><a href="index.php?page=list_repakreditasi1a">Bidang IA</a></li>
									<li><a href="index.php?page=list_repakreditasi1b">Bidang IB</a></li>
									<li><a href="index.php?page=list_repakreditasi2">Bidang II</a></li>
									<li><a href="index.php?page=list_repakreditasi3">Bidang III</a></li>
									<li><a href="index.php?page=list_repakreditasi4">Bidang IV</a></li>
								</ul>
							</li>
							<li><a href="index.php?page=xrep_akreditasi">Data Perolehan</a></li>
						</ul>
					</li>
					<li><a href="index.php?page=xrep_pegawai">Daftar Pegawai</a></li>
					<li><a href="index.php?page=xrep_lamaran">Daftar Calon Pegawai</a></li>
					<? /* <li>Non-Edukatif <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=xrep_ndpendakhir">Pendidikan</a></li>
						</ul>
					</li>*/?>
					<li><a href="index.php?page=xrep_jeniskelamin">Jenis Kelamin</a></li>
					<li><a href="index.php?page=xrep_pensiun">Pensiun</a></li>
					<li><a href="index.php?page=xrep_pendakhir">Pendidikan</a></li>
				</ul>
			</li>
			<li>Referensi <img src="images/down_m.gif">
				<ul>
					<li><a href="index.php?page=list_satker">Satuan Kerja</a></li>
					<li><a href="index.php?page=list_wilayah">Wilayah</a></li>
					<li><a href="index.php?page=ms_statusaktif">Status Aktif</a></li>
					<li><a href="index.php?page=ms_statuskepeg">Status Pegawai</a></li>
					<li><a href="index.php?page=ms_jenispeg">Jenis Pegawai</a></li>
					<li><a href="index.php?page=ms_jobtitle">Deskripsi Kerja</a></li>		
					<li><a href="index.php?page=ms_jeniskp">Jenis KP</a></li>					
					<li><a href="index.php?page=ms_jfungsional">Jabatan Fungsional</a></li>
					<? /*<li><a href="index.php?page=ms_ljfungsional">Jabatan Fungsional Lokal</a></li> */ ?>
					<li><a href="index.php?page=list_struktural">Jabatan Struktural</a></li>	
					<li>Aktivitas <img src="images/left_m.gif">
						<ul>
						<li><a href="index.php?page=ms_diklats">Diklat Umum</a></li>	
						<li><a href="index.php?page=ms_diklatns">Diklat Khusus</a></li>	
						<li><a href="index.php?page=list_jeniscuti">Jenis Cuti</a></li>		
						<li><a href="index.php?page=list_penilaian">Aturan Penilaian AK</a></li>
						<li><a href="index.php?page=ms_permohonan">Jenis Permohonan</a></li>			
					</ul>
					<li>Penggajian <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=ms_kmk">KMK</a></li>	
							<li><a href="index.php?page=ms_tunjangan">Jenis Tunjangan</a></li>
							<li><a href="index.php?page=list_tariftunjangan">Tarif Tunjangan</a></li>
							<li><a href="index.php?page=ms_rgp">Ruang Gaji Pokok</a></li>	
							<li><a href="index.php?page=list_standargaji">Standar Gaji</a></li>											
						</ul>
					</li>
					<li>Pelengkap <img src="images/left_m.gif">
						<ul>
							<li><a href="index.php?page=ms_agama">Data Agama</a></li>
							<li><a href="index.php?page=ms_bahasa">Data Bahasa</a></li>
							<li><a href="index.php?page=ms_pendidikan">Pendidikan</a></li>
							<li><a href="index.php?page=ms_bank">Bank</a></li>
							<? /*<li><a href="index.php?page=ms_bidang">Bidang</a></li>		*/ ?>					
						</ul>
					</li>
					<li><a href="index.php?page=list_pelanggaran">Pelanggaran</a></li>								
					<li><a href="index.php?page=ms_sanksi">Sanksi</a></li>					
				</ul>
			</li>
			<?	} ?>
			<li><font color="yellow">Keluar </font><img src="images/down_m.gif">
				<ul>
					<li><a href="<?= $_SESSION['menupage']; ?>">Kembali ke Menu</a></li>
					<li><a href="index.php?page=sys_logout"><font color="yellow">Logout</font></a></li>
				</ul>
			</li>
		</ul>
	</div>
	</td>
</tr>
<tr height="20" style="font-family: sans-serif, Tahoma; background-color:#ece9d8;">
	<td align="right" nowrap style="padding-right:5;color:#666">
		User ID: <?= $_SESSION['PERPUS_USER'].$separator; ?>
		Hak Akses: <?= $_SESSION['PERPUS_NAMAROLE'].' - '.$_SESSION['PERPUS_NAMASATKER'].$separator; ?>
		Login Terakhir: <?= Helper::formatDateTime($_SESSION['PERPUS_LASTLOG']); ?>
	</td>
</tr>

</table>
</form>

<?php } else { 
// Start Menu Section =======================================================================
?>

<table width="30" align="center" border=1 cellspacing=0 cellpadding="0" class="GridStyle" bgcolor="#999999">
  <tr> 
    <td width="20%" height="29" align="left" nowrap>
	<button onClick="window.close();" class="OuButton" onMouseOver="this.className='OvButton'" onMouseOut="this.className='OuButton'" style="cursor:pointer;height:30px;width:100px;font-size:14px;">
	<img src="images/bdel.gif"> Tutup</button>
	</td>
  </tr>
</table>
<br>

<? 
 // End No Menu Section ======================================================================
} ?>