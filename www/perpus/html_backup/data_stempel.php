<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	// otentiksi user
	Helper::checkRoleAuth($conng);
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$p_filelist = Helper::navAddress('list_pagu.php');
	$p_title = "Upload File TTD dan Stempel";
	
	$p_foto = Config::stempelUrl.'stempel.jpg';
	$p_hfoto = $p_foto;
	
	if(!empty($_POST)){
		// echo $_FILES['foto']['tmp_name'];
		// exit;
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == "simpanfoto" and $c_edit) {
			if($_FILES['foto']['error'] == UPLOAD_ERR_OK)
				$sfok = UI::createFoto($_FILES['foto']['tmp_name'],$p_foto,500,500);
			else if($_FILES['foto']['error'] == UPLOAD_ERR_INI_SIZE or $_FILES['foto']['error'] == UPLOAD_ERR_FORM_SIZE)
				$sfok = -4; // ukuran file melebihi batas
			else if($_FILES['foto']['error'] == UPLOAD_ERR_NO_FILE)
				$sfok = -5; // tidak ada file yang diupload
			
			@unlink($_FILES['foto']['tmp_name']); // hapus dan jangan tampilkan pesan error bila error
			
			switch($sfok) {
				case -1: $uploadmsg = 'Format file gambar tidak dikenali.'; break;
				case -2: $uploadmsg = 'Format file gambar harus GIF, JPEG, atau PNG.'; break;
				case -3: $uploadmsg = 'File gambar tidak bisa diupload.'; break;
				case -4: $uploadmsg = 'Ukuran file gambar melebihi batas maksimal.'; break;
				case -5: $uploadmsg = 'Tidak ada file gambar yang di-upload.'; break;
				default: $uploadmsg = 'Upload gambar berhasil.'; break;
			}
			
			if($sfok < 0)
				$uploadmsg = UI::message($uploadmsg,true);
			else
				$uploadmsg = UI::message($uploadmsg);
		}
		else if($r_aksi == "hapusfoto" and $c_edit) {
			$dfok = unlink($p_foto);
			
			if($dfok)
				$uploadmsg = UI::message("Penghapusan gambar berhasil.");
			else
				$uploadmsg = UI::message("Penghapusan gambar tidak berhasil.",true);
		}
		
		// if(!empty($uploadmsg)) {
			// // html ditulis di iframe "upload_iframe"
			// echo '<html><body><script type="text/javascript">'."\n";
			// echo 'var parentdoc = window.parent.document;'."\n";
			// echo "parentdoc.getElementById('uploadmsg').innerHTML = '$uploadmsg';\n";
			// echo 'parent.$("#imgfoto").waitload({mode: "unload"});'."\n";
			
			// if($sfok > 0 or $dfok)
				// echo 'parentdoc.getElementById("imgfoto").src = "'.(is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default.jpg').'?'.mt_rand(1000,9999).'";'."\n";
			
			// echo '</script></body></html>';
			
			// exit();
		// }
		
	}
	
?>

<html>
<head>
	<title>[PJB LIBRARY] Setting TTD dan Stempel</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link rel="shortcut icon" href="images/favicon.ico" />
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<?/*?>
<table width="100">
	<tr>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<?}?>
	</tr>
</table>
<?*/?>
<?php echo $uploadmsg; ?>
<br><br>
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr>
		<td width="<?= $p_tbwidth ?>" class="SubHeaderBGAlt" align="center" colspan="4">Upload File TTD dan Stempel</td>
	</tr>
	
<tr>
	<td valign="top" align="center" rowspan="6"> 
			<div id="uploadmsg"></div>
			<img border="1" id="imgfoto" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default.jpg') ?>?<?= mt_rand(1000,9999) ?>" width="180" height="120"><br>
			<input type="hidden" name="MAX_FILE_SIZE" value="10000000">
			<input type="file" name="foto" class="ControlStyle" size="15">
			<br><br><input type="button" value="Simpan" class="ControlStyle" onClick="aSavePhoto();"> 
			<input type="button" value="Hapus" class="ControlStyle" onClick="aDelPhoto();">
			</td>
</tr>


<iframe name="upload_iframe" style="display:none;"></iframe>
<input type="hidden" name="act" id="act">
<input type="hidden" name="delfile" id="delfile">

</table>

</form>
</div>
</body>

<script language="javascript">
function aSavePhoto(){
		document.getElementById('act').value="simpanfoto";
		goSubmit();
}

function aDelPhoto(){
	var hapusfoto=confirm("Apakah Anda yakin akan menghapus gambar ini ?");
	if (hapusfoto){
		document.getElementById('act').value="hapusfoto";
		goSubmit();
	}

}
</script>

</html>