<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_id = $_REQUEST['id'];
	
	if($r_id=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Surat Tagih';
	
	$sql = "select * from pp_tagihan where idtagihan=$r_id";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Surat Tagih</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="100" align="center"><strong>Tanggal Tagihan</strong></th>
    <th width="80" align="center"><strong>Id Anggota</strong></th>
    <th width="80" align="center"><strong>Tagihan Ke</strong></th>
	<th width="100" align="center"><strong>Tanggal Expired</strong></th>
	<th width="150" align="center"><strong>Keterangan</strong></th>
   </tr>
  <?php
	$n=0;
	while($rs=$row->FetchRow()) 
	{  	$n++;
	$ket=explode("]",$rs['keterangan']);
	?>
    <tr height=25>
	<td align="center"><?= $n ?></td>
    <td align="center"><?= Helper::tglEng($rs['tgltagihan']) ?></td>
	<td align="center"><?= $rs['idanggota'] ?></td>
	<td align="center"><?= $rs['tagihanke'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglexpired']) ?></td>
	<td><?= $ket[0]."]<br>".$ket[1] ?></td>
  </tr>
	<?  } ?>
	<? if($rsc==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak Sirkulasi</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=5><b>Jumlah Tagihan : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>