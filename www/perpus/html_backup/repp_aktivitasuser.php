<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_thn = Helper::removeSpecial($_POST['tahun']);
	$r_unit = Helper::removeSpecial($_POST['kdjurusan']);
	$r_bln = Helper::removeSpecial($_POST['bulan']);

	if($r_format=='' or $r_thn==''){
		header("location: index.php?page=home");
	}
//var_dump($sekarang);
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Aktivitas Pengunjung Perpustakaan PJB <br>'.$r_bln.$r_thn ;
	
	$p_namafile = 'lap_sirkualsi_per_bulan_'.$r_bln.$r_thn;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select * 
			from 
			(
				/*peminjaman*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd')) tgl, 'pinjam' jenis 
				from pp_transaksi t 
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tgltransaksi,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 1 and t.perpanjangke is null 
				and coalesce(to_char(b.tanggal,'mm'),to_char(t.tgltransaksi,'mm')) = '$r_bln'
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tgltransaksi,'YYYY')) = '$r_thn'
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd'))
				union
				/*pengembalian*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd')) tgl, 'kembali' jenis 
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglpengembalian,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 0 and t.perpanjangke is null 
				and coalesce(to_char(b.tanggal,'mm'),to_char(t.tglpengembalian,'mm')) = '$r_bln' 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglpengembalian,'YYYY')) = '$r_thn'
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd'))
				union
				/*perpanjangan*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd')) tgl, 'panjang' jenis 
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglperpanjang,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 1 and t.perpanjangke is not null 
				and coalesce(to_char(b.tanggal,'mm'),to_char(t.tglperpanjang,'mm')) = '$r_bln' 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglperpanjang,'YYYY')) = '$r_thn' 
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd'))
				union 
				/*berkunjung*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, to_char(b.tanggal,'YYYY-mm-dd') tgl, 'kunjung' jenis 
				from pp_bukutamu b  
				left join pp_transaksi t on t.idanggota = b.idanggota 
				join ms_anggota a on a.idanggota = b.idanggota 
				where t.idtransaksi is null 
				and to_char(b.tanggal,'mm') = '$r_bln' 
				and to_char(b.tanggal,'YYYY') = '$r_thn' 
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, to_char(b.tanggal,'YYYY-mm-dd')

			) 
			where 1=1  ";
	
	if(!empty($r_unit)){
		$sql .= " and idunit = '$r_unit' ";
	}

	$sql .= " order by kdjenisanggota, tgl, namaanggota ";

	$rs = $conn->Execute($sql);
	while($row=$rs->FetchRow()){
		$data['data'][$row['kdjenisanggota']][] = $row;
	}

	
	##################################
	$t_sql = "select * 
			from 
			(
				/*peminjaman*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd')) tgl, 'pinjam' jenis 
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tgltransaksi,'mm')) bln
				from pp_transaksi t 
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tgltransaksi,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 1 and t.perpanjangke is null 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tgltransaksi,'YYYY')) = '$r_thn'
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd'))
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tgltransaksi,'mm'))
				union
				/*pengembalian*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd')) tgl, 'kembali' jenis 
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tglpengembalian,'mm')) bln 
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglpengembalian,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 0 and t.perpanjangke is null 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglpengembalian,'YYYY')) = '$r_thn'
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd'))
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tglpengembalian,'mm'))
				union
				/*perpanjangan*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd')) tgl, 'panjang' jenis 
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tglperpanjang,'mm')) bln
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglperpanjang,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				where t.statustransaksi = 1 and t.perpanjangke is not null 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglperpanjang,'YYYY')) = '$r_thn' 
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd'))
				, coalesce(to_char(b.tanggal,'mm'),to_char(t.tglperpanjang,'mm'))
				union 
				/*berkunjung*/
				select a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, to_char(b.tanggal,'YYYY-mm-dd') tgl, 'kunjung' jenis 
				, to_char(b.tanggal,'mm') bln
				from pp_bukutamu b  
				left join pp_transaksi t on t.idanggota = b.idanggota 
				join ms_anggota a on a.idanggota = b.idanggota 
				where t.idtransaksi is null 
				and to_char(b.tanggal,'YYYY') = '$r_thn' 
				group by a.kdjenisanggota, a.idunit, a.idanggota, a.namaanggota, to_char(b.tanggal,'YYYY-mm-dd'), to_char(b.tanggal,'mm')

			) 
			where 1=1  ";
	
	if(!empty($r_unit)){
		$t_sql .= " and idunit = '$r_unit' ";
	}

	$t_sql .= " order by kdjenisanggota, tgl, namaanggota ";
	$t_rs = $conn->Execute($t_sql);
	while($t_row=$t_rs->FetchRow()){
		$data['total'][(int)$t_row['bln']][$t_row['kdjenisanggota']][] = $row;
	}

	$satker = "";
	if(!empty($r_unit)){
		$satker .= $conn->GetOne("select namasatker from ms_satker where kdsatker = '$r_unit' ");
		$judul = "Laporan Aktivitas Pengunjung <br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn."<br/>".$satker;
	}else{
		$judul = "Laporan Aktivitas Pengunjung <br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;
	}
	
	$j_sql = "select * from lv_jenisanggota order by kdjenisanggota ";
	$jas = $conn->GetArray($j_sql);
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/font-awesome.css">	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=999>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<br><br>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2><?= $judul; ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="1000" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="right"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>
	<table width="1150" cellspacing="0" cellpadding="4" border="0">
		<tr><td align="left" colspan=2><h3>A. PENGELOLAAN PERPUSTAKAAN</h3></td></tr>
		<tr>
			<td width="10"></td>
			<td align="left"><h3>A.1.  Daftar Absensi Pengunjung Perpustakaan</h3></td></tr>
	</table>
	
	<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
		<tr height="25" style="background-color: #000000; color: #FFFFFF; font-weight: bold;">
			<td width="10" align="center">NO</td>
			<td align="center">NAMA</td>
			<td align="center">NID</td>
			<td width="50" align="center">KEGIATAN</td>
		</tr>
	<? 
	foreach($jas as $ja){
	?>
		<tr height="25" style="background-color: #000000; color: #FFFFFF;">
			<td colspan=4 align="center"><b><?=strtoupper($ja['namajenisanggota']);?></b></td>
		</tr>
		<?	if(count($data['data'][$ja['kdjenisanggota']])>0){
			$i=1;
			foreach($data['data'][$ja['kdjenisanggota']] as $kda){

			if($kda['jenis'] == "pinjam"){
				$jenis = "Meminjam";
			}elseif($kda['jenis'] == "kembali"){
				$jenis = "Mengembalikan";
			}elseif($kda['jenis'] == "panjang"){
				$jenis = "Memperpanjang";
			}elseif($kda['jenis'] == "kunjung"){
				$jenis = "Berkunjung";
			}
		?>
		<tr height="25">
			<td align="center"><?=$i;?></td>
			<td><?=$kda['namaanggota'];?></td>
			<td><?=$kda['idanggota'];?></td>
			<!--<td><?=$jenis;?></td>-->
			<td  width="250">
			<table cellspacing="0" border="1">
				<th style="background-color: #fff; color: #000;">Baca</th>
				<th style="background-color: #fff; color: #000;">Sharing</th>
				<th style="background-color: #fff; color: #000;">Pinjam Buku</th>
				<th style="background-color: #fff; color: #000;">Scan</th>
				<th style="background-color: #fff; color: #000;">Lain-lain</th>
				
				<?php
				$ida=$kda['idanggota'];
				$sqla="select b.baca, b.sharing, b.pinjam_buku, b.scan, b.lainlain from pp_bukutamu b where b.idanggota='$ida'";
				$ak = $conn->Execute($sqla);

				$baca = $conn->GetOne("select count(b.baca) from pp_bukutamu b where b.idanggota='$ida' and b.baca=1");
				$sharing = $conn->GetOne("select count(b.sharing) from pp_bukutamu b where b.idanggota='$ida' and b.sharing=1");
				$pinjam = $conn->GetOne("select count(b.pinjam_buku) from pp_bukutamu b where b.idanggota='$ida' and b.pinjam_buku=1");
				$scan = $conn->GetOne("select count(b.scan) from pp_bukutamu b where b.idanggota='$ida' and b.scan=1");
				$lainlain = $conn->GetOne("select count(b.lainlain) from pp_bukutamu b where b.idanggota='$ida' and b.lainlain=1");
				//var_dump($baca);	
				//while($val=$ak->FetchRow()){
				//echo "<pre>";var_dump($val);
				?>
				<tr>

					<td align="center"><?= $baca ? $baca : '' ?></td>
					<td align="center"><?= $sharing ? $sharing : '' ?></td>
					<td align="center"><?= $pinjam ? $pinjam : '' ?></td>
					<td align="center"><?= $scan ? $scan : '' ?></td>
					<td align="center"><?= $lainlain ? $lainlain : '' ?></td>
				</tr>
				<?php 
				//} 
				?>
			</table>
			</td>
			
		</tr>
		<?		
			$i++;}
			}else{
		?>
		<tr height="25">
			<td colspan=4 align="center">tidak ada data</td>
		</tr>
		<?		
			}
		?>
		
		<tr height="25" style="background-color: #A9F5F2; ">
			<td colspan=3 align="center"><b>JUMLAH</b></td>
			<td align="right"><b><?=strtoupper(count($data['data'][$ja['kdjenisanggota']]));?></b></td>
		</tr>
		
		<tr height="25" style="border-left: hidden; border-right: hidden; border-bottom: hidden;"><td colspan=4></td></tr>

	<?
	}
	$mbln = date('n');
	$tja = count($jas);
	?>
	
	

	</table>
	
	<table width="1150" cellspacing="0" cellpadding="4" border="0">
		<tr><td colspan="2">
		<table width="1150" cellspacing="0" cellpadding="4" border="1">
		<tr height="25" style="background-color: #000000; color: #FFFFFF; font-weight: bold;">
			<td width="10" rowspan="2" align="center">NO</td>
			<td rowspan="2" align="center">BULAN</td>
			<td colspan="<?=$tja;?>" align="center">JUMLAH PENGUNJUNG</td>
		</tr>
		<tr style="background-color: #000000; color: #FFFFFF; font-weight: bold;">
		<?foreach($jas as $ja){?>
			<td align="center"><?=strtoupper($ja['namajenisanggota']);?></td>
		<?}?>
		</tr>
	<?
	for($i=1;$i<=$mbln;$i++){
	?>
		<tr>
			<td width="10" align="center"><?=$i;?></td>
			<td width="30"><?=Helper::bulanInd($i);?></td>
		<?foreach($jas as $ja){?>
			<td align="right"><?=count($data['total'][$i][$ja['kdjenisanggota']]);?></td>
		<?}?>
		</tr>
	<?
	}
	?>
		</table>
		</td></tr>
		<tr>
			<td width="10"></td>
			<td align="left"><br/><br/><h3>A.3.  Statistik / Chart / Grafik Jumlah Pengunjung Perpustakaan Tiap Bulan.</h3></td></tr>
	</table>
	
	<table width="1150" cellspacing="0" cellpadding="4" border="0">
	<tr><td width="10"></td><td width="30%">
	<table width="300" border="1">
	<?
		foreach($jas as $ja){
			$tot = $tot + count($data['data'][$ja['kdjenisanggota']]);
	?>		
		<tr>
			<td width="220"><?=$ja['namajenisanggota'];?></td>
			<td width="80" align="center"><?=count($data['data'][$ja['kdjenisanggota']]);?></td>
		</tr>
	<?		
		}
	?>
		<tr>
			<td width="220" align="right"><b>Total</b>&nbsp;&nbsp; </td>
			<td width="80" align="center"><b><?=$tot;?></b></td>
		</tr>
	</table>
	</td>
	<td></td></tr>
	</table>
	
	<br/><br/><br/>

<br><br>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>