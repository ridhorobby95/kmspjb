<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and (pu.idunit in ($units) or a.idunit in ($units)) ";
		
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Proses Validasi Pepustakaan';
	$p_title = 'Proses Validasi Perpustakaan';
	$p_tbheader = '.: Daftar Pengadaan :.';
	$p_col = 7;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('data_pgvalperpus.php');
	$p_id = "pengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idpengadaan';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from $p_dbtable where statuspengadaan='PP'";
	$p_sqlstr2="select pu.idunit, pp.*
		from pp_pengadaan pp
		left join pp_orderpustaka po on po.idpengadaan=pp.idpengadaan
		left join pp_usul pu on pu.idusulan=po.idusulan
		left join ms_anggota a on a.idanggota = pu.idanggota 
		where statuspengadaan='PP' $sqlAdminUnit ";
	$rs_unit = $conn->GetRow($p_sqlstr2);
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$id=Helper::removeSpecial($_POST['id']);
		if ($r_aksi=='filter') {
			$filt=Helper::removeSpecial($_POST['txtid']);
			if($filt!='')
				$p_sqlstr.=" and nopengadaan like '%$filt%'";
		
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
	<link rel="stylesheet" href="style/jquery.treeview.css">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div class="filterTable table-responsive">
			<table width="100%"border="0">
				<tr>
					<td class="thLeft" width="150" style="border:0 none;">Masukkan Id Pengadaan </td>
					<td class="thLeft" style="border:0 none;">: &nbsp; <input type="text" name="txtid" id="txtid" maxlength="20" value="<?= $filt ?>"  onkeydown="etrFil(event);"></td>
					<td  align="right"><input type="button" value="Filter" class="ControlStyle" onclick="goFilt()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRef();" /></td>
				</tr>
			</table>
		</div>
		<br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			<tr height="20"> 
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','idpengadaan:N');">No Pengadaan<?= $p_xsort['idpengadaan']; ?></td>		
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','tglpengadaan:D');">Tanggal<?= $p_xsort['tglpengadaan']; ?></td>	
				<td width="60" nowrap align="center" class="SubHeaderBGAlt thLeft">Tahun</td>
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Jenis Pengadaan</td>
				<td width="170" nowrap align="center" class="SubHeaderBGAlt thLeft" width="40%">Keterangan</td>
				<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Status</td>
				
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
						if ($row['idusulan'] == '0') $rowstyle = 'YellowBG';
			?>
			</tr>
			<tr class="<?= $rowstyle ?>" height=20> 
				<td align="center" nowrap="nowrap"><u onClick="goDetail('<?= $p_filedetail; ?>','<?= $row['idpengadaan']; ?>');"  title="Detail Pengadaan" class="link"><?= $row['nopengadaan']; ?></u></td>	
				<td align="center">	<?= Helper::tglEng($row['tglpengadaan'],false); ?></td>	
				<td align="center"><?= substr($row['tglpengadaan'],0,4); ?></td>
				<?if(!empty($rs_unit['idunit'])){?>
					<td align="center">Unit</td>	
				<?}else{?>
					<td align="center">Biasa</td>
				<?}?>
				<td align="left"><?= $row['keterangan']?></td>
				<td align="center"><?= $row['statuspengadaan']?></td>
			</tr>
			<?php
					}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
			</tr>
		</table>
                </div>
		<?php require_once('inc_listnav.php'); ?><br>

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>

		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		</form>

		</div>
	</div>
</div>


</body>

<script language="javascript">
var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

function goFilt(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function etrFil(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goFilt();
}
function goRef() {
	document.getElementById("act").value="";
	document.getElementById("txtid").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

</script>
</html>