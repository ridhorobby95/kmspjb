<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// otentiksi user
	Helper::checkRoleAuth($conng);
	
	if(!empty($_POST)){
		$tgl1=Helper::removeSpecial($_POST['tglmulai']);
		$tgl2=Helper::removeSpecial($_POST['tglselesai']);
//		echo "tgl1". $tgl1;
	//	echo "tgl2". $tgl2;
		$record=array();
		$tgl=strtotime($tgl1);
		//echo "strtotime". $tgl."<br>";
		while ($tgl<=strtotime($tgl2)) {
		$tgllib=date("d-m-Y",$tgl);
		//echo $tgllib. "<br>";
		$record['tgllibur']=Helper::formatDate($tgllib);
		$record['namaliburan']=Helper::removeSpecial($_POST['namalibur']);
		$record['keterangan']=Helper::removeSpecial($_POST['keterangan']);
		Helper::Identitas($record);
		$err=Sipus::InsertBiasa($conn,$record,ms_libur);
		//echo $tgl;
		$tgl=$tgl+(86400);
		
		}
	}

?>
<html>
<head>
	<title>Input Libur Spesial</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/jquery.common.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.opener.location.reload(); ;" >
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="">
		<br>
		<header style="width:430px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<!--img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" /-->
					<h1>Input Hari Libur Spesial</h1>
				</div>
			</div>
		</header>
		<table width="430" cellspacing="1" class="GridStyle">
		<!--tr>
			<th colspan="2">.:: Input Hari Libur Spesial ::.</th>
		</tr-->
		<tr>
			<td class="LeftColumnBG thLeft" width="150">Tanggal Mulai</td>
			<td><input type="text" id="tglmulai" name="tglmulai" size="20">

				<img src="images/cal.png" id="tgllibur1" style="cursor:pointer;" title="Pilih tanggal libur">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglmulai",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgllibur1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
			</td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft">Tanggal Selesai</td>
			<td>
			<input type="text" id="tglselesai" name="tglselesai" size="20">

				<img src="images/cal.png" id="tgllibur2" style="cursor:pointer;" title="Pilih tanggal libur">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglselesai",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgllibur2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
			</td>
		</tr>	
		<tr>
			<td class="LeftColumnBG thLeft">Nama Libur</td>
			<td><input type="text" id="namalibur" name="namalibur" size="40"></td>
		</tr>
		<tr>
			<td class="LeftColumnBG thLeft">Keterangan</td>
			<td><?= UI::createTextArea('keterangan','','ControlStyle',3,37,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		</tr>
		<tr>
			<th colspan="2" align="center"><input type="submit" name="simpan" value="Simpan">&nbsp;<input type="reset"  name="reset" value="Reset&nbsp;"></th>
		</tr>
		<tr>
			<td class="footBG" colspan="2">&nbsp;</td>
		</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>
</html>
