<?php
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// koneksi ke sdm
	$conns = Factory::getConnSdm();
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);


	// definisi variabel halaman
	$p_dbtable = 'pp_sumbangan';
	$p_window = '[PJB LIBRARY] Pustaka Sumbangan';
	$p_title = 'Pustaka Sumbangan';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_sumbangan.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		$r_key3 = Helper::removeSpecial($_REQUEST['key3']);
		
		if($r_aksi == 'simpansumb' and $c_edit) {
				
				$p_id=$conn->GetRow("select max(idsumbangan) as maxid from pp_sumbangan");
				$maxid=$p_id['maxid'] + 1;
				
				$record = array();
				$record = Helper::cStrFill($_POST);
				
				if($r_key == '') { // insert record	
				$record['idsumbangan']=$maxid;
				$record['idanggota']=Helper::cStrNull($_POST['idanggota']);
				$record['namapenyumbang']=Helper::cStrNull($_POST['namapengusul']);
				$record['idunit']=Helper::cStrNull($_POST['idsatker']);
				$record['tglsumbangan']=Helper::formatDate($_POST['tglsumbangan'])." ".date('H:i:s');
				$record['npksumbangan']=$_SESSION['PERPUS_USER'];

				//Helper::Identitas($record);
				
				// input pp_usul
				Sipus::InsertBiasa($conn,$record,pp_sumbangan);

				}
				else { // update record
				$record['idanggota']=Helper::cStrNull($_POST['idanggota']);
				$record['namapenyumbang']=Helper::cStrNull($_POST['namapengusul']);
				$record['idunit']=Helper::cStrNull($_POST['idsatker']);
				$record['tglsumbangan']=Helper::formatDate($_POST['tglsumbangan'])." ".date('H:i:s');
				
				Sipus::UpdateBiasa($conn,$record,pp_sumbangan,idsumbangan,$r_key);
				}	
				
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				
				$r_key = $record['idsumbangan'];
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
				$r_key = $record['idusulan'];
				}
				else {
				$row = $_POST; // direstore
				$p_errdb = true;
				}
		}
		else if($r_aksi == 'insersi' and $c_add) {
			$record = array();

			$record['idsumbangan']=$r_key;
			$record['judul'] = Helper::removeSpecial($_POST['i_judul']);
			$record['authorfirst1'] = Helper::removeSpecial($_POST['i_authorfirst1']);
			$record['authorlast1'] = Helper::cStrNull(strtoupper($_POST['i_authorlast1']));
			$record['authorfirst2'] = Helper::cStrNull($_POST['i_authorfirst2']);
			$record['authorlast2'] = Helper::cStrNull(strtoupper($_POST['i_authorlast2']));
			$record['authorfirst3'] = Helper::cStrNull($_POST['i_authorfirst3']);
			$record['authorlast3'] = Helper::cStrNull(strtoupper($_POST['i_authorlast3']));
			$record['penerbit'] = Helper::cStrNull($_POST['i_penerbit']);
			$record['tahunterbit'] = Helper::cStrNull($_POST['i_tahun']);
			$record['qtysumbangan'] = Helper::cStrNull($_POST['i_qty']);
			$record['stssumbangan'] = 1;
			$record['qtyttb'] = Helper::cStrNull($_POST['i_qty']);
			$record['ststtb'] = 1;
			
			//Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::InsertBiasa($conn,$record,pp_orderpustaka);
			
			$idcheck=Helper::removeSpecial($_POST['idttbe']);
			if($idcheck==''){
			//no ttb
			$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
			$slice=explode("/",$getttb['ttb']);
			$isttb=$slice[0]+1;
			$nottb=Helper::strPad($isttb,4,0,'l')."/TTP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y');
			$idttb=Sipus::GetLast($conn,pp_ttb,idttb);
			
			$recttb['idttb']=$idttb;
			$recttb['nottb']=$nottb;
			$recttb['tglttb']=date('Y-m-d');
			$recttb['npkttb']=$_SESSION['PERPUS_USER'];
			$recttb['jnsttb']=2;
			
			Sipus::InsertBiasa($conn,$recttb,pp_ttb);
			$ttb=$recttb['idttb'];
			}else
			$ttb=$idcheck;
			
			$recdttb['idorderpustaka']=$conn->GetOne("select max(idorderpustaka) from pp_orderpustaka ");
			$recdttb['idttb']=$ttb;
			$recdttb['qtyttbdetail']=Helper::cStrNull($_POST['i_qty']);
			$recdttb['ststtbdetail']=1;			
			Sipus::InsertBiasa($conn,$recdttb,pp_orderpustakattb);
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo()!= 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['judul'] = Helper::removeSpecial($_POST['u_judul']);
			$record['authorfirst1'] = Helper::removeSpecial($_POST['u_authorfirst1']);
			$record['authorlast1'] = Helper::cStrNull($_POST['u_authorlast1']);
			$record['authorfirst2'] = Helper::cStrNull($_POST['u_authorfirst2']);
			$record['authorlast2'] = Helper::cStrNull($_POST['u_authorlast2']);
			$record['authorfirst3'] = Helper::cStrNull($_POST['u_authorfirst3']);
			$record['authorlast3'] = Helper::cStrNull($_POST['u_authorlast3']);
			$record['penerbit'] = Helper::cStrNull($_POST['u_penerbit']);
			$record['tahunterbit'] = Helper::cStrNull($_POST['u_tahun']);
			$record['qtysumbangan'] = Helper::cStrNull($_POST['u_qty']);
			$record['qtyttb'] = Helper::cStrNull($_POST['u_qty']);
			$record['ststtb'] = 1;
			//Helper::Identitas($record);
						
			$conn->StartTrans();
			Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idorderpustaka,$r_key2);

			$recdttb['qtyttbdetail']=Helper::cStrNull($_POST['u_qty']);
			Sipus::UpdateBiasa($conn,$recdttb,pp_orderpustakattb,idorderpustakattb,$r_key3);
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,pp_orderpustaka,idorderpustaka,$r_key2);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key2;
			$p_editkey2 = $r_key3;
		}
		
	}
	if ($r_key!='') {
		$p_sqlstr = "select idsumbangan, idanggota, idunit, namapenyumbang, to_char(tglsumbangan, 'dd-mm-YYYY') as tglsumbangan, npksumbangan, to_char(keterangan) keterangan from pp_sumbangan
					where idsumbangan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);		
		$rs=$conn->Execute("select o.*,d.idttb,d.idorderpustakattb from pp_orderpustaka o
							left join pp_orderpustakattb d on o.idorderpustaka=d.idorderpustaka
							where o.idsumbangan='$r_key' order by to_char(o.judul) ");
	}
	
	$namaunit=Sipus::NamaUnit($conn);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile ?>"  enctype="multipart/form-data">
		<!--table width="<?//= $p_tbwidth ?>">
			<tr height="30">
				<td align="center" class="PageTitle"><?//= $p_title ?></td>
			</tr>
		</table-->
		<!-- ================================================ sumbangan ============================================ -->

		<table width="100" class="GridStyle">
			<tr>
			<? if($c_readlist) { ?>
			<td class="thLeft" align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Sumbangan</span></a>
			</td>
			<? } if($c_edit) { ?>

			<td class="thLeft" align="center">
				<a href="javascript:goSaveSumb();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td class="thLeft" align="center">
				<? if($r_key =='') { ?>
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
				<? } else { ?>
				<a href="javascript:goPrint('<?= $r_key ?>');" class="button"><span class="print">Cetak Bukti</span></a>
				<? } ?>
			</td>
			<? } if($c_delete and $r_key != '') { ?>
			<td class="thLeft" align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br>
		<center>
		<?php include_once('_notifikasi.php'); ?>
		<header style="width:600px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" align="center" cellspacing=0 cellpadding="6" class="GridStyle">
			<tr height=25> 
				<td class="LeftColumnBG thLeft" width="180">Id Anggota</td>
				<td width="370"><b><?= UI::createTextBox('idanggota',$row['idanggota'],'ControlStyle',20,20,$c_edit); ?></b>
				<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=us',620,500);">
					<? } ?>
				</td>
				
			</tr><input type="hidden" name="idsumbangan" id="idsumbangan" value="<?= $row['idsumbangan'] ?>">
			
			<tr height=25> 
				<td class="LeftColumnBG thLeft" height="30">Nama Penyumbang</td>
				<td><b><?= UI::createTextBox('namapengusul',$row['namapenyumbang'],'ControlStyle',50,50,$c_edit); ?></b></td>
			</tr>
			</tr>
				
				<td class="LeftColumnBG thLeft" width="180">Id Unit</td>
				<td><b><?= UI::createTextBox('namasatker',$namaunit[$row['idunit']],'ControlStyle',20,20,$c_edit); ?></b>
				<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_satker',620,500);">
					<? } ?>
				<input type="hidden" id="idsatker" name="idsatker" value="<?= $row['idunit'] ?>">
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft" height="30">Tanggal</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tglsumbangan',$r_key !='' ? $row['tglsumbangan'] : date('d-m-Y'),'ControlStyle',10,10,$c_edit); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleusumb" style="cursor:pointer;" title="Pilih tanggal sumbangan">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglsumbangan",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleusumb",
					align          :    "Br",
					singleClick    :    true
				});
				</script><? } ?>
				</td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft" width="150">Keterangan</td>
				<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',5,65,$c_edit); ?></td>
			</tr>
			<tr>
				<td colspan="4" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br />
		<? if($r_key!=''){ ?>
		<input type="hidden" name="idttbe" id="idttbe" value="<?= $rs->fields['idttb'] ?>">
		<header style="width:872px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<!--img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" /-->
					<!--h1><?//= $p_title ?></h1-->
				</div>
		</header>
		<table border="0" align="center" cellspacing=0 cellpadding="6" class="GridStyle">
		<tr>
			<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul</td>		
			<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>
			<td width="150"nowrap align="center" class="SubHeaderBGAlt thLeft">Penerbit</td>		
			<td width="60"nowrap align="center" class="SubHeaderBGAlt thLeft">Tahun</td>		
			<td width="60"nowrap align="center" class="SubHeaderBGAlt thLeft">Qty</td>	
			<td width="60"nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>	
		</tr>
		<? 
			$i=0;
			while ($rows=$rs->FetchRow()) { 
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($rows['idorderpustaka'],$p_editkey)) {
		?>
		<tr class="<?= $rowstyle ?>">
			<td>
			<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEdIP('<?= $rows['idorderpustaka']; ?>','<?= $rows['idorderpustakattb'] ?>');" class="link"><?= $rows['judul']; ?></u>
				<? } else { ?>
					<?= $rows['judul']; ?>
				<? } ?>
			</td>
			<td>
				<?php
					echo $rows['authorfirst1']. " " .$rows['authorlast1']; 
					if ($rows['authorfirst2']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $rows['authorfirst2']. " " .$rows['authorlast2'];
					}
					if ($rows['authorfirst3']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $rows['authorfirst3']. " " .$rows['authorlast3'];
					}
				?>
			</td>
			<td><?= $rows['penerbit'] ?></td>
			<td><?= $rows['tahunterbit'] ?></td>
			<td><?= $rows['qtysumbangan'] ?></td>
			<? if($c_delete or $c_edit) { ?>
				<td align="center">
				
					<u title="Hapus Data" onclick="goDelIP('<?= $rows['idorderpustaka']; ?>','<?= $rows['judul']; ?>');" class="link">[hapus]</u>
				
				</td><? } ?>
		</tr>
		<? } else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center" valign="top"><?= UI::createTextBox('u_judul',$rows['judul'],'ControlStyle',200,35,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center" style="line-height:2">
				1. <?= UI::createTextBox('u_authorfirst1',$rows['authorfirst1'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('u_authorlast1',$rows['authorlast1'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?><br>
				2. <?= UI::createTextBox('u_authorfirst2',$rows['authorfirst2'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('u_authorlast2',$rows['authorlast2'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?><br>
				3. <?= UI::createTextBox('u_authorfirst3',$rows['authorfirst3'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('u_authorlast3',$rows['authorlast3'],'ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				</td>
				<td align="center" valign="top"><?= UI::createTextBox('u_penerbit',$rows['penerbit'],'ControlStyle',50,20,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center" valign="top"><?= UI::createTextBox('u_tahun',$rows['tahunterbit'],'ControlStyle',4,4,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center" valign="top"><?= UI::createTextBox('u_qty',$rows['qtysumbangan'],'ControlStyle',4,4,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center" valign="top">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="upData('<?= $rows['idorderpustaka']; ?>','<?= $rows['idorderpustakattb'] ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="6"><b>Belum ada sumbangan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center" valign="top"><?= UI::createTextBox('i_judul','','ControlStyle',200,35,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center" style="line-height:2">
				1. <?= UI::createTextBox('i_authorfirst1','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('i_authorlast1','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?><br>
				2. <?= UI::createTextBox('i_authorfirst2','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('i_authorlast2','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?><br>
				3. <?= UI::createTextBox('i_authorfirst3','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				<?= UI::createTextBox('i_authorlast3','','ControlStyle',50,15,true,'onKeyDown="etrInsert(event);"'); ?>
				</td>
				<td align="center"  valign="top"><?= UI::createTextBox('i_penerbit','','ControlStyle',50,20,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"  valign="top"><?= UI::createTextBox('i_tahun','','ControlStyle',4,4,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center" valign="top"><?= UI::createTextBox('i_qty','','ControlStyle',4,4,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center" valign="top"><input type="button" value="Simpan" onClick="insData()" class="ControlStyle"></td>
			</tr>
			<?php } ?>
			<tr>
				<td class="footBG" colspan="6">&nbsp;</td>
			</tr>
		</table><br>
		<? } ?>
		</center>
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key2 ?>">
		<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">

		</form>
		</div>
	</div>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tglsumbangan").mask("99-99-9999");
	   $("#i_tahun").mask("9999");
	   $("#u_tahun").mask("9999");
	   

});

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		upData('<?= $p_editkey; ?>','<?= $p_editkey2; ?>');
}
function goSaveSumb(){
	if(cfHighlight("namapengusul,tglsumbangan")){
	document.getElementById('act').value="simpansumb";
	goSubmit();
	}
}

function insData(){
	if(cfHighlight("i_judul,i_authorfirst1,i_penerbit,i_qty")){
		goInsIP();
		
	}
}

function upData(key,key3){
	if(cfHighlight("u_judul,u_authorfirst1,u_penerbit,u_qty")){
		goUpIP(key,key3);
		
	}
}

function goDelIP(key,label) {
	var hapus = confirm('Apakah anda yakin akan menghapus data "' + label + '"?');
	if(hapus) {
		document.getElementById("key2").value = key;
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goEdIP(key,key3) {
	document.getElementById("key2").value = key;
	document.getElementById("key3").value = key3;
	document.getElementById("act").value = "sunting";
	goSubmit();
}

function goInsIP() {
	document.getElementById("act").value = "insersi";
	goSubmit();
}

function goUpIP(key,key3) {
	document.getElementById("key2").value = key;
	document.getElementById("key3").value = key3;
	document.getElementById("act").value = "update";
	goSubmit();
}

function goPrint(key){
	popup('index.php?page=nota_sumbangan&id='+key,600,500);
}

</script>
</html>