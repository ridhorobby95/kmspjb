<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];


	
	// definisi variabel halaman
	$p_dbtable = 'lv_klasifikasi';
	$p_window = '[PJB LIBRARY] Daftar Klasifikasi Pustaka';
	$p_title = 'Daftar Klasifikasi Pustaka';
	$p_tbheader = '.: Daftar Klasifikasi Pustaka :.';
	$p_col = 3;
	$p_tbwidth = 100;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdklasifikasi";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['kdklasifikasi'] = Helper::cStrNull($_POST['i_kdklasifikasi']);
			$record['namaklasifikasi'] = Helper::cStrNull($_POST['i_namaklasifikasi']);
			//$record['denda'] = Helper::cStrNull($_POST['i_denda']);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			//$record['kdklasifikasi'] = Helper::cStrNull($_POST['u_kdklasifikasi']);
			$record['namaklasifikasi'] = Helper::cStrNull($_POST['u_namaklasifikasi']);
			//$record['denda'] = Helper::cStrNull($_POST['u_denda']);
			
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,kdklasifikasi,$r_key);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,kdklasifikasi,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
	//$x=$conn->GetRow("select * from pp_histregistrasi order by idhistregistrasi desc");
	//print_r($x);
	//$conn->Execute("delete from pp_histregistrasi where idhistregistrasi=21342");
	//$conn->Execute("update pp_eksemplar set noseri='KS-FA-19' where idpustaka='222544'");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="40">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">

			<tr height="20"> 
				<td width="120" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Kode Klasifikasi</td>
				<td width ="90%" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Klasifikasi</td>
				<? if ($c_edit or $c_delete) { ?>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
				<? } ?>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['kdklasifikasi'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<a class="link" title="Sunting Data" onclick="goEditIP('<?= $row['kdklasifikasi']; ?>');"><?= $row['kdklasifikasi']; ?></a>
				<? } else { ?>
					<?= $row['kdklasifikasi']; ?>
				<? } ?>
				</td>
				<td><?= $row['namaklasifikasi']; ?></td>
				<? if($c_delete or $c_edit) { ?>
				<td align="center">
					<img src="images/delete.png" title="Hapus pelayanan" onclick="goDeleteIP('<?= $row['kdklasifikasi']; ?>','<?= $row['namaklasifikasi']; ?>');" style="cursor:pointer">
				
				</td>
				<? } ?>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= $row['kdklasifikasi'] // UI::createTextBox('u_kdklasifikasi',$row['kdklasifikasi'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_namaklasifikasi',$row['namaklasifikasi'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center">
				<? if($c_edit) { ?>
					<img src="images/edit.png" title="Update Data" onclick="updateData('<?= $row['kdklasifikasi']; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_kdklasifikasi','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_namaklasifikasi','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr>
			<td colspan="3" class="footBG">&nbsp;
			</td>
			</tr>
		</table>
                </div>
                <br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namaklasifikasi').focus();
	<? } else { ?>
	document.getElementById('i_kdklasifikasi').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_kdklasifikasi,i_namaklasifikasi"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_namaklasifikasi")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>