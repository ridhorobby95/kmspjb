<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Daftar Pengusulan Oleh Anggota';
	$p_title = 'Daftar Pengusulan Oleh Anggota';
	$p_tbheader = '.: Daftar Pengusulan Oleh Anggota :.';
	$p_col = 9;
	$p_tbwidth = 970;
	$p_filedetail = Helper::navAddress('data_usulan.php');
	$p_filedetail2 = Helper::navAddress('data_usulans.php');
	$p_id = "tglusulan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'tglusulan';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select *, namaanggota from pp_usulan u
				left join ms_anggota a on u.idanggota=a.idanggota where 1=1 and ispengadaan='0'";
  	
	
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		$idanggota=Helper::removeSpecial($_POST['idanggota']);
		if($r_aksi=='validasi'){
	
		if ($id!='' and $idanggota!=''){
			$record=array();
			$record['tglrealisasi']=Helper::formatDate($_POST['tgl']);
			$record['statususulan']=Helper::removeSpecial($_POST['status']);
			Helper::Identitas($record);
			Sipus::UpdateBiasa($conn,$record,pp_usulan,idusulan,$id);
				
			if($conn->ErrorNo() != 0){
					$errdb = 'Verifikasi Pengusulan Pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Verifikasi Pengusulan Pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		}
		
			
			//}
			
		}else if($r_aksi=='valpus') {

			$record=array();
			$status=Helper::removeSpecial($_POST['statusawal']);
			if ($status==2){
			$record['stssupplier']=2;
			$record['stspo']=2;
			$record['stspengadaan']=2;		
			$record['statususulan']=2;
			}
			$record['stsperpus']=$status;
			$record['tglvalidasiperpus']=Helper::formatDate($_POST['tgl1']);
			$record['npkpetugasvalidasi']=$_SESSION['PERPUS_USER'];
			Helper::Identitas($record);
			Sipus::UpdateBiasa($conn,$record,pp_usulan,idusulan,$id);
				
			if($conn->ErrorNo() != 0){
					$errdb = 'Verifikasi Pengusulan Pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Verifikasi Pengusulan Pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}


		}else if ($r_aksi=='filter') {
		$filt=Helper::removeSpecial($_POST['txtid']);
		$jenis=Helper::removeSpecial($_POST['jenis']);
		if($filt!='')
		$p_sqlstr.=" and u.idanggota='$filt'";

		
		}
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
	
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
<tr>
	<td colspan=9 align="center">
<table width="100%">
	<tr height="20">
		<td align="center" class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td align="center"><? include_once('_notifikasi.php'); ?></td>
	</tr>
	<tr>
		<td align="center" valign="bottom">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_add) { ?>
				<td width="170" align="center">
				<a href="javascript:goNew('<?= $p_filedetail; ?>')" class="button"><span class="new">
				Buat usulan baru</span></a></td>
				<? } ?>
				<td width="170" align="center">
				<a href="javascript:goRef()" class="buttonshort"><span class="refresh">
				Refresh</span></a></td>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
	<td colspan=9>
	<table width="100%">
		<tr>
			<td width="150">Masukkan Id Anggota </td>
			<td>: &nbsp; <input type="text" name="txtid" id="txtid" maxlength="20" value="<?= $filt ?>"  onkeydown="etrFil(event);"></td>
			<td  align="right"><a href="javascript:goFilt()" class="buttonshort"><span class="filter">Filter</span></a></td>
		</tr>
	</table>
	</td>
</tr>
	<tr height="20"> 
		<td width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.tglusulan:D');">Tanggal<?= $p_xsort['tglusulan']; ?></td>		
		<td width="300"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>		
		<td width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.namaanggota:C');">Anggota Pengusul <?= $p_xsort['namaanggota']; ?></td>
		<td width="50" nowrap align="center" class="SubHeaderBGAlt">A</td>
		<td width="50" nowrap align="center" class="SubHeaderBGAlt">S</td>
		<td width="50" nowrap align="center" class="SubHeaderBGAlt">PO</td>
		<td width="50" nowrap align="center" class="SubHeaderBGAlt">TTP</td>
		<td width="60" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.statususulan:C');">Realisasi<?= $p_xsort['statususulan']; ?></td>
		<td width="90" nowrap align="center" class="SubHeaderBGAlt">Status Realisasi</td>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['idusulan'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height=20> 
		<td align="center">

		<u onClick="goDet('<?= $p_filedetail; ?>','<?= $row['idusulan'] ?>','<?= $row['idanggota'] ?>');"  title="Tampilkan Usulan" class="link"><?= Helper::tglEng($row['tglusulan'],false); ?></u>

		</td>		
		<td align="left"><?= $row['judul']; ?></td>
		<td align="left"><?= $row['namaanggota']; ?></td>
		<? if($c_edit) { ?>
		<td align="center"><?= $row['stsperpus']=='1' ? "<img src='images/centang.gif' title='Diterima'>" : ($row['stsperpus']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<u  class='showDialog' onclick='goStatus1(\"".$row['idusulan']."\",\"".$row['idanggota']."\")'><img src='images/wait.gif' style='cursor:pointer' title='Menunggu'></u>"); ?></td>
		<? } else { ?>
		<td align="center"><?= /*$row['statususulan']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : */($row['stsperpus']=='1' ? "<img src='images/centang.gif' title='Diterima'>" : ($row['stsperpus']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<img src='images/wait.gif'  style='cursor:pointer' title='Menunggu'>")); ?></td>
		<? } ?>
		<td align="center"><?=   $row['stsperpus']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : ($row['stssupplier']=='1' ? "<img src='images/centang.gif' title='Diterima'>" : ($row['stssupplier']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<img src='images/wait.gif' title='Menunggu'>")); ?></td>
		<td align="center"><?= $row['stssupplier']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : ($row['stspo']=='1' ? "<img src='images/centang.gif' title='Diterima'>" : ($row['stspo']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<img src='images/wait.gif' title='Menunggu'>")); ?></td>
		<td align="center"><?= $row['stspo']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : ($row['stspengadaan']=='1' ? "<img src='images/centang.gif' title='Diterima'>" : ($row['stspengadaan']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<img src='images/wait.gif' title='Menunggu'>")); ?></td>
		<td align="center"><?= $row['stspengadaan']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : ($row['statususulan']=='1' ? "<img src='images/centang.gif' title='Terealisasi'>" : ($row['statususulan']=='2' ? "<img src='images/uncheck.gif' title='Ditolak'>" : "<img src='images/wait.gif' title='Menunggu'>")); ?></td>
		<? if($c_edit and $row['tglvalidasiperpus']!='' and $row['supplierdipilih']!='' and $row['nopo']!='' and $row['tgldelivery']!='' ) { ?>
		<td align="center"><u class="showDialog1" title="Edit Status" onclick="goStatus('<?= $row['idusulan'] ?>','<?= $row['idanggota'] ?>')" style="cursor:pointer;color:blue">Edit</u></td>
		<? } else { ?>
		<td align="center"><u style="cursor:pointer;color:gray">Edit</u></td>
		<? } ?>
	</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
		</td>
	</tr>
	<tr>
		<td colspan=9 align="center">
		<table>
			<tr>
				<td align="center">
					<table align="center" cellspacing="3" cellpadding="2" width="350" border=0 bgcolor="#f1f2d1">
						<tr>
							<td colspan=2><u>Keterangan :</u></td>
						</tr>
						<tr>
							<td width=10>A</td><td>= Verifikasi Perpustakaan </td>
						</tr>
						
						<tr>
							<td width=10>S</td><td>= Verifikasi Supplier</td>
						<tr>
						<tr>
							<td width=10>PO</td><td>= Pembuatan PO</td>
						<tr>
						<tr>
							<td width=10>TTP</td><td>= Tanda Terima Pustaka</td>
						<tr>
					</table>
				</td>
				<td>
				<table align="center" cellspacing="3" cellpadding="2" width="350" border=0 bgcolor="#f1f2d1">
						<tr>
							<td colspan=2><u>Keterangan :</u></td>
						</tr>
						<tr>
							<td width=10><img src="images/centang.gif" title="Diterima"></td><td>= Usulan Pustaka Diterima/Disetujui </td>
						</tr>
						
						<tr>
							<td width=10><img src="images/wait.gif" title="Menunggu"></td><td>= Usulan Pustaka masih tahap menunggu</td>
						<tr>
						<tr>
							<td width=10><img src="images/uncheck.gif" title="Ditolak"></td><td>= Usulan Pustaka telah ditolak</td>
						<tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table><br>



<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="id" id="id">
<input type="hidden" name="idanggota" id="idanggota">
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>

<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

<div class="jqmWindow" id="dialog" class="GridStyle">

<!-- <u class="jqmClose" title="Tutup" style="cursor:pointer">Tutup</u><br> -->

<table border=0 cellpadding=0 cellspacing=0>
	<tr height=25>
		<td colspan=3><b><u>Verifikasi Penyerahan Pustaka :</u></b></td>
	</tr>
	<tr>
		<td >Tanggal</td>
		<td >:</td>
		<td><input name="tgl" id="tgl" type="text" value="<?= date("d-m-Y") ?>" maxlength="10" size="10">
		<td>
	</tr>
	<tr>
		<td >Status</td>
		<td >:</td>
		<td><label><input name="status" type="radio" value="1" checked />&nbsp; Terealisasi</label>
		
		<td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><label><input name="status" type="radio" value="2" />&nbsp; Ditolak</label></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><label><input name="status" type="radio" value="0" />&nbsp; Menunggu</label></td>
	</tr>
	<tr height=30>
		<td colspan=3 valign="bottom" align="center"><input type="button" name="updated" value="Update" onClick="goValidasi()">&nbsp;&nbsp;<input type="button" name="close" value=" Batal" class="jqmClose"></td>
	</tr>	
</table>

</div>
<div class="jqmWindow" id="valpus" class="GridStyle">

<!-- <u class="jqmClose" title="Tutup" style="cursor:pointer">Tutup</u><br> -->

<table border=0 cellpadding=0 cellspacing=0>
	<tr height=25>
		<td colspan=3><b><u>Verifikasi Awal Perpustakaan :</u></b></td>
	</tr>
	<tr>
		<td >Tanggal</td>
		<td >:</td>
		<td><input name="tgl1" id="tgl1" type="text" value="<?= date("d-m-Y") ?>" maxlength="10" size="10">
		<td>
	</tr>
	<tr>
		<td >Status</td>
		<td >:</td>
		<td><label><input name="statusawal" type="radio" value="1" checked />&nbsp; Diterima</label>
		
		<td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><label><input name="statusawal" type="radio" value="2" />&nbsp; Ditolak</label></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td><label><input name="statusawal" type="radio" value="0" />&nbsp; Menunggu</label></td>
	</tr>
	<tr height=30>
		<td colspan=3 valign="bottom" align="center"><input type="button" name="validasi" value="Verifikasi" onClick="goValPus()">&nbsp;&nbsp;<input type="button" name="close" value=" Batal" class="jqmClose"></td>
	</tr>	
</table>


</div>
</form>
</div>



</body>

<script src="scripts/jqModal.js"></script>


<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl").mask("99-99-9999");
	   $("#tgl1").mask("99-99-9999");
});
</script>
<script>
$().ready(function() {
  $('#dialog').jqm({modal: true, trigger: 'u.showDialog1'});
  $('#valpus').jqm({modal: true, trigger: 'u.showDialog'});
});
</script>
<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function etrFil(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goFilt();
}
function goStatus(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}
function goStatus1(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}
function goValidasi(){
	document.getElementById("act").value="validasi";
	goSubmit();
}

function goValPus(){
	document.getElementById("act").value="valpus";
	goSubmit();
}
function goFilt(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function goRef() {
	document.getElementById("act").value="";
	document.getElementById("txtid").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}
</script>
</html>