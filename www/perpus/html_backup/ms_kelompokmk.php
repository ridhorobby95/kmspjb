<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];


	
	// definisi variabel halaman
	$p_dbtable = 'lv_kelompokmk';
	$p_window = '[PJB LIBRARY] Daftar Kelompok Mata Kuliah';
	$p_title = 'Daftar Kelompok Mata Kuliah';
	$p_tbheader = '.: Daftar Kelompok Mata Kuliah :.';
	$p_col = 3;
	$p_tbwidth = 400;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdkelompokmk";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['kdkelompokmk'] = Helper::cStrNull($_POST['i_kdkelompokmk']);
			$record['namakelompokmk'] = Helper::cStrNull($_POST['i_namakelompokmk']);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['kdkelompokmk'] = Helper::cStrNull($_POST['u_kdkelompokmk']);
			$record['namakelompokmk'] = Helper::cStrNull($_POST['u_namakelompokmk']);
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,kdkelompokmk,$r_key);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,kdkelompokmk,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Pennghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="40">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
		<? include_once('_notifikasi.php'); ?>
		<header style="width:400px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Kode Kelompok</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Kelompok</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['kdkelompokmk'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['kdkelompokmk']; ?>');" class="link"><?= $row['kdkelompokmk']; ?></u>
				<? } else { ?>
					<?= $row['kdkelompokmk']; ?>
				<? } ?>
				</td>
				<td><?= $row['namakelompokmk']; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['kdkelompokmk']; ?>','<?= $row['namakelompokmk']; ?>');" class="link">[hapus]</u>
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= UI::createTextBox('u_kdkelompokmk',$row['kdkelompokmk'],'ControlStyle',15,10,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_namakelompokmk',$row['namakelompokmk'],'ControlStyle',50,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['kdkelompokmk']; ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_kdkelompokmk','','ControlStyle',15,10,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_namakelompokmk','','ControlStyle',50,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr>
			<td colspan="4" class="footBG">&nbsp;
			</td>
		</table><br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_kdkelompokmk').focus();
	<? } else { ?>
	document.getElementById('i_kdkelompokmk').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_kdkelompokmk,i_namakelompokmk"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_kdkelompokmk,u_namakelompokmk")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>