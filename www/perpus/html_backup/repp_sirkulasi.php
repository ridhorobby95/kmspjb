<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	
	$r_jenis = 'PJN';
	$r_pustaka = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_status = Helper::removeSpecial($_POST['status']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Sirkulasi Pustaka';
	
	$p_namafile = 'laporan_sirkulasi_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$row_jenis = $conn->GetRow("select namajenistransaksi from lv_jenistransaksi where kdjenistransaksi='$r_jenis'");
	$sql = "select v.*
		from v_trans_list v
		left join ms_pustaka p on p.idpustaka=v.idpustaka
		left join lv_lokasi l on l.kdlokasi = v.kdlokasi 
		where to_char(tgltransaksi,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' $sqlAdminUnit ";
	
	#jenis pustaka
	if($r_pustaka!='')
		$sql .=" and p.kdjenispustaka='$r_pustaka'";
	
	#status peminjaman
	if($r_status=='9')
		$sql .="  ";
	else
		$sql .=" and statustransaksi='$r_status' ";
	
	if($r_lokasi)
		$sql .=" and l.kdlokasi = '$r_lokasi' ";
	
	$sql .=" order by tgltransaksi";
	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width="775" cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h3>Laporan Sirkulasi Pustaka<br>
	Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <?if($r_status!='9'){?>
  <tr>
	<td align="left">Status Pustaka : <?= $r_status=='0' ? 'Kembali' :($r_status=='1' ? 'Belum Kembali' : 'Perpanjang')?></td>
  </tr>
  <?}?>
  </table>
<table width="775" border="1" cellpadding="2" cellspacing="0">
  <tr>
	<th width="12" align="center"><strong>No.</strong></th>
    <th width="10" align="center"><strong>No. Induk</strong></th>
    <th width="130" align="center"><strong>Judul Pustaka</strong></th>
	<th width="50" align="center"><strong>No. Panggil</strong></th>
	<th width="10" align="center"><strong>Id<br>Anggota</strong></th>
	<th width="80" align="center"><strong>Jenis Pinjam</strong></th>
	<th width="80" align="center"><strong>Tanggal<br>Pinjam</strong></th>
	<th width="80" align="center"><strong>Tanggal<br>Tenggat</strong></th>
	<th width="60" align="center"><strong>Perpanjang<br>Ke</strong></th>
	<th width="50" align="center"><strong>Tanggal<br>Perpanjang</strong></th>
	<th width="60" align="center"><strong>Tanggal<br>Kembali</strong></th>
	<th width="60" align="center"><strong>Telat</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{
		$jml_telat = Sipus::hitung_telat($conn,$rs['tgl_tenggat'],$rs['tglpengembalian']);
?>
    <tr height=25>
	<td align="center" valign="top"><?= $no ?></td>
	<td align="left" valign="top"><?= $rs['noseri'] ?></td>
	<td align="left" valign="top"><?= Helper::limitS($rs['judul'],70) ?></td>
	<td align="left" valign="top"><?= $rs['nopanggil'] ?></td>
	<td align="left" valign="top"><?= $rs['idanggota'] ?></td>
	<td align="center" valign="top"><?= $rs['kdjenistransaksi']=='PJF' ? 'Pinjam Fotocopy' : 'Pinjam Biasa' ?></td>
	<td align="center" valign="top"><?= Helper::tglEngTime($rs['tgltransaksi']) ?></td>
	<td align="center" valign="top"><?= $rs['tgltenggat']=='' ? 'Maksimal' : Helper::tglEngTime($rs['tgltenggat']) ?></td>
	<td align="center" valign="top"><?= $rs['perpanjangke']=='' ? '-' : $rs['perpanjangke'] ?></td>
	<td align="center" valign="top"><?= $rs['tglperpanjang']=='' ? '-' : Helper::tglEngTime($rs['tglperpanjang']) ?></td>
	<td align="center" valign="top"><?= Helper::tglEngTime($rs['tglpengembalian']) ?></td>
	<td align="center" valign="top"><?= $jml_telat>0?$jml_telat:'-'?></td>
  </tr>
	<? $no++; } ?>
	<? if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=12 >Tidak ada transaksi Sirkulasi</td>
	</tr>
	<? } ?>
    <tr>
    <td align="left" height=25 colspan=12><strong>Total Jumlah Sirkulasi : <?= $rsc; ?></strong></td>

  </tr>
</table>


</div>
</body>
</html>
