<?php
	$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_anggota = Helper::removeSpecial($_POST['txtanggota']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_anggota=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Reservasi Pustaka';
	
	$p_namafile = 'rekap_reserve'.$r_anggota.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.*, a.namaanggota, p.judul,e.noseri from pp_reservasi r
			join ms_anggota a on r.idanggotapesan=a.idanggota
			join pp_eksemplar e on r.ideksemplarpesan=e.ideksemplar
			join ms_pustaka p on e.idpustaka = p.idpustaka
			where r.idanggotapesan='$r_anggota' and r.tglreservasi between '$r_tgl1' and '$r_tgl2' order by r.tglreservasi desc";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	
	ob_end_clean();
	ob_start();

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;

	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	.up{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.print()">

<div align="center">
<table width="675" cellpadding="2" cellspacing="0" align="center">

<tr>
	<td align="center">
		<table width="675" border=0>
			<tr>
				<td width=60><img src="images/logounusa.jpg" style="width:50px;height:50px"></td>
				<td width="440" valign="bottom" align="left"><h3>PERPUSTAKAAN<br>PJB</h3></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<table width="675" cellpadding="2" cellspacing="0">
		  <tr>
			<td align="center" width="500"><strong>
			<h2>Laporan Reservasi Pustaka</h2>
			</strong></td>
		  </tr>
		</table>
		<table  width="500" cellpadding="2" cellspacing="0">
			<tr>
			<td align="left" width="93"> Id Anggota</td>
			<td  align="left">: <?= $row->fields['idanggotapesan'] ?>
		  </tr>
		  <tr>
			<td align="left"> Nama Anggota </td>
			<td align="left">: <?= $row->fields['namaanggota'] ?></td>
			</tr>
		  <tr>
			<td align="left"> Tanggal </td>
			<td align="left">: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="675" border="1" cellpadding="2" cellspacing="0">

		  <tr height=25>
			<td class="up" width="10" align="center"><strong>No.</strong></td>
			<td class="up" width="80" align="center"><strong>NO INDUK</strong></td>
			<td class="up" width="200" align="center"><strong>Judul Pustaka</strong></td>
			<td class="up" width="100" align="center"><strong>Tanggal Reservasi</strong></td>
			<td class="up" width="100" align="center"><strong>Status</strong></td>
		  </tr>
		  <?php
			$no=1;
			while($rs=$row->FetchRow()) 
			{  ?>
			<tr height=25>
			<td align="center"><?= $no ?></td>
			<td align="center"><?= $rs['noseri'] ?></td>
			<td  align="left"><?= $rs['judul'] ?></td>
			<td align="center"><?= Helper::tglEng($rs['tglreservasi']) ?></td>
			<td align="center"><?= $rs['statusreservasi']==0 ? "Terealisasi" : "Menunggu" ?></td>
			
		  </tr>
			<? $no++; } ?>
			<? if($no==0) { ?>
			<tr height=25>
				<td align="center" colspan=9 >Tidak ada pustaka</td>
			</tr>
			<? } ?>
		   <tr height=25><td colspan=5  align="left"><b>Jumlah : <?= $rsc ?></b></td></tr>
		</table>
	</td>
</tr>
</table>


</div>
</body>
</html>
<?
	if($r_format == 'pdf')
	{
		require_once("includes/dompdf/dompdf_config.inc.php");
		
		$string = ob_get_contents();
		ob_end_clean();

		$dompdf = new DOMPDF();
		$dompdf->load_html($string);
		$dompdf->debug = true;
		$dompdf->render();
		$dompdf->stream($p_namafile.'.pdf');
		//echo $string;
		
	}
	else
		ob_end_flush();

?>