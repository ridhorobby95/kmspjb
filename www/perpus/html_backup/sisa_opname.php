<?php
$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	$r_key = Helper::removeSpecial($_POST['key']);
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Daftar Eksemplar';
	$p_title = 'Daftar Sisa Eksemplar - Stock Opname';
	$p_tbheader = '.: Daftar Eksemplar Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 950;
	//$p_filedetail = Helper::navAddress('ms_eksemplar.php');
	$p_ekslist = Helper::navAddress('list_opname.php');
	$p_id = "ms_eksemplar";
	
	$p_title .= " ".$conn->GetOne("select namaopname from pp_opname where idopname = '$r_key'");

	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'e.noseri';//'p.kdjenispustaka';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	// sql untuk mendapatkan isi list max(t.tgltenggat) as tgltenggat,
	$p_sqlstr="select p.noseri as kodepustaka, p.judul, p.judulseri, p.edisi, p.authorfirst1, p.authorlast1, p.authorfirst2, p.authorlast2,
			p.authorfirst3, p.authorlast3, e.idpustaka,e.noseri, e.ideksemplar,e.statuseksemplar,s.keterangan as serial,
			k.namaklasifikasi, l.namalokasi, e.tglperolehan
		from pp_eksemplar e 
		join ms_pustaka p on p.idpustaka=e.idpustaka 
		left join lv_lokasi l on l.kdlokasi=e.kdlokasi 
		left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi 
		left join pp_serialitem s on e.idserialitem=s.idserialitem
		where p.kdjenispustaka in (".$_SESSION['roleakses'].")
			and e.ideksemplar not in (select ideksemplar from pp_detailopname where idopname = '$r_key') $sqlAdminUnit";

	
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyklasifikasi=Helper::removeSpecial($_POST['kdklasifikasi']);
		$keyjenis=Helper::removeSpecial($_POST['kdjenispustaka']);

		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$keynohal = Helper::removeSpecial($_POST['nohalaman']);
		
		$filtersearch = Helper::removeSpecial($_POST['filtersearch']);
		$carifilter = Helper::removeSpecial($_POST['carifilter']);
		
		$tglawal = Helper::removeSpecial(Helper::formatDate($_POST['tglawal']));
		$tglakhir = Helper::removeSpecial(Helper::formatDate($_POST['tglakhir']));
		
		$statuseks = Helper::removeSpecial($_POST['statuseks']);
		$kdkondisi = Helper::removeSpecial($_POST['kdkondisi']);
			
		if($filtersearch == "kdpustaka"){
			$p_sqlstr.=" and upper(p.noseri) like upper('%$carifilter%')";
		}elseif($filtersearch == "noinduk"){
			$p_sqlstr.=" and upper(e.noseri) like upper('%$carifilter%') ";
		}elseif($filtersearch == "judul"){
			$p_sqlstr.=" and upper(p.judul) like upper('%$carifilter%') ";
		}elseif($filtersearch == "nopanggil"){
			$p_sqlstr.=" and upper(p.nopanggil) like upper('%$carifilter%')";
		}elseif($filtersearch == "pengarang"){
			$p_sqlstr.=" and( (upper(p.authorfirst1||' '||p.authorlast1) like upper('%$carifilter%') or upper(p.authorfirst2||' '||p.authorlast2) like upper('%$carifilter%') or upper(p.authorfirst3||' '||p.authorlast3) like upper('%$carifilter%')) ";
			$p_sqlstr.=" or (upper(p.authorfirst1) like upper('%$carifilter%') or upper(p.authorfirst2) like upper('%$carifilter%') or upper(p.authorfirst3) like upper('%$carifilter%')) ";
			$p_sqlstr.=" or (upper(p.authorlast1) like upper('%$carifilter%') or upper(p.authorlast2) like upper('%$carifilter%') or upper(p.authorlast3) like upper('%$carifilter%')) )";
		}elseif($filtersearch == "penerbit"){
			$p_sqlstr.=" and upper(p.namapenerbit) like upper('%$carifilter%')";
		}elseif($filtersearch == "keyword"){
			$p_sqlstr.=" and upper(p.keywords) like upper('%$carifilter%')";
		}elseif($filtersearch == "isbn"){
			$p_sqlstr.=" and upper(p.isbn) like upper('%$carifilter%')";
		}
		
		if($tglawal and $tglakhir){
			$p_sqlstr.=" and (e.tglperolehan between '$tglawal' and '$tglakhir')";
		}else{
			if($tglawal){
				$p_sqlstr.=" and (p.tglperolehan = '$tglawal')";
			}elseif($tglakhir){
				$p_sqlstr.=" and (p.tglperolehan = '$tglakhir')";
			}
		}
		
		
		if($keyklasifikasi!=''){
			$p_sqlstr.=" and e.kdklasifikasi='$keyklasifikasi' ";	
		}
		if($keyjenis!=''){
			$p_sqlstr.=" and p.kdjenispustaka='$keyjenis' ";	
		}
		if($keylokasi!=''){
			$p_sqlstr .= " and e.kdlokasi='$keylokasi' ";
		}
		
		if($statuseks!=''){
			if($statuseks == "null")
				$p_sqlstr.=" and e.statuseksemplar is null ";
			else
				$p_sqlstr.=" and e.statuseksemplar='$statuseks' ";	
		}
		if($kdkondisi!=''){
			$p_sqlstr .= " and e.kdkondisi='$kdkondisi' ";
		}
		
		if($keynohal!=null or $keynohal!='')
			$p_page 	= $keynohal;
		else
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	// Group by
	//$p_sqlstr .=" group by p.judul, p.judulseri, p.edisi,e.idpustaka, e.noseri,e.ideksemplar,t.tgltenggat,t.idanggota,e.statuseksemplar,k.namaklasifikasi, l.namalokasi ";
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by kdklasifikasi");
	$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$keyklasifikasi,true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:150" onchange="goFilterEx()" ');
	$l_klasifikasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_klasifikasi);	
	
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$keyjenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goFilterEx()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);

	$rs_lokasi = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi");
	$l_lokasi = $rs_lokasi->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150" onchange="goFilterEx()" ');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
	
	$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
	$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$kdkondisi ,true,false,0,'id="kdkondisi" class="ControlStyle" style="width:150" onchange="goFilterEx()"');
	$l_kondisi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_kondisi);
	
	$a_status = array('' => '- Pilih Status -', 'null' => 'Proses', 'ADA' => 'Ada', 'PJM' => 'Dipinjam');
	$l_status = UI::createSelect('statuseks',$a_status,$statuseks,'ControlStyle',true, ' style="width:150" onchange="goFilterEx()"');
	
	$a_filter = array('' => '- Pilih Filter Pencarian -', 'kdpustaka' => 'Kode Pustaka', 'noinduk' => 'No. Induk', 'judul' => 'Judul', 'nopanggil' => 'No. Panggil', 'pengarang' => 'Pengarang', 'penerbit' => 'Penerbit','keyword' => 'Keyword', 'isbn' => 'ISBN');
	$l_filter = UI::createSelect('filtersearch',$a_filter,$filtersearch,'ControlStyle');

?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<style>

	/* tooltip styling. by default the element to be styled is .tooltip  */
	#tooltip {
		display:none;
		background:transparent url('images/black_arrow.png');
		font-size:8px;
		height:70px;
		width:160px;
		padding:25px;
		color:#fff;	
		border :1;
		
		position:absolute;
		top:5px;
		z-index: 3000;
		opacity: 0.85;
	}

	/* style the trigger elements */
	#test u {
		border:0;
		cursor:pointer;
		margin:0 8px;
		
	}
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);document.getElementById('carireg').focus();" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div class="filterTable">
          <table border=0 width="100%">
	    <tr>
              <td><?=$l_filter;?></td>
              <td>:</td>
              <td><input type="text" id="carifilter" name="carifilter" size="40" value="<?= $_POST['carifilter'] ?>" onKeyDown="etrCari(event);">
	      </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Tanggal Perolehan</strong></td>
              <td>:</td>
              <td>
		<?= UI::createTextBox('tglawal',$_POST['tglawal'],'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tglawale" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglawal",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglawale",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		s/d
		
		<?= UI::createTextBox('tglakhir',$_POST['tglakhir'],'ControlStyle',10,10,true); ?>
		<img src="images/cal.png" id="tglakhire" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglakhir",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglakhire",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
	      </td>
	                    <td>&nbsp;</td>
              <td><strong>Lokasi</strong></td>
              <td>:</td>
              <td>
		<?= $l_lokasi ?>
	      </td>
            </tr>
            <tr>
              <td><strong>Label Pustaka</strong></td>
              <td>:</td>
              <td><?= $l_klasifikasi ?></td>
              <td>&nbsp;</td>
              <td><strong>Jenis Pustaka</strong></td>
              <td>:</td>
              <td>
		<?= $l_jenis ?>
	      </td>
            </tr>
            <tr>
              <td><strong>Kondisi</strong></td>
              <td>:</td>
              <td><?= $l_kondisi ?></td>
              <td>&nbsp;</td>
              <td><strong>Status</strong></td>
              <td>:</td>
              <td>
		<?= $l_status ?>
	      </td>
              <td>&nbsp;</td>
              <td rowspan="2"><input type="button" value="Filter" class="ControlStyle" onClick="goFilterEx()">
                <input type="button" value="Refresh" class="ControlStyle" onClick="goClear(); goFilter(false);">
	      </td>
            </tr>
          </table>
        </div>
        <br/>
		<header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
            <div class="right">
              <h1>
                <a href="<?= $p_ekslist; ?>" class="button" style="margin-top: 3; margin-right: 3;"><span class="list">Daftar Stock Opname </span></a>
              </h1>
            </div>
          </div>
        </header>
		<table width="100%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">KODE PUSTAKA  <?= $p_xsort['e.noseri']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO. INDUK  <?= $p_xsort['e.noseri']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.judul:C');">Judul Pustaka  <?= $p_xsort['p.judul']; ?></th>		
			<th align="center" class="SubHeaderBGAlt">Pengarang</th>		
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','k.namaklasifikasi:C');">Klasifikasi<?= $p_xsort['k.namaklasifikasi']; ?></th>
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.statuseksemplar:C');">Status<?= $p_xsort['e.statuseksemplar']; ?></th>		
			<th align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi <?= $p_xsort['l.namalokasi']; ?></th>
			<?php
			$i = 0;
			if($showlist) {
				// mulai iterasi
				while ($row = $rs->FetchRow()) 
				{
					if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
					if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
		?>
		<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
			
			<td>
			<?= $row['kodepustaka'];//str_pad($row['kodepustaka'],7,"0",STR_PAD_LEFT); ?>
			</td>
			<td>
			<?= $row['noseri']; ?>
			</td>
		
			<td nowrap align="left"><p class="link" title="Lihat Detail" onclick="popup('index.php?page=show_eksemplar&id=<?= $row['idpustaka'] ?>&eks=<?= $row['ideksemplar'] ?>',850,600)"><?= Helper::limitS($row['judul']); ?></p></td>
			<td align="left">
				<?
					echo $row['authorfirst1']. " " .$row['authorlast1']; 
					if ($row['authorfirst2']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst2']. " " .$row['authorlast2'];
					}
					if ($row['authorfirst3']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst3']. " " .$row['authorlast3'];
					}
				?>
			</td>
			<td align="center"><?= $row['namaklasifikasi']; ?></td>
			<td align="center">
			<div id="test">
			
			<?
			if ($row['statuseksemplar']=='ADA')
			echo '<img src="images/ada.png" title="Ada" />'; 
			elseif ($row['statuseksemplar']=='PJM')
			{ 
			echo '<img src="images/pinjam.png" title="Terpinjam" />';
			?>
			<? } else { ?>
			<img src="images/Gear_32.png"title="Pustaka masih tahap proses" />
			<? } ?>
			</div>
			</td>
			<td align="left"><?= $row['namalokasi']; ?></td>
		</tr>
		<?php
			}
			}
			if ($i==0) {
		?>
		<tr height="20">
			<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
		</tr>
		<?php } ?>
		<tr> 
			<td colspan="11" align="right" class="FootBG">
				<div style="float:left">
					Menuju ke halaman : <?= UI::createTextBox('nohalaman','','ControlStyle',6,6); ?> <input type="submit" name="halaman" id="halaman" onClick="goHalaman()" value="Go">
					&nbsp;&nbsp;Menampilkan <?= Helper::formatNumber($rsc)?> Data Eksemplar
				</div>
				<div style="float:right">
					Halaman
					<?= $p_page ?>
					/
					<?= $p_lastpage ?>
					<?= $p_status ?>						
				</div>
			</td>
		</tr>
	</table> 
		<?php require_once('inc_listnav.php'); ?>
        <br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="nohal" id="nohal">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>
			<br />
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="350">
				<tr>
					<td style="border:0 none;" width=80><u>Keterangan :</u></td>
					<td style="border:0 none;" width=270> <span>-</span> <img src="images/pinjam.png" alt="" /> = Pustaka Sedang Dipinjam </td>
				</tr>
				
				<tr><td style="border:0 none;">&nbsp;</td>
					<td style="border:0 none;"> <span>-</span> <img src="images/ada.png" alt="" /> = Pustaka Status tersedia</td>
				</tr>
				<tr><td style="border:0 none;">&nbsp;</td>
					<td style="border:0 none;"> <span>-</span> <img src="images/Gear_32.png"/> = Pustaka masih tahap proses</td>
				</tr>
			</table>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
			<table width=100  class="menu-body">
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
					<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
				</tr>
				<tr>
					<td class="separator"><div class="separator-line"></div></td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
				</tr>
				<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
					<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
				</tr>
			</table>

		</form>
		</div>
	</div>
</div>



</body>
	<!--[if lt IE8] >
		<script src="scripts/jquery.tooltip2.js" type="text/javascript" />

	<! [endif]-->
<script src="scripts/jquery.tooltip.js"></script>
<script>
$('#test u').tooltip();
</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carifilter").value='';
	document.getElementById("kdjenispustaka").value='';
	document.getElementById("filtersearch").value='';
	document.getElementById("tglawal").value='';
	document.getElementById("tglakhir").value='';
	document.getElementById("kdlokasi").value='';
	document.getElementById("kdklasifikasi").value='';
	document.getElementById("statuseks").value='';
	document.getElementById("kdkondisi").value='';
	//goSubmit();
}

function goHalaman() {
	document.getElementById("nohal").value = $("#nohalaman").val();
	goSubmit();
}

function goFilterEx(){
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}
</script>

</html>