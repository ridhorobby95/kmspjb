<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	//if(!$isAdminPusat)	
	//	$sqlAdminUnit = " and la.idunit in ($units) ";

	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_anggota = Helper::removeSpecial($_POST['txtanggota']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Log Anggota';
	
	$p_namafile = 'rekap_sirkulasi'.$r_anggota.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select * FROM (
			(
				select p.t_userid, p.t_username, to_date(p.t_usertime,'YYYY-mm-dd HH24:mi:ss') t_usertime, p.t_userip, p.t_useraksi,
					p.t_userupdated, p.t_osname, p.t_browsername 
				from pp_historyaction p
				where 1=1 $sqlAdminUnit
			)
			union
			(
				SELECT v.t_user as t_userid, (
						select a.namaanggota
						from ms_anggota a where a.idanggota = v.idanggota and rownum = 1 $sqlAdminUnit
					) as t_username,
					to_date(v.waktuvisit,'YYYY-mm-dd HH24:mi:ss') as t_usertime, v.t_ipaddress as t_userip, 'login' as t_useraksi,
					'' as t_userupdated, v.t_osname, v.t_browsername
				FROM pp_visitor v
			)
			union
			(
				SELECT v.t_user as t_userid, (
					select a.namaanggota from ms_anggota a where a.idanggota = v.idanggota and rownum = 1 $sqlAdminUnit
					) as t_username, to_date(v.waktukeluar,'YYYY-mm-dd HH24:mi:ss') as t_usertime, v.t_ipaddress as t_userip,
						'logout' as t_useraksi,
					'' as t_userupdated, v.t_osname, v.t_browsername
				FROM pp_visitor v
				where waktukeluar is not null
			)
			
		) la
		where to_date(to_char(t_usertime,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') ";
	
	if($r_anggota!='')
	$sql .=" and t_userid = '$r_anggota' ";
	
	$sql .=" order by t_usertime desc ";	
	$rs = $conn->getArray($sql);
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Log Aktifitas Online</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Id Anggota </td>
	<td>: <?= $r_anggota!='' ? $r_anggota : 'Semua' ?></td>
	</tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="5%" align="center"><strong>No.</strong></th>
	<th width="10%" nowrap align="center"><strong>Id Anggota</strong></th>
	<th width="20%" nowrap align="center"><strong>Nama Anggota</strong></th>
	<th width="10%" nowrap align="center"><strong>Tanggal Log</strong></th>
	<th width="10%" nowrap align="center"><strong>IP Addres</th>
	<th width="20%" nowrap align="center"><strong>Aksi</strong></th>
	<th width="25%" nowrap align="center"><strong>Update</strong></th>
  </tr>
  <?php
	$no=1;
	$harga = 0;
	foreach ($rs as $row){  ?>
  <tr height=25>
	<td align="center" valign="top"><?= $no ?></td>
	<td align="left" valign="top"><?= $row['t_userid'] ?></td>
	<td valign="top"><?= $row['t_username'] ?></td>
	<td align="center" valign="top"><?= Helper::tglEngStamp($row['t_usertime']) ?></td>
	<td valign="top"><?= $row['t_userip'] ?></td>
	<td valign="top"><?= $row['t_useraksi'] ?></td>
	<td valign="top"><?= Helper::limitS($row['t_userupdated'],100);?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==1) { ?>
	<tr height=25>
		<td align="center" colspan='7' >Tidak ada Log</td>
	</tr>
	<? } else { ?>
   <tr height=25>
   <td colspan='7'><b>Jumlah : <?= $no-1 ?></b></td>
   
   </tr>
   <? } ?>
</table>


</div>
</body>
</html>
