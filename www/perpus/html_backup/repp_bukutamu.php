<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Buku Tamu Pengunjung';
	
	$p_namafile = 'laporan_bukutamu_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql="select b.idanggota, namaanggota, namasatker, to_char(tanggal,'dd-mm-YYYY') as tgl, to_char(tanggal,'HH24:MI:SS') as jam, tanggal, baca, sharing, pinjam_buku, scan, lainlain
		from pp_bukutamu b
		left join ms_anggota a on a.idanggota=b.idanggota 
		left join ms_satker j on j.kdsatker=a.idunit
		left join lv_lokasi l on l.kdlokasi = b.kdlokasi 
		where to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')
			$sqlAdminUnit ";
	
	if($r_lokasi)
		$sql .=" and b.kdlokasi = '$r_lokasi' ";
	
	$sql .="  order by tanggal ";
	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();

	var_dump($idunit);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/font-awesome.css">	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Laporan History Kunjungan
	<br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="560" border="1" cellpadding="2" cellspacing="2">
  
  <tr height="30">
	<th nowrap align="center"><strong>No.</strong></th>
	<th nowrap align="center"><strong>No Anggota</strong></th>
	<th nowrap align="center"><strong>Nama</strong></th>
	<th nowrap align="center"><strong>Unit</strong></th>
	<th nowrap align="center"><strong>Tanggal</strong></th>
	<th nowrap align="center"><strong>Pukul</strong></th>
	<th nowrap align="center"><strong>Baca</strong></th>
	<th nowrap align="center"><strong>Sharing</strong></th>
	<th nowrap align="center"><strong>Pinjam Buku</strong></th>
	<th nowrap align="center"><strong>Scan</strong></th>
	<th nowrap align="center"><strong>Lain-lain</strong></th>
  </tr>
  <?php
	$no=1;
	$jumlah = 0;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="center" nowrap><?= $rs['idanggota'] ?></td>
	<td align="center" nowrap><?= $rs['namaanggota'] ?></td>
	<td align="center" nowrap><?= $rs['namasatker'] ?></td>
	<td align="center" nowrap><?= $rs['tgl']; ?></td>
	<td align="center" nowrap><?= $rs['jam']; ?></td>
	<td align="center" nowrap><?= $rs['baca'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
	<td align="center" nowrap><?= $rs['sharing'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
	<td align="center" nowrap><?= $rs['pinjam_buku'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
	<td align="center" nowrap><?= $rs['scan'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
	<td align="center" nowrap><?= $rs['lainlain'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
  </tr>
	<?php $no++; } ?>
	<?php if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=11 >Tidak ada pengunjung</td>
	</tr>
	<?php } ?>
	<tr height=25 >	

    <td align="left" colspan='11'><strong>Total Jumlah  : <?= $rsc; ?></strong></td>
	
  </tr>
</table>


</div>
</body>
</html>