<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	

	
	// definisi variabel halaman
	$p_dbtable = 'lv_golonganpustaka';
	$p_window = '[PJB LIBRARY] Daftar Golongan Pustaka';
	$p_title = 'Daftar Golongan Pustaka';
	$p_tbheader = '.: Daftar Golongan Pustaka :.';
	$p_col = 5;
	$p_tbwidth = 100;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by golongan";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			//$record['idgolongan'] = Helper::cStrNull($_POST['i_idgolongan']);
			$record['golongan'] = Helper::cStrNull($_POST['i_golongan']);
			$record['kriteria'] = Helper::cStrNull($_POST['i_kriteria']);
			$record['awal'] = Helper::cStrNull($_POST['i_awal']);
			$record['akhir'] = Helper::cStrNull($_POST['i_akhir']);
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['golongan'] = Helper::cStrNull($_POST['u_golongan']);
			$record['kriteria'] = Helper::cStrNull($_POST['u_kriteria']);
			$record['awal'] = Helper::cStrNull($_POST['u_awal']);
			$record['akhir'] = Helper::cStrNull($_POST['u_akhir']);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idgolongan,$r_key);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idgolongan,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<header style="width:<?= $p_tbwidth ?>%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20"> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Golongan</td>
				<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kriteria</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Awal</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Akhir</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idgolongan'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<a title="Sunting Data" onclick="goEditIP('<?= $row['idgolongan']; ?>');" class="link"><?= $row['golongan']; ?></a>
				<? } else { ?>
					<?= $row['golongan']; ?>
				<? } ?>
				</td>
				<td><?= $row['kriteria']; ?></td>
				<td align="center"><?= $row['awal']; ?></td>
				<td align="center"><?= $row['akhir']; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
					<img src="images/delete.png" title="Hapus pelayanan" onclick="goDeleteIP('<?= $row['idgolongan']; ?>','<?= $row['golongan']." ".$row['kriteria'] ; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= UI::createTextBox('u_golongan',$row['golongan'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_kriteria',$row['kriteria'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_awal',$row['awal'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_akhir',$row['akhir'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center">
				<? if($c_edit) { ?>
					<img src="images/edit.png" title="Update Data" onclick="updateData('<?= $row['idgolongan']; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_golongan','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_kriteria','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_awal','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_akhir','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr>
			<td colspan="5" class="footBG">&nbsp;
			</td>
			</tr>
		</table>
            </div>
                <br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_kriteria').focus();
	<? } else { ?>
	document.getElementById('i_golongan').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_golongan,i_kriteria,i_awal,i_akhir"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_golongan,u_kriteria,u_awal,u_akhir")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>