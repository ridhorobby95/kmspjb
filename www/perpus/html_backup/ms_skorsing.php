<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// definisi variabel halaman
	$p_dbtable = 'pp_skorsing';
	$p_window = '[PJB LIBRARY] Data Skorsing';
	$p_title = 'Data Skorsing';
	$p_tbwidth = 530;
	$p_filelist = Helper::navAddress('list_skorsing.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
				
		if($r_aksi == 'simpan' and $c_edit) {
			$idanggota=Helper::removeSpecial($_POST['idanggota']);
			$c_anggota=$conn->GetRow("select idanggota, tglselesaiskors from ms_anggota where idanggota='$idanggota'");
			if(!$c_anggota){
				$errdb = 'Id Anggota tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
			} else {
			$record = array();$rec=array();
			$record = Helper::cStrFill($_POST);//,array tabel
			
			if($r_key == '') { // insert record	
				$record['tglskorsing']=Helper::formatDate($_POST['tglskorsing']);
				$record['tglexpired']=Helper::formatDate($_POST['tglexpired']);
				$record['keterangan']=strtoupper($_POST['keterangan']);
				Helper::Identitas($record);
				$conn->StartTrans();
				Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				
				$rec['tglselesaiskors']=Helper::formatDate($_POST['tglexpired']);
				Helper::Identitas($rec);
				Sipus::UpdateBiasa($conn,$rec,ms_anggota,idanggota,$idanggota);
				$conn->CompleteTrans();
				
				if($conn->ErrorNo()){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
				
			}
			else { // update record
				$record['tglskorsing']=Helper::formatDate($_POST['tglskorsing']);
				$record['tglexpired']=Helper::formatDate($_POST['tglexpired']);
				$record['keterangan']=strtoupper($_POST['keterangan']);
				Helper::Identitas($record);
				$conn->StartTrans();
				Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idskorsing",$r_key);
				
				$rec['tglselesaiskors']=Helper::formatDate($_POST['tglexpired']);
				Helper::Identitas($rec);
				Sipus::UpdateBiasa($conn,$rec,ms_anggota,idanggota,$idanggota);
				$conn->CompleteTrans();
				
				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					
					}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					header('Location: '.$p_filelist);
					exit();
					}
			}	
				
			
			if($err == 0) {
				if($r_key == '')
					$r_key = $record['idskorsing'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"idskorsing",$r_key);
			
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}else{
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
			
		}
		
	}
	if(!$p_errdb) {
	if($r_key !=''){
		$p_sqlstr = "select * from $p_dbtable 
					where idskorsing = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		}
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('idanggota').focus();" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
	<tr height="20">
		<td align="center" colspan=2 class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td colspan=2 align="center">
		<table border="0" cellpadding="2" cellspacing=0>
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
					<a href="<?= $p_filelist; ?>" class="buttonshort"><span class="list">&nbsp;&nbsp;&nbsp;&nbsp;Daftar Skors </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="buttonshort"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table><? include_once('_notifikasi.php') ?><br>
		</td>
	</tr>
	<tr>
		<td width=150 class="LeftColumnBG">Id Anggota</td>
		<td><?= UI::createTextBox('idanggota',$row['idanggota'],'ControlStyle',20,20,$c_edit); ?>
		<? if($c_edit) { ?>&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onclick="popup('index.php?page=pop_anggota&code=s',600,540);">
			<span id="span_posisi"><u title="Lihat Anggota" onclick="popup('index.php?page=pop_anggota&code=s',600,540);" style="cursor:pointer;color:blue" class="Link">Lihat Anggota...</u></span>
			<? } ?>
		</td>
	</tr>
	<tr>
		<td width=150 class="LeftColumnBG">Tanggal Skorsing</td>
		<td><?= UI::createTextBox('tglskorsing',($r_key=='' ? date("d-m-Y") : Helper::formatDate($row['tglskorsing'])),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleskor" style="cursor:pointer;" title="Pilih tanggal skorsing">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglskorsing",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleskor",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>
	<tr>
		<td width=150 class="LeftColumnBG">Tanggal Expired</td>
		<td><?= UI::createTextBox('tglexpired',Helper::formatDate($row['tglexpired']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleexpired" style="cursor:pointer;" title="Pilih tanggal expired">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglexpired",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleexpired",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		
		</td>
	</tr>
	<tr>
		<td width=150 class="LeftColumnBG">Keterangan</td>
		<td><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',5,50,$c_edit); ?></td>
	</tr>
</table>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>

<script language="javascript">
$(function(){
	   $("#tglskorsing").mask("99-99-9999");
	   $("#tglexpired").mask("99-99-9999");
});
function saveData() {
	if(cfHighlight("idanggota,tglskorsing,tglexpired"))
		goSave();
}

</script>
</html>	