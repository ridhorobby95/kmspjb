<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::formatDate($_REQUEST['tgl1']); //2012-01-30
	$r_tgl2 = Helper::formatDate($_REQUEST['tgl2']);
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	$r_jenispustaka = Helper::removeSpecial($_REQUEST['kdjenispustaka']);
	$r_jenisanggota = Helper::removeSpecial($_REQUEST['kdjenisanggota']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Peminjam Buku di Perpustakaan PJB';
	
	$p_namafile = 'Laporan Peminjam Buku Periode_'.$r_tgl1.'_'.$r_tgl2;

	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//untuk select semua fakultas yang dimiliki oleh PJB dan akan dimasukkan ke array.
	$fakultas = array();
	$sql_fakultas = "select namasatker from ms_satker lvf ";
	$sql_fakultas.=" order by lvf.namasatker ";	
	
	$rs= $conn->Execute($sql_fakultas);
	$rsc= $rs->RowCount();

	$i=0;
	while($row = $rs->FetchRow())
	{
		$fakultas[$i] = $row['namasatker'];
		$i++;
	}

	$buln = Helper::getMonthYear($r_tgl1,$r_tgl12);
	
	$jml_bulan = count($buln);
	$col_tahun = array();
	$cols_b = array();
	foreach($buln as $bln){
		$b = explode("-",$bln);
		$k = intval($b[0]);
		$t = intval($b[1]);
		$bulangrafik[$k.$t] = Helper::indoMonth($k,false)." ".$b[1];
		
		if($thn == $t){
			$cols_b[$t]++;
			continue;
		}
		else{
			$thn = $t;
			$col_tahun[] = $t;
			$cols_b[$t] = 1;
		}
	}

	$sql = "select count(ppt.idanggota) as jumlah_peminjam_buku, to_char(ppt.tgltransaksi,'mm') as bulan, to_char(ppt.tgltransaksi,'YYYY') as tahun,
			kdjenisanggota,ljv.namasatker as namasatker
		from pp_transaksi ppt
		left join ms_anggota msa on ppt.idanggota = msa.idanggota
		left join ms_satker ljv on ljv.kdsatker = msa.idunit
		join pp_eksemplar ppe on ppe.ideksemplar=ppt.ideksemplar
		join ms_pustaka msp on msp.idpustaka=ppe.idpustaka
		where to_date(to_char(ppt.tgltransaksi,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') ";
	

	if($r_jenispustaka != ''){
		$sql .= " and msp.kdjenispustaka='$r_jenispustaka'";
	}
	
	if($r_jenisanggota != ''){
		$sql .= " and msa.kdjenisanggota='$r_jenisanggota'";
	}
	
	$sql .= "group by ljv.namasatker,to_char(ppt.tgltransaksi,'mm'), to_char(ppt.tgltransaksi,'YYYY'),kdjenisanggota order by bulan,tahun";
	
	$rs = $conn->Execute($sql);
        while($row = $rs->FetchRow())
        {
		$data[$row['namasatker']][(int) $row['tahun']][(int) $row['bulan']] += $row['jumlah_peminjam_buku'];
        }
		
	$_SESSION['_DATA_BULAN'] = $bulangrafik;	
	
	if ($r_jenispustaka)	$namajenisanggota = $conn->getOne("select namajenispustaka from lv_jenispustaka where kdjenispustaka = '$r_jenispustaka'");
	if ($r_jenisanggota)	$namajenispustaka = $conn->getOne("select namajenisanggota from lv_jenisanggota where kdjenisanggota = '$r_jenisanggota'");
	if ($r_lokasi)	$namalokasi = $conn->getOne("select namalokasi from lv_lokasi where kdlokasi = '$r_lokasi'");
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=950>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=950 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Statistik Peminjam Buku di Perpustakaan PT Pembangkit Jawa Bali<br>Bulan <?= Helper::indoMonth((int)substr($r_tgl1,5,2))." ".substr($r_tgl1,0,4)." - ".Helper::indoMonth((int)substr($r_tgl2,5,2))." ".substr($r_tgl2,0,4) ?></h2>
  	</strong></td>
  </tr>
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()">Cetak Laporan</a></td>
	</tr>
	<? } ?>
</table>
<table width="950">
	<tr>
		<td width=100">Jenis Pustaka</td>
		<td width="10">:</td>
		<td><?= $namajenispustaka?></td>
	</tr>
	<tr>
		<td>Jenis Anggota</td>
		<td>:</td>
		<td><?= $namajenisanggota?></td>
	</tr>
	<tr>
		<td>Lokasi</td>
		<td>:</td>
		<td><?= $namalokasi?></td>
	</tr>
	<tr>
		<td>Tanggal</td>
		<td>:</td>
		<td><?= Helper::formatDateInd($r_tgl1) .' s/d'. Helper::formatDateInd($r_tgl2)?></td>
	</tr>
</table>
<table width="950" border="1" cellpadding="2" cellspacing="0">
	<tr height=25>
	      <th rowspan="2" width="150" align="center"><strong>Unit</strong></th>
	      <? foreach($col_tahun as $thn){?>
	      <th colspan="<?= $cols_b[$thn];?>" width="150" align="center"><strong><?=$thn?></strong></th>
	      <?}?>
	      <th rowspan="2" width="130" align="center"><strong>Jumlah</strong></th>
	</tr>
	<tr height=25>
	<?
		foreach($buln as $bln){
			$b = explode("-",$bln);
			$k = intval($b[0]);
	?>	
		<th width="130" align="center"><strong> <?= Helper::indoMonth($k,false);//." ".$b[1]; ?> </strong></th>
	<? } ?>
  	</tr>
  

   <? for($i=0;$i<count($fakultas);$i++) { ?>
	   <? $jumlahperFakultas = 0; ?>
			<tr height=25>    
				<td align="left"><?= $fakultas[$i]?></td>
				
				<? 
				foreach($buln as $bln){
					$b = explode("-",$bln);
					$j = intval($b[0]);
					$t = intval($b[1]);
					if($data[$fakultas[$i]][$t][$j] > 0) { ?>
						<td align="right"><?= Helper::formatNumber($data[$fakultas[$i]][$t][$j])?></td>
						<? $jumlahperFakultas += $data[$fakultas[$i]][$t][$j]; ?>
				<? } else { ?>
					<td align="right">0</td>
				<? } } ?>
			 
				<td align="right"> <?= Helper::formatNumber($jumlahperFakultas) ?> </td>
		<? $totalsemua += $jumlahperFakultas; ?>
		
		   </tr>
	<? } ?>
	
	
	<tr>
		<td align="center"><b>Jumlah</b></td>
		
		<? 
			foreach($buln as $bln){
					$b = explode("-",$bln);
					$j = intval($b[0]);
					$t = intval($b[1]);
			$t_bulan = 0;?>
			 
		<?
			for($i=0;$i<count($fakultas);$i++)
			{
				$t_bulan += $data[$fakultas[$i]][$t][$j];
			}
			$a_jumlahpeminjam[Helper::indoMonth($j,false)." ".$t] = $t_bulan;
		?>
			<td align="right"><b> <?= Helper::formatNumber($t_bulan) ?></b> </td>
		<? } ?>
		
		<td align="right"> <b><?= Helper::formatNumber($totalsemua) ?> </b>	 </td>
	</tr>
	   	    
</table>

<!-- Menampilkan grafik -->
<?if($r_format == 'html') { ?>
<?$_SESSION['_DATA_JUMLAHPEMINJAM'] = $a_jumlahpeminjam; ?>
<br><table>
	<tr>
		<? if($r_grafik =='batang') { ?>
		<td align="center"> Grafik Batang Jumlah Peminjam </td>
		<? } else { ?>
		<td align="center"> Grafik Pie Jumlah Peminjam </td>
		<? } ?>
	</tr>
	
	<tr>
		<td align="center">
		<img src="<?= Helper::navAddress('img_bar_peminjam') ?> ">
		</td>
		
	</tr>	
</table>
<?} ?>
<br><br><br><br>

<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border="0" >
	<tr><td width="500"></td><td>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td></td><td><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td></td><td><b><?=$kepala['namalengkap'];?><br>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>
