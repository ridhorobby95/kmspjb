<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and (a.idunit in ($units) or u.idunit in ($units)) ";
		$sqlAdminUnit_b = " and a.idunit in ($units) ";
		$sqlAdminUnit_u = " and u.idunit in ($units) ";
	}
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_status = Helper::removeSpecial($_POST['status']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_lokasi){
		$slokasi = " and (a.idunit = '$r_lokasi' or u.idunit = '$r_lokasi')) ";
		$slokasi_b = " and a.idunit = '$r_lokasi' ";
		$slokasi_u = " and u.idunit = '$r_lokasi' ";
	}
	
	if($r_jenis == 'Biasa')
		$r_urutan = Helper::removeSpecial(Helper::formatDate($_REQUEST['urutan']));
	else
		$r_urutan = Helper::removeSpecial(Helper::formatDate($_REQUEST['urutan1']));

	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') { // or $r_jenis==''
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Usulan Pengadaan Tahun '.substr($r_tgl1,0,4);
	$p_window2 = 'Laporan Usulan Pengadaan <br>Tahun '.substr($r_tgl1,0,4);
	
	$p_namafile = 'Laporan Pengusulan Pustaka_'.$r_jenis.'_'.$r_status.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	//statususulan = '0'
	if($r_jenis == 'Biasa')
		$sql = "select u.judul,u.hargausulan,u.authorfirst1,u.authorlast1,u.penerbit,u.tahunterbit,u.vote_usulan,u.tglusulan,u.isbn,u.tahunterbit,
				s.namasatker 
			from pp_usul u
			left join ms_anggota a on a.idanggota = u.idanggota
			left join ms_satker s on s.kdsatker = a.idunit 
			where to_char(u.tglusulan,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' and u.idunit is null $sqlAdminUnit_b $slokasi_b ";
	else if($r_jenis == 'Unit Kerja')
		$sql = "select u.judul,u.hargausulan,u.authorfirst1,u.authorlast1,u.penerbit,u.tahunterbit,u.tglusulan,u.isbn,u.tahunterbit, s.namasatker 
			from pp_usul u
			left join ms_satker s on s.kdsatker = u.idunit 
			where  to_char(u.tglusulan,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' and u.idunit is not null $sqlAdminUnit_u $slokasi_u";
	else
		$sql = "select u.judul,u.hargausulan,u.authorfirst1,u.authorlast1,u.penerbit,u.tahunterbit,u.tglusulan,u.isbn,u.tahunterbit, s.namasatker,
				t.namasatker as usatker 
			from pp_usul u
			left join ms_anggota a on a.idanggota = u.idanggota
			left join ms_satker s on s.kdsatker = a.idunit
			left join ms_satker t on t.kdsatker = u.idunit 
			where to_char(u.tglusulan,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' $sqlAdminUnit $slokasi ";

	if($r_urutan == 'harga'){
		$sql .= ' order by hargausulan desc';
	}
	
	else if($r_urutan == 'tanggal'){
		$sql .= ' order by tglusulan desc';
	}
	else{
		$sql .= ' order by vote_usulan desc';
	}

	$row = $conn->Execute($sql);
	$rsj = $row->RowCount();
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=995>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=1000 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2><?= $p_window2?></h2>
  	</strong></td>
  </tr>
	<tr>
		<td>Periode : <?= Helper::formatDateInd($r_tgl1) ?> s/d <?= Helper::formatDateInd($r_tgl2) ?></td>
	</tr>
	<tr>
		<td>Jenis Pengusulan : <?= $r_jenis=='semua'?'Biasa dan Unit':$r_jenis ?> </td>
	</tr>
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()">Cetak Laporan</a></td>
	</tr>
	<? } ?>
</table>


<table width="1000" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
	<th width="150" nowrap align="center"><strong>Judul Pustaka</strong></th>
	<th width="130" align="center"><strong>Pengarang</strong></th>
	<th width="130" align="center"><strong>Penerbit</strong></th>
	<th width="100" align="center"><strong>ISBN</strong></th>
	<th width="100" align="center"><strong>Tahun Terbit</strong></th>
	<th width="90" align="center"><strong>Tanggal Usulan</strong></th>
	<th width="100" align="center"><strong>Harga Usulan</strong></th>
	<th width="100" align="center"><strong>Unit Kerja</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    
	<td align="left"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?></td>
	<td align="left"><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
	<td align="center"><?= $rs['penerbit'] ?></td>
	<td align="center"><?= $rs['isbn'] ?></td>
	<td align="center"><?= $rs['tahunterbit'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglusulan']) ?></td>
	<td align="right"><?= $rs['hargausulan']!='' ? Helper::formatNumber($rs['hargausulan'],'0',true,true) : '-' ?></td>
	<td align="left"><?= ($rs['namasatker'] ? $rs['namasatker'] : $rs['usatker']); ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka yang diusulkan</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah Pengusulan : <?= $rsj ?><b></td></tr>
   
   
</table>
</div>
</body>
</html>