<?php
	// bagian include inisialisasi
		
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	
	// definisi variabel halaman
	$p_xpage = Helper::navAddress('list_infotracking.php');
	$p_bppage = Helper::navAddress('list_infotrackingp.php');
	$p_title = 'Tracking Usulan Bahan Pustaka';
	$p_window = '[PJB LIBRARY] Tracking Usulan Bahan Pustaka';
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<?php include('inc_menu.php'); ?>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	<br><br>
    <table cellpadding="0" cellspacing="0" align="center" width="100%" style="border-collapse:collapse">
      <tr>
        <td></td>
        <td align="center">
			<table width="530" border=0 align="center" cellpadding="4" cellspacing=0 class="instan">
			  <tr>
				<th align="center" colspan="2"><?= $p_title; ?></th>
			  </tr>
			  <tr height="35" class="LeftColumnBG" >
				<td>
				<?= UI::createSelect('tipecari',array('P'=>'Pengarang','K'=>'Pustaka'),'','ControlStyle',true); ?></td>
				<td><?= UI::createTextBox('keyword','','ControlStyle',50,50,true,'onKeyPress="etrCari(event)"') ?>
				&nbsp; <input type="button" name="btncari" id="btncari" value="Cari" style="cursor:pointer" onClick="goCari();">	
				</td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
        <td colspan="2" valign="top" align="center" id="contents" style="border-left: #a3b3c9 1px solid"></td>
	</tr>
    </table>
</form>
	
	<div id="progressbar" style="position:absolute;visibility:hidden;left:0px;top:0px;">
		<table bgcolor="#FFFFFF" border="1" style="border-collapse:collapse;"><tr><td align="center">
		Mohon tunggu...<br><br><img src="images/loading.gif">
		</td></tr></table>
	</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/forpagerx.js"></script>
<script type="text/javascript">

// untuk ajaxing list
xtdid = "contents";
expanded = true;

$(function() {
		$("#keyword").focus();
});

function etrCari(e) {
	var ev = (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goCari();
}

function goCari(alt) {
	if(document.getElementById("keyword").value == ""){
		alert("PERINGATAN: Silahkan Mengisi Kata Kunci Pada Form Pencarian.");
	}else{
		var sent = $("#perpusform").serialize();
		if ($("#tipecari").val() = 'P')			
			xlist = "df";
		<?php /*?>else
			xlist = "<?= $p_bppage; ?>";<?php */?>
			
		$("#" + xtdid).divpost({page: xlist, sent: sent});
	}
}


</script>

</html>