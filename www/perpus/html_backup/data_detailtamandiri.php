<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$idnotifikasi = Helper::removeSpecial($_REQUEST['idnotif']);
	// $c_edit2 = Helper::removeSpecial($_REQUEST['c_edit2']);
	
	// if($c_edit2 == '')
		// $c_edit2 = true;
	// else $c_edit2 = false;
	
	$c_edit2 = false;
	
	// definisi variabel halaman
	$p_dbtable = 'pp_ta';
	$p_window = '[PJB LIBRARY] Detail Upload Tugas Akhir';
	$p_title = 'Detail Upload Tugas Akhir';
	$p_tbwidth = 610;
	$p_filelist = Helper::navAddress('list_uploadtamandiri.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']); 
		$idnotifikasi = Helper::removeSpecial($_POST['idnotif']); 
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		if($r_aksi == 'simpan' and $c_edit) {
				$conn->StartTrans();
				// insert ke historyvalidasita
				$rs_cek_val = $conn->GetRow("select * from pp_historyvalidasita where iduploadta=$r_key and npkvalidasi='".'pusdig'."'");
				
				$record = array();
				$record = Helper::cStrFill($_POST);
				$record['iduploadta'] = $r_key;
				$record['tglhistoryvalidasi'] = date('Y-m-d');
				$record['npkvalidasi'] = 'pusdig';
				$record['stsvalidasi'] = $_POST['stsvalidasi'];
				if($record['stsvalidasi'] == '')
					$record['stsvalidasi'] = 0;
				$record['catatanvalidasi'] = $_POST['catatanvalidasi'];
				Helper::Identitas($record);
				if(empty($rs_cek_val))
					Sipus::InsertBiasa($conn, $record, 'pp_historyvalidasita');
				else
					Sipus::UpdateSpecial($conn, $record, 'pp_historyvalidasita', " iduploadta ",$r_key," npkvalidasi ", 'pusdig');
			
				//insert ke tabel pp_historynotifikasiuploadta====================================================================
				$rs_cek_nrp = $conn->GetRow("select * from pp_uploadta where iduploadta=$r_key");
				
				$rec_histnotif = array();
				$rec_histnotif['iduploadta'] = $r_key;
				$rec_histnotif['tglhistorynotifikasi'] = date('Y-m-d');
				$rec_histnotif['nrptujuannotifikasi'] = $rs_cek_nrp['idanggota'];
				$rec_histnotif['pesan'] = $_POST['catatanvalidasi'];
				$rec_histnotif['stsnotifikasi'] = 0;
				Helper::Identitas($rec_histnotif);
				Sipus::InsertBiasa($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
				
				$recupdate_histnotif = array();
				$recupdate_histnotif['stsnotifikasi'] = 1;
				Sipus::UpdateBiasa($conn, $recupdate_histnotif, 'pp_historynotifikasiuploadta', " idhistorynotifikasi ",$idnotifikasi);
				
				if($_POST['stsvalidasi']==null or $_POST['stsvalidasi']==0){ //jika tidak disetujui dan pernah di cek			
					$rec_uploadta = array();
						$rec_uploadta['statusvalstafdigitalisasi'] = 1;
						$rec_uploadta['tgllastvalstafdigitalisasi'] = date("Y-m-d");
						$rec_uploadta['npkvalstafdigitalisasi'] = $_SESSION['PERPUS_USER'];
					Sipus::UpdateBiasa($conn, $rec_uploadta, 'pp_uploadta', " iduploadta " ,$r_key);
					
				}else if($_POST['stsvalidasi']==1){ //jika disetujui/validasi
					$rec_uploadta = array();
					$rec_uploadta = array();
						$rec_uploadta['statusvalstafdigitalisasi'] = 2;
						$rec_uploadta['tgllastvalstafdigitalisasi'] = date("Y-m-d");
						$rec_uploadta['npkvalstafdigitalisasi'] = $_SESSION['PERPUS_USER'];
					
					Sipus::UpdateBiasa($conn, $rec_uploadta, 'pp_uploadta', " iduploadta ",$r_key);
				
				}	
				
				$conn->CompleteTrans();
				
			if($conn->ErrorNo() != 0){				
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				$restore=1;
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				if($r_key=='')
				$r_key=$record['iddaftarta'];
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key . '&c_edit2=false';
				Helper::redirect($url);
				}
			//print_r($record);
		}
		else if($r_aksi=='getnrp'){
			$nrp = Helper::removeSpecial(trim($_POST['nrp']));
			$nama = $conn->GetOne("select namaanggota from ms_anggota where idanggota='$nrp'");				
			
			$namae = explode(' ',$nama);
			$jnama = count($namae)-1;
		}

		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,iddaftarta,$r_key);
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
	}
	if ($r_key !='') {
		$p_sqlstr = "select b.catatanvalidasi,a.* from pp_uploadta a
						left join pp_historyvalidasita b on a.iduploadta = b.iduploadta and npkvalidasi = 'pusdig'
					 where a.iduploadta=$r_key";
		$row = $conn->GetRow($p_sqlstr);

		$rset_val = $conn->Execute("select a.namaanggota as mhs, b.namaanggota as dosen, h.* from pp_historynotifikasiuploadta h left join ms_anggota a on a.idanggota=h.t_user left join ms_anggota b on b.idanggota=h.npktujuannotifikasi where iduploadta=$r_key order by idhistorynotifikasi asc");
		
		$sqlFile = "select iduploadtafile, file, login, download
			from uploadta_file
			where iduploadta = '$r_key'
			order by iduploadtafile ";
		$rsFile = $conn->GetArray($sqlFile);
	}
	if($restore==1)
		$row = $_POST;
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charow=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link rel="shortcut icon" href="images/favicon.ico" />
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('idanggota').focus()">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><h1 style="font-weight:normal;color:#015593;"><?= $p_title ?></h1></td>
			</tr>
		</table>
		<?php include_once('_notifikasi.php'); 
		$c_edit = false;
		?>
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Tugas Akhir</span></a>
			</td>
			<? } if($c_edit2) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<?}?>
			</tr>
		</table><br/>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<header style="width:62.5%;">
		<div class="inner">
			<div class="left title">
				<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
				<h1>Detail Upload Tugas Akhir</h1>
			</div>
		</div>
	</header>
	<tr height=25> 
		<td width="100" class="LeftColumnBG">Judul Pustaka</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',1000,61,$c_edit); ?></td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 1</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('nrp1',$row['nrp1'],'ControlStyle',20,20,$c_edit,'id="nrp1"'); ?> - 
		<?= UI::createTextBox('authorfirst1',$r_key!='' ? $row['authorfirst1'] : str_ireplace(' '.$namae[$jnama],'',$nama),'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast1',$r_key!='' ? $row['authorlast1'] : $namae[$jnama] ,'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 2</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('nrp2',$row['nrp2'],'ControlStyle',20,20,$c_edit,'id="nrp2"'); ?> - 
		<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Penulis 3</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('nrp3',$row['nrp3'],'ControlStyle',20,20,$c_edit,'id="nrp3"'); ?> - 
		<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
	</tr>
	<tr height=25> 
	<td width="150" nowrap class="LeftColumnBG">Pembimbing 1</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('npkpembimbing1',$row['npkpembimbing1'],'ControlStyle',20,20,$c_edit,'id="npkpembimbing1"'); ?> - 
		<?= UI::createTextBox('pembimbing1',$row['pembimbing1'],'ControlStyle',100,29,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
	<td class="LeftColumnBG">Pembimbing 2</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('npkpembimbing2',$row['npkpembimbing2'],'ControlStyle',20,20,$c_edit,'id="npkpembimbing2"'); ?> - 
		<?= UI::createTextBox('pembimbing2',$row['pembimbing2'],'ControlStyle',100,29,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
	<td class="LeftColumnBG">Pembimbing 3</td>
		<td class="RightColumnBG" style="line-height:260%">
		<?= UI::createTextBox('npkpembimbing3',$row['npkpembimbing3'],'ControlStyle',20,20,$c_edit,'id="npkpembimbing3"'); ?> - 
		<?= UI::createTextBox('pembimbing3',$row['pembimbing3'],'ControlStyle',100,29,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Pesan Notifikasi</td>
		<td class="RightColumnBG"><?= UI::createTextArea('pesan',$row['pesan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table><br>

<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<header style="width:62.5%;">
		<div class="inner">
			<div class="left title">
				<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
				<h1>File Upload</h1>
			</div>
		</div>
	</header>
	<tr height=20> 
		<td class="RightColumnBG" ><input type="hidden" value="2" id="theValue" /><br>
			<?
			$k=0;
			foreach($rsFile as $f){
				$k++;
			?>
			<a href="uploads/uploadtamandiri/<?= $row['idanggota'].'/'.Helper::GetPath($f['file']) ?>"><?= $f['file']!='' ? "<img src='images/attach.gif' border=0> ".$k.". ".Helper::GetPath($f['file']) : '' ?></a><br><br>
			<? } ?>
			<? if($k==0) echo "File Kosong";?>
		</td>
	</tr>
</table><br>


<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<header style="width:62.5%;">
		<div class="inner">
			<div class="left title">
				<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
				<h1>History Notifikasi</h1>
			</div>
		</div>
	</header>
	<?while($row_notif = $rset_val->FetchRow()){
		if($row_notif['dosen']=='') $dosen='Petugas Digitalisasi';
		else $dosen=$row_notif['npktujuannotifikasi'].' - '.$row_notif['dosen'] ;
	?>
	<tr>
	<td width="<?= $p_tbwidth ?>" colspan="9">
		<div>
			<div style="padding:5px;margin-bottom:15px">
				<div style="border-bottom:1px dashed #999;margin-bottom:5px;padding-bottom:5px">
					<span style="float:left">
						<?php echo 'Pengirim #'. $row_notif['t_user'].' - '.$row_notif['mhs']?><br>
						<?php echo 'Penerima #'. $dosen ?>
					</span>
					
					<span style="float:right;font-size:smaller">
						<?php echo Helper::formatDateTime($row_notif['tglhistorynotifikasi'])?>
					</span>
					<div style="clear:both"></div>
				</div>
				<div>
					<? $p_foto = Sipus::getFoto2($conn,trim($row_notif['t_user']));?>
					<img style="float:right;border:2px solid gray" width="50" height="50" src="<?=$p_foto;?>" />
					<?php echo $row_notif['pesan']; ?>
					<div style="clear:both"></div>
				</div>
				
			</div>
		</div>
	</td>
	</tr>
	<?}?>
</table>
</br>			
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="idnotif" id="idnotif" value="<?= $idnotifikasi ?>">
<input type="hidden" name="nrp" id="nrp" value="<?= $nrp ?>">
<input type="hidden" name="nrpne2" id="nrpne2" value="<?= $nrp2 ?>">
<input type="hidden" name="nrpne3" id="nrpne3" value="<?= $nrp3 ?>">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

</form>
</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tgldaftarta").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function etrNrp(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		goNrp();
	}
}


function saveData() {
	if(cfHighlight("idanggota,namapengusul,tgldaftar,judul,pembimbing,authorfirst1"))
		goSave();
}

function goNrp(){
	var x = document.getElementById('idanggota').value;
	document.getElementById('act').value='getnrp';
	document.getElementById('nrp').value=x;
	
	goSubmit();

}


function goPrint(key){
	popup('index.php?page=nota_ta&id='+key,600,500);
}

function goJudul(){
	file = "<?= Helper::navAddress('sys_judulta.php'); ?>";
      $.ajax({
          type: "post",
          url: file,
          data: "idanggota="+ $("#idanggota").val(),
          cache: false,
          success: function(data){
			var hasil = data.split("*");
        		$("#judul").val(hasil[0]);
			$("#pembimbing").val(hasil['1']);
          }
      });
      return false;
}
</script>
</html>