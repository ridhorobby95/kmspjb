<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	$periode = Helper::periodeISO($r_tgl1);
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan ISO Reservasi';
	
	$p_namafile = 'rekap_lSOReservasi'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.*,e.noseri,t.tgltenggat,t.tglpengembalian from pp_reservasi r
			join pp_eksemplar e on r.ideksemplarpesan=e.ideksemplar
			join pp_transaksi t on r.idtransaksi=t.idtransaksi
			where r.tglreservasi between '$r_tgl1' and '$r_tgl2' ";
	$rs = $conn->Execute($sql);
	
	$i=0;
	$ket=0;
	$x=0;
	while($row=$rs->FetchRow()){
		$ArIdanggota[$i]=$row['idanggotapesan'];
		$ArRegcomp[$i]=$row['noseri'];
		$ArTglRe[$i]=$row['tglreservasi'];
		$ArTglTe[$i]=$row['tgltenggat'];
		$ArTglKe[$i]=$row['tglpengembalian'];
		$ArTglPi[$i]=$row['tglpinjam'];
		if($row['tglpinjam']>=$row['tglpengembalian'])
		$selisih[$i]=Helper::dateKurangi('-', $row['tglpinjam'], $row['tglpengembalian']);
		else
		$selisih[$i]=0;
		
		if($selisih[$i]<=3 and $ArTglPi[$i]!=''){
			$ket += 1;
			$ArTer[$i]='Tercapai';
		}else
		    $ArTer[$i] = 'Tidak Tercapai';
		    
		if($ArTglPi[$i]=='')
		    $ArTer[$i] = '';
		    
		if($ArTglPi[$i]!='')
		    $x +=1;
		
		$i++;		
	}
	//echo $ket;
	if($i!=0)
	$persen=($ket/$x*100);
	else
	$persen=0;


?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=700>
	<tr>
		<td width=100><img src="<?= $dirIcon.'logounusa.jpg' ?>" width=70 height=60></td>
		<td valign="center">Monitoring Sasaran Mutu<br>FM-MR-04/-/01/Rev 01<br>BDU: Perpustakaan</td>
	</tr>
</table><br>
<table width=700 border=0 cellpadding="2" cellspacing="0">
  <tr>
	<td>Nomor Prosedur : PM-PERPUS-03<br>
		Nama Dokumen : Monitoring Sasaran Mutu<br>
		Sasaran Mutu : Pemesanan buku yang sedang terpinjam<br>
		Kriteria Keberhasilan : <?= $periode ?> % Pemesan mendapatkan buku pesanan tidak lebih
		3 hari dari tanggal kembali<br>

		Periode : <?= Helper::formatDate($r_tgl1) ." s/d ". Helper::formatDate($r_tgl2) ?><br>
		Keberhasilan : 
		<? if($persen>0) { ?>
		Tercapai <?= Helper::formatNumber($persen,'0',false,true) ?> % 
		<? } else { echo '-'; }?>
</table><br>

<table width="700" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center" rowspan=2><strong>No.</strong></th>
    <th width="70" align="center" rowspan=2><strong>Id Anggota</strong></th>
    <th width="70" align="center" rowspan=2><strong>NO INDUK</strong></th>
    <th width="80" align="center" colspan=2><strong>Order</strong></th>
    <th width="80" align="center" colspan=2><strong>TRANS</strong></th>
	<th width="90" align="center" rowspan=2><strong>Selisih Hari</strong></th>
	<th width="90" align="center" rowspan=2><strong>Keterangan</strong></th>
	<th align="center" rowspan=2>Nomor Registrasi MR</th>
  </tr>
  <tr height=25>
    <th width="75" align="center"><strong>Tanggal</strong></th>
    <th width="75" align="center"><strong>Kembali</strong></th>
	<th width="75" align="center"><strong>Kembali</strong></th>
	<th width="75" align="center"><strong>Pinjam</strong></th>
  </tr>
  <?php
	$x=0;
	for($n=0;$n<$i;$n++)
	{ $x++		
	?>
    <tr height=25>
	<td align="center"><?= $x ?></td>
    <td align="left"><?= $ArIdanggota[$n]?></td>
    <td align="left"><?= $ArRegcomp[$n] ?></td>
    <td align="center"><?= Helper::formatDate($ArTglRe[$n]) ?></td>
	<td align="center"><?= Helper::formatDate($ArTglTe[$n]) ?></td>
	<td align="center"><?= Helper::formatDate($ArTglKe[$n]) ?></td>
	<td align="center"><?= Helper::formatDate($ArTglPi[$n]) ?></td>
	<td align="left"><?= $ArTglPi[$n]=='' ? 'Belum terpinjam' : $selisih[$n] ?></td>
	<td align="left"><?= $ArTer[$n] ?></td>
	<td>&nbsp;</td>
	
  </tr>
	<?  } ?>
	<? if($i==0) { ?>
	<tr height=25>
		<td align="center" colspan=10 >Tidak ada reservasi</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=10><b>Jumlah Reservasi : <?= $i ?></b></td></tr>
</table>


</div>
</body>
</html>