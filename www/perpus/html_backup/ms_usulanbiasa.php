<?php
//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// print_r($_REQUEST);
	// $conn->debug=true;
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);

	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Pengusulan Biasa';
	$p_title = 'Pengusulan Biasa';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_usulanbiasa.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		if($r_aksi == 'simpan' and $c_edit) {
		
				$record = array();
				$record = Helper::cStrFill($_POST);
				$record['judul'] = Helper::removeSpecial($_POST['judul']);
				$conn->StartTrans();
				if($r_key == '') { // insert record	
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['statususulan']=0;
				$record['isdenganpagu']=0;
				//$record['kdperolehan'] = $_POST['asalp'];
				//$record['kota'] = Helper::removeSpecial($_POST['kotaterbit']);
				Helper::Identitas($record);
				
				// input pp_usul
				Sipus::InsertBiasa($conn,$record,$p_dbtable);

				}
				else { // update record
					$record['kdperolehan'] = $_POST['asalp'];
					$record['kota'] = Helper::removeSpecial($_POST['kotaterbit']);
					$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
					Helper::Identitas($record);
					Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
				}	
				
				$conn->CompleteTrans();
				
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist);
				exit();
				}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
				$r_key = $record['idusulan'];
				}
				else {
				$row = $_POST; // direstore
				$p_errdb = true;
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$rs_cek = $conn->GetRow("select * from pp_usul where idusulan=$r_key");
			if($rs_cek['statususulan']=='1'){ // sdh diproses
				$err_o=Sipus::DeleteBiasa($conn,'pp_orderpustaka',idusulan,$r_key);
				if($err_o==0) {
					$err=Sipus::DeleteBiasa($conn,$p_dbtable,idusulan,$r_key);
					if($err == 0){
						header('Location: '.$p_filelist);
						exit();
					}else{
						$errdb = 'Penghapusan data gagal.';	
						Helper::setFlashData('errdb', $errdb);
					}
				}else{
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}else{
				$err=Sipus::DeleteBiasa($conn,$p_dbtable,idusulan,$r_key);
				if($err == 0){
					header('Location: '.$p_filelist);
					exit();
				}else{
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
			
		}
	}
	if ($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable
					where idusulan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);		
	}
	
	$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by namaperolehan");
	$l_asal = $rs_cb->GetMenu2('asalp',$row['kdperolehan'],true,false,0,'id="kdperolehan" class="ControlStyle" style="width:200"');
	//echo "r_key ".$r_key;
	//echo "r_key2 ".$r_key2;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/combo.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage(),loadauthor()">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<!--table width="<?//= $p_tbwidth ?>">
			<tr height="30">
				<td align="center" class="PageTitle"><?//= $p_title ?></td>
			</tr>
		</table-->
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Usulan</span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_key != '') { ?>
			<?//if($row['statususulan']!='1'){?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? }//} ?>
			
			</tr>
		</table>
		<br>
		<?php include_once('_notifikasi.php'); ?>
		<header style="width:600px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
			<tr height=25> 
				<td class="LeftColumnBG thLeft" width="180">Id Anggota *</td>
				<td width="370"><b><?= UI::createTextBox('idanggota',$row['idanggota'],'ControlRead',20,20,$c_edit,'readonly'); ?></b>
				<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=us',620,500);">
					<? } ?>
				</td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft" height="30">Nama Pengusul *</td>
				<td><b><?= UI::createTextBox('namapengusul',$row['namapengusul'],'ControlRead',50,50,$c_edit,'readonly'); ?></b></td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Tanggal Usulan *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tglusulan',$r_key!='' ? Helper::formatDate($row['tglusulan']) : date('d-m-Y'),'ControlStyle',10,10,$c_edit); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglusulan",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleusulan",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				
				[ Format : dd-mm-yyyy ]<? } ?>
				</td>
			</tr>
			
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Judul Pustaka *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,60,$c_edit); ?></td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Harga</td>
				<td class="RightColumnBG"><?= UI::createTextBox('hargausulan',Helper::formatNumber($row['hargausulan'],'0',false,false),'ControlStyle',14,14,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
			</tr>

			<tr height=25>
			<td class="LeftColumnBG thLeft">Pengarang 1 *</td>
				<td class="RightColumnBG">
				<?= UI::createTextBox('authorfirst1',$row['authorfirst1'],'ControlStyle',100,29,$c_edit); ?> 
				<?= UI::createTextBox('authorlast1',$row['authorlast1'],'ControlStyle',100,28,$c_edit); ?>
			</td>
			</tr>
			<tr height=25>
			<td class="LeftColumnBG thLeft">Pengarang 2</td>
				<td class="RightColumnBG">
				<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,29,$c_edit); ?>
				<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,28,$c_edit); ?>
			</td>
			</tr>
			<tr height=25>
			<td class="LeftColumnBG thLeft">Pengarang 3</td>
				<td class="RightColumnBG">
				<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,29,$c_edit); ?>
				<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,28,$c_edit); ?>
			</td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Penerbit *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit,'readonly'); ?>
				<input type="hidden" name="idpenerbit" id="idpenerbit">
				<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',550,430);">
					<? } ?>
				</td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Tahun Terbit *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?> [ Format : yyyy ] </td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Edisi</td>
				<td class="RightColumnBG"><?= UI::createTextBox('edisi',$row['edisi'],'ControlStyle',20,20,$c_edit); ?></td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">ISBN</td>
				<td class="RightColumnBG"><?= UI::createTextBox('isbn',$row['isbn'],'ControlStyle',30,30,$c_edit); ?></td>
			</tr>
			<tr height=25> 
				<td class="LeftColumnBG thLeft">Keterangan</td>
				<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table><br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">

		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function saveData() {
	if(cfHighlight("idanggota,namapengusul,tglusulan,judul,authorfirst1,tahunterbit,penerbit"))
		goSave();
}
function clearAuthor(urut) {
	document.getElementById("idauthor"+urut).value = "";
	document.getElementById("authorfirst"+urut).value = "";
	document.getElementById("authorlast"+urut).value = "";
}
</script>
</html>