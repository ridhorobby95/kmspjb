<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Upload Tugas Akhir';

	$p_filerep = 'repp_tugasakhir';
	$p_tbwidth = 420;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$a_status = array('semua' => '-Pilih Status-', 'sudah' => 'Sudah', 'belum' => 'Belum');
	$l_status = UI::createSelect('status',$a_status,'','ControlStyle');
	
	$yearnow = date('Y');
	$thnajarannow = date('Y');

	$thnajaran = array();
	$thnajaran[null] = "-Pilih Tahun Ajaran-";
	for($y1=($yearnow-10);$y1<=$yearnow;$y1++){
		$thnajaran[$y1] = $y1.'/'.($y1+1);
	}
	$l_thnajaran = UI::createSelect('thnajaran',$thnajaran,'','ControlStyle');
	
	$tahun = array();
	$tahun[null] = "-Pilih Tahun -";
	for($y1=($yearnow-10);$y1<=$yearnow;$y1++){
		$tahun[$y1] = $y1;
	}
	$l_tahun = UI::createSelect('thnterbit',$tahun,'','ControlStyle');
	
	?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:420px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Upload Tugas Akhir</h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">Tahun Ajaran</td>
				<td><?= $l_thnajaran ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Tahun Terbit</td>
				<td><?= $l_tahun ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Status Upload</td>
				<td class="RightColumnBG"><?= $l_status; ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">

function goPreSubmit() {
	if(cfHighlight("thnajaran"))
		goSubmit();
}

</script>
</html>