<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	//$conn->debug= true;
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$persen = Helper::removeSpecial(Helper::formatDate($_POST['persen']));
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	if($persen=='')
		$persen=100;
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Keuangan Perpustakaan';
	
	$p_namafile = 'rekap_keu'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$psqlstr = "select tgl,sum(rupiah) as jumlah from (
			select tglbayar as tgl,sum(coalesce(nilai,0)) as rupiah from pp_bayardenda where tglbayar between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') and nilai > 0 group by tglbayar
		) keu  group by tgl order by tgl";
	$rs=$conn->Execute($psqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=660>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=660 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Keuangan Perpustakaan</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
 <? /*  <tr>
	<td> Persentase</td>
	<td>: <?= $persen.' %' ?></td>
	</tr>
	*/ ?>
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>Tanggal</strong></th>
	<th width="200" align="center"><strong>Jumlah</strong></th>
  </tr>
  <? $jumlah=0;
  while ($row=$rs->FetchRow()){ 
  $rp=$persen/100*$row['jumlah'];
  ?>
  <tr height="25">
	<td><?= Helper::tglEng($row['tgl']) ?></td>
	<td align="right"><?= Helper::formatNumber($rp,'0',true,true)?>&nbsp;</td>
  </tr>
  <? $jumlah +=$rp; } ?>
  <? if($jumlah==0) { ?>
	<tr height=25>
		<td align="center" colspan=2 >Tidak ada laporan</td>
	</tr>
	<? } ?>
  <? if ($jumlah !=0) {
  ?> 
   <tr height=25>
   <td><b>Total</b></td>
   <td align="right"><b><?= Helper::formatNumber($jumlah,'0',true,true) ?>&nbsp;</b></td>
   </tr>
   <? } ?>
</table>
</div>
</body>
</html>