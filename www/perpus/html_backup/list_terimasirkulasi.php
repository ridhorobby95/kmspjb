<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengolahan.class.php');
	require_once('classes/sirkulasi.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarolah';
	$p_window = '[PJB LIBRARY] Daftar Penerimaan Sirkulasi';
	$p_title = 'Daftar Penerimaan Eksemplar';
	$p_tbheader = '.: Daftar Penerimaan Eksemplar :.';
	$p_col = 9;
	$p_tbwidth = 980;
	$p_filedetail = Helper::navAddress('data_kirimeksternal.php');
	$p_id = "lssirkulasiterima";
	
	$p_defsort = 'ideksemplarolah';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$p_sqlstr = "select p.*, e.noseri as regcomp ,l.namalokasi,m.*,k.namaklasifikasi, j.namajenispustaka from $p_dbtable p
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar
				left join ms_pustaka m on m.idpustaka=e.idpustaka
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka
				left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
				left join pp_orderpustakattb ot on ot.idorderpustakattb=p.idorderpustakattb
				left join pp_orderpustaka op on op.idorderpustaka=ot.idorderpustaka
				left join lv_lokasi l on e.kdlokasi=l.kdlokasi
				where (stskirimpengolahan=1 and stskirimeksternal=1 and m.kdjenispustaka in ($_SESSION[roleakses]) and isfinish=0 
				and (stssumbangan=1 or stssumbangan=0 or stssumbangan is null) 
				)";
				//and (idusulan in(select idusulan from pp_usul where idunit is null and isdenganpagu=0) or idusulan is null) 
				//";

	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carino']);
		$f5=Helper::removeSpecial($_POST['kdjenispustaka']);
		$f_usulan = Helper::removeSpecial($_POST['jenisusulan']);
		$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
		
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(m.judul) like upper('%$keyjudul%') ";
		}if($f5!='') {
			$p_sqlstr.=" and m.kdjenispustaka='$f5' ";
		}
		$status = Helper::removeSpecial($_POST['status']);
		if($status!='x'){
			$p_sqlstr .=" and p.stssirkulasi=$status ";
		}
		if ($r_lokasi != '')
			$p_sqlstr .= " and e.kdlokasi = '$r_lokasi' ";
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
		if ($r_aksi == 'terima'){
			$record = array();
			$record['tglsirkulasi'] = date('Y-m-d H:m:s');
			$record['npksirkulasi'] = $_SESSION['PERPUS_USER'];
			$record['stssirkulasi'] = 1;
			
			Helper::Identitas($record);
			
			for($i=0;$i<count($_POST['cbSelect']);$i++)
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				
			$r_strsent = implode("','",$_POST['cbSelect']);
			
			$err = Sipus::UpdateComplete($conn,$record,$p_dbtable,"ideksemplarolah in ('$r_strsent')");
			if($err != 0)
			{
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
			}else {
					//record untuk history
					$rechis = array();
					$rechis['tkhistory'] = 'T';
					$rechis['keterangan'] = 'Terima Sirkulasi';
					$rechis['tglterima'] = date('Y-m-d H:m:s');
					$rechis['npkterima'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
					Pengolahan::insHistory($conn,$rechis,$r_strsent);
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					//Helper::redirect();
			}
		}
		else if ($r_aksi == 'kirim'){
			$record = array();
			$record['tglkirim'] = date('Y-m-d H:m:s');
			$record['npkkirim'] = $_SESSION['PERPUS_USER'];
			$record['stskirimeksternal'] = 1;
			
			for($i=0;$i<count($_POST['cbSelect']);$i++)
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				
			$r_strsent = implode("','",$_POST['cbSelect']);
			
			$err = Sipus::UpdateComplete($conn,$record,$p_dbtable,"ideksemplarolah in ('$r_strsent')");
			
			if($err != 0)
			{
					$errdb = 'Pengiriman data gagal.';	
					Helper::setFlashData('errdb', $errdb);
			}else {
					$sucdb = 'Pengiriman data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if ($r_aksi == 'setada'){
			$record = array();
			$record['isfinish'] = 1;
			
			for($i=0;$i<count($_POST['cbSelect']);$i++)
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				
			$r_strsent = implode("','",$_POST['cbSelect']);
			
			//$err = Sipus::UpdateComplete($conn,$record,$p_dbtable,"ideksemplarolah in ('$r_strsent')");
			$err = Sirkulasi::setAvailable($conn,$r_strsent);
			if($err != 0)
			{
					$errdb = 'Pengiriman data gagal.';	
					Helper::setFlashData('errdb', $errdb);
			}else {
					$sucdb = 'Pengiriman data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
			}
		}
		/*
		else if($r_aksi == 'status'){
			$status=Helper::removeSpecial($_POST['status']);
			if($status!='x'){
				$p_sqlstr .=" and p.stssirkulasi=$status ";
			}
		} */

	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where kdjenispustaka in ($_SESSION[roleakses]) order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$f5,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_jenis);	
	$a_status = array('x' => '-- Semua --', '0' => 'Belum Diterima', '1' => 'Sudah Diterima');
	$l_status = UI::createSelect('status',$a_status,$status,'ControlStyle',true,'style="width:150" onchange="goSubmit()"');
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$r_lokasi,true,false,0,'class="ControlStyle" style="width:150" onchange="goSubmit()"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_lokasi);	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	
	<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing="0" class="GridStyle">

	<tr height="30">
		<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
	</tr>
	<tr>
	<td width="100%"  colspan="<?= $p_col; ?>">
		<table width="100%" cellpadding="4" cellspacing="0" border="0">
			<tr>
				<td width="150">
					<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
					Refresh</span></a>
				</td>	
				<? if ($c_edit) { ?>
				<td width="170">
					<a href="javascript:goApprove();" class="buttonshort"><span class="approve">
					Terima</span></a>
				</td>		
				<td width="150">
					<a href="javascript:setAvailable();" class="button"><span class="approve">
					Set Available</span></a>
				</td>		
				<td width="150">&nbsp;
				</td>				
				<? } ?>
				<td align="right" valign="middle" colspan="3">
					<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
					<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
					<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
					<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			<tr>
				<td width="150"><strong>Judul</strong></td>
				<td colspan="3" width="400">:&nbsp;<input type="text" name="carino" id="carino" size="50" value="<?= $_POST['carino'] ?>" onKeyDown="etrCari(event);"></td> <td  align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="filter">Filter</span></a></td>
			</tr>
			<tr>
				<td><strong>Jenis Pustaka</strong></td>
				<td colspan="3">: <?= $l_jenis ?></td>
			</tr>
			<tr>
				<td><b>Status</b></td>
				<td>: <?= $l_status ?></td>
				<td colspan=3><strong>Lokasi</strong> : <?= $l_lokasi ?></td>
			</tr>
		</table>
	</td>
	<tr height="20"> 
		<td nowrap align="center" class="SubHeaderBGAlt" s><input type="checkbox" name="cbSelAll" id="cbSelAll" value="1" class="ControlStyle" onClick="checkedAll()"></td>
		<td width="200" align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO INDUK  <?= $p_xsort['noseri']; ?></td>
		<td width="350" align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.judul:C');">Judul  <?= $p_xsort['judul']; ?></td>
		<td width="150" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.authorfirst1:C');">Pengarang  <?= $p_xsort['authorfirst1']; ?></td>
		<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.nopanggil:C');">No Panggil<?= $p_xsort['nopanggil']; ?></td>
		<td width="70" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.edisi:C');">Edisi <?= $p_xsort['edisi']; ?></td>		
		<td nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.kdjenispustaka:C');">Jenis  <?= $p_xsort['kdjenispustaka']; ?></td>
		<td width="110" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.kdlokasi:C');">Lokasi<?= $p_xsort['kdlokasi']; ?></td>
		<td width="110" nowrap align="center" class="SubHeaderBGAlt">Status</td>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" valign="middle"> 
		<td align="center">
		<input type="checkbox" id="cbSelect" name="cbSelect[]" value="<?= $row['ideksemplarolah']; ?>" class="ControlStyle">
		</td>
		<td><?= $row['regcomp']; ?></td>
		<td><?= $row['judul']; ?></td>
		<td><?php
							echo $row['authorfirst1']. " " .$row['authorlast1']; 
							if ($row['authorfirst2']) 
							{
								echo " <strong><em>,</em></strong> ";
								echo $row['authorfirst2']. " " .$row['authorlast2'];
							}
							if ($row['authorfirst3']) 
							{
								echo " <strong><em>,</em></strong> ";
								echo $row['authorfirst3']. " " .$row['authorlast3'];
							}
						?></td>
		<td align="center"><?= $row['nopanggil']; ?></td>
		<td align="center"><?= $row['edisi']; ?></td>
		<td align="center"><?= $row['namajenispustaka']; ?></td>
		<td align="center"><?= $row['namalokasi'] ?></td>
		<td align="center"><? if ($row['stssirkulasi'] != 1) {?>
							<font color="red"><b>
							<script>
							var skor="<?= $row['stssirkulasi'] != 1 ? 'Belum Diterima' : 'Sudah Diterima';?>";
							document.write(skor.blink());
							</script>
							</b></font>
							<? }else echo 'Sudah Diterima'; ?>
		</td>
	</tr>
	<?php
		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col ; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
		</td> 
		
	</tr>
</table><br>

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="act" id="act">
<input type="hidden" name="arrcb" id="arrcb">
</form>
</div>



</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	$("#carino").val("");
	$("#kdjenispustaka").val("");
	$("#jenisusulan").val("");
	$("#kdlokasi").val("");
	<? unset($_SESSION['statusl']) ?>
}

function goApprove(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih dulu data yang ingin diterima ke pegolahan dengan mencentang barisnya.");
	else {
		var retval;
		retval = confirm("Anda yakin untuk menerima ke pengolahan data yang dicentang?");
		if (retval) {
			$("#act").val("terima");
			goSubmit();
			
		}
	}
}

function setAvailable(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih dulu data yang ingin di set available ke sirkulasi dengan mencentang barisnya.");
	else {
		var retval;
		retval = confirm("Anda yakin untuk set available ke sirkulasi data yang dicentang?");
		if (retval) {
			$("#act").val("setada");
			goSubmit();
			
		}
	}
}

function goSent(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih dulu data yang ingin dikirm ke sirkulasi dengan mencentang barisnya.");
	else {
		var retval;
		retval = confirm("Anda yakin untuk mengirim ke sirkulasi data yang dicentang?");
		if (retval) {
			$("#act").val("kirim");
			goSubmit();
			
		}
	}
}


checked=false;
function checkedAll () {
	var aa= document.getElementById('perpusform');
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i =0; i < aa.elements.length; i++) 
	{
	 	aa.elements[i].checked = checked;
	}
}
function goStatus(){
	$("#act").val("status");
	goSubmit();
}

</script>

</html>