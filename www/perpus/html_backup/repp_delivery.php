<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	include_once('includes/init.php');
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) "; // u.idunit dibuangm, karena gak ada inisial u di query dan membuat error

	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_nodev = Helper::removeSpecial($_POST['nodev']);
	
	if($r_format=='' or $r_nodev=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Tanda Terima Pustaka';
	
	$p_namafile = 'rekap_'.$r_nodev;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql = "select o.judul, ot.qtyttbdetail, t.npkttb, t.tglttb, s.namasupplier, t.nottb  
		from pp_orderpustakattb ot 
		join pp_ttb t on ot.idttb = t.idttb
		join pp_orderpustaka o on ot.idorderpustaka = o.idorderpustaka
		left join ms_supplier s on o.supplierdipilih = s.kdsupplier
		left join pp_usul pu on pu.idusulan=o.idusulan 
		left join ms_anggota a on a.idanggota = pu.idanggota 
		where t.idttb = $r_nodev and t.jnsttb = 1 $sqlAdminUnit ";
	
	$row = $conn->Execute($sql);
	$user= $conng->GetRow("select userdesc from sc_user where username='".$row->fields['npkttb']."'");
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
	<table width=675>
		<tr>
			<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
			<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
		</tr>
	</table>
	<table width=675 cellpadding="2" cellspacing="0" border=0>
		<tr>
		      <td align="center"><strong>
		      <h2>Laporan Tanda Terima Pustaka</h2>
		      </strong></td>
		</tr>
		<tr>
		    <td>No. TTP : <b><?= $row->fields['nottb'] ?></b><br><br></td>
		</tr>
		<tr>
		    <td>Telah terima pesanan dari Penerbit/Toko Buku : <b><?= $row->fields['namasupplier'] ?></b><br><br></td>
		</tr>
	</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="150" align="center"><strong>Judul Pustaka</strong></th>
    <th width="80" align="center"><strong>Jumlah</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="left"><?= $rs['judul'] ?></td>	
	<td align="center"><?= $rs['qtyttbdetail'] ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=3 >Tidak ada keterangan/td>
	</tr>
	<? } ?>
</table><br><br>
<table width="675" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td width=450>&nbsp;</td>
	<td>
	Surabaya, <?= Helper::formatDateInd(date('Y-m-d')); ?>
	<br><br>
	Penerima,
	<br><br><br><br>
	<?= $user['userdesc'] ?>
</div>
</body>
</html>
