<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Penagihan';

	$p_filerep = 'repp_tagihan';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	$a_jenis = array('0' => 'Penagihan', '1' => 'Cek Ke Rak');
	$l_jenis = UI::createSelect('jenis',$a_jenis,'','ControlStyle');
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka order by kdjenispustaka");
	$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka','',true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150"');
	$l_pustaka = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_pustaka);
	
	$rs_cb = $conn->Execute("select namajenisanggota, kdjenisanggota from lv_jenisanggota order by namajenisanggota");
	$l_anggota = $rs_cb->GetMenu2('kdjenisanggota','',true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:150"');
	$l_anggota = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_anggota);

	$a_tagih = array('semua' => '-- Semua --','0' => 'Peminjaman Biasa', '1' => 'Pinjam Baca');
	$l_tagih = UI::createSelect('tagih',$a_tagih,'','ControlStyle',true,'style="width:150"');
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi where 1=1 $sqlAdminUnit order by namalokasi");
	$l_jur = $rs_cb->GetMenu2('kdlokasi','',true,false,0,'id="kdlokasi" class="ControlStyle" style="width:200"');
		

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('ke').focus()">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Pustaka Terlambat</h1>
				</div>
			</div>
		</header>
          <div class="table-responsive">  
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">Penagihan Ke</td>
				<td>
					<select name="ke" id="ke">
						<option value=''> -- semua --</option>
						<?php for($a=1; $a<=999; $a++) {?>
						<option value="<?= $a?>"><?= $a?></option>
						<?php } ?>
					</select>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Tagihan</td>
				<td><?= $l_jenis ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Anggota</td>
				<td><?= $l_anggota ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Peminjaman</td>
				<td><?= $l_tagih ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Pustaka</td>
				<td><?= $l_pustaka ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Unit</td>
				<td><?= $l_jur ?></td>
			</tr>
			<tr> 
				<td class="LeftColumnBG thLeft" width="120">Tanggal Harus Kembali</td>
				<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
				<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl1",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				s/d &nbsp;
				<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>

				<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl2",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>

				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
		</div>
</body>
<script language="javascript">

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2"))
		goSubmit();
}

$(document).ready(function() {
    $("#ke").keydown(function (e) {
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ) {
            e.preventDefault();
        }
    });
});


</script>
</html>
