// Popup Menu SECTION START  ---------------------------------

// mouse position

 
function checkS(e){ 
// capture the mouse position must be called at body onload
    if (!e) var e = window.event; 
    if (e.pageX || e.pageY) 
    { 
		mouseX = e.pageX-10;
		mouseY =  e.pageY;
    } 
    else if (e.clientX || e.clientY) 
    { 
        mouseX =  event.clientX-10+document.body.scrollLeft;
        mouseY = event.clientY+document.body.scrollTop;
    } 
} 

function getLikeElements(tagName, attrName, attrValue) {
    var startSet;
    var endSet = new Array( );
    if (tagName) {
        startSet = document.getElementsByTagName(tagName);    
    } else {
        startSet = (document.all) ? document.all : 
            document.getElementsByTagName("*");
    }
    if (attrName) {
        for (var i = 0; i < startSet.length; i++) {
            if (startSet[i].getAttribute(attrName)) {
                if (attrValue) {
                    if (startSet[i].getAttribute(attrName).substring(0,attrValue.length) == attrValue) {
                        endSet[endSet.length] = startSet[i];
                    }
                } else {
                    endSet[endSet.length] = startSet[i];
                }
            }
        }
    } else {
        endSet = startSet;
    }
    return endSet;
}

function ClosePopup(e)
{
	if( isMenuOpened )
	{
    	if( overpopupmenu == false )
    	{
			isMenuOpened = false ;
			overpopupmenu = false;
			var arrDiv = getLikeElements("div","id","pop");
			for (i=0;i<arrDiv.length;i++) // close all popup windows
				arrDiv[i].style.display = "none";
			return true ;
    	}
    	return true ;
  	}
  	return false;
}

function PopupMenu(pMenu,pParam)
{
	var popUp = document.getElementById(pMenu);
	gParam = pParam;
	if (ns6)
	{
		popUp.style.left = mouseX;
		popUp.style.top = mouseY;
	} else {
		popUp.style.pixelLeft = mouseX;
		popUp.style.pixelTop = mouseY;
	}
	popUp.style.display = "";
	isMenuOpened = true;
	return false ;
}

if(window.addEventListener){ // Mozilla, Netscape, Firefox
	document.addEventListener('mousedown', ClosePopup, false);
} else { // IE
	document.attachEvent('onmousedown', ClosePopup);
}
