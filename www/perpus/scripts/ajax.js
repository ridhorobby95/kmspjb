function makeRequest() {
	var httpRequest;
	
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			// httpRequest.overrideMimeType('text/xml'); // biar tidak terjadi not-well formed error
		}
	} 
	else if (window.ActiveXObject) { // MSIE
		try {
			httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (e) {
			try {
				httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} 
			catch (e) {}
		}
	}

	if (!httpRequest) {
		//alert('Tidak bisa membuat sebuah instansi XMLHTTP');
		alert('Terjadi kesalahan. Mohon ulangi sekali lagi.');
		return false;
	}
	
	return httpRequest;
}

function execFunction(control,url,func,arg) {
	var i, argv, argn;
	
	httpRequest = makeRequest();
	
	//memecah argumen yang dijoin dengan ';'
	argv = arg.split(';');
	if(argv.length > 1) {
		argn = "";
		for(i=0;i<argv.length;i++)
			argn += ("&arg[]=" + argv[i]);
	}
	else
		argn = ("&arg[]=" + argv);
		
	httpRequest.onreadystatechange = function() { setContent(httpRequest,control); };
	httpRequest.open('GET', url+"?function="+func+argn, true);
	httpRequest.send('');
}

function getServerContent(control,url) {
	httpRequest = makeRequest();
	httpRequest.onreadystatechange = function() { setContent(httpRequest,control); };
	httpRequest.open('GET', url, true);
	httpRequest.send('');
}

function getImageContent(imageid,url) {
	httpRequest = makeRequest();
	httpRequest.onreadystatechange = function() { setImage(httpRequest,imageid,url); };
	httpRequest.open('GET', url, true);
	httpRequest.send('');
}

function setContent(httpRequest,control) {	
	if (httpRequest.readyState == 4) {
		if (httpRequest.status == 200) {
			if(control.type) //berarti sebuah kontrol
				control.value = httpRequest.responseText;
			else //bukan kontrol, anggapannya span atau div :p
				control.innerHTML = httpRequest.responseText;
		} else {
			//alert('Ada masalah pada permohonan anda');
			alert('Terjadi kesalahan. Mohon ulangi sekali lagi.');
		}
	}
}

function setImage(httpRequest,imageid,url) {	
	if (httpRequest.readyState == 4) {
		if (httpRequest.status == 200) {
			imageid.src = url + '?' + Math.random();
		} else {
			//alert('Ada masalah pada permohonan anda');
			alert('Terjadi kesalahan. Mohon ulangi sekali lagi.');
		}
	}
}