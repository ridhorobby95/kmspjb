function goSort(direction) {
	if (gParam) {
		arrParam = gParam.split(":");
		document.getElementById("sort").value = arrParam[0] + ' ' + direction;	
		goSubmit();
	}
}

function goFilter(filterOn) {
	if (!filterOn) {
		// reset filter
		document.getElementById("page").value = 1;
		document.getElementById("sort").value = "";
		document.getElementById("filter").value = "";
		goSubmit();
	}
	else 
	{
		if (document.getElementById("popPaging")) { // tampilkan teksboks filter
			if (ns6) 
			{
				document.getElementById("popFilter").style.left = document.getElementById("popPaging").style.left;
				document.getElementById("popFilter").style.top = document.getElementById("popPaging").style.top;
			} 
			else
			{
				document.getElementById("popFilter").style.left = document.getElementById("popPaging").style.pixelLeft;
				document.getElementById("popFilter").style.top = document.getElementById("popPaging").style.pixelTop;
			}
			
			document.getElementById("popFilter").style.display = "inline";
			document.getElementById("popPaging").style.display = "none" ;
			document.getElementById("txtFilter").value = "";
			goSubmit();
		}
	}
}

function doFilter(e) {
	var ev = (window.event) ? window.event: e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key==13 && gParam) // jika ditekan tombol enter
	{
		// processing filter
		arrParam = gParam.split(":");
		var columnFilter = arrParam[0];
		var columnType = arrParam[1];
		var retval;
		retval = document.getElementById("txtFilter").value;
		if (retval)
		{
			if (document.getElementById("filter").value)  // tambah kriteria filter
				document.getElementById("filter").value = document.getElementById("filter").value + ':';
			document.getElementById("filter").value = document.getElementById("filter").value + columnFilter  + ':' + retval + ':' + columnType;
			document.getElementById("page").value = 1;	
			goSubmit();
		}
	} 
	else if (key==27) // jika ditekan tombol escape
		document.getElementById("popFilter").style.display = "none";
}

function goSubmit() {
	document.getElementById("perpusform").submit();
}