var xlist, xtdid, colparam;
var cat, str, page, sorts, filter;

// untuk paging

function initButton(isfirst,islast) {
	if(isfirst) {
		$("#firstButton,#prevButton").attr("disabled",true);
		$("#firstButton").attr("src","images/first2.gif");
		$("#prevButton").attr("src","images/prev2.gif");
		$("#firstButton,#prevButton").css("cursor",null);
	} else {
		$("#firstButton,#prevButton").attr("disabled",false);
		$("#firstButton").attr("src","images/first.gif");
		$("#prevButton").attr("src","images/prev.gif");
	} if (islast) {
		$("#nextButton,#lastButton").attr("disabled",true);
		$("#nextButton").attr("src","images/next2.gif");
		$("#lastButton").attr("src","images/last2.gif");
		$("#nextButton,#lastButton").css("cursor",null);
	} else {
		$("#nextButton,#lastButton").attr("disabled",false);
		$("#nextButton").attr("src","images/next.gif");
		$("#lastButton").attr("src","images/last.gif");
	}
}

function goFirst() {
	if(page > 1) {
		page = 1;
		goListX();
	}
}

function goPrev() {
	if(page > 1) {
		page--;
		goListX();
	}
}

function goNext(last) {
	if(page < last) {
		page++;
		goListX();
	}
}

function goLast(last) {
	if(page < last) {
		page = last;
		goListX();
	}
}

function goPage(npage) {
	page = npage;
	goListX();
}

function goRefresh() {
	page = 1;
	sorts = "";
	filter = "";
	goListX();
}

function goListX() {
	sent = "cat=" + cat + "&str=" + str + "&page=" + page + "&sort=" + sorts + "&filter=" + filter;
	$("#" + xtdid).divpost({page: xlist, sent: sent});
}

// untuk link ke halaman edit

function goNew(loc) {
	location.href = loc;
}

function goDetail(file,key) {
	$("#perpusform").attr("action",file);
	$("#key").val(key);
	if($("#fmenu"))
		$("#fmenu").val(cat+"#"+str);
	$("#perpusform").submit();
}

// untuk menu popup kolom

function goSort(direction) {
	if (colparam) {
		arrparam = colparam.split(":");
		sorts = arrparam[0] + " " + direction;
		goListX();
	}
}

function resetFilter() {
	filter = "";
	goListX();
}

function doFilter(e) {
	var key = e.charCode || e.keyCode || 0;
	
	if (key == 13 && colparam) // jika ditekan tombol enter
	{
		arrparam = colparam.split(":");
		retval = $("#txtFilter").val();
		retval = chartrans(retval,'%','*');
		retval = chartrans(retval,'?','_');
		
		if (retval)
		{
			if (filter)  // tambah kriteria filter
				filter += ':';
			filter += (arrparam[0] + ':' + retval + ':' + arrparam[1]);
			page = 1;
			goListX();
		}
	} 
	else if (key==27) // jika ditekan tombol escape
		$("#popFilter").css("visibility","hidden");
}

// fungsi tambahan

function chartrans(str,cb,ca) {
	var nstr = "";
	for(i=0;i<str.length;i++) {
		if(str.charAt(i) == cb)
			nstr += ca;
		else
			nstr += str.charAt(i);
	}
	return nstr;
}