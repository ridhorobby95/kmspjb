/*
 * Async Treeview 0.1 - Lazy-loading extension for Treeview
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 *
 * Copyright (c) 2007 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id$
 *
 */

;(function($) {

function load(settings, root, child, container) {
	$.getJSON(settings.url, {root: root}, function(response) {
		function createNode(parent) {
			var current = $("<li/>").attr("id", this.id || "").html("<a>" + this.text + "</a>").appendTo(parent);
			if (this.classes) {
				current.children("span").addClass(this.classes);
			}
			if (this.expanded) {
				current.addClass("open");
			}
			if (this.hasChildren || this.children && this.children.length) {
				var branch = $("<ul/>").appendTo(current);
				if (this.hasChildren) {
					current.addClass("hasChildren");
					createNode.call({
						text:"placeholder",
						id:"placeholder",
						children:[]
					}, branch);
				}
				if (this.children && this.children.length) {
					$.each(this.children, createNode, [branch])
				}
			}
		}
		$.each(response, createNode, [child]);
        $(container).treeview({add: child});
		
		// added for SIM Kepegawaian, 09-10-08
		$(child).setupWilayah($(container),$("#contents")); // set here so there isn't more modifying :)
	
		arrcode = location.search.substr(1).split('&');
		if(arrcode.length == 2) {
			linkid = arrcode[1];
			currid = linkid.substr(0,2);
			
			for(i=2;currid.length < linkid.length;i++) { // expand ancestors
				li = $(child).find("li[id='" + currid + "']");
				li.children("div.hitarea").click();
				currid = linkid.substr(0,2*i);
			}
			li.children("a").click();
		}
		else if(child == container) {
			$("#contents").divpost({page: "index.php?page=xms_propinsi"});
		}
    });
}

var proxied = $.fn.treeview;
$.fn.treeview = function(settings) {
	if (!settings.url) {
		return proxied.apply(this, arguments);
	}
	var container = this;
	load(settings, "source", this, container);
	var userToggle = settings.toggle;
	return proxied.call(this, $.extend({}, settings, {
		collapsed: true,
		toggle: function() {
			var $this = $(this);
			if ($this.hasClass("hasChildren")) {
				var childList = $this.removeClass("hasChildren").find("ul");
				childList.empty();
				load(settings, this.id, childList, container);
			}
			if (userToggle) {
				userToggle.apply(this, arguments);
			}
		}
	}));
};

// added for SIM Kepegawaian, 09-10-08
$.fn.setupWilayah = function(container,xviewer) {
	return $(this).each(function() {
		$(this).find("a").each(function() {
			sentid = $(this).parents("li").attr("id");
			
			if(sentid.length <= 6) {
				$(this).click(function(e) {
					e.preventDefault();
					container.find("a").removeClass("selected");
					$(this).addClass("selected");
					
					sentid = $(this).parents("li").attr("id");
					if(sentid.length == 2)
						page = "index.php?page=xms_kabupaten";
					else if(sentid.length == 4)
						page = "index.php?page=xms_kecamatan";
					else if(sentid.length == 6)
						page = "index.php?page=xms_kelurahan";
					else
						page = "index.php?page=xms_propinsi";
					
					xviewer.divpost({page: page, sent: "pkey=" + sentid});
				});
			}
			else {
				$(this).css("color","black");
				$(this).css("cursor","text");
			}
		});
	});
};

})(jQuery);