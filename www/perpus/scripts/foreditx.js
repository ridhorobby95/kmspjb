/* fungsi umum */
var sent="";

function goSubmit(formid) {
	if(!formid)
		formid = "perpusform";
	document.getElementById(formid).submit();
}

function goUndo() {
	document.getElementById("perpusform").reset();
}

/* fungsi dengan jquery */

function aDelPhoto(formid) {
	if(!formid)
		formid = "perpusform";
	
	var retval;
	retval = confirm("Anda yakin untuk menghapus foto ini?");
	if (retval) {
		$("#" + formid).attr("target","upload_iframe");
		$("#" + formid + " #act").val("hapusfoto");
		$("#" + formid + " #imgfoto").waitload();
		goSubmit(formid);
	}
}

function aSavePhoto(formid) {
	if(!formid)
		formid = "perpusform";
	$("#" + formid).attr("target","upload_iframe");
	$("#" + formid + " #act").val("simpanfoto");
	$("#" + formid + " #imgfoto").waitload();
	goSubmit(formid);
}

//dimasukan ke dalam file js
function uploadFile(formid) {
	if(!formid)
		formid = "perpusform";
	$("#" + formid).attr("target","upload_iframe");
	$("#" + formid + " #act").val("simpanfile");
	goSubmit(formid);
}

function confirmFile(formid)
{
	if(!formid)
		formid = "perpusform";
	$("#" + formid).attr("target","upload_iframe");
	$("#" + formid + " #act").val("confirmFile");
	goSubmit(formid);
}

function deleteFile(formid)
{
	if(!formid)
		formid = "perpusform";
	$("#" + formid).attr("target","upload_iframe");
	$("#" + formid + " #act").val("hapusFile");
	goSubmit(formid);
}

function goDoX(file,act,formid) {
	if(!formid)
		formid = "perpusform";
	$("#act").val(act);
	//alert("TES: "+file+" "+act);
	// $("#contents").postpage(file,$("#" + formid).serializeArray());
	$("#contents").divpost({page: file, sent: $("#" + formid).serializeArray()});
}

function goDoXX(file,actx,formid) {
	if(!formid)
		formid = "perpusform";
	$("#actx").val(actx);
	// $("#contents").postpage(file,$("#" + formid).serializeArray());
	$("#contents2").divpost({page: file, sent: $("#" + formid).serializeArray()});
}

function goLinX(file,sent) {
	$("#header").chooselinx(file,sent);
}

function goPostX(file,sent) {
	// $("#contents").postpage(file,sent);
	$("#contents").divpost({page: file, sent: sent});
}

function goPostXX(file,sent) {
	// $("#contents").postpage(file,sent);
	$("#contents2").divpost({page: file, sent: sent});
}

/* validasi form edit */

function arHighlight(arrvar) {
	var i, err = false, werr;
	
	// arrvar[0]: id, arrvar[1]: format regular expression, arrvar[3]: apakah harus diisi
	for(i=0;i<arrvar[0].length;i++) {
		e = document.getElementById(arrvar[0][i]);
		if(e != null) {
			werr = false;
			if(e.value != "" && arrvar[1][i] && !e.value.match(arrvar[1][i]))
				werr = true;
			else if(e.value == "" && arrvar[2][i])
				werr = true;
			
			if(werr) {
				e.className = "ControlErr";
				e.onfocus = function () { this.className = "ControlStyle"; }
				err = true;
			}
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang berwana kuning sesuai dengan format.");
		return false;
	}
	return true;
}

function cfAlert(id,alertmsg,stat) {
	if(!stat)
		return false;
	if(document.getElementById(id) != null && document.getElementById(id).value == "") {
		alert(alertmsg);
		document.getElementById(id).focus();
		return false;
	}
	return true;
}

function cfHighlight(csv) {
	var i, err = false;
	var aid = csv.split(",");
	
	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(e != null && e.value.trim() == "") {
				e.className = "ControlErr";
				e.onfocus = function () { this.className = "ControlStyle"; }
				err = true;
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(e != null && e.value.trim() == "") {
			e.className = "ControlErr";
			e.onfocus = function () { this.className = "ControlStyle"; }
			err = true;
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang berwana kuning terlebih dahulu.");
		return false;
	}
	return true;
}

function reHighlight(csv,reformat,notnull) {
	var i, err = false, werr = false;
	var aid = csv.split(",");
	
	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(e != null) {
				werr = false;
				if(e.value != "" && reformat && !e.value.match(reformat))
					werr = true;
				else if(e.value == "" && notnull)
					werr = true;
				
				if(werr) {
					e.className = "ControlErr";
					e.onfocus = function () { this.className = "ControlStyle"; }
					err = true;
				}
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(e != null) {
			if(e.value != "" && reformat && !e.value.match(reformat))
				err = true;
			else if(e.value == "" && notnull)
				err = true;
			
			if(err) {
				e.className = "ControlErr";
				e.onfocus = function () { this.className = "ControlStyle"; }
			}
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang berwana kuning sesuai dengan format.");
		return false;
	}
	return true;
}

/* untuk paging list */

function goFirst(page) {
	if(page > 1)
		goPostX(phpself,"key=" + key + "&numpage=1" + sent);
}

function goPrev(page) {
	if(page > 1)
		goPostX(phpself,"key=" + key + "&numpage=" + (page-1) + sent);
}

function goNext(page,last) {
	if(page < last){
			goPostX(phpself,"key=" + key + "&numpage=" + (page+1) + sent);
	}
}

function goLast(page,last) {
	if(page < last)
		goPostX(phpself,"key=" + key + "&numpage=" + last + sent);
}
/*
(function($){
$.fn.txtFieldDateFormat = function(options) {
		var settings = $.extend({
			point: true
		}, options);
		
		$(this).change(function() {
			dtString = $(this).val();
			if(dtString.length == 8) {
				var newdate = dtString.substring(0,2)+'-'+dtString.substring(2,4)+'-'+dtString.substring(4,8);
				$(this).val(newdate);
			}
			else if(dtString.length == 10) {
				var newdate = dtString.substring(0,2)+'-'+dtString.substring(3,5)+'-'+dtString.substring(6,10);
				$(this).val(newdate);
			};
		});
	};
})(jQuery);*/