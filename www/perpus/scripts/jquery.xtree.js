(function($){
	var ctid, page, add, remove, pcid, pcname;
	
	$.fn.filterTreeFromXML = function(options) {
		var settings = jQuery.extend({
			xml: "", ctid: "", page: "", init: ""
		}, options);
		
		ctid = settings.ctid;
		page = settings.page;
		
		return $(this).each(function() {
			kutab = $('<table width="100%"></table>').appendTo($(this));
			kutr1 = $('<tr></tr>').appendTo(kutab);
			kutr2 = $('<tr></tr>').appendTo(kutab);
			kutd1 = $('<td height="100%" nowrap></td>').appendTo(kutr1);

			menudiv = $('<div style="height:400px;width:220px;overflow:auto;font-size:9px;"></div>').appendTo(kutd1);
			radiotd = $('<td align="center"><input type="button" id="goradio" value="Tampilkan">&nbsp; &nbsp; &nbsp; &nbsp;<input type="button" id="refresh" value="Refresh"></td>').appendTo(kutr2);
			
			list = document.createElement("ul");
			list = $(list).appendTo(menudiv);
			
			$.get(settings.xml, {}, function(data) {
				$("item",data).each(function() {
					category = $("category",$(this)).text();
					key = category.toLowerCase().replace(" ","");
					
					li = document.createElement("li");
					li = $(li).appendTo(list);
					li.html("<span>" + category + "</span>");
					
					ul = document.createElement("ul");
					ul = $(ul).appendTo(li);
					
					clevel = 1;
					$(this).children("linx").each(function() {
						label = $(this).children("label").text();
						value = $(this).children("value").text();
						level = $(this).children("level").text();
						
						if(level) {
							if(level > clevel)
								ul = $("<ul></ul>").appendTo(lii);
							else if(level < clevel)
								ul = ul.parents("ul:eq(" + ((clevel - level)-1) + ")");
							clevel = level;
						}
						
						lii = document.createElement("li");
						lii = $(lii).appendTo(ul);
						lii.html('<input type="radio" id="cbfilter" name="' + key + '" value="' + value + '"><a id="menulink" href="' + key + "#" + value + '">' + label + '</a>');
					});
				});
				
				list.filterAfterEffect();
				
				if(settings.init == "") {
					$("#" + ctid).divpost({page: page});
				}
				else {
					arrlp = settings.init.split("#");
					
					ctcat = arrlp[0];
					ctstr = arrlp[1];
					
					if(ctcat.indexOf(",") == -1) {
						$("a[href='" + ctcat + "#" + ctstr + "']").addClass("selected");
					}
					else {
						arcat = ctcat.split(",");
						arstr = ctstr.split(",");
						
						for(i=0;i<arcat.length;i++) {
							$("input[type='radio'][name='"+arcat[i]+"'][value='"+arstr[i]+"']").attr("checked",true);
						}
					}
					
					$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr});
				}
			});
		});
	};
	
	$.fn.filterAfterEffect = function(init) {
		return $(this).each(function() {
			// memberi collapsion :p ke tree menu
			$(this).treeview({
				animated: "fast",
				collapsed: false,
				unique: false
			});
			
			// memberi bind ke filter radio
			$("#goradio").click(function() {
				var kode,cat,strcat,strstr;
				var arcat = new Array();
				var arstr = new Array();
				
				$("input[id='cbfilter']:checked").each(function() {
					arstr.push($(this).val());
					arcat.push($(this).attr("name"));
				});
				
				ctcat = arcat.join(',');
				ctstr = arstr.join(',');
				
				$("a[id='menulink']").removeClass("selected");
				$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr + "&page=1"});
			});
			
			// memberi bind ke refresh
			$("#refresh").click(function() {
				$("a[id='menulink']").removeClass("selected");
				$("input[id='cbfilter']:checked").attr("checked",false);
				// $("#" + ctid).divpost({page: page, sent: "cat=&str=&page=1"});
				if(location.search == "")
					location.href = location.href+"?r=1";
				else
					location.href = location.href+"&r=1";
			});
			
			// memberi bind ke link
			$("a[id='menulink']").click(function(e) {
				e.preventDefault();
				$("a[id='menulink']").removeClass("selected");
				$(this).addClass("selected");
				
				arhref = $(this).attr("href").split("/");
				arrlp = arhref[(arhref.length)-1].split("#");
				
				ctcat = arrlp[0];
				ctstr = arrlp[1];
				
				$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr + "&page=1"});
			});
		});
	};
	
	$.fn.filtercTreeFromXML = function(options) {
		var settings = jQuery.extend({
			xml: "", ctid: "", page: "", init: ""
		}, options);
		
		ctid = settings.ctid;
		page = settings.page;
		
		return $(this).each(function() {
			kutab = $('<table width="100%"></table>').appendTo($(this));
			kutr1 = $('<tr></tr>').appendTo(kutab);
			kutr2 = $('<tr></tr>').appendTo(kutab);
			kutd1 = $('<td height="100%" nowrap></td>').appendTo(kutr1);

			menudiv = $('<div style="height:400px;width:220px;overflow:auto;font-size:9px;"></div>').appendTo(kutd1);
			radiotd = $('<td align="center"><input type="button" id="gocheck" value="Tampilkan">&nbsp; &nbsp; &nbsp; &nbsp;<input type="button" id="refresh" value="Refresh"></td>').appendTo(kutr2);
			
			list = document.createElement("ul");
			list = $(list).appendTo(menudiv);
			
			$.get(settings.xml, {}, function(data) {
				$("item",data).each(function() {
					category = $("category",$(this)).text();
					key = category.toLowerCase().replace(" ","");
					
					li = document.createElement("li");
					li = $(li).appendTo(list);
					li.html("<span>" + category + "</span>");
					
					ul = document.createElement("ul");
					ul = $(ul).appendTo(li);
					
					clevel = 1;
					$(this).children("linx").each(function() {
						label = $(this).children("label").text();
						value = $(this).children("value").text();
						level = $(this).children("level").text();
						
						if(level) {
							if(level > clevel)
								ul = $("<ul></ul>").appendTo(lii);
							else if(level < clevel)
								ul = ul.parents("ul:eq(" + ((clevel - level)-1) + ")");
							clevel = level;
						}
						
						lii = document.createElement("li");
						lii = $(lii).appendTo(ul);
						lii.html('<input type="checkbox" id="cbfilter" name="' + key + '" value="' + value + '"><a id="menulink" href="' + key + "#" + value + '">' + label + '</a>');
					});
				});
				
				list.filtercAfterEffect();
				
				if(settings.init == "") {
					$("#" + ctid).divpost({page: page});
				}
				else {
					arrlp = settings.init.split("#");
					
					ctcat = arrlp[0];
					ctstr = arrlp[1];
					
					if(ctcat.indexOf(",") == -1) {
						$("a[href='" + ctcat + "#" + ctstr + "']").addClass("selected");
					}
					else {
						arcat = ctcat.split(",");
						arstr = ctstr.split(",");
						
						for(i=0;i<arcat.length;i++) {
							$("input[type='checkbox'][name='"+arcat[i]+"'][value='"+arstr[i]+"']").attr("checked",true);
						}
					}
					
					$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr});
				}
			});
		});
	};
	
	$.fn.filtercAfterEffect = function(init) {
		return $(this).each(function() {
			// memberi collapsion :p ke tree menu
			$(this).treeview({
				animated: "fast",
				collapsed: false,
				unique: false
			});
			
			// memberi bind ke filter checkbox
			$("#gocheck").click(function() {
				var kode,cat,strcat,strstr;
				var arcat = new Array();
				var arstr = new Array();
				
				$("input[id='cbfilter']:checked").each(function() {
					arstr.push($(this).val());
					arcat.push($(this).attr("name"));
				});
				
				ctcat = arcat.join(',');
				ctstr = arstr.join(',');
				
				$("a[id='menulink']").removeClass("selected");
				$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr + "&page=1"});
			});
			
			// memberi bind ke refresh
			$("#refresh").click(function() {
				$("a[id='menulink']").removeClass("selected");
				$("input[id='cbfilter']:checked").attr("checked",false);
				// $("#" + ctid).divpost({page: page, sent: "cat=&str=&page=1"});
				if(location.search == "")
					location.href = location.href+"?r=1";
				else
					location.href = location.href+"&r=1";
			});
			
			// memberi bind ke link
			$("a[id='menulink']").click(function(e) {
				e.preventDefault();
				$("a[id='menulink']").removeClass("selected");
				$(this).addClass("selected");
				
				arhref = $(this).attr("href").split("/");
				arrlp = arhref[(arhref.length)-1].split("#");
				
				ctcat = arrlp[0];
				ctstr = arrlp[1];
				
				$("#" + ctid).divpost({page: page, sent: "cat=" + ctcat + "&str=" + ctstr + "&page=1"});
			});
		});
	};
	
	$.fn.editTreeFromXML = function(options) {
		var settings = jQuery.extend({
			xml: "", ctid: "", page: "", add: false, remove: false
		}, options);
		
		ctid = settings.ctid;
		page = settings.page;
		add = settings.add;
		remove = settings.remove;
		
		return $(this).each(function() {
			kutab = $('<table width="100%" id="x"></table>').appendTo($(this));
			kutr1 = $('<tr></tr>').appendTo(kutab);
			kutd1 = $('<td height="100%" nowrap></td>').appendTo(kutr1);
			menudiv = $('<div style="height:400px;width:220px;overflow:auto;"></div>').appendTo(kutd1);
			
			list = document.createElement("ul");
			list = $(list).appendTo(menudiv);
			
			// mendapatkan isi menu
			$.get(settings.xml, {}, function(data) {
				$("item",data).each(function() {
					category = $("category",$(this)).text();
					
					li = document.createElement("li");
					li = $(li).appendTo(list);
					li.html("<span>" + category + "</span>");
					
					ul = document.createElement("ul");
					ul = $(ul).appendTo(li);
					
					clevel = 1;
					$(this).children("linx").each(function() {
						label = $(this).children("label").text();
						value = $(this).children("value").text();
						level = $(this).children("level").text();
						
						if(level) {
							if(level > clevel)
								ul = $("<ul></ul>").appendTo(lii);
							else if(level < clevel)
								ul = ul.parents("ul:eq(" + ((clevel - level)-1) + ")");
							clevel = level;
						}
						
						lii = document.createElement("li");
						lii = $(lii).appendTo(ul);
						lii.html('<a id="menulink" href="' + value + '">' + label + '</a>');
						if(add)
							lii.append(' &nbsp; <img id="addt" src="images/add.png" style="cursor:pointer;">');
						if(remove && level > 1)
							lii.append(' &nbsp; <img id="delt" src="images/delete.png" style="cursor:pointer;">');
					});
				});
				
				list.editAfterEffect();
				if(location.search != '') {
					linkid = location.search.substr(18);
					$("a[href='" + linkid + "']").click();
				}
			});
		});
	};
	
	$.fn.editAfterEffect = function() {
		return $(this).each(function() {
			// memberi collapsion :p ke tree menu
			$(this).treeview({
				animated: "fast",
				collapsed: false,
				unique: false
			});
			
			if(add) {
				$("img[id='addt']").click(function() {
					$("a[id='menulink']").removeClass("selected");
					a = $(this).siblings("a");
					a.addClass("selected");
					$("#" + ctid).divpost({page: page, sent: "act=tambahanak&key=" + a.attr("href")});
				});
			}
			
			if(remove) {
				$("img[id='delt']").click(function() {
					a = $(this).siblings("a");
					hapus = confirm('Apakah anda yakin akan menghapus data "' + a.text() + '"?');
					if(hapus)
						$("#" + ctid).divpost({page: page, sent: "act=hapus&key=" + a.attr("href")});
				});
			}
			
			// memberi bind ke link
			$("a[id='menulink']").click(function(e) {
				e.preventDefault();
				$("a[id='menulink']").removeClass("selected");
				$(this).addClass("selected");
				
				arhref = $(this).attr("href").split("/");
				href = arhref[(arhref.length)-1];
				$("#" + ctid).divpost({page: page, sent: "act=edit&key=" + href});
			});
		});
	};
	
	$.fn.popupTreeFromXML = function(options) {
		var settings = jQuery.extend({
			xml: "", ctid: "", pcid: "", pcname: ""
		}, options);
		
		ctid = settings.ctid;
		pcid = settings.pcid;
		pcname = settings.pcname;
		
		return $(this).each(function() {
			kutab = $('<table width="100%"></table>').appendTo($(this));
			kutr1 = $('<tr></tr>').appendTo(kutab);
			kutd1 = $('<td height="100%" nowrap></td>').appendTo(kutr1);
			menudiv = $('<div style="font-family:Verdana, Arial, Helvetica, sans-serif;font-size:11px;"></div>').appendTo(kutd1);
			
			list = document.createElement("ul");
			list = $(list).appendTo(menudiv);
			
			// mendapatkan isi menu
			$.get(settings.xml, {}, function(data) {
				$("item",data).each(function() {
					category = $("category",$(this)).text();
					
					li = document.createElement("li");
					li = $(li).appendTo(list);
					li.html('<span style="font-weight:bold;">' + category + '</span>');
					
					ul = document.createElement("ul");
					ul = $(ul).appendTo(li);
					
					clevel = 1;
					$(this).children("linx").each(function() {
						label = $(this).children("label").text();
						value = $(this).children("value").text();
						level = $(this).children("level").text();
						
						if(level) {
							if(level > clevel)
								ul = $("<ul></ul>").appendTo(lii);
							else if(level < clevel)
								ul = ul.parents("ul:eq(" + ((clevel - level)-1) + ")");
							clevel = level;
						}
						
						lii = document.createElement("li");
						lii = $(lii).appendTo(ul);
						lii.html('<a id="menulink" href="' + value + '">' + label + '</a>');
					});
				});
				
				list.popupAfterEffect();
			});
		});
	};
	
	$.fn.popupAfterEffect = function() {
		return $(this).each(function() {
			// memberi collapsion :p ke tree menu
			$(this).treeview({
				animated: "fast",
				collapsed: false,
				unique: false
			});
			
			// memberi bind ke link
			$("a[id='menulink']").click(function(e) {
				e.preventDefault();
				
				arhref = $(this).attr("href").split("/");
				href = arhref[(arhref.length)-1];
				
				window.opener.$("#" + pcid).val(href);
				window.opener.$("#" + pcname).val($(this).text());
				window.close();
			});
		});
	};
})(jQuery);