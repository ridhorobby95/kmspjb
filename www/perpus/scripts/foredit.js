/* fungsi umum gogogogo */

	var ajaxpage = "index.php?page=ajax";

function goHalDetail(file,key) {
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value = key;
	goSubmit();
}

function goSubmit() {
	document.getElementById("perpusform").submit();
}

function AutoType(file,table,locate,where) {	
	file = file;
      $.ajax({
          type: "post",
          url: file,
          data: "table="+table+"&locate="+locate+"&where="+where,
          cache: false,
          success: function(data){
			var xdata = data.split("|~|");
			var loc = xdata[0].split(",");
			var hasil = xdata[1].split("*");
			if(hasil=='')
			alert('Data tidak ditemukan');
			else {
				for(i=0;i<loc.length;i++){
				$("#"+loc[i]).val(hasil[i]);
				}
			}
        }
      });
      return false;
}


/* fungsi aksi */

function goDelete() {
	var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
	if(hapus) {
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goSave() {
	document.getElementById("act").value = "simpan";
	goSubmit();
}

/* untuk popup jaaaaaaaaaa */
function popup(url,width,height) 
{
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}

function goUndo() {
	document.getElementById("perpusform").reset();
}

/* validasi input */

function cfAlert(id,alertmsg,stat) {
	if(!stat)
		return false;
	if(document.getElementById(id) != null && document.getElementById(id).value == "") {
		alert(alertmsg);
		document.getElementById(id).focus();
		return false;
	}
	return true;
}

function noFormat(str) {
	if(str == "")
		return "";
	else {
		var nof = str.replace(/[\.\(\)]/g,''); 
		return parseInt(nof);
	}
}

function numberFormat(num) {
	var bracket = false;
	var ret = '';
	var j = 0;
	
	num = parseInt(num);
	if (num < 0){
		num = Math.abs(num);
		bracket = true;
	}
	
	num = String(num);
	for(i=num.length-1;i>=0;i--) {
		if(j == 3) {
			ret = "." + ret;
			j = 0;
		}
		ret = num.charAt(i) + ret;
		j++;
	}
	
	if (bracket)
		return '('+ret+')';
	else
		return ret;
}

function cfHighlight(csv) {
	var i, err = false;
	var aid = csv.split(",");
	
	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(e != null && e.value.trim() == "") {
				e.className = "ControlErr";
				e.onfocus = function () { this.className = "ControlStyle"; }
				err = true;
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(e != null && e.value.trim() == "") {
			e.className = "ControlErr";
			e.onfocus = function () { this.className = "ControlStyle"; }
			err = true;
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang berwana kuning terlebih dahulu.");
		return false;
	}
	return true;
}

//Fungsi Watermark Textbox dan perubahan background ; ketentuan: value texbox dan text (parameter kedua dari watermark) harus sama.
/*
function watermark(inputId,text){
  var inputBox = document.getElementById(inputId);
    if (inputBox.value.length > 0){
      if (inputBox.value == text){
        inputBox.value = '';
		inputBox.style.backgroundColor='#F8F670';
		}
    }
    else {
      inputBox.value = text;
	  inputBox.style.backgroundColor='#FFFFFF';
	  }
}
*/
