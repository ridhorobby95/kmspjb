// bagian DAFTAR

function goDetail(file,key) {
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value = key;
	goSubmit();
}

function goNew(file) {
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value = '';
	goSubmit();
}

// bagian DETIL

function goDelete() {
	var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
	if(hapus) {
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goSave() {
	document.getElementById("act").value = "simpan";
	goSubmit();
}

function goUndo() {
	document.getElementById("perpusform").reset();
}

// bagian AJAX-ED WITH JQUERY

function goDoX(file,act) {
	document.getElementById("act").value = act;
	$("#contents").postpage(file,$("#perpusform").serializeArray());
}

function goLinX(file,sent) {
	$("#header").chooselinx(file,sent);
}

function goPostX(file,sent) {
	$("#contents").postpage(file,sent);
}

function waitStart(id,forparent) {
	if(forparent) {
		ctr = parent.$("#" + id);
		div = parent.$("#progress");
	}
	else {
		ctr = $("#" + id);
		div = $("#progress");
	}
	
	ctr.css("opacity",0.5);
	ctr.css("filter","alpha(opacity=50)");
	
	ctrofs = ctr.offset();
	divleft = ctrofs.left + (ctr.width()/2) - (div.width()/2);
	divtop = ctrofs.top + (ctr.height()/2) - (div.height()/2);
	
	div.css("left",divleft);
	div.css("top",divtop);
	// div.show();
	div.css("visibility","visible");
}

function waitFinish(id,forparent) {
	if(forparent) {
		ctr = parent.$("#" + id);
		div = parent.$("#progress");
	}
	else {
		ctr = $("#" + id);
		div = $("#progress");
	}
	
	ctr.css("opacity",1);
	ctr.css("filter",null);
	// div.hide();
	div.css("visibility","hidden");
}

// bagian INPLACE

function goDeleteIP(key,label) {
	var hapus = confirm('Apakah anda yakin akan menghapus data "' + label + '"?');
	if(hapus) {
		document.getElementById("key").value = key;
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goEditIP(key) {
	document.getElementById("key").value = key;
	document.getElementById("act").value = "sunting";
	goSubmit();
}

function goInsertIP() {
	document.getElementById("act").value = "insersi";
	goSubmit();
}

function goUpdateIP(key) {
	document.getElementById("key").value = key;
	document.getElementById("act").value = "update";
	goSubmit();
}

// bagian UMUM

function goSubmit() {
	document.getElementById("perpusform").submit();
}

function goTo(file) {
	location.href = file;
}

function initScroll(scrolln) {
	document.body.scrollTop = scrolln;
}

function totalLT(elem) {
	var total = new Array();
	total[0] = 0; total[1] = 0;
	var celem = elem;
	
	while(celem) {
		if(celem.offsetLeft != null)
			total[0] += celem.offsetLeft;
		if(celem.offsetTop != null)
			total[1] += celem.offsetTop;
		celem = celem.parentNode;
	}
	
	return total;
}