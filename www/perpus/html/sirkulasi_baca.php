<?php
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Transaksi Tugas Akhir';
	$p_title = 'Transaksi Tugas Akhir';
	
	if(isset($_POST['btnbaca'])){
		$idlogin=Helper::removeSpecial($_POST['idanggotalogin']);
		$passwd=Helper::removeSpecial($_POST['password']);
		$salt = $Csalt;
		$pass=md5(md5($passwd.$salt).$salt);
		$isLogin=$conn->GetRow("select idanggota,tglexpired from ms_anggota where idanggota='$idlogin' and password='$pass'");
		//coba
		$_SESSION['idanggotata']=$idlogin;
		Helper::redirect();
		
		// if ($isLogin){
			// if($isLogin['tglexpired']>= date('Y-m-d') or $isLogin['tglexpired']==''){
				// $_SESSION['idanggotata']=$idlogin;
				// Helper::redirect();
			// }else{
				// $errdb = 'Anggota Telah Expired.';
				// Helper::setFlashData('errdb', $errdb); 	
				// Helper::redirect();
			// }
		// }else {
			// $errdb = 'Masukkan Id Anggota dan Password dengan benar.';
			// Helper::setFlashData('errdb', $errdb); 	
			// Helper::redirect();
		// }
	
	}
	if($_SESSION['idanggotata']!=''){
	$anggota=$conn->GetRow("select *,j.namajenisanggota from ms_anggota a
							join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
							where a.idanggota='".$_SESSION['idanggotata']."'");
	$p_foto = Config::dirFoto.'anggota/'.trim($_SESSION['idanggotata']).'.jpg';
	$p_hfoto = Config::fotoUrl.'anggota/'.trim($_SESSION['idanggotata']).'.jpg';
	$p_mhsfoto = Config::dirFotoMhs.trim($_SESSION['idanggotata']).'.jpg';
	$p_pegfoto = Config::dirFotoPeg.trim($_SESSION['idanggotata']).'.jpg';
	
	$transnow=$conn->Execute("select * from v_trans_list where tglpengembalian is null 
							  and idanggota='".$_SESSION['idanggotata']."' and tgltenggat=tgltransaksi and kdjenispustaka in(".$_SESSION['roleakses'].")");
	$transe=$conn->Execute("select * from v_trans_list where idanggota='".$_SESSION['idanggotata']."' and tgltenggat=tgltransaksi  and kdjenispustaka in(".$_SESSION['roleakses'].") order by idtransaksi desc  limit 15");
	
	$p_cektenggat=$conn->GetRow("select idtransaksi from pp_transaksi where idanggota='".$_SESSION['idanggotata']."' and tgltenggat<current_date and tglpengembalian is null");
	$p_skors = $conn->GetRow("select keterangan from pp_skorsing where idanggota='".$_SESSION['idanggotata']."' order by idskorsing desc");
	
	
	}

	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_seri=Helper::removeSpecial($_POST['txtpustaka']);
		$r_anggota=Helper::removeSpecial($_POST['i_idanggota']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		$r_key3=Helper::removeSpecial($_POST['key3']);
		
		if($r_aksi=='pinjambaca'){
			$getEks=$conn->GetRow("select e.ideksemplar,e.statuseksemplar,p.kdjenispustaka from pp_eksemplar e
								   join ms_pustaka p on e.idpustaka=p.idpustaka
								   where upper(e.noseri)=upper('$r_seri') and e.kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
			if(!$getEks){
				$errdb = 'Data Pustaka tidak ditemukan.';
				Helper::setFlashData('errdb', $errdb); 	
			} else {
				if ($getEks['statuseksemplar']!='ADA') {
					$errdb = 'Pustaka dalam status terpinjam atau berstatus proses';
					Helper::setFlashData('errdb', $errdb); 	
					//Helper::redirect();
				}else {
					$perjenis=$conn->GetRow("select namajenispustaka,isbaca from lv_jenispustaka where kdjenispustaka='".$getEks['kdjenispustaka']."'");
					$trans_count=$conn->Execute("select idtransaksi from v_trans_list where idanggota ='".$_SESSION['idanggotata']."' and kdjenispustaka='".$getEks['kdjenispustaka']."' and tgltransaksi=current_date and tgltransaksi=tgltenggat");
					$t_count=$trans_count->RowCount();
					if($t_count>=$perjenis['isbaca'] and $perjenis['isbaca']!=0) {
						$errdb = 'Transaksi Baca '.$perjenis['namajenispustaka'].' pada hari ini telah maksimal.';
						Helper::setFlashData('errdb', $errdb); 	
					}else{
						$record['kdjenistransaksi']='PJN';
						$record['ideksemplar']=$getEks['ideksemplar'];
						//cek user anggota/petugas
						if($_SESSION['idanggotata']!=$_SESSION['PERPUS_USER'] or $r_anggota==''){
							$record['idanggota']=$_SESSION['idanggotata'];
							$record['fix_status']=0;
						} else {
							$record['idanggota']=$r_anggota;
							$record['fix_status']=1;
						}
						
						$record['tgltransaksi']=date('Y-m-d');
						$record['tgltenggat']=date('Y-m-d');
						$record['statustransaksi']=1;
						$record['fix_status']=0;
						Helper::IdentitasBaca($record,$_SESSION['idanggotata']);
						
						$conn->StartTrans();
						Sipus::InsertBiasa($conn,$record,pp_transaksi);
						
						$receks['statuseksemplar']='PJM';
						Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,ideksemplar,$getEks['ideksemplar']);
						
						$conn->CompleteTrans();
						
							if($conn->ErrorNo() != 0){
								$errdb = 'Transaksi Baca Pustaka Gagal.';	
								Helper::setFlashData('errdb', $errdb);
								}
							else {
								if($_SESSION['idanggotata']!=$_SESSION['PERPUS_USER'] or $r_anggota=='')
								$sucdb = 'Transakai Baca Pustaka Berhasil.';
								else
								$sucdb = 'Transaksi Baca Pustaka untuk Anggota '.$record['idanggota'].' Berhasil';
								Helper::setFlashData('sucdb', $sucdb);
								//Helper::redirect();
							}
					}
				}
			}
		}
		else if($r_aksi=='delsession') {
		
			$conn->Execute("update pp_transaksi set fix_status='1' where idanggota = '".$_SESSION['idanggotata']."'");
			unset($_SESSION['idanggotata']);
			$sucdb = 'Proses transaksi telah selesai';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		
		}
		else if ($r_aksi=='batal') {
			$p_eks=$conn->GetRow("select max(ideksemplar) as ideksemplar from pp_eksemplar where noseri='$r_key2'");
			$ideksemplar=$p_eks['ideksemplar'];
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_transaksi,idtransaksi,$r_key);
			//update status
			$rec['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);

			$conn->CompleteTrans();
			
				if($conn->ErrorNo() != 0){
					$errdb = 'Transaksi gagal dibatalkan.';	
					Helper::setFlashData('errdb', $errdb);
					}
					else {
					$sucdb = 'Transaksi sukses dibatalkan.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect();
					}
			}
			else if ($r_aksi=='kembali') {
			$hariini=date("Y-m-d");
			$err=Sipus::fastback($conn,$r_key2,$hariini,'nolog');
			 
			if($err != 0){
				$errdb = 'Pengembalian peminjaman gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pengembalian peminjaman berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
		}
	
	
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="<?= $_SESSION['idanggotata']=='' ? "document.getElementById('idanggotalogin').focus()" : "document.getElementById('txtpustaka').focus()" ?>">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<? if($_SESSION['idanggotata']==''){ ?>
<!--<div align="center" style="position:relative;top:-11px;">
	<div class="loginBox">
		<div class="loginTitle">SIRKULASI TUGAS AKHIR</div>
		<div class="loginContent"> -->
		<table width="400" border=0 align="center" cellpadding="6" cellspacing=0 style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
			<tr height="25">
				<th colspan="2">SIRKULASI BACA</th>
			</tr>
			<tr>
				<td width="130"><b>ID ANGGOTA</b></td>
				<td><input type="text" maxlength="20" name="idanggotalogin" class="ControlStyleT" id="idanggotalogin"></td>
			</tr>
			<tr>
				<td><b>PASSWORD</b></td>
				<td><input type="password" name="password" id="password" class="ControlStyleT"></td>
			</tr>
			<tr>
				<td align="center" colspan=2><input type="submit" name="btnbaca" id="btnbaca" value="Mulai Transaksi" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:150px;font-size:12px;"></td>
			</tr>
		</table>
	<!--	</div>
	</div>
</div> -->
<? include_once('_notifikasi.php'); ?>
<? } else { ?>
	<table cellpadding="0" cellspacing="0" align="center" width="600"  style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
		<tr height="25">
			<th colspan="4">Sirkulasi Baca</td>
		</tr>
		<tr height="10">
			<td>&nbsp;</td>
		</tr>
		<tr height="22">
			<td width=160 ><b>Id Anggota</b></td>
			<td width=10>:</td>
			<td> <b><?= $anggota['idanggota'] ?></b>
			</td>
			<td rowspan=5 align="center">
			<? if(is_file($p_foto)) { ?>
			<img border="1" id="imgfoto" src="<?= $p_hfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
			<? } else{
				if($anggota['kdjenisanggota'] =='D' or $anggota['kdjenisanggota']=='T'){
			?>
			<img border="1" id="imgfoto" src="<?= $p_pegfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
			<? } elseif ($anggota['kdjenisanggota'] =='M') { ?>
			<img border="1" id="imgfoto" src="<?= $p_mhsfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
			<? } else { ?>
			<img border="1" id="imgfoto" src="<?= Config::fotoUrl.'default1.jpg' ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
			<? } } ?>			<!-- <img src="<?//= (is_file($p_foto) ? $p_hfoto : (@fopen($p_pegfoto,'r') ? $p_pegfoto : (@fopen($p_mhsfoto,'r') ? $p_mhsfoto : Config::fotoUrl.'default1.jpg'))) ?>" width="110" height="110" border=1></td>-->
		</tr>
		<tr height="22">
			<td ><b>Nama Anggota </b></td>
			 <td width="10">: </td>
			 <td width="300"><b><?= $anggota['namaanggota'] ?></b></td>
			
		</tr>
		<tr height="22">
			<td><b>Jenis Keanggotaan</b></td>
			<td width="10">: </td>
			<td> <?= $anggota['namajenisanggota'] ?></td>
		</tr>		
		<tr height="22">
			<td valign="top"><b>Alamat</b></td>
			<td width="10" valign="top">: </td>
			<td > <?= $anggota['alamat'] ?></td>
		</tr>
		<tr height="22">
			<td><b>Tanggal Registrasi</b></td>
			<td width="10">: </td>
			<td> <?= Helper::formatDateInd($anggota['tgldaftar']) ?></td>
		</tr>
		<? if($anggota['tglexpired']!='') {?>
		<tr height="22">
			<td><b>Tanggal Expired</b></td>
			<td width="10">: </td>
			<td> <?= Helper::formatDateInd($anggota['tglexpired']) ?></td>
			<td>&nbsp;</td>
		</tr>	
		<? } ?>
		<? if ($anggota['statusanggota'] !=1){ ?>
		<tr height="22">
			<td colspan="4" align="center"><font color="red"><b>
			<script>
			var sts="Anggota tersebut telah nonaktif [ <?= $anggota['catatan'] ?> ] !!!";
			document.write(sts.blink());
			</script>
			<? $aktive='block' ?>
			</b></font></td>
		</tr>
		<? }else if($anggota['statusanggota']==2){ ?>
			<tr height="22">
			<td colspan="4" align="center"><font color="red"><b>
			<script>
			var sts="Anggota tersebut telah terblokir !!!";
			document.write(sts.blink());
			</script>
			<? $aktive='block' ?>
			</b></font></td>
		</tr>	
		<? }else { ?>
		<? if ($anggota['tglselesaiskors'] >= date("Y-m-d")){ ?>
		<tr height="22">
			<td colspan="4" align="center"><font color="red"><b>
			<script>
			var skor="Anggota terkena masa skors [ <?= $p_skors['keterangan'] ?> ] sampai tanggal <?= Helper::formatDateInd($anggota['tglselesaiskors']) ?>.";
			document.write(skor.blink());
			</script>
			<? //$aktive='block' ?>
			</b></font></td>
		</tr>
		<? } ?>
		<? if($p_cektenggat) { ?>
		<tr height="22">
			<td colspan="4" align="center"><font color="red"><b>
			<script>
			var tenggat="Terdapat pustaka belum dikembalikan yang melewati masa tenggat.";
			document.write(tenggat.blink());
			</script>
			<? //$aktive='block' ?>
			</b></font></td>
		</tr>
		<? } }?>
	</table>
		<table style="padding:5px 5px 3px 20px;margin:5px 0px 5px 490px;background-image:none;border-width:1px;width:110px;height:30px;" class="GridStyle">
		<tr>
			<td align="center">
				<span class="out" name="btnselesai" id="btnselesai" value="Selesai" onClick="goClear()" style="padding-right:0px;cursor:pointer;"></span><u style="cursor:pointer;" onClick="goClear()">Selesai</u>
			</td>
		</td>
		</table><br>
	<table width="99%">
	<tr>	
		<td>
		<div class="tabs" style="width:100%">
		<ul>
			<li><a id="tablink" href="javascript:void(0)" onClick="goTab(0)">Peminjaman</a></li>
			
			<li><a id="tablink" href="javascript:void(0)"  onclick="goTab(3)">Sejarah Peminjaman</a></li>
			
			<div class="a" align="right" valign="center"><? include_once('_notifikasi_trans.php'); ?><br></div>
		
		</ul>

<!-- ================================= transaksi peminjaman umum============================ -->

	<div id="items" style="position:relative;top:-2px;">
		<table border=0 width="100%" cellpadding=0 cellspacing=0>
			<tr height="35">
				<? if($_SESSION['idanggotata']==$_SESSION['PERPUS_USER']){ ?>
				<td width="120" class="LeftColumnBG" style="border:none;">Id Anggota</td>
				<td width="190" class="LeftColumnBG" style="border:none;">: 
				<input type="text" name="i_idanggota" id="i_idanggota" maxlength="20" size="22">
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota',630,500);">
				</td>
				<? } ?>
				<td width="150" class="LeftColumnBG" style="border:none;">Masukkan NO INDUK</td>
				<td width="170" class="LeftColumnBG" style="border:none;">: 
				<? if($aktive=='block') { ?>
				<input type="text" name="txtpustakad" id="txtpustakad" maxlength="20" size="22" class="ControlRead" disabled>
				<? } else { ?>
				<input type="text" name="txtpustaka" id="txtpustaka" maxlength="20" size="22" onKeyDown="etrPinjamTA(event);">
				<? } ?>
				</td>
				<td class="LeftColumnBG"  style="border:none;">
				<? if($aktive=='block') { ?>
				<input type="button" name="btnta" id="btnta" value="Pinjam Baca" class="buttonSmallDis" style="height:23px;width:130px;font-size:12px;">
				<? } else { ?>
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam',700,500);">
				<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
				&nbsp;
				<input type="button" name="btnta" id="btnta" value="Pinjam Baca" onClick="goPinjamTA()" class="buttonSmall" style="height:23px;width:130px;font-size:12px;">
				
				<? } ?>
				</td>
				
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
				<td width="6%" nowrap align="center" class="SubHeaderBGAlt">Batal</td>
				<td width="6%" nowrap align="center" class="SubHeaderBGAlt">Kembali</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt">NO INDUK</td>
				<td width="40%" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka</td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt">Jenis Pustaka</td>
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt">Pengarang</td>
			</tr>
			<? $i=0;
			while ($row=$transnow->FetchRow()){
			if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idtransaksi'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="35">
				<td align="center">
				<? if ($row['fix_status']==0) { ?>
				<u title="Batal" onclick="goBatal('<?= $row['idtransaksi'] ?>','<?= $row['noseri'] ?>');" class="link"><img title="Batal meminjam" src="images/batal.png"></u>
				<? } else { ?>
				<u title="Batal" ><img title="Batal meminjam" src="images/batal2.png"></u>
				<? } ?></td>
				<td align="center">
				<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $row['idtransaksi']; ?>','<?= $row['noseri'] ?>');" class="link"><img src="images/kembalikan.png"></u>
				</td>
				<td>&nbsp; <?= $row['noseri'] ?></td>
				<td><?= $row['judul'].($row['edisi']!='' ? " / ".$row['edisi'] : "") ?></td>
				<td><?= $row['namajenispustaka'] ?></td>
				<td><?= $row['authorfirst1']." ".$row['authorlast1'] ?></td>
			</tr>
			<? } if ($i==0) { ?>
			<tr height="20">
				<td align="center" colspan="6"><b>Belum ada peminjaman.</b></td>
			</tr>
			<?php } ?>
				
		</table>
	</div>
		
	<div id="items" style="position:relative;top:-2px"> 
		<table cellspacing="0" width="100%">
			<tr><td class="LeftColumnBG"  style="border-top:none;border-left:thin solid #999" colspan=7>&nbsp;</td></tr>
			<tr height="20"> 
				<td width="80" nowrap align="center" class="SubHeaderBGAlt">NO INDUK</td>
				<td width="42%" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka </td>
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Pinjam</td>
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Harus Kembali</td>
				<td width="13%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Kembali</td>
				
				<?php
				$a = 0;

					// mulai iterasi
					while ($rows = $transe->FetchRow()) 
					{
						if ($a % 2) $rowstyle3 = 'NormalBG';  else $rowstyle3 = ''; $a++; 
						if ($rows['idtransaksi'] == '0') $rowstyle3 = 'YellowBG';
			?>
			<tr class="<?= $rowstyle3 ?>" height="20" valign="middle"> 
				<td><?= $rows['noseri']; ?></u></td>
				<td align="left"><?=  Helper::limitS($rows['judul'],53).($rows['edisi']!='' ? " / ".$rows['edisi'] : "") ?></td>
				<td align="center"><?= Helper::tglEng($rows['tgltransaksi']); ?></td>
				<td align="center"><?= $rows['tgltenggat']=='' ? "Maksimal" : Helper::tglEng($rows['tgltenggat']); ?></td>
				<td >&nbsp;
				<? if ($rows['tglpengembalian']=='')
					echo "Belum kembali";
					else
					echo Helper::tglEng($rows['tglpengembalian']);
				?></td>			
			</tr>
			<?php
				}
				if ($a==0) {
			?>
			<tr height="20">
				<td align="center" colspan="5"><b>Belum ada transaksi.</b></td>
			</tr>
			<?php } ?>
			
		</table>
		<br>
		<table align="center" cellspacing="3" cellpadding="2" width="370" border=0 bgcolor="#f1f2d1">
			<tr>
				<td width=270 align="center"><u>
				<script>
				var text="Sejarah transaksi diatas merupakan 15 transaksi terakhir";
				document.write(text.blink());
				</script>
				</u></td>
			</tr>
		</table>
		
		</div>
		</div>

		</td>
	</tr>
	</table>
	
<? } ?>
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
<input type="text" name="test" id="test" style="visibility:hidden">
</form>
</div>
</body>
<script language="javascript">
$(document).ready(function() {

	
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	var key=document.getElementById("key3").value;
	
	if (key=='' || key==0)
	chooseTab(0);

	else if (key==1)
	chooseTab(1);
	else if(key==2)
	chooseTab(2);
	else
	chooseTab(3);

});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>
<script type="text/javascript">
function goPinjamTA(){
	if(cfHighlight("txtpustaka")){
	document.getElementById('act').value="pinjambaca";
	goSubmit();
	}
}
function etrPinjamTA(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById('btnta').onclick();
}
function goClear() {
	var conf=confirm("Apakah Anda yakin telah menyelesaikan peminjaman ?");
	if(conf){
	document.getElementById("act").value='delsession';
	goSubmit();
	}
}
function goBatal($key,$eks) {
		var batalno=confirm("Apakah Anda yakin akan membatalkan transaksi ini ?");
		if(batalno){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goKembali($key,$eks) {
		var kembalikan=confirm("Apakah Anda yakin akan melakukan proses pengembalian ?");
		if(kembalikan){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goTab($key3) {
		document.getElementById("key3").value=$key3;
		//document.perpusform.submit();
		
}
</script>
</html>