<?	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$c_edit = $a_auth['canedit'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Denda Keterlambatan';
	$p_title = 'Laporan Denda Keterlambatan';
	$p_filerep =  Helper::navAddress('rep_dendatelat.php');
	
	$rs = $conn->Execute("select * from lv_jenisanggota order by namajenisanggota");
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<style type="text/css" media="screen">@import "style/basic.css";</style>
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">

		<div style="width:500px margin:0 auto;">		
		<form name="perpusform" id="perpusform" method="post" action="<?= $p_filerep; ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="100%" cellspacing="4" cellpadding="4"  class="EditTabTable GridStyle">
			<!--tr> 
				<td class="EditTabLine" colspan="3"><?= $p_title ?></td>
			</tr-->
			<tr>
				<td width="25%" class="EditTabLabel thLeft">Jenis Anggota</td>
				<td  width="75%"><? while($row = $rs->FetchRow()) { ?>
				<li><input type="checkbox" name="jenis[]" id="jenis_<?= $row['kdjenisanggota'] ?>" value="<?= $row['kdjenisanggota'] ?>" class="ControlStyle"> <label for="jenis_<?= $row['kdjenisanggota'] ?>"><?= $row['namajenisanggota'] ?></label></li>
				<? } ?></td>
			</tr>
			<tr>
				<td class="EditTabLabel thLeft">Tanggal</td>
				<td><?= UI::createTextBox('periode1',$r_periode1,'ControlStyle',10,10,true,'readonly'); ?>
				<img src="images/cal.png" id="periode1_trg" style="cursor:pointer;" title="Pilih tanggal Awal">
				<strong>s/d</strong>
				<?= UI::createTextBox('periode2',$r_periode2,'ControlStyle',10,10,true,'readonly'); ?>
				<img src="images/cal.png" id="periode2_trg" style="cursor:pointer;" title="Pilih tanggal Akhir"></td>
			</tr>
			<tr>
				<td width="25%" class="EditTabLabel thLeft">Format</td>
				<td><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:showData();" class="button"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
		</div>
		</div>
</body>

<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#periode1").mask("99-99-9999");
	   $("#periode2").mask("99-99-9999");
});

Calendar.setup({
	inputField     :    "periode1",		// id of the input field
	ifFormat       :    "%d-%m-%Y",		// format of the input field
	button         :    "periode1_trg",	// trigger for the calendar (button ID)
	align          :    "Br",			// alignment (defaults to "Bl")
	singleClick    :    true
});

Calendar.setup({
	inputField     :    "periode2",		// id of the input field
	ifFormat       :    "%d-%m-%Y",		// format of the input field
	button         :    "periode2_trg",	// trigger for the calendar (button ID)
	align          :    "Br",			// alignment (defaults to "Bl")
	singleClick    :    true
});

function showData() {
	if(cfHighlight("periode1,periode2"))
		document.getElementById("perpusform").submit();
}
</script>
</html>