<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengolahan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and (us.idunit in ($units) or a.idunit in ($units)) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarolah';
	$p_window = '[PJB LIBRARY] Daftar Pengolahan';
	$p_title = 'Daftar Pengiriman Eksemplar';
	$p_tbheader = '.: Daftar Pengiriman Eksemplar :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('data_pustakaolah.php');

	$p_filekirimint = Helper::navAddress('data_kiriminternal.php');
	$p_id = "lsolah";
	
	$p_defsort = 'ideksemplarolah';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$p_sqlstr = "select p.*,e.noseri,lk.namaklasifikasi, m.judul, j.namajenispustaka, e.tglperolehan, 
		m.authorfirst1, m.authorlast1, m.authorfirst2, m.authorlast2, m.authorfirst3, m.authorlast3 
		from $p_dbtable p
		left join pp_eksemplar e on e.ideksemplar=p.ideksemplar
		left join ms_pustaka m on m.idpustaka=e.idpustaka
		left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka
		left join lv_klasifikasi lk on lk.kdklasifikasi=e.kdklasifikasi
		left join pp_orderpustakattb tb on tb.idorderpustakattb=p.idorderpustakattb
		left join pp_orderpustaka op on op.idorderpustaka=tb.idorderpustaka
		left join pp_usul us on op.idusulan=us.idusulan
		left join pp_sumbangan s on s.idsumbangan =  op.idsumbangan
		left join ms_anggota a on (a.idanggota = us.idanggota or a.idanggota = s.idanggota or op.nrp1 = a.idanggota)
		where stskirimpengolahan=1 and iskirimeksternal=0 and stskirimeksternal=0 $sqlAdminUnit";

	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carino']);
		$f5=Helper::removeSpecial($_POST['kdjenispustaka']);
		$status = Helper::removeSpecial($_POST['status']);
		$fkoleksi=Helper::removeSpecial($_POST['kdklasifikasi']);
		
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(to_char(judul)) like upper('%$keyjudul%') ";
		}
		
		if($f5!='') {
			$p_sqlstr.=" and m.kdjenispustaka='$f5' ";
		}
		
		if($fkoleksi!='') {
			$p_sqlstr.=" and e.kdklasifikasi='$fkoleksi' ";
		}
		
		if ($status != '')
			$p_sqlstr.=" and p.stspengolahan=$status";
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
				
		if ($r_aksi == 'terima'){
			$record = array();
			$record['tglpengolahan'] = date('Y-m-d H:m:s');
			$record['npkpengolahan'] = $_SESSION['PERPUS_USER'];
			$record['stspengolahan'] = 1;
			
			Helper::Identitas($record);
			
			for($i=0;$i<count($_POST['cbSelect']);$i++)
				$_POST['cbSelect'][$i] = Helper::cAlphaNum($_POST['cbSelect'][$i]);
				
			$r_strsent = implode("','",$_POST['cbSelect']);
			
			$err = Sipus::UpdateComplete($conn,$record,$p_dbtable,"ideksemplarolah in ('$r_strsent')");
			$err=0;
			if($err != 0)
			{
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}else {
				//record untuk history
				$rechis = array();
				$rechis['tkhistory'] = 'T';
				$rechis['keterangan'] = 'Terima Pengolahan';
				$rechis['tglterima'] = date('Y-m-d H:m:s');
				$rechis['npkterima'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
				Pengolahan::insHistory($conn,$rechis,$r_strsent);
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex		
	$p_sqlstr .= " order by tglkirimpengolahan desc";

	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
		
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where kdjenispustaka in ($_SESSION[roleakses]) order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$f5,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_jenis);	
	$a_status = array('' => '-- Semua --', '0' => 'Belum Diterima', '1' => 'Sudah Diterima');
	$l_status = UI::createSelect('status',$a_status,$status,'ControlStyle',true,'style="width:150" onchange="goSubmit()"');
	
	// untuk combobox pengiriman apakah ke sirkulasi, referensi, tandon, karya ilmiah dan terbitan berkala
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by kdklasifikasi");
	$l_kirim = $rs_cb->GetMenu2('kdklasifikasi',$fkoleksi,true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:150"');
	$l_kirim = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_kirim);	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div class="filterTable table-responsive">
				<table width="100%" cellpadding="4" cellspacing="0" border="0">
					<tr>
						<? if ($c_edit) {	?>	
						<td class="thLeft" style="border:0 none;">
							<a href="javascript:goApprove();" class="buttonshort"><span class="approve">Terima</span></a>
						</td>		
						<td class="thLeft" style="border:0 none;">
							<a href="javascript:goNewKirim('<?= $p_filekirimint; ?>');" class="buttonshort"><span class="list">Kirim</span></a>
						</td>	
						<? } ?>
						<td class="thLeft"></td>
						<td class="thLeft"></td>
					</tr>
					<tr>
						<td class="thLeft" style="border:0 none;" width="150"><strong>Judul</strong></td>
						<td class="thLeft" style="border:0 none;" colspan="3">:&nbsp;<input type="text" name="carino" id="carino" size="50" value="<?= $_POST['carino'] ?>" onKeyDown="etrCari(event);"></td>
						<td class="thLeft" style="border:0 none;">
							<a href="javascript:goSubmit()" class="buttonshort"><span class="filter">Filter</span></a>
						</td>
						<td class="thLeft" style="border:0 none;">
							<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">Refresh</span></a>
						</td>
					</tr>
					<tr>
						<td class="thLeft" style="border:0 none;"><strong>Jenis Pustaka</strong></td>
						<td class="thLeft" style="border:0 none;">: <?= $l_jenis ?>
						<td class="thLeft" style="border:0 none;" align="right">
							<div style="float:right;"><b>Koleksi : </b><?= $l_kirim ?></div>
						</td>
						<td class="thLeft" style="border:0 none;" colspan="2">
							<div style="float:right;"><b>Status : </b><?= $l_status ?></div>
						</td>
					</tr>
				</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
			<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			<tr height="20"> 
				<td nowrap align="center" class="SubHeaderBGAlt thLeft" s><input type="checkbox" name="cbSelAll" id="cbSelAll" value="1" class="ControlStyle" onClick="checkedAll()"></td></td>
				<td width="300" align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO. INDUK PUSTAKA  <?= $p_xsort['e.noseri']; ?></td>
				<td width="300" align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul  <?= $p_xsort['judul']; ?></td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','authorfirst1:C');">Pengarang  <?= $p_xsort['authorfirst1']; ?></td>
				<td width="60" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','edisi:C');">Koleksi <?= $p_xsort['kdklasifikasi']; ?></td>		
				<td nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','kdjenispustaka:C');">Jenis  <?= $p_xsort['kdjenispustaka']; ?></td>
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.tglperolehan:D');">Tgl. Perolehan<?= $p_xsort['tglperolehan']; ?></td>
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;">Status</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;">Edit</td>
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" valign="middle"> 
				<td align="center">
				<input type="checkbox" id="cbSelect" name="cbSelect[]" value="<?= $row['ideksemplarolah']; ?>" class="ControlStyle">
				</td>
				<td><?= $row['noseri']; ?></td>
				<td><?= $row['judul']; ?></td>
				<td><?php
					echo $row['authorfirst1']. " " .$row['authorlast1']; 
					if ($row['authorfirst2']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst2']. " " .$row['authorlast2'];
					}
					if ($row['authorfirst3']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst3']. " " .$row['authorlast3'];
					}
				?></td>
				<td align="center"><?= $row['namaklasifikasi']; ?></td>
				<td align="center"><?= $row['namajenispustaka']; ?></td>
				<td align="center"><?= Helper::formatDateInd($row['tglperolehan'],false); ?></td>
				<td align="center"><? if ($row['stspengolahan'] != 1) {?>
					<font color="red"><b>
					<script>
					var skor="<?= $row['stspengolahan'] != 1 ? 'Belum Diterima' : 'Sudah Diterima';?>";
					document.write(skor.blink());
					</script>
					</b></font>
					<? }else echo 'Sudah Diterima'; ?>
				</td>
				<td align="center">
				<? if ($row['stspengolahan'] != 0){?>
				<u onClick="goDetail('<?= $p_filedetail; ?>','<?= $row['ideksemplar'] ?>');"  title="Detail Eksemplar" class="link"><img src="images/edited.gif"></u>
				<? }else{ ?>
				<img src="images/edited2.gif">
				<? } ?>
				</td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col ; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
				
			</tr>
		</table>
            </div>
		<?php require_once('inc_listnav.php'); ?><br>

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="arrcb" id="arrcb">
		</div>
		</form>
		</div>
	</div>
</div>



</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carino").value='';
}

function goApprove(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih dulu data yang ingin diterima ke pegolahan dengan mencentang barisnya.");
	else {
		var retval;
		retval = confirm("Anda yakin untuk menerima ke pengolahan data yang dicentang?");
		if (retval) {
			$("#act").val("terima");
			goSubmit();
			
		}
	}
}

function goSent(){
	if($("input[id='cbSelect']:checked").val() == null) // tidak ada yang dicentang
		alert("Pilih dulu data yang ingin dikirm ke sirkulasi dengan mencentang barisnya.");
	else {
		var retval;
		retval = confirm("Anda yakin untuk mengirim ke sirkulasi data yang dicentang?");
		if (retval) {
			$("#act").val("kirim");
			goSubmit();
			
		}
	}
}


checked=false;
function checkedAll () {
	var aa= document.getElementById('perpusform');
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i =0; i < aa.elements.length; i++) 
	{
	 	aa.elements[i].checked = checked;
	}
}

function goNewKirim(file) {
	document.getElementById("perpusform").action = file;
	goSubmit();
}
</script>

</html>