<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Rekap Reservasi';
	
	$p_namafile = 'laporan_rekreserve_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$s_jurusan = $conn->Execute("select * from lv_jurusan");
	
	$sql = "select count(a.idanggota) as jumlah,a.idunit as kdjurusan from pp_reservasi r join ms_anggota a on r.idanggotapesan=a.idanggota where 1=1";
	$sql .=" and tglreservasi between '$r_tgl1' and '$r_tgl2' group by a.idanggota,a.idunit";
	$rs = $conn->Execute($sql);
	
	while($row=$rs->FetchRow()){
	$ArJur[$row['kdjurusan']] = $row['jumlah'];	
	}
	
	while($rowj=$s_jurusan->FetchRow()){
	$LJum[$rowj['kdfakultas']][]=$rowj['kdjurusan'];
	$LJur[$rowj['kdfakultas']][]=$rowj['namajurusan'];
	}
	$s_fakultas = $conn->Execute("select * from lv_fakultas order by kdfakultas");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=50 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Rekap Reservasi Pustaka
	<br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
  <? $total=0;
  while($rowf=$s_fakultas->FetchRow()){ ?>
  <tr height="30">
	<td><?= "<b><u>".$rowf['namafakultas']."</u></b><br>"; 
	echo "<table>";
	$jumtotal = 0;
	for($i=0;$i<count($LJur[$rowf['kdfakultas']]);$i++){
	echo "<tr>";
		echo "<td width=150>&nbsp;</td>";
		echo "<td width=450>";
		echo $LJur[$rowf['kdfakultas']][$i];
		echo "</td>";
		echo "<td>";
		echo $ArJur[$LJum[$rowf['kdfakultas']][$i]]=='' ? '0' : $ArJur[$LJum[$rowf['kdfakultas']][$i]];
		echo '</td>';
	echo "</tr>";
	$jumtotal +=$ArJur[$LJum[$rowf['kdfakultas']][$i]];
	}
	echo "<tr><td>&nbsp;</td><td><b><u>Jumlah<u></b></td><td><b><u>".$jumtotal."</u></b></td>";
	echo "</table>";
	?>
	</td>
  </tr>
  <? $total +=$jumtotal; } ?>
  <tr height=30>
	<td>
		<table>
		<tr>
		<td width=650><b>JUMLAH TOTAL</b>
		<td width="50" style="border-left-color:#fff;"><b><?= $total ?></b></td>
		</tr>
		</table>
	</td>
	</tr>
</table>


</div>
</body>
</html>