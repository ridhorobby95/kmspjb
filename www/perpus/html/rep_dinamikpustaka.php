<?php
	// di-buffer, karena ada penggunaan header vital
	ob_start();
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);	
	
	$a_kolom=$_POST['kolom'];
	$a_urutan=$_POST['urutan'];
	$a_kriteria=$_POST['kriteria'];
	$a_paramkriteria=$_POST['paramkriteria'];	
	$r_format = Helper::cAlphaNum($_REQUEST['format']);

	//cek join
	$cek['idpustaka'] = false;
	$cek['judul'] = false;
	$cek['idauthor'] = false;
	$cek['nopanggil'] = false;
	$cek['kdjenispustaka'] = false;
	$cek['judulseri'] = false;
	$cek['kdbahasa'] = false;
	$cek['kodeddc'] = false;
	$cek['idtopik'] = false;
	$cek['idpenerbit'] = false;
	$cek['isbn'] = false;
	$cek['kota'] = false;
	$cek['tahunterbit'] = false;
	$cek['edisi'] = false;
	$cek['tglperolehan'] = false;
	$cek['jmlhalromawi'] = false;
	$cek['jmlhalaman'] = false;
	$cek['dimensipustaka'] = false;
	$cek['keywords'] = false;
	$cek['linkpustaka'] = false;
	$cek['keterangan'] = false;	
	$cek['sumeks'] = false;	
	
	//kolom yang dipilih(di select) dan urutan
	$kolom = array();
	$terjemah_urutan['idpustaka'] = $kolom['idpustaka'] = 'p.idpustaka';
	$kolom['judul'] = 'p.judul';
	$kolom['nopanggil'] = 'nopanggil';
	$kolom['kdjenispustaka'] = 'namajenispustaka';
	$kolom['judulseri'] = 'judulseri';
	$kolom['idauthor'] = "authorfirst1||' '||authorlast1 as namapengarang";
	$kolom['kdbahasa'] = 'namabahasa';
	$kolom['kodeddc'] = 'kodeddc';
	$kolom['idtopik'] = 'namatopik';
	$kolom['idpenerbit'] = "mp.namapenerbit||' - '||mp.kota as penerbit";
	$kolom['isbn'] = 'isbn';
	$kolom['kota'] = 'kota';
	$kolom['tahunterbit'] = 'tahunterbit';
	$kolom['edisi'] = 'edisi';
	$kolom['tglperolehan'] = 'e.tglperolehan';
	$kolom['jmlhalromawi'] = 'jmlhalromawi';
	$kolom['jmlhalaman'] = 'jmlhalaman';
	$kolom['dimensipustaka'] = 'dimensipustaka';
	$kolom['keywords'] = 'keywords';
	$kolom['linkpustaka'] = 'linkpustaka';
	$kolom['keterangan'] = 'e.keterangan';
	$kolom['sumeks'] = '(select count(*) from pp_eksemplar where idpustaka=p.idpustaka) as sumeks';
	
	//===================== URUTAN =====================
	$terjemah_urutan['judul'] = 'to_char(judul)';
	$terjemah_urutan['kdjenispustaka'] = 'namajenispustaka';
	$terjemah_urutan['nopanggil'] = 'nopanggil';
	$terjemah_urutan['judulseri'] = 'judulseri';
	$terjemah_urutan['idauthor'] = 'namapengarang';
	$terjemah_urutan['kdbahasa'] = 'namabahasa';
	$terjemah_urutan['kodeddc'] = 'kodeddc';
	$terjemah_urutan['idtopik'] = 'namatopik';
	$terjemah_urutan['idpenerbit'] = 'namapenerbit';
	$terjemah_urutan['isbn'] = 'isbn';
	$terjemah_urutan['kota'] = 'kota';
	$terjemah_urutan['tahunterbit'] = 'tahunterbit';
	$terjemah_urutan['edisi'] = 'edisi';
	$terjemah_urutan['tglperolehan'] = 'tglperolehan';
	$terjemah_urutan['jmlhalromawi'] = 'jmlhalromawi';
	$terjemah_urutan['jmlhalaman'] = 'jmlhalaman';
	$terjemah_urutan['dimensipustaka'] = 'dimensipustaka';
	$terjemah_urutan['keywords'] = 'keywords';
	$terjemah_urutan['linkpustaka'] = 'linkpustaka';
	$terjemah_urutan['keterangan'] = 'keterangan';
	$terjemah_urutan['sumeks'] = 'sumeks';
	
	
	//lebar kolom
	$lebarkolom['idpustaka'] = 50;
	$lebarkolom['judul'] = 250;
	$lebarkolom['nopanggil'] = 100;
	$lebarkolom['judulseri'] = 100;
	$lebarkolom['kdjenispustaka'] = 100;
	$lebarkolom['idauthor'] = 200;
	$lebarkolom['kdbahasa'] = 200;
	$lebarkolom['kodeddc'] = 100;
	$lebarkolom['idtopik'] = 100;
	$lebarkolom['idpenerbit'] = 200;
	$lebarkolom['isbn'] = 50;
	$lebarkolom['kota'] = 100;
	$lebarkolom['tahunterbit'] = 50;
	$lebarkolom['edisi'] = 20;
	$lebarkolom['tglperolehan'] = 50;
	$lebarkolom['jmlhalromawi'] = 20;
	$lebarkolom['jmlhalaman'] = 20;
	$lebarkolom['dimensipustaka'] = 50;
	$lebarkolom['keywords'] = 100;
	$lebarkolom['linkpustaka'] = 100;
	$lebarkolom['keterangan'] = 100;
	$lebarkolom['sumeks'] = 50;
	
	$nama_tabel = 'ms_pustaka p';
	
	//nama header kolom
	$namakolom['idpustaka'] = 'KODE PUSTAKA';
	$namakolom['judul'] = 'JUDUL';
	$namakolom['nopanggil'] = 'NO PANGGIL';
	$namakolom['judulseri'] = 'JUDUL SERI';
	$namakolom['kdjenispustaka'] = 'JENIS PUSTAKA';
	$namakolom['idauthor'] = 'PENGARANG';
	$namakolom['kdbahasa'] = 'BAHASA';
	$namakolom['kodeddc'] = 'DDC';
	$namakolom['idtopik'] = 'TOPIK';
	$namakolom['idpenerbit'] = 'PENERBIT';
	$namakolom['isbn'] = 'ISBN';
	$namakolom['kota'] = 'KOTA';
	$namakolom['tahunterbit'] = 'TAHUN TERBIT';
	$namakolom['edisi'] = 'EDISI';
	$namakolom['tglperolehan'] = 'TGL. PEROLEHAN';
	$namakolom['jmlhalromawi'] = 'HAL. (ROMAWI)';
	$namakolom['jmlhalaman'] = 'HAL.';
	$namakolom['dimensipustaka'] = 'DIMENSI';
	$namakolom['keywords'] = 'KEYWORD';
	$namakolom['linkpustaka'] = 'LINK';
	$namakolom['keterangan'] = 'KETERANGAN';
	$namakolom['sumeks'] = 'JUMLAH EKSEMPLAR';
	
	for($i=0;$i<count($a_kolom);$i++){			
		if($kolom[$a_kolom[$i]]=='p.idpustaka'){			
			$nama_kolom = ''; 
		}		
		else{
			$nama_kolom = $nama_kolom .','. $kolom[$a_kolom[$i]];
						
				//join tabel
				if($a_kolom[$i] == 'kdjenispustaka'){
					$nama_tabel1 = " left join lv_jenispustaka jp on p.kdjenispustaka = jp.kdjenispustaka";
					$cek['kdjenispustaka'] = true;
				}
				else if($a_kolom[$i] == 'kdbahasa'){
					$nama_tabel1 = " left join lv_bahasa b on p.kdbahasa = b.kdbahasa";
					$cek['kdbahasa'] = true;
				}
				else if($a_kolom[$i] == 'idauthor'){
					$nama_tabel1 = " left join pp_author pa on pa.idpustaka = p.idpustaka
									left join ms_author ma on ma.idauthor = pa.idauthor";
					$cek['idauthor'] = true;
				}
				else if($a_kolom[$i] == 'idpenerbit'){
					$nama_tabel1 = " left join ms_penerbit mp on mp.idpenerbit = p.idpenerbit ";
					$cek['idpenerbit'] = true;
				}
				else if($a_kolom[$i] == 'idtopik'){
					$nama_tabel1 = " left join pp_topikpustaka tp on tp.idpustaka = p.idpustaka
									left join lv_topik vt on vt.idtopik= tp.idtopik";
					$cek['idauthor'] = true;
				}
				else
					$nama_tabel1='';					
				$nama_tabel	= $nama_tabel .''.$nama_tabel1;		

		}			
	}	
	$nama_kolom = "p.idpustaka" . $nama_kolom;		
	
	//nama kolom di terjemahkan
	$terjemah_kolom = array();
	$terjemah_kolom['idpustaka'] = 'idpustaka';
	$terjemah_kolom['ideksemplar'] = 'ideksemplar';
	$terjemah_kolom['judul'] = 'judul';
	$terjemah_kolom['nopanggil'] = 'nopanggil';
	$terjemah_kolom['kdjenispustaka'] = 'namajenispustaka';
	$terjemah_kolom['judulseri'] = 'judulseri';
	$terjemah_kolom['idauthor'] = 'namapengarang';
	$terjemah_kolom['kdbahasa'] = 'namabahasa';
	$terjemah_kolom['kodeddc'] = 'kodeddc';
	$terjemah_kolom['idtopik'] = 'namatopik';
	$terjemah_kolom['idpenerbit'] = 'penerbit';
	$terjemah_kolom['isbn'] = 'isbn';
	$terjemah_kolom['kota'] = 'kota';
	$terjemah_kolom['tahunterbit'] = 'tahunterbit';
	$terjemah_kolom['edisi'] = 'edisi';
	$terjemah_kolom['tglperolehan'] = 'tglperolehan';
	$terjemah_kolom['jmlhalromawi'] = 'jmlhalromawi';
	$terjemah_kolom['jmlhalaman'] = 'jmlhalaman';
	$terjemah_kolom['dimensipustaka'] = 'dimensipustaka';
	$terjemah_kolom['keywords'] = 'keywords';
	$terjemah_kolom['linkpustaka'] = 'linkpustaka';
	$terjemah_kolom['keterangan'] = 'keterangan';
	$terjemah_kolom['sumeks'] = 'sumeks';
	
	//kriteria (where)
	$terjemah_kriteria = array();
	$terjemah_kriteria['jenispustaka'] = 'jp.kdjenispustaka';
	$terjemah_kriteria['topik'] = 'tp.idtopik';
	$terjemah_kriteria['bahasa'] = 'b.kdbahasa';
	$terjemah_kriteria['author'] = 'lower(ma.namadepan)';
	$terjemah_kriteria['penerbit'] = 'lower(mp.namapenerbit)';
	$terjemah_kriteria['kodeddc'] = 'kodeddc';
	$terjemah_kriteria['tglperolehan'] = 'tglperolehan';
	
	$a_kriteria_select = array('jenispustaka', 'topik','bahasa','author','penerbit');
	$a_kriteria_between = array('kodeddc','tglperolehan');			
	
	
	for($i=0;$i<count($a_kriteria);$i++){	
		$where2 = "";
		if(in_array($a_kriteria[$i],$a_kriteria_select)){ 	
			$paramkriteria=explode(':',$a_paramkriteria[$i]);
			for($j=0;$j<count($paramkriteria);$j++){
				if ($a_kriteria[$i] == 'jenispustaka')
				{
					$nama_tabel	= $nama_tabel .''.cekjoin($cek['kdjenispustaka'],$a_kriteria[$i]);
					$cek['kdjenispustaka'] = true;		
					
					if(count($paramkriteria)==1){	
						$where1 =" in ('$paramkriteria[$j]')";
					}
					else{
						
						if($j==0){
							$where1 =" in ('$paramkriteria[$j]',";
						}
						elseif($j==count($paramkriteria)-1){
							$where1 ="'$paramkriteria[$j]')";
						}
						else{
							$where1 ="'$paramkriteria[$j]',";
						}
						
					}	
				}
				else if ($a_kriteria[$i] == 'bahasa')
				{
					$nama_tabel	= $nama_tabel .''.cekjoin($cek['kdbahasa'],$a_kriteria[$i]);
					$cek['kdbahasa'] = true;		
					
					if(count($paramkriteria)==1){	
						$where1 =" in ('$paramkriteria[$j]')";
					}
					else{
						
						if($j==0){
							$where1 =" in ('$paramkriteria[$j]',";
						}
						elseif($j==count($paramkriteria)-1){
							$where1 ="'$paramkriteria[$j]')";
						}
						else{
							$where1 ="'$paramkriteria[$j]',";
						}
						
					}	
				}
				else if ($a_kriteria[$i] == 'author')
				{
					$nama_tabel	= $nama_tabel .''.cekjoin($cek['idauthor'],$a_kriteria[$i]);
					$cek['idauthor'] = true;		
					
					if(count($paramkriteria)==1){	
						$where1 =" like '%$paramkriteria[$j]%' or lower(ma.namabelakang) like '%$paramkriteria[$j]%' ";
					}
				}
				else if ($a_kriteria[$i] == 'penerbit')
				{
					$nama_tabel	= $nama_tabel .''.cekjoin($cek['idpenerbit'],$a_kriteria[$i]);
					$cek['idpenerbit'] = true;		
					
					if(count($paramkriteria)==1){	
						$where1 =" like '%$paramkriteria[$j]%'";
					}
				}
				else {			
					if(count($paramkriteria)==1){	
						$where1 =" in ('$paramkriteria[$j]')";
					}
					else{
						
						if($j==0){
							$where1 =" in ('$paramkriteria[$j]',";
						}
						elseif($j==count($paramkriteria)-1){
							$where1 ="'$paramkriteria[$j]')";
						}
						else{
							$where1 ="'$paramkriteria[$j]',";
						}
						
					}
				}
				$where2 = $where2 . $where1;					
			}
			$where2 = $terjemah_kriteria[$a_kriteria[$i]].' '.$where2;	
		}
		elseif(in_array($a_kriteria[$i],$a_kriteria_between)){		
			$paramkriteria=explode(':',$a_paramkriteria[$i]);
			for($j=0;$j<2;$j++){
				if($a_kriteria[$i] == 'kodeddc'){
					$paramkriteria[$j]= $paramkriteria[$j];					
				}	
				if($a_kriteria[$i] == 'tglperolehan'){
					$paramkriteria[$j]= Helper::formatDate($paramkriteria[$j]);					
				}	
				
				if($j==0)
					$where1= "between '$paramkriteria[$j]'";		
				elseif($j==1)
					$where1= " and '$paramkriteria[$j]'";
				$where2 = $where2 . $where1;
			}			
			$where2 = $terjemah_kriteria[$a_kriteria[$i]].' '.$where2; 			
		}
		
		if($i==0  && ($cekwhere['pendidikan'] == false))
			$where = $where . $where2;		
		else
			$where = $where ." and ". $where2;			
		
	}
	
	
	//urutan
	for($i=0;$i<count($a_urutan);$i++){
		$urutan1[$i]= explode(':',$a_urutan[$i]);
		for($j=0;$j<2;$j++){ //ada dua parameter dalam menentukan urutan, berdasarkan pilihan list dan bedasarkan asc/disc.
			$urutan2[$i][$j]=$urutan1[$i][$j];	
			if($urutan2[$i][$j]=='A'){
				$urutan = $urutan . " ASC";
			}
			else if($urutan2[$i][$j]=='D')	{
				$urutan = $urutan . " DESC";
			}			
			else if($i>0 && $j==0)	{				
				$urutan = $urutan . ",". $terjemah_urutan[$urutan2[$i][$j]];
				$nama_tabel	= $nama_tabel .''.cekjoin($cek[$urutan2[$i][$j]],$urutan2[$i][$j]);
			}
			else{			
				$urutan = $urutan . "".$terjemah_urutan[$urutan2[$i][$j]];	
				$nama_tabel	= $nama_tabel .''.cekjoin($cek[$urutan2[$i][$j]],$urutan2[$i][$j]);				
			}					
		}	
	}
	
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Daftar Pustaka';
	$p_title = 'Daftar Pustaka';
	$p_namafile = 'Daftar_Pustaka';
	
	switch($r_format) {
		case "doc" :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case "xls" :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	// mendapatkan informasi pegawai 
	$p_sqlstr = "
	select 
		$nama_kolom
	from 
		$nama_tabel";	
	if($a_kriteria!='')		
		$p_sqlstr =  $p_sqlstr . " where ".$where;		
	if($a_urutan!='')
		$p_sqlstr = $p_sqlstr ." order by ".$urutan;	

	$rs = $conn->Execute($p_sqlstr);
	
	$p_col = count($tabel);
	$p_tbwidth = 900;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<style>
		td { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 12px;}
	</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width="<?= $p_tbwidth; ?>">
	<tr>
		<td align="center" colspan="<?= $p_col; ?>"><strong><font size="3"><?= $p_title; ?></font></strong></td>
	</tr>
</table>
<br>
<table cellpadding="3" border="1" style="border-collapse:collapse;">
	<tr>
		<?
		echo "<td bgcolor='#CCCCCC'>&nbsp;NO</td>";
		for($i=0;$i<count($a_kolom);$i++){?>
			<td nowrap bgcolor="#CCCCCC" width="<?= $lebarkolom[$a_kolom[$i]]; ?>"><?=$namakolom[$a_kolom[$i]]; ?></td>
			
		<? }
		?>			
	</tr>
	<?	$n = 1;
	while(!$rs->EOF) { 
	?>
	<tr>
		<?
		echo "<td align='right'>"; echo $n++; echo "</td>";
		for($i=0;$i<count($a_kolom);$i++){	
			if(substr($a_kolom[$i],0,2) == 'mk' && $rs->fields[$terjemah_kolom[$a_kolom[$i]]] != '')		{
				echo "<td>"; 
				echo substr($rs->fields[$terjemah_kolom[$a_kolom[$i]]],0,2)." thn "; 
				echo substr($rs->fields[$terjemah_kolom[$a_kolom[$i]]],2,2). " bln"; 
				
				echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,3) == 'tgl' or substr($a_kolom[$i],0,3) == 'tmt'){
				echo "<td>"; echo Helper::formatDate($rs->fields[$terjemah_kolom[$a_kolom[$i]]]); echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,9) == 'unitkerja'){
					echo "<td>"; echo $rs->fields[$terjemah_kolom['parent']]." - ".$rs->fields[$terjemah_kolom[$a_kolom[$i]]]; echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,8) == 'tmplahir'){
				echo "<td>"; echo ucwords($rs->fields[$terjemah_kolom[$a_kolom[$i]]]); echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,6) == 'rgpkmk'){
				echo "<td>"; echo $rs->fields[$terjemah_kolom[$a_kolom[$i]]]." - ".$rs->fields[$terjemah_kolom['kmk']]; echo "</td>";
			}
			 elseif(substr($a_kolom[$i],0,2) == 'lk'){
			 $lk=$rs->fields[$terjemah_kolom[$a_kolom[$i]]];
				echo "<td>"; if($lk=='1')echo 'Ya'; else echo 'tidak'; echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,3) == 'iok'){
			$iok=$rs->fields[$terjemah_kolom[$a_kolom[$i]]];
				echo "<td>"; if($iok=='1')echo 'Ya'; else echo 'tidak'; echo "</td>";
			}
			elseif(substr($a_kolom[$i],0,2) == 'ot'){
			$ot=$rs->fields[$terjemah_kolom[$a_kolom[$i]]];
				echo "<td>"; if($ot=='1')echo 'Ya'; else echo 'tidak'; echo "</td>";
			} 
			else{		
				echo "<td>"; echo $rs->fields[$terjemah_kolom[$a_kolom[$i]]]; echo "</td>";
			}
		}
		?>	
	</tr>
	<? 		
		$rs->MoveNext();
	} 
	if($i == 1) { ?>
	<tr>
		<td colspan="<?= $p_col ?>" height="30" align="center">Data tidak ditemukan</td>
	</tr>
<?	} ?>
</table>
<?
/*fungsfi*/

function cekjoin($cek,$urut){
	if($cek == false && $urut =='jenispustaka')
		return " left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka"; 
	else if($cek == false && $urut =='bahasa')
		return " left join lv_bahasa b on p.kdbahasa = b.kdbahasa";
	else if($cek == false && $urut =='author')
		return " left join pp_author pa on pa.idpustaka = p.idpustaka
				left join ms_author ma on ma.idauthor = pa.idauthor";
	else if($cek == false && $urut =='penerbit')
		return " left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit";
	else
		return "";	 
	
}
?>
<br>

</div>
</body>
</html>

<? ob_end_flush(); ?>
