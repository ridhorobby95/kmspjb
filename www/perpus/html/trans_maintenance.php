<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// $conn->debug=true;
	// print_r($_REQUEST);
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_readlist = $a_auth['canread'];
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_historymaintaining';
	$p_window = '[PJB LIBRARY] Transaksi maintenance';
	$p_title = 'Transaksi Maintenance';
	$p_tbheader = '.: Transaksi Maintenance :.';
	$p_col = 9;
	$p_tbwidth = 700;
	$p_filelist = Helper::navAddress("list_maintenance.php");
	$r_key=Helper::removeSpecial($_GET['key']);
	
	if(!empty($_POST)){
		//$record=array();
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$arr = $_POST['arr'];
		$filter = str_replace(',,',',',$arr);
		$list=str_replace(',',",",$filter);
		$xlist=$list;
		
		if($r_aksi=='tambah'){
			$regcomp=Helper::removeSpecial($_POST['noreg']);
			$check=$conn->GetRow("select noseri from pp_eksemplar where noseri='$regcomp' and upper(statuseksemplar)=upper('ada')");
			
			if(!$check){
			$filter = str_replace($regcomp,'',$filter);
			$errdb = 'Eksemplar tidak dapat ditambahkan.';	
			Helper::setFlashData('errdb', $errdb);
			}
		}			
		else if($r_aksi=='simpan'){			
			$record['tkhistory']='K';
			$record['tglkirim']=date('Y-m-d H:i:s');
			$record['npkkirim']=$_SESSION['PERPUS_USER'];
			if($r_key==''){
				// echo 'kosong';
				$record['tglhistory']=date('Y-m-d H:i:s');
				$record['kdlokasi']=Helper::removeSpecial($_POST['kdlokasi']);
				$record['kdkondisi']=Helper::removeSpecial($_POST['kdkondisi']);
				
				$rs_get_reference = $conn->GetRow("select max(no_referensi::integer) as maxid from pp_historymaintaining");
				$maxid_referensi = (int)$rs_get_reference['maxid'] + 1;
				$record['no_referensi'] = $maxid_referensi;
				// $record['no_referensi']=$conn->GetOne("select (max(no_referensi::int)+1) as maxid from pp_historymaintaining where no_referensi is not null ");
			//die();
			} else {
				$record['tglhistory']=Helper::removeSpecial($_POST['u_tgl']);
				$record['kdlokasi']=Helper::removeSpecial($_POST['u_lokasi']);
				$record['kdkondisi']=Helper::removeSpecial($_POST['u_kondisi']);
				$record['no_referensi']=$r_key;
			}
			$record['keterangan']=Helper::cStrNull($_POST['keterangan']);
			Helper::Identitas($record);
			$conn->StartTrans();

			if($arr!=''){
				//$checkArr=$conn->GetRow("select ideksemplar from pp_historymaintaining where 
				$pustakaIn=$conn->Execute("select e.noseri,p.judul,e.ideksemplar,e.kdkondisi,e.kdlokasi from pp_eksemplar e join ms_pustaka p on e.idpustaka = p.idpustaka where e.noseri in ($xlist) and upper(e.statuseksemplar)=upper('ada')");
				if($pustakaIn){
				
				while($rsp=$pustakaIn->FetchRow()){
					$record['ideksemplar']=$rsp['ideksemplar'];
					$record['kdkondisi_lama']=$rsp['kdkondisi'];
					$record['kdlokasi_lama']=$rsp['kdlokasi'];
					Sipus::InsertBiasa($conn,$record,pp_historymaintaining);
					Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$rsp['ideksemplar']);
				}
				}else{
				$errdb = 'Masukkan Data Eksemplar dengan benar.';	
				Helper::setFlashData('errdb', $errdb);
				}
			}

			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$r_key = $record['no_referensi'];
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				//Helper::redirect($url);
			}
		}
		
		else if($r_aksi=='batalno'){
			$conn->StartTrans();
			$rkey=Helper::removeSpecial($_POST['rkey']);			
			$ideks=Helper::removeSpecial($_POST['ideks']);
			$record['kdkondisi']=Helper::removeSpecial($_POST['kl']);
			$record['kdlokasi']=Helper::removeSpecial($_POST['ll']);
			//update eksemplar kembali semula
			Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$ideks);
			//delete histori maintenance
			Sipus::DeleteBiasa($conn,pp_historymaintaining,idhistorymaintaining,$rkey);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
		}
		else if($r_aksi=='deleteAll'){
			$conn->StartTrans();
			$preDel = $conn->Execute("select * from pp_historymaintaining where no_referensi='$r_key'");
			if($preDel){
				while($rs=$preDel->FetchRow()){
				$record['kdlokasi']=$rs['kdlokasi_lama'];
				$record['kdkondisi']=$rs['kdkondisi_lama'];
				Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$rs['ideksemplar']);
				}
			}
			Sipus::DeleteBiasa($conn,pp_historymaintaining,no_referensi,$r_key);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() ==0){
				header('Location: '.$p_filelist);
				exit();
			}
		}
		else if($r_aksi=='reset'){
			Helper::redirect();
		}
	}
	$list=str_replace(',',",",$filter);
	$xlist=$list;
	if($r_key!=''){
		$p_sqlstr = "select e.noseri,p.judul,h.no_referensi,e.ideksemplar,h.idhistorymaintaining,h.tglhistory,h.kdkondisi,h.kdlokasi,h.kdkondisi_lama,h.kdlokasi_lama,h.keterangan from pp_eksemplar e join ms_pustaka p on e.idpustaka = p.idpustaka left join pp_historymaintaining h on e.ideksemplar=h.ideksemplar where h.no_referensi='$r_key'";
		if($arr !=''){
		$checkArr=$conn->GetOne("select e.noseri from pp_historymaintaining h 
								 join pp_eksemplar e on h.ideksemplar=e.ideksemplar and h.no_referensi='$r_key'
								 where  e.noseri in($xlist)");
		if($checkArr)
		$p_sqlstr .= " or e.noseri in ($xlist)";
		}
		$p_sqlstr .=" order by h.idhistorymaintaining,h.kdkondisi";
		$pustaka=$conn->Execute($p_sqlstr);								
	}else {
		// if($xlist!=''){
		if($xlist!=''){
		$pustaka=$conn->Execute("select e.noseri,p.judul,e.ideksemplar,e.kdkondisi,e.kdlokasi,p.kodeddc,p.authorfirst1,p.authorlast1 from pp_eksemplar e 
								join ms_pustaka p on e.idpustaka = p.idpustaka
								where e.noseri in ($xlist) and upper(e.statuseksemplar)=upper('ada') order by e.noseri ");	
		/*
		if($pustaka->fields['noseri']==''){
			$filter='';
			$sucdb = 'Eksemplar tidak dapat ditambahkan.';	
			Helper::setFlashData('sucdb', $sucdb);
			//Helper::redirect();
		}
		*/
		}
	}
	
	if($c_edit){
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		if($r_key!='')
		$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$pustaka->fields['kdlokasi'],true,false,0,'id="kdlokasi" disabled class="ControlStyle" style="width:250"');		
		else
		$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$_POST['kdlokasi'],true,false,0,'id="kdlokasi" class="ControlStyle" style="width:250" onchange="goKet(this.value)"');		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		if($r_key!='')
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$pustaka->fields['kdkondisi'],true,false,0,'id="kdkondisi" disabled class="ControlStyle" style="width:250"');
		else
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$_POST['kdkondisi'],true,false,0,'id="kdkondisi" class="ControlStyle" style="width:250"');
	}
	//print_r(explode(',',$xlist));
	//echo $pustaka->fields['no_referensi'];
	
	//echo "'".$x='ttttt'."'";
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post">	
			<!--table width="<?//= $p_tbwidth ?>">
				<tr height="20">
					<td align="center" class="PageTitle"><?//= $p_title ?></td>
				</tr>
			</table-->
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<?if($r_key){?>
			<td align="center">
				<!--<a href="javascript:goReset();" class="button"><span class="reset">Reset</span></a>-->
				<a href="index.php?page=repp_maintenance&noref=<?= $r_key ?>" target="_blank" class="button"><span class="print">Cetak Transaksi</span></a>
			</td>
			<?}?>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelAll();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table><br><?php include_once('_notifikasi.php'); ?>

			<header style="width:700px;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
			<table width="700" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
				<tr>
					<td width="150" class="LeftColumnBG thLeft">Lokasi Dikirim</td>
					<td><?= $l_lokasi ?>
					<? if($r_key!='') { ?>
					<input type="hidden" name="u_lokasi" id="u_lokasi" value="<?= $pustaka->fields['kdlokasi'] ?>">
					<? } ?>			</td>
				</tr>
				<tr>
					<td class="LeftColumnBG thLeft">Kondisi</td>
					<td><?= $l_kondisi ?>
					  <? if($r_key !=''){ ?>
					  <input type="hidden" name="u_kondisi" id="u_kondisi" value="<?= $pustaka->fields['kdkondisi'] ?>">
					<input type="hidden" name="u_tgl" id="u_tgl" value="<?= $pustaka->fields['tglhistory'] ?>">
					<? } ?>		  </td>
				</tr>
					<td class="leftColumnBG thLeft">Keterangan</td>
					<td>
					<? if($r_key=='') { ?>
					<?= UI::CreateTextArea('keterangan',$_POST['keterangan'],'ControlStyle',5,55,$c_edit) ?>
					<? } else { ?>
					<?= UI::CreateTextArea('keterangan',$pustaka->fields['keterangan'],'ControlRead',5,55,$c_edit,'readonly'); 
					}
					?>			</td>					
					
				</tr>
			</table>
			<br />
			<table  width="870" border="0" cellpadding="4" cellspacing=0 style="border-collapse:collapse" class="GridStyle">
				<tr>
					<td width="260" align="center" class="SubHeaderBGAlt thLeft">No. Induk</td>
					<td width="500" align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka</td>
					<td width="150" align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>
					<td width="100" align="center" class="SubHeaderBGAlt thLeft">No. Class</td>
					<td align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
				</tr>
				<? 
				if($arr!='' or $r_key!='') {
				while($row=$pustaka->FetchRow()) {
					if($row['kdkondisi']=='') $bgcolor='#EEEEEE'; else $bgcolor='white';
				?>
				<tr>
					<td bgcolor="<?= $bgcolor ?>" align="left" style="border:thin solid brown"><?= $row['noseri'] ?></td>
					<td bgcolor="<?= $bgcolor ?>" align="left" style="border:thin solid brown"><?= $row['judul'] ?></td>
					<td bgcolor="<?= $bgcolor ?>" align="left" style="border:thin solid brown"><?= $row['authorfirst1']." ".$row['authorlast1'] ?></td>
					<td bgcolor="<?= $bgcolor ?>" align="left" style="border:thin solid brown"><?= $row['kodeddc'] ?></td>
					<td bgcolor="<?= $bgcolor ?>" align="center" style="border:thin solid brown">
					<? if ($r_key!=''){
					if($row['kdkondisi']=='') {?>
					<u title="Batal" style="cursor:pointer" onclick="goBatal('<?= $row['noseri'] ?>')"><img src="images/delete.png"></u>
					<? } else {?>
					<u title="Batal" style="cursor:pointer" onclick="goBatalno('<?= $row['idhistorymaintaining'] ?>','<?= $row['ideksemplar'] ?>','<?= $row['kdkondisi_lama'] ?>','<?= $row['kdlokasi_lama'] ?>')"><img src="images/delete.png"></u>
					<? }}else { ?>
					<u title="Batal" style="cursor:pointer" onclick="goBatal('<?= $row['noseri'] ?>')"><img src="images/delete.png"></u>
					<? } ?>
					</td>
				</tr>
				<? } } 
				if ($r_key=='') {?>
				<tr>
					<td align="center" nowrap style="border:thin solid brown"><input type="text" name="noreg" id="noreg" size="20" maxlength="20" onKeyDown="etrTambah(event);"> <img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_maineks',600,500);"></td>
					<td align="center" style="border:thin solid brown"><input type="text" name="judul" id="judul" size="70" class="ControlRead" readonly></td>
					<td align="center" style="border:thin solid brown"><input type="text" name="pengarang" id="pengarang" size="30" class="ControlRead" readonly></td>
					<td align="center" style="border:thin solid brown"><input type="text" name="ddc" id="ddc" size="10" class="ControlRead" readonly></td>
					<td align="center" style="border:thin solid brown"><input type="button" name="btntambah" id="btntambah" value="Tambah" onClick="goTambah()"></td>
				</tr>
				<? } ?>
				<tr>
					<td colspan="5" class="footBG">&nbsp;</td>
				</tr>
			</table>	
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2">
		<input type="hidden" name="rkey" id="rkey">
		<input type="hidden" name="ideks" id="ideks">
		<input type="hidden" name="kl" id="kl">
		<input type="hidden" name="ll" id="ll">
		<input type="hidden" name="arr" id="arr" value="<?= $filter ?>">

		</form>
		</div>
	</div>
</div>
</body>
<script language="javascript">
function etrTambah(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById('btntambah').click();
}

function goTambah(){
	if(cfHighlight("noreg")){
	document.getElementById('act').value="tambah";
	var reg=document.getElementById('noreg').value;
	var val=document.getElementById('arr').value;
	var h=val.match(reg);
	
	if (h=='' || h==null){
	if(val=='')
	document.getElementById('arr').value="'"+document.getElementById('noreg').value+"'";
	else
	document.getElementById('arr').value=val+",'"+document.getElementById('noreg').value+"'";
	goSubmit();
	}else
		alert('Eksemplar tersebut telah ditambahkan');
	}
}

function goBatal(key){
	var batal=confirm("Apakah Anda yakin akan membatalkan maintenance eksemplar tersebut ?");
	if(batal){
		var val=document.getElementById('arr').value;
		var value = val.replace(key,"");
		document.getElementById('arr').value=value
		goSubmit();
	}
}

function goBatalno(key,key1,key2,key3){
	var batal=confirm("Apakah Anda yakin akan membatalkan maintenance eksemplar tersebut ?");
	if(batal){
		document.getElementById('act').value="batalno";
		document.getElementById('rkey').value=key;
		document.getElementById('ideks').value=key1;
		document.getElementById('kl').value=key2;
		document.getElementById('ll').value=key3;
		goSubmit();
	}
}

function goDelAll(){
	var del=confirm("Apakah Anda yakin akan menghapus data ini ?");
	if(del){
		document.getElementById('act').value="deleteAll";
		goSubmit();	
	}
}
function goKet(val){
	document.getElementById('keterangan').value='KIRIM UNTUK '+val;
}
function saveData(){
	if(cfHighlight("kdlokasi,kdkondisi")){
		var par = '<?= $r_key ?>';
		if(par==''){
			var arr=document.getElementById('arr').value;
			if(arr=='null' || arr==''){
			alert('Masukkan Eksemplar dengan benar');
			document.getElementById('noreg').focus();
			} else {
			document.getElementById('act').value='simpan';
			goSubmit();
			}
		}else
			alert('Penyimpanan data berhasil');
	}
}

function goReset(){
	document.getElementById('act').value='reset';
	goSubmit();
}
</script>
</html>