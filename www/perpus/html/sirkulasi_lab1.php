<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_del = $a_auth['candelete'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Sirkulasi Penimjaman Laboratorium';
	$p_title = 'Sirkulasi Peminjaman Laboratorium';
	$p_col = 9;
	$p_tbwidth = 1120;
	$p_id = "sirkulasi_lab";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idtransaksi';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	// sql untuk mendapatkan isi list
	//header('Location: '.$p_filelist);
	$fil=Helper::removeSpecial($_REQUEST['kd']);
	
	$p_sqlstr="select * from v_trans_list where kdlokasi='$fil'";

	
	// pengaturan ex
	if (!empty($_POST))
	{	
		//$p_sqlstr="select * from v_trans_list where 1=1";
		//$keyfilter=Helper::removeSpecial($_GET['kd']);
		//$keyfilter=Helper::removeSpecial($_POST['kdlokasi']);
		$carii=Helper::removeSpecial($_POST['txtkey']);
		
		
		//if($keyfilter!='')
			//$p_sqlstr.=" and kdlokasi='$keyfilter'";
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2 = Helper::removeSpecial($_POST['key2']);
		$r_seri = Helper::removeSpecial($_POST['txtpustaka']);
		$r_angg=  Helper::removeSpecial($_POST['idanggota']);
		$r_tgl=  Helper::removeSpecial($_POST['tgltransaksi']);
			
		
		
		
		if($r_aksi=="pinjam" and $c_add ){
			$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where noseri='$r_seri'");
			$r_eks=$p_eks['ideksemplar'];
			$p_angg=$conn->GetRow("select namaanggota,tglselesaiskors,statusanggota from ms_anggota where idanggota='$r_angg'");
			$p_cek=$conn->GetRow("select statuseksemplar from pp_eksemplar where ideksemplar=$r_eks and kdkondisi='V'");
			$p_cek2=$conn->GetRow("select tglexpired from pp_reservasi where ideksemplarpesan=$r_eks and tglexpired>=current_date");
			if (!$p_angg){
				$errdb = 'Identitas Anggota tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);	
			}else {
			if($p_angg['tglselesaiskors']>=date('Y-m-d') or $p_angg['statusanggota']==0){
				$errdb = 'Peminjaman Pustaka gagal, Anggota sedang bermasalah.';	
				Helper::setFlashData('errdb', $errdb);
			
			}else {
			if (!$p_cek2 and $p_cek['statuseksemplar']=='ADA'){
			
			$record=array();
			$record = Helper::cStrFill($_POST);
			$record['kdlokasi']=Helper::removeSpecial($_POST['kdlokasi2']);
			$record['kdjenistransaksi']='PJL';
			$record['tgltransaksi']=Helper::formatDate($_POST['tgltransaksi']);
			$record['ideksemplar']=$r_eks;
			$record['statustransaksi']=1;
			$record['fix_status']=1;
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			$rec['statuseksemplar']='PJM';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$r_eks);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Peminjaman Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Peminjaman Pustaka berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//$url='index.php?page=sirkulasi_lab&kd='.$keyfilter;
				//Helper::redirect();
			}	
			}else{
				$errdb = 'Pustaka tidak dapat dipinjam.';	
				Helper::setFlashData('errdb', $errdb);
			}}
		}
		}
		elseif($r_aksi=="kembali" and $c_add and $c_edit){
			$conn->StartTrans();
			$record['tglpengembalian']=date("Y-m-d");
			$record['statustransaksi']=0;
			Helper::Identitas($record);
			Sipus::UpdateBiasa($conn,$record,$p_dbtable,idtransaksi,$r_key);
			
			$rec['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$r_key2);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Pengembalian Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pengembalian Pustaka Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//$url='index.php?page=sirkulasi_lab&kd='.$keyfilter;
				Helper::redirect();
			}	
		}
		elseif ($r_aksi=="cari"){
			$r_key3= Helper::removeSpecial($_POST['key3']);
			if($fil==''){
				$errdb = 'Masukkan Lokasi laboratorium.';	
				Helper::setFlashData('errdb', $errdb);
			} else{
			if ($r_key3!='' and $carii!=''){
				$p_sqlstr .=" and $r_key3 ='$carii'";
			}else{
				$errdb = 'Masukkan kategori dan kata kunci dengan benar.';	
				Helper::setFlashData('errdb', $errdb);}
		}}
		elseif ($r_aksi=="batal"){
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_transaksi,idtransaksi,$r_key);
			
			$record['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$r_key2);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Pembatalan Peminjaman gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembatalan Peminjaman Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
			
		
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	// list lokasi
	if($c_edit) {
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2("kdlokasi",$fil,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:140" onChange="goFilter()"');
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi2 = $rs_cb->GetMenu2("kdlokasi2",$_POST['kdlokasi2'],true,false,0,'id="kdlokasi2" class="ControlStyle" style="width:140" onChange="goFilter2()"');
		
		$a_filt=array('' => 'Pilih', 'idanggota' => 'Id Anggota', 'noseri' => 'Nomor Seri');
		$l_filt = UI::createSelect('kategori',$a_filt,$r_key3,'ControlStyle',true,'id="kategori" onchange="goPilih()" style="width:140"');
		
		}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
		<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>																			<?// = $fil!='' ? "initButton(($p_atfirst ? '1' : '0'),($p_atlast ? '1' : '0'));" : "" ?>						 
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>)" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile."&kd=".$fil; ?>">			
	<div id="items" style="position:relative;bgcolor:#fff000;">
	<table width="600" cellspacing="0" cellpadding="0"  border="0" style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
		<tr height="25">
			<th colspan="4"><?= $p_title; ?></td>
		</tr>
		<tr height="5">
			<td>&nbsp;</td>
		</tr>
		<tr>
		<td style="padding-right:10px;" valign="top">
		<table cellpadding="2" cellspacing="0" width="260" border="0" style="background:url(images/separator.png) no-repeat right center;">
			<tr height=25>
				<td colspan="2" align="left"><b><u>Pencarian : </u></b></td>
			</tr>
			<tr>
				<td>Lokasi</td>
				<td>: <?= $l_lokasi ?></td>
			</tr>
			<tr>
				<td width="90">Kategori</td>
				<td width="170">: 
				<?= $l_filt ?>
				</td>
			</tr>
			<tr>
				<td valign="top">Kata Kunci</td><td>: <input type="text" id="txtkey" name="txtkey" value="<?= $carii ?>" maxlength="75" onKeyDown="etrSearch(event);"><br></td>
			</tr>

			<tr>
				<td colspan="2" align="center" ><input type="button" id="btncari" name="btncari" value="Cari" onClick="goCari()" class="buttonSmall" style="cursor:pointer;height:23px;width:130px;font-size:12px;"></td>
			</tr>
		</table></td><td>
		<table cellpadding="2" cellspacing="0" border="0" width="395" >
			<tr height=25>
				<td colspan="2" align="left"><b><u>Peminjaman Baru : </u></b></td>
			</tr>
			<tr>
				<td width=100>Lokasi Lab.</td>
				<td>: <?= $l_lokasi2 ?></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Id Anggota</td>
				<td>: <?= UI::createTextBox('idanggota',$r_angg,'ControlStyle',20,20,$c_edit); ?>
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=s',630,500);">
				<span id="span_anggota"><u title="Lihat Anggota" onclick="popup('index.php?page=pop_anggota&code=s',630,500);" style="cursor:pointer;" class="Link">Lihat Anggota</u></span>
				
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Tanggal Pinjam</td>
				<td>: <?= UI::createTextBox('tgltransaksi',date('d-m-Y'),'ControlStyle',10,10,$c_edit) ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgletrans" style="cursor:pointer;" title="Pilih tanggal pinjam">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgltransaksi",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgletrans",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				<? } ?>
				[ Format : dd-mm-yyyy ]
				
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>NO INDUK</td>
				<td>: <?= UI::createTextBox('txtpustaka',$r_eks,'ControlStyle',20,20,$c_edit); ?>
	<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam',700,500);">
	<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar</u></span>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<? if($c_add) { ?><input type="button" id="btntrans" name="btntrans" onClick="goPinjam();" value="Pinjam" class="buttonSmall" style="cursor:pointer;height:23px;width:130px;font-size:12px;padding-bottom:3px;">
				<? } ?>
				</td>
			</tr>
		</table></td>
	</table>
	<div id="a" align="center"><? include_once('_notifikasi_trans.php') ?><br></div>
	<? //if($fil!='') { ?>
	<table cellpadding="0" cellspacing="0" border="0" width="98%">
			<tr>
				<td align="right" valign="middle" colspan=3>
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" width="98%">
	<tr height="20">
		<td width="60" nowrap align="center" class="SubHeaderBGAlt">Batal</td>		
		<td width="60" nowrap align="center" class="SubHeaderBGAlt">Kembali</td>	
		<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','idanggota:C');">Id Anggota <?= $p_xsort['idanggota']; ?></td>
		<td width="60%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul Pustaka <?= $p_xsort['judul']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','tgltransaksi:D');">Tanggal Pinjam<?= $p_xsort['tgltransaksi']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','tglpengembalian:C');">Tanggal kembali<?= $p_xsort['tglpengembalian']; ?></td>
	
		<?php
		$i = 0;

		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idtranskasi'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="30" valign="middle"> 
		<td align="center">
		<? if ($c_del and $row['tglpengembalian']=='') {?>
		<img src="images/batal.png" title="Pembatalan Peminjaman" style="cursor:pointer" onClick="goBatal('<?= $row['idtransaksi'] ?>','<?= $row['ideksemplar'] ?>')">
		<? } else {?>
		<img src="images/batal2.png" title="Pembatalan Peminjaman">
		<? } ?>
		</td>
		<td align="center">
		<? if($c_add and $c_edit and $row['tglpengembalian']==''){ ?>
		<img src="images/kembalikan.png" title="Pengembalian Pustaka" onClick="goBalek('<?= $row['idtransaksi'] ?>','<?= $row['ideksemplar'] ?>')" style="cursor:pointer">
		<?} else { ?>
		<img src="images/kembalikan.png" title="Pustaka Telah dikembalikan">
		<? } ?>
		</td>
		<td align="center"><span title="Id Penanggung Jawab"><?= $row['idanggota'] ?></span></td>
		<td align="left">&nbsp; <?= $row['judul']; ?></td>
		<td align="center"> <?= Helper::tglEng($row['tgltransaksi']); ?></td>
		<td align="center">
		<? if ($row['tglpengembalian']=='') 
				echo "Belum Kembali";
			else
				echo Helper::tglEng($row['tglpengembalian']); ?>
		</td>
	</tr>
	<?php
		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr></table>
	<?// } ?>
</div>

</div>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="act" id="act">
<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

</form>
</div>



</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>

<script type="text/javascript">
$(function(){
	   $("#tgltransaksi").mask("99-99-9999");
});
</script>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script language="javascript">
function etrSearch(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btncari").click();
}

function goPinjam() {
	var pinjam = confirm("Apakah Anda yakin akan melakukan peminjaman ?");
	if(pinjam){
		document.getElementById("act").value='pinjam';
		if(cfHighlight("kdlokasi2,idanggota,tgltransaksi,txtpustaka")){
		var kd=document.getElementById("kdlokasi").value;
		document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
		goSubmit();
		}
	}
}

function goFilter(){
	var kd=document.getElementById("kdlokasi").value;
	document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
	goSubmit();
}
function goFilter2(){
	var kd=document.getElementById("kdlokasi2").value;
	document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
	goSubmit();
}

function goBalek(key,key2){
	var balek = confirm("Apakah Anda yakin akan mengembalikan pustaka tersebut ?");
	if(balek){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=key;
		document.getElementById("key2").value=key2;
		var kd=document.getElementById("kdlokasi").value;
		document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
		goSubmit();
	}
}

function goCari(){
	var kd=document.getElementById("kdlokasi").value;

	if(cfHighlight("kdlokasi,kategori,txtkey")){
	document.getElementById("act").value='cari';
		
	document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
	goSubmit();
	}
}

function goBatal(key,key2){
	var batal = confirm("Apakah Anda yakin akan membatalkan peminjaman ?");
	if(batal){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=key;
		document.getElementById("key2").value=key2;
		var kd=document.getElementById("kdlokasi").value;
		document.getElementById("perpusform").action = 'index.php?page=sirkulasi_lab&kd='+kd;
		goSubmit();
	}
}

function goPilih(){
	var x=document.getElementById("kategori").value;
	document.getElementById("key3").value=x;
}
</script>


</html>