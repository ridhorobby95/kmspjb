<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengolahan.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	$r_key = Helper::removeSpecial($_REQUEST['key']); 
	$rkey = Helper::removeSpecial($_REQUEST['rkey']); 
		
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Data Eksemplar';
	$p_title = '.: Data Eksemplar :.';
	$p_tbwidth = 720;

	$p_filelist = Helper::navAddress('xdata_rwteksemplarinv.php');

	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']); 
		
		if($r_aksi == 'simpan' and $c_edit) {			
			$recdetail = array();
			$recdetail['kdklasifikasi'] = Helper::cStrNull($_POST['kdklasifikasi']);
			$recdetail['kdperolehan'] = Helper::cStrNull($_POST['kdperolehan']);  
			$recdetail['harga'] = Helper::cStrNull($_POST['harga']);  
			$recdetail['kdkondisi'] = 'V'; 
			$recdetail['tglpengadaan'] = Helper::formatDate($_POST['tglpengadaan']);
			
			$err = Sipus::UpdateBiasa($conn,$recdetail,'pp_eksemplar','ideksemplar',$rkey);
			
			if($err != 0){
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		

		if ($rkey != ''){
			$p_sqlstr = "select * from ms_pustaka p
						left join pp_eksemplar e on e.idpustaka=p.idpustaka 
						left join lv_bahasa b on b.kdbahasa=p.kdbahasa
						left join lv_jenispustaka lj on lj.kdjenispustaka=p.kdjenispustaka
						where e.ideksemplar=$rkey";
			$rowp = $conn->GetRow($p_sqlstr);
		}

		$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");
		$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$rowp['kdklasifikasi'],true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:200"');
		$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by kdperolehan");
		$l_perolehan = $rs_cb->GetMenu2('kdperolehan',$rkey != '' ? $rowp['kdperolehan'] : $row['kdperolehan'],true,false,0,'id="kdperolehan" class="ControlStyle" style="width:200"');

	}
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width="<?= $p_tbwidth ?>" border=0>
	<tr height="40">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="100" border="0">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<? if ($c_edit) {?>
		<a href="javascript:goPostX('<?= $p_filelist; ?>','key=<?= $r_key; ?>')" class="button"><span class="list">Daftar </span></a>
		<? } ?>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:goProses();" class="button"><span class="validasi">Simpan</span></a>
	</td>
	<? } ?>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	</tr>
</table>
<br><? include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<table width="<?= $p_tbwidth ?>" border="1" cellspacing="0" cellpadding="6" class="GridStyle">
	<tr> 
		<td width="720" class="SubHeaderBGAlt" colspan="2" align="center">Data Pustaka</td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG" width="150">Judul Pustaka *</td>
		<td class="RightColumnBG"><?= $rowp['judul']?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Judul Seri </td>
		<td class="RightColumnBG"><?= $rowp['judulseri']?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Edisi </td>
		<td class="RightColumnBG"><?= $rowp['edisi']?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG" width="150">Jenis Pustaka *</td>
		<td class="RightColumnBG"><?= $rowp['namajenispustaka'] ?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Bahasa *</td>
		<td class="RightColumnBG"><?= $rowp['namabahasa']?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Pengarang *</td>
		<td class="RightColumnBG">
			<? if (!empty($rowp['authorfirst3'])) $y=3; else if (!empty($rowp['authorfirst2'])) $y=2; else if(!empty($rowp['authorfirst1'])) $y=1; else $y=0;
			for ($x=1; $x<=$y;$x++){
				echo $x.'. '.$rowp['authorfirst'.$x].' '.$rowp['authorlast'.$x].'<br>'; ?>
			<? } ?>
			<table id="table_templateau" style="display:none">
				<tr> 
					<td bgcolor="#EEEEEE"><input type="hidden" name="addauthor[]" id="addauthor" disabled>
					<input type="hidden" name="namaauth[]" id="namaauth" disabled>
					</td>
					<td bgcolor="#EEEEEE" width="20" align="center">
					<img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer">
					</td>
				</tr>
			</table>
        </td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">ISBN </td>
		<td class="RightColumnBG"><?= $rowp['isbn']?></td>
	</tr>
	<tr height="30"> 
		<td class="LeftColumnBG">Kota Terbit </td>
		<td class="RightColumnBG"><?= $rowp['kota']?></td>
	</tr>
	<tr> 
		<td class="SubHeaderBGAlt" colspan="2" align="center">Data Eksemplar</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="150">Label *</td>
		<td class="RightColumnBG">
		<?= $l_klasifikasi ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="150">Asal Perolehan *</td>
		<td class="RightColumnBG">
		<?= $l_perolehan ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tanggal Perolehan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglpengadaan',Helper::formatDate($rowp['tglpengadaan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="e_tglpengadaan" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglpengadaan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "e_tglpengadaan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		[ Format : dd-mm-yyyy ]
		<? } ?>
		
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Harga </td>
		<td class="RightColumnBG"><?= UI::createTextBox('harga',$rowp['harga'],'ControlStyle',14,14,$c_edit); ?></td>
	</tr>
</table>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="rkey" id="rkey" value="<?= $rkey ?>">
</form>

<div id="newpengarang_block" class="popWindow" style="position:absolute;">
	<form method="post" onSubmit="isnewPengarang();return false;">
	<input type="hidden" name="pf" value="edit_disposisi" />
    <table cellspacing=1 cellpadding="4">
		<tr class="popWindowtr">
			<td colspan="2">
				<div style="float:left;font-weight:bold;padding:2px">Tambah Pengarang</div>
				<div style="float:right">
				<span id="loadingGif2" style="display:none"><img src="images/loading.gif" /></span>
				&nbsp;
				<input type="button" class="btn_close" value="" onClick="closePop()"/>
				</div>
			</td>
		</tr>
        <tr> 
          <td width="120">Nama Depan</td>
          <td>
		<input type="text" id="auth_namadepan" name="auth_namadepan" size="30" maxlength="100"> 
          </td>
        </tr>
        <tr> 
          <td>Nama Belakang</td>
          <td>
		<input type="text" id="auth_namabelakang" name="auth_namabelakang" size="30" maxlength="100"> 
          </td>
        </tr>
    </table>
    <div style="margin-top:5px;padding:5px;background-color:#FFD787;text-align:center;"><input type="submit" value="Tambah" class="btn" /></div>
	</form>
</div>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script language="javascript">
function addBahasa(kode,nama) {
	$("#kdbahasa").val(kode);
	$("#namabahasa").val(nama);
}

function addPengarang(kode,nama) {
	$("#idauthor").val(kode);
	$("#pengarang").val(nama);
}

function goProses() {
	if(cfHighlight("kdklasifikasi,kdperolehan,tglpengadaan")){
		sent = $("#perpusform").serialize();
		sent += "&act=simpan";
		goPostX('<?= $i_phpfile; ?>',sent);
	}
}

function addAuthor() {
	var idauthor = $("#idauthor").val();
	if(idauthor=='') {
		$("#span_error2").show();
		setTimeout('$("#span_error2").hide()',1000);
		return false; 
	} else {
		if($("[name='addauthor[]'][value='"+idauthor+"']").length > 0) {
			$("#span_error").show();
			setTimeout('$("#span_error").hide()',1000);
			return false;
		}
	}
	
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_aukosong:visible").length > 0)
		$("#tr_aukosong").hide();
	
	var newtab = $($("#table_templateau tbody").html()).insertBefore("#tr_authorhidden");
	var newtdf = newtab.find("td").eq(0);
	var addauthor = newtdf.find("[name='addauthor[]']");
	var namaauthor = $("#pengarang").val();
	
	newtdf.prepend(namaauthor);
	addauthor.val(idauthor);
	addauthor.attr("disabled",false);
}

function delAuthor(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
}

</script>
</html>