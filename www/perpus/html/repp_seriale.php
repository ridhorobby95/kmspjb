<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	if(is_array($_POST['jenis']))
		$r_jenis = implode("','",$_POST['jenis']);
	else
		$r_jenis = $_POST['jenis'];
	$r_asal = Helper::removeSpecial($_POST['kdperolehan']);
	$r_tgl1 = Helper::formatDate($_POST['tgl1']);
	$r_tgl2 = Helper::formatDate($_POST['tgl2']);
	$r_jurusan = Helper::removeSpecial($_POST['kdjurusan']);
	
	
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Data Serial Perjudul';
	
	$p_namafile = 'rekapseriale_'.$r_tgl1.'-'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	// $sqlp = "select idpustaka, noseri,judul, namaperiode from ms_pustaka p left join lv_periode o on p.idperiode=o.idperiode where noseri='$r_seri' ";
	
	// if ($r_jenis!=''){
		// $sqlp .=" and kdjenispustaka in ('$r_jenis')";
	// }
	
		
	// $rowp = $conn->GetRow($sqlp);
	
	$sql = "select max(p.noseri) as noseri,max(kdperolehan) as kdperolehan , 
			max(namakondisi) as namakondisi,s.keterangan,tgldatang,p.judul,max(jumlah)as jumlah 
			from pp_eksemplar e 
			join ms_pustaka p on e.idpustaka = p.idpustaka
			left join pp_bidangjur j on j.idpustaka=p.idpustaka
			left join lv_kondisi k on e.kdkondisi = k.kdkondisi 
			left join pp_serialitem s on e.idserialitem=s.idserialitem where 1=1 ";
			
	if($r_asal!='')
		$sql .= " and kdperolehan='$r_asal' ";
	
	if ($r_jurusan != 'semua'){
		$sql .= " and j.kdjurusan='$r_jurusan' ";
		$namajurusan = $conn->GetOne("select namajurusan from lv_jurusan where kdjurusan='$r_jurusan'");
	}
		
	$sql .=" and tgldatang between '$r_tgl1' and '$r_tgl2' ";
        $sql .=" group by p.judul,s.keterangan,tgldatang ";
	$sql .=" order by max(p.noseri) ";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="left" colspan=2>
		<strong>
		<h2>Data Serial Per Periode</h2>
		</strong>
	</td>
  </tr>
<? if (!empty($namajurusan)){ ?>
	<tr>
		<td><b>Jurusan</b></td><td>: <?= $namajurusan ?></td>
	</tr>
<? } ?>
	<tr>
		<td><b>Periode</b></td><td>: <?= Helper::formatDate($r_tgl1) .' s/d '. Helper::formatDate($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="100" align="center"><strong>NO INDUK</strong></th>
	<th width="200" align="center"><strong>Judul</strong></th>
    <th width="230" align="center"><strong>Terbitan</strong></th>
	<th width="120" align="center"><strong>Status</strong></th>
    <th width="100" align="center"><strong>Tgl Perolehan</strong></th>
    <th width="80" align="center"><strong>Jumlah</strong></th>
	
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="left"><?= $rs['noseri'] ?></td>
	<td align="left"><?= $rs['judul'] ?></td>
	<td align="left"><?= $rs['keterangan'] ?></td>
    <td align="left"><?= $rs['namakondisi'] ?></td>
	<td align="left"><?= Helper::formatDate($rs['tgldatang']) ?></td>
	<td><?= $rs['kdperolehan'] .' : '.$rs['jumlah'] ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan=9><b>Jumlah : <?= $rsc ?></b></td>
	</tr>
   
</table>


</div>
</body>
</html>