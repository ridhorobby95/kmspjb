<?php
	defined('__VALID_ENTRANCE') or die('Akses terbatas');

	$a_auth = Helper::checkRoleAuth($conng, false);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$c_add = $a_auth['cancreate'];
	
	$p_window = '[PJB LIBRARY] Serial Control';
	
	//parameter
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	if($r_key=='')
		header("location: index.php?page=home");
	
	if($r_key!=''){
	$sqljenis = $conn->Execute("select * from pp_serialcontrol where idpustaka='$r_key'");
	$judul = $conn->GetRow("select judul,noseri from ms_pustaka where idpustaka='$r_key'");
	
	if(!$judul)
		header("location: index.php?page=home");
	}
	$r_id = Helper::removeSpecial($_GET['id']);
	
	if($r_id!='')
	$rs = $conn->Execute("select * from pp_serialitem where idserial='$r_id' order by seq_number");

	if(!empty($_POST)){
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$rid = Helper::removeSpecial($_POST['id']);
		
		if($r_aksi=='simpan'){
			$conn->StartTrans();
			$record['tglperkiraan'] = Helper::formatDate($_POST['tglmulai_'.$rid]);
			$record['tgldatang'] = Helper::formatDate($_POST['tgldatang_'.$rid]);
			$record['jumlah'] = Helper::removeSpecial($_POST['jumlah_'.$rid]);
			$record['keterangan'] = Helper::removeSpecial($_POST['keterangan_'.$rid]);
			Helper::Identitas($record);
			
			Sipus::UpdateBiasa($conn,$record,pp_serialitem,idserialitem,$rid);
			
			$eks = $record['jumlah'];
			if($eks!='' and $eks>0){
				for($i=0;$i<$eks;$i++){
				$lastid=Sipus::GetLast($conn,pp_eksemplar,ideksemplar);
				$receks['ideksemplar']=$lastid;
				$receks['idpustaka']=$r_key;
				$receks['kdklasifikasi']='SR';
				$receks['kdperolehan']='B';
				$receks['kdkondisi']='V';
				$receks['tglperolehan']=$record['tgldatang'];
				$receks['tglterbit']=$record['tgldatang'];
				$receks['noseri']=Sipus::CreateSeri($conn,$judul['noseri'],$r_key);
				$receks['statuseksemplar']='ADA';
				$receks['idserialitem']=$rid;
				Helper::Identitas($receks);
				
				Sipus::InsertBiasa($conn,$receks,pp_eksemplar);
				
				}
			}else
			$error=true;
		
			if($error==true)
				$conn->CompleteTrans(false);
			else
				$conn->CompleteTrans(true);
			
			
			
			if($conn->ErrorNo() == 0) {
				$sucdb = 'Perubahan data serial item berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			} else {
				$errdb = 'Perubahan data serial item gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		
		}elseif($r_aksi=='insersi'){
			$record['idserialitem'] = Sipus::GetLast($conn,pp_serialitem,idserialitem);
			$record['seq_number'] = Helper::removeSpecial($_POST['seq_number']);
			$record['tglperkiraan'] = Helper::formatDate($_POST['tglmulaibaru']);
			$record['tgldatang'] = Helper::formatDate($_POST['tgldatangbaru']);
			$record['jumlah'] = Helper::removeSpecial($_POST['jumlah']);
			$record['keterangan'] = Helper::removeSpecial($_POST['keterangan']);
			$record['idserial'] = $r_id;
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,pp_serialitem);
			
			if($err==0){
				$sucdb = 'Penambahan data serial item berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}else{
				$errdb = 'Penambahan data serial item gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}elseif($r_aksi=='hapus'){
			$idhapus = Helper::removeSpecial($_POST['id']);
			if($idhapus!='')
				$err = Sipus::DeleteBiasa($conn,pp_serialcontrol,idserial,$idhapus);
			
			if($err == 0){
				$sucdb = 'Penghapusan data serial item berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}else{
				$errdb = 'Penghapusan data serial item gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		
		}
	Helper::redirect();
	}
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<?php include('inc_menu.php'); ?>
    <div id="wrapper">
		<div class="SideItem" id="SideItem">
	<form name="perpusform" id="perpusform" method="post"  enctype="multipart/form-data">
	<table cellpadding="0" cellspacing="0" align="center" width="98%" style="border-collapse:collapse">
	<tr>
		<td id="leftcolhead" style="background:url(images/tab_header_gray.gif) no-repeat; font-size:12px;" height="25" valign="middle">
			<a href="javascript:void(0);" id="minibutton"><img id="tabbutton" align="absmiddle" border="0" src="images/tab_minimize.gif"/></a>
			<font color="#FFFFFF"><strong>Serial Control</strong></font></td>
		<td style="background:url(images/tab_header_gray.gif) no-repeat; padding-left: 30px; font-size:12px;" height="25">
			<font color="#FFFFFF"><strong>Serial Item</strong></font></td>
	</tr>
	<tr height=150>
		<td id="leftcolmenu" valign="top" style="border: #a3b3c9 1px solid; width:400px;" bgcolor="#f5f5f5">
		<div align="center"><br>
			<? if($c_add){?>
			<table width="100%" cellpadding="4" cellspacing=0>
				<tr>
					
					<td width="170"  align="center">
					<u onclick="popup('index.php?page=list_serialcontrol&key=<?= $r_key ?>',600,400);" style="cursor:pointer" class="buttonshort"><span class="new">
					&nbsp;&nbsp;Buat baru</span></u></td>
					
				</tr>

			</table>
			<? } ?>
			<br>
			<table width="95%" cellpadding="4" cellspacing=0  style="border: #a3b3c9 1px solid;">
				<tr>
					<td colspan="2" class="SubHeaderBGAlt"><b>Pilih Serial Control : </b></td>
				</tr>
				<? 
				$i=0;
				while($row=$sqljenis->FetchRow()){
					$isOk = Sipus::isEksemplar($conn,$row['idserial']);
				if ($i % 2 and $r_id!=$row['idserial']) $rowstyle = 'NormalBG'; elseif($r_id==$row['idserial']) $rowstyle = 'BlueBG'; else $rowstyle = 'AlternateBG'; $i++; 
				?>
				<tr class="<?= $rowstyle ?>">
					<td style="line-height:2;cursor:pointer" onClick="goShow('index.php?page=ms_serialcontrol&key=<?= $r_key ?>&id=<?= $row['idserial'] ?>')">
					<b><u><?= $row['periode'] ?></u></b> | <?= Helper::tglEng($row['tglmulai']) ?> |  
					<?= $row['keterangan'] ?>
					</td>
					<? if($c_delete and $isOk){?>
					<td width="30" align="center"><img src="images/delete.png" title='Hapus' style="cursor:pointer" onClick="goHapus('<?= $row['idserial'] ?>')"></td>
					<? } ?>
				</tr>

				<? } ?>
				<? if($i==0){ ?>
				<tr>
					<td colspan=2 align="center"><b>Data masih kosong</b></td>
				</tr>
				<? } ?>
			</table>
			<br>
			
		</div>
		</td>
		<td valign="top" align="left" id="contents" style="margin-left:100px; border: #a3b3c9 1px solid">
		<div style="margin-left:20px;">
		<div align="center"><?php include_once('_notifikasi.php'); ?></div>
		<header style="width:690px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/keanggotaan.png" alt="" onerror="loadDefaultActImg(this)">
					<h1>Majalah <?= $judul['judul'] ?></h1>
				</div>
			</div>
		</header>
		<table width="690" cellpadding="4" cellspacing=0 class="gridStyle">
			<tr>
				<th width="50" align="center" class="SubHeaderBGAlt thLeft">No Urut</th>
				<th width="120" align="center" class="SubHeaderBGAlt thLeft">Tgl Pesan</th>
				<th width="120" align="center" class="SubHeaderBGAlt thLeft">Tgl Datang</th>
				<th width="60" align="center" class="SubHeaderBGAlt thLeft">Jumlah</th>
				<th width="180" align="center" class="SubHeaderBGAlt thLeft">No. Seri</th>
				<th width="60" align="center" class="SubHeaderBGAlt thLeft">Proses</th>
			</tr>
			<? $i=0;
			if($r_id!=''){
			while($rows = $rs->FetchRow()) { $i++;
			$x=$rows['seq_number'];
			$isPro = Sipus::isEksemplarItem($conn,$rows['idserialitem']);
			?>
			<tr>
				<td align="center"><?= $rows['seq_number'] ?></td>
				<td align="center">
					<?= UI::createTextBox('tglmulai_'.$rows['idserialitem'],Helper::formatDate($rows['tglperkiraan']),'ControlStyle',10,10,false); ?>
					<!--<input type="text" name="tglmulai_<?= $rows['idserialitem'] ?>" id="tglmulai_<?= $rows['idserialitem'] ?>" maxlength="10" size="10" value="<?= Helper::formatDate($rows['tglperkiraan']) ?>" readonly></td>-->
				<td align="center">
					<?= UI::createTextBox('tgldatang_'.$rows['idserialitem'],Helper::formatDate($rows['tgldatang']),'ControlStyle',10,10,$isPro); ?>
					<!--<input type="text" name="tgldatang_<?= $rows['idserialitem'] ?>" id="tgldatang_<?= $rows['idserialitem'] ?>" maxlength="10" size="10" value="<?= Helper::formatDate($rows['tgldatang']) ?>">-->
				<?php if($isPro){?>
				<img src="images/cal.png" id="tgledatang_<?= $rows['idserialitem'] ?>" style="cursor:pointer;" title="Pilih tanggal datang">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgldatang_<?= $rows['idserialitem'] ?>",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgledatang_<?= $rows['idserialitem'] ?>",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				<?php } ?>
				</td>
				<td align="center">
					<?= UI::createTextBox('jumlah_'.$rows['idserialitem'],($rows['jumlah']!='' ? $rows['jumlah'] : '1'),'ControlStyle',10,5,$isPro); ?>
					<!--<input type="text" name="jumlah_<?= $rows['idserialitem'] ?>" maxlength="10" size="5" value="<?= $rows['jumlah']!='' ? $rows['jumlah'] : '1' ?>"></td>-->
				<td>
					<?= UI::createTextBox('keterangan_'.$rows['idserialitem'],$rows['keterangan'],'ControlStyle',255,60,$isPro); ?>
					<!--<input type="text" name="keterangan_<?= $rows['idserialitem'] ?>" maxlength="255" size="35" value="<?= $rows['keterangan'] ?>"></td>-->
				<td align="center">
				<? //if($rows['tgldatang']==''){ ?>
				<? if($isPro){ ?>
				<img src="images/Gear_32.png" title="Terima / Proses Eksemplar" style="cursor:pointer" width=20 height=20 onClick="goSerial('<?= $rows['idserialitem'] ?>')">
				<? } else { ?>
				<img src="images/ada.png" title="Proses Selesai" width=20 height=20>
				<? } ?>
				</td>
			</tr>
			<? }} ?>
			<? if ($i==0) { ?>
			<tr>
				<td colspan="6" align="center"><b>Jadwal kedatangan Kosong</b></td>
			</tr>
			<? } ?>
			<? if($c_add and $r_id!=''){?>
			<tr>
				<td align="center"><input type="text" name="seq_number" maxlength="10" size="5" value="<?= $x+1 ?>" readonly ></td>
				<td align="center"><input type="text" name="tglmulaibaru" id="tglmulaibaru" maxlength="10" size="10">
				<img src="images/cal.png" id="tglemulai" style="cursor:pointer;" title="Pilih tanggal mulai">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglmulaibaru",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tglemulai",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				</td>
				<td align="center"><input type="text" name="tgldatangbaru" id="tgldatangbaru" maxlength="10" size="10">
				<img src="images/cal.png" id="tgledatang" style="cursor:pointer;" title="Pilih tanggal mulai">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgldatangbaru",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgledatang",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				</td>
				<td align="center"><input type="text" name="jumlah" maxlength="10" size="5" value=1></td>
				<td><input type="text" name="keterangan" maxlength="255" size="35"></td>
				<td align="center"><img src="images/disk.png" style="cursor:pointer" title="tambah data baru" onClick="goInsersi()"></td>
			</tr>
			<? } ?>
		</table>	
				
		</div>
		</td>
	</tr>
	</table>
	<input type="hidden" name="act" id="act">
	<input type="hidden" name="id" id="id">
	</form>
    </div>
    </div>
</body>

</html>
<script type="text/javascript" language="javascript">
function goShow(file) {
	document.getElementById("perpusform").action = file;
	goSubmit();
}

function goSerial(key){
	var pesan = confirm("Apakah Anda yakin akan melakukan proses untuk serial ini?");
	if(pesan){
	if(cfHighlight("tgldatang_"+key)){
		document.getElementById('act').value='simpan';
		document.getElementById('id').value=key;
		goSubmit();
	}
	}
}

function goInsersi(){
	if(cfHighlight("tgldatang,jumlah")){
	document.getElementById('act').value='insersi';
	goSubmit();
	}
}

function goHapus(key){
	var hapus = confirm("Apakah Anda yakin akan menghapus data ini ?");
	if(hapus){
	document.getElementById('act').value='hapus';
	document.getElementById('id').value=key;
	goSubmit();
	}
}

</script>