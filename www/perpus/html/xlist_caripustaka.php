<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,'list_caripustaka');
 
 	require_once('classes/fts.class.php');

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canupdate'];
		
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Daftar Pustaka';
	$p_title = 'Pencarian Daftar Pustaka';
	$p_tbheader = '.: Pencarian Daftar Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 900;
	$p_filedetail = Helper::navAddress('ms_pustaka.php');
	$p_id = "ms_pustaka";
	$p_eksemplar = Helper::navAddress('ms_eksemplar.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idpustaka';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	//print_r($_REQUEST);
	
	if (!empty($_POST)) {
		$isResult = true;
		$r_keyword = Helper::removeSpecial(Helper::strOnly(trim($_REQUEST['keyword'])));
		$r_tipe = Helper::removeSpecial($_POST['tipecari']);
		$r_bahasa = Helper::removeSpecial($_POST['kdbahasa']);
		$r_tahun1 = Helper::removeSpecial($_POST['tahun1']);
		$r_tahun2 = Helper::removeSpecial($_POST['tahun2']);
						
		$p_sqlstr = FTS::cariPustaka($r_keyword,$r_tipe,$r_bahasa,$r_tahun1,$r_tahun2);	
		$p_insql = FTS::getIdPustaka($r_keyword,$r_tipe,$r_bahasa,$r_tahun1,$r_tahun2);
		
	}
	
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
			
	if ($isResult){
		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
		if ($rs->EOF) {
			// tidak ditemukan record atau ada kesalahan
			$p_atfirst = true;
			$p_atlast = true;
			$p_lastpage = 0;
			$p_page = 0;
			$p_recount = 0;
		}
		else {
			// ditemukan record
			$p_atfirst = $rs->AtFirstPage();
			$p_atlast = $rs->AtLastPage();
			$p_lastpage = $rs->LastPageNo();
			$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
			$p_recount = $rs->_maxRecordCount;
			$showlist = true;
		}
		
		$peng = $conn->GetArray("select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka 
					from ms_author p 
					left join pp_author t on t.idauthor = p.idauthor   ");
		$pengarang = array();
		foreach($peng as $p){
			$pengarang[$p['idpustaka']][] = $p['pengarang'];
		}
			
		//seleksi untuk eksemplar
		$sql = "select l.namalokasi, r.namarak, statuseksemplar, e.idpustaka, e.ideksemplar, e.noseri
				from pp_eksemplar e 
				left join lv_lokasi l on l.kdlokasi=e.kdlokasi 
				left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi 
				left join ms_rak r on r.kdrak=e.kdrak
				where e.idpustaka in ($p_insql) and e.kdkondisi<>'M' order by e.idpustaka";
		$rseks = $conn->Execute($sql);
		
		while ($roweks = $rseks->FetchRow()){
			$ideksemplar[$roweks['idpustaka']][] = $roweks['ideksemplar'];
			$eksrak[$roweks['idpustaka']][] = $roweks['namarak'];
			$noseri[$roweks['idpustaka']][] = $roweks['noseri'];
			$ekslokasi[$roweks['idpustaka']][] = $roweks['namalokasi'];
			$eksstatus[$roweks['idpustaka']][] = $roweks['statuseksemplar'];
		}
	}
	
	//list jenis pustaka
	$sqlj ="select kdjenispustaka,namajenispustaka from lv_jenispustaka order by kdjenispustaka";
	$rsj=$conn->Execute($sqlj);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<style>
		td { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; }
		span.highlighted {
		  background-color: #161616;
		  font-weight: bold;
		}
		span.term0 {
		  background-color: #161633;
		}
		span.term1 {
		  background-color: #331616;
		}
		span.term2 {
		  background-color: #163316;
		}

	</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<br/>
<div id="wrapper" style="width:750px;">
    <div style="width:100%;">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<table width="100%" border="0" cellpadding="4" cellspacing="0">
			<tr>
				<td width="100%" class="searchRight">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="20"> 
							<td align="left" colspan="4" width="40%">Halaman <?= $p_page ?> dari sekitar <?= $p_recount ?> hasil <?= $p_status ?> untuk keyword <?= UI::message($r_keyword,true)?></td>
						</tr>
					</table>
					<br/>
						<?php
							$i = 0;
							while ($row = $rs->FetchRow()){
								$i++;
						?>
						<table width="100%" class="result" border="0" cellpadding="0" cellspacing="0" onMouseOver="this.className='resultH'" onMouseOut="this.className='result'" style="border-top:2px solid #064082;padding:20px 10px;">
							<tr>
								<td rowspan="9" valign="top" width="105"><strong style="float:left"></strong>
								
								<? 
										$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
										$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';	
									?>
									<img id="imgfoto" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'none.png') ?>?<?= mt_rand(1000,9999) ?>" width="80" height="108"></td>
								<td class="resultTitle" colspan="4"><h3 style="margin:5px 0; text-transform:capitalize"><?= $i; ?>. &nbsp;<?= str_replace(strtolower($r_keyword),'<span style="background:yellow">'.$r_keyword.'</span>',strtolower($row['judul']));?>&nbsp;&nbsp;<a style="font-size:10px; " title="Tampilkan Pustaka" onClick="popup('index.php?page=show_pustaka&amp;id=<?= $row['idpustaka'] ?>',600,700);">[Detail]</a></h3></td>
							</tr>
							<tr>
								<td class="resultDesc" colspan="4"><p style="margin-bottom:20px">[tidak ada deskripsi]</p></td>
							</tr>
							<tr>
								<td class="resultSubB thLeft2" style="border:1px solid #ccc;" width="100" valign="top">Author</td>
								<td class="resultSub thLeft2" style="border:1px solid #ccc;" width="3%" valign="top">:</td>
								<td class="resultSub" style="border-top:1px solid #ccc;border-bottom:1px solid #ccc;border-right:1px solid #ccc;" align="left" valign="top">
									<? 
										/*if (!empty($row['authorfirst1'])) echo '- '.$row['authorfirst1'].' '.$row['authorlast1'].'<br>';
										if (!empty($row['authorfirst2'])) echo '- '.$row['authorfirst2'].' '.$row['authorlast2'].'<br>';
										if (!empty($row['authorfirst3'])) echo '- '.$row['authorfirst3'].' '.$row['authorlast3'].'<br>';*/
									?>
									
									<?php 
										$auth = array();
										if(!empty($pengarang[$row['idpustaka']])){
											foreach($pengarang[$row['idpustaka']] as $a){
												$auth[] = $a;
											}
											echo implode(", ",$auth);
										}
									?>
								</td>
							</tr>
							<tr>
								<td class="resultSubB thLeft2" width="100" valign="top" style="border:1px solid #ccc;">Tahun Terbit</td>
								<td class="resultSub thLeft2" width="3%" valign="top">:</td>
								<td style="border-bottom:1px solid #ccc;border-right:1px solid #ccc;" class="resultSub" align="left" valign="top"><?= $row['tahunterbit']?></td>
							</tr>
							<tr>
								<td class="resultSubB" width="110" colspan="4">
								<br/><h4>Eksemplar</h4>
								<? 	if (count($eksrak[$row['idpustaka']]) > 0){ ?>
								<table class="GridStyle" width="100%" border="1" cellpadding="4" cellspacing="0">
									<tr>
										<td width="120" align="left" class="eksHead thLeft2">No. Induk</td>
										<td width="120" align="left" class="eksHead thLeft2">Rak Pustaka</td>
										<td width="300" align="left" class="eksHead thLeft2">Lokasi</td>
										<td width="120" align="center" class="eksHead thLeft2">Status</td>
									</tr>			
									<? for($j=0;$j<count($eksrak[$row['idpustaka']]);$j++) { ?>
									<tr>
										<td align="left"><?= $noseri[$row['idpustaka']][$j];?></td>
										<td align="left"><?= $eksrak[$row['idpustaka']][$j];?></td>
										<td><?= $ekslokasi[$row['idpustaka']][$j];?></td>
										<? $eksstatus[$row['idpustaka']][$j]=='ADA' ? $class = 'resultAvailable' : $class = 'resultUnavailable' ?>
										<td align="center" class="<?= $class; ?>"><strong><?= $eksstatus[$row['idpustaka']][$j]=='ADA' ? 'ADA' : ($eksstatus[$row['idpustaka']][$j]=='PJM' ? 'DIPINJAM' : 'DIPROSES');?></strong></td>
									</tr>
									<? } ?>
								</table>
								<? } ?>
								</td>
							</tr>
						</table>
						<? } ?>
				</td>
			</tr>
			<tr>
				<td align="center">Halaman&nbsp;:&nbsp;
						<?	if($p_page == 0)
								echo '&nbsp;';
							else {
								$start = (($p_page > 9) ? ($p_page-9) : 1);
							$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
								$link = array();
								
								for($i=$start;$i<=$end;$i++) {
									if($i == $p_page)
										$link[] = '<strong>'.$i.'|</strong>';
									else
										$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a>|</strong>';
								}
								echo implode(' ',$link);
							}
						?>
				</td>
			</tr>
		</table>

			<input type="text" name="pelengkap3" style="visibility:hidden">
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		</form>
		</div>
	</div>
</div>

</body>

<script type="text/javascript">
	
	function goPage(npage) {
		sent = "&tipecari=<?= $r_tipe?>&kdbahasa=<?= $r_bahasa ?>&tahun1=<?= $r_tahun1?>&tahun2<?= $r_tahun2 ?>&keyword=<?= $r_keyword; ?>&page=" + npage;
		$("#isi").divpost({page: xlist, sent: sent});
	}
	
	function showEksemplar(id){
		window.open("<?= Helper::navAddress('rep_rwtbidang1a.php') ?>"+"&format=pdf&key=<?= $r_nip?>&str=<?= $filterstr; ?>","_blank");
	}
	
	function popup(url,width,height) 
	{
		var left   = (screen.width  - width)/2;
		var top    = (screen.height - height)/2;
		var params = 'width='+width+', height='+height;
		params += ', top='+top+', left='+left;
		params += ', directories=no';
		params += ', location=no';
		params += ', menubar=no';
		params += ', resizable=no';
		params += ', scrollbars=yes';
		params += ', status=no';
		params += ', toolbar=no';
		newwin=window.open(url,'windowname5', params);
		if (window.focus) {newwin.focus()}
		return false;
	}
	
</script>
</html>