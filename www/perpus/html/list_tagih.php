<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
 
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	require_once('classes/mail.class.php');
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Surat Tagih Anggota';
	$p_title = 'Surat Tagih Anggota';
	$p_tbheader = '.: Surat Tagih Anggota :.';
	$p_col = 7;
	$p_tbwidth = 860;
	$p_filelist= Helper::navAddress('surattagih.php');
	$p_fileedit = Helper::navAddress('ms_tagih.php');
	$p_id = "list_tagih";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'tgltagihan';
	$p_row = 15;
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_id = Helper::removeSpecial($_REQUEST['id']);
	$r_nama = Helper::removeSpecial($_REQUEST['nama']);
	// sql untuk mendapatkan isi list
	$p_sqlstr="select * from pp_tagihandetail where idtagihan='$r_key'";

	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
		$r_aksi=Helper::removeSpecial($_POST['act']);
		
		if($r_aksi=='kirim'){
			Mail::srtTagih($conn,$r_key);
			
		}else if ($r_aksi == 'hapus'){
			Sipus::DeleteBiasa($conn,pp_tagihan,idtagihan,$r_key);
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//Helper::redirect(); 
			}
		}			
	}else{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
<div align="center">
<table width="100" border=0>
	<tr>
	<? //if($c_readlist) { ?>
	<td>
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Tagihan </span></a>
	</td>
	<?// } ?>
</table>
<? include_once('_notifikasi.php') ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="index.php?page=list_tagih">
	<table width="<?= $p_tbwidth ?>">
	<tr>
		<td colspan=2>
		<table width="<?= $p_tbwidth ?>" border="0" cellspacing="4" cellpadding="0">
			<tr height=25>
				<td align="left" width="130"><b>Id Anggota</b></td>
				<td align="left" width="40%"><b>: <?= $r_id ?></b></td>
				
			</tr>
			<tr height=25>
				<td> <b>Nama Anggota</b></td>
				<td><b>: <?= $r_nama; ?></b></td>
				<td align="right"><? include_once('_notifikasi_trans.php'); ?></td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
        <header style="width:<?= $p_tbwidth ?>">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
          </div>
        </header>
	<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr height="20"> 
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">NO INDUK</td>		
		<td width="250" nowrap align="center" class="SubHeaderBGAlt">Judul</td>
		<td width="150" nowrap align="center" class="SubHeaderBGAlt">Pengarang1</td>
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">No Panggil</td>
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">Tgl Pinjam</td>
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">Tgl Tenggat</td>
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">Tgl Kembali</td>
		<?php
		$i = 0;
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idtagihan'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="20" valign="middle"> 
		<td align="center"><?= $row['noseri']; ?></u></td>	
		<td align="left"><?= $row['judul']; ?></td>
		<td align="center"><?= $row['authorfirst1']." ".$row['authorlast1']; ?></td>
		<td align="left"><?= $row['nopanggil']; ?></td>
		<td align="left"><?= Helper::tglEng($row['tgltransaksi']); ?></td>
		<td align="left"><?= Helper::tglEng($row['tgltenggat']); ?></td>
		<td align="left"><?= Helper::tglEng($row['tglkembali']); ?></td>
	</tr>
	<?php
		}
		
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Tidak terdapat Surat Tagih yang aktif</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="FootBG" align="right" colspan="<?= $p_col; ?>"> 
		
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
		</td>
		
	</tr>
	</table>
	        <?php require_once('inc_listnav.php'); ?>

	<br>
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="edit" id="edit">
<input type="hidden" name="id" id="id" value="<?= $r_id ?>">
<input type="hidden" name="nama" id="nama" value="<?= $r_nama ?>">
<input type="hidden" name="act" id="act">
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
</form>
</div>
    </div></div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

$(document).ready(function(){
	initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);
});

function goKirim(key){
	document.getElementById("act").value='kirim';
	document.getElementById("key").value = key;
	document.getElementById("perpusform").submit();
}


function goEdit(file,key,edit) {
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value = key;
	document.getElementById("edit").value = edit;
	//document.getElementById("act").value='simpan';
	goSubmit();
}
</script>
</html>