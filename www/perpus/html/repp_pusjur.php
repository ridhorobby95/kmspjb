<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	// $conn->debug=true;
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_kdjurusan = Helper::removeSpecial($_POST['kdjurusan']);
	$r_kdjenispustaka = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_tahun = Helper::removeSpecial($_POST['tahun']);
	$r_tahun2 = Helper::removeSpecial($_POST['tahun2']);
	$r_fil = Helper::removeSpecial($_POST['fil']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pustaka Per Jurusan';
	$p_width = '1200';
	
	$p_namafile = 'lap_pustaka_per_jurusan_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select b.kdjurusan, j.namajurusan, p.noseri, p.idpustaka, p.judul, s.namajenispustaka, p.tahunterbit, count(e.ideksemplar) jml_eksemplar 
		from ms_pustaka p 
		left join pp_bidangjur b on p.idpustaka = b.idpustaka 
		left join lv_jurusan j on j.kdjurusan = b.kdjurusan
		join lv_jenispustaka s on s.kdjenispustaka = p.kdjenispustaka 
		left join pp_eksemplar e on e.idpustaka = p.idpustaka  
		where 1=1 ";
		
	if($r_tahun and $r_fil=="between")
		$sql .=" and p.tahunterbit $r_fil '$r_tahun' and '$r_tahun2' ";
	else
		$sql .=" and p.tahunterbit $r_fil '$r_tahun' ";
	
	if($r_kdjurusan!='')
		$sql .=" and j.kdjurusan = '$r_kdjurusan'";
		
	if($r_kdjenispustaka!='')
		$sql .=" and p.kdjenispustaka = '$r_kdjenispustaka'";
		
	$sql .="group by b.kdjurusan, j.namajurusan, p.noseri, p.idpustaka, p.judul, s.namajenispustaka, p.tahunterbit
		order by j.namajurusan, s.namajenispustaka,  p.tahunterbit, p.judul  ";

	$rs = $conn->GetArray($sql);
	foreach($rs as $row){
		if($idu == $row['namajurusan']){
			$colspan_u[$row['namajurusan']] = $colspan_u[$row['namajurusan']] + 1;
		}else{
			$idu = $row['namajurusan'];
			$colspan_u[$row['namajurusan']] = 1;
		}
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" >

<div align="center">
<table width="<?=$p_width;?>">
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom" colspan=10><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width="<?=$p_width;?>" cellpadding="2" cellspacing="0" border=0>
	<tr>
		<td align="center" colspan=11>
		<strong>
			<h2>LAPORAN PUSTAKA PER JURUSAN</h2>
		</strong>
		</td>
	</tr>
	<tr>
		<td align="right" colspan=11>Tanggal Cetak : <?= Helper::tglEng(date('Y-m-d')) ?></td>
	</tr>
  
</table>
<table width="<?=$p_width;?>" border="1" cellpadding="2" cellspacing="0">

	<tr height=25>
	      <th width="15%" align="center"><strong>Unit</strong></th>
	      <th width="3%" align="center"><strong>No.</strong></th>
	      <th width="7%" align="center"><strong>No. Induk</strong></th>
	      <th width="20%" align="center"><strong>Judul</strong></th>
	      <th width="10%" align="center"><strong>Tahun</strong></th>
	      <th width="10%" align="center"><strong>Jenis Pustaka</strong></th>
	      <th width="7%" align="center"><strong>Jumlah Eksemplar</strong></th>
	</tr>
       <?php
	$n=0;
	//while($row = $rs->FetchRow()){$n++;
	foreach($rs as $row){$n++;
		if($idu == $row['namajurusan']){
			$cols_u = $cols_u + 1;
		}else{
			$idu = $row['namajurusan'];
			$cols_u = 1;
		}
	?>
	<tr height=25 valign="top">
		<? if($cols_u == 1){ ?>
			<td rowspan="<?=$colspan_u[$row['namajurusan']];?>" align="left"><strong><?= ($row['kdjurusan']?$row['kdjurusan']:"Tidak ada")."<br/>".($row['namajurusan']?$row['namajurusan']:"Jurusan") ?></strong></td>
		<? } ?>
		<td align="center"><?= $n; ?></td>
		<td align="left"><?= $row['noseri']; ?></td>
		<td align="left"><?= $row['judul']; ?></td>
		<td align="left"><?= $row['tahunterbit']; ?></td>
		<td align="left"><?= $row['namajenispustaka']; ?></td>
		<td align="left"><?= $row['jml_eksemplar'] ?></td>
	</tr>
	<?  } ?>
	<? if($n==0) { ?>
	<tr height=25>
		<td align="center" colspan="11" >Tidak Ada Data</td>
	</tr>
	<? } ?>
</table>
<br/><br/><br/>

</div>
</body>
</html>