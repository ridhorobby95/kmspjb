<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	//$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_noref = Helper::removeSpecial($_GET['noref']);
	
	if($r_noref=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Daftar Transaksi Maintenance';
	
	
	$sql = "select e.noseri,p.judul,h.no_referensi,e.ideksemplar,h.idhistorymaintaining,h.tglhistory,h.kdkondisi,h.kdlokasi,h.kdkondisi_lama,
			h.kdlokasi_lama,h.keterangan,lk.namalokasi,lko.namakondisi,h.npkkirim from pp_eksemplar e join ms_pustaka p on e.idpustaka = p.idpustaka 
			left join pp_historymaintaining h on e.ideksemplar=h.ideksemplar left join lv_lokasi lk on lk.kdlokasi=h.kdlokasi 
			left join lv_kondisi lko on lko.kdkondisi=h.kdkondisi where h.no_referensi='$r_noref' 
			order by h.idhistorymaintaining,h.kdkondisi";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Daftar Transaksi Maintenance</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150>Nomor Referensi</td>
	<td>: <?= $r_noref ?>
  </tr>    
  <tr>
	<td width=150>Tanggal Transaksi</td>
	<td>: <?= Helper::formatDateInd($row->fields['tglhistory']) ?>
  </tr>
  <tr>
	<td width=150>Dikirim Ke</td>
	<td>: <?= $row->fields['namalokasi'] ?>
  </tr>
  <tr>
	<td width=150>Kondisi</td>
	<td>: <?= $row->fields['namakondisi'] ?>
  </tr>

</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>No. Induk</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= $rs['noseri'] ?></td>
	<td ><?= $rs['judul'] ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada transaksi maintenance</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=6><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>
<br>
	<table>
	<tr>
		<td width="400">&nbsp;</td>
		<td>
		Surabaya, <?= Helper::formatDateInd($row->fields['tglhistory']) ?><br>
		Pengirim,<br><br><br><br>
		<?= $row->fields['npkkirim']; ?>
		</td>
	</tr>
	</table>

</div>
</body>
</html>