<? 
	define ('__VALID_ENTRANCE', 1);
	
	require_once('../includes/init.php');
	
	// make connection to gate
	$conng = Factory::getConnGate();
	$rpcsalt = "f48nei5a2vin6lvoe3r6tane87";
	
	// function to get shared session from gate
	function get_session($psess_id) {
		global $conng;
		$sess_id = Helper::cAlphaNum($psess_id);
		
		// mendapatkan data session gate
		$query = "select f_sessdata('$sess_id')";
		$sess_raw = $conng->GetOne($query);
	
		if(empty($sess_raw))
			return false;
		else
			$conng->Execute("begin f_sessclose('$sess_id'); end;");
		
		// menerjemahkan SESSDATA
		$sess_data = Helper::getSessionData($sess_raw);
		
		$role = Helper::cAlphaNum($sess_data['idrole']);
		$modul = Helper::cAlphaNum($sess_data['idmodul']);
		
		// mendapatkan data otorisasi item
		/*$query = "select * from ft_roletarget('$modul','$role')";
		$rs = $conng->Execute($query);
		
		$record = array();
		while($row = $rs->FetchRow()) {
			foreach($row as $k => $v) {
				$k = strtolower($k);
				if(substr($k,0,3) == 'can')
					$record[$row['namafile']][$k] = ($v == 1 ? true : false); 
			}
		}
		
		$sess_data['roletarget'] = $record;*/
		
		// mendapatkan menu
		$query = "select * from ft_menu('$modul','$role')";
		$a_menu = $conng->GetArray($query);
		
		// memfilter menu yang tidak perlu ditampilkan, kurang efisien tapi
		$a_menufix = array();
		$n = count($a_menu);
		
		for($i=0;$i<$n;$i++) {
			$inserted = true;
			
			if($a_menu[$i]['namafile'] == '') {
				$inserted = false;
				for($j=($i+1);$j<$n;$j++) {
					if($a_menu[$j]['level'] > $a_menu[$i]['level'] and $a_menu[$j]['namafile'] != '') {
						$inserted = true;
						break;
					}
					else if($a_menu[$j]['level'] <= $a_menu[$i]['level'])
						break;
				}
			}
			
			if($inserted)
				$a_menufix[] = $a_menu[$i];
		}
		
		$sess_data['menu'] = $a_menufix;
		
		
		return $sess_data;
	}

	// setting gate & login url
	$_SESSION['menupage'] = Config::gateUrl;
	$_SESSION['gate'] = get_session(Helper::decrypt(trim($_GET['s']), $rpcsalt));
//var_dump($_SESSION);die();	
	if(empty($_SESSION['gate'])) 
	{
		Helper::navigateOut();
	}
	else 
	{
		Helper::navigate('sys_login');
	}
?>