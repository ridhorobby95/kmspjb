<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and e.kdkolasi in (select l.kdlokasi from lv_lokasi where l.idunit in ($units)) ";
		$sqlAdminUnit_ = " and b.kdkolasi in (select l.kdlokasi from lv_lokasi where l.idunit in ($units)) ";
	}
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_thn = Helper::removeSpecial($_POST['tahun']);
	$r_unit = Helper::removeSpecial($_POST['kdjurusan']);
	$r_bln = Helper::removeSpecial($_POST['bulan']);

	if($r_format=='' or $r_thn==''){
		header("location: index.php?page=home");
	}

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Sirkulasi Per Bulan Perpustakaan PJB <br>'.$r_bln.$r_thn ;
	
	$p_namafile = 'lap_sirkualsi_per_bulan_'.$r_bln.$r_thn;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select * 
			from 
			(
				/*peminjaman*/
				select count(p.idpustaka) jml, a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul) judul, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd')) tgl, 'pinjam' jenis 
				from pp_transaksi t 
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tgltransaksi,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka p on p.idpustaka = e.idpustaka 
				where t.statustransaksi = 1 and t.perpanjangke is null $sqlAdminUnit
				and coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd')) = '$r_bln' 
				and coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd')) = '$r_thn' 
				group by a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul), coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tgltransaksi,'YYYY-mm-dd'))
				
				union
				
				/*pengembalian*/
				select count(p.idpustaka) jml, a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul) judul, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd')) tgl, 'kembali' jenis 
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglpengembalian,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka p on p.idpustaka = e.idpustaka 
				where t.statustransaksi = 0 and t.perpanjangke is null $sqlAdminUnit
				and coalesce(to_char(b.tanggal,'mm'),to_char(t.tglpengembalian,'mm')) = '$r_bln'
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglpengembalian,'YYYY')) = '$r_thn'
				group by a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul), coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglpengembalian,'YYYY-mm-dd'))
				
				union
				
				/*perpanjangan*/
				select count(p.idpustaka) jml, a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul) judul, coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd')) tgl, 'panjang' jenis 
				from pp_transaksi t
				left join pp_bukutamu b on b.idanggota = t.idanggota and to_char(b.tanggal,'YYYY-mm-dd') = to_char(t.tglperpanjang,'YYYY-mm-dd')
				join ms_anggota a on a.idanggota = t.idanggota 
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
				join ms_pustaka p on p.idpustaka = e.idpustaka  
				where t.statustransaksi = 1 and t.perpanjangke is not null $sqlAdminUnit
				and coalesce(to_char(b.tanggal,'mm'),to_char(t.tglperpanjang,'mm')) = '$r_bln' 
				and coalesce(to_char(b.tanggal,'YYYY'),to_char(t.tglperpanjang,'YYYY')) = '$r_thn' 
				group by a.idunit, a.idanggota, a.namaanggota, p.noseri, to_char(p.judul), coalesce(to_char(b.tanggal,'YYYY-mm-dd'),to_char(t.tglperpanjang,'YYYY-mm-dd'))
				
				union 
				
				/*berkunjung*/
				select 0 jml, a.idunit, a.idanggota, a.namaanggota, '' noseri, '' judul, to_char(b.tanggal,'YYYY-mm-dd') tgl, 'kunjung' jenis 
				from pp_bukutamu b  
				left join pp_transaksi t on t.idanggota = b.idanggota 
				join ms_anggota a on a.idanggota = b.idanggota 
				where t.idtransaksi is null $sqlAdminUnit_
				and to_char(b.tanggal,'mm') = '$r_bln' 
				and to_char(b.tanggal,'YYYY') = '$r_thn' 	
				group by a.idunit, a.idanggota, a.namaanggota, to_char(b.tanggal,'YYYY-mm-dd')

			) 
			where 1=1  ";
	
	if(!empty($r_unit)){
		$sql .= " and idunit = '$r_unit' ";
	}

	$sql .= " order by tgl ";

	$rs = $conn->Execute($sql);
	while($row=$rs->FetchRow()){
		//$data[$row['tgl']] = (int) $data[$row['tgl']] + 1;
		$data['data'][$row['tgl']][] = $row;
	}

	$satker = "";
	if(!empty($r_unit)){
		$satker .= $conn->GetOne("select namasatker from ms_satker where kdsatker = '$r_unit' ");
		$judul = "Laporan Data Pengunjung <br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn."<br/>".$satker;
	}else{
		$judul = "Laporan Data Pengunjung Perpustakaan<br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;
	}
	
	$sql_l = "select to_char(tgllibur,'YYYY-mm-dd') tgllibur from ms_libur where to_char(tgllibur,'mm') = '$r_bln' ";
	$liburs = $conn->GetArray($sql_l);
	if(count($liburs)>0){
		$holy = array();
		foreach($liburs as $libur){
			$holy[$libur['tgllibur']] = true;
		}
	}
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=975>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<br><br>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2><?= $judul; ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="1000" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="left"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>

	<? 
	$at = date('t', strtotime($r_thn.'-'.$r_bln.'-1'));	
	?>
	<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
		<tr height="25">
			<th width="5">NO</th>
			<th width="20">TGL BERKUNJUNG</th>
			<th width="10">TGL PINJAM</th>
			<th width="10">TGL KEMBALI</th>
			<th width="20">NAMA</th>
			<th width="10">NID</th>
			<th width="20">KEGIATAN</th>
			<th width="5">ITEM</th>
			<th width="30">BUKU YANG DIPINJAM</th>
		</tr>
	<? 
	$k = 1;
	for($t=1; $t<=$at; $t++) {
		$date = $r_thn.'-'.$r_bln.'-'.Helper::plusNol($t);
		$h=date("w",strtotime($date));
		if($h==0 or $h==6){
			$bdc = '#F5A8A8';
		}elseif($holy[$date]){
			$bdc = '#9DE6A5';
		}else{
			$bdc = '#FFFFFF';
		}

		$cols = count($data['data'][$date]);
		
		if($cols > 0){
			if($cols > 1)
				$rowspan = ' rowspan="'.$cols.'"';
			else
				$rowspan = '';
			
			$tgl = "";
			foreach($data['data'][$date] as $d_trans){
				if($d_trans['jenis'] == "pinjam"){
					$tglpinjam = $d_trans['tgl'];
					$tglkembali = "";
					$jenis = "Meminjam";
				}elseif($d_trans['jenis'] == "kembali"){
					$tglpinjam = "";
					$tglkembali = $d_trans['tgl'];
					$jenis = "Mengembalikan";
				}elseif($d_trans['jenis'] == "kunjung"){
					$tglpinjam = "";
					$tglkembali = "";
					$jenis = "Berkunjung";
				}
				
?>
		<tr height="25" style="background-color: <?=$bdc;?>">
			<td align="center"><?=$k;?></td>
			<? if($tgl <> $d_trans['tgl']){?>
			<td align="center"<?=$rowspan;?>><?=$t." ".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;?></td>
			<? } ?>
			<td><?=$tglpinjam;?></td>
			<td><?=$tglkembali;?></td>
			<td><?=$d_trans['namaanggota'];?></td>
			<td><?=$d_trans['idanggota'];?></td>
			<td align="center"><?=$jenis;?></td>
			<td align="center"><?=$d_trans['jml'];?></td>
			<td><?=$d_trans['judul'];?></td>
		</tr>
<? 
					if($cols > 1)
						$k++;

					$tgl = $d_trans['tgl'];
			}
				if($cols > 1)
						$k--;
		}else{
?>
		<tr height="25" style="background-color: <?=$bdc;?>">
			<td align="center"><?=$k;?></td>
			<td align="center"><?=$t." ".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;?></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
<?		
		}
		
		$k++;
	} 
?>
	</table><br/>
		<table width="1000" cellspacing="0" cellpadding="4"><tr><td align="left">
		<table width="200" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
			<tr>
				<th colspan="2">KETERANGAN</th>
			</tr>
			<tr>
				<td style="background-color: #F5A8A8" width="30%"></td>
				<td>Sabtu/Minggu</td>
			</tr>
				<?
					$liburan = Sipus::getLibur($conn,$r_bln,$r_thn);
					if(count($liburan)>0){
						foreach($liburan as $libur){
				?>
			<tr>
				<td valign="top" align="center" style="background-color: #9DE6A5" width="30%"><?=$libur['tgl'];?></td>
				<td><?=$libur['namaliburan'];?></td>
			</tr>
				<?
						}
					}
				?>
		</table>
		</td>
		</tr></table>
	<br/><br/>

<br><br>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<?/*table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table*/?>
</div>
</body>
</html>