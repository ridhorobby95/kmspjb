<?php
	//$conn->debug=false;
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$kepala = Sipus::getHeadPerpus();
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$p_filelist=Helper::navAddress('dbebas_sumbangan.php');
	$r_id=Helper::removeSpecial($_GET['id']);
	
	if($r_id =='') {
		header('Location: '.$p_filelist);
		exit();
	}
	
	$sumbangan=$conn->GetRow("select ms.namasatker,ps.*, to_char(ps.tglsumbangan,'yyyy-mm-dd') as tglsumbangan1 from pp_sumbangan ps left join ms_satker ms on ms.kdsatker=ps.idunit where idsumbangan='$r_id'");

	$buku=$conn->Execute("select o.*,t.nottb from pp_orderpustaka o
						  join pp_orderpustakattb tb on o.idorderpustaka=tb.idorderpustaka
						  join pp_ttb t on tb.idttb=t.idttb
						  where o.idsumbangan=".$sumbangan['idsumbangan']."");
?>

<html>
<head>
<title>Surat Bebas Pinjam</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="printpage()">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><img src="images/perpustakaan/kop.png" width="800" height="95" style="border-bottom:1px;"></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="800" border="0">
	<tr>
		<td style="border-top:double #000000"  colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" colspan="2"><b><u><font size="3" face="Bodoni MT">K E T E R A N G A N</font></u></b></td>
	</tr>
	<tr>
		<td align="center" colspan="2">No : <?= $buku->fields['nottb'] ?><br><br><br></td>
	</tr>
	<tr>
		<td>Perpustakaan mengucapkan terima kasih kepada :
	</tr>
	<tr>
		<td height="70">
		<table>
		<tr><td width="25">&nbsp;</td>
			<td width="120">NO IDENTITAS</td><td>: </td><td><b><?= $sumbangan['idanggota']!='' ? $sumbangan['idanggota'] : '-' ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="120">NAMA</td><td>: </td><td><b><?= $sumbangan['namapenyumbang'] ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="120">UNIT</td><td>: </td><td><b><?= $sumbangan['idunit']!='' ? $sumbangan['idunit'].' - '.$sumbangan['namasatker'] : '-' ?></b></td>
		</tr>
		<tr><td width="25">&nbsp;</td>
			<td width="120" valign="top">KETERANGAN</td><td valign="top">: </td><td><div><b><?= $sumbangan['keterangan']!='' ? str_replace("\n","<br/>",$sumbangan['keterangan']) : '-' ?></b></div></td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>Atas sumbangsih yang diberikan untuk menambah koleksi perpustakaan dengan keterangan sebagai berikut :
		</td>
	</tr>
	<tr>
		<td align="center"><br>
		<table border=0 width="510" cellpadding=0 cellspacing=0 style="border:1pt solid black;border-collapse:collapse">
			<tr>
				<td width="50" align="center" style="border-right:1pt solid black;border-bottom:1pt solid black;"><b>NO.</b></td>
				<td width="200" align="center" style="border-right:1pt solid black;border-bottom:1pt solid black;"><b>JUDUL BUKU</b></td>
				<td width="60" align="center" style="border-right:1pt solid black;border-bottom:1pt solid black;"><b>JUMLAH EKSEMPLAR </b></td>
			<tr>
			<? $i=0;
			while($rs=$buku->FetchRow()) { $i++;?>
			<tr height="20">
				<td align="center" style="border-right:1pt solid black;border-bottom:1pt solid black;"><?= $i ?></td>
				<td style="border-right:1pt solid black;border-bottom:1pt solid black;"><?= $rs['judul'] ?></td>
				<td align="center" style="border-right:1pt solid black;border-bottom:1pt solid black;"><?= $rs['qtysumbangan'] ?></td>
			</tr>
			<? } ?>
			<? if($i==0) { ?>
			<tr height="20">
				<td colspan=3 align="center">Sumbangan masih kosong</td>
			</tr>
			<? } ?>
		</table>
		</td>
	</tr>
	</table>
	</td>
	</tr>
</table><br><br><br>
<table>
	<tr>
		<td width="600"></td>
		<td><br>
		Surabaya, <?= Helper::formatDateInd($sumbangan['tglsumbangan1']) ?><br>Kabag Perpustakaan,<br><br><br><br><?=$kepala['namalengkap'];?>
		<br>NIP. <?=$kepala['nik'];?>
		<td>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>