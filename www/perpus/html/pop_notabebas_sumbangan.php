<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	$conn->debug=false;
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$kepala = Sipus::getHeadPerpus();

	$p_filelist=Helper::navAddress('dbebas_sumbangan.php');
	
	$r_id=Helper::removeSpecial($_GET['id']);
	$r_idp=Helper::removeSpecial($_GET['idp']);
	
	if($r_id =='' or $r_idp=='') {
		header('Location: '.$p_filelist);
		exit();
	}
	
	//$anggota=$conn->GetRow("select idanggota, idunit, namaanggota from ms_anggota where idanggota='$r_id'");
	$anggota=$conn->GetRow("select u.*, u.nama as namaanggota, n.nama from um.users u 
				join um.unit n on u.kodeunit = n.kodeunit where nid = '$r_id'");
	$buku=$conn->Execute("select * from pp_pelayanandetil where idpelayanan=$r_idp");
?>

<html>
<head>
<title>Surat Bebas Pinjam</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onLoad="printpage()">
<table cellpadding="8" cellspacing="0" border="0" width="870">
<tr><td>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td><img src="images/logosim.jpg" height="60"></td>
            <!-- <td valign="bottom"><h3 style="margin;0">PERPUSTAKAAN<br>PJB</h3></td> -->
		</tr>
		<tr height="10"><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2" align="center"><font size="4"><b>SURAT KETERANGAN BEBAS SUMBANGAN PUSTAKA</b></font><br>
			No. Surat : <?= Helper::removeSpecial($_GET['nomor']) ?>
			</td>
		</tr>
		<tr height="10"><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="padding:1em 1em;" colspan="2" align="left">Yang bertanda tangan dibawah ini menerangkan bahwa:<br><br>
				Nama <?php echo str_repeat("&nbsp;",15); ?>: <?= $anggota['namaanggota'] ?><br>
				No. Anggota <?php echo str_repeat("&nbsp;",5);?>: <?= $anggota['nid'] ?><br>
				Unit <?php echo str_repeat("&nbsp;",18);?>: <?= $anggota['nama'];?><br><br>
				Benar-benar tidak mempunyai tanggungan berupa pinjaman buku yang harus dikembalikan ke Perpustakaan PT Pembangkitan Jawa Bali, 
				dan telah menyumbang buku sebagai berikut:
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="2" align="center"><br>          
			<table border=1 width="800" cellpadding=0 cellspacing=0 style="border:0pt solid black;border-collapse:collapse">
				<tr height="25">
					<td width="50" align="center" ><b>No. </b></td>
					<td width="400" align="center" ><b>Judul Pustaka</b></td>
					<td width="60" align="center" ><b>Jumlah</b></td>
				</tr>
				<? $i=0;
				while($rs=$buku->FetchRow()) { $i++;?>
				<tr height="30">
					<td align="center">&nbsp;<?= $i ?> </td>
					<td style="padding:1em 1em;" width="400" ><?= $rs['nama'] ?></td>
					<td align="center" ><?= $rs['jumlah'] ?></td>
				</tr>
				<? } ?>
				<? if($i==0) { ?>
				<tr>
					<td colspan="3" align="center">Sumbangan masih kosong</td>
				</tr>
				<? } ?>
			</table>
			</td>
		</tr>
		<tr height="10"><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td style="padding:.3em 10em;" colspan="2">
				<div style="float:left;margin-left:400px;">
					<span>Surabaya, <?php echo Helper::formatDateInd(date("Y-m-d"))?></span><br>
					<div style="background-size:80px 80px;background-repeat:no-repeat;">
					<span><b><?=$kepala['jabatan'];?> PJB,</b></span><br><br><br><br><br>
					<span><b><?=$kepala['namalengkap'];?></b></span><br>
					<span><b>NIP. <?=$kepala['nik'];?></b></span>
					</div>
				</div>
			</td>
		</tr>
	</table>
<br> <br> <br> 
</td></tr>
</table><br> <br> <br> 
</body>
<script type="text/javascript">
function printpage()
  {
  //window.print()
  }
</script>
</html>