<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	

	
	// definisi variabel halaman
	$p_dbtable = 'lv_jenispelayanan';
	$p_window = '[PJB LIBRARY] Daftar Jenis Pelayanan';
	$p_title = 'Daftar Jenis Pelayanan';
	$p_tbheader = '.: Daftar Jenis Pelayanan :.';
	$p_col = 3;
	$p_tbwidth = 400;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdpelayanan";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['kdpelayanan'] = Helper::cStrNull($_POST['i_kdpelayanan']);
			$record['pelayanan'] = Helper::cStrNull($_POST['i_pelayanan']);
			$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['t_updatetime']=date('d-m-Y H:i:s');
			$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
			$col = $conn->Execute("select * from $p_dbtable where 1=-1");
			$p_svsql = $conn->GetInsertSQL($col,$record);
			$conn->Execute($p_svsql);
			
			if($conn->ErrorNo() != 0)
				$errdb = 'Penyimpanan data gagal.';
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['kdpelayanan'] = Helper::cStrNull($_POST['u_kdpelayanan']);
			$record['namapelayanan'] = Helper::cStrNull($_POST['u_pelayanan']);
			$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['t_updatetime']=date('d-m-Y H:i:s');
			$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);			
			$col = $conn->Execute("select * from $p_dbtable where kdpelayanan = '$r_key'");
			$p_svsql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($p_svsql);
			
			if($conn->ErrorNo() != 0)
				$errdb = 'Penyimpanan data gagal.';
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where kdpelayanan = '$r_key'";
			$conn->Execute($p_delsql);
			
			if($conn->ErrorNo() != 0)
				$errdb = 'Penghapusan data gagal.';
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth; ?>">
	<tr height="40">
		<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
	</tr>
</table>
<? if($errdb != '') echo UI::message($errdb,true).'<br><br>'; ?>
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr height="20"> 
		<td class="HeaderBG" colspan="<?= $p_col ?>" align="center">
		<?= $p_tbheader ?></td>
	</tr>
	<tr height="20"> 
		<td width="90" nowrap align="center" class="SubHeaderBGAlt">Id Jenis</td>
		<td nowrap align="center" class="SubHeaderBGAlt">Nama Jenis Member</td>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
	</tr>
	<?php
		$i = 0;
		while ($row = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($row['kdpelayanan'],$p_editkey)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>">
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Sunting Data" onclick="goEditIP('<?= $row['kdpelayanan']; ?>');" class="link"><?= $row['kdpelayanan']; ?></u>
		<? } else { ?>
			<?= $row['kdpelayanan']; ?>
		<? } ?>
		</td>
		<td><?= $row['namapelayanan']; ?></td>
		<td align="center">
		<? if($c_delete) { ?>
			<u title="Hapus Data" onclick="goDeleteIP('<?= $row['kdpelayanan']; ?>','<?= $row['pelayanan']; ?>');" class="link">[hapus]</u>
		<? } ?>
		</td>
	</tr>
	<?php
			} else { // row diupdate
	?>
	<tr class="<?= $rowstyle ?>"> 
		<td align="center"><?= UI::createTextBox('u_kdpelayanan',$row['kdpelayanan'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('u_pelayanan',$row['pelayanan'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Update Data" onclick="updateData('<?= $row['kdpelayanan']; ?>');" class="link">[update]</u>
		<? } ?>
		</td>
	</tr>
	<?php 	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } if($c_add) { ?>
	<tr class="LiteSubHeaderBG"> 
		<td align="center"><?= UI::createTextBox('i_kdpelayanan','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
		<td align="center"><?= UI::createTextBox('i_pelayanan','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
		<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
	</tr>
	<?php }  ?>
</table><br>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="scroll" id="scroll">
</form>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_kdpelayanan').focus();
	<? } else { ?>
	document.getElementById('i_kdpelayanan').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_kdpelayanan,i_pelayanan"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_kdpelayanan,u_pelayanan")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>