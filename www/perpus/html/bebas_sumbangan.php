<?php
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanandetil';
	$p_window = '[PJB LIBRARY] Surat Bebas Pinjam dan Sumbangan';
	$p_title = 'SURAT PEMINJAMAN DAN SUMBANGAN';
	$p_tbheader = '.: SURAT PEMINJAMAN DAN SUMBANGAN:.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filelist =  Helper::navAddress('dbebas_sumbangan.php');
	
	$r_key = Helper::removeSpecial($_POST['key']);
	if(!empty($_POST)){
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_id = Helper::removeSpecial($_POST['txtid']);
		
		if($r_aksi=='proses'){
			
			$sqlangg=$conn->GetRow("select a.idanggota, a.namaanggota, a.idunit from ms_anggota a where a.idanggota='$r_id'");
			if(!$sqlangg){
				$errdb = 'Identitas Anggota tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
			} else{
				if($sqlangg['idunit'] != $idunit){
					echo "Anggota bukan berasal dari unit $unitlogin.";
					Helper::setFlashData('errdb', "Anggota bukan berasal dari unit $unitlogin.");
				}else{
					
					$sqlcheck=$conn->Execute("select * from v_trans_list where idanggota='$r_id' and statustransaksi='1' ");
					$jumlah=$sqlcheck->RowCount();
	
					$cek_denda = $conn->GetRow("select denda from ms_anggota where idanggota='$r_id'");
					if($jumlah>0) {
						$errdb = "<font color='red'>Anggota tersebut masih mempunyai tanggungan pinjaman pustaka<br></font>";	
						Helper::setFlashData('errdb', $errdb);
					}else if($cek_denda['denda'] > 0){
						$errdb = "<font color='red'>Anggota tersebut masih mempunyai hutang denda sebesar Rp.".$cek_denda['denda'].",-<br></font>
							<br><u title='Lunasi Hutang Denda' onclick='goFreeSkors(\"$r_id\")' style='cursor:pointer;color:#3300FF'><img src='images/freeskors.gif'>&nbsp;Bayarkan";
						Helper::setFlashData('errdb', $errdb);
					} else {
						header('Location: '.$p_filelist.'&id='.$r_id);
					}
				}
			}
		}
		else if ($r_aksi=='freeskors'){
			$recbebas=array();
			$recbayar=array();
			$r_key=Helper::removeSpecial($_POST['key']);
			$recbebas['denda']=null;
			$err=Sipus::UpdateBiasa($conn,$recbebas,ms_anggota,idanggota,$r_key);
			
			//cari transaksi yang mana untuk memberikan tgl bayar
			$sql_transaksi = $conn->Execute("select *
							from pp_denda d
							left join pp_transaksi t on d.idtransaksi=t.idtransaksi
							where d.tglbayar is null and (statustransaksi='0' or statustransaksi='2') and idanggota='$r_key'");
			$recbayar['tglbayar'] = date("Y-m-d");
			while($row_bayar = $sql_transaksi->FetchRow()){
				$err=Sipus::UpdateBiasa($conn,$recbayar,pp_denda,idtransaksi,$row_bayar['idtransaksi']);
			}
			
			if($err != 0){
				$errdb = 'Pembayaran denda gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembayaran denda berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist.'&id='.$r_id);
				exit();
			}
		}

	}
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>

	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<style>
        .ui-autocomplete{
            margin-top:35px !important;
            width: 280px;
            text-align:left;
        }
        #anggota_label{
            width: 280px;
            margin: 10px 0;
        }
    </style>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon"> Surat Bebas Pinjam dan Sumbangan Pustaka</div>
		<div align="center" style="position:relative;">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile ?>">

			<div id="ac-wrapper" style="position:relative; width: 280px;">
				<b>Nama /  ID Anggota :</b> 
				<input style="width:300px" type="text" name="txtid_label" id="txtid_label" value="<?= $_POST['txtid_label'] ?>" data-cari="anggota" data-inkey="txtid">
				<input type="hidden" name="txtid" id="txtid" value="<?= $_POST['txtid'] ?>">
			</div>
				<input type="button" name="btncek" id="btncek" class="buttonSmall" value="Periksa" onClick="goProses();" style="cursor:pointer;height:25;width:100">


		<div class="table-responsive">
            <table width="100%" border="0" cellpadding="6" cellspacing=0>
		<tr>
			<td width="100%" align="center">
			<br>
			<? include_once('_notifikasi.php') ?>
			<? if($jumlah>0) { ?>
				<table  width='700' border=0 align='center' cellpadding='4' cellspacing='0'>
					<tr>
						<td width="100"><b>Nama Anggota</b></td>
						<td><b>: &nbsp;<?= $sqlangg['namaanggota'] ?></b></td>
					</tr>
				</table>
			
				<table  width='700' border=0 align='center' cellpadding='4' cellspacing='0' class="GridStyle">
				<tr>
					<td width="100" class="SubHeaderBGAlt" align="center">NO INDUK</td>
					<td  class="SubHeaderBGAlt" align="center">Judul Pustaka </td>
					<td width="120" class="SubHeaderBGAlt" align="center">Tanggal Pinjam</td>
					<td width="120" class="SubHeaderBGAlt" align="center">Tanggal Kembali</td>
				</tr>

				<?php
					$i = 0;
						// mulai iterasi
						while ($rs = $sqlcheck->FetchRow()) 
						{				
							if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				?>
				<tr class="<?= $rowstyle ?>" height="25">
				<td align="center"><?= $rs['noseri'] ?></td>
				<td><?= $rs['judul'] ?></td>
				<td  align="center"><?= Helper::tglEngTime($rs['tgltransaksi']) ?> </td>
				<td  align="center"><?= Helper::tglEngTime($rs['tgltenggat']) ?> </td>
				</tr>
				<? } if ($i==0) {
				?>
				<tr height="20">
					<td align="center" colspan="4"><b>Tidak Ditemukan.</b></td>
				</tr>
				<?php } ?>
				</table>
			</td>
		</tr>
		<? } ?>
		</table>
            </div>
			<input type="text" name="zzz" style="visibility:hidden">
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="ida" id="ida" value="<?= $r_id ?>">
			<input type="hidden" name="key" id="key">
			
		</form>
		</div>
		</div>
		</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

	$(document).ready(function() {

		$("[data-cari]").not(".tt-hint").each(function() {
			var elemid  = $(this).attr('id');
			var inkey  = $(this).attr('data-inkey');
			var datacari  = $(this).attr('data-cari');

			$('#' + elemid).autocomplete({
				source: 'index.php?page=ajax&f=' + datacari,
				minLength: 2,
				select: function( event, i ) {
					$('#' + inkey).val(i.item.key);
				},
            position: { my : "right top", at: "right bottom" },
            appendTo: '#ac-wrapper'
			});
		});
    
	});

	function goProses(){
		document.getElementById("act").value="proses";
		goSubmit();
	}
	
	function etrR(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		goProses();
		}
	}

	function goFreeSkors(key) {
		var free = confirm("Apakah anda yakin akan membayarkan denda Id Anggota "+key+" ?");
		if(free) {
			document.getElementById("act").value = 'freeskors';
			document.getElementById("key").value = key;
			goSubmit();
		}
	}


</script>
</html>
	
