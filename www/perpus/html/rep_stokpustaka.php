<?php
	ob_start();
	
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	$c_edit = $a_auth['canedit'];	
	
	// variabel request
	$r_format = Helper::cAlphaNum($_REQUEST['format']);
	$r_tglawal = Helper::formatDate($_REQUEST['periode1']);
	$r_tglakhir = Helper::formatDate($_REQUEST['periode2']);
	$r_jenis = $_REQUEST['jenis'];
		
	if(empty($r_jenis))
		$sqlj = '';
	else if(count($r_jenis) == 1) {
		if(is_array($r_jenis)) $r_jenis = $r_jenis[0];
		$sqlj = "and m.kdjenisanggota = '".Helper::cAlphaNum($r_jenis)."' ";
	}
	else {
		for($i=0;$i<count($r_jenis);$i++)
			$r_jenis[$i] = Helper::cAlphaNum($r_jenis[$i]);
		$i_jenis = implode("','",$r_jenis);
		$sqlj = "and m.kdjenisanggota in ('$i_jenis') ";
	}
		
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Skrosing';
	$p_title = 'Daftar Laporan Skorsing';
	$p_dbtable = 'ms_anggota';
	$p_namafile = 'Daftar_Laporan_Skorsing';
	
	switch($r_format) {
		case "doc" :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case "xls" :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$p_sqlstr = "select * from $p_dbtable m 
				left join lv_jenisanggota j on j.kdjenisanggota=m.kdjenisanggota
				where tglselesaiskors between '$r_tglawal' and '$r_tglakhir' {$sqlj}";
	$rs = $conn->Execute($p_sqlstr);
	
	$p_col = 11;
	$p_tbwidth = 800;
	//print_r($_POST);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<style>
		td { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 12px;}
	</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width="<?= $p_tbwidth; ?>">
	<tr>
		<td align="center" colspan="<?= $p_col; ?>"><strong><font size="3"><?= $p_title; ?></font></strong></td>
	</tr>
	<tr>
		<td align="center" colspan="<?= $p_col; ?>"><strong>Per Tgl <?= Helper::formatDate($r_tglawal).' s/d '.Helper::formatDate($r_tglakhir);?></strong></td>
	</tr>
</table>
<br>
<table width="<?= $p_tbwidth; ?>" cellpadding="3" border="1" style="border-collapse:collapse;">
	<tr>
		<td align="center">ID ANGGOTA</td>
		<td align="center">NAMA</td>
		<td align="center">JENIS ANGGOTA</td>
		<td align="center">TGL. SKORSING</td>
		<td align="center">PETUGAS</td>
		<td align="center">ALASAN</td>
	</tr>
<?	$i = 0;
	while($row = $rs->FetchRow()) { $i++;?>
	<tr>
		<td><?= $row['idanggota']; ?></td>
		<td><?= $row['namaanggota']; ?></td>
		<td><?= $row['namajenisanggota']; ?></td>
		<td align="center"><?= Helper::formatDate($row['tglselesaiskors']); ?></td>
		<td></td>
		<td></td>
	</tr>
<?
	} 
	if($i == 0) { ?>
	<tr>
		<td colspan="<?= $p_col ?>" height="30" align="center">Data tidak ditemukan</td>
	</tr>
<?	} ?>
</table>
</div>
</body>
</html>

<? ob_end_flush(); ?>