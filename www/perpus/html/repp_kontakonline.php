<?php
	set_time_limit(0);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Kontak Online';
	
	$p_namafile = 'laporan_Kontak_Online'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$p_kontak = $conn->Execute("select * from lv_kontak");
	while($rowk = $p_kontak->FetchRow()){
		$ArId [] = $rowk['idkontak'];
		$ArNama [] = $rowk['namakontak'];
		$ArKet [] = $rowk['keterangan'];
	}
	
	$p_detail = $conn -> Execute("select * from pp_kontakonline order by idkontak,idkontakol,coalesce(idparentol,0)");
	while ($rowd = $p_detail -> FetchRow()){
		$ArJudul[$rowd['idkontak']][] = $rowd['judul'];
		$ArPar[$rowd['idkontak']][] = $rowd['idparentol'];
		$ArIdko[$rowd['idkontak']][] = $rowd['idkontak'];
		$ArK[$rowd['idkontak']][]  = $rowd['keterangankontak'];
		$ArIn[$rowd['idkontak']][] = $rowd['t_input'];
		$ArUser[$rowd['idkontak']][] = $rowd['t_user'];
		$ArEmail[$rowd['idkontak']][] = $rowd['email'];
		$ArTlp[$rowd['idkontak']][] = $rowd['telp'];
	}


?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Kontak Online</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">
	<? 
	for($i=0;$i<count($ArId);$i++)
	{
	?>
	<tr height=25>
		<th colspan=6 align="left"><b>Judul : <?= $ArNama[$i] ?></b></td>
	</tr>
  <tr height=25>
	<th width="5%" align="center"><strong>No.</strong></th>
	<th width="50%" align="center"><strong>Keterangan</strong></th>
	<th width="15%" align="center"><strong>Tanggal Input</strong></th>
	<th width="10%" align="center"><strong>User</strong></th>
	<th width="10%" align="center"><strong>Email</strong></th>
	<th width="10%" align="center"><strong>Telepon</strong></th>
  </tr>
  <?php
	$no = 0;
	for($j=0;$j<count($ArJudul[$ArId[$i]]);$j++)
	{  $no++; 
	if($ArPar[$ArId[$i]][$j]=='') {
	?>
	<tr>
		<td colspan="6">Posting : <?= $ArJudul[$ArId[$i]][$j] ?></td>
	</tr>
	<? } ?>
    <tr height=25>
	<td align="left"><?= $no ?></td>
	<td align="left"><?= $ArK[$ArId[$i]][$j] ?></td>
	<td ><?= Helper::tglEngStamp($ArIn[$ArId[$i]][$j]) ?></td>
	<td align="left"><?= $ArUser[$ArId[$i]][$j] ?></td>
	<td align="left"><?= $ArEmail[$ArId[$i]][$j] ?></td>
	<td align="left"><?= $ArTlp[$ArId[$i]][$j] ?></td>	
  </tr>
	<?  } ?>
	<? if($no==0){ ?>
	<tr height=25>
	<td colspan="6" align="center">Data Kontak Online Kosong</td>
	</tr>
	<? } ?>

	<? } ?>
</table>


</div>
</body>
</html>
</html>