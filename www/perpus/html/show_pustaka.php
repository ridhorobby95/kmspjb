<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$p_ekslist = Helper::navAddress('ms_eksemplar.php');
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	$connLamp = Factory::getConnLamp();
	$cek = $connLamp->IsConnected();
	
	$id = Helper::removeSpecial($_REQUEST['id']);
	$sql = "select p.*, j.namajenispustaka, b.namabahasa 
			from ms_pustaka p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$id'";
	$row = $conn->GetRow($sql);	
	
	$sqltopik = "select t.* from pp_topikpustaka p join lv_topik t on p.idtopik=t.idtopik
		     where p.idpustaka='$id'";
	$rstopik = $conn->GetRow($sqltopik);
		
	$sql = "select * from pp_author a 
			left join ms_author m on a.idauthor=m.idauthor
			where a.idpustaka='$id'";
	$rsauth = $conn->Execute($sql);
	
	//untuk eksemplar
	$sql = "select e.idpustaka, e.ideksemplar, e.noseri, r.namarak, t.tgltenggat, a.idanggota, a.namaanggota, s.keterangan as serial, l.namalokasi,
			e.statuseksemplar, l.idunit 
		from pp_eksemplar e 
		left join lv_lokasi l on e.kdlokasi=l.kdlokasi
		left join ms_rak r on r.kdrak=e.kdrak
		left join pp_transaksi t on e.ideksemplar=t.ideksemplar and t.statustransaksi='1'
		left join ms_anggota a on t.idanggota=a.idanggota
		left join pp_serialitem s on e.idserialitem=s.idserialitem
		where idpustaka='$id'  
		order by e.noseri ";
	$rseks = $conn->Execute($sql);
	
	$sqlFile = "select idpustakafile, files, login, download from pustaka_file where idpustaka = '$id' order by idpustakafile ";
	$rsFile = $conn->GetArray($sqlFile);
	
	$peng = $conn->GetArray("select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor
				where t.idpustaka = '$id' "); 
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	$sql3 = "select isifile from lampiranperpus where jenis = 1 and idlampiran = $id ";
	$rsCover= $connLamp->GetOne($sql3);
	
	$sql4 = "select isifile from lampiranperpus where jenis = 2 and idlampiran = $id ";
	$rsSinopsis= $connLamp->GetOne($sql4);
?>

<html>
<head>
<title>Detail Pustaka</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0>
<div id="wrapper" style="width:auto;">
	<div class="SideItem" id="SideItem" style="margin:0;width:auto;">
		<header style="width:800px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/BIODATA.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>DETAIL PUSTAKA</h1>
				</div>
			</div>
		</header>
		<form name="perpusform" id="perpusform" target="_blank" method="post" enctype="multipart/form-data">
		<table width="800px" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td valign="top">
				<table class="GridStyle" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="thLeft" width=120>KODE PUSTAKA</td>
						<td>&nbsp;<?= $row['noseri']?></td>
						<td width="105" valign="middle" align="center" rowspan="7">	
							<? 
								$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
								$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
								/*$cover = Sipus::loadBlobFile($rsCover, 'jpg');*/
							?>
							<!-- <img id="imgfoto" width="130" height="150" src="<?= (is_null($cover)) ? Config::fotoUrl.'none.jpg' : $cover ?>" ><br> -->
							<img id="imgfoto" width="130" height="150" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'none.png') ?>?<?= mt_rand(1000,9999) ?>">
						</td>
					</tr>
					<tr>
						<td class="thLeft">Judul</td>
						<td>&nbsp;<?= $row['judul']?></td>
					</tr>
					<tr>
						<td class="thLeft">Edisi</td>
						<td>&nbsp;<?= $row['edisi']?></td>
					</tr>
					<tr>
						<td class="thLeft">No Panggil</td>
						<td>&nbsp;<?= $row['nopanggil']?></td>
					</tr>
					<tr>
						<td class="thLeft">Pengarang</td>
						<td>
						<?php 
							$auth = array(); 
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									echo "<a style='padding-left:4px;' href='index.php?page=show_pauthor&author=".$a."'>".$a."</a><br>";
								}
							}else{
								if($row['authorfirst1']!='')
									echo "<a style='padding-left:4px' href='index.php?page=show_pauthor&author=".$row['authorfirst1'].' '.$row['authorlast1']."'>".$row['authorfirst1'].' '.$row['authorlast1']."</a><br>";
								if($row['authorfirst2']!='')
									echo "<a style='padding-left:4px;' href='index.php?page=show_pauthor&author=".$row['authorfirst2'].' '.$row['authorlast2']."'>".$row['authorfirst2'].' '.$row['authorlast2']."</a><br>";
								
								if($row['authorfirst3']!='')
									echo "<a style='padding-left:4px;' href='index.php?page=show_pauthor&author=".$row['authorfirst3'].' '.$row['authorlast3']."'>".$row['authorfirst3'].' '.$row['authorlast3']."</a><br>";
							}
						?>
						</td>
					</tr>
					<tr>
						<td class="thLeft">Penerbit</td>
						<td>&nbsp;<?= $row['kota'].": ".$row['namapenerbit'].", ".$row['tahunterbit']?></td>
					</tr>
					<? if($row['pembimbing']!='') { ?>
					<tr>
						<td class="thLeft">Pembimbing</td>
						<td>&nbsp;<?= $row['pembimbing']?></td>
					</tr>
					<? } ?>
					<tr>
						<td class="thLeft">Kolasi</td>
						<?if($row['ilustrasi']==''){
								if($row['index_buku']=='')
									$ilus_ind = '';
								else
									$ilus_ind = ": ".$row['index_buku'].".";
							}else{
								if($row['index_buku']=='')
									$ilus_ind = ": ".$row['ilustrasi'].".";
								else
									$ilus_ind = ": ".$row['ilustrasi'].".: ".$row['index_buku'].".";
							}
						?>
						<td>&nbsp;<?= $row['jmlhalromawi'].", ".$row['jmlhalaman'].".".$ilus_ind."; ".$row['dimensipustaka']."."?></td>
					</tr>
					<tr>
						<td class="thLeft">ISBN</td>
						<td colspan=2>&nbsp;<?= $row['isbn']?></td>
					</tr>
					<tr>
						<td class="thLeft">Bahasa</td>
						<td colspan="2">&nbsp;<?= $row['namabahasa']?></td>
					</tr>
					<tr>
						<td class="thLeft">Jenis Pustaka</td>
						<td colspan="2">&nbsp;<?= $row['namajenispustaka']?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Kode DDC</td>
						<td colspan="2">&nbsp;<?= $row['kodeddc']?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Topik Pustaka</td>
						<td colspan="2">&nbsp; <?= $rstopik['namatopik'] ?></td>
					</tr>
					
					<tr>
						<td class="thLeft">Tahun Terbit</td>
						<td colspan="2">&nbsp;<?= $row['tahunterbit']?></td>
					</tr>
					<tr>
						<td class="thLeft">Tanggal Perolehan</td>
						<td colspan="2">&nbsp;<?= Helper::formatDateInd($row['tglperolehan'])?></td>
					</tr>
					<tr>
						<td class="thLeft">Sinopsis</td>
						<td  colspan=2><?  if($c_edit) { ?>
							<a style="padding-left:4px;" href="index.php?page=download&_auto=1&_ocd=sin&key=<?=$id?>"><?= $row['sinopsis_upload']!='' ? "<img src='images/attach.gif' border=0> ".Helper::GetPath($row['sinopsis_upload']) : '' ?></a>
							<br>
							<?= $row['sinopsis']!='' ?  $row['sinopsis'] : '' ?>
							<!--a style="padding-left:4px;" href="<?//= 'uploads/sinopsis/'.$row['sinopsis_upload'] ?>"><?//= $row['sinopsis_upload']!='' ? "<img src='images/attach.gif' border=0> ".Helper::GetPath($row['sinopsis_upload']) : '' ?></a-->
							<br>
							<?} else echo "<span style='padding-left:4px;'>".$row[$nama]."</span>"."<br>"; ?>
						</td>
					</tr>
					<tr>
						<td class="thLeft">File(s)</td>
						<td colspan=2>
							<?
							$k=0;
							foreach($rsFile as $f){
								$k++;
							?>
							<a href="<?= $f['files'] ?>" target="_BLANK"><?= $f['files']!='' ? "<img src='images/attach.gif' border=0> ".Helper::GetPath($f['files']) : '' ?></a>
							<span id="aksesShow<?=$f['idpustakafile'];?>">
								&nbsp; <img src="images/<?=($f['login']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Harus Login">&nbsp;Harus Login
								&nbsp; <img src="images/<?=($f['download']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Bisa didownload">&nbsp;Bisa didownload
							</span>
							<br>
							<?
							}
							?>
						</td>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<table class="GridStyle" width="100%">
					<tr>
						<th align="center" colspan="5" style="background:#015593;color:#fff;font-weight:normal;">EKSEMPLAR</th>
					</tr>
					<tr>
						<th>NO. INDUK</th>
						<? if($row['idperiode']!='') { ?>
						<th>Serial</th>
						<? } ?>
						<th>Nama Rak</th>
						<th>Lokasi</th>
						
						<th>Status</th>
					</tr>
					<? 
						while ($roweks = $rseks->FetchRow()){
						if($roweks['kdklasifikasi']=='LH')
							$warna='green';
						elseif($roweks['kdklasifikasi']=='LM')
							$warna='red';
						else
							$warna='black';
					?>
					<tr>
						<td><u title="Edit Eksemplar" style="cursor:pointer;color:blue" onclick="javascript:goDetEks('<?= $p_ekslist; ?>','<?= $roweks['ideksemplar']; ?>','<?= $row['idpustaka'] ?>');"><?= $roweks['noseri']?></u></td>
						<? if($row['idperiode']!='') { ?>
						<td style="color:<?= $warna ?>"><?= $roweks['serial']?></td>
						<? } ?>
						<td style="color:<?= $warna ?>"><?= $roweks['namarak']?></td>
						<td style="color:<?= $warna ?>"><?= $roweks['namalokasi']?></td>
						<td style="color:<?= $warna ?>"><?= $roweks['statuseksemplar']=='ADA' ? 'ADA' : ($roweks['statuseksemplar']=='PJM' ? $roweks['idanggota'].' - '. $roweks['namaanggota'] : 'PROSES');?></td>
					</tr>
					<? } ?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5" class="FootBG" align="right">&nbsp;</td>
		</tr>
		</table>
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		</form>
	</div>
</div>

</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
</html>