<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengadaan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
		
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_pengadaan';
	$p_window = '[PJB LIBRARY] Data Pemilihan Supplier';
	$p_title = '.: Data Pemilihan Supplier :.';
	$p_title1 = 'Data Pemilihan Supplier';
	$p_tvalidasi = 'Validasi Keuangan';
	$p_tvalidasi2 = 'Validasi Keuangan';
	$p_titlelist = '.: Daftar Detail Pengadaan :.';
	$p_tbheader = '.: Pengadaan :.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_versupplier.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idusulan';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$cari=$conn->GetOne("select idorderpustaka from pp_orderpustaka where supplierdipilih is null and idpengadaan=$r_key");
			if($cari=='')
			$record['statuspengadaan'] = 'S';
			else
			$record['statuspengadaan'] = 'V';
			
			Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
			
			if($err[0] == 0) {
				$sucdb = 'Validasi Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
			else{
				$errdb = 'Validasi Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'proses' and $c_edit) {
			$record = array();
			$record['statuspengadaan'] = 'V';
			Sipus::UpdateComplete($conn,$record,'pp_pengadaan',"idpengadaan=$r_key");
		}
		else if($r_aksi == 'pilih' and $c_edit) {
			$rnum = Helper::removeSpecial($_POST['num']);
			$record = array();
			$record['namasupplier'.$rnum] = Helper::cStrNull($_POST['namasupplier'.$rnum.'_'.$rkey]);
			$record['hargasupplier'.$rnum] = Helper::cStrNull($_POST['hargasupplier'.$rnum.'_'.$rkey]);
				
			$err = Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey");
			if($err[0] == 0) {
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
			else{
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'hapus' and $c_edit) {
			$rnum = Helper::removeSpecial($_POST['num']);
			$record = array();
			$record['namasupplier'.$rnum] = 'null';
			$record['hargasupplier'.$rnum] = 'null';
			//$record['alasandipilih'] = Helper::cStrNull($_POST['alasandipilih_'.$rnum.'_'.$rkey]);
			//$record['supplierdipilih'] = Helper::cStrNull($_POST['supplierdipilih_'.$rkey]);
			$err = Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey");
			if($err[0] == 0) {
				$sucdb = 'Penghapusan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
			else{
				$errdb = 'Penghapusan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'pilihsupp' and $c_edit) {
			$rnum = Helper::removeSpecial($_POST['num']);
			$record = array();
			$record['alasandipilih'] = Helper::cStrNull($_POST['alasandipilih_'.$rnum.'_'.$rkey]);
			$record['supplierdipilih'] = Helper::cStrNull($_POST['supplierdipilih_'.$rnum.'_'.$rkey]);
			$record['hargadipilih'] = Helper::cStrNull($_POST['hargadipilih_'.$rnum.'_'.$rkey]);
			$record['stspengadaan']=1;
			$err = Sipus::UpdateComplete($conn,$record,'pp_orderpustaka',"idorderpustaka=$rkey");
			if($err[0] == 0) {
				$sucdb = 'Pemilihan Supplier Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
			else{
				$errdb = 'Pemilihan Supplier Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $rkey;
		}
	}
	
  	$sql = "select * from pp_pengadaan  
			where idpengadaan=$r_key";	
	$row = $conn->GetRow($sql);
				
		// sql untuk mendapatkan isi list
	$p_sqlstr="select *,s1.namasupplier as supp1,s2.namasupplier as supp2,s3.namasupplier as supp3  
				from pp_orderpustaka op 
				left join pp_usul u on u.idusulan=op.idusulan 
				left join ms_supplier s1 on s1.kdsupplier=op.namasupplier1
				left join ms_supplier s2 on s2.kdsupplier=op.namasupplier2
				left join ms_supplier s3 on s3.kdsupplier=op.namasupplier3
			   where idpengadaan = $row[idpengadaan] order by tglusulan, idorderpustaka";
	$rs = $conn->Execute($p_sqlstr);
	
	$isItemEdit = true;
  	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table border="0" cellpadding="4" cellspacing="0">
	<tr height="20">
		<td align="center" colspan=4 class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
	<td align="center">
		<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar</span></a>
	</td>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goReset();" class="buttonshort"><span class="reset">Reset</span></a>
	</td>
	<? if (trim($row['statuspengadaan']) == 'PK'){?>
	<td align="center">
		<a href="javascript:goProses();" class="buttonshort"><span class="validasi">Proses</span></a>
	</td>
	<? }} if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
	<tr>
		<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
	</tr>
</table>
<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth+50 ?>" class="instan">
	<tr>
		<td valign="top">
			<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth ?>" style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
				<tr>
					<td align="center" class="SubHeaderBGAlt" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1; ?></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">No. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?=  $row['nopengadaan']; ?></td>
				</tr>
				<tr height="30">
					<td width="150" class="LeftColumnBG">Tgl. Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?= Helper::formatDateInd($row['tglpengadaan'],false) ?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Kode Aktivitas</td>
        			<td class="RightColumnBG" colspan="2"><?= $row['kodeaktivitas'] ?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Jumlah</td>
					<td class="RightColumnBG" colspan="2"><div id="totqty">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Total Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><div id="totprice">0</div></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Status Pengadaan</td>
					<td class="RightColumnBG" colspan="2"><?= Helper::getArrStatusP(trim($row['statuspengadaan']))?></td>
				</tr>
				<tr>
					<td class="LeftColumnBG">Keterangan</td>
					<td class="RightColumnBG" colspan="2"><?= $row['keterangan'] ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table cellpadding="4" cellspacing="0" width="<?= p_tbwidth; ?>">
	<tr>
		<td align="center" class="PageTitle"><?= $p_titlelist; ?></td>
	</tr>
</table>
<br>
<table width="900" cellpadding="4" cellspacing="0" style="border-collapse:collapse" border="1">
	<tr>
		<td width="30" nowrap align="center" class="SubHeaderBGAlt">&nbsp;</td>
		<td class="SubHeaderBGAlt"align="center" width="10%">Id Usulan</td>
		<td class="SubHeaderBGAlt" align="center">Tgl Usulan</td>
		<td class="SubHeaderBGAlt" align="center" nowrap>Judul</td>
		<td class="SubHeaderBGAlt" align="center">Pengarang</td>
		<td class="SubHeaderBGAlt" align="center">Pengusul</td>
		<td class="SubHeaderBGAlt" align="center">Harga</td>
		<td class="SubHeaderBGAlt" align="center">Qty</td>
		<td class="SubHeaderBGAlt" align="center">Validasi Perpustakaan</td>
		<td class="SubHeaderBGAlt" align="center">Validasi Keuangan</td>
	</tr>
    <? if (!empty($r_key)){ 
    		$i=0;
			while ($rows = $rs->FetchRow()){
				$z=0;
				if ($i % 2) $rowstyle = 'OtherBG';  else $rowstyle = 'AlternateBG'; $i++;		
				if (empty($rows['supp1'])) $z=1; else if (empty($rows['supp2'])) $z=2; else if (empty($rows['supp3'])) $z=3; else $z=3;
	?>
    <tr class="<?= $rowstyle ?>"> 
		<td align="center"><? if (trim($rows['statusvalperpus']) != 1 or trim($rows['statusvalkeu']) != 1) echo '&nbsp;'; else{?><img src="images/tree/<?= $isItemEdit?"minus":"add"; ?>.png" id="imgtree<?= $i; ?>" title="Detail Supplier" onClick="showSupp(this,'<?= $i; ?>')" class="link"/><? } ?></td>
    	<td align="center" valign="top"><?= $rows['idusulan']; ?></td>
        <td align="center" valign="top"><?= Helper::formatDate($rows['tglusulan']); ?></td>
    	<td align="left" valign="top"><?= $rows['judul']; ?></td>
        <td align="left" valign="top"><?= $rows['authorfirst1'].' '.$rows['authorlast1']; ?></td>
    	<td valign="top"><?= $rows['namapengusul']; ?></td>
    	<td align="right" valign="top"><?= Helper::formatNumber($rows['hargausulan']); ?><input type="hidden" name="hargausulan" id="hargausulan" value="<?= $rows['hargausulan']?>"></td>
    	<td align="right" valign="top"><?= $rows['qtypengadaan']; ?><input type="hidden" name="qtypengadaan" id="qtypengadaan" value="<?= $rows['qtypengadaan']?>"></td>
    	<td align="left" valign="top"><?= $rows['statusvalperpus'] == '' ? '<strong>Belum</strong>' : ($rows['statusvalperpus'] == 0 ? '<strong>Ditolak</strong>' : '<strong>Disetujui</strong>') ; ?><?= $rows['memoperpus'] != '' ? ',&nbsp;<br>'.$rows['memoperpus'] : ''; ?></td>
    	<td align="left" valign="top"><?= $rows['statusvalkeu'] == '' ? '<strong>Belum</strong>' : ($rows['statusvalkeu'] == 0 ? '<strong>Ditolak</strong>' : '<strong>Disetujui</strong>') ; ?><?= $rows['memokeu'] != '' ? ',&nbsp;<br>'.$rows['memokeu'] : ''; ?></td>
    </tr>
	<tr id="tr_item<?= $i; ?>" style="display:<?= $isItemEdit?"":"none"; ?>">
		<td>&nbsp;</td>
		<td colspan="9">
			<table cellpadding="4" cellspacing="0" width="100%" style="border-collapse:collapse" border="1">
				<tr>
					<td class="SubHeaderBGAlt" align="center">Dipilih</td>
					<td class="SubHeaderBGAlt" align="center">Supplier</td>
					<td class="SubHeaderBGAlt" align="center">Harga</td>
					<td class="SubHeaderBGAlt" align="center">Alasan Dipilih</td>
					<td class="SubHeaderBGAlt" align="center">Aksi</td>
				</tr>
				<? for($y=1;$y<=$z;$y++){?>
				<tr  valign="top">
					<? if (!empty($rows['supp'.$y])){ ?>
						<? if(strcasecmp(trim($rows['supp'.$y]),$p_editkey)) {  ?>
							<td align="center">
							<input type="radio" name="pilih_<?= $rows['idorderpustaka'] ?>" id="pilih_<?= $rows['idorderpustaka'] ?>" value="<?= $y; ?>" onClick="showPilih('<?= $rows[idorderpustaka] ?>','<?= $y; ?>')" <?= strcasecmp(trim($rows['supplierdipilih']),trim($rows['namasupplier'.$y])) ? '' : 'checked'; ?>>
							<input type="hidden" name="supplierdipilih_<?= $y.'_'.$rows['idorderpustaka'] ?>" id="supplierdipilih_<?= $y.'_'.$rows['idorderpustaka'] ?>" value="<?= $rows['namasupplier'.$y]; ?>">
							<input type="hidden" name="hargadipilih_<?= $y.'_'.$rows['idorderpustaka'] ?>" id="hargadipilih_<?= $y.'_'.$rows['idorderpustaka'] ?>" value="<?= $rows['hargasupplier'.$y]; ?>">
							</td>
							<td><?= $rows['supp'.$y]; ?></td>
							<td align="right"><?= Helper::formatNumber($rows['hargasupplier'.$y]); ?></td>
							<td><?= strcasecmp(trim($rows['supplierdipilih']),trim($rows['namasupplier'.$y])) ? '' : $rows['alasandipilih']; ?><span id="alasan<?= $rows['idorderpustaka'].$y; ?>" style="display:none"><?= UI::createTextArea('alasandipilih_'.$y.'_'.$rows['idorderpustaka'],$rows['alasandipilih'],'ControlStyle',3,50,$c_edit); ?></span></td>
							<td align="center"><img src="images/tombol/file.png" width="16" title="Edit" onClick="accPilihan('<?= $rows['idorderpustaka'] ?>','<?= $y; ?>')" style="cursor:pointer;color:#3300FF;display:none" id="acc<?= $rows['idorderpustaka'].$y; ?>"><img src="images/tombol/edited.gif" width="16" title="Edit" onClick="editPilihan('<?= $rows['supp'.$y] ?>','<?= $y; ?>')" class="link" id="edit<?= $rows['idorderpustaka'].$y; ?>"/>&nbsp;<img src="images/tombol/delete.png" width="16" title="Hapus" onClick="delPilihan('<?= $rows['idorderpustaka']?>','<?= $y; ?>')" class="link"/>
							</td>
							
						<? }else{ ?>
							<td align="center">&nbsp;</td>
							<td><?= UI::createTextBox('namaspp'.$y.'_'.$rows['idorderpustaka'],$rows['supp'.$y],'ControlRead',30,50,$c_edit,'readonly'); ?><input type="hidden" name="namasupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" id="namasupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" value="<?= $rows['namasupplier'.$y] ?>">&nbsp;<img src="images/tombol/breakdown.png" id="btnsupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" title="Cari Supplier" style="cursor:pointer" onClick="openLOV('btnsupplier<?= $y.'_'.$rows['idorderpustaka']; ?>', 'supplier',-400, 20,'addSupplier',800,'','<?= $rows['idorderpustaka']; ?>|<?= $y?>')"></td>
							<td><?= UI::createTextBox('hargasupplier'.$y.'_'.$rows['idorderpustaka'],$rows['hargasupplier'.$y],'ControlStyle',14,14,$c_edit); ?></td>
							<td></td>
							<td align="center"><img src="images/tombol/file.png" width="16" title="Simpan" onClick="savePilihan('<?= $rows['idorderpustaka']?>','<?= $y; ?>')" class="link" id="edit<?= $rows['idorderpustaka'].$y; ?>"/>&nbsp;<img src="images/tombol/delete.png" width="16" title="Hapus" onClick="delPilihan('<?= $rows['idorderpustaka']?>','<?= $y; ?>')" class="link"/></td>
						<? } ?>
						
					<? }else{ ?>
					<td align="center">&nbsp;</td>
					<td><?= UI::createTextBox('namaspp'.$y.'_'.$rows['idorderpustaka'],'','ControlRead',30,50,$c_edit,'readonly'); ?><input type="hidden" name="namasupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" id="namasupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" >&nbsp;<img src="images/tombol/breakdown.png" id="btnsupplier<?= $y.'_'.$rows['idorderpustaka']; ?>" title="Cari Supplier" style="cursor:pointer" onClick="openLOV('btnsupplier<?= $y.'_'.$rows['idorderpustaka']; ?>', 'supplier',-400, 20,'addSupplier',800,'','<?= $rows['idorderpustaka']; ?>|<?= $y?>')"></td>
					<td><?= UI::createTextBox('hargasupplier'.$y.'_'.$rows['idorderpustaka'],'','ControlStyle',14,14,$c_edit); ?></td>
					<td><span id="alasan<?= $rows['idorderpustaka'].$y; ?>" style="display:none"><?= UI::createTextArea('alasandipilih_'.$y.'_'.$rows['idorderpustaka'],'','ControlStyle',3,50,$c_edit); ?></span></td>
					<td align="center"><img src="images/tombol/file.png" width="16" title="Simpan" onClick="savePilihan('<?= $rows['idorderpustaka']?>','<?= $y; ?>')" class="link"/></td>
					<? } ?>
				</tr>
				
				<? } ?>
			</table>
		</td>
	</tr>
	<? }} ?>
</table>
<input type="hidden" name="key" id="key" value="<?= $r_key; ?>">
<input type="hidden" name="rkey" id="rkey">
<input type="hidden" name="act" id="act">
<input type="hidden" name="num" id="num">
</form>
</div>

<div id="newsupplier_block" class="popWindow" style="position:absolute;">
	<form method="post" onSubmit="isnewSupplier();return false;">
	<input type="hidden" name="pf" value="edit_disposisi" />
    <table cellspacing=1 cellpadding="4">
		<tr class="popWindowtr">
			<td colspan="2">
				<div style="float:left;font-weight:bold;padding:2px">Tambah Supplier</div>
				<div style="float:right">
				<span id="loadingGif2" style="display:none"><img src="images/loading.gif" /></span>
				&nbsp;
				<input type="button" class="btn_close" value="" onClick="closePop()"/>
				</div>
			</td>
		</tr>
        <tr> 
          <td width="120">Nama Supplier</td>
          <td>
				<input type="text" id="namasupplier" name="namasupplier" size="30" maxlength="100"> 
          </td>
        </tr>
        <tr> 
          <td>Alamat</td>
          <td>
				<input type="text" id="alamat" name="alamat" size="30" maxlength="100"> 
          </td>
        </tr>
    </table>
    <div style="margin-top:5px;padding:5px;background-color:#FFD787;text-align:center;"><input type="submit" value="Tambah" class="btn" /></div>
	</form>
</div>

</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

$(function(){
	hitTotal();
});

function saveData(){
		$("#act").val("simpan");
		goSubmit();
}

function goProses(id){
	$("#rkey").val(id);
	$("#act").val("proses");
	goSubmit();
}

function savePilihan(id, x){
	arr1 = "namaspp"+x+"_"+id;
	arr2 = "hargasupplier"+x+"_"+id;
	if (cfHighlight(arr1)){
		if (cfHighlight(arr2)){
			$("#rkey").val(id);
			$("#num").val(x);
			$("#act").val("pilih");
			goSubmit();
		}
	}
}

function delPilihan(id, x){
	$("#rkey").val(id);
	$("#num").val(x);
	$("#act").val("hapus");
	goSubmit();
}

function editPilihan(id, x){
	$("#rkey").val(id);
	$("#num").val(x);
	$("#act").val("sunting");
	goSubmit();
}

function accPilihan(id, x){
	$("#rkey").val(id);
	$("#num").val(x);
	$("#act").val("pilihsupp");
	goSubmit();
}

function showPilih(id,num){
	$("#alasan"+id+num).fadeIn(800);
	$("#edit"+id+num).fadeOut(800);
	$("#acc"+id+num).fadeIn(800);
	if (num == 1){
		$("#acc"+id+"2").fadeOut(800);
		$("#acc"+id+"3").fadeOut(800);
		$("#alasan"+id+"2").fadeOut(800);
		$("#alasan"+id+"3").fadeOut(800);
	}else if (num == 2){
		$("#acc"+id+"1").fadeOut(800);
		$("#acc"+id+"3").fadeOut(800);
		$("#alasan"+id+"1").fadeOut(800);
		$("#alasan"+id+"3").fadeOut(800);
	}else{
		$("#acc"+id+"1").fadeOut(800);
		$("#acc"+id+"2").fadeOut(800);
		$("#alasan"+id+"1").fadeOut(800);
		$("#alasan"+id+"2").fadeOut(800);
	}
	//document.getElementById("alasan"+id+num).style.display = '';
}

function addSupplier(kode, nama, id, num) {
	$("#namasupplier"+num+"_"+id).val(kode);
	$("#namaspp"+num+"_"+id).val(nama);
}

function showSupp(img,id){
	if($(img).attr("src") == "images/tree/add.png"){
		$(img).attr("src","images/tree/minus.png")
	
		$("#tr_item"+id).show();
		
	}else{
		$(img).attr("src","images/tree/add.png")
		$("#tr_item"+id).hide();
	}

}

function hitTotal(){
	var total = 0;
	var totprice = 0;
	$("input[name^=qtypengadaan]").each(function(i){
		var qty = parseInt($("input[name^=qtypengadaan]").eq(i).val());
		total += qty;
	});
	
	$("input[name^=hargausulan]").each(function(i){
		var hrg = parseInt($("input[name^=hargausulan]").eq(i).val());
		totprice += hrg;	
	});
	
	total += <?= $qty != '' ? $qty : 0; ?>;
	totprice += <?= $hrg != '' ? $hrg : 0;; ?>;
	$("#totqty").html(total);
	$("#totprice").html(totprice);
}

function goReset(){
	$("input[type='text'], textarea").val("");
	$("input[type='radio']").attr("checked",false); 
}
</script>
</html>