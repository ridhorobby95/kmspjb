<?php
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
		
	// require tambahan
	$r_id=Helper::removeSpecial($_GET['id']);
	$r_date=Helper::removeSpecial($_GET['date']);
	$r_jenis=Helper::removeSpecial($_GET['code']);
	if($r_jenis=='t'){
	$pinjam=$conn->Execute("select ideksemplar,noseri,judul,authorlast1,tgltenggat,nopanggil from v_trans_list 
							where idanggota='$r_id' and tgl_transaksi='$r_date' and tglpengembalian is null and tglperpanjang is null");
	
	$panjang=$conn->Execute("select ideksemplar,noseri,judul,authorlast1,tgltenggat,nopanggil from v_trans_list 
							where idanggota='$r_id' and tgl_perpanjang='$r_date' and tglpengembalian is null ");
	
	$kembali=$conn->Execute("select ideksemplar,noseri,judul,authorlast1,tgltenggat,nopanggil from v_trans_list 
							where idanggota='$r_id' and tgl_pengembalian='$r_date' and statustransaksi='0'");
	
	$reserve=$conn->Execute("select ideksemplarpesan,noseri,judul,authorlast1,tglexpired from v_reserve where idanggotapesan='$r_id' and tglreservasi='$r_date' and statusreservasi='1'");
	}
	if($r_jenis=='b'){
	$terpinjam=$conn->Execute("select ideksemplar,noseri,judul,authorlast1,tgltenggat,nopanggil from v_trans_list 
							   where idanggota='$r_id' and tglpengembalian is null and kdjenistransaksi='PJN'");
	}
	
	$anggota=$conn->GetRow("select namaanggota from ms_anggota where idanggota='$r_id'");
	
	$user= $conng->GetRow("select userdesc from gate.sc_user where username='".$_SESSION['PERPUS_USER']."'");
?>

<html>
<head>
<title>Nota Transaksi</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onLoad="printpage()">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>PERPUSTAKAAN PJB<br>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="430" border="0" >
	<tr>
		<td width="170">Id Anggota</td>
		<td>: <?= $r_id ?></td>
	</tr>
	<tr>
		<td>Nama Anggota</td>
		<td>: <?= $anggota['namaanggota'] ?></td>
	</tr><br>
	<tr>
		<td colspan="2"><br>
		<? if($pinjam->fields['ideksemplar']!='') {?>
			<u>PEMINJAMAN PUSTAKA</u><br><br>
			<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">

			<tr height="20">
				<td width="150" style="border-bottom:1px solid">NO INDUK</td>
				<td width="150" style="border-bottom:1px solid">NO PANGGIL</td>
				<td width="150" style="border-bottom:1px solid">BTS KEMBALI</td>
			</tr>
			<?php 
			// mulai iterasi
				$i=0;
				while ($rowp = $pinjam->FetchRow()) 
				{ $i++; ?>
					<tr height="15">
						<td><?= $rowp['noseri']; ?></td>
						<td><?= $rowp['nopanggil'] ?></td>
						<td><?= Helper::tglEngTime($rowp['tgltenggat']) ?></td>
					</tr>
					<tr>
						<td colspan="3"><?= Helper::limitS($rowp['judul'],50) ?><br><?= $rowp['authorlast1'] ?></td>
					</tr>
				<?php } if($i==0) {?>
					<tr><td align="center" colspan="3">Data Peminjaman Pustaka Tidak Ditemukan</td></tr>
				<?}?>
			</table><br>
		<? }?>
		<? if($panjang->fields['ideksemplar']){ ?>
		<u>PERPANJANGAN PUSTAKA</u><br><br>
			<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">

		<tr height="20">
			<td width="150" style="border-bottom:1px solid">NO INDUK</td>
			<td width="150" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="150" style="border-bottom:1px solid">BTS KEMBALI</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rowj = $panjang->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rowj['noseri']; ?></td>
			<td><?= $rowj['nopanggil'] ?></td>
			<td><?= Helper::tglEngTime($rowj['tgltenggat']) ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rowj['judul'],50) ?><br><?= $rowj['authorlast1'] ?></td>
		</tr>
		<?php } if($i==0){ ?>
			<tr><td align="center" colspan="3">Data Perpanjangan Pustaka Tidak Ditemukan</td></tr>
		<?}?>
		</table><br><? } ?>
		<? if($reserve->fields['ideksemplarpesan']){ ?>
		<u>PEMESANAN PUSTAKA</u><br><br>
			<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">

		<tr height="20">
			<td width="150" style="border-bottom:1px solid">NO INDUK</td>
			<td width="150" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="150" style="border-bottom:1px solid">TGL EXPIRED</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rows = $reserve->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rows['noseri']; ?></td>
			<td><?= $rows['nopanggil'] ?></td>
			<td><?= Helper::tglEng($rows['tglexpired']) ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rows['judul'],50) ?><br><?= $rows['authorlast1'] ?></td>
		</tr>
		<?php } if($i==0){ ?>
			<tr><td align="center" colspan="3">Data Pemesanan Pustaka Tidak Ditemukan</td></tr>
		<?}?>
		</table><br><?}?>
		<? if($kembali->fields['ideksemplar']){ ?>
		<u>PENGEMBALIAN PUSTAKA</u> <br><br>
			<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">

		<tr height="20">
			<td width="150" style="border-bottom:1px solid">NO INDUK</td>
			<td width="150" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="150" style="border-bottom:1px solid">BTS KEMBALI</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rowk = $kembali->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rowk['noseri']; ?></td>
			<td><?= $rowk['nopanggil'] ?></td>
			<td><?= Helper::tglEngTime($rowk['tgltenggat']) ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rowk['judul'],50) ?><br><?= $rowk['authorlast1'] ?></td>
		</tr>
		<?php } if($i==0){ ?>
			<tr><td align="center" colspan="3">Data Pengembalian Pustaka Tidak Ditemukan</td></tr>
		<?}?>
		</table><br><?}?>
		<? if($r_jenis=='b') {?>
		<u>PUSTAKA BELUM KEMBALI</u><br><br>
			<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">

		<tr height="20">
			<td width="150" style="border-bottom:1px solid">NO INDUK</td>
			<td width="150" style="border-bottom:1px solid">NO PANGGIL</td>
			<td width="150" style="border-bottom:1px solid">BTS KEMBALI</td>
		</tr>
		<?php 
		// mulai iterasi
			$i=0;
			while ($rowt = $terpinjam->FetchRow()) 
			{ $i++; ?>
		<tr height="15">
			<td><?= $rowt['noseri']; ?></td>
			<td><?= $rowt['nopanggil'] ?></td>
			<td><?= Helper::tglEngTime($rowt['tgltenggat']) ?></td>
		</tr>
		<tr>
			<td colspan=3><?= Helper::limitS($rowt['judul'],50) ?><br><?= $rowt['authorlast1'] ?></td>
		</tr>
		<?php } if($i==0){ ?>
			<tr><td align="center" colspan="3">Data Pustaka Belum Kembali Tidak Ditemukan</td></tr>
		<?}?>
		</table><br><? }?>
		</td>
	</tr>
</table>
<br>
<table width="450">
	<tr>
		<td>Terima Kasih | <?= date('d-m-Y') ?><br><br>
		Waktu awal : <?= $_SESSION['wmulai'] ?> waktu akhir : <?= date('H:i:s') ?><br>
		<?= $user['userdesc'] ?></td>
	</tr>
	<tr height=30>
	    <td align="center">Perpanjangan Online http://digilib.PJB.ac.id/</td>
	</tr>
	<tr height=30>
	    <td align="center">------ Informasi : perpus.PJB@gmail.com ------</td>
	</tr>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>
