<div style="width:100%;float:left;">
	<center>
		<div style="width:<?= $p_tbwidth ?>%">
			<div class="extension bottom inright pagination">
				<div class="dataTables_paginate paging_full_numbers" id="DataTables_Table_0_paginate">
					<a class="first paginate_button <?= $p_page == 1 ? 'paginate_button_disabled' : '"' ?>" title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)"><<</a>
					<a class="previous paginate_button <?= $p_page == 1 ? 'paginate_button_disabled' : '"' ?>" title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)"><</a>
					<a class="next paginate_button <?= $p_page == $p_lastpage ? 'paginate_button_disabled' : '"' ?>" title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)">></a>
					<a class="last paginate_button <?= $p_page == $p_lastpage ? 'paginate_button_disabled' : '"' ?>" title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)">>></a>
				</div>
			</div>
		</div>
	</center>
</div>