<?php
//$conn->debug = true;
	define('BASEPATH',1);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	require_once('../application/helpers/x_helper.php');

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Sirkulasi Peminjaman Pustaka';
	$p_title = 'SIRKULASI PEMINJAMAN';
	$p_tbheader = '.: SIRKULASI PEMINJAMAN :.';
	$p_col = 10;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_anggota.php');
	$p_filedetaild = Helper::navAddress('ms_anggotad.php');
	
	if(isset($_POST['btntrans'])){
		
		$idcheck=trim(Helper::removeSpecial($_POST['txtid']));
		$sqlanggota=$conn->GetRow("select * from ms_anggota where idanggota='$idcheck'");
		if ($sqlanggota['idanggota']) {
			if($sqlanggota['idunit'] != $idunit){
				echo "Anggota bukan berasal dari unit $unitlogin.";
			}else{
				$sekarang = date('Y-m-d');
				# cek buku tamu
				//session di selesai di bawa kemari
				$conn->StartTrans();
				$conn->Execute("update pp_transaksi set fix_status='1' where idanggota = '".$_SESSION['keyword']."'");
				$conn->CompleteTrans();
				
				Helper::clearTrans();
				unset($_SESSION['waktu']);
				unset($_SESSION['wmulai']);
				
				//session saat login idanggota
				$_SESSION['keyword']=$idcheck;
				$_SESSION['waktu']=date('Y-m-d H:i:s');
				$_SESSION['wmulai']=date('H:i:s');
				Helper::redirect();
			}
		}else {
			unset($_SESSION['keyword']);
			unset($_SESSION['waktu']);
			unset($_SESSION['tab']);


			$errdb = 'Data anggota tidak ditemukan.';
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}
	}
	
	$p_anggota="select lj.namasatker, a.idanggota, a.catatan, a.namaanggota, a.alamat, a.email, a.statusanggota, a.tgldaftar, a.tglexpired,
			a.kdjenisanggota, a.tglselesaiskors, a.uangjaminan, j.namajenisanggota, a.denda, a.idpegawai 
			from ms_anggota a
			join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota 
			left join ms_satker lj on lj.kdsatker=a.idunit
			where a.idanggota='".$_SESSION['keyword']."' ";
	$r_anggota=$conn->GetRow($p_anggota);

	$p_cektenggat=$conn->GetRow("select idtransaksi
				    from pp_transaksi
				    where idanggota='".$_SESSION['idanggotata']."' and to_char(tgltenggat,'YYYY-mm-dd') < to_char(current_date,'YYYY-mm-dd') and tglpengembalian is null");
	$r_aksi=Helper::removeSpecial($_POST['act']);
	

	
	if (!empty($_POST)) {
		$idanggota=$_SESSION['keyword'];
		$jenisanggota=$r_anggota['kdjenisanggota'];
		$noseri=Helper::removeSpecial($_POST['txtpustaka']);
		$idpesen=Helper::removeSpecial($_POST['txtreservasi']);
		$noserifoto=Helper::removeSpecial($_POST['txtpustakafoto']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		$r_key3=Helper::removeSpecial($_POST['key3']); // tab yg dipilih
		$r_ideks=Helper::removeSpecial($_POST['ideks']);
		$waktumulai=Helper::removeSpecial($_POST['keydate']);
		
		$bebasd=$_POST['bebasd']; //die($bebasd);
		if($bebasd)
			$_SESSION['bebsd'] = true;
		else
			$_SESSION['bebsd'] = false;
		
		if($r_key3)
			$_SESSION['tab'] = $r_key3;
		else
			$_SESSION['tab'] = "0";
					
		if(!$r_anggota){
			$errdb = 'Data anggota tidak ditemukan.';
			unset($_SESSION['keyword']);
			unset($_SESSION['waktu']);
			unset($_SESSION['tab']);
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}else {
		if ($r_aksi=='pinjam'){
			if ($noseri==''){
				$errdb = 'Masukkan NO INDUK dengan benar.';
				Helper::setFlashData('errdb', $errdb);			
			} else {
				$rs_cek_expired = $conn->GetRow("select * from ms_anggota where idanggota='".$_SESSION['keyword']."'");
				if($rs_cek_expired['tglexpired'] > date("Y-m-d") or $rs_cek_expired['tglexpired']==''){
					Sipus::newTrans($conn,$noseri,$idanggota,$jenisanggota);
				}else{
					$errdb = 'Masa Berlaku keanggotaan telah habis! Silakan diperpanjang terlebih dahulu.';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				}
			}
		}
		elseif ($r_aksi=='pinjamfoto'){
			if ($noserifoto==''){
				$errdb = 'Masukkan NO INDUK dengan benar.';
				Helper::setFlashData('errdb', $errdb);			
			} else {
				Sipus::newTransFotocopy($conn,$noserifoto,$idanggota,$jenisanggota);
			}
		}
		elseif ($r_aksi=='pinjamkhusus') {
			$noseri2 = Helper::removeSpecial($_POST['txtpustaka2']);
			Sipus::newTransSpecial($conn,$noseri2,$idanggota,$jenisanggota);
		} 
		elseif ($r_aksi=='delsession') {
			$conn->StartTrans();			
			$conn->Execute("update pp_transaksi set fix_status='1' where idanggota = '".$_SESSION['keyword']."'");
			
			$conn->CompleteTrans();
			
			Helper::clearTrans();
			unset($_SESSION['waktu']);
			unset($_SESSION['wmulai']);
			unset($_SESSION['tab']);
			unset($_SESSION['bebsd']);
			$sucdb = 'Proses transaksi telah selesai';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
			
		} 
		elseif ($r_aksi=='batal') {
			$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where noseri='$r_key2'");
			$ideksemplar=$p_eks['ideksemplar'];
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_transaksi,idtransaksi,$r_key);
			//update status
			$rec['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$ideksemplar);
			
			$p_rupiah=$conn->GetRow("select uangjaminanpinjaman from lv_jenisanggota where kdjenisanggota='".$r_anggota['kdjenisanggota']."'");
			$rupiah=$p_rupiah['uangjaminanpinjaman'];
			
			//update uang jaminan anggota
					
			$recuang['uangjaminan'] =$r_anggota['uangjaminan']-$rupiah;
			Sipus::UpdateBiasa($conn,$recuang,ms_anggota,idanggota,$idanggota);
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Transaksi gagal dibatalkan.';	
				Helper::setFlashData('errdb', $errdb);
			}else {
				$sucdb = 'Transaksi sukses dibatalkan.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		elseif ($r_aksi=='batalR') {
			$conn->StartTrans();
			$record['statusreservasi']=3;#batal
			Sipus::UpdateBiasa($conn,$record,pp_reservasi,idreservasi,$r_key);

			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Reservasi gagal dibatalkan.';	
				Helper::setFlashData('errdb', $errdb);
				}
			else {
				$sucdb = 'Reservasi sukses dibatalkan.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		elseif ($r_aksi=='panjang') {
			#sebelum batas kembali
			$p_cektrans=$conn->GetRow("select idtransaksi from pp_transaksi where idanggota='".$_SESSION['keyword']."' and tglpengembalian is null and statustransaksi='1'");
			if($p_cektrans) {
				$hariini=date('Y-m-d H:i:s');//date("Y-m-d");
				$err=Sipus::fastnew2($conn,$r_key2,$hariini,$r_ideks,$_SESSION['keyword'],true,$_SESSION['bebsd']);
			} 
		}
		elseif ($r_aksi=='kembali') {
			$hariini=date('Y-m-d H:i:s');//date("Y-m-d");
			$err=Sipus::fastback($conn,$r_key2,$hariini,$r_ideks,true,$_SESSION['bebsd']);
			 
			if($err != 0){
				$errdb = 'Pengembalian peminjaman gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				if($_SESSION['reserve'] !='')				
					$sucdb = "Pengembalian peminjaman berhasil. <u style='color:red;cursor:pointer' onclick=\"popup('index.php?page=nota_reserve&id=$_SESSION[reserve]&time=$_SESSION[wmulai]',450,400)\"><b>Cetak Nota Reservasi</b></u>";
				else
					$sucdb = 'Pengembalian peminjaman berhasil.';	
				
				Helper::setFlashData('sucdb', $sucdb);
				unset($_SESSION['telat']);
				unset($_SESSION['denda']);
				unset($_SESSION['anggotadenda']);
				Helper::redirect();
			}
		}
		
		elseif ($r_aksi=='panjangcepat') {
			#getdata
			$p_cektrans=$conn->GetRow("select t.idtransaksi, t.ideksemplar 
						  from pp_transaksi t
						  join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
						  where t.idanggota='".$_SESSION['keyword']."'
							and t.tglpengembalian is null
							and t.statustransaksi='1'
							and e.noseri = '$r_key2' ");
			$ideks = $p_cektrans['ideksemplar'];
			if($p_cektrans['idtransaksi']) {
				$hariini=date('Y-m-d H:i:s');//date("Y-m-d");
				$err=Sipus::fastnew2($conn,$r_key2,$hariini,$ideks,$_SESSION['keyword'],true,$_SESSION['bebsd']);
			}else{
				$errdb = 'Data Eksemplar tidak cocok.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		elseif ($r_aksi=='kembalicepat') {
			#getdata
			$p_cektrans=$conn->GetRow("select t.idtransaksi, t.ideksemplar 
						  from pp_transaksi t
						  join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
						  where t.idanggota='".$_SESSION['keyword']."'
							and t.tglpengembalian is null
							and t.statustransaksi='1'
							and e.noseri = '$r_key2' ");
			$ideks = $p_cektrans['ideksemplar'];
			if($p_cektrans['idtransaksi']) {
				$hariini=date('Y-m-d H:i:s');//date("Y-m-d");
				$err=Sipus::fastback($conn,$r_key2,$hariini,$ideks,true,$_SESSION['bebsd']);
				 
				if($err != 0){
					$errdb = 'Pengembalian peminjaman gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					if($_SESSION['reserve'] !='')				
					$sucdb = "Pengembalian peminjaman berhasil. <u style='color:red;cursor:pointer' onclick=\"popup('index.php?page=nota_reserve&id=$_SESSION[reserve]&time=$_SESSION[wmulai]',450,400)\"><b>Cetak Nota Reservasi</b></u>";
					else
					$sucdb = 'Pengembalian peminjaman berhasil.';	
					
					Helper::setFlashData('sucdb', $sucdb);
					unset($_SESSION['telat']);
					unset($_SESSION['denda']);
					unset($_SESSION['anggotadenda']);
					Helper::redirect();
				}
			}else{
				$errdb = 'Data Eksemplar tidak cocok.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		
		elseif ($r_aksi=='pesen') {
			if ($idpesen==''){
				$errdb = 'Masukkan No.Induk dengan benar.';
				Helper::setFlashData('errdb', $errdb); 
			}
			else {
				$p_pustaka=$conn->GetRow("select p.idpustaka, e.ideksemplar
							 from pp_eksemplar e
							 join ms_pustaka p on e.idpustaka=p.idpustaka
							 where e.noseri='$idpesen'");
				
				$p_cektrans2=$conn->GetRow("select idpustaka,ideksemplar
							   from v_trans_list
							   where idanggota='".$_SESSION['keyword']."' and idpustaka='".$p_pustaka['idpustaka']."'
								and statustransaksi='1'");
				if(!$p_cektrans2) {
					Sipus::newReserve($conn,$idpesen,$idanggota,$jenisanggota);
				}else {
					$errdb = 'Pustaka tidak dapat dipesan oleh peminjamnya.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}
		elseif ($r_aksi=='freeskors') { // selanjutnya akan dikembangkan dengan sistem cicilan :D
			$r_nilai=Helper::removeSpecial($_POST['nilai']);
			$r_bayardenda=Helper::removeSpecial($_POST['bayar']);
			if(!$r_bayardenda)
				$r_bayardenda = $conn->GetOne("select denda from ms_anggota where idanggota = '$r_key' ");

			$recbebas=array();
			$recbayar=array();
			if($r_nilai)
				$recbebas['denda']=$r_nilai;
			else
				$recbebas['denda']=null;
			
			$err=Sipus::UpdateBiasa($conn,$recbebas,ms_anggota,idanggota,$r_key);
			if($_SESSION['bebsd'] == false){ #jika bebas denda non aktif
				$recordbayar = array();
				$recordbayar['idanggota']=$r_key;
				$recordbayar['tglbayar']=date('Y-m-d');
				$recordbayar['nilai']=$r_bayardenda;
				Helper::Identitas($recordbayar);
				$err=Sipus::InsertBiasa($conn,$recordbayar,pp_bayardenda);
			}
			
			//cari transaksi yang mana untuk memberikan tgl bayar
			$sql_transaksi = $conn->Execute("select * 
							from pp_denda d left join pp_transaksi t on d.idtransaksi=t.idtransaksi
									where d.tglbayar is null and (statustransaksi='0' or statustransaksi='2') and d.idanggota='$r_key'");
			$recbayar['tglbayar'] = date("Y-m-d");
			while($row_bayar = $sql_transaksi->FetchRow()){
				$recbayar['petugasbayar'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
				$recbayar['hostbayar'] = Helper::cStrNull($_SERVER['REMOTE_ADDR']);
				$recbayar['timesbayar'] = date('d-m-Y H:i:s');
				$err=Sipus::UpdateBiasa($conn,$recbayar,pp_denda,idtransaksi,$row_bayar['idtransaksi']);
			}
			
			if($err != 0){
				$errdb = 'Pembayaran denda gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembayaran denda berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();				
			}
		}
		elseif($r_aksi=='pinjamreserve'){
			$res = $conn->GetRow("select r.*, e.noseri
					     from pp_reservasi r
					     join pp_eksemplar e on e.ideksemplar = r.ideksemplarpesan
					     where r.idreservasi='$r_ideks'");
			if ($res['noseri']==''){
				$errdb = 'Masukkan NO INDUK dengan benar.';
				Helper::setFlashData('errdb', $errdb);			
			} else {
				$rs_cek_expired = $conn->GetRow("select * from ms_anggota where idanggota='".$res['idanggotapesan']."'");
				if($rs_cek_expired['tglexpired'] > date("Y-m-d") or $rs_cek_expired['tglexpired']==''){
					Sipus::newTrans($conn,$res['noseri'],$rs_cek_expired['idanggota'],$rs_cek_expired['kdjenisanggota']);
				}else{
					$errdb = 'Masa Berlaku keanggotaan telah habis! Silakan diperpanjang terlebih dahulu.';	
					Helper::setFlashData('errdb', $errdb);
					Helper::redirect();
				}
			}
		}
	}
	}

		//list sirkulasi
		if($_SESSION['keyword']!=''){
			$sql ="select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka
						from ms_author p
						left join pp_author t on t.idauthor = p.idauthor"; 
			$peng = $conn->GetArray($sql);
			$pengarang = array();
			foreach($peng as $p){
				$pengarang[$p['idpustaka']][] = $p['pengarang'];
			}
			
			
			$sql2 = "select idtransaksi,ideksemplar,judul,judulseri,edisi,authorfirst1,authorlast1,tgltransaksi,tgltenggat,noseri,fix_status
				from v_trans_list
				where tgltransaksi=current_date and statustransaksi='1'
				and idanggota='".$_SESSION['keyword']."' and kdjenistransaksi<>'PJL' and fix_status='0'";
			$p_trans=$conn->Execute($sql2);
						
			$p_pinjam=$conn->Execute("select * from v_trans_list where statustransaksi='1' and idanggota='".$_SESSION['keyword']."' and kdjenistransaksi='PJN' and kdjenispustaka in(".$_SESSION['roleakses'].")");
			$p_kembalicepat=$conn->Execute("select * from v_trans_list where statustransaksi='1' and idanggota='".$_SESSION['keyword']."' and kdjenistransaksi='PJN' and kdjenispustaka in(".$_SESSION['roleakses'].")");
			$p_panjangcepat=$conn->Execute("select * from v_trans_list where statustransaksi='1' and idanggota='".$_SESSION['keyword']."' and kdjenistransaksi='PJN' and kdjenispustaka in(".$_SESSION['roleakses'].")");

			$p_fotocopy=$conn->Execute("select * from v_trans_list where statustransaksi='1' and idanggota='".$_SESSION['keyword']."' and kdjenistransaksi='PJF' and kdjenispustaka in(".$_SESSION['roleakses'].")");
			
			$p_reservasi=$conn->Execute("SELECT * FROM (select inner_query.*, rownum rnum FROM (
						    select *
						    from v_reserve
						    where to_date(to_char(tglexpired,'YYYY-mm-dd'),'YYYY-mm-dd') >= to_date(to_char(current_date,'YYYY-mm-dd'),'YYYY-mm-dd') 
						    and idanggotapesan='".$_SESSION['keyword']."'
						    and statusreservasi='1'
						    )  inner_query WHERE rownum <= 15) order by idreservasi");
			
			$p_sejarah=$conn->Execute("SELECT * FROM (select inner_query.*, rownum rnum FROM (
						  select * from v_trans_list where idanggota='".$_SESSION['keyword']."'  and kdjenispustaka in(".$_SESSION['roleakses'].")
						  )  inner_query WHERE rownum <= 15) order by tgltransaksi desc ");
			
			$p_skors = $conn->GetRow("select keterangan from pp_skorsing where idanggota='".$_SESSION['keyword']."' order by idskorsing desc");
			
			$p_foto = Config::dirFoto.'anggota/'.trim($_SESSION['keyword']).'.jpg';
			$p_hfoto = Config::fotoUrl.'anggota/'.trim($_SESSION['keyword']).'.jpg';
			
			$p_mhsfoto = Config::dirFotoMhs.trim($_SESSION['keyword']).'.jpg';
			$p_pegfoto = Config::dirFotoPeg.trim($_SESSION['keyword']).'.jpg';
			
			$gambar = Sipus::getFoto2($conn,$_SESSION['keyword']);
			
			$rs_b = $conn->Execute("select kdjenisanggota, biayakeanggotaan from lv_jenisanggota");
			while ($row_b=$rs_b->FetchRow()){
				$jbiaya[$row_b['kdjenisanggota']]=$row_b['biayakeanggotaan'];
			}		
		}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
		
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon">TRANSAKSI</div>
		<? if($_SESSION['keyword']!='') { ?>
		<div><span class="out" name="btnselesai" id="btnselesai" value="Selesai" onClick="goClear()" style="padding-right:0px;cursor:pointer;"></span><span class="buttonSmall" style="cursor:pointer;background-color: #F4FA58;" onClick="goClear()">Selesai</span></div>
		<? } ?>
		<div align="center">
				<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
                  <div class="table-responsive">  
				<table width="100%" border="0" cellpadding="4" cellspacing=0>
					<? if($_SESSION['keyword']=='') { ?>
					<tr>
					<td width="<?= $p_tbwidth ?>" align="center">
					<center><br><? include_once('_notifikasi.php'); ?></center>
					  </td>
					  </tr><? } ?>	
						
					<? if($_SESSION['keyword']!='' or $_SESSION['keyword']=='') { ?>
					
					<td align="center">
					
					<table width="600" cellspacing="0" cellpadding="0"  border="0" style="background:#EBF2F9;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
					<tr height="25">
						<th colspan="4">Sirkulasi Transaksi</td>
					</tr>
					<tr height="10">
						<td>&nbsp;</td>
					</tr>
					<tr height="22">
						<td width=160 ><b>Id Anggota <?//=$jbiaya[$r_anggota['kdjenisanggota']];?></b></td>
						<td width="220">:
						<input type="text" name="txtid" id="txtid" size="15" maxlength="20" class="ControlStyleT" onKeyDown="etrTrans(event);" value="<?= $r_anggota['idanggota'];?>">
						<? if($c_edit) { 
						if($jbiaya[$r_anggota['kdjenisanggota']]!='' or $jbiaya[$r_anggota['kdjenisanggota']]!=0) {
						?>
						<u title="Edit Anggota" onclick="goDetail('<?= $p_filedetail; ?>','<?= $r_anggota['idanggota']; ?>');" class="link"><img src="images/edit.png"></u>
						<? } else { ?>
						<u title="Edit Anggota" onclick="goDetail('<?= $p_filedetaild; ?>','<?= $r_anggota['idanggota']; ?>');" class="link"><img src="images/edit.png"></u>
						<div style="display:none"><input type="submit" name="btntrans" class="buttonSmall" id="btntrans"></div>
						<? }}else {?>
						<?= $r_anggota['idanggota'] ?>
						<? } ?>
						</td>
						<td rowspan=5 align="center">
						<img border="1" id="imgfoto" src="<?= Config::pictProf.xEncrypt($r_anggota['idpegawai']).'&s=300'; ?>" width="110" height="110">
						</td>
					</tr>
					<tr height="22">
						<td ><b>Nama Anggota </b></td>
						<td width="300"> : <b><?= $r_anggota['namaanggota'] ?></b></td>
						
					</tr>
					<tr height="22">
						<td ><b>Bagian </b></td>
						<td width="300"> : <b><?= $r_anggota['namasatker']=='' ? '-' : $r_anggota['namasatker'] ?></b></td>
					</tr>
					<tr height="22">
						<td><b>Jenis Keanggotaan</b></td>
						<td>: <?= $r_anggota['namajenisanggota'] ?></td>
					</tr>		
					<tr height="22">
						<td ><b>Alamat</b></td>
						<td >: <?= $r_anggota['alamat'] ?></td>
					</tr>
					<tr height="22">
						<td ><b>E-Mail</b></td>
						<td >: <?= $r_anggota['email']=='' ? '-' : $r_anggota['email'] ?></td>
					</tr>
					<? if($r_anggota['tglexpired']!='') {?>
					<tr height="22">
						<td><b>Tanggal Expired</b></td>
						<td>: <?= Helper::formatDateInd($r_anggota['tglexpired']) ?></td>
						<td>&nbsp;</td>
					</tr>
					<? } ?>
					<tr height="22">
						<td><font color="red"><b>Hutang Denda</b></td>
						<td>: <font color="red" <?=($r_anggota['denda']?'class = "blink"':"");?>">Rp. <?= $r_anggota['denda'] == null ? '0' : $r_anggota['denda']?>,- &nbsp;</font>
							<?if($r_anggota['denda']!=null or $r_anggota['denda'] != 0){?>
								<?= UI::createTextBox('bayar','','ControlStyleT',10,10,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
								<u title="Bayarkan Denda" onclick="goFreeSkors('<?= $r_anggota['idanggota'] ?>','<?=$r_anggota['denda']?>')" class="link"><img src="images/freeskors.gif">bayarkan</u>
							<?}?>
						</td>
						<td>&nbsp;</td>
					</tr>	
					<?php if($_SESSION['keyword']){ ?>
					<? if ($r_anggota['statusanggota']==0){ ?>
					<tr height="22">
						<td colspan="3" align="center"><font color="red"><b>
						<blink> Anggota tersebut telah nonaktif [ <?= $r_anggota['catatan'] ?> ] !!! </blink>
						</b></font></td>
					</tr>
					<? }elseif($r_anggota['statusanggota']==2){ ?>
						<tr height="22">
						<td colspan="3" align="center"><font color="red"><b>
						<blink>Anggota tersebut telah terblokir !!!</blink>
						</b></font></td>
					</tr>	
					<? }
					} ?>
					<? if ($r_anggota['tglselesaiskors'] >= date("Y-m-d")){ ?>
					<tr height="22">
						<td colspan="3" align="center"><font color="red"><b>
						<script>
						var skor="Anggota terkena masa skors [ <?= $p_skors['keterangan'] ?> ] sampai tanggal <?= Helper::formatDateInd($r_anggota['tglselesaiskors']) ?>.";
						document.write(skor.blink());
						</script>
						</b></font></td>
					</tr>
					<? } ?>
					<? if($p_cektenggat) { ?>
					<tr height="22">
						<td colspan="3" align="center"><font color="red"><b>
						<script>
						var tenggat="Terdapat pustaka belum dikembalikan yang melewati masa tenggat.";
						document.write(tenggat.blink());
						</script>
						</b></font></td>
					</tr>
					<? }?>
					<?
					if($_SESSION['keyword']){
						if($r_anggota['tglexpired'] < date("Y-m-d") and $r_anggota['tglexpired'] <> null){?>
							<tr height="22">
							<td colspan="3" align="center"><font color="red"><b>
							<span class="blink">Masa Berlaku anggota telah habis !!!</span>
							</b></font></td>
					<?}
					}?>
					<tr>
						<td align="right" colspan=5><hr><b>Bebas Denda : </b>&nbsp;<input type="checkbox" id="bebasd" <?=($_SESSION['bebsd']?"checked":"");?> name="bebasd" value="1" />
							<?if($_SESSION['bebsd']){?>
							<br/><font color="red"><b><span class="blink">Bebas Denda AKTIF !!!</span></b></font>
							<?}?>
						</td>
					</tr>
					</table>
    </div><br />
					<?php if($_SESSION['keyword']){?>
					<table style="padding:3px 5px;margin:0 auto;background-image:none;border-top:1px solid #ccc;" class="GridStyle">
						<tr>
							<td class="thLeft" style="border:0 none;">
								<img src="images/printer.png" title="Cetak Nota Transaksi" onClick="popup('index.php?page=cetak_nota&id=<?= $_SESSION['keyword']; ?>&date=<?= date("Y-m-d"); ?>&code=t',450,400);" style="cursor:pointer">
								<span style="cursor:pointer;position:relative;bottom:3px;" onClick="popup('index.php?page=cetak_nota&id=<?= $_SESSION['keyword']; ?>&date=<?= date("Y-m-d"); ?>&code=t',450,400);">Nota Transaksi</span>
							</td>
							<td class="thLeft" style="padding:0px 15px;border:0 none;">|</td>
							<td class="thLeft" style="border:0 none;padding-right:15px;">
								<img src="images/printer.png" title="Cetak Terpinjam" onClick="popup('index.php?page=cetak_nota&id=<?= $_SESSION['keyword']; ?>&date=<?= date("Y-m-d"); ?>&code=b',450,400);" style="cursor:pointer">
								<span style="cursor:pointer;position:relative;bottom:3px;" onClick="popup('index.php?page=cetak_nota&id=<?= $_SESSION['keyword']; ?>&date=<?= date("Y-m-d"); ?>&code=b',450,400);">Nota Belum Kembali</span>
							</td>
						</tr>
					</table>
					<?php } ?>
					</td>
					</tr>
					
					<tr>
					<td>			
				<div class="tabs" style="width:100%">
					<ul>
						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)" onClick="goTab(0)">Sedang Dipinjam</a></li>
						
						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)"  onclick="goTab(1)">Pengembalian</a></li>
						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)"  onclick="goTab(2)">Perpanjangan</a></li>

						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)"  onclick="goTab(3)">Fotocopy</a></li>
						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)"  onclick="goTab(4)">Reservasi</a></li>
						<li onClick="chCol()"><a id="tablink" href="javascript:void(0)"  onclick="goTab(5)">Sejarah Peminjaman</a></li>
						
						<div class="a" align="right" valign="center"><? include_once('_notifikasi_trans.php'); unset($_SESSION['reserve']) ?><br></div>
					
					</ul>
					<div style="background:#015593;width:100%;height:1px;margin-bottom:10px;"></div>
				<!-- ================================= sedang dipinjam ============================ -->
				<div id="items"  style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Sedang Dipinjam [Peminjaman]</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle">
					<tr height="35" valign="center">
						<td colspan="<?= $p_col; ?>" class="LeftColumnBG thLeft">
						Masukkan No. Induk : &nbsp; 
						<input type="text" name="txtpustaka" id="txtpustaka" size="30" value="<?= $noseri ?>" onKeyDown="etrPinjam(event);">&nbsp; &nbsp;
						
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $r_anggota['statusanggota']!=1) { ?>
						
						<input type="button" name="btnpustaka" id="btnpustaka" value="Pinjam" class="buttonSmallDis" disabled style="height:23px;width:130px;font-size:12px;">
						<? }else {?>
						<input type="button" name="btnpustaka" id="btnpustaka" value="Pinjam" onClick="goPinjam();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
						<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam',700,500);">
						<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
						<? } ?>
						</td>
					</tr>
					<tr height="20"> 
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kembali</td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Perpanjang </td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">No. Induk</td>
						<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
						<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Batas Kembali</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Denda</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Lokasi</td>
					<?php
					$i = 0;

						if($_SESSION['keyword']){
							// mulai iterasi
							while ($rowp = $p_pinjam->FetchRow()) 
							{ 
								if ($i % 2) $rowstyle1 = 'NormalBG';  else $rowstyle1 = 'AlternateBG'; $i++; 
								if ($rowp['idtransaksi'] == '0') $rowstyle1 = 'YellowBG';
								$dendaperbuku = Sipus::hitungdendaperbuku($conn,$rowp['tgl_tenggat'],date('Y-m-d'),$rowp['idanggota'],$rowp['ideksemplar']);
					?>
					<tr class="<?= $rowstyle1 ?>" height="30" valign="middle"> 
						<td align="center">
							<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $rowp['idtransaksi']; ?>','<?= $rowp['noseri'] ?>','<?= $rowp['ideksemplar']?>');" class="link"><img src="images/kembalikan.png"></u>
						</td>
						<td align="center">
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $rowp['kdjenistransaksi']!='PJN' or $r_anggota['statusanggota']!=1 or $rowp['tgltransaksi']==$rowp['tgltenggat']) { ?>
						<img src="images/add2.png">
						<? } else { 
							$minperpanjangan = date('Y-m-d', strtotime('+1 days', strtotime($rowp['tgl_transaksi'])));
							
							// pengecekan tambahan : fach
							// 3 hari sebelum tgl batas kembali
							$datenow = date('Y-m-d');
							if ($datenow >= $minperpanjangan and $datenow <= $rowp['tgl_tenggat']) { ?>
								<img src="images/tombol/time_add.png" style="cursor:pointer" title="Perpanjang" onclick="goPerpanjang('<?= $rowp['ideksemplar']; ?>','<?= $rowp['idtransaksi']; ?>')" />
							<?php }else{
								echo '<br>tanggal perpanjangan <br> '.Helper::tglEng($minperpanjangan).' s/d '.Helper::tglEng($rowp['tgl_tenggat']);
							} 
						 } ?>
						</td>
						<td>&nbsp; <?= $rowp['noseri'] ?></td>
						<td align="left"><?= $rowp['judul'].($rowp['judulseri']!='' ? ' : '.$rowp['judulseri'] : '').($rowp['edisi']!='' ? " / ".$rowp['edisi'] : "") ?></td>
						<td align="left">&nbsp;<?= $rowp['authorfirst1'].' '.$rowp['authorlast1'] ?>
						<?php 
							$authPinjam = array();
							if(!empty($pengarang[$rowp['idpustaka']])){
								foreach($pengarang[$rowp['idpustaka']] as $a){
									$authPinjam[] = $a;
								}
								echo implode(", ",$authPinjam);
							}
						?>
						</td>
						<td align="center"><?= Helper::tglEngTime($rowp['tgltransaksi']); ?></td>
						<td align="center"><?
						if($rowp['tgltenggat']=='')
							echo "Maksimal";
						else {
							if($rowp['tgl_tenggat']<date('Y-m-d')) { 
								echo "<font color='red'><b>" ;?>
							<script>
							var strs='<?= Helper::tglEngTime($rowp['tgltenggat']) ?>';
							document.write(strs.blink());
							</script>			
						<? echo "</b></font>"; 
						} else 
						  echo Helper::tglEngTime($rowp['tgltenggat']) ; 
						 }?>
						</td>
						<?if($dendaperbuku==0){?>
							<td align="center"><font color="green"><b>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</b></font></td>
						<?}else{?>
							<td align="center"><font color="red"><b><blink>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</blink></b></font></td>
						<?}?>
						<td><?= $rowp['namalokasi'] ?></td>
					</tr>
					<?php
					}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Tidak ada peminjaman.</b></td>
					</tr>
					<?php }} ?>
					<tr>
						<td class="footBG" colspan="<?= $p_col; ?>">&nbsp;</td>
					</tr>
				</table>
				</div>
				
				<!-- ================================= pengembalian ============================ -->
				<div id="items"  style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Sedang Dipinjam [Pengembalian]</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle">
					<tr height="35" valign="center">
						<td colspan="<?= $p_col; ?>" class="LeftColumnBG thLeft">
						Masukkan No. Induk : &nbsp; 
						<input type="text" name="txtpustaka_kembali" id="txtpustaka_kembali" size="30" value="<?= $noseri ?>" onKeyDown="etrKembaliCepat(event);">&nbsp; &nbsp;
						
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $r_anggota['statusanggota']!=1) { ?>
						
						<input type="button" name="btnpustaka_kembali" id="btnpustaka_kembali" value="Kembali" class="buttonSmallDis" disabled style="height:23px;width:130px;font-size:12px;">
						<? }else {?>
						<input type="button" name="btnpustaka_kembali" id="btnpustaka_kembali" value="Kembali" onClick="goKembaliCepat();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
						<? } ?>
						</td>
					</tr>
					<tr height="20"> 
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kembali</td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Perpanjang </td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">No. Induk</td>
						<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
						<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Batas Kembali</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Denda</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Lokasi</td>
					<?php
					$i = 0;

						if($_SESSION['keyword']){
							// mulai iterasi
							while ($rowp = $p_kembalicepat->FetchRow()) 
							{
								if ($i % 2) $rowstyle1 = 'NormalBG';  else $rowstyle1 = 'AlternateBG'; $i++; 
								if ($rowp['idtransaksi'] == '0') $rowstyle1 = 'YellowBG';
								$dendaperbuku = Sipus::hitungdendaperbuku($conn,$rowp['tgl_tenggat'],date('Y-m-d'),$rowp['idanggota'],$rowp['ideksemplar']);
					?>
					<tr class="<?= $rowstyle1 ?>" height="30" valign="middle"> 
						<td align="center">
							<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $rowp['idtransaksi']; ?>','<?= $rowp['noseri'] ?>','<?= $rowp['ideksemplar']?>');" class="link"><img src="images/kembalikan.png"></u>
						</td>
						<td align="center">
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $rowp['kdjenistransaksi']!='PJN' or $r_anggota['statusanggota']!=1 or $rowp['tgltransaksi']==$rowp['tgltenggat']) { ?>
						<img src="images/add2.png">
						<? } else { 
							$minperpanjangan = date('Y-m-d', strtotime('+1 days', strtotime($rowp['tgl_transaksi'])));
							
							// pengecekan tambahan : fach
							// 3 hari sebelum tgl batas kembali
							$datenow = date('Y-m-d');
							if ($datenow >= $minperpanjangan and $datenow <= $rowp['tgl_tenggat']) { ?>
								<img src="images/tombol/time_add.png" style="cursor:pointer" title="Perpanjang" onclick="goPerpanjang('<?= $rowp['ideksemplar']; ?>','<?= $rowp['idtransaksi']; ?>')" />
							<?php }else{
								echo '<br>tanggal perpanjangan <br> '.Helper::tglEng($minperpanjangan).' s/d '.Helper::tglEng($rowp['tgl_tenggat']);
								}
						 } ?>
						</td>
						<td>&nbsp; <?= $rowp['noseri'] ?></td>
						<td align="left"><?= $rowp['judul'].($rowp['judulseri']!='' ? ' : '.$rowp['judulseri'] : '').($rowp['edisi']!='' ? " / ".$rowp['edisi'] : "") ?></td>
						<td align="left">&nbsp;<?= $rowp['authorfirst1'].' '.$rowp['authorlast1'] ?>
						<?php 
							$authKembali = array();
							if(!empty($pengarang[$rowp['idpustaka']])){
								foreach($pengarang[$rowp['idpustaka']] as $a){
									$authKembali[] = $a;
								}
								echo implode(", ",$authKembali);
							}
						?>
						</td>
						<td align="center"><?= Helper::tglEngTime($rowp['tgltransaksi']); ?></td>
						<td align="center"><?
						if($rowp['tgltenggat']=='')
							echo "Maksimal";
						else {
						
							if($rowp['tgl_tenggat']<date('Y-m-d')) { 
							echo "<font color='red'><b>" ;?>
							<script>
							var strs='<?= Helper::tglEngTime($rowp['tgltenggat']) ?>';
							document.write(strs.blink());
							</script>			
						<? echo "</b></font>"; 
						} else 
						  echo Helper::tglEngTime($rowp['tgltenggat']) ; 
						 }?>
						</td>
						<?if($dendaperbuku==0){?>
							<td align="center"><font color="green"><b>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</b></font></td>
						<?}else{?>
							<td align="center"><font color="red"><b><blink>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</blink></b></font></td>
						<?}?>
						<td><?= $rowp['namalokasi'] ?></td>
					</tr>
					<?php
					}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Tidak ada peminjaman.</b></td>
					</tr>
					<?php }} ?>
					<tr>
						<td class="footBG" colspan="<?= $p_col; ?>">&nbsp;</td>
					</tr>
				</table>
				</div>

				<!-- ================================= perpanjangan ============================ -->
				<div id="items"  style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Sedang Dipinjam [Perpanjangan]</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle">
					<tr height="35" valign="center">
						<td colspan="<?= $p_col; ?>" class="LeftColumnBG thLeft">
						Masukkan No. Induk : &nbsp; 
						<input type="text" name="txtpustaka_panjang" id="txtpustaka_panjang" size="30" value="<?= $noseri ?>" onKeyDown="etrPanjangCepat(event);">&nbsp; &nbsp;
						
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $r_anggota['statusanggota']!=1) { ?>
						
						<input type="button" name="btnpustaka_panjang" id="btnpustaka_panjang" value="Perpanjang" class="buttonSmallDis" disabled style="height:23px;width:130px;font-size:12px;">
						<? }else {?>
						<input type="button" name="btnpustaka_panjang" id="btnpustaka_panjang" value="Perpanjang" onClick="goPanjangCepat();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
						<? } ?>
						</td>
					</tr>
					<tr height="20"> 
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kembali</td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Perpanjang </td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">No. Induk</td>
						<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
						<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Batas Kembali</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Denda</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Lokasi</td>
					<?php
					$i = 0;

						if($_SESSION['keyword']){
							// mulai iterasi
							while ($rowp = $p_panjangcepat->FetchRow()) 
							{
								if ($i % 2) $rowstyle1 = 'NormalBG';  else $rowstyle1 = 'AlternateBG'; $i++; 
								if ($rowp['idtransaksi'] == '0') $rowstyle1 = 'YellowBG';
								$dendaperbuku = Sipus::hitungdendaperbuku($conn,$rowp['tgl_tenggat'],date('Y-m-d'),$rowp['idanggota'],$rowp['ideksemplar']);
					?>
					<tr class="<?= $rowstyle1 ?>" height="30" valign="middle"> 
						<td align="center">
							<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $rowp['idtransaksi']; ?>','<?= $rowp['noseri'] ?>','<?= $rowp['ideksemplar']?>');" class="link"><img src="images/kembalikan.png"></u>
						</td>
						<td align="center">
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $rowp['kdjenistransaksi']!='PJN' or $r_anggota['statusanggota']!=1 or $rowp['tgltransaksi']==$rowp['tgltenggat']) { ?>
						<img src="images/add2.png">
						<? } else { 
						$minperpanjangan = date('Y-m-d', strtotime('+1 days', strtotime($rowp['tgl_transaksi'])));
						
						// pengecekan tambahan : fach
						// 3 hari sebelum tgl batas kembali
						$datenow = date('Y-m-d');
						if ($datenow >= $minperpanjangan and $datenow <= $rowp['tgl_tenggat']) { ?>
							<img src="images/tombol/time_add.png" style="cursor:pointer" title="Perpanjang" onclick="goPerpanjang('<?= $rowp['ideksemplar']; ?>','<?= $rowp['idtransaksi']; ?>')" />
						<?php }else{
							echo '<br>tanggal perpanjangan <br> '.Helper::tglEng($minperpanjangan).' s/d '.Helper::tglEng($rowp['tgl_tenggat']);
						} 							
						 } ?>
						</td>
						<td>&nbsp; <?= $rowp['noseri'] ?></td>
						<td align="left"><?= $rowp['judul'].($rowp['judulseri']!='' ? ' : '.$rowp['judulseri'] : '').($rowp['edisi']!='' ? " / ".$rowp['edisi'] : "") ?></td>
						<td align="left">&nbsp;<?= $rowp['authorfirst1'].' '.$rowp['authorlast1'] ?>
						<?php 
							$authPanjang = array();
							if(!empty($pengarang[$rowp['idpustaka']])){
								foreach($pengarang[$rowp['idpustaka']] as $a){
									$authPanjang[] = $a;
								}
								echo implode(", ",$authPanjang);
							}
						?>
						</td>
						<td align="center"><?= Helper::tglEngTime($rowp['tgltransaksi']); ?></td>
						<td align="center"><?
						if($rowp['tgltenggat']=='')
							echo "Maksimal";
						else {
						
							if($rowp['tgl_tenggat']<date('Y-m-d')) { 
							echo "<font color='red'><b>" ;?>
							<script>
							var strs='<?= Helper::tglEngTime($rowp['tgltenggat']) ?>';
							document.write(strs.blink());
							</script>			
						<? echo "</b></font>"; 
						} else 
						  echo Helper::tglEngTime($rowp['tgltenggat']) ; 
						 }?>
						</td>
						<?if($dendaperbuku==0){?>
							<td align="center"><font color="green"><b>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</b></font></td>
						<?}else{?>
							<td align="center"><font color="red"><b><blink>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</blink></b></font></td>
						<?}?>
						<td><?= $rowp['namalokasi'] ?></td>
					</tr>
					<?php
					}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Tidak ada peminjaman.</b></td>
					</tr>
					<?php }} ?>
					<tr>
						<td class="footBG" colspan="<?= $p_col; ?>">&nbsp;</td>
					</tr>
				</table>
				</div>

				<!-- ================================= Fotocopy ============================ -->
				<div id="items"  style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Fotocopy</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle">
					<tr height="35" valign="center">
						<td colspan="8" class="LeftColumnBG thLeft">
						Masukkan No.Induk : &nbsp; 
						<input type="text" name="txtpustakafoto" id="txtpustakafoto" size="30" value="<?= $noseri ?>" onKeyDown="etrFotocopy(event);">&nbsp; &nbsp;
						
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $r_anggota['statusanggota']!=1) { ?>
						
						<input type="button" name="btnfc" id="btnfc" value="Pinjam Fotocopy" class="buttonSmallDis" disabled style="height:23px;width:130px;font-size:12px;">
						<? }else {?>
						<input type="button" name="btnfc" id="btnfc" value="Pinjam Fotocopy" onClick="goPinjamFoto();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
						<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam&code=f',700,500);">
						<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam&code=f',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
						<? } ?>
						</td>
					</tr>
					<tr height="20"> 
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kembali</td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Perpanjang </td>
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">No.Induk</td>
						<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
						<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Batas Kembali</td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Lokasi</td>
						<?php
						$i = 0;

						if($_SESSION['keyword']){	
							// mulai iterasi
							while ($rowf = $p_fotocopy->FetchRow()) 
							{
								if ($i % 2) $rowstyle1 = 'NormalBG';  else $rowstyle1 = 'AlternateBG'; $i++; 
								if ($rowf['idtransaksi'] == '0') $rowstyle1 = 'YellowBG';
								//$dendaperbuku = Sipus::hitungdendaperbuku($conn,$rowp['tgl_tenggat'],date('Y-m-d'),$rowp['idanggota'],$rowp['ideksemplar']);
					?>
					<tr class="<?= $rowstyle1 ?>" height="30" valign="middle"> 
						<td align="center">
							<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $rowf['idtransaksi']; ?>','<?= $rowf['noseri'] ?>','<?= $rowf['ideksemplar']?>');" class="link"><img src="images/kembalikan.png"></u>
						</td>
						<td align="center">
						<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $rowf['kdjenistransaksi']!='PJN' or $r_anggota['statusanggota']!=1 or $rowf['tgltransaksi']==$rowf['tgltenggat']) { ?>
						<img src="images/add2.png">
						<? } else { ?>
						<u title="Perpanjang Pinjaman" onclick="goPanjang('<?= $rowf['idtransaksi']; ?>','<?= $rowf['noseri']; ?>','<?= $rowf['ideksemplar']?>');" class="link"><img src="images/perpanjang.png"></u>
						<? } ?>
						</td>
						<td>&nbsp; <?= $rowf['noseri'] ?></td>
						<td align="left"><?= $rowf['judul'].($rowf['judulseri']!='' ? ' : '.$rowf['judulseri'] : '').($rowf['edisi']!='' ? " / ".$rowf['edisi'] : "") ?></td>
						<td align="left">&nbsp;<?= $rowf['authorfirst1'].' '.$rowf['authorlast1'] ?>
						<?php 
							$authFotocopy = array();
							if(!empty($pengarang[$rowf['idpustaka']])){
								foreach($pengarang[$rowf['idpustaka']] as $a){
									$authFotocopy[] = $a;
								}
								echo implode(", ",$authFotocopy);
							}
						?>
						</td>
						<td align="center"><?= Helper::tglEngTime($rowf['tgltransaksi']); ?></td>
						<td align="center"><?
						if($rowf['tgltenggat']=='')
							echo "Maksimal";
						else {
						
							if($rowf['tgltenggat']<date('Y-m-d')) { 
							echo "<font color='red'><b>" ;?>
							<script>
							var strs='<?= Helper::tglEngTime($rowf['tgltenggat']) ?>';
							document.write(strs.blink());
							</script>			
						<? echo "</b></font>"; 
						} else 
						  echo Helper::tglEngTime($rowf['tgltenggat']) ; 
						 }?>
						</td>
						<td><?= $rowf['namalokasi'] ?></td>
					</tr>
					<?php
					}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Tidak ada peminjaman.</b></td>
					</tr>
					<?php } }?>
					<tr>
						<td class="footBG" colspan="8">&nbsp;</td>
					</tr>
				</table>
				</div>

				<!-- ================================= transaksi reservasi============================ -->
				<div id="items" style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Reservasi</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" border="0" class="GridStyle">
					<tr height="35">
					<td colspan="6" class="LeftColumnBG thLeft">
					Masukkan No.Induk : &nbsp; <input type="text" name="txtreservasi" id="txtreservasi" size="30" onKeyDown="etrReserve(event);" >&nbsp; &nbsp;
					<? if(strtotime($r_anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $r_anggota['statusanggota']!=1) { ?>
					<input type="button" name="btnreserve" id="btnreserve" value="Pesan" class="buttonSmallDis" disabled style="height:23px;width:130px;font-size:12px;">
					<? } else {?>
					<input type="button" name="btnreserve" id="btnreserve" value="Pesan" onClick="goReservasi();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
					<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_eksreserve',700,500);">
					<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_eksreserve',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
					
					<? } ?>
					</td>
					</tr>
					<tr height="20"> 
						<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">Batal </td>
						<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">No.Induk</td>
						<td width="50%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
						<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pesan</td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Expired</td>
						
						<?php
						$m = 0;
						if($_SESSION['keyword']){
							// mulai iterasi
							while ($rowr = $p_reservasi->FetchRow()) 
							{
								if ($m % 2) $rowstyle2 = 'NormalBG';  else $rowstyle2 = 'AlternateBG'; $m++; 
								if ($rowr['idreservasi'] == '0') $rowstyle2 = 'YellowBG';
					?>
					<tr class="<?= $rowstyle2 ?>" height="20" valign="middle"> 
						<td align="center">
							<u title="Batal" onclick="goBatalR('<?= $rowr['idreservasi'] ?>','<?= $rowr['ideksemplarpesan'] ?>');" class="link">
								<img title="Batal memesan" src="images/batal.png">
							</u>
							<?php if($rowr['statuseksemplar'] == "ADA"){?>
							<img src="images/borrowed.png" width="20" title="Peminjaman Pustaka" onclick="goPinjamresev('<?=$rowr['idreservasi'];?>');" style="cursor:pointer">
							<?php } ?>
						</td>
						<td>&nbsp;<?= $rowr['noseri']; ?></td>			
						<td align="left"><?= $rowr['judul'].($rowr['edisi']!='' ? " / ".$rowr['edisi'] : "") ?></td>
						<td align="left">&nbsp;<?= $rowr['authorfirst1'].' '.$rowr['authorlast1'] ?>
						<?php 
							$authReservasi = array();
							if(!empty($pengarang[$rowr['idpustaka']])){
								foreach($pengarang[$rowr['idpustaka']] as $a){
									$authReservasi[] = $a;
								}
								echo implode(", ",$authReservasi);
							}
						?>
						</td>
						<td align="center"><?= Helper::tglEngTime($rowr['tglreservasi']); ?></td>
						<td align="center"><?= Helper::tglEngTime($rowr['tglexpired']); ?></td>
					
					</tr>
					<?php
						}

						if ($m==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Belum ada pemesanan yang aktif.</b></td>
					</tr>
					<?php }} ?>
					<tr>
						<td class="footBG" colspan="8">&nbsp;</td>
					</tr>
				</table>
				</div>

				<!-- ================================= Sejarah Transaksi============================ -->
				<div id="items" style="position:relative;top:-2px">
				<header style="width:100%;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1>Sejarah Peminjaman</h1>
						</div>
					</div>
				</header>
				<table cellspacing="0" width="100%" class="GridStyle">
					<tr height="20"> 
						<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft">NO INDUK</td>
						<td width="42%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>
						<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang </td>
						<td width="5%" nowrap align="center" class="SubHeaderBGAlt thLeft">Jenis Transaksi</td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Harus Kembali</td>
						<td width="13%" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Kembali</td>
						<td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft">Perpanjang Ke</td>
						
						<?php
						$a = 0;
							if($_SESSION['keyword']){
							// mulai iterasi
							while ($rows = $p_sejarah->FetchRow()) 
							{
								if ($a % 2) $rowstyle3 = 'NormalBG';  else $rowstyle3 = ''; $a++; 
								if ($rows['idtransaksi'] == '0') $rowstyle3 = 'YellowBG';
								
								
					?>
					<tr class="<?= $rowstyle3 ?>" height="20" valign="middle"> 
						<td><?= $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".$rows['noseri']."</font>" : $rows['noseri']; ?></u></td>
						<td align="left"><?=  $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".Helper::limitS($rows['judul'],53).($rows['edisi']!='' ? " / ".$rows['edisi'] : "")."</font>" : Helper::limitS($rows['judul'],53).($rows['edisi']!='' ? " / ".$rows['edisi'] : "") ?></td>
						<td align="left"><?= trim($rows['authorfirst1'].' '.$rows['authorlast1']) ?>
						<?php 
							$authSejarah = array();
							if(!empty($pengarang[$rows['idpustaka']])){
								foreach($pengarang[$rows['idpustaka']] as $a){
									$authSejarah[] = $a;
								}
								echo implode(", ",$authSejarah);
							}
						?>
						</td>
						<td align="center"><?= $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".$rows['kdjenistransaksi']."</font>" : $rows['kdjenistransaksi'] ?></td>
						<td align="center"><?= $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".(Helper::tglEngTime($rows['tgltransaksi']))."</font>" : Helper::tglEngTime($rows['tgltransaksi']); ?></td>
						<td align="center"><?= $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".($rows['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($rows['tgltenggat']))."</font>" : ($rows['tgltenggat']=='' ? "Maksimal" : Helper::tglEngTime($rows['tgltenggat'])); ?></td>
						<td >&nbsp;
						<? if($rows['tgltransaksi']==$rows['tgltenggat'])
								echo "<font color='blue'>";

						   if ($rows['statustransaksi']=='1')
							echo "Belum kembali";
							else
							echo  ($rows['statustransaksi']==2 ? Helper::tglEngTime($rows['tglpengembalian'])." *" : Helper::tglEngTime($rows['tglpengembalian'])); 
						
							if($rows['tgltransaksi']==$rows['tgltenggat'])
								echo "</font>";
						?></td>
						<td align="center"><?= $rows['tgltransaksi']==$rows['tgltenggat'] ? "<font color='blue'>".$rows['perpanjangke']."</font>" : $rows['perpanjangke']; ?></td>
						
					</tr>
					<? if($rows['tgltransaksi']==$rows['tgltenggat'])
						echo "</font>";
					?>
					<?php
						}
						if ($a==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Belum ada transaksi.</b></td>
					</tr>
					<?php } } ?>
					<tr>
						<td class="footBG" colspan="8">&nbsp;</td>
					</tr>
				</table>
				<br>
				<table align="center" cellspacing="3" cellpadding="2" width="370" border="0">
					<tr>
						<td width=270 align="center"><u>
						<script>
						var text="Sejarah transaksi diatas merupakan 15 transaksi terakhir";
						document.write(text.blink());
						</script>
						</u></td>
					</tr>
				</table>
				<table align="center" cellspacing="3" cellpadding="2" width="370" border="0">
					<tr>
						<td width=110><u>Keterangan </u></td>
						<td width=270>: &nbsp; </td>
					</tr>
					<tr>
						<td>Tanggal kembali </td>
						<td >: * Transaksi Perpanjangan Pustaka </td>
					</tr>
				</table>
				</div>

				<!-- ================================= transaksi peminjaman khusus============================ -->

					<div id="items" style="position:relative;top:-2px;">
					<table cellspacing="0" cellpadding="0" width="100%" border="0" class="GridStyle">
					<tr height="35" valign="center">
					<td colspan="5" class="LeftColumnBG" style="border-top:none;border-left:thin solid #999;border-right:thin solid #D7B46C">
					Masukkan NO INDUK : &nbsp; 
					<input type="text" name="txtpustaka2" id="txtpustaka2" size="30" onKeyDown="etrPinjamKhusus(event);">&nbsp; &nbsp;

					<input type="button" name="btnpustaka2" id="btnpustaka2" value="Pinjam Khusus" onClick="goPinjamKhusus();" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">&nbsp; 
					<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam&code=k',700,500);">
					<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam&code=k',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
					</td>

					</tr>
					</table>
				</div>
				<? } ?>

				</td>
				</tr>
				</table>
				
				<input type="hidden" name="key" id="key">
				<input type="hidden" name="key2" id="key2">
				<input type="hidden" name="key3" id="key3" value="<?= $_SESSION['tab'] ?>">
				<input type="hidden" name="ideks" id="ideks">
				<input type="hidden" name="keydate" id="keydate" value="<?= $_SESSION['waktu'] ?>">

				<input type="hidden" name="act" id="act">
				<input type="hidden" name="nilai" id="nilai">
				</form>

				<form name="test" id="test" target="_blank">
				<input type="hidden" name="test2" id="test2" value="<?= $_SESSION['reserve'] ?>">
				</form>
				</div>
		</div>
	</div>
</div>

</body>


<script language="javascript">
$(document).ready(function() {	
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	var key=document.getElementById("key3").value;
	
	if (key==0 || key == ''){
		chooseTab(0);
		document.getElementById('txtpustaka').focus();
	}
	else if (key==1){
		chooseTab(1);
		document.getElementById('txtpustaka_kembali').focus();
	}
	else if (key == 2){ 
		chooseTab(2);
		document.getElementById('txtpustaka_panjang').focus();
	}
	else if (key == 3){ 
		chooseTab(3);
		document.getElementById('txtpustakafoto').focus();
	}
	else if (key == 4){ 
		chooseTab(4);
		document.getElementById('txtreservasi').focus();
	}
	else if (key == 5){ 
		chooseTab(5);
	}	
	
});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>


<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script language="javascript">
function etrTrans(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btntrans").click();
}
function etrPinjam(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka").click();
		//goPinjam();
}

function etrKembaliCepat(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka_kembali").click();
		//goPinjam();
}
function etrPanjangCepat(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka_panjang").click();
		//goPinjam();
}

function goPanjangCepat() {
	if(cfHighlight("txtpustaka_panjang")){
		var yakin=confirm("Apakah Anda yakin akan melakukan perpanjangan pustaka ini ?");
		if(yakin){
			keys = document.getElementById("txtpustaka_panjang").value;
			document.getElementById("act").value='panjangcepat';
			document.getElementById("key2").value=keys;
			goSubmit();
		}
	}
}

function goKembaliCepat() {
	if(cfHighlight("txtpustaka_kembali")){
		var yakin=confirm("Apakah Anda yakin akan melakukan pengembalian pustaka ini ?");
		if(yakin){
			keys = document.getElementById("txtpustaka_kembali").value;
			document.getElementById("act").value='kembalicepat';
			document.getElementById("key2").value=keys;
			goSubmit();
		}
	}
}

function etrPinjamKhusus(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka2").click();
		//goPinjam();
}
function etrReserve(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnreserve").click();
		//goReservasi();
}

function etrFotocopy(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnfc").click();
		//goReservasi();
}
function goClear() {
	document.getElementById("act").value='delsession';
	goSubmit();

}

function goTrans() {
	var idanggota = $("#txtid").val();


	if(idanggota=='') {
		$("#span_error").show();
		setTimeout('$("#span_error").hide()',2000);
		return false; } 
	else {
		document.getElementById("act").value= 'stanby';
		goSubmit();
		}		
}

function goPinjam() {
	if(cfHighlight("txtpustaka")){
		var yakin=confirm("Apakah Anda yakin akan melakukan peminjaman pustaka ini ?");
		if(yakin){
			document.getElementById("act").value='pinjam';
			goSubmit();
		}
	}
		
}

function goPinjamresev(ideks) { 
	var pinjam=confirm("Apakah Anda yakin akan menambahkan peminjaman pustaka atas reservasi pustaka ini ?")
	if(pinjam){
		document.getElementById("act").value='pinjamreserve';
		document.getElementById("ideks").value=ideks;
		goSubmit();
	}
		
}

function goPinjamFoto() {
	if(cfHighlight("txtpustakafoto")){
		var yakin=confirm("Apakah Anda yakin akan melakukan peminjaman fotokopi pustaka ini ?");
		if(yakin){
			document.getElementById("act").value='pinjamfoto';
			goSubmit();
		}
	}
		
}

function goPinjamKhusus() {
	if(cfHighlight("txtpustaka2")){
		document.getElementById("act").value='pinjamkhusus';
		goSubmit();
	}		
}

function goBatal($key,$eks) {
		var batalno=confirm("Apakah Anda yakin akan membatalkan transaksi ini ?");
		if(batalno){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}

function goBatalR($key,$eks) {
		var batalR=confirm("Apakah Anda yakin akan membatalkan reservasi ini ?");
		if(batalR){
		document.getElementById("act").value='batalR';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
		
}

function goPanjang($key,$eks,$ideks) {
		var panjang=confirm("Apakah Anda yakin akan memperpanjang transaksi ini ?");
		if(panjang){
		document.getElementById("act").value='panjang';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		document.getElementById("ideks").value=$ideks;
		goSubmit();
		}
}

function goKembali($key,$eks,$ideks) {
		var kembalikan=confirm("Apakah Anda yakin akan melakukan proses pengembalian ?");
		if(kembalikan){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		document.getElementById("ideks").value=$ideks;
		goSubmit();
		}
}

function goReservasi(){
		document.getElementById("act").value='pesen';
		goSubmit();
}

function goTab(key3) { //alert(key3);
	if(key3=='0')	
		document.getElementById('txtpustaka').focus();
	else if(key3=='1'){
		document.getElementById('txtpustaka_kembali').focus();
	}else if(key3=='2')
		document.getElementById('txtpustaka_panjang').focus();
	else if(key3=='3')
		document.getElementById('txtpustakafoto').focus();
	else if(key3=='4')
		document.getElementById('txtreservasi').focus();
		
	document.getElementById("key3").value=key3;
}

function goFreeSkors(key,denda) {
	var free = confirm("Apakah anda yakin akan membayarkan denda Id Anggota "+key+" ?");
	if(free) {
		byr = document.getElementById("bayar").value;
		if(parseInt(byr) > parseInt(denda)){
			alert("Uang pembayaran lebih besar dari denda");
			return false;
		}
		
		if(byr){
			n = parseInt(denda) - parseInt(byr);
			document.getElementById("nilai").value = n;
		}

		document.getElementById("act").value = "freeskors";
		document.getElementById("key").value = key;
		goSubmit();
	}
}

function initPage() {
	//initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);

	var xid=document.getElementById("txtid").value;
	var x=document.getElementById("key3").value;
	if(xid=="")
	document.getElementById('txtid').focus();
	else if(x=='0')	
	document.getElementById('txtpustaka').focus();
	else if(x=='1')
	document.getElementById('txtpustaka_kembali').focus();
	else if(x=='2')
	document.getElementById('txtpustaka_panjang').focus();
	else if(x=='3')
	document.getElementById('txtpustakafoto').focus();
	else if(x=='4')
	document.getElementById('txtreservasi').focus();
	else
	document.getElementById('txtid').focus();
}
function chCol() {
	var selected = document.getElementById('khs').className;
	if (selected == 'selected'){
		document.getElementById('khs').firstChild.style.backgroundColor = "#383838";
		}
	else {
		document.getElementById('khs').firstChild.style.backgroundColor = "#000";
	}
}

function onlyNumber(e,elem,dec) {
	var code = e.keyCode || e.which;
	if ((code > 57 && code < 96) || code > 105 || code == 32) {
		if(code == 190 && dec) {
			if(elem.value == "") // belum ada isinya, titik tidak boleh didepan
				return false;
			if(elem.value.indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
				return false;
			return true;
		}
		return false;
	}
}

</script>
</html>
