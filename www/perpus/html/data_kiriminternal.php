<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/pengolahan.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
		
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_kirim = "";
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_historymaintaning';
	$p_window = '[PJB LIBRARY] Pengiriman Eksemplar Ke';
	$p_title = '.: Pengiriman Eksemplar Ke';
	$p_title1 = 'Data Pengiriman Eksemplar Ke';
	$p_titlelist = '.: Daftar Eksemplar :.';
	$p_tbheader = '.: Pengiriman Eksemplar Ke:.';
	$p_col = 9;
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_pengolahan.php');

	$p_id = "mspengadaan";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'ideksemplarolah';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	if (!empty($_POST))
	{
		$r_aksi= Helper::removeSpecial($_POST['act']);
		$r_tgl = Helper::removeSpecial($_POST['tglkirim']);
		$arrkey = $_POST['arrkey'];
		$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
		
		if($r_aksi == 'kirim' and $c_edit) {
			$rechis = array();
			$record = array();
			$record['kdlokasi'] = Helper::cStrNull($_POST['kdlokasi']);
			
			$err = Sipus::UpdateComplete($conn,$record,"pp_eksemplar"," noseri in ($arrkey)");	
			
			if($err == 0) {
				$lokasi = $conn->GetOne("select namalokasi from lv_lokasi where kdlokasi='$r_lokasi'");
	
				$rechis['tkhistory'] = 'K';
				$rechis['keterangan'] = 'Terima dari Pengolahan';
				$rechis['tglkirim'] = date('Y-m-d H:m:s');
				$rechis['npkkirim'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
				//$rechis['keterangan'] = 'Kirim ke Koleksi '.strtoupper($klasifikasi).' di '.strtoupper($lokasi);
				
				Pengolahan::insSentLocation($conn,$rechis,$arrkey);
				$sucdb = 'Penyimpanan Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);$parts = Explode('/', $_SERVER['PHP_SELF']);
			}
			else{
				$errdb = 'Penyimpanan Gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
		}
		
		if ($arrkey != ''){
			$sql = "select p.*,e.noseri as seri, m.judul, m.authorfirst1, m.authorlast1, m.authorfirst2, m.authorlast2, m.authorfirst3, m.authorlast3
				from pp_eksemplarolah p 
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
				left join ms_pustaka m on m.idpustaka=e.idpustaka 
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
				where stskirimpengolahan=1 and stskirimeksternal=0 
				and m.kdjenispustaka in ($_SESSION[roleakses]) and p.stspengolahan=1 and e.noseri in ($arrkey) order by ideksemplarolah";
				
			$rs = $conn->Execute($sql);
		}
	}
	
  	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi where internal=1 order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$r_lokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:180"');
	
	$rs_kirim = $conn->GetRow("select namaklasifikasi from lv_klasifikasi where kdklasifikasi='$r_kirim'");
	//echo $arrkey;
?>
<html>
<head>
	<title><?= $p_window." ".$rs_kirim['namaklasifikasi'] ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<table border="0" cellpadding="4" cellspacing="0">
			<!--tr height="20">
				<td align="center" colspan=4 class="PageTitle"><?= $p_title." ".$rs_kirim['namaklasifikasi']." ::."; ?></td>
			</tr-->
			<tr>
			<td align="center">
				<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar</span></a>
			</td>
			<? if ($c_edit){ ?>
			<td align="center">
				<a href="javascript:saveData();" class="buttonshort"><span class="save">Kirim</span></a>
			</td>
			<td align="center">
				<a href="javascript:goReset();" class="buttonshort"><span class="reset">Reset</span></a>
			</td>
			<? } ?>
			</tr>
			<tr>
				<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
			</tr>
		</table><br />
		<header style="width:650px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title." ".$rs_kirim['namaklasifikasi']." ::."; ?></h1>
				</div>
			</div>
		</header>
		<table cellpadding="4" cellspacing="0" width="<?= $p_tbwidth+50 ?>" class="GridStyle">
			<tr>
				<td align="center" class="SubHeaderBGAlt thLeft" colspan="4" width="<?= $p_tbwidth/2; ?>"><?= $p_title1." ".$rs_kirim['namaklasifikasi']; ?></td>
			</tr>
			<tr height="30">
				<td width="150" class="LeftColumnBG thLeft">Tgl. Pengiriman *</td>
				<td class="RightColumnBG" colspan="2"><input type="text" name="tglkirim" size="10" id="tglkirim" value="<?= $r_tgl=='' ? date('d-m-Y') : $r_tgl; ?>">
					<? if($c_edit) { ?>
					<img src="images/cal.png" id="i_tglkirim" style="cursor:pointer;" title="Pilih tanggal pengiriman">
					&nbsp;
					<script type="text/javascript">
					Calendar.setup({
						inputField     :    "tglkirim",
						ifFormat       :    "%d-%m-%Y",
						button         :    "i_tglkirim",
						align          :    "Br",
						singleClick    :    true
					});
					</script>
					
					<? } ?>
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Lokasi *</td>
				<td class="RightColumnBG" colspan="2"><em><?= $rs_kirim['namaklasifikasi']?></em> <?= $l_lokasi; ?></td>
			</tr>
			<tr>
				<td class="footBG" colspan="2">&nbsp;</td>
			</tr>
		</table>
		<br>
		<header style="width:943;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_titlelist; ?></h1>
				</div>
			</div>
		</header>
		<table width="900" cellpadding="4" cellspacing="0" border="0" class="GridStyle">
			<tr>
				<td class="SubHeaderBGAlt thLfet" colspan="2" align="center">No. Induk</td>
				<td class="SubHeaderBGAlt thLfet" align="center" nowrap>Judul</td>
				<? if ($c_edit){ ?>
				<td class="SubHeaderBGAlt thLfet" align="center">Aksi</td>
				<? } ?>
			</tr>
			<? if (!empty($arrkey)){ 
					$i=0;
					while ($rows = $rs->FetchRow()){
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center" colspan="2" width="60"><?= $rows['seri']; ?></td>
				<td align="left"><?= $rows['judul']; ?></td>
				<td align="center"><img src="images/tombol/delete.png" title="hapus" onClick="goDelete('<?= $rows['seri']; ?>')" style="cursor:pointer"></td>
			</tr>
			<? } ?>
			<tr id="tr_add">
				<td width="40"><?= UI::createTextBox('noseri','','ControlStyle',20,30,$c_edit); ?></td>
				<td width="20"><img src="images/tombol/breakdown.png" id="btnusulan" title="Cari Usulan" style="cursor:pointer" onClick="openLOV('btnusulan', 'eksemplarolah',-100, 20,'addEksemplar',800,'','<?= str_replace("'","",$arrkey); ?>#<?= $r_kirim?>')"></td>
				<td width="100"><?= UI::createTextBox('jdl','','ControlRead',100,100,$c_edit,'readonly'); ?></td>
				<td><input type="button" name="tambah" id="tambah" value="Tambah" onClick="addKirim();"></td>
			</tr>
			<? }else{ ?>
			<tr id="tr_add">
				<td width="40"><?= UI::createTextBox('noseri','','ControlStyle',20,30,$c_edit); ?></td>
				<td width="20"><img src="images/tombol/breakdown.png" id="btnusulan" title="Cari Usulan" style="cursor:pointer" onClick="openLOV('btnusulan', 'eksemplarolah',-100, 20,'addEksemplar',800,'','#<?= $r_kirim?>')"></td>
				<td width="100"><?= UI::createTextBox('jdl','','ControlRead',100,100,$c_edit,'readonly'); ?></td>
				<td><input type="button" name="tambah" id="tambah" value="Tambah" onClick="addKirim();"></td>
			</tr>
			<? } ?>
			<tr>
				<td class="footBG" colspan="4">&nbsp;</td>
			</tr>
		</table>
		<input type="hidden" name="arrkey" id="arrkey" value="<?= $arrkey; ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="kdklasifikasi" id="kdklasifikasi" value="<?= $r_kirim?>">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

function saveData(){
	if(cfHighlight("kdlokasi,tglkirim")){
		var arr=document.getElementById('arrkey').value;
		if(arr=='')
			alert('Masukkan Eksemplar dengan benar');
		else {
		$("#act").val("kirim");
		goSubmit();
		}
	}
}

/* function addEksemplar(noseri, judul) {
		$("#noseri").val(noseri);
		$("#jdl").val(judul);
} */

function addEksemplar(elem) {
		$("#noseri").val($(elem).attr("seri"));
		$("#jdl").val($(elem).attr("judul"));
}

function addKirim()
{
	if(cfHighlight("noseri,kdlokasi,tglkirim")){
		if ($("#arrkey").val() != '')
			$("#arrkey").val($("#arrkey").val()+",'"+$("#noseri").val()+"'");
		else
			$("#arrkey").val("'"+$("#noseri").val()+"'");
		goSubmit();
	}
}

function goDelete(row){
	var arr = $("#arrkey").val();
	$("#arrkey").val(arr.replace(row,""));
	goSubmit();
}

function goReset(){
	$("#arrkey").val('');
	$("#kdlokasi").val('');
	goSubmit();
}

</script>
</html>