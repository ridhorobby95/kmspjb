<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	
	$r_kondisi = Helper::removeSpecial($_POST['kdkondisi']);
	
	if($r_format=='' or $r_kondisi=='') {
		header("location: index.php?page=home");
	}
	
	//$conn->debug = true;
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Jenis Pustaka';
	
	$p_namafile = 'rekapjpus_'.$r_kondisi;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//sql jumlah "judul" per "jenis pustaka"
	$sql = "select count(distinct(e.idpustaka)) as jml_judul, p.kdjenispustaka, s.namajenispustaka, e.kdkondisi, k.namakondisi
			from pp_eksemplar e
			JOIN ms_pustaka p ON p.idpustaka=e.idpustaka
			join lv_jenispustaka s on s.kdjenispustaka = p.kdjenispustaka
			join lv_kondisi k on k.kdkondisi = e.kdkondisi where 1=1
			";	
			
	if ($r_kondisi!=''){
		$sql .=" and e.kdkondisi = '$r_kondisi'";
	}
	
	$sql .=" group by p.kdjenispustaka, e.kdkondisi, s.namajenispustaka, k.namakondisi
			 order by s.namajenispustaka";
	$row = $conn->Execute($sql);
	$rs_count=$row->RowCount();
	$j=0;
	while($rs_sql=$row->FetchRow())
	{
		$j++;
		$jmljp[$j]=$rs_sql['jml_judul']; 
		$namajp[$j]=$rs_sql['namajenispustaka'];
		$namakd[$j]=$rs_sql['namakondisi'];
		
		$kdjp[$j] = $rs_sql['kdjenispustaka'];
		$kdkd[$j] = $rs_sql['kdkondisi'];
	}
	
	$sql2 = "select count(e.ideksemplar) as jumlah, e.kdklasifikasi, p.kdjenispustaka, j.namajenispustaka, e.kdkondisi 
			from pp_eksemplar e 
			left join ms_pustaka p on p.idpustaka=e.idpustaka 
			left join lv_jenispustaka j on  j.kdjenispustaka=p.kdjenispustaka
			where 1=1 ";
	if ($r_kondisi!=''){
		$sql2 .=" and e.kdkondisi = '$r_kondisi'";
	}
	$sql2 .=" group by p.kdjenispustaka,  j.namajenispustaka, e.kdklasifikasi, e.kdkondisi ";
	
	$rs_sql2 = $conn->Execute($sql2);
	while(!$rs_sql2->EOF){
		$data[$rs_sql2->fields["kdjenispustaka"]][$rs_sql2->fields["kdkondisi"]][$rs_sql2->fields["kdklasifikasi"]] = $rs_sql2->fields["jumlah"]; 
		
		$datatotal[$rs_sql2->fields["kdjenispustaka"]][$rs_sql2->fields["kdkondisi"]] = $datatotal[$rs_sql2->fields["kdjenispustaka"]][$rs_sql2->fields["kdkondisi"]] + $data[$rs_sql2->fields["kdjenispustaka"]][$rs_sql2->fields["kdkondisi"]][$rs_sql2->fields["kdklasifikasi"]]; 
		
		$rs_sql2->MoveNext();	
	}
	$rsj = $conn->getRow("select namakondisi from lv_kondisi where kdkondisi='$r_kondisi'");

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Rekap Jenis Pustaka</h2>
  	</strong></td>
  </tr>
	<tr>
	<td><b>Jenis Kondisi : <?= $r_kondisi=='semua' ? "Semua Kondisi" : $rsj['namakondisi'] ?></b></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="110" align="center" rowspan="2"><strong>Jenis Pustaka</strong></th>
	<th width="118" align="center" rowspan="2"><strong>Kondisi Pustaka</strong></th>
	<th width="118" align="center" rowspan="2"><strong>Jumlah Judul</strong></th>
	<th align="center" colspan="4"><strong>Jumlah Eksemplar</strong></th>
  </tr>
  <tr>
  	<th width="99">LH</th>
	<th width="103">LM</th>
	<th width="90">LT</th>
	<th width="117">Total</th>
  </tr>
  
  
  <?php
	if($rs_count>0){
		for($i=1;$i<=$rs_count;$i++)
		{ 
	?>
		<tr height=25>
			<td align="left"><?= $namajp[$i];?></td>
			<td align="left"><?= $namakd[$i];?></td>
			<td align="left"><?= number_format($jmljp[$i],0);?></td>
			
			<td align="right"><?= number_format(($data[$kdjp[$i]][$kdkd[$i]]['LH']),0);?></td>
			<td align="right"><?= number_format(($data[$kdjp[$i]][$kdkd[$i]]['LM']),0);?></td>
			<td align="right"><?= number_format(($data[$kdjp[$i]][$kdkd[$i]]['LT']),0);?></td>
			<td align="right"><?= number_format(($datatotal[$kdjp[$i]][$kdkd[$i]]),0);?></td>
	<?
		}
	?>
		<tr>

	<?
	//}if($item==0) { 
	}if($rs_count==0) {
	?>
	<tr height=25>
		<td align="center" colspan="6" >Tidak ada pustaka</td>
	</tr>
	<? } ?>
</table>
<br>

</div>
</body>
</html>