<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Sirkulasi Pustaka';
	
	$p_namafile = 'rekap_sirkulasi'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select t.idanggota,a.namaanggota,t.tgltransaksi,j.namasatker as namajurusan
		from pp_transaksi t
		join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
		join ms_anggota a on t.idanggota=a.idanggota
		left join ms_satker j on j.kdsatker=a.idunit
		where to_date(to_char(tgltransaksi,'YYYY-mm-dd'),'YYYY-mm-dd')
			between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd') $sqlAdminUnit ";
	
	if($r_lokasi)
		$sql .=" and e.kdlokasi = '$r_lokasi' ";
	
	$sql .= "group by t.idanggota,a.namaanggota,t.tgltransaksi,j.namasatker 
		order by t.tgltransaksi";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Detil Peminjam Pustaka</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="20" nowrap align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Id Anggota</strong></th>
    <th width="200" align="center"><strong>Nama Anggota</strong></th>
    <th width="200" align="center"><strong>Unit</strong></th>
	<th width="100" align="center"><strong>Tanggal Transaksi</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td width="20" nowrap align="center"><?= $no ?></td>
    <td align="left"><?= $rs['idanggota'] ?></td>
	<td ><?= $rs['namaanggota'] ?></td>
	<td ><?= $rs['namajurusan'] ?></td>
	<td align="center"><?= Helper::tglEngTime($rs['tgltransaksi']) ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=5><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>