<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_idanggota = Helper::removeSpecial($_POST['idanggota']);
	$r_lama = Helper::removeSpecial($_POST['lama']);
	$r_anggota = Helper::removeSpecial($_POST['kdjenisanggota']);
	$r_jenis = Helper::removeSpecial($_POST['jenis']);
	$r_idunit = Helper::removeSpecial($_POST['idunit']);
	$r_fil = Helper::removeSpecial($_POST['fil']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Keterlambatan';
	$p_width = '1200';
	
	$p_namafile = 'rekap_penagihan'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select e.idpustaka, current_date, to_char(t.tgltenggat,'dd-mm-yyyy') tenggat, to_char(t.tgltransaksi,'dd-mm-yyyy') ttransaksi,
			trunc(sysdate) - to_date(to_char(t.tgltenggat, 'dd-mm-yyyy'), 'dd-mm-yyyy') AS selisih_hari,
			to_number(round( sysdate - to_date(to_char(t.tgltenggat,'dd-mm-yyyy'), 'dd-mm-yyyy') ) / 365 ) selisih_tahun,
			MONTHS_BETWEEN (TO_DATE (TO_CHAR(CURRENT_DATE, 'yyyy/mm/dd'), 'yyyy/mm/dd'), TO_DATE (TO_CHAR(t.tgltenggat, 'yyyy/mm/dd'), 'yyyy/mm/dd') ) selisih_bulan,
			a.namaanggota, a.idanggota, s.namasatker, a.idunit, j.namajenisanggota, a.kdjenisanggota, e.noseri, p.judul,
			p.authorfirst1, p.authorlast1, p.edisi, p.nopanggil, t.tgltransaksi     
		from pp_transaksi t
		left join ms_anggota a on a.idanggota = t.idanggota
		join pp_eksemplar e on e.ideksemplar = t.ideksemplar
		join ms_pustaka p on p.idpustaka = e.idpustaka 
		left join ms_satker s on s.kdsatker = a.idunit 
		join lv_jenisanggota j on j.kdjenisanggota = a.kdjenisanggota 
		where t.tglpengembalian is null and t.kdjenistransaksi='PJN' ";
		
	if($r_lokasi)
		$sql .=" and e.kdlokasi = '$r_lokasi' ";
		
	if($r_idanggota)
		$sql .=" and a.idanggota ='$r_idanggota' ";
	
	if($r_anggota!='')
		$sql .=" and a.kdjenisanggota = '$r_anggota' ";
	
	if ($r_jenis=='H' and $r_lama)
		$sql .=" and trunc(sysdate) - to_date(to_char(t.tgltenggat, 'dd-mm-yyyy'), 'dd-mm-yyyy') $r_fil $r_lama ";
	elseif($r_jenis=='B' and $r_lama)
		$sql .=" and MONTHS_BETWEEN (TO_DATE (TO_CHAR(CURRENT_DATE, 'yyyy/mm/dd'), 'yyyy/mm/dd'), TO_DATE (TO_CHAR(t.tgltenggat, 'yyyy/mm/dd'), 'yyyy/mm/dd') ) $r_fil $r_lama ";
	elseif($r_jenis=='T' and $r_lama)
		$sql .=" and datepart('year', age(t.tgltenggat)) $r_fil $r_lama ";
	else
		$sql .=" and (trunc(sysdate) - to_date(to_char(t.tgltenggat, 'dd-mm-yyyy'), 'dd-mm-yyyy')) >= 30 ";
		
	if($r_idunit)
		$sql .= " and a.idunit = '$r_idunit' ";
		
	$sql .=" order by s.namasatker, a.namaanggota ";

	$rs = $conn->GetArray($sql);

	foreach($rs as $row){
		if($ida == $row['idanggota']){
			$colspan_a[$row['idanggota']] = $colspan_a[$row['idanggota']] + 1;
		}else{
			$ida = $row['idanggota'];
			$colspan_a[$row['idanggota']] = 1;
		}
		
		if($idu == $row['idunit']){
			$colspan_u[$row['idunit']] = $colspan_u[$row['idunit']] + 1;
		}else{
			$idu = $row['idunit'];
			$colspan_u[$row['idunit']] = 1;
		}
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" >

<div align="center">
<table width="<?=$p_width;?>">
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom" colspan=10><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width="<?=$p_width;?>" cellpadding="2" cellspacing="0" border=0>
	<tr>
		<td align="center" colspan=11>
		<strong>
			<h2>LAPORAN KETERLAMBATAN</h2>
		</strong>
		</td>
	</tr>
	<tr>
		<td align="right" colspan=11>Tanggal Cetak : <?= Helper::tglEng(date('Y-m-d')) ?></td>
	</tr>
  
</table>
<table width="<?=$p_width;?>" border="1" cellpadding="2" cellspacing="0">

	<tr height=25>
	      <th width="15%" align="center"><strong>Unit</strong></th>
	      <th width="7%" align="center"><strong>Id Anggota</strong></th>
	      <th width="10%" align="center"><strong>Nama</strong></th>
	      <th width="3%" align="center"><strong>No.</strong></th>
	      <th width="7%" align="center"><strong>No. Induk</strong></th>
	      <th width="20%" align="center"><strong>Judul</strong></th>
	      <th width="10%" align="center"><strong>Author</strong></th>
	      <th width="7%" align="center"><strong>No Panggil</strong></th>
	      <th width="7%" align="center"><strong>Tgl Pinjam</strong></th>
	      <th width="7%" align="center"><strong>Tgl Tenggat</strong></th>
	      <th width="7%" align="center"><strong>Telat (<?=($r_jenis=="H"?"hari":($r_jenis=="B"?"bulan":($r_jenis=="T"?"tahun":"hari")));?>)</strong></th>
	</tr>
       <?php
	$n=0;

	foreach($rs as $row){$n++;
		if($ida == $row['idanggota']){
			$cols_a = $cols_a + 1;
		}else{
			$ida = $row['idanggota'];
			$cols_a = 1;
		}
		
		if($idu == $row['idunit']){
			$cols_u = $cols_u + 1;
		}else{
			$idu = $row['idunit'];
			$cols_u = 1;
		}
	?>
	<tr height=25 valign="top">
		<? if($cols_u == 1){ ?>
			<td rowspan="<?=$colspan_u[$row['idunit']];?>" align="left"><strong><?= $row['idunit']."<br/>".$row['namasatker'] ?></strong></td>
		<? } ?>
		
		<? if($cols_a == 1){ ?>
			<td rowspan="<?=$colspan_a[$row['idanggota']];?>" align="left"><strong><?= $row['idanggota'] ?></strong></td>
			<td rowspan="<?=$colspan_a[$row['idanggota']];?>" align="left"><strong><?= $row['namaanggota'] ?></strong></td>
		<? } ?>
		<td align="center"><?= $n ?></td>
		<td align="left"><?= $row['noseri'] ?></td>
		<td align="left"><?= $row['judul'] ?></td>
		<td align="left"><?= $row['authorfirst1']." ".$row['authorlast1'] ?></td>
		<td align="left"><?= $row['nopanggil'] ?></td>
		<td align="left"><?= $row['ttransaksi'] ?></td>
		<td align="left"><?= $row['tenggat'] ?></td>
		<td align="right"><?= ($r_jenis=="H"?$row['selisih_hari']:($r_jenis=="B"?$row['selisih_bulan']:($r_jenis=="T"?$row['selisih_tahun']:$row['selisih_hari']))); ?></td>
	</tr>
	<?  } ?>
	<? if($n==0) { ?>
	<tr height=25>
		<td align="center" colspan="11" >Tidak Ada Data</td>
	</tr>
	<? } ?>
</table>
<br/><br/><br/>

</div>
</body>
</html>
