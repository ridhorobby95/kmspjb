<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];


	// definisi variabel halaman
	$p_dbtable = 'lv_roleaksespustaka';
	$p_window = '[PJB LIBRARY] Daftar Role User Petugas';
	$p_title = 'Daftar Role User Petugas';
	$p_tbheader = '.: Daftar Role User Petugas :.';
	$p_col = 3;
	$p_tbwidth = 500;
	$p_id = "ms_role";
	$p_row = 15;
	
	$sql = "select * from ( ";
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select r.*,j.namajenispustaka, Row_Number() OVER (order by r.rolename, r.kdjenispustaka) linenum from $p_dbtable r 
				 join lv_jenispustaka j on r.kdjenispustaka=j.kdjenispustaka
				 where 1=1";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2 = Helper::removeSpecial($_POST['key2']);
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['rolename'] = Helper::cStrNull($_POST['i_username']);
			$record['kdjenispustaka'] = Helper::cStrNull($_POST['i_kdjenispustaka']);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['rolename'] = Helper::cStrNull($_POST['u_username']);
			$record['kdjenispustaka'] = Helper::cStrNull($_POST['u_kdjenispustaka']);
			
			
			$err=Sipus::UpdateSpecial($conn,$record,$p_dbtable,rolename,$r_key,kdjenispustaka,$r_key2);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteComplete($conn,$p_dbtable,"where rolename='$r_key' and kdjenispustaka='$r_key2'");
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key.','.$r_key2;
		}
		
		$keyrole=Helper::removeSpecial($_POST['carirole']);
		if($keyrole !='')
		$p_sqlstr .=" and rolename='$keyrole' ";
	
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr .=" order by r.rolename, r.kdjenispustaka";

	$sql .= $p_sqlstr." ) where linenum between $p_page and $p_row ";	
	
	//$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rs = $conn->Execute($sql);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}

	if($c_edit){
		$rs_cb1 = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka order by kdjenispustaka");
		$rs_cb2 = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka order by kdjenispustaka");
		//$l_jenis2 = $rs_cb2->GetMenu2('u_kdjenispustaka',$row['rolename'],true,false,0,'id="u_kdjenispustaka" class="ControlStyle" style="width:120"');
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="40">
				<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
			</tr>
		</table-->
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable" style="width:<?= $p_tbwidth ?>px">
		<table width="100%" border="0">
			<tr>
				<td align="left" colspan="2">User Name : &nbsp;<input type="text" name="carirole" id="carirole" size="25" value="<?= $keyrole ?>" onKeyDown="etrCari(event);"></td>
				<!--td align="right" ><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td-->
				<td align="right" >
					<input type="button" value="Cari" class="ControlStyle" onClick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onClick="goRefresh();">
				</td>
			</tr>
		</table>
		</div>
		<br />
		<header style="width:<?= $p_tbwidth ?>px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/keanggotaan.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>px" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			<tr height="20"> 
				<td width="120" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">User Name</td>
				<td width ="90%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Jenis Pustaka</td>
				<? if ($c_edit or $c_delete) { ?>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
				<? } ?>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['rolename'].','.$row['kdjenispustaka'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditRole('<?= $row['rolename']; ?>','<?= $row['kdjenispustaka'] ?>');" class="link"><?= $row['rolename']; ?></u>
				<? } else { ?>
					<?= $row['rolename']; ?>
				<? } ?>
				</td>
				<td><?= $row['namajenispustaka']; ?></td>
				<? if($c_delete or $c_edit) { ?>
				<td align="center">
				
					<u title="Hapus Data" onclick="goDeleteRole('<?= $row['rolename']; ?>','<?= $row['kdjenispustaka']; ?>');" class="link"><img src="images/deletes.png" width="14" /></u>
				
				</td>
				<? } ?>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center">
					<?= UI::createTextBox('u_username',$row['rolename'],'ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?>
					<img src="images/tombol/breakdown.png" id="btnPetugasU" title="Cari Petugas" style="cursor:pointer" onClick="openLOV('btnPetugasU', 'petugas',-100, 20,'updatePetugas',800)">
				</td>
				<td align="center"><?= $l_jenis2 = $rs_cb2->GetMenu2('u_kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="u_kdjenispustaka" class="ControlStyle" style="width:120" onKeyDown="etrUpdate(event);"');  ?></td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['rolename']; ?>','<?= $row['kdjenispustaka'] ?>');" class="link"><img src="images/updates.png" /></u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 		
				<td align="center">
					<?= UI::createTextBox('i_username','','ControlStyle',20,10,true,'onKeyDown="etrInsert(event);"'); ?>
					<img src="images/tombol/breakdown.png" id="btnPetugas" title="Cari Petugas" style="cursor:pointer" onClick="openLOV('btnPetugas', 'petugas',-100, 20,'addPetugas',800)">
				</td>
				<td align="center"><?= $l_jenis = $rs_cb1->GetMenu2('i_kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="i_kdjenispustaka" onKeyDown="etrInsert(event);" class="ControlStyle" style="width:120"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="4"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
        
        <?php require_once('inc_listnav.php'); ?>
        <br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key2" id="key2">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_username').focus();
	<? } else { ?>
	document.getElementById('i_username').focus();
	<? } ?>
}

function goEditRole(key1,key2){
	document.getElementById('key').value=key1;
	document.getElementById('key2').value=key2;
	document.getElementById('act').value='sunting';
	goSubmit();
}

function insertData() {
	if(cfHighlight("i_username,i_kdjenispustaka")){
		goInsertIP();
		}
}

function updateData(key1,key2) {
	if(cfHighlight("u_username,u_kdjenispustaka")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		document.getElementById('key').value=key1;
		document.getElementById('key2').value=key2;
		document.getElementById('act').value='update';
		goSubmit();
	}
}

function goDeleteRole(key1,key2){
	var del=confirm('Apakah Anda yakin akan menghapus role '+key1+' '+key2+' ?');
	if(del){
		document.getElementById('key').value=key1;
		document.getElementById('key2').value=key2;
		document.getElementById('act').value='hapus';
		goSubmit();
	}
}

function addPetugas(idpetugas) {
	$("#i_username").val(idpetugas);
}

function updatePetugas(idpetugas) {
	$("#u_username").val(idpetugas);
}

function goRefresh(){
document.getElementById("carirole").value='';
goSubmit();

}
</script>
</html>