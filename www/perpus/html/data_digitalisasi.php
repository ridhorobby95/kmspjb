<?php
	set_time_limit(0);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/pdf.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$rkey = Helper::removeSpecial($_REQUEST['key2']);

	$connLamp = Factory::getConnLamp();
	//$connLamp->debug = true;
	$cek = $connLamp->IsConnected();
	
	// definisi variabel halaman
	$p_dbtable = 'pp_orderpustakattb';
	$p_window = '[PJB LIBRARY] Data Digitalisasi Pustaka';
	$p_title = 'Data Digitalisasi Pustaka';
	$p_tbwidth = 750;
	$p_filelist = Helper::navAddress('list_digitalisasi.php');
	
	if($rkey != '') {
	    $p_foto = Config::dirFoto.'pustaka/'.trim($rkey).'.jpg';
	    $p_hfoto = Config::fotoUrl.'pustaka/'.trim($rkey).'.jpg';
	    $p_sinopsis = "/var/www/html/perpus/";
	}
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		if($r_aksi == 'simpan' and $c_edit) {
			$errfile = "";
			$record = array();
			$seri=Helper::removeSpecial($_POST['noseri']);
			$judul=Helper::removeSpecial($_POST['judul']);
			$conn->StartTrans();
			$maxfile = Helper::removeSpecial($_POST['theValue']);
			$sinfile=Sipus::UploadAbstrak($judul,$seri);
			if ($sinfile!=''){
				$record['sinopsis_upload']=$sinfile;
			}

			Sipus::UpdateBiasa($conn,$record,ms_pustaka,idpustaka,$rkey);
			//$seri = $conn->GetOne("select noseri from ms_pustaka where idpustaka = $rkey");
			
			// Upload Sinopsis ke BLOB datatype
			$id_sinopsis = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 2 and idlampiran = $rkey ");
			$recordsi = array();
			$recordsi['idlampiran'] = $rkey;
			$recordsi['jenis'] = '2'; # 1= cover, 2=sinopsis, 3=file pustaka 4=foto umum
			$recordsi['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
			$recordsi['filename'] = $_FILES['uploadsinopsis']['name']; 
			
			$content = $_FILES['uploadsinopsis']['tmp_name'];
			if($content) {
				$tmp_file = $_FILES['uploadsinopsis']['tmp_name'];
				$filetype = $_FILES['uploadsinopsis']['type'];
				$filesize = $_FILES['uploadsinopsis']['size'];
				$filename = $_FILES['uploadsinopsis']['name'];
				$s_typefile = false;
				$s_sizefile = false;
				if ($filesize < 5000000){
					$s_sizefile = true;
					if ($filetype=='application/pdf') {
						$s_typefile = true;
					}else
						$errfile .= 'Format file sinopsis tidak dikenali.[Bukan .pdf]';	
				}else {
					$errfile .= 'Besar File Melebihi 5 MB.';	
				}
				
				if($s_typefile and $s_sizefile){
					if($id_sinopsis){
						$err = Sipus::UpdateBlob($connLamp,$recordsi,'lampiranperpus',$content,'idlampiranperpus','isifile',$id_sinopsis);
					}else{
						$err = Sipus::InsertBlob($connLamp,$recordsi,'lampiranperpus',$content,'idlampiranperpus','isifile');
					}
				}else
					$errfile .= '<br/>';
			}
						
			#FILE UPLOAD
			for ($i = 0; $i <= $maxfile; $i++)
			{
				$r_file = array();
				$p_tmp_file = $_FILES['userfile']['tmp_name'][$i];
				$p_filetype = $_FILES['userfile']['type'][$i];
				$p_filesize = $_FILES['userfile']['size'][$i];
				$filename = $_FILES['userfile']['name'][$i];
				$fke = $i + 1;
				if($p_tmp_file){
					$ext=explode(".",$filename);
					$extjum=count($ext);
					$eksten=$ext[$extjum-1]; 
					$fileup=Helper::removeSpecial($filename);
					$directori = 'uploads/file/' . $seri;
					$p_sizefile = false;
					$p_typefile = false;
					if ($p_filesize < 10000000){
						$p_sizefile = true;
						if ($p_filetype=='application/pdf') {
							$p_typefile = true;
						}else
							$errfile .= 'Format file pustaka ke-'.$fke.' tidak dikenali.[Bukan .pdf]';	
					}else {
						$errfile .= 'Besar File pustaka ke-'.$fke.' Melebihi 10 MB.';	
					}
				
					if($p_sizefile and $p_typefile){
						if(!is_dir($directori))
							mkdir($directori,0775);
							
						$destination = $directori .'/'.$fileup;
						
						if (copy($p_tmp_file,$destination))
						{					
							$r_file['files']=Helper::cStrNull($destination);
						}					
						
						$r_file['idpustaka'] = $rkey;
						$r_file['login'] = (Helper::removeSpecial($_POST['isLogin'][$i]) == "on" ? "1" : "0");
						$r_file['download'] = (Helper::removeSpecial($_POST['isDownload'][$i]) == "on" ? "1" : "0");
						Helper::Identitas($r_file);
						if(!empty($r_file['files']))
							Sipus::InsertBiasa($conn,$r_file,pustaka_file);
						
						$idpustakafile = $conn->GetOne("select idpustakafile from pustaka_file where idpustaka = $r_key and files = '".$r_file['files']."' ");
							
						// Upload File Pustaka ke BLOB datatype
						$id_filepus = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 3 and idlampiran = $idpustakafile ");
						$recordfp = array();
						$recordfp['idlampiran'] = $idpustakafile;
						$recordfp['jenis'] = '3'; # 1= cover, 2=sinopsis, 3=file pustaka, 4 = foto umum
						$recordfp['tiduser'] = '1'; #test
						$recordfp['filename'] = $fileup; 
						
						$content = $_FILES['userfile']['tmp_name'][$i];

						if($content){
							if($id_filepus){
								$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$content,'idlampiranperpus','isifile');
							}else{
								$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$content,'idlampiranperpus','isifile');
							}
						}
					}else
						$errfile .= '<br/>';
				}
			}
			
			$recdig = array();
			$recdig['tgldigitalisasi']=date('Y-m-d');
			$recdig['npkdigitalisasi']=$_SESSION['PERPUS_USER'];
			$recdig['stsdigitalisasi']=1;
			Sipus::UpdateComplete($conn,$recdig,'pp_orderpustakattb',"idorderpustakattb=$r_key");
			
			$conn->CompleteTrans();
				
			if ($conn->ErrorNo() == 0){
				if($errfile){
					$errdb = "<br/>".$errfile;
					Helper::setFlashData('errdb', $errdb);
				}else{
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$rkey = $rkey;
					
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key.'&key2='.$rkey;
					Helper::redirect($url);
				}
			} else {
				$errdb = 'Penyimpanan data gagal.';
				$errdb .= "<br/>".$errfile;
				Helper::setFlashData('errdb', $errdb);
			} 
		}
		else if($r_aksi == "delsin" and $c_delete){
			$file=Helper::removeSpecial($_POST['delfile']);
			if($file){
				unlink($p_sinopsis.$file);
			}
	
			$recsin['sinopsis_upload']='';
			Sipus::UpdateBiasa($conn,$recsin,ms_pustaka,idpustaka,$rkey);
			$id_sinopsis = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 2 and idlampiran = '$rkey' ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_sinopsis);
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan file sinopsis gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan file sinopsis berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key.'&key2='.$rkey;
				Helper::redirect($url);
				
			}
		
		}else if($r_aksi == "delfilepus" and $c_delete){
			// Menghapus file pustaka type BLOB
			$tmp_file = $_FILES['userfile']['tmp_name'][$i];
			$filetype = $_FILES['userfile']['type'][$i];
			$filesize = $_FILES['userfile']['size'][$i];
			$filename = $_FILES['userfile']['name'][$i];
			$ext=explode(".",$filename);
			$fileup=Helper::removeSpecial($filename);
			
			$id_filepus = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 3 and idlampiran = $r_key and filename = '' ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_filepus);
				if($connLamp->ErrorNo() != 0){
					$errdb = 'Penghapusan file pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}else {
					$sucdb = 'Penghapusan file pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
		
		}else if($r_aksi =="delfile" and $c_delete){
			$file=Helper::removeSpecial($_POST['delfile']);
			$fkey=Helper::removeSpecial($_POST['delkey']);
			$seri=Helper::removeSpecial($_POST['delseri']);
			unlink($file);
			$fdel = explode('.',Helper::GetPath($file));
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiran,$fkey);
			Sipus::DeleteBiasa($conn,pustaka_file,idpustakafile,$fkey);
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan file upload gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan file upload berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
			}
		}else if($r_aksi =="ubahaksesfile" and $c_edit and $r_key != ''){
			$fkey=Helper::removeSpecial($_POST['delkey']);
			$uLogin = Helper::removeSpecial($_POST['uLogin'.$fkey]);
			$uDownload = Helper::removeSpecial($_POST['uDownload'.$fkey]);
			$recfile = array();
			$recfile['login'] = ($uLogin == "on" ? "1" : "0");
			$recfile['download'] = ($uDownload == "on" ? "1" : "0");
			Sipus::UpdateBiasa($conn,$recfile,pustaka_file,idpustakafile,$fkey);
				if($conn->ErrorNo() != 0){
					$errdb = 'Update file upload gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Update file upload berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				}
		}else if($r_aksi == "simpanfoto" and $c_edit and $rkey != '') {
			if($_FILES['foto']['error'] == UPLOAD_ERR_OK)
				$sfok = UI::createFoto($_FILES['foto']['tmp_name'],$p_foto,500,500);
			else if($_FILES['foto']['error'] == UPLOAD_ERR_INI_SIZE or $_FILES['foto']['error'] == UPLOAD_ERR_FORM_SIZE)
				$sfok = -4; // ukuran file melebihi batas
			else if($_FILES['foto']['error'] == UPLOAD_ERR_NO_FILE)
				$sfok = -5; // tidak ada file yang diupload
			
			$path = $_FILES['foto']['tmp_name'];
			if($sfok>0){
				$id_cover = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 1 and idlampiran = $rkey ");
				$recordc = array();
				$recordc['idlampiran'] = $rkey;
				$recordc['jenis'] = '1'; # 1= cover, 2=sinopsis, 3=file pustaka, 4= foto
				$recordc['tiduser'] = '1'; #test
				$recordc['filename'] = $_FILES['foto']['name']; 
				if($id_cover){
					$err = Sipus::UpdateBlob($connLamp,$recordc,'lampiranperpus',$path,'idlampiranperpus','isifile',$id_cover);
				}else{
					$err = Sipus::InsertBlob($connLamp,$recordc,'lampiranperpus',$path,'idlampiranperpus','isifile');
				}
			}
			
			@unlink($_FILES['foto']['tmp_name']); // hapus dan jangan tampilkan pesan error bila error
			
			switch($sfok) {
				case -1: $uploadmsg = 'Format file foto tidak dikenali.'; break;
				case -2: $uploadmsg = 'Format file foto harus GIF, JPEG, atau PNG.'; break;
				case -3: $uploadmsg = 'File foto tidak bisa diupload.'; break;
				case -4: $uploadmsg = 'Ukuran file foto melebihi batas maksimal.'; break;
				case -5: $uploadmsg = 'Tidak ada file foto yang di-upload.'; break;
				default: $uploadmsg = 'Upload foto berhasil.'; break;
			}
			
			if($sfok < 0)
				$uploadmsg = UI::message($uploadmsg,true);
			else
				$uploadmsg = UI::message($uploadmsg);
		}
		else if($r_aksi == "hapusfoto" and $c_edit and $rkey != '') {
			$dfok = unlink($p_foto);
			$id_cover = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 1 and idlampiran = '$rkey' ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_cover);
			
			if($dfok)
				$uploadmsg = UI::message("Penghapusan foto berhasil.");
			else
				$uploadmsg = UI::message("Penghapusan foto tidak berhasil.",true);
		}
		
		// untuk upload dan hapus foto
		if(!empty($uploadmsg)) {
			// html ditulis di iframe "upload_iframe"
			echo '<html><body><script type="text/javascript">'."\n";
			echo 'var parentdoc = window.parent.document;'."\n";
			echo "parentdoc.getElementById('uploadmsg').innerHTML = '$uploadmsg';\n";
			echo 'parent.$("#imgfoto").waitload({mode: "unload"});'."\n";
			
			if($sfok > 0 or $dfok)
				echo 'parentdoc.getElementById("imgfoto").src = "'.(is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default.jpg').'?'.mt_rand(1000,9999).'";'."\n";
			
			echo '</script></body></html>';
			
			exit();
		}	
	}
	if ($r_key !='') {
		$p_sqlstr = "select p.*, pe.namapenerbit
			from ms_pustaka p
			left join ms_penerbit pe on pe.idpenerbit=p.idpenerbit
			where idpustaka=$rkey";
		$row = $conn->GetRow($p_sqlstr);
		
		$sqlFile = "select idpustakafile, files, login, download from pustaka_file where idpustaka = '$rkey' order by idpustakafile ";
		$rsFile = $conn->GetArray($sqlFile);
		
		// get cover from blob file
		$sqlc = "select isifile from lampiranperpus where jenis = 1 and idlampiran = '$rkey' ";
		$rsCover = $connLamp->GetOne($sqlc);
		
		// get sinopsis from blob file
		$sqls = "select isifile,filename from lampiranperpus where jenis = 2 and idlampiran = '$rkey' ";
		$rsSinop = $connLamp->GetOne($sqls);
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/combo.js"></script>
	<script type="text/javascript" src="scripts/foreditx.js"></script>
	<script type="text/javascript" >
	function CreateTextbox()
	{

		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		var i = (document.getElementById('theValue').value -1)+2;
		h.value=i;
		var x='file'+i;
		var no=i+1;
		createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type='checkbox' name='isLogin[]'>&nbsp;Harus Login&nbsp;<input type='checkbox' name='isDownload[]'>&nbsp;Bisa didownload&nbsp;<input type=file onchange='if(!checkfile(this.files[0].type,this.files[0].size))$(this).val(null);' name='userfile[]' size='40' /> </p>";

	}
	</script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><h1 style="font-weight:normal;color:#015593;"><?= $p_title ?></h1></td>
			</tr>
		</table>
		<? include_once('_notifikasi.php'); ?>
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Digitalisasi </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:goDig();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } ?>
			</tr>
		</table><br>
		<?php include_once('_notifikasi.php');?>
		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<table width="750" border="0" cellspacing=0 cellpadding="7">
		<tr>
			<td>
			<div class="tabs" style="width:<?= $p_tbwidth ?>">
				<ul>
					<li><a id="tablink" href="javascript:void(0)">Identitas Pustaka</a></li>
					<li><a id="tablink" href="javascript:void(0)">Upload Data</a></li>
							
				</ul><br>
				<div style="width:<?= $p_tbwidth ?>;height:1px;background:#20915e;position:relative;top:-2px;margin-bottom:10px;"></div>
			</div>
			<div id="items" style="position:relative;top:-2px">
			<header style="width:<?= $p_tbwidth ?>;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1>Identitas Pustaka</h1>
					</div>
				</div>
			</header>
			<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">
				<tr height="20"> 
					<td class="LeftColumnBG thLeft">Kode Pustaka</td>
					<td class="RightColumnBG"><?= $row['noseri']; ?>
						<input type="hidden" name="noseri" id="noseri" value="<?= $row['noseri']?>">
					</td>
					<td valign="top" align="center" rowspan="6" width=150> 
						<? if ($r_key!='') { ?>
						<div id="uploadmsg"></div>
						<?php
							$cover = Sipus::loadBlobFile($rsCover, 'jpg');
						?>
						<img border="1" id="imgfoto" src="<?= (is_null($cover)) ? Config::fotoUrl.'default.jpg' : $cover ?>" width="120" height="150"><br>
						<!--img border="1" id="imgfoto" src="<?//= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default.jpg') ?>?<?//= mt_rand(1000,9999) ?>" width="120" height="150"-->
						<? if($c_edit) { ?>
						<input type="hidden" name="MAX_FILE_SIZE" value="10000000">
						<input type="file" name="foto" class="ControlStyle" size="15">
						<br><input type="button" value="Simpan" class="ControlStyle" onClick="aSavePhoto();"> 
						<input type="button" value="Hapus" class="ControlStyle" onClick="aDelPhoto();">
						<? } ?>
						<? } ?>
						</td>
				</tr>
				<tr height="20"> 
					<td class="LeftColumnBG thLeft" width=150>Judul *</td>
					<td class="RightColumnBG" ><?= $row['judul']?><input type="hidden" name="judul" id="judul" value="<?= $row['judul']?>">
					</td>
				</tr>
				<tr height="20"> 
					<td class="LeftColumnBG thLeft">Penerbit</td>
					<td class="RightColumnBG"><?= $row['namapenerbit']; ?></td>
					
				</tr>
				<tr height="20"> 
					<td class="LeftColumnBG thLeft"  width="150">Tahun Terbit </td>
					<td class="RightColumnBG"><?= $row['tahunterbit'] ?></td>
				</tr>
				<tr height="20"> 
					<td class="LeftColumnBG thLeft">Edisi </td>
					<td class="RightColumnBG"><?= $row['edisi'] ?></td>
				</tr>
				<tr height="20"> 
					<td class="LeftColumnBG thLeft">ISBN </td>
					<td class="RightColumnBG"><?= $row['isbn'] ?></td>
				</tr>
				<tr height="20"> 
					<td colspan="3" class="footBG">&nbsp; </td>
				</tr>
			</table>
			</div>
			<div id="items" style="position:relative;top:-2px"> <!-- ============= UPLOAD PUSTAKA ============================= -->
			<header style="width:<?= $p_tbwidth ?>;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1>Identitas Pustaka</h1>
					</div>
				</div>
			</header>
			<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
				<tr height=20>
					<td class="LeftColumnBG thLeft">Sinopsis Upload<br><span style="font-weight:normal">( Max Upload 5 MB )</span></td>
					<td class="RightColumnBG">
					<?php
					$sinop = Sipus::loadBlobFile($rsSinop['filename'], 'url',$r_key,'sinopsis');
					?>
					<? if($c_edit) { ?><input type="file" name="uploadsinopsis" id="uploadsinopsis" size=40 class="ControlStyle"><? } ?>
					<? if($sinop!='') {?>
						<br><a href="index.php?page=download&_auto=1&_ocd=sin&key=<?= $rkey ?>" title="Download sinopsis"><img src="images/attach.gif" border=0>Download Sinopsis</a>
						&nbsp; <img src="images/delete.png" style="cursor:pointer" alt="Hapus file sinopsis" onClick="goDelSin('<?= $row['sinopsis_upload'] ?>')">
					<? } ?>
					</td>
				</tr>
				<? if($r_key != '') { ?>
					<tr  height=20>
					
					<td class="LeftColumnBG thLeft" width="150">Properti Pustaka</td>
					<td align="center" valign="top">
					<table width="100%" border=0 cellpadding=0 cellspacing=0>
					<tr  height=20>
						<td width="50%" class="LeftColumnBG">Uploaded Files</td>
						
					</tr>
					<tr>
						<td width="50%" valign="top">
							<?
							$k=0;
							foreach($rsFile as $f){
								$k++;
								if($c_edit) {
							?>
							<a href="<?= $f['files'] ?>" target="_BLANK"><?= $f['files']!='' ? "<img src='images/attach.gif' border=0> ".$k.'. '.Helper::GetPath($f['files']) : '' ?></a>
							<span id="aksesShow<?=$f['idpustakafile'];?>">
								&nbsp; <img src="images/<?=($f['login']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Harus Login">&nbsp;Harus Login
								&nbsp; <img src="images/<?=($f['download']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Bisa didownload">&nbsp;Bisa didownload
								&nbsp; <img src="images/edited.gif" style="cursor:pointer" alt="Ubah Hak Akses" onClick="goShowAkses('<?= $f['idpustakafile'] ?>')">
							</span>
							<span id="aksesHide<?=$f['idpustakafile'];?>" style="display: none;">
								&nbsp;<input <?=($f['login'] ? "checked" : "");?> type="checkbox" name="uLogin<?=$f['idpustakafile'];?>">&nbsp;Harus Login
								&nbsp;<input <?=($f['download'] ? "checked" : "");?> type="checkbox" name="uDownload<?=$f['idpustakafile'];?>">&nbsp;Bisa didownload
								&nbsp; <img src="images/save.png" style="cursor:pointer" alt="Simpan Hak Akses" onClick="goUpdateAkses('<?= $f['idpustakafile'] ?>')">
							</span>
							<? $temp = explode("/",$f['files'])?>
							&nbsp; <img src="images/delete.png" style="cursor:pointer" alt="Hapus file upload" onClick="goDelFilePus('<?= $temp[3] ?>'); goDelFile('<?= $f['idpustakafile'] ?>','<?= $f['files'] ?>','<?= $row['noseri'] ?>');">
							<br>
							<?
								}else echo $i.'. '.Helper::GetPath($f['files'])."<br>"; 
							}
							?>
						</td>
					</tr>
					</table>
					</td>
				</tr><? } ?>
				<? if($c_edit) {?>
				<tr    height=20> 
					<td class="LeftColumnBG"  width="150">File Upload<br><span style="font-weight:normal">( Max filesize = 10 MB )</span></td>
					<td class="RightColumnBG">
					<input type="hidden" value="2" id="theValue" />
					<br>
					1. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="isDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>
					<p>2. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="iDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>
					<p>3. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="isDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>
					<div id="createTextbox"></div>
					<br/>
					<input type="button" name="btntext" onClick="CreateTextbox()" value="Tambah File" style="cursor:pointer">
					</td>
				</tr>
				<? } ?>
				<tr><td class="footBG" colspan="2">&nbsp;</td></tr>
			</table>
			</div>
		</td>
		</tr>
		</table>
		<iframe name="upload_iframe" style="display:none;"></iframe>
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $rkey ?>">
		<input type="hidden" name="delfile" id="delfile">
		<input type="hidden" name="delkey" id="delkey">
		<input type="hidden" name="delseri" id="delseri">
		<input type="hidden" name="delfilepus" id="delfilepus">
		</form>
		</div>
	</div>
</div>

</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
	$('#uploadsinopsis').bind('change', function() {
		alerr = "";
		sts = false;
		if(this.files[0].type != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(this.files[0].size > 5000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 5MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			$('#uploadsinopsis').val("");
		}
		
	
	});
	
	$('.t_pustakafile').bind('change', function() {
		alerr = "";
		sts = false;
		if(this.files[0].type != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(this.files[0].size > 10000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 10MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			$('.t_pustakafile').val("");
		}
		
	
	});
	
	function checkfile(jenis,ukuran){
		alerr = "";
		sts = false;
		if(jenis != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(ukuran > 10000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 10MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			return false;
		}else
			return true;
	}

$(document).ready(function() {

	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	chooseTab(0);
});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}

function goDelPhoto(){
	var delphoto=confirm("Apakah Anda yakin akan menghapus foto ini ?");
	if (delphoto){
		document.getElementById('act').value="delphoto";
		goSubmit();
	}

}

function goDelSin(key){
	var delsin=confirm("Apakah Anda yakin akan menghapus file sinopsis ?");
	if (delsin){
		document.getElementById('act').value="delsin";
		document.getElementById('delfile').value=key;
		goSubmit();
	}

}

function goDelFilePus(key4){
	var delfilepus=confirm("Apakah Anda yakin akan menghapus file pustaka tersebut?");
	if (delfilepus){
		document.getElementById('act').value="delfilepus";
		document.getElementById('delfilepus').value=key4;
		goSubmit();
	}

}

function goDelFile(key1,key2,key3){
	var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
	if(delfile){
		document.getElementById('act').value="delfile";
		document.getElementById('delkey').value=key1;
		document.getElementById('delfile').value=key2;
		document.getElementById('delseri').value=key3;
		goSubmit();
	}
}

function goDig() {
	$("#act").val("simpan");
	document.perpusform.submit();
}

function goShowAkses(key){ 
	$("#aksesShow"+key).hide();
	$("#aksesHide"+key).show();
}

function goUpdateAkses(key){
	document.getElementById('act').value="ubahaksesfile";
	document.getElementById('delkey').value=key;
	goSubmit();
}
</script>
</html>
