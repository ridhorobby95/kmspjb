<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Keterlambatan';

	$p_filerep = 'repp_telat';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$a_jenis = array('H' => 'Hari', 'B' => 'Bulan', 'T' => 'Tahun');
	$l_jenis = UI::createSelect('jenis',$a_jenis,'','ControlStyle');
	
	$a_fil = array('=' => '=', '<' => '<', '>' => '>');
	$l_fil = UI::createSelect('fil',$a_fil,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namajenisanggota, kdjenisanggota from lv_jenisanggota order by namajenisanggota");
	$l_anggota = $rs_cb->GetMenu2('kdjenisanggota','',true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:150"');
	$l_anggota = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_anggota);

	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi where 1=1 $sqlAdminUnit order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('kdlokasi','',true,false,0,'id="kdlokasi" class="ControlStyle" style="width:187"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('ke').focus()">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:<?= $p_tbwidth ?>%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Keterlambatan</h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">No. Anggota</td>
				<td><input type="text" name="idanggota" id="idanggota"></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Unit Kerja</td>
				<td><?= UI::createTextBox('namasatker','','ControlRead',50,50,true,'readonly'); ?>
					<img src="images/popup.png" style="cursor:pointer;" title="Pilih Unit kerja" onClick="showSatKerPU();">
					<img src="images/cancel.png" style="cursor:pointer;" title="Kosongkan Unit kerja" onClick="clearSatKerPU();">
					<input type="hidden" name="idunit" id="idunit">
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Anggota</td>
				<td><?= $l_anggota ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Lokasi Pustaka</td>
				<td><?= $l_lokasi ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Lama keterlambatan</td>
				<td><?= $l_fil ?>&nbsp;<input type="text" name="lama" id="lama" size=5 maxlength=5 onkeydown="return onlyNumber(event,this,false,true)">&nbsp;<?= $l_jenis ?> [Default >= 30 hari]</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<small>* laporan untuk melihat peminjaman yang belum dikembalikan.</small>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
		</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {	
	goSubmit();
}

function showSatKerPU() {
	win = window.open("<?= Helper::navAddress('pop_satker.php'); ?>&nama=namasatker&id=idunit","popup_satker","width=450,height=400,scrollbars=1");
	win.focus();
}
function clearSatKerPU() {
	document.getElementById("namasatker").value = "";
	document.getElementById("idunit").value = "";
}
</script>
</html>