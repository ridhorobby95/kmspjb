<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	define( '__PERPUS_ROW_LIMIT', 10 );
	
	error_reporting(0);

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng, 'home');

    if ($_GET['act']) {
		$act = Helper::removeSpecial($_GET['act']);
		
		switch ($act) {
			case 'getUsulanBaru':
			case 'getPesanDigilib':
			case 'order':
				$act($act);
				break;
		}
    }
    exit;

	
	#notifikasi
	function getUsulanBaru(){
	    $conn = Factory::getConn();
	    $countUsulan = $conn->Getone("select count(*) from pp_usul u where u.idunit is null and statususulan = '0' ");
	    echo '<span id="notif1" class="SideSubTitle '.($countUsulan>0?" blink":"").'">'.number_format($countUsulan,0,'','').'</span>';
	}
	

	function getPesanDigilib(){
	    $conn = Factory::getConn();
	    $countDigilib = $conn->Getone("select count(*) from pp_kontakonline p where idparentol is null and isaktif=1
					  and (select count(*) from pp_kontakonline where idparentol = p.idkontakol) = 0  ");
	    echo '<span id="notif4" class="SideSubTitle '.($countDigilib>0?" blink":"").'">'.number_format($countDigilib,0,'','').'</span>';
	}
	
?>