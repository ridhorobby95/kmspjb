<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	require_once('classes/barcode.class.php');
	
	$code=Helper::removeSpecial($_GET['code']);
	$start=Helper::removeSpecial($_GET['start']);
	$end=Helper::removeSpecial($_GET['end']);
	$c=Helper::removeSpecial($_GET['c']);
	$jnse=Helper::removeSpecial($_GET['j']);
	
	$jns = $conn->GetOne("select kdjenispustaka from lv_jenispustaka where kdjenispustaka='$jnse'");
	
	if($end!=''){
	$data=$conn->Execute("select e.ideksemplar,e.noseri, p.nopanggil,p.kodeddc
			from pp_eksemplar e
			join ms_pustaka p on e.idpustaka=p.idpustaka
			where e.noseri between '$start' and '$end' and kdklasifikasi='$code'
			order by e.ideksemplar");
	} else {
	$total=explode("|",$start);
	$jum=(count($total))-2;

	$sql="select k.namaklasifikasi labelklasifikasi,j.islokal,p.kdjenispustaka,e.tglperolehan,e.ideksemplar,e.noseri, 
		p.nopanggil,p.kodeddc,p.judul, substr(to_char(p.judul), 1, 1) as ins_judul, l.namalokasi,
		(
			select SUBSTR (LTRIM(b.namadepan), 1, 3) as ins_aut 
			from ms_author b 
			join pp_author a on b.idauthor = a.idauthor 
			where ROWNUM <= 1 and a.idpustaka = p.idpustaka
		) as ins_aut
		from pp_eksemplar e 
		join ms_pustaka p on e.idpustaka=p.idpustaka
		left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka 
		left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi
		where 1=1 ";
		 $i=0;
		 while($i<=$jum){
			if($i==0)
				$or=" and ";
				else
				$or=" or ";
				
			$x=$or." e.noseri='$total[$i]'";
			$s .=$x;
		$i++;
		}
	$sql .=$s;
	if($code!='')
		$sql .=" and e.kdklasifikasi='$code' ";
	
	$sql .=" order by noseri";
	$data2=$conn->Execute($sql);
	}
	if($c=='n')
		$p_title='Pelabelan Pustaka';
	elseif($c=='y')
		$p_title='Barcode Pustaka';
	
?>

<html>
<head>
<title><?= $p_title ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
<style type="text/css">
		
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10pt;
	padding:0px;
	
}
</style>
<style type="text/css">
@media print {
	.title-label td {
		background-color: #CEF6F5 !important;border-bottom:1px solid #000000 !important;
	}
}

    </style>

</head>
<body topmargin=0 leftmargin=10 rightmargin=0 bottommargin=0> 

<table border=1 cellspacing=20 style="border:0pt double black;-moz-border-radius:5px;padding:5px;margin-top:2px;">
<? 
		
		$i=0;
		$r=0;
		$loop=1;$inisial_pengarang="";
		while ($row=$data2->FetchRow()){ 
			if($row['authorlast1']==null or $row['authorlast1']==""){
				if($row['authorfirst1']==null or $row['authorfirst1']==""){
					$inisial_pengarang = "";
				}else{
					$pengarang = explode(",",$row['authorfirst1']);
					$inisial_pengarang = substr(strtoupper($pengarang[0]),0,3);
				}	
			}else{
				$inisial_pengarang = substr(strtoupper($row['authorlast1']),0,3);
			}
			
			$i++; 
			if($c=='n'){
				
					if($i%5==0 and ($i-1)%4==0){ 
						echo "<tr>";
						$r++;
					}
					?>
						<td width="250" height="130" valign="top" style="border:1px solid #000">
							<div style="vertical-align:top">
								<?=Sipus::getLabelCode($row['kodeddc'],$row['noseri'],$row['ins_judul'],$row['ins_aut'],'P',$row['namalokasi']);?>
							</div>
						</td>
					<? if($i%4==0) echo "</tr>";

		 } else if($c=='y') { ?>
			<?if($loop%5==0 and ($loop-1)%4==0){?>
				<tr height="70">
			<?}?>	
					<td width="200" valign="bottom" style="vertical-align:top"><br>
						<?=Sipus::getLabelCode($row['kodeddc'],$row['noseri'],$row['ins_judul'],$row['ins_aut'],'B',$row['namalokasi']);?>
					</td>
			<?if($loop%4==0){?>
				</tr>
			<?}?>
		<?$loop++; }
	} ?>
<table>	


</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>
