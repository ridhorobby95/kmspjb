<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'ms_anggota';
	$p_window = '[PJB LIBRARY] Daftar Anggota';
	$p_title = 'Daftar Anggota';
	$p_tbheader = '.: Daftar Anggota :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_anggotad.php');
	$p_id = "ms_anggota";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'a.idanggota';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';

	//variabel get
	$code=Helper::removeSpecial($_GET['code']);
	// sql pagu aktif
	$thpagu=Sipus::getPaguAktif($conn);
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select a.idanggota, a.namaanggota,a.noika, j.namajenisanggota, a.alamat, a.email
		from $p_dbtable a 
		join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota ";
			
	$p_sqlstr.=" where 1=1 $sqlAdminUnit ";
				   
	if($code=='s')
		$p_sqlstr .=" and a.kdjenisanggota not in(select kdjenisanggota from lv_jenisanggota where biayakeanggotaan is not null) ";
	else if($code=='u')
		$p_sqlstr .=" and a.kdjenisanggota not in(select kdjenisanggota from lv_jenisanggota where biayakeanggotaan is not null) ";
	else if($code=='ta')
		$p_sqlstr .=" and a.kdjenisanggota in ('M') ";
	else if($code=='ta2')
		$p_sqlstr .=" and a.kdjenisanggota in ('M','TA') ";
	else if($code=='ta3')
		$p_sqlstr .=" and a.kdjenisanggota in ('M','TA') ";
		
	else if($code=='sp')
		$p_sqlstr .=" and j.setpagu=1 ";
	else if($code=='pa')
		$p_sqlstr .=" and j.setpagu=1 ";
	else if($code=='d')
		$p_sqlstr .=" and a.kdjenisanggota='D' ";
		 
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyid=Helper::removeSpecial($_POST['cariid']);
		$keynama=Helper::removeSpecial($_POST['carinama']);	
		$keyjenis=Helper::removeSpecial($_POST['kdjenisanggota']);
		
		if ($keyid!='')
			$p_sqlstr.=" and a.idanggota = '$keyid'";
			if ($keynama!='')
				$p_sqlstr.=" and upper(a.namaanggota) like upper('%$keynama%')";
				if ($keyjenis!='')
					$p_sqlstr.=" and a.kdjenisanggota='$keyjenis' ";
	
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	$cb_jenis="select namajenisanggota, kdjenisanggota from lv_jenisanggota where 1=1";
	if($code=='s')
		$cb_jenis .=" and kdjenisanggota not in(select kdjenisanggota from lv_jenisanggota where biayakeanggotaan is not null) ";
	else if($code=='u')
		$cb_jenis .=" and kdjenisanggota not in(select kdjenisanggota from lv_jenisanggota where biayakeanggotaan is not null) ";
	else if($code=='ta')
		$cb_jenis .=" and kdjenisanggota in ('M','TA') ";
	else if($code=='ta2')
		$cb_jenis .=" and kdjenisanggota in ('M','TA') ";
	else if($code=='ta3')
		$cb_jenis .=" and kdjenisanggota in ('M','TA') ";
	else if($code=='sp')
		$cb_jenis .=" and setpagu=1 ";
	else if($code=='pa')
		$cb_jenis .=" and setpagu=1 ";
	else if($code=='d')
		$cb_jenis .=" and kdjenisanggota='D'";
	
	$cb_jenis .=" order by kdjenisanggota";
	$rs_cb = $conn->Execute($cb_jenis);
	$l_jenis = $rs_cb->GetMenu2('kdjenisanggota',$keyjenis,true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:120" onchange="goSubmit()"');
		
	//echo "carinama ".$keynama;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile."&code=".$code; ?>">
			<div class="filterTable">
				<table cellpadding=0 cellspacing="2" border=0 width="540">
					<tr height="25">
						<td width="120">Id Anggota</td>
						<td width="170" colspan=2>: <input type="text" id="cariid" size="20" name="cariid" value="<?= $keyid ?>" onKeyDown="etrCari(event);"></td>
						</tr>
					<tr height="25">
						<td>Nama Anggota</td>
						<td colspan=2>: <input type="text" id="carinama" name="carinama" size="35" value="<?= $keynama ?>" onKeyDown="etrCari(event);"></td>
						
						
					</tr>
					<tr>
						<td>Jenis Anggota</td>
						<td>: <?= $l_jenis ?> </td>
						<td align="right">
							<input type="button" value="Filter" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
						</td>		
					</tr>
				</table>
			</div><br />
			<header style="width:580;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
			<table width="580" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20">
				<td width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.idanggota:C');">Id Anggota  <?= $p_xsort['idanggota']; ?></td>		
				<td width="250" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.namaanggota:C');">Nama Anggota <?= $p_xsort['namaanggota']; ?></td>
				<td width="130" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.namajenisanggota:C');">Jenis Anggota<?= $p_xsort['namajenisanggota']; ?></td>
				<td width="50" nowrap align="center" class="SubHeaderBGAlt">Pilih</td>
				<?php
				$i = 0;

				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idanggota'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
				<td align="center">
				<?= $row['idanggota']; ?>
				</td>
				<td align="left">
				<? if ($code=='s') { ?>
				<u title="Pilih Anggota" onclick="goSend2('<?= $row['idanggota']?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='u') { ?>
				<u title="Pilih Anggota" onclick="goSend3('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='us'){ ?>
				<u title="Pilih Anggota" onclick="goSend4('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='ud') { ?>
				<u title="Pilih Anggota" onclick="goSend5('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['sisasementara'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='sp') { ?>
				<u title="Pilih Anggota" onclick="goSend6('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['sisasementara'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='ta') { ?>
				<u title="Pilih Anggota" onclick="goSend7('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['noika']?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='ta2') { ?>
				<u title="Pilih Anggota" onclick="goSend10('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='ta3') { ?>
				<u title="Pilih Anggota" onclick="goSend11('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='pa') { ?>
				<u title="Pilih Anggota" onclick="goSend8('<?= $row['idanggota']?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else if($code=='d') { ?>
				<u title="Pilih Anggota" onclick="goSend9('<?= $row['namaanggota']?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } else { ?>
				<u title="Pilih Anggota" onclick="goSend('<?= $row['idanggota']?>')" style="cursor:pointer;color:blue"><?= $row['namaanggota']; ?></u>
				<? } ?>
				</td>
				<td ><?= $row['namajenisanggota']; ?></td>
				<td align="center">
				<? if ($code=='s') { ?>
				<img title="Pilih Anggota" src="images/centang.gif" style="cursor:pointer" onClick="goSend2('<?= $row['idanggota']?>')">
				<? } else if($code=='u') { ?>
				<u title="Pilih Anggota" onclick="goSend3('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='us') { ?>
				<u title="Pilih Anggota" onclick="goSend4('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='ud') { ?>
				<u title="Pilih Anggota" onclick="goSend5('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['sisasementara'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='sp') { ?>
				<u title="Pilih Anggota" onclick="goSend6('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['sisasementara'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='ta') { ?>
				<u title="Pilih Anggota" onclick="goSend7('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>','<?= $row['noika']?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='ta2') { ?>
				<u title="Pilih Anggota" onclick="goSend10('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='ta3') { ?>
				<u title="Pilih Anggota" onclick="goSend11('<?= $row['idanggota']?>','<?= $row['namaanggota'] ?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='pa') { ?>
				<u title="Pilih Anggota" onclick="goSend8('<?= $row['idanggota']?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else if($code=='d') { ?>
				<u title="Pilih Anggota" onclick="goSend9('<?= $row['namaanggota']?>')" style="cursor:pointer;color:blue"><img title="Pilih Anggota" src="images/centang.gif"></u>
				<? } else { ?>
				<img title="Pilih Anggota" src="images/centang.gif" style="cursor:pointer" onClick="goSend('<?= $row['idanggota']?>')">
				<? } ?>
				</td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="4"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
				<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="npwd" id="npwd">

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script type="text/javascript">
	function goClear(){
	document.getElementById("cariid").value='';
	document.getElementById("carinama").value='';
	document.getElementById("kdjenisanggota").value='';
	//goSubmit();
	}

	function goSend(key) {
		
		window.opener.document.getElementById("i_idanggota").value = key;
		window.close();
	}
	function goSend8(key) {
		
		window.opener.document.getElementById("i_idanggota").value = key;
		window.close();
	}
	function goSend2(key) {
		
		window.opener.document.getElementById("idanggota").value = key;
		window.close();
	}
	function goSend3(key1,key2) {
		
		window.opener.document.getElementById("idanggota").value = key1;
		window.opener.document.getElementById("namaanggota").value = key2;
		window.opener.document.getElementById("namaanggota2").value = key2;
		window.close();
	}
	
	function goSend4(key1,key2) {
		window.opener.document.getElementById("idanggota").value = key1;
		window.opener.document.getElementById("namapengusul").value = key2;
		window.close();
	}
	function goSend5(key1,key2,key3) {
		window.opener.document.getElementById("idanggota").value = key1;
		window.opener.document.getElementById("namapengusul").value = key2;
		window.opener.document.getElementById("sisapagu").value = key3;
		window.close();
	}
	function goSend6(key1,key2,key3) {
		window.opener.document.getElementById("idpagu").value = key1;
		window.opener.document.getElementById("namapagu").value = key2;
		window.opener.document.getElementById("sisa").value = key3;
		window.close();
	}
	function goSend7(key1,key2,key3) {
		var slice_a = key2.split(" ");
		var jum_s = slice_a.length-1;
		
		window.opener.document.getElementById("idanggota").value = key1;
		window.opener.document.getElementById("namaanggota").value = key2;
		window.opener.document.getElementById("noika").value = key3;
		if(jum_s > 0){
		window.opener.document.getElementById("authorfirst1").value = key2.replace(" "+slice_a[jum_s],"");
		window.opener.document.getElementById("authorlast1").value = slice_a[jum_s];
		} else {
		window.opener.document.getElementById("authorfirst1").value = key2;
		window.opener.document.getElementById("authorlast1").value ="";
		}
		window.close();
	}
	function goSend10(key1,key2) {
		var slice_a = key2.split(" ");
		var jum_s = slice_a.length-1;
		
		window.opener.document.getElementById("nrp2").value = key1;
		if(jum_s > 0){
		window.opener.document.getElementById("authorfirst2").value = key2.replace(" "+slice_a[jum_s],"");
		window.opener.document.getElementById("authorlast2").value = slice_a[jum_s];
		} else {
		window.opener.document.getElementById("authorfirst2").value = key2;
		window.opener.document.getElementById("authorlast2").value ="";
		}
		window.close();
	}
	function goSend11(key1,key2) {
		var slice_a = key2.split(" ");
		var jum_s = slice_a.length-1;
		
		window.opener.document.getElementById("nrp3").value = key1;
		if(jum_s > 0){
		window.opener.document.getElementById("authorfirst3").value = key2.replace(" "+slice_a[jum_s],"");
		window.opener.document.getElementById("authorlast3").value = slice_a[jum_s];
		} else {
		window.opener.document.getElementById("authorfirst3").value = key2;
		window.opener.document.getElementById("authorlast3").value ="";
		}
		window.close();
	}
	
	function goSend9(key) {
		var show=window.opener.document.getElementById("pembimbing").value;
		if(show=='')
		window.opener.document.getElementById("pembimbing").value = key;
		else
		window.opener.document.getElementById("pembimbing").value += ","+key;
		
		window.close();
	}
	
	function initPage(){
		document.getElementById("cariid").focus();
	}
</script>
</html>