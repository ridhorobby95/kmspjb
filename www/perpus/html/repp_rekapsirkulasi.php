<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_pustaka = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_status = Helper::removeSpecial($_POST['status']);
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Buku Tamu Pengunjung';
	
	$p_namafile = 'laporan_bukutamu_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$s_jurusan = $conn->Execute("select * from ms_satker");
	
	$sql = "select count(a.idunit) as jumlah,a.idunit kdsatker
		from v_trans_list t
		join ms_anggota a on t.idanggota=a.idanggota
		where 1=1";
	
	#jenis pustaka
	if($r_pustaka!='')
		$sql .=" and kdjenispustaka='$r_pustaka'";
	
	$sql .=" and t.tgltransaksi between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')
		group by a.idunit";
	$rs = $conn->Execute($sql);
	
	while($row=$rs->FetchRow()){
		$ArJur[$row['kdsatker']] = $row['jumlah'];	
	}
	
	while($rowj=$s_jurusan->FetchRow()){
		$LJum[$rowj['kdsatker']][]=$rowj['kdsatker'];
		$LJur[$rowj['kdsatker']][]=$rowj['namasatker'];
	}
	$s_fakultas = $conn->Execute("select * from ms_satker order by namasatker");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Rekap <?= $r_lap=='tglpengembalian' ? 'Pengembalian' : ($r_lap=='tgltransaksi' ? 'Peminjaman' : 'Perpanjangan') ?> Pustaka
	<br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
  <? $total=0;
  while($rowf=$s_fakultas->FetchRow()){ ?>
  <tr height="30">
	<td><?= "<b><u>".$rowf['namasatker']."</u></b><br>"; 
	echo "<table>";
	$jumtotal = 0;
	for($i=0;$i<count($LJur[$rowf['kdsatker']]);$i++){
	echo "<tr>";
		echo "<td width=150>&nbsp;</td>";
		echo "<td width=450>";
		echo $LJur[$rowf['kdsatker']][$i];
		echo "</td>";
		echo "<td>";
		echo $ArJur[$LJum[$rowf['kdsatker']][$i]]=='' ? '0' : $ArJur[$LJum[$rowf['kdsatker']][$i]];
		echo '</td>';
	echo "</tr>";
	$jumtotal +=$ArJur[$LJum[$rowf['kdsatker']][$i]];
	}
	echo "<tr><td>&nbsp;</td><td><b><u>Jumlah<u></b></td><td><b><u>".$jumtotal."</u></b></td>";
	echo "</table>";
	?>
	</td>
  </tr>
  <? $total +=$jumtotal; } ?>
  <tr height=30>
	<td>
		<table>
		<tr>
		<td width=650><b>JUMLAH TOTAL</b>
		<td width="50" style="border-left-color:#fff;"><b><?= $total ?></b></td>
		</tr>
		</table>
	</td>
	</tr>
</table>


</div>
</body>
</html>