<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	session_start();

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_edit = $a_auth['canedit'];

	// bila belum login redirect ke index
	if(!Helper::xIsLoggedIn($conn)) {
		Helper::navigateOut();
		exit();
	}

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlUnitNotif = " and a.idunit in ($units) ";
		
	$sqlUnitEks = " and l.idunit in ($units) ";	

	// variabel esensial
	$r_idx = floor($_POST['rolesc']);
	if($r_idx != '' and isset($_SESSION['PERPUS_ARR_ROLE'])) {
		$_SESSION['PERPUS_INDEX'] = $r_idx;

		$_SESSION['PERPUS_ROLE'] = $_SESSION['PERPUS_ARR_ROLE'][$r_idx];
		$_SESSION['PERPUS_NAMAROLE'] = $_SESSION['PERPUS_ARR_NAMAROLE'][$r_idx];
		$_SESSION['PERPUS_SATKER'] = $_SESSION['PERPUS_ARR_SATKER'][$r_idx];
		$_SESSION['PERPUS_NAMASATKER'] = $_SESSION['PERPUS_ARR_NAMASATKER'][$r_idx];
		$_SESSION['PERPUS_ADMIN'] = $_SESSION['PERPUS_ARR_ADMIN'][$r_idx];
		$_SESSION['PERPUS_INITPAGE'] = $_SESSION['PERPUS_ARR_INITPAGE'][$r_idx];

		// bikin entri menu
		unset($_SESSION['PERPUS_MENU']);
		$p_sqlstr = "select namapage from sc_targetrole where idrole = '".$_SESSION['PERPUS_ROLE']."' and canread = 1";
		$rs = $conng->Execute($p_sqlstr);
		while(!$rs->EOF) {
			$_SESSION['PERPUS_MENU'][$rs->fields['namapage']] = true;
			$rs->MoveNext();
		}
	}

	if(!empty($_SESSION['PERPUS_INITPAGE'])) {
		// sementara, entah nanti penyelesaiannya gimana
		$isread = $conng->GetRow("select canread from gate.sc_targetrole where namapage = '".$_SESSION['PERPUS_INITPAGE']."' and idrole = '".$_SESSION['PERPUS_ROLE']."'");
		if(!empty($isread)) {
			header('Location: '.$_SESSION['PERPUS_INITPAGE']);
			exit();
		}
	}
	$db->debug = false;

	$countUsulan = $conn->Getone("select count(*)
				     from pp_usul u
				     join ms_anggota a on a.idanggota = u.idanggota 
				     where u.idunit is null and u.statususulan = '0' $sqlUnitNotif ");
	#kontak online
	$countKontak = $conn->Getone("select count(*)
				     from pp_kontakonline p
				     where p.idparentol is null and p.isaktif=1 and p.idkontak <> 0
				      and (
						select count(*)
						from pp_kontakonline
						where idparentol = p.idkontakol
					) = 0  ");
	
	$countDigilib = $conn->Getone("select count(*)
				      from pp_kontakonline p
				      where idparentol is null and isaktif=1 and p.idkontak = 0
				      and (
						select count(*)
						from pp_kontakonline
						where idparentol = p.idkontakol
					) = 0  ");
	
	$countReservasi = $conn->Getone("select count(*)
					from pp_reservasi r
					left join ms_anggota a on a.idanggota = r.idanggotapesan
					where r.statusreservasi='1' and r.tglexpired > current_date $sqlUnitNotif ");
	
	$countPanjang = $conn->Getone("select count(*)
				      from pp_perpanjangan j
				      join pp_transaksi t on t.idtransaksi = j.idtransaksi  
				      left join ms_anggota a on a.idanggota = t.idanggota 
				      where j.statusajuan='1' $sqlUnitNotif ");
	
	if(!$isAdminPusat)
		$pengunjungunit = " and l.idunit in ($units) ";

	#pengunjung
	$pengunjung = $conn->GetArray("select count(*) jml, j.namajenisanggota
				from pp_bukutamu b
				join ms_anggota a on a.idanggota = b.idanggota
				join lv_jenisanggota j on j.kdjenisanggota = a.kdjenisanggota
				join lv_lokasi l on l.kdlokasi = b.kdlokasi 
				where to_char(tanggal, 'YYYY') = '".date('Y')."' $pengunjungunit
				group by j.namajenisanggota
				order by j.namajenisanggota ");
	$dat_peng = array();
	foreach($pengunjung as $pg){
		$dat_peng[] = "['".$pg['namajenisanggota']."',".$pg['jml']."]";
		$t_pengunjung = $t_pengunjung + $pg['jml'];
	}

	#pengunjung mahasiswa
	$pengunjung_mhs = $conn->GetArray("select count(*) jml, j.namasatker
				from pp_bukutamu b
				join ms_anggota a on a.idanggota = b.idanggota
				join ms_satker j on j.idunit = b.idunit
				join lv_lokasi l on l.kdlokasi = b.kdlokasi 
				where to_char(tanggal, 'YYYY') = '".date('Y')."' and a.kdjenisanggota = 'M' $pengunjungunit
				group by j.namasatker
				order by j.namasatker");

	if(!$isAdminPusat)
		$peminjamunit = " and l.idunit in ($units) ";
	#peminjam
	$anggota = $conn->GetArray("select count(*) jml, j.namajenisanggota
				from pp_transaksi t
				join pp_eksemplar e on e.ideksemplar = t.ideksemplar
				join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				join ms_anggota a on a.idanggota = t.idanggota
				join lv_jenisanggota j on j.kdjenisanggota = a.kdjenisanggota $peminjamunit 
				where to_char(t.tgltransaksi, 'YYYY') = '".date('Y')."'
				group by j.namajenisanggota
				order by j.namajenisanggota ");
	$dat_ang = array();
	foreach($anggota as $da){
		$dat_ang[] = "['".$da['namajenisanggota']."',".$da['jml']."]";
		$total = $total + $da['jml'];
	}

	#peminjam mahasiswa
	$peminjam_mhs = $conn->GetArray("select count(*) jml, j.namasatker
				from pp_transaksi b
				join pp_eksemplar e on e.ideksemplar = b.ideksemplar
				join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				join ms_anggota a on a.idanggota = b.idanggota
				join ms_satker j on j.idunit = a.idunit
				where to_char(b.tgltransaksi, 'YYYY') = '".date('Y')."' and a.kdjenisanggota = 'M'  $peminjamunit
				group by j.namasatker
				order by j.namasatker");


	#koleksi
	$koleksi = $conn->GetArray("select count(*) jml, p.kdjenispustaka, j.namajenispustaka
				from ms_pustaka p  
				left join lv_jenispustaka j on j.kdjenispustaka = p.kdjenispustaka
				where 1=1 and p.idpustaka in (
					select e.idpustaka
					from  pp_eksemplar e 
					join lv_lokasi l on l.kdlokasi = e.kdlokasi
					where 1=1 $peminjamunit
				)
				group by p.kdjenispustaka, j.namajenispustaka
				order by j.namajenispustaka ");
	$dat_kol = array();
	foreach($koleksi as $da){
		$dat_kol[] = "['".str_replace('Jenis ','',$da['namajenispustaka'])."',".$da['jml']."]";
		$t_koleksi = $t_koleksi + $da['jml'];
	}

	#koleksi eksemplar
	$koleksie = $conn->GetArray("select count(e.ideksemplar) jml, p.kdjenispustaka, j.namajenispustaka
				from pp_eksemplar e
				join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				join ms_pustaka p on p.idpustaka = e.idpustaka
				left join lv_jenispustaka j on j.kdjenispustaka = p.kdjenispustaka
				where 1=1 $peminjamunit
				group by p.kdjenispustaka, j.namajenispustaka
				order by j.namajenispustaka ");
	$dat_kole = array();
	foreach($koleksie as $da){
		$dat_kole[] = "['".str_replace('Jenis ','',$da['namajenispustaka'])."',".$da['jml']."]";
		$t_koleksie = $t_koleksie + $da['jml'];
	}

	#koleksi terpinjam per bulan
	$koleksi_t = $conn->GetArray("select count(*) jml, to_char(b.tgltransaksi, 'mm') bulan
				from pp_transaksi b
				join pp_eksemplar e on e.ideksemplar = b.ideksemplar
				join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				where to_char(b.tgltransaksi, 'YYYY') = '".date('Y')."' $peminjamunit
				group by to_char(b.tgltransaksi, 'mm')
				order by to_char(b.tgltransaksi, 'mm') ");
	$dat_kolt = array();
	foreach($koleksi_t as $dat){
		$dat_kolt[intval($dat['bulan'])] = $dat['jml'];
		#get bulan aktif terakhir
		if($bul){
			if(intval($dat['bulan']) > $bul){
				$bul = intval($dat['bulan']);
				$data['jumlahbulan'] = $bul;
			}else{
				continue;
			}
		}else{
			$bul = intval($dat['bulan']);
			$data['jumlahbulan'] = $bul;
		}
	}
	$jumlahbulan = $data['jumlahbulan'];

	$categorie =  array();
	for($i=1; $i<=$jumlahbulan; $i++) {
		$categorie[] = Helper::bulanInd($i);
		$d_pin[] = ($dat_kolt[$i]?$dat_kolt[$i]:0);
	}
	$categories = "'".implode("','",$categorie)."'";
	if($dat_kolt)
		$koljem = implode(",",$d_pin);

	#koleksi pustaka eksemplar per tahun
	$y2 = intval(date('Y'));
	$y1 = intval(date('Y')) - 4;
	$koleksi_p = $conn->GetArray("select count(distinct e.idpustaka) jmlpustaka, count(e.ideksemplar) jmleksemplar, p.tahunterbit
				from pp_eksemplar e
				join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				join ms_pustaka p on p.idpustaka = e.idpustaka
				where 1=1 and coalesce(p.tahunterbit,'0') between '$y1' and '$y2' $peminjamunit
				group by p.tahunterbit
				order by p.tahunterbit");
	$pustakas = array();
	$eksemplars = array();
	$tahuns = array();
	foreach($koleksi_p as $dap){
		$pustakas[] = $dap['jmlpustaka'];
		$eksemplars[] = $dap['jmleksemplar'];
		$tahuns[] = $dap['tahunterbit'];
	}
?>
<html>
<head>
<title>[PJB LIBRARY] Selamat Datang</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link rel="stylesheet" href="style/pager.css">

<link href="style/tabpane.css" rel="stylesheet" type="text/css">
<link href="../application/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script language="JavaScript" type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/highcharts/highcharts.js"></script>
<script type="text/javascript" src="scripts/highcharts/modules/exporting.js"></script>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();" >
<?php include('inc_menu.php'); ?>
<div class="container">
<!--Dashboard-->
    <div class="col-md-12">
        <div class="row">
        <div class="SideItem" id="SideItem" >
                <table>
                    <tr>
                        <td><img width="24px" src="images/aktivitas/pustaka.png" class="addButton"></td>
                        <td> &nbsp; <span style="font-size: large" class="SideSubTitle">SELAMAT DATANG di SISTEM INFORMASI MANAJEMEN PERPUSTAKAAN</span></td>
                    </tr>
                </table>
            <div style="background:#015593;width:100%;height:1px;margin-bottom:10px;"></div>
        <div class="LeftRibbon"> <img width="24px" src="images/aktivitas/DASHBOARD.png" onerror="loadDefaultActImg(this)"> Dashboard </div>
        <div class="dashboard">
            <div class="tabs" style="width:100%">
                <ul>
                    <li><a id="tablink" href="javascript:void(0)">Koleksi Pustaka</a></li>
                    <li><a id="tablink" href="javascript:void(0)">Koleksi Pustaka per Jenis Pustaka</a></li>
                    <li><a id="tablink" href="javascript:void(0)">Koleksi Eksemplar per Jenis Pustaka</a></li>
                    <li><a id="tablink" href="javascript:void(0)">Koleksi Terpinjam</a></li>
                    <li><a id="tablink" href="javascript:void(0)">Peminjam</a></li>
                    <li><a id="tablink" href="javascript:void(0)">Pengunjung</a></li>
                </ul>
                <div id="items">
                    <div class="table-responsive">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('pustaka')" /></td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top" align="center">
                                <span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Pustaka Perpustakaan PJB <br/>Tahun <?=$y1." s/d ".$y2;?></span><br/>
                                <div id="container_kolekpus" style="width: 750px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
                <div id="items">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('pusjur')" /></td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top" align="center">
                                <span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Perpustakaan PJB Hingga Tahun <?=date('Y');?></span><br/>
                                <div id="container_koleksi" style="width: 750px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top">
                                <table cellpadding="4" cellspacing="0" border=1 style="width: 50%">
                                    <tr class="DataBG" align="center">
                                        <td>Jenis Pustaka</td>
                                        <td>Jumlah Judul Pustaka</td>
                                    </tr>
                                    <?
                                        foreach($koleksi as $a){
                                    ?>
                                    <tr>
                                        <td><?=$a['namajenispustaka'];?></td>
                                        <td align="right"><?=number_format($a['jml'],0, ',', '.');?></td>
                                    </tr>
                                    <?
                                        }
                                    ?>
                                    <tr>
                                        <td align="center" class="FootBG"><b>Total</b></td>
                                        <td align="right" class="FootBG"><b><?= number_format($t_koleksi,0, ',', '.');?></b></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="items">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('eksjur')" /></td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top" align="center">
                                <span class="SideSubTitle" style="font-size: 14px;">Data Eksemplar Perpustakaan PJB Hingga Tahun <?=date('Y');?></span><br/>
                                <div id="container_koleksie" style="width: 750px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top">
                                <table cellpadding="4" cellspacing="0" border=1 style="width: 50%">
                                    <tr class="DataBG" align="center">
                                        <td>Jenis Pustaka</td>
                                        <td>Jumlah Eksemplar</td>
                                    </tr>
                                    <?
                                        foreach($koleksie as $a){
                                    ?>
                                    <tr>
                                        <td><?=$a['namajenispustaka'];?></td>
                                        <td align="right"><?=number_format($a['jml'],0, ',', '.');?></td>
                                    </tr>
                                    <?
                                        }
                                    ?>
                                    <tr>
                                        <td align="center" class="FootBG"><b>Total</b></td>
                                        <td align="right" class="FootBG"><b><?= number_format($t_koleksie,0, ',', '.');?></b></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="items">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('terpinjam')" /></td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top" align="center">
                                <span class="SideSubTitle" style="font-size: 14px;">Data Koleksi Terpinjam Perpustakaan PJB Tahun <?=date('Y');?></span><br/>
                                <div id="container_koleksit" style="width: 750px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="items"  style="position:relative;top:-2px">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('peminjam')" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" class="SideSubTitle" style="font-size: 14px;">
                                Data Peminjam Perpustakaan PJB Tahun <?=date('Y');?>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top">
                                <table cellpadding="4" cellspacing="0" border=1>
                                    <tr class="DataBG" align="center">
                                        <td width="70%">Jenis Anggota</td>
                                        <td>Jumlah</td>
                                    </tr>
                                    <?
                                        foreach($anggota as $a){
                                            if(trim($a['namajenisanggota']) == "Mahasiswa"){
                                    ?>
                                    <tr>
                                        <td colspan="2">
                                            <table cellpadding="4" cellspacing="0" border=1>
                                                <tr><td colspan="2"><strong><?=$a['namajenisanggota'];?></td></strong></tr>
                                                <?
                                                    foreach($peminjam_mhs as $mhs){
                                                ?>
                                                <tr>
                                                    <td width="71.8%"><?=$mhs['namasatker'];?></td>
                                                    <td align="right" style="padding-right: 30px;"><?=number_format($mhs['jml'],0, ',', '.');?></td>
                                                </tr>
                                                <?
                                                    }
                                                ?>
                                                <tr>
                                                    <td align="right"><strong>Subtotal Mahasiswa</strong></td>
                                                    <td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?
                                            }else{
                                    ?>
                                    <tr>
                                        <td><strong><?=$a['namajenisanggota'];?></strong></td>
                                        <td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
                                    </tr>
                                    <?
                                            }
                                        }
                                    ?>
                                    <tr>
                                        <td align="center" class="FootBG"><b>Total</b></td>
                                        <td align="right" class="FootBG"><b><?= number_format($total,0, ',', '.');?></b></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" valign="top" align="center">
                                <div id="container_peminjam" style="width: 500px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="items">
                    <table width="100%" cellspacing="0" cellpadding="4">
                        <tr>
                            <td colspan="2" align="right"><input type="button" value="Print Tab" onclick="gotPrintTab('pengunjung')" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" class="SideSubTitle" style="font-size: 14px;">
                                Data Pengunjung Perpustakaan PJB Tahun <?=date('Y');?>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" valign="top">
                            <table cellpadding="4" cellspacing="0" border=1>
                                <tr class="DataBG" align="center">
                                    <td width="70%">Jenis Anggota</td>
                                    <td>Jumlah</td>
                                </tr>
                                <?
                                    foreach($pengunjung as $a){
                                        if(trim($a['namajenisanggota']) == "Mahasiswa"){
                                ?>
                                    <tr>
                                        <td colspan="2">
                                            <table cellpadding="4" cellspacing="0" border=1>
                                                <tr><td colspan="2"><strong><?=$a['namajenisanggota'];?></td></strong></tr>
                                                <?
                                                    foreach($pengunjung_mhs as $mhs){
                                                ?>
                                                <tr>
                                                    <td width="71.8%"><?=$mhs['namasatker'];?></td>
                                                    <td align="right" style="padding-right: 30px;"><?=number_format($mhs['jml'],0, ',', '.');?></td>
                                                </tr>
                                                <?
                                                    }
                                                ?>
                                                <tr>
                                                    <td align="right"><strong>Subtotal Mahasiswa</strong></td>
                                                    <td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?
                                            }else{
                                    ?>
                                    <tr>
                                        <td><strong><?=$a['namajenisanggota'];?></strong></td>
                                        <td align="right"><strong><?=number_format($a['jml'],0, ',', '.');?></strong></td>
                                    </tr>
                                    <?
                                            }
                                    }
                                ?>
                                <tr>
                                    <td align="center" class="FootBG"><b>Total</b></td>
                                    <td align="right" class="FootBG"><b><?= number_format($t_pengunjung,0, ',', '.');?></b></td>
                                </tr>
                            </table>
                            </td>
                            <td width="50%" valign="top" align="center">
                                <div id="container_pengunjung" style="width: 500px; height: 300px; margin: 0 auto"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--Shortcut-->
<div class="col-md-7 col-sm-7 col-xs-12 SideItem SideItem-shortcut" id="SideItem" style="float:left;">
<div class="LeftRibbon"> <img width="24px" src="images/aktivitas/WILAYAH.png" onerror="loadDefaultActImg(this)"> Shortcut </div>
<? #if ($c_edit) { ?>
<div class="dashboard">
	<div class="dashboard-left">
		<ul class="list">
			<li>
				<span style="left: 10; width: 30%;">
					<a href="index.php?page=list_pustaka"><img src="images/menu_icon/biblio.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="index.php?page=list_pustaka"><h3>Bibliografi</h3></a>
					<p>Memudahkan anda dalam mencari Daftar Pustaka, Eksemplar, Tugas Akhir, dan melakukan stock opname</p>
				</span>
			</li>
			<li>
				<span style="left: 10; width: 30%;">
					<a href="index.php?page=sirkulasi_pinjam"><img src="images/menu_icon/sirkulasi.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="index.php?page=sirkulasi_pinjam"><h3>Sirkulasi</h3></a>
					<p>Seluruh Transaksi mulai dari penerimaan eksemplar, peminjaman, pengembalian dan pengembalian cepat.</p>
				</span>
			</li>
			<li>
				<span style="right: 10; width: 30%;">
					<a href="index.php?page=list_pelayanan"><img src="images/menu_icon/fasilitas.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="index.php?page=list_pelayanan"><h3>Fasilitas</h3></a>
					<p>Fasilitas perpustakaan kini dapat diatur sesuai dengan kondisi perpustakaan.</p>
				</span>
			</li>
		</ul>
	</div>
	<div class="dashboard-right">
		<ul class="list">
			<li>
				<span style="right: 10; width: 30%;">
					<a href="index.php?page=list_anggota"><img src="images/menu_icon/keanggotaan.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="index.php?page=list_anggota"><h3>Keanggotaan</h3></a>
					<p>Menu Keanggotaan dikhususkan untuk mengatur hal-hal yang berkaitan dengan anggota perpustakaan.</p>
				</span>
			</li>
			<li>
				<span style="right: 10; width: 30%;">
					<a href="index.php?page=list_usulanbiasa"><img src="images/menu_icon/pengusulan.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="index.php?page=list_usulanbiasa"><h3>Pengusulan Pustaka</h3></a>
					<p>Menu Pengusulan Pustaka berfungsi untuk melihat apa saja daftar bahan pustaka yang telah diusulkan. </p>
				</span>
			</li>
			<li>
				<span style="right: 10; width: 30%;">
					<a href="<?=config::digilib_url;?>" title="Digilib PJB" target="_blank"><img src="images/menu_icon/digilib.png" border="0"></a>
				</span>
				<span style="right: 10; width: 70%;">
					<a href="<?=config::digilib_url;?>"><h3>Digilib PJB</h3></a>
					<p>Lihat dan akses website Digilib (Digital Library) PJB</p>
				</span>
			</li>
	</div>
</div>
</div>

<!--KANAN-->
<div class="col-md-4 col-sm-4 col-xs-12 SideItem" style="float:right;">
  <div class="LeftRibbon"> <img width="24px" src="images/aktivitas/JADWAL.png" onerror="loadDefaultActImg(this)"> <?=Helper::indoDay(date('N')).", ".date('d M Y');?> </div>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tbody>
      <tr>
        <td colspan="3" style="border-bottom:1px solid #647287; font-size:12px;"><img width="16px" src="images/aktivitas/ADMIN.png" onerror="loadDefaultActImg(this)"> &nbsp;
          <?= $_SESSION['PERPUS_USER'].' - '.$_SESSION['PERPUS_NAME'] ?></td>
      </tr>
      <tr class="Break">
        <td></td>
      </tr>
      <tr>
        <td colspan="3" style="font-size:12px;"><img width="16px" src="images/aktivitas/HISTORI.png" onerror="loadDefaultActImg(this)"> &nbsp; <span class="SideSubTitle">Login Terakhir:</span>
          <?= Helper::formatDateTime($_SESSION['PERPUS_LASTLOG']); ?></td>
      </tr>
    </tbody>
  </table>
  <br/><br/>
  <div class="LeftRibbon"> <img width="24px" src="images/aktivitas/NOTIFICATION.png" onerror="loadDefaultActImg(this)"> Notifikasi [Digilib]</div>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tbody>
      <tr>
        <td width="10%"><img width="16px" src="images/aktivitas/DEFAULT.png" class="addButton"></td>
        <td valign="center" width="90%">
		<a href="index.php?page=list_usulanbiasa">
		<span class="SideSubTitle<?=($countUsulan>0?" blink":"");?>">Pengusulan Pustaka Baru : <?=number_format($countUsulan,0,'','');?></span>
		</a>
	</td>
      </tr>
      <tr class="Break">
        <td></td>
      </tr>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/FORUM.png" class="addButton"></td>
        <td valign="center">
		<a href="index.php?page=list_answerkontak">
		<span class="SideSubTitle<?=($countKontak>0?" blink":"");?>">Pesan dari Kontak Online : <?=number_format($countKontak,0,'','');?></span>
		</a>
	</td>
      </tr>
      <tr class="Break">
        <td></td>
      </tr>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/FORUM.png" class="addButton"></td>
        <td valign="center">
		<a href="index.php?page=list_answeronline">
		<span class="SideSubTitle<?=($countDigilib>0?" blink":"");?>">Pesan dari Hubungi Kami : <?=number_format($countDigilib,0,'','');?></span>
		</a>
	</td>
      </tr>
      <tr class="Break">
        <td></td>
      </tr>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/ABSENSI.png" class="addButton"></td>
        <td valign="center">
		<a href="index.php?page=list_reservasi">
		<span class="SideSubTitle<?=($countReservasi>0?" blink":"");?>">Reservasi Pustaka : <?=number_format($countReservasi,0,'','');?></span>
		</a>
	</td>
      </tr>
      <tr class="Break">
        <td></td>
      </tr>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/KULIAH.png" class="addButton"></td>
        <td valign="center">
		<a href="index.php?page=list_reservasi">
		<span class="SideSubTitle<?=($countPanjang>0?" blink":"");?>">Perpanjangan Pustaka : <?=number_format($countPanjang,0,'','');?></span>
		</a>
	</td>
      </tr>
    </tbody>
  </table>
  <br/><br/>
<!--UG-->
  <div class="LeftRibbon"> <img width="24px" src="images/aktivitas/GUIDE.png" onerror="loadDefaultActImg(this)"> User Guide </div>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tbody>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center">
		<a href="UG/UG Back-End.pdf" target="_blank"><span class="SideSubTitle">UG SIM Perpustakaan [Back-End]</span></a>
	</td>
      </tr>
      <tr>
        <td width="10%"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center" width="90%">
		<a href="UG/UG Digilib.pdf" target="_blank"><span class="SideSubTitle">UG Digilib</span></a>
	</td>
      </tr>
    </tbody>
  </table>
  <br/><br/>
<!--QG-->
  <div class="LeftRibbon"> <img width="24px" src="images/aktivitas/GUIDE.png" onerror="loadDefaultActImg(this)"> Quick Guide </div>
  <table width="100%" cellpadding="4" cellspacing="0">
    <tbody>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center">
		<a href="QG/QG Pengadaan Pustaka.pdf" target="_blank"><span class="SideSubTitle">QG Pengadaan</span></a>
	</td>
      </tr>
      <tr>
        <td width="10%"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center" width="90%">
		<a href="QG/QG Daftar Pustaka.pdf" target="_blank"><span class="SideSubTitle">QG Daftar Pustaka</span></a>
	</td>
      </tr>
      <tr>
        <td valign="top"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center">
		<a href="QG/QG Sirkulasi.pdf" target="_blank"><span class="SideSubTitle">QG Sirkulasi</span></a>
	</td>
      </tr>
      <tr>
        <td width="10%"><img width="16px" src="images/aktivitas/SUBMIT.png" class="addButton"></td>
        <td valign="center" width="90%">
		<a href="QG/QG Opname Pustaka.pdf" target="_blank"><span class="SideSubTitle">QG Stock Opname</span></a>
	</td>
      </tr>
    </tbody>
  </table>
</div>
<!--BATAS KANAN-->

</body>
<script language="javascript">
$(document).ready(function() {
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);

		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");

		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});

	chooseTab(0);
});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>
<script type="text/javascript">
$(function () {
    var pie_peminjam, pie_pengunjung, pie_koleksi, pie_koleksie, bar_koleksi, bar_kolekpus;

    $(document).ready(function() {

	pie_peminjam = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_peminjam',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		colors: ['#FE9A2E', '#64E572', '#ED561B', '#DDDF00', '#50B432', '#FF9655', '#24CBE5', '#FFF263', '#6AF9C4'],
		series: [{
		    type: 'pie',
		    name: 'Data Anggota',
		    data: [<?=implode(",",$dat_ang);?>]
		}]
	});

	pie_pengunjung = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_pengunjung',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		series: [{
		    type: 'pie',
		    name: 'Data Anggota',
		    data: [<?=implode(",",$dat_peng);?>]
		}]
	});

	pie_koleksi = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_koleksi',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		series: [{
		    type: 'pie',
		    name: 'Data Koleksi',
		    data: [<?=implode(",",$dat_kol);?>]
		}]
	});

	pie_koleksie = new Highcharts.Chart({
		chart: {
		    renderTo: 'container_koleksie',
		    plotBackgroundColor: null,
		    plotBorderWidth: null,
		    plotShadow: false
		},
		title: {
		    text: ' '
		},
		tooltip: {
			pointFormat: '<strong>{point.percentage}%</strong>',
			percentageDecimals: 2
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
				}
			}
		    }
		},
		series: [{
		    type: 'pie',
		    name: 'Data Koleksi',
		    data: [<?=implode(",",$dat_kole);?>]
		}]
	});

	bar_koleksi = new Highcharts.Chart({
            chart: {
                renderTo: 'container_koleksit',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
				title: {
                    text: 'Bulan'
                },
                categories: [<?=$categories;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Koleksi (Eksemplar)'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
						}
					}
                }
            },
	    legend: {
				enabled: false
			},
            series: [
				{
					name: 'Jumlah Eksemplar',
					data: [<?=$koljem;?>]
				},
		]
        });

	bar_kolekpus = new Highcharts.Chart({
            chart: {
                renderTo: 'container_kolekpus',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
				title: {
                    text: 'Tahun'
                },
                categories: [<?=implode(",",$tahuns)?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Koleksi'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
						}
					}
                }
            },
	    legend: {
				enabled: false
			},
            series: [
			{
				name: 'Pustaka',
				data:[<?=implode(",",$pustakas)?>]
			},
			{
				name: 'Eksemplar',
				data:[<?=implode(",",$eksemplars)?>]

			}
		]
        });

    });

});

function gotPrintTab(typep){
	window.open('index.php?page=cetak_tab&tab='+typep);
}
</script>
</html>
