<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Daftar Pengusulan Biasa';
	$p_title = 'Daftar Pengusulan Biasa';
	$p_tbheader = '.: Daftar Pengusulan Biasa :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_usulanbiasa.php');
	$p_id = "usulbiasa";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'tglusulan';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select u.*, o.stsusulan, o.stspengadaan, o.stspo, o.ststtb
		from pp_usul u
		left join pp_orderpustaka o on o.idusulan=u.idusulan
		join ms_anggota a on a.idanggota = u.idanggota 
		where u.idunit is null and o.idsumbangan is null $sqlAdminUnit";
  	
	$p_order=$conn->GetRow("select * from pp_orderpustaka where 1=1");
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		$idanggota=Helper::removeSpecial($_POST['idanggota']);
		if($r_aksi=='validasi'){
	
		if ($id!='' and $idanggota!=''){
			$record=array();
			$record['tglrealisasi']=Helper::formatDate($_POST['tgl']);
			$record['statususulan']=Helper::removeSpecial($_POST['status']);
			Helper::Identitas($record);
			Sipus::UpdateBiasa($conn,$record,pp_usulan,idusulan,$id);
				
			if($conn->ErrorNo() != 0){
					$errdb = 'Verifikasi Pengusulan Pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Verifikasi Pengusulan Pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		}

		}
		else if($r_aksi=='proses'){
			$p_proses=$conn->GetRow("select * from pp_usul where idusulan=$id");
			$recproses=array();
			$recproses['idusulan']=$id;
			$recproses['judul']=Helper::removeSpecial($p_proses['judul']);
			$recproses['hargausulan']=$p_proses['hargausulan'];
			$recproses['qtyusulan']=$p_proses['qtyusulan'];
			$recproses['keterangan']=Helper::cStrNull($p_proses['keterangan']);
			$recproses['authorfirst1']=Helper::cStrNull($p_proses['authorfirst1']);
			$recproses['authorlast1']=Helper::cStrNull($p_proses['authorlast1']);
			$recproses['authorfirst2']=Helper::cStrNull($p_proses['authorfirst2']);
			$recproses['authorlast2']=Helper::cStrNull($p_proses['authorlast2']);
			$recproses['authorfirst3']=Helper::cStrNull($p_proses['authorfirst3']);
			$recproses['authorlast3']=Helper::cStrNull($p_proses['authorlast3']);
			$recproses['penerbit']=Helper::cStrNull($p_proses['penerbit']);
			$recproses['tahunterbit']=Helper::cStrNull($p_proses['tahunterbit']);
			$recproses['stsusulan']=1;
			
			$p_check=$conn->GetRow("select idusulan from pp_orderpustaka where idusulan=$id");
			
			$conn->StartTrans();
			if(!$p_check)
				Sipus::InsertBiasa($conn,$recproses,pp_orderpustaka);
			else
				Sipus::UpdateBiasa($conn,$recproses,pp_orderpustaka,idusulan,$id);
			
			$record['tglvalidasiusul']=date('Y-m-d H:i:s');
			$record['npkpetugasvalidasi']=$_SESSION['PERPUS_USER'];
			$record['statususulan']=1;
			Sipus::UpdateBiasa($conn,$record,pp_usul,idusulan,$id);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
					$errdb = 'Proses Usulan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
#kirim notifikasi : proses pengusulan biasa
					$sucdb = 'Proses Usulan Berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		
		}
		else if ($r_aksi=='filter') {
			$filt=Helper::removeSpecial($_POST['txtid']);
			$status=Helper::removeSpecial($_POST['status']);
			if($filt!='')
			$p_sqlstr.=" and u.idanggota='$filt'";
			if($status=='x')
			$p_sqlstr.="";
			else if($status =='0')
			$p_sqlstr.=" and o.stsusulan is null";
			else
			$p_sqlstr.=" and o.stsusulan='$status'";
		}
		else if ($r_aksi=='status'){
			$status=Helper::removeSpecial($_POST['status']);
			if($status=='x')
			$p_sqlstr.="";
			else if($status=='0')
			$p_sqlstr.=" and o.stsusulan is null";
			else
			$p_sqlstr.=" and o.stsusulan='$status'";
		}
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}

	$a_proses = array('x' => '-- Semua --', '0' => 'Belum Diproses', '1' => 'Telah Diproses');
	$l_proses = UI::createSelect('status',$a_proses,$status,'ControlStyle',$c_edit,'onchange="goStatus()"');
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable table-responsive">
			<table width="100%" border="0" style="border:0 none;">
				<tr>
					<td width="150">Masukkan Id Anggota</td>
					<td>: &nbsp; <input type="text" name="txtid" id="txtid" maxlength="20" value="<?= $filt ?>"  onkeydown="etrFil(event);"></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="150">Status</td>
					<td>: &nbsp; <?= $l_proses ?> </td>
					<td align="right">
						<input type="button" value="Filter" class="ControlStyle" onclick="goFilt()"> &nbsp;&nbsp;
						<input type="button" value="Refresh" class="ControlStyle" onclick="goRef();">
					</td>
				</tr>
			</table>
		</div>
		<br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
				<div class="right">
				  <?	#if($c_add) { ?>
				  <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data" onClick="goNew('<?= $p_filedetail; ?>')">+</div>
				  <?	#} ?>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="0" cellspacing="0" class="GridStyle">
			<tr height="20"> 
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.tglusulan:D');">Tanggal <?= $p_xsort['tglusulan']; ?></td>		
				<td width="220" align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.namapengusul:C');">Anggota Pengusul <?= $p_xsort['namapengusul']; ?></td>
				<td width="220"nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.judul:C');">Judul Pustaka <?= $p_xsort['judul']; ?></td>		
				<td width="150"nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.authorfirst1:C');">Pengarang <?= $p_xsort['authorfirst1']; ?></td>		
				<td width="50" nowrap align="center" class="SubHeaderBGAlt thLeft">PE</td>
				<td width="50" nowrap align="center" class="SubHeaderBGAlt thLeft">TTP</td>
				<td width="50" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>	
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
						if ($row['idusulan'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height=20> 
				<td align="center">	<?= Helper::tglEng($row['tglusulan'],false); ?></td>	
				<td align="left"><?= $row['idanggota'].' - '.$row['namapengusul']; ?></td>		
				<td align="left"><?= $row['judul']; ?></td>
				<td align="left"><?= $row['authorfirst1']." ".$row['authorlast1']; ?></td>
				<td align="center"><?= $row['stspengadaan']=='1' ? "<img title=\"Proses Selesai\" src='images/centang.gif'>" : ($row['stspengadaan']==0 ? "<img title=\"Proses Menunggu\" src='images/menunggu.png'>" : "<img title=\"Pengusulan Ditolak\" src='images/uncheck.gif'>") ?></td>
				<td align="center"><?= $row['stspengadaan']=='2' ? "<img title=\"Pengusulan Ditolak\" src='images/uncheck.gif'>" : ($row['ststtb']==1 ? "<img title=\"Proses Selesai\" src='images/centang.gif'>" : "<img title=\"Proses Menunggu\" src='images/menunggu.png'>") ?></td>
				<td align="center">
				<? if($c_edit){ ?>
				<u onClick="goDet('<?= $p_filedetail; ?>','<?= $row['idusulan'] ?>','<?= $row['idanggota'] ?>');"  title="Detail Usulan" class="link"><img src="images/edited.gif"></u>
				<? if ($row['stsusulan']!=1 and $row['statususulan']!=1){?>
				<u onClick="goProses('<?= $row['idusulan'] ?>')"  title="Proses Usulan" class="link"><img src="images/proses.png"></u>
				<? } else { ?>
				<img title="Usulan Sedang Diproses"src="images/proses2.png">
				<? }} else { ?>
				<img title="Tidak Dapat Mengedit" src="images/edited2.gif">
				<img title="Tidak Dapat Diproses" src="images/proses2.png">
				<? }?>
				</td>
			</tr>
			<?php
					}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col; ?>" style="padding:5px;"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
			</tr>
		</table>
                </div>
        <?php require_once('inc_listnav.php'); ?> <br />
				<table>
					<tr>
						<td align="center">
							<table align="center" cellspacing="3" cellpadding="2" width="200" border=0 >
								<tr>
									<td colspan=2><u>Keterangan :</u></td>
								</tr>
								<tr>
									<td width=10>PE</td><td>= Proses Pengadaan</td>
								<tr>
								<tr>
									<td width=10>TTP</td><td>= Tanda Terima Pustaka</td>
								<tr>
								<tr>
									<td colspan="2" width=10>&nbsp;</td>
								<tr>
							</table>
						</td>
						<td>
						<table align="center" cellspacing="3" cellpadding="2" width="200" border=0 >
								<tr>
									<td colspan=2><u>Keterangan :</u></td>
								</tr>
								<tr>
									<td width=10><img src="images/centang.gif" title="Diterima"></td><td>= Proses Selesai </td>
								</tr>
								
								<tr>
									<td width=10><img src="images/wait.gif" title="Menunggu"></td><td>= Proses Menunggu</td>
								<tr>
								<tr>
									<td width=10><img src="images/uncheck.gif" title="Ditolak"></td><td>= Proses Ditolak</td>
								<tr>

							</table>
						</td>
					</tr>
				</table><br>



		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key2" id="key2">
		<input type="hidden" name="id" id="id">
		<input type="hidden" name="idanggota" id="idanggota">
		<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>

		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>

<script src="scripts/jqModal.js"></script>


<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl").mask("99-99-9999");
	   $("#tgl1").mask("99-99-9999");
});
</script>

<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function etrFil(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goFilt();
}
function goStatus(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}
function goStatus1(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}

function goProses(key){
	var conf=confirm("Apakah Anda yakin akan proses usulan ini ?");
	if(conf){
	document.getElementById("act").value="proses";
	document.getElementById('id').value=key;
	goSubmit();
	}
}

function goValidasi(){
	document.getElementById("act").value="validasi";
	goSubmit();
}

function goFilt(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function goStatus(){
	document.getElementById("act").value="status";
	goSubmit();
}

function goRef() {
	document.getElementById("act").value="";
	document.getElementById("txtid").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}
</script>
</html>