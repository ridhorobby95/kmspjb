<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user

	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$p_window="[PJB LIBRARY] Laporan Pustaka Opname";
	$r_id = Helper::removeSpecial($_GET['id']);


	if ($r_id !=''){
		$strsql=$conn->Execute("select o.*,p.judul, k.namakondisi as kondisiawal,e.noseri, k2.namakondisi as kondisiop, l.namalokasi, r.namarak, r.lantai
								from pp_detailopname o join pp_eksemplar e on o.ideksemplar=e.ideksemplar
								join ms_pustaka p on e.idpustaka=p.idpustaka
								join lv_kondisi k on o.kondisiawal=k.kdkondisi
								join lv_kondisi k2 on o.kondisiopname=k2.kdkondisi
								join lv_lokasi l on e.kdlokasi=l.kdlokasi
								left join ms_rak r on e.kdrak=r.kdrak
								where o.idopname=$r_id order by o.ideksemplar");
		$strjum=$strsql->RowCount();	
		
		$str=$conn->GetRow("select * from pp_opname where idopname=$r_id");
	}else
		echo"<script>javascript:window.close()</script>";
	
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/report.css" type="text/css" rel="stylesheet">
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="javascript:window.print();" >
<div align="center">
<table width="675" border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td align="center" colspan=2><strong><h2>LAPORAN OPNAME EKSEMPLAR<br></h2></strong>

		</td>
	</tr>
	<tr height=20>
		<td width=150>Tahun Opname </td><td>: <?= $str['namaopname'] ?></td>

	</tr>
	<!--<tr height=20>
		<td>Tanggal </td><td>: <?= Helper::tglEng($str['tglmulai'])?> s/d <?= Helper::tglEng($str['tglselesai']) ?></td>
	</tr>-->
</table>

<table width="675" border="1" cellpadding="2" cellspacing="0">
	<!--<tr>
		<th width="50" align="center" rowspan=2><strong>No.</strong></th>
		<th width="80" align="center" rowspan=2><strong>NO INDUK</strong></th>
		<th width="170" align="center" rowspan=2><strong>Judul Pustaka</strong></th>
		<th width="150" align="center" rowspan=2><strong>Kondisi Awal</strong></th>
		<th width="150" align="center" rowspan=2><strong>Kondisi Opname</strong></th>
		<th width="170" align="center" rowspan=2><strong>Lokasi</strong></th>
		<th width="150" align="center" colspan=2><strong>Tempat</strong></th>
	</tr>
	<tr>
		<th width="100" align="center"><strong>Nama Rak</strong></th>
		<th width="100" align="center"><strong>Lantai</strong></th>
	</tr>-->
	
	<tr>
		<th width="5%" align="center"><strong>No.</strong></th>
		<th width="10%" align="center"><strong>NO INDUK</strong></th>
		<th width="31%" align="center"><strong>Judul Pustaka</strong></th>
		<th width="17%" align="center"><strong>Kondisi Awal</strong></th>
		<th width="17%" align="center"><strong>Kondisi Opname</strong></th>
		<th width="20%" align="center"><strong>Lokasi</strong></th>
	</tr>
	<? 
		$no=0;
		while($row=$strsql->FetchRow()){ $no++?>
	<tr height=20>
		<td align="center" valign="top"><?= $no ?></td>
		<td align="center" valign="top"><?= $row['noseri'] ?></td>
		<td valign="top"><?= $row['judul'] ?></td>
		<td valign="top"><?= $row['kondisiawal'] ?></td>
		<td valign="top"><?= $row['kondisiop'] ?></td>
		<td valign="top"><?= $row['namalokasi'] ?></td>
		<!--<td>&nbsp;<?= $row['namarak'] ?></td>-->
		<!--<td align="center"><?= $row['lantai'] ?></td>-->
	</tr>
	
	<? } ?>
	
	<? if ($no==0){ ?>
	<tr>
		<td colspan=6 align="center">Data tidak ditemukan.</td>
	</tr>
	<? }else { ?>
	<tr height=20>
		<td colspan=6><strong>JUMLAH : <?= $strjum ?> </strong></td>
	</tr>
	
	<? } ?>
	
</table>
</div>
</body>

</html>