<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// definisi variabel halaman
	// $p_dbtable = 'lv_lokasipustaka';
	$p_dbtable = 'lv_lokasi';
	$p_window = '[PJB LIBRARY] Daftar Lokasi';
	$p_title = 'Daftar Lokasi';
	$p_tbheader = '.: Daftar Lokasi:.';
	$p_col = 3;
	$p_tbwidth = 100;
	$p_id = "ms_lok";
	$p_row = 10;
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	//dapatkan code GET
	$code=Helper::removeSpecial($_GET['code']);
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select l.* from $p_dbtable l where 1=1 $sqlAdminUnit";
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
	//filtering
	$keylokasi=Helper::removeSpecial($_POST['carilokasi']);
	if($keylokasi!='')
		$p_sqlstr.=" and upper(l.namalokasi) like upper('%$keylokasi%')";
	
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr.=" order by l.kdlokasi";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('carilokasi').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post">
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable">
			<table width="100%">
				<tr>
					<td colspan="3">
					<table border=0  cellpadding=0 cellspacing=0 width="100%">
					<tr><td>
					Nama Lokasi : &nbsp;<input type="text" name="carilokasi" id="carilokasi" size="25" value="<?= $keylokasi ?>" onKeyDown="etrCari(event);"></td>
					<td  align="right"><input type="button" value="Filter" class="ControlStyle" onclick="javascript:goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="javascript:goRefresh()" /></td>
					</tr>
					</table>
					</td>
				</tr>
			</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="100%" border="1" cellpadding="3" cellspacing=0 class="GridStyle">
			<tr height="20"> 
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">Kode Lokasi</td>
				<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Lokasi</td>
				<td width="60" nowrap align="center" class="SubHeaderBGAlt thLeft">Pilih</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">	
					<?= $row['kdlokasi']; ?>
				</td>
				<td>
				<? if($code!='') {?>
				<u title="Pilih Lokasi" class="link" onclick="goSendOp('<?= $row['kdlokasi'] ?>')"><?= $row['namalokasi']; ?></u>
				<? } else {?>
				<u title="Pilih Lokasi" class="link" onclick="goSend('<?= $row['kdlokasi'] ?>','<?= $row['namalokasi']; ?>')"><?= $row['namalokasi']; ?></u>
				<? } ?>
				</td>
				<td align="center">
				<? if($code!='') {?>
				<img title="Pilih Lokasi" src="images/centang.gif" style="cursor:pointer" onclick="goSendOp('<?= $row['kdlokasi'] ?>')">
				<? } else {?>
				<img title="Pilih Lokasi" src="images/centang.gif" style="cursor:pointer" onclick="goSend('<?= $row['kdlokasi'] ?>','<?= $row['namalokasi']; ?>')">
				<? } ?>
				</td>
			</tr>
			<? } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="4"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
			<?php require_once('inc_listnav.php'); ?><br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">
function goRefresh(){
document.getElementById("carilokasi").value='';
goSubmit();

}

function goSend(id,nama) {
	window.opener.document.getElementById("kdlokasi").value = id;
	window.opener.document.getElementById("namalokasi").value = nama;
	
	window.close();
}
function goSendOp(id) {
	window.opener.document.getElementById("kdlokasi").value = id;
	window.close();
}

</script>
</html>