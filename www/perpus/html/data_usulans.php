<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// koneksi ke sdm
	$conns = Factory::getConnSdm();
	

	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Data Usulan Pustaka';
	$p_title = 'Data Usulan Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_usulanspustaka.php');
	$p_fileexit = Helper::navAddress('home.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
				
		if($r_aksi == 'simpan' and $c_edit) {
						
			$record = array();
			$record = Helper::cStrFill($_POST);
			if($r_key == '') { // insert record	
				$record['idunit']=Helper::removeSpecial($_POST['idsatker']);
				$record['idanggota']=Helper::removeSpecial($_POST['idanggota']);
				$record['namapengusul']=Helper::removeSpecial($_POST['namaanggota2']);//$sql['namaanggota'];
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['tglrealisasi']=Helper::formatDate($_POST['tglrealisasi']);
				$record['statususulan']=0;
				$record['ispengadaan']='1';
				Helper::Identitas($record);
				Sipus::InsertBiasa($conn,$record,$p_dbtable);
				}
			else { // update record
				$record['idunit']=Helper::removeSpecial($_POST['idsatker']);
				$record['idanggota']=Helper::removeSpecial($_POST['idanggota']);
				$record['namapengusul']=Helper::removeSpecial($_POST['namaanggota2']);
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['tglrealisasi']=Helper::formatDate($_POST['tglrealisasi']);
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['tglrealisasi']=Helper::formatDate($_POST['tglrealisasi']);
				Helper::Identitas($record);
				Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
				}
		
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
				}
		
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
				$r_key = $record['idusulan'];
				}
				else {
				$row = $_POST; // direstore
				$p_errdb = true;
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			echo "aaa";
			echo $err=Sipus::DeleteBiasa($conn,$p_dbtable,idusulan,$r_key);
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}else{
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
		}
	}
	
	if ($r_key !='') {
		$p_sqlstr = "select p.*, namaanggota from $p_dbtable p 
					left join ms_anggota a on a.idanggota=p.idanggota
					where idusulan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		
		$u_row = $conns->GetRow("select idsatker, namasatker from ms_satker where idsatker='".$row['idunit']."'");
	}
	if(!$c_edit and $r_key2==''){
		header('Location: '.$p_fileexit);
		exit();
	}
	//echo "r_key ".$r_key;
	//echo "r_key2 ".$r_key2;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Usulan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDel();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table>
<br>

<?php include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr height=25> 
		<td class="LeftColumnBG" height="30" width="180">Unit Kerja *</td>
		<td >
		<input type="text" id="namasatker" name="namasatker" size="35" style="background:#ccc" disabled value="<?= $row['idunit']." - ".$u_row['namasatker'] ?>">
		<input type="hidden" id="idsatker" name="idsatker" size="10" value="<?= $row['idunit'] ?>">
		<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Lihat Unit Kerja" onClick="popup('index.php?page=pop_satker',500,500);">
			<span id="span_posisi"><u title="Lihat Unit Kerja" onclick="popup('index.php?page=pop_satker',500,500);" style="cursor:pointer;" class="Link">Lihat Unit...</u></span>
			<? } ?>
		</td>
	</tr>
	
	<tr height=25> 
		<td class="LeftColumnBG" height="30" width="180">Id Anggota *</td>
		<td>
		<input type="text" id="namaanggota" name="namaanggota" size="35" style="background:#ccc" disabled value="<?= $row['namaanggota'] ?>">
		<input type="hidden" id="namaanggota2" name="namaanggota2" size="35" style="background:#ccc" value="<?= $row['namaanggota'] ?>">
		<input type="hidden" id="idanggota" name="idanggota" size="10" value="<?= $row['idanggota'] ?>">
		<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=u',600,500);">
			<span id="span_posisi"><u title="Lihat Anggota" onclick="popup('index.php?page=pop_anggota&code=u',600,500);" style="cursor:pointer;" class="Link">Lihat Anggota...</u></span>
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tanggal Usulan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglusulan',$r_key=='' ? date('d-m-Y') : Helper::formatDate($row['tglusulan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglusulan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Judul Pustaka</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Harga</td>
		<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',20,15,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang</td>
		<td class="RightColumnBG"><?= UI::createTextBox('author',$row['author'],'ControlStyle',100,50,$c_edit); 
		 
		 ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">

</form>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function saveData() {
	var idsatker=document.getElementById("idsatker").value;
	var idanggota=document.getElementById("idanggota").value;
	
	if (idsatker=='')
		alert('Masukkan data unit kerja dengan benar');
		else if(idanggota=='')
			alert('Masukkan Anggota dengan benar');
				else
					goSave();
}

function goDel() {
	var hapus = confirm("Apakah anda yakin akan menghapus data iniiiiiiiiiii?");
	if(hapus) {
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

</script>
</html>