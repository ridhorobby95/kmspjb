<?php
	//$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Sirkulasi Pembayaran Denda';
	$p_title = 'SIRKULASI PEMBAYARAN DENDA';
	$p_tbheader = '.: SIRKULASI PEMBAYARAN DENDA :.';
	$p_col = 9;
	$p_tbwidth = 1135;
	$p_filedetail = Helper::navAddress('ms_anggota.php');
	$p_filedetaild = Helper::navAddress('ms_anggotad.php');
		
	if(isset($_POST['btntrans'])){
		
		$idcheck=trim(Helper::removeSpecial($_POST['txtid']));
		$sqlanggota=$conn->GetRow("select idanggota from ms_anggota where idanggota='$idcheck'");
		if ($sqlanggota) {
		
			//session di selesai di bawa kemari
			$conn->StartTrans();
			$conn->Execute("update pp_transaksi set fix_status='1' where idanggota = '".$_SESSION['keyword_denda']."'");
			$conn->CompleteTrans();
			
			Helper::clearTrans();
			unset($_SESSION['waktu_denda']);
			unset($_SESSION['wmulai_denda']);
			
			//session saat login idanggota
			$_SESSION['keyword_denda']=$idcheck;
			$_SESSION['waktu_denda']=date('Y-m-d H:i:s');
			$_SESSION['wmulai_denda']=date('H:i:s');
			// $_SESSION['tab'] = 0;
			Helper::redirect();
		}else {
			$errdb = 'Data anggota tidak ditemukan.';
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}
	}
	
	$p_anggota="select lj.namajurusan,a.idanggota,a.catatan,a.namaanggota,a.alamat,a.email,a.statusanggota, a.tgldaftar, a.tglexpired, a.kdjenisanggota, 
			a.tglselesaiskors,a.uangjaminan, j.namajenisanggota,a.denda from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota 
			left join lv_jurusan lj on lj.kdjurusan=a.idunit where a.idanggota='".$_SESSION['keyword_denda']."'";
	$r_anggota=$conn->GetRow($p_anggota);
	// $p_cektenggat=$conn->GetRow("select idtransaksi from pp_transaksi where idanggota='".$_SESSION['keyword_denda']."' and tgltenggat<current_date and tglpengembalian is null");
	$p_cektenggat=$conn->GetRow("select idtransaksi from pp_transaksi where idanggota='".$_SESSION['keyword_denda']."' and tgltenggat<current_date and statustransaksi='1'");
	
	$r_aksi=Helper::removeSpecial($_POST['act']);
	

	
	if (!empty($_POST)) {
		$idanggota=$_SESSION['keyword_denda'];
		$jenisanggota=$r_anggota['kdjenisanggota'];
		$noseri=Helper::removeSpecial($_POST['txtpustaka']);
		$idpesen=Helper::removeSpecial($_POST['txtreservasi']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		$r_key3=Helper::removeSpecial($_POST['key3']);
		$r_ideks=Helper::removeSpecial($_POST['ideks']);
		$waktumulai=Helper::removeSpecial($_POST['keydate']);
				
		if(!$r_anggota){
			$errdb = 'Data anggota tidak ditemukan.';
			unset($_SESSION['keyword_denda']);
			unset($_SESSION['waktu_denda']);
			Helper::setFlashData('errdb', $errdb); 	
			Helper::redirect();
		}
		
		if($r_aksi=='delsession') {
		
			unset($_SESSION['keyword_denda']);
			$sucdb = 'Proses transaksi telah selesai';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		
		}
	}

		//list sirkulasi
		if($_SESSION['keyword_denda']!=''){
			$sql2 = "select idtransaksi,ideksemplar,judul,judulseri,edisi,authorfirst1,authorlast1,tgltransaksi,tgltenggat,noseri,fix_status from v_trans_list where tgltransaksi=current_date and tglpengembalian is null and idanggota='".$_SESSION['keyword_denda']."' and kdjenistransaksi<>'PJL' and fix_status='0'";
			$p_trans=$conn->Execute($sql2);
			
			$p_pinjam=$conn->Execute("select * from v_trans_list where /*tglpengembalian is null*/ statustransaksi='1' and idanggota='".$_SESSION['keyword_denda']."' and kdjenistransaksi='PJN' and kdjenispustaka in(".$_SESSION['roleakses'].")");
			
			$p_reservasi=$conn->Execute("select * from v_reserve where tglexpired>=current_date and idanggotapesan='".$_SESSION['keyword_denda']."' and statusreservasi='1' order by idreservasi desc limit 15");
			
			$p_sejarah=$conn->Execute("select * from v_trans_list where idanggota='".$_SESSION['keyword_denda']."'  and kdjenispustaka in(".$_SESSION['roleakses'].") order by tgltenggat desc limit 15");
			
			$p_skors = $conn->GetRow("select keterangan from pp_skorsing where idanggota='".$_SESSION['keyword_denda']."' order by idskorsing desc");
			
			$p_foto = Config::dirFoto.'anggota/'.trim($_SESSION['keyword_denda']).'.jpg';
			$p_hfoto = Config::fotoUrl.'anggota/'.trim($_SESSION['keyword_denda']).'.jpg';
			
			$p_mhsfoto = Config::dirFotoMhs.trim($_SESSION['keyword_denda']);
			$p_pegfoto = Config::dirFotoPeg.trim($_SESSION['keyword_denda']).'.jpg';
			
			// $rs_b = $conn->Execute("select kdjenisanggota, biayakeanggotaan from lv_jenisanggota");
			// while ($row_b=$rs_b->FetchRow()){
				// $jbiaya[$row_b['kdjenisanggota']]=$row_b['biayakeanggotaan'];
			// }		
		}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();" >
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon">TRANSAKSI</div>
		<div align="center" style="position:relative;top:-11px;">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<table width="99%" border="0" cellpadding="4" cellspacing=0>
			<? if($_SESSION['keyword_denda']=='') { ?>
			<tr>
			<td width="<?= $p_tbwidth ?>" align="center">
			
			<table width="600" border=0 align="center" cellpadding="4" cellspacing=0 class="instan">
			  
			  <tr height="35">
			  <td>
				<table width="95%" border="0" align="center" cellpadding="4" cellspacing="0" class="filterTable">
				<!--tr height="25">
				<th colspan="2">TRANSAKSI</th>
			  </tr-->
				<tr height="50">
				<td valign="bottom" width="200" ><h3>Masukkan Id Anggota </h3>
				</td>
				<td>
				&nbsp;<input type="text" name="txtid" id="txtid" size="25" maxlength="20" class="ControlStyleT" onKeyDown="etrTrans(event);">
				</td>
				<td align="center">
				<input type="submit" name="btntrans" class="buttonSmall" id="btntrans" value="Mulai Transaksi" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">	
				</td>
				</tr>
				<!--tr>
				<td colspan=2 align="center">
				<input type="submit" name="btntrans" class="buttonSmall" id="btntrans" value="Mulai Transaksi" style="padding-bottom:3px;cursor:pointer;height:23px;width:130px;font-size:12px;">	
				</td>
				</tr-->
			  </td>	
			  </tr>
			</table>
			
			<center><br><? include_once('_notifikasi.php'); ?></center>
			  </td>
			  </tr><? } ?>	
				
			<? if($_SESSION['keyword_denda']!='') { ?>
			
			<td align="center">
			
			<table width="600" cellspacing="0" cellpadding="0"  border="0" style="background:#EBF2F9;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
			<tr height="25">
				<th colspan="4">Sirkulasi Transaksi</td>
			</tr>
			<tr height="10">
				<td>&nbsp;</td>
			</tr>
			<tr height="22">
				<td width=160 ><b>Id Anggota</b></td>
				<td width="220">: 
				<? if($c_edit) { 
				if($jbiaya[$r_anggota['kdjenisanggota']]!='' or $jbiaya[$r_anggota['kdjenisanggota']]!=0) {
				?>
				<u title="Edit Anggota" onclick="goDetail('<?= $p_filedetail; ?>','<?= $r_anggota['idanggota']; ?>');" class="link"><?= $r_anggota['idanggota'] ?></u>
				<? } else { ?>
				<input type="text" name="txtid" id="txtid" size="15" maxlength="20" onKeyDown="etrTrans(event);" value="<?= $r_anggota['idanggota'];?>">
				<u title="Edit Anggota" onclick="goDetail('<?= $p_filedetaild; ?>','<?= $r_anggota['idanggota']; ?>');" class="link"><img src="images/find.png"></u>
				<div style="display:none"><input type="submit" name="btntrans" class="buttonSmall" id="btntrans"></div>
				<? }}else {?>
				<?= $r_anggota['idanggota'] ?>
				<? } ?>
				</td>
				<td rowspan=5 align="center">
				<!-- <img src="images/perpustakaan/default1.jpg" width="110" height="110" border=1>-->
					<? if(is_file($p_foto)) { ?>
					<img border="1" id="imgfoto" src="<?= $p_hfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
					<? } else{
						if($r_anggota['kdjenisanggota'] =='D' or $r_anggota['kdjenisanggota']=='T'){
					?>
					<img border="1" id="imgfoto" src="<?= $p_pegfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
					<? } elseif ($r_anggota['kdjenisanggota'] =='M') { ?>
					<img border="1" id="imgfoto" src="<?= $p_mhsfoto ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
					<? } else { ?>
					<img border="1" id="imgfoto" src="<?= Config::fotoUrl.'default1.jpg' ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110">
					<? } } ?>		
				</td>
			</tr>
			<tr height="22">
				<td ><b>Nama Anggota </b></td>
				<td width="300"> : <b><?= $r_anggota['namaanggota'] ?></b></td>
				
			</tr>
			<tr height="22">
				<td ><b>Jurusan </b></td>
				<td width="300"> : <b><?= $r_anggota['namajurusan']=='' ? '-' : $r_anggota['namajurusan'] ?></b></td>
			</tr>
			<tr height="22">
				<td><b>Jenis Keanggotaan</b></td>
				<td>: <?= $r_anggota['namajenisanggota'] ?></td>
			</tr>		
			<tr height="22">
				<td ><b>Alamat</b></td>
				<td >: <?= $r_anggota['alamat'] ?></td>
			</tr>
			<tr height="22">
				<td ><b>E-Mail</b></td>
				<td >: <?= $r_anggota['email']=='' ? '-' : $r_anggota['email'] ?></td>
			</tr>
			<tr height="22">
				<td><b>Tanggal Registrasi</b></td>
				<td>: <?= Helper::formatDateInd($r_anggota['tgldaftar']) ?></td>
			</tr>
			<? if($r_anggota['tglexpired']!='') {?>
			<tr height="22">
				<td><b>Tanggal Expired</b></td>
				<td>: <?= Helper::formatDateInd($r_anggota['tglexpired']) ?></td>
				<td>&nbsp;</td>
			</tr>	
			<tr height="22">
				<td><font color="red"><b>Hutang Denda</b></td>
				<td>: <font color="red">Rp. <?= $r_anggota['denda'] == null ? '0' : $r_anggota['denda']?>,- &nbsp;
					</td>
				<td>&nbsp;</td>
			</tr>	
			<? } ?>
			<? if ($r_anggota['statusanggota']==0){ ?>
			<tr height="22">
				<td colspan="3" align="center"><font color="red"><b>
				<blink> Anggota tersebut telah nonaktif [ <?= $r_anggota['catatan'] ?> ] !!! </blink>
				</b></font></td>
			</tr>
			<? }elseif($r_anggota['statusanggota']==2){ ?>
				<tr height="22">
				<td colspan="3" align="center"><font color="red"><b>
				<blink>Anggota tersebut telah terblokir !!!</blink>
				</b></font></td>
			</tr>	
			<? }?>
			<? if ($r_anggota['tglselesaiskors'] >= date("Y-m-d")){ ?>
			<tr height="22">
				<td colspan="3" align="center"><font color="red"><b>
				<script>
				var skor="Anggota terkena masa skors [ <?= $p_skors['keterangan'] ?> ] sampai tanggal <?= Helper::formatDateInd($r_anggota['tglselesaiskors']) ?>.";
				document.write(skor.blink());
				</script>
				</b></font></td>
			</tr>
			<? } ?>
			<? if($p_cektenggat) { ?>
			<tr height="22">
				<td colspan="3" align="center"><font color="red"><b>
				<script>
				var tenggat="Terdapat pustaka belum dikembalikan yang melewati masa tenggat.";
				document.write(tenggat.blink());
				</script>
				</b></font></td>
			</tr>
			<? }?>
			<? if($r_anggota['tglexpired'] < date("Y-m-d")){?>
			<tr height="22">
				<td colspan="3" align="center"><font color="red"><b>
				<blink>Masa Berlaku anggota telah habis !!!</blink>
				</b></font></td>
			<?}?>
			</table>
			<br />
				<table style="padding:3px 5px;margin:0 auto;background-image:none;border-top:1px solid #ccc;" class="GridStyle">
					<tr>
						<td class="thLeft" style="border:0 none;padding-left:15px;padding-right:15px;">
							<span class="out" name="btnselesai" id="btnselesai" value="Selesai" onClick="goClear()" style="padding-right:0px;cursor:pointer;"></span><span style="cursor:pointer;" onClick="goClear()">Selesai</span>
						</td>
					</tr>
				</table>
				<br>
			</td>
			</tr>
			
			<tr>
			<td>			
		<div class="tabs" style="width:100%">
			<ul>
				<li onclick="chCol()"><a id="tablink" href="javascript:void(0)" onClick="goTab(0)">Sedang Dipinjam</a></li>
				<div class="a" align="right" valign="center"><? include_once('_notifikasi_trans.php'); unset($_SESSION['reserve_denda']) ?><br></div>
			
			</ul>
					<div style="background:#015593;width:100%;height:1px;margin-bottom:10px;"></div>

		<!-- ================================= sedang dipinjam ============================ -->
		<div id="items"  style="position:relative;top:-2px">
		<table cellspacing="0" width="100%" class="GridStyle" style="border-bottom:0 none;border-top:1px solid #ccc;">
			<tr height="20"> 
				<td width="8%" nowrap align="center" class="SubHeaderBGAlt thLeft">NO INDUK</td>
				<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul Pustaka </td>		
				<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang</td>		
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Tanggal Pinjam</td>
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft">Batas Kembali</td>
				<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">Denda</td>
				
				<?php
				$i = 0;


					// mulai iterasi
					while ($rowp = $p_pinjam->FetchRow()) 
					{
						if ($i % 2) $rowstyle1 = 'NormalBG';  else $rowstyle1 = 'AlternateBG'; $i++; 
						if ($rowp['idtransaksi'] == '0') $rowstyle1 = 'YellowBG';
						$dendaperbuku = Sipus::hitungdendaperbuku($conn,$rowp['tgltenggat'],date('Y-m-d'),$rowp['idanggota'],$rowp['ideksemplar']);
			?>
			<tr class="<?= $rowstyle1 ?>" height="30" valign="middle"> 
				<td>&nbsp; <?= $rowp['noseri'] ?></td>
				<td align="left"><?= $rowp['judul'].($rowp['judulseri']!='' ? ' : '.$rowp['judulseri'] : '').($rowp['edisi']!='' ? " / ".$rowp['edisi'] : "") ?></td>
				<td align="left">&nbsp;<?= $rowp['authorfirst1'].' '.$rowp['authorlast1'] ?></td>
				<td align="center"><?= Helper::tglEng($rowp['tgltransaksi']); ?></td>
				<td align="center"><?
				if($rowp['tgltenggat']=='')
					echo "Maksimal";
				else {
				
					if($rowp['tgltenggat']<date('Y-m-d')) { 
					echo "<font color='red'><b>" ;?>
					<script>
					var strs='<?= Helper::tglEng($rowp['tgltenggat']) ?>';
					document.write(strs.blink());
					</script>			
				<? echo "</b></font>"; 
				} else 
				  echo Helper::tglEng($rowp['tgltenggat']) ; 
				 }?>
				</td>
				<?if($dendaperbuku==0){?>
					<td align="center"><font color="green"><b>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</b></font></td>
				<?}else{?>
					<td align="center"><font color="red"><b><blink>Rp. <?= Helper::formatNumber($dendaperbuku); ?>,-</blink></b></font></td>
				<?}?>
			</tr>
			<?php
			}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Tidak ada peminjaman.</b></td>
			</tr>
			<?php } ?>
				<tr>
					<td colspan="6" class="footBG">&nbsp;</td>
				</tr>
			
		</table>
		</div>

		</div>
		<? } ?>

		</td>
		</tr>
		</table>
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key2" id="key2">
		<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
		<input type="hidden" name="ideks" id="ideks">
		<input type="hidden" name="keydate" id="keydate" value="<?= $_SESSION['waktu_denda'] ?>">

		<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">

		</form>

		<form name="test" id="test" target="_blank">
		<input type="hidden" name="test2" id="test2" value="<?= $_SESSION['reserve_denda'] ?>">
		</form>
		</div>
	</div>
</div>

</body>


<script language="javascript">
$(document).ready(function() {

	
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	var key=document.getElementById("key3").value;
	
	if (key==0 || key == ''){
		chooseTab(0);
	}
	else if (key==1){
		chooseTab(1);
	}
	else if (key == 2){ 
		chooseTab(2);
	}

});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>


<? //if ($_SESSION['telat']>0){ ?>
		<script type="text/javascript">	
		// var skorr=confirm('Pengembalian terlambat, apakah dilakukan skorsing?');
		// if (skorr)
			// alert("Keterlambatan pengembalian <?= $_SESSION['telat'] ?> hari, Skorsing sampai <?= Helper::tglEng($_SESSION['skors']) ?>");
		// else{
			// document.getElementById('act').value='freeskor';
			// goSubmit();
		// }
		</script>
	<? //unset($_SESSION['telat']);unset($_SESSION['skors']);
 //} ?>


<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script language="javascript">
function etrTrans(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btntrans").click();
}
function etrPinjam(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka").click();
		//goPinjam();
}
function etrPinjamKhusus(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnpustaka2").click();
		//goPinjam();
}
function etrReserve(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btnreserve").click();
		//goReservasi();
}
function goClear() { 
document.getElementById("act").value='delsession';
goSubmit();

}

function goTrans() {
	var idanggota = $("#txtid").val();


	if(idanggota=='') {
		$("#span_error").show();
		setTimeout('$("#span_error").hide()',2000);
		return false; } 
	else {
		document.getElementById("act").value= 'stanby';
		goSubmit();
		}		
}

function goPinjam() {
	if(cfHighlight("txtpustaka")){
		document.getElementById("act").value='pinjam';
		goSubmit();
	}
		
}

function goPinjamKhusus() {
	if(cfHighlight("txtpustaka2")){
		document.getElementById("act").value='pinjamkhusus';
		goSubmit();
	}		
}

function goBatal($key,$eks) {
		var batalno=confirm("Apakah Anda yakin akan membatalkan transaksi ini ?");
		if(batalno){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}

function goBatalR($key,$eks) {
		var batalR=confirm("Apakah Anda yakin akan membatalkan reservasi ini ?");
		if(batalR){
		document.getElementById("act").value='batalR';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
		
}

function goPanjang($key,$eks,$ideks) {
		var panjang=confirm("Apakah Anda yakin akan memperpanjang transaksi ini ?");
		if(panjang){
		document.getElementById("act").value='panjang';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		document.getElementById("ideks").value=$ideks;
		goSubmit();
		}
}

function goKembali($key,$eks,$ideks) {
		var kembalikan=confirm("Apakah Anda yakin akan melakukan proses pengembalian ?");
		if(kembalikan){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		document.getElementById("ideks").value=$ideks;
		goSubmit();
		}
}

function goReservasi(){
		document.getElementById("act").value='pesen';
		goSubmit();
}

function goTab(key3) {
		document.getElementById("key3").value=key3;
		//document.perpusform.submit();	
}

function goFreeSkors(key) {
	var free = confirm("Apakah anda yakin akan membayarkan denda Id Anggota "+key+" ?");
	if(free) {
		document.getElementById("act").value = "freeskors";
		document.getElementById("key").value = key;
		goSubmit();
	}
}

function initPage() {
	//initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	var x=document.getElementById("key3").value;
	if(x=='0')	
	document.getElementById('txtpustaka').focus();
	else if(x=='1')
	document.getElementById('txtreservasi').focus();
	else
	document.getElementById('txtid').focus();
}
function chCol() {
	var selected = document.getElementById('khs').className;
	if (selected == 'selected'){
		document.getElementById('khs').firstChild.style.backgroundColor = "#383838";
		}
	else {
		document.getElementById('khs').firstChild.style.backgroundColor = "#000";
	}
}
</script>
</html>

<? if(Helper::getFlashData('sucdb')!='' and Helper::getFlashData('sucdb')!='Peminjaman berhasil.' 
	and Helper::getFlashData('sucdb')!='Proses transaksi telah selesai' and Helper::getFlashData('sucdb')!='Transaksi sukses dibatalkan.') { ?>
		<script type="text/javascript">	
			alert('<?= Helper::getFlashData('sucdb') ?>');
		</script>
<? }elseif (Helper::getFlashData('errdb')){ ?>
	<script type="text/javascript">
	    alert('<?= Helper::getFlashData('errdb') ?>');
	</script>
<? } ?>