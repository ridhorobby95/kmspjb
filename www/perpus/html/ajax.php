<?php
	$f = Helper::cAlphaNum($_REQUEST['f']);
	$q = Helper::removeSpecial($_REQUEST['q']); // untuk xautox
	$term = Helper::removeSpecial($_REQUEST['term']); // untuk xautox
	if($f=='pengarang') 
		getPengarang($q);
	else if ($f == 'anggota')
		getAnggota($term);
	
	function getAnggota($str){ 
		
		global $conn;
		$str = strtolower($str);
		
		$sql="select  idanggota, idanggota||'-'||namaanggota  as namaanggota
				from perpus.ms_anggota 
				where lower(idanggota||'-'||namaanggota) like '%$str%' and rownum <= 20";
		$rs = $conn->getArray($sql);
		
		$data =array();
		foreach ($rs as $row)
			$data[] = array('key'=>$row['idanggota'],'value'=>$row['namaanggota']);
		
		echo json_encode($data);	
	
	}
	
	function getPengarang($str) {
		global $conn;
		
		$a_sort = array();
		$a_data = array();
		
		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM (";
		$sql .= " select idauthor, namadepan||'   '||namabelakang as namapengarang from ms_author where lower(namadepan||' '||namabelakang) like '%".strtolower($str)."%'";
		$sql .= " )  inner_query WHERE rownum <= 100) ";

		$rs = $conn->Execute($sql);
		
		while($row = $rs->FetchRow()) {
			$a_sort[] = $row['namapengarang'];
			$a_data[] = array('kode' => $row['idauthor'], 'nama' => $row['namapengarang']);
		}
		
		sort($a_sort);
		
		$menu = '<response>';
		if(!empty($a_sort)) {
			foreach($a_sort as $k => $v) {
				$menu .= '<item><kode>'.$a_data[$k]['kode'].'</kode><nama>'.$a_data[$k]['nama'].'</nama></item>'; // dibentuk seperti xml :D
			}
		}
		$menu .= '</response>';
		
		 /* header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="iso-8859-1"?>'; */
		echo $menu;
	}
?>
