<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		

	// definisi variabel halaman
	$p_dbtable = 'ms_supplier';
	$p_window = '[PJB LIBRARY] Daftar Track Pagu';
	$p_title = 'Daftar Track Pagu';
	$p_tbheader = '.: Daftar Track Pagu :.';
	$p_col = 9;
	$p_tbwidth = 1200;
	$p_filedetail = Helper::navAddress('repp_pagu.php');
	$p_id = "daftrack";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	//$p_id = Helper::navAddress('list_struktural');
	$p_defsort = 'u.tglusulan';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// variabel get
	$getId=Helper::removeSpecial($_GET['id']);
	
	if($getId==''){
	   echo "<script>window.close()</script>";
	}
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select pu.*,o.judul,u.namapengusul,u.tglusulan,lh.sisapagu,lh.sisasementara from pp_paguusulan pu
				 join pp_orderpustaka o on pu.idorderpustaka=o.idorderpustaka
				 join pp_usul u on o.idusulan=u.idusulan
				 join pp_pagulh lh on pu.idanggota=lh.idanggota and pu.periodeakad=lh.periodeakad
				 where pu.idanggota='$getId' and pu.void=0";
  	
	// pengaturan ex
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi=='filter'){
			$periode=Helper::removeSpecial($_POST['periode']);
			if($periode!=''){
				$p_sqlstr .=" and pu.periodeakad='$periode'";
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$rs_cb = $conn->Execute("select distinct(periodeakad), (periodeakad) as periode from pp_paguusulan where idanggota='$getId' order by periodeakad desc");
	$l_periode = $rs_cb->GetMenu2('periode',$periode,true,false,0,'id="periode" class="ControlStyle" onchange="goSelect()" style="width:150"');
	$l_periode = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_periode);	
	$rsc=$rs->RowCount()
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php //include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post">
<table width="650">
	<tr height="40">
		<td align="center" class="PageTitle"><?= $p_title; ?></td>
	</tr>
	<tr>
		<td align="center" valign="bottom">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_add) { ?>
				<td width="170" align="center">
				<a href="<?= $p_filedetail."&id=".$rs->fields['idanggota']."&periode=".$periode ; ?>" target="_blank" class="button"><span class="print">
				Cetak List</span></a></td>
				<? } ?>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<br>

<table width="650" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	<tr>
		<td>Filter</td>
		<td><?= $l_periode ?></td>
	</tr>
	<tr height="20"> 
		<td width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.tglusulan:C');">Tanggal<?= $p_xsort['tglusulan']; ?></td>		
		<td width="250" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','o.judul:C');">Judul Pustaka<?= $p_xsort['judul']; ?></td>
		<td width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','pu.paguusulan:C');">Pagu Dipakai<?= $p_xsort['paguusulan']; ?></td>
		<td width="150" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.namapengusul:C');">Pengusul Utama<?= $p_xsort['namapengusul']; ?></td>
	<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['idpaguusulan'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>"> 
		<td align="center"><?= Helper::tglEng($row['tglusulan']); ?></td>		
		<td align="left"><?= $row['judul']; ?></td>
		<td align="right">&nbsp;<?= Helper::formatNumber($row['paguusulan'],'0',true,true); ?></td>
		<td align="left"><?= $row['namapengusul']; ?></td>		
	</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<? if($periode!='') { ?>
	<tr height=25>
   <td colspan=6>
   <b>Jumlah Pemakaian: <?= $rsc ?>&nbsp;&nbsp;&nbsp; | 
   &nbsp;&nbsp;&nbsp;Sisa Pagu Real: 
   <?= Helper::formatNumber($rs->fields['sisapagu'],'0',true,true) ?>
   &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Sisa Pagu Usulan :
   <?= Helper::formatNumber($rs->fields['sisasementara'],'0',true,true) ?>
   </b></td></tr>
   <? } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
	</tr>
</table><br>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="act" id="act">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>
<script type="text/javascript">
function goSelect(){
	document.getElementById('act').value='filter';
	goSubmit();
}
</script>
</html>