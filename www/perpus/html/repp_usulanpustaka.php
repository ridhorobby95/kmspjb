<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and (a.idunit in ($units) or u.idunit in ($units)) ";
		$sqlAdminUnit_b = " and a.idunit in ($units) ";
		$sqlAdminUnit_u = " and u.idunit in ($units) ";
	}
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_status = Helper::removeSpecial($_POST['status']);
	$r_jenis = Helper::removeSpecial($_POST['jenis']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_lokasi){
		$slokasi = " and (a.idunit = '$r_lokasi' or u.idunit = '$r_lokasi')) ";
		$slokasi_b = " and a.idunit = '$r_lokasi' ";
		$slokasi_u = " and u.idunit = '$r_lokasi' ";
	}

	if($r_format=='' or $r_jenis=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pemesanan Pustaka';
	
	$p_namafile = 'rekap_'.$r_jenis.'_'.$r_status.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql = "select o.*, u.*, u.vote_usulan
	from pp_orderpustaka o
	join pp_usul u on o.idusulan=u.idusulan
	left join ms_anggota a on a.idanggota = u.idanggota  
	where to_char(u.tglusulan,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2'";
	
	if ($r_jenis=='0'){
		$sql .=" and u.idunit is null";
		$sql .= $slokasi_b;
		$sql .= $sqlAdminUnit_b;		
	}
	elseif ($r_jenis=='1'){
		$sql .=" and u.idunit is not null";
		$sql .= $slokasi_u;
		$sql .= $sqlAdminUnit_u;		
	}
	else{
		$sql .= $slokasi;
		$sql .= $sqlAdminUnit;		
	}
	
	$sql .=" order by u.tglusulan desc";
	$row = $conn->Execute($sql);
	$rsj = $row->RowCount();
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=996>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Pengusulan Pustaka yang Telah di Proses</h2>
  	</strong></td>
  </tr>
    <tr>
	<td>Periode : <?= Helper::formatDateInd($r_tgl1) ?> s/d <?= Helper::formatDateInd($r_tgl2) ?></td>
	</tr>
</table>
<table width="1000" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
    <th width="150" align="center"><strong>Judul Pustaka</strong></th>
    <th width="130" align="center"><strong>Pengarang</strong></th>
	<th width="100" align="center"><strong>Penerbit</strong></th>
	<th width="100" align="center"><strong>ISBN</strong></th>
	<th width="60" align="center"><strong>Tahun Terbit</strong></th>
	<th width="90" align="center"><strong>Tanggal Usulan</strong></th>
	<th width="100" align="center"><strong>Harga Usulan</strong></th>
	<th width="100" align="center"><strong>Pengusul</strong></th>
	<th width="100" align="center"><strong>Keterangan</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
	<td align="left"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?></td>
	<td align="left"><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
	<td align="left"><?= $rs['penerbit']?></td>
	<td align="left"><?= $rs['isbn']?></td>
	<td align="center"><?= $rs['tahunterbit']?></td>
	<td align="center"><?= Helper::formatDateInd($rs['tglusulan']) ?></td>
	<td align="right"><?= $rs['hargausulan']!='' ? Helper::formatNumber($rs['hargausulan'],'0',true,true) : '-' ?></td>
	<td align="left"><?= $rs['ispengadaan']==0 ? $rs['namapengusul'] : $rs['namapengusul']."<br>Id Unit : ".$rs['idunit']  ?></td>
	<td align="left"><?= $rs['keterangan'] ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=11><b>Jumlah Pengusulan : <?= $rsj ?><b></td></tr>
</table>


</div>
</body>
</html>