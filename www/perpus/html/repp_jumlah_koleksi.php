<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	//definisi hari,bulan,tahun
	$sekarang = date('d F Y');
	
	$kepala = Sipus::getHeadPerpus();
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	$r_param = Helper::removeSpecial($_REQUEST['param']);
	$r_bulan = Helper::removeSpecial($_REQUEST['r_bulan']);
	$r_tahun = Helper::removeSpecial($_REQUEST['tahun2']);
	$r_jenispustaka = Helper::removeSpecial($_REQUEST['kdjenispustaka']);
	
	if($r_param == 'semua'){
		if($r_tahun)
			$bulantahun = "Tahun ".$r_tahun;
		else	
			$bulantahun = "Sampai Dengan ".date('M Y');
		
	}else{
		if($r_bulan=='01')
			$bln = 'Januari';
		if($r_bulan=='02')
			$bln = 'Februari';
		if($r_bulan=='03')
			$bln = 'Maret';
		if($r_bulan=='04')
			$bln = 'April';
		if($r_bulan=='05')
			$bln = 'Mei';
		if($r_bulan=='06')
			$bln = 'Juni';
		if($r_bulan=='07')
			$bln = 'Juli';
		if($r_bulan=='08')
			$bln = 'Agustus';
		if($r_bulan=='09')
			$bln = 'September';
		if($r_bulan=='10')
			$bln = 'Oktober';
		if($r_bulan=='11')
			$bln = 'Nopember';
		if($r_bulan=='12')
			$bln = 'Desember';
		$bulantahun = $bln." ".$r_tahun;
	}
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Jumlah Koleksi Buku di Perpustakaan PJB';
	
	if($r_param == 'semua'){
		$p_namafile = 'Laporan Jumlah Koleksi Buku Sampai_'.$bulantahun;
	}
	else
		$p_namafile = 'Laporan Jumlah Koleksi Buku Bulan_'.$r_bulan.'_'.$r_tahun;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//melakukan query mendapatkan jumlah koleksi per DDC untuk judul dan eksemplar.
	//yang di select adalah semua koleksi yang dimiliki hingga saat ini.
	if($r_param == 'semua'){
		$filter = '';
		if($r_tahun){
			$filter .= " and 1=1 and date_part('year',e.tglperolehan) = '$r_tahun'";
		}
	}
	else{
		$filter = " and 2=2 and date_part('year',e.tglperolehan) = '$r_tahun'";
		if($r_bulan)
			$filter .= " and date_part('month',e.tglperolehan) = '$r_bulan' ";
	}
	
	if($r_jenispustaka != ''){
		$filter .= " and p.kdjenispustaka = '$r_jenispustaka' ";
		$jp = $conn->GetOne("select namajenispustaka from lv_jenispustaka where kdjenispustaka = '$r_jenispustaka' ");
	}
	
	$sql = "select g.golongan, g.kriteria, g.awal, g.akhir, count(distinct p.idpustaka) jumlah_judul, count(e.ideksemplar)  jumlah_eksemplar 
		from lv_golonganpustaka g 
		left join ms_pustaka p on substring(p.kodeddc,1,3) between g.awal and g.akhir 
		left join pp_eksemplar e on e.idpustaka = p.idpustaka 
		where 1=1 $filter
		group by golongan, g.kriteria, g.awal, g.akhir
		order by golongan ";

	$rs = $conn->Execute($sql);
	
	//membuat array yang dibutuhkan.
	$koleksi = array();
	$gol = array();

	//membuat array untuk mennyimpan hasil Execute SQL ke array.
	//Ini dilakukan agar bila ada FetchRow() yang kosong tetap bisa muncul di tabel.
	$a_jumlahjudul = array();
	$a_jumlaheksemplar = array();
	$t_judul = array();
	$t_eksemplar = array();
	
	//mengisi array dengan ketentuan $array[judul(0)/eksemplar(1)][KodeDDC].
	
	$rs3 = $conn->Execute($sql);
	while($row = $rs3->FetchRow()) {
		$gol[] = $row['golongan'];
		$t_judul[0][$row['golongan']] = $row['jumlah_judul'];
		$t_eksemplar[1][$row['golongan']] = $row['jumlah_eksemplar'];		
	}
	
	$i = 0;
	$rs2 = $conn->Execute($sql);
	while($row = $rs2->FetchRow()) 
	{
		// persiapan data-data untuk membuat grafik
		$a_jumlahjudul[$gol[$i]] = $t_judul[0][$row['golongan']];
		$a_jumlaheksemplar[$gol[$i]] =  $t_eksemplar[1][$row['golongan']];
		
		//untuk SUM judul dan eksemplar
		$jumlahjudul +=  $row['jumlah_judul'];
		$jumlaheks += $row['jumlah_eksemplar'];
		$i++;
	}
	
	$_SESSION['_DATA_DDC'] = $gol;
	$_SESSION['_DATA_KOLEKSIJUDUL'] = $a_jumlahjudul;
	$_SESSION['_DATA_KOLEKSIEKSEMPLAR'] = $a_jumlaheksemplar;
	$_SESSION['jumlahjudul'] = $jumlahjudul;
	$_SESSION['jumlaheksemplar'] = $jumlaheks;
	
	
?>

<html>
<head>
<script type="text/javascript" src="includes/FusionCharts/JSClass/FusionCharts.js" ></script>

	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Jumlah Koleksi <?=$jp;?> di Perpustakaan PJB <?= $bulantahun ?></h2>
  	</strong></td>
  </tr>
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()">Cetak Laporan</a></td>
	</tr>
	<? } ?>
</table>


<table width="800" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th rowspan="2" rowspan="2" width="10" align="center"><strong>No.</strong></th> 
	<th rowspan="2" rowspan="2" width="10" align="center"><strong>Golongan</strong></th>    
        <th rowspan="2" width="150" align="center"><strong>Macam Koleksi</strong></th>
        <th colspan="2" width="130" align="center"><strong>Jumlah</strong></th>
  </tr>
	<tr height=25>   
        <th width="150" align="center"><strong>Judul</strong></th>
        <th width="130" align="center"><strong>Eksemplar</strong></th>
  </tr>
	
	<!-- sintaks-sintaks dibawah ini akan memunculkan jumlah judul dan eksemplar termasuk bila jumlah itu adalah 0 -->
	<!-- Ingat! Ketentuan $array[judul(0)/eksemplar(1)][KodeDDC] -->
	<?	$in=0;
		while ($row = $rs->FetchRow())  { $in++;?>
		<tr height=25>
		    <td align="left"><?=$in; ?>. </td>
		    <td align="center"><?= $row['golongan'];?></td>
		    <td align="left"><?= $row['kriteria'];?></td>
		    <td align="right"><?= Helper::formatNumber($row['jumlah_judul']); ?></td>
		    <td align="right"><?= Helper::formatNumber($row['jumlah_eksemplar']); ?></td>
		</tr>
	<? } ?>
	<!-- menampilkan SUM judul dan eksemplar -->
	<tr height=25>
		<td colspan="3" align="center"><b> Jumlah</b> </td>
		<td align="right"><b> <?= Helper::formatNumber($jumlahjudul) ?> </b></td>
		<td align="right"><b> <?= Helper::formatNumber($jumlaheks) ?> </b></td>
	</tr>
</table><br><br>


<!-- Menampilkan grafik -->
<? if($r_format == 'html') { ?>
<table>
	<? if($r_grafik == 'batang') { ?>
	<tr>
		<td align="center"> Grafik Batang Jumlah Judul Koleksi <?=$jp;?> </td>
	</tr>
	<? } ?>
	
	<? if($r_grafik == 'pie') { ?>
	<tr>
		<td align="center"> Grafik Pie Jumlah Judul Koleksi <?=$jp;?> </td>
	</tr>
	<? } ?>
	
	<tr>
		<td align="center">
		<? if($r_grafik == 'batang') { ?>
		<img src="<?= Helper::navAddress('img_bar_koleksijudul') ?> ">
		<? } ?>
		
		<? if($r_grafik == 'pie') { ?>
		<img src="<?= Helper::navAddress('img_pie_koleksijudul') ?> ">
		<? } ?>
		</td>
		
	</tr>
	<br>
		<br>
			<br>
	
	<? if($r_grafik == 'batang') { ?>			
	<tr>
		<td align="center"> Grafik Batang Jumlah Eksemplar Koleksi <?=$jp;?> </td>
	</tr>
	<? } ?>
	
	<? if($r_grafik == 'pie') { ?>			
	<tr>
		<td align="center"> Grafik Pie Jumlah Eksemplar Koleksi <?=$jp;?> </td>
	</tr>
	<? } ?>
	
	<tr>
		<td align="center">
		<? if($r_grafik == 'batang') { ?>
		<img src="<?= Helper::navAddress('img_bar_koleksieksemplar') ?> ">
		<? } ?>
		
		<? if($r_grafik == 'pie') { ?>
		<img src="<?= Helper::navAddress('img_pie_koleksieksemplar') ?> ">
		<? } ?>
		</td>
		
	</tr>
</table>

<? } ?>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?></b></td></tr>
	<tr valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>