<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Buku Tamu Pengunjung';
	
	$p_namafile = 'laporan_bukutamu_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(*) as jumlah, idunit
		from pp_bukutamu
		where to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')";
	$sql .=" group by idunit";
	$rs = $conn->Execute($sql);
	
	while($row=$rs->FetchRow()){
		$ArJur[$row['idunit']] = $row['jumlah'];	
	}
	
	$s_fakultas = $conn->Execute("select * from ms_satker order by namasatker");

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Laporan Kunjungan Per Unit
	<br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
	<tr height=30>
		<th rowspan="2" nowrap width="50%" align="center"><b>Unit</b</th>
		<th rowspan="2" nowrap width="20%" align="center"><b>Jumlah Pengunjung</b</th>
		<th colspan="5" nowrap width="20%" align="center" style="background: #aaa;"><b>Aktivitas</b</th>
	</tr>
	<tr>
		<th nowrap width="10%" align="center" style="background: #aaa;"><b>Baca</b</th>
		<th nowrap width="10%" align="center" style="background: #aaa;"><b>Sharing</b</th>
		<th nowrap width="20%" align="center" style="background: #aaa;"><b>Pinjam Buku</b</th>
		<th nowrap width="20%" align="center" style="background: #aaa;"><b>Scan</b</th>
		<th nowrap width="20%" align="center" style="background: #aaa;"><b>Lain-lain</b</th>
	</tr>
	<? $total=0; while($rowf=$s_fakultas->FetchRow()){ 
	$idu=$rowf['kdsatker'];
	$baca = $conn->GetOne("select count(b.baca) from pp_bukutamu b where b.idunit='$idu' and b.baca=1 and to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')");
	$sharing = $conn->GetOne("select count(b.sharing) from pp_bukutamu b where b.idunit='$idu' and b.sharing=1 and to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')");
	$pinjam = $conn->GetOne("select count(b.pinjam_buku) from pp_bukutamu b where b.idunit='$idu' and b.pinjam_buku=1 and to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')");
	$scan = $conn->GetOne("select count(b.scan) from pp_bukutamu b where b.idunit='$idu' and b.scan=1 and to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')");
	$lainlain = $conn->GetOne("select count(b.lainlain) from pp_bukutamu b where b.idunit='$idu' and b.lainlain=1 and to_date(to_char(tanggal,'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$r_tgl1','YYYY-mm-dd') and to_date('$r_tgl2','YYYY-mm-dd')");

//var_dump($rowf['kdsatker']);
	?>
	<tr>
		<td><?=$rowf['namasatker'];?></td>
		<td align="center"><?=number_format($ArJur[$rowf['kdsatker']],0, ',', '.');?></td>
		<td align="center"><?= $baca ?></td>
		<td align="center"><?= $sharing ?></td>
		<td align="center"><?= $pinjam ?></td>
		<td align="center"><?= $scan ?></td>
		<td align="center"><?= $lainlain ?></td>
	</tr>
	<? $total +=$ArJur[$rowf['kdsatker']];
		$totBaca +=$baca;
		$totSharing +=$sharing;
		$totPinjam +=$pinjam;
		$totScan +=$scan;
		$totLainlain +=$lainlain;
	} ?>
	<tr height=30>
		<td align="center"><b>JUMLAH TOTAL</b></td>
		<td align="center"><b><?= number_format($total,0, ',', '.') ?></b></td>
		<td align="center"><b><?= number_format($totBaca,0, ',', '.') ?></b></td>
		<td align="center"><b><?= number_format($totSharing,0, ',', '.') ?></b></td>
		<td align="center"><b><?= number_format($totPinjam,0, ',', '.') ?></b></td>
		<td align="center"><b><?= number_format($totScan,0, ',', '.') ?></b></td>
		<td align="center"><b><?= number_format($totLainlain,0, ',', '.') ?></b></td>
	</tr>
</table>
<br/><br/><br/>
</div>
</body>
</html>