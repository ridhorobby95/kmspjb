<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and (a.idunit in ($units) or u.idunit in ($units)) ";
	}
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_lokasi){
		$slokasi_b = " and a.idunit = '$r_lokasi' ";
		$slokasi_u = " and u.idunit = '$r_lokasi' ";
	}
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Buku Datang';
	
	$p_namafile = 'rekap_bukudatang_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	$sql = "select * from pp_orderpustakattb ot
			join pp_ttb t on ot.idttb=t.idttb and t.jnsttb not in(2,3)
			join pp_orderpustaka o on ot.idorderpustaka=o.idorderpustaka
			join pp_usul u on o.idusulan=u.idusulan
			left join ms_anggota a on a.idanggota = u.idanggota
			left join ms_supplier s on o.supplierdipilih=s.kdsupplier
			where to_char(t.tglttb,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' $sqlAdminUnit ";
	
	if($r_jenis==0){
		$sql .=" and u.idunit is null ";
		$sql .= $slokasi_b;
	}elseif($r_jenis==1){
		$sql .=" and u.idunit is not null ";
		$sql .= $slokasi_u;
	}
	
	$sql .=" order by t.tglttb ";
	$row = $conn->Execute($sql);
	$rsj = $row->RowCount();
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=900>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=900 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Tanda Terima Pustaka</h2>
  	</strong></td>
  </tr>
    <tr>
	<td>Periode : <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="900" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
    <th width="130" align="center"><strong>Pengarang</strong></th>
	<th width="90" align="center"><strong>Tanggal</strong></th>
	<th width="100" align="center"><strong>Harga Satuan</strong></th>
	<th width="100" align="center"><strong>Jumlah Eksemplar</strong></th>
	<th width="100" align="center"><strong>Harga Total</strong></th>
	<th width="100" align="center"><strong>Keterangan</strong></th>

  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{
		$hargatotal = intval($rs['qtyttbdetail'])*intval($rs['hargadipilih']);
		$tot = $tot + $hargatotal;
	?>
    <tr height=25>
	<td align="center" valign="top"><?= $no ?></td>   
	<td align="left" valign="top"><?= $rs['judul'].($rs['edisi']!='' ? " / ".$rs['edisi'] : '') ?></td>
	<td align="left" valign="top"><?= $rs['authorfirst1']." ".$rs['authorlast1'] ?></td>
	<td align="center" valign="top"><?= Helper::tglEngTime($rs['tglttb']) ?></td>
	<td align="right" valign="top"><?= $rs['hargadipilih']!='' ? Helper::formatNumber($rs['hargadipilih'],'0',true,true) : '-' ?>&nbsp;</td>
	<td align="right" valign="top"><?= $rs['qtyttbdetail'] ?></td>
	<td align="right" valign="top"><?= $hargatotal!='' ? Helper::formatNumber($hargatotal,'0',true,true) : '-' ?>&nbsp;</td>
	<td align="left" valign="top"><?= $rs['keterangan'] ?></td>

  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25>
	<td colspan=5><b>Jumlah Buku: <?= $rsj ?><b></td>
	<td><b>Total Harga: <b></td>
	<td colspan=2><b><?= Helper::formatNumber($tot,'0',true,true) ?><b></td>
   </tr>
</table>
<br/><br/><br/>

</div>
</body>
</html>