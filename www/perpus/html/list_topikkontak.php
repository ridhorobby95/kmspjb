<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	$rkey = Helper::removeSpecial($_REQUEST['rkey']);
	$r_key = Helper::removeSpecial($_REQUEST['key']);
		
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	// definisi variabel halaman
	$p_dbtable = 'lv_kontak';
	$p_window = '[PJB LIBRARY] Daftar Topik Kontak Online';
	$p_title = 'Daftar Sub Topik Kontak Online';
	$p_tbheader = '.: Daftar Sub Topik Kontak Online :.';
	$p_col = 5;
	$p_tbwidth = 700;
	$p_filedetail = Helper::navAddress('ms_topikkontak.php');
	$p_filesubdetail = Helper::navAddress('list_post.php');
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable where idparent='$rkey' order by namakontak";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['namakontak'] = Helper::cStrNull($_POST['i_namakontak']);
			$record['keterangan'] = Helper::cStrNull($_POST['i_keterangankontak']);
			$record['isaktif'] = Helper::cStrNull($_POST['i_isaktif']);
			$record['idparent'] = Helper::cStrNull($rkey);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {	
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key.'&rkey='.$rkey;
				Helper::redirect($url);
			}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['namakontak'] = Helper::cStrNull($_POST['u_namakontak']);
			$record['keterangan'] = Helper::cStrNull($_POST['u_keterangankontak']);
			$record['isaktif'] = Helper::cStrNull($_POST['u_isaktif']);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idkontak,$r_key);
			
			if($err != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key.'&rkey='.$rkey;
				Helper::redirect($url);	
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idkontak,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&rkey='.$rkey;
				Helper::redirect($url);	
			}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/button.css">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><h1 style="font-weight:normal;color:#015593;"><?= $p_title ?></h1></td>
			</tr>
		</table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<table width="<?= $p_tbwidth; ?>">
			<tr>
				<td align="center" valign="bottom">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<? if($c_add) { ?>
							<td width="170" align="center">
							<a href="javascript:goBack('<?= $p_filedetail; ?>')" class="button"><span class="list">
							Daftar Kontak</span></a></td>
							<? } ?>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<? include_once('_notifikasi.php'); ?>
		<header style="width:700px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<tr height="20"> 
				<td width="10" nowrap align="center" class="SubHeaderBGAlt thLeft">No.</td>
				<td width= "30%" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Kontak</td>
				<td width= "60%" nowrap align="center" class="SubHeaderBGAlt thLeft">Keterangan</td>
				<td width="10" nowrap align="center" class="SubHeaderBGAlt thLeft">isAktif?</td>
				<? if($c_edit or $c_delete) { ?>
				<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
				<? } ?>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idkontak'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['idkontak']; ?>');" class="link"><?= $i; ?></u>
				<? } else { ?>
					<?= $i; ?>
				<? } ?>
				</td>
				<td><?= $row['namakontak']; ?></td>
				<td><?= $row['keterangan']; ?></td>
				<td align="center"><?= $row['isaktif'] == '1' ? '<img src="images/centang.gif">' : '' ; ?></td>
				<? if($c_delete or $c_edit) { ?>
				<td align="center">
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['idkontak']; ?>','<?= $row['namakontak']; ?>');" class="link">[hapus]</u>
				</td><? } ?>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= $i; ?></td>
				<td align="center"><?= UI::createTextBox('u_namakontak',$row['namakontak'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextArea('u_keterangankontak',$row['keterangan'],'ControlStyle',2,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="u_isaktif" id="u_isaktif" value="1" <?= $row['isaktif']=='1' ? 'checked':''; ?>></td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['idkontak']; ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= $i+1; ?></td>
				<td align="center"><?= UI::createTextBox('i_namakontak','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextArea('i_keterangankontak','','ControlStyle',3,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="i_isaktif" id="i_isaktif" value="1"></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php } ?>
			
			<tr>
				<td class="footBG" colspan="<?=$p_col;?>">&nbsp;</td>
			</tr>
		</table><br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="rkey" id="rkey" value="<?= $rkey; ?>">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namakontak').focus();
	<? } else { ?>
	document.getElementById('i_namakontak').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_namakontak,i_isaktif"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_isaktif,u_namakontak")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

function goBack(file){
	document.getElementById("perpusform").action = file;
	goSubmit();
}

function goDetail(file,key,rkey){
	document.getElementById("perpusform").action = file;
	document.getElementById("rkey").value = key;
	document.getElementById("key").value = rkey;
	document.getElementById("perpusform").submit();
}


</script>
</html>