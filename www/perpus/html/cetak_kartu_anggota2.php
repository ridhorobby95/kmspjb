<?php
	 $conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
	// require tambahan
	
	$r_id=Helper::removeSpecial($_GET['id']);
	$start=Helper::removeSpecial($_GET['start']);	
	$total = explode("|",$start);
	
	$str = "("; //$loop=0;
	for($k=0;$k<count($total);$k++){
		if($k == count($total)-2){
			$str .= "'$total[$k]'";
		}else if($k == count($total)-1 ){
			$str .= ")";
		}else{
			$str .= "'$total[$k]',";
		}
		// $loop++;
	}
	// var_dump($str);
	
	$rs_anggota=$conn->Execute("select idanggota,namaanggota,alamat,asal,noika,tglexpired,tgldaftar,lj.namajurusan,lf.namafakultas,lf.singkatan,ja.singkatan as singkatan_a from ms_anggota a 
						left join lv_jurusan lj on lj.kdjurusan=a.idunit left join lv_jenisanggota ja on ja.kdjenisanggota=a.kdjenisanggota
						left join lv_fakultas lf on lf.kdfakultas=lj.kdfakultas where idanggota in $str order by idanggota asc");
	
	$user= $conng->GetRow("select userdesc from gate.sc_user where username='".$_SESSION['PERPUS_USER']."'");
	
	// $p_foto = Config::dirFoto.'anggota/'.trim($anggota['idanggota']).'.jpg';
	// $p_hfoto = Config::fotoUrl.'anggota/'.trim($anggota['idanggota']).'.jpg';
?>

<html>
<head>
<title>Kartu Anggota</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="printpage()"> <!--onload="printpage()"-->
<?php $k=1; while ($anggota = $rs_anggota->FetchRow()){
	$p_foto = Config::dirFoto.'anggota/'.trim($anggota['idanggota']).'.jpg';
	$p_hfoto = Config::fotoUrl.'anggota/'.trim($anggota['idanggota']).'.jpg';
?>	
	
	<?if($k%5==0){?>
	<div style="page-break-after:always">
	<?}?>
	<table cellpadding="8" >
	<tr><td>
		<table width="300" cellpadding="1" cellspacing="0" border="0" style="background:#FFF;border:1pt solid #000;-moz-border-radius:8px;padding:2px;margin-top:2px;" > <!-- style="background:#FFF;border:1pt solid #d2d2d2 double black;-moz-border-radius:5px;padding:10px;margin-top:10px;"-->
		<tr>
		<td>
			<table height="180" width="320" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td rowspan="5" align="center" valign="top" width="200"><img src="images/perpustakaan/logo_kartu.png" width="170" height="50">
						<br><font size="-3">Kampus Ketintang Surabaya 60231 <br> Telp.(031) 8280009 Pst. 105-137 <br>Fax.(031)8287740 </font>
						<br><img src="images/perpustakaan/garis.png" width="200" height="15">
						<br><font size="2" face="arial"><b><?= $anggota['namaanggota']?></b></font>
						<?if(strlen($anggota['alamat']) >= 45){?>
						<br><font size="-3"><?= $anggota['alamat']?></font>
						<?}else{?>
						<br><font size="1"><?= $anggota['alamat']?></font>
						<?}?>
						<br><font size="1"><?= $anggota['singkatan'].'/'.$anggota['namajurusan'] ?></font>
						<br><img height="50" nowrap src="index.php?page=image_anggota&key=<?= $anggota['idanggota'] ;?>">
					</td>
					<td nowrap align="center" valign="top" width="120"><div style="background:#000;border:2pt solid #000;padding:2px;margin-top:1px;box-shadow: 2px 2px 3px #222;
						-moz-box-shadow: 2px 2px 3px #222;
						-webkit-box-shadow: 2px 2px 3px #222"><font color="white"><b><?= strtoupper($anggota['singkatan_a'])?></b></font></div>
						<p>
						<?if(strlen($anggota['idanggota'])>10){?>
							<font size="1"><b><?= $anggota['idanggota'] ?></b></font>
						<?}else{?>
							<font size="3"><b><?= $anggota['idanggota'] ?></b></font>
						<?}?>
						<br>
						<img style="padding:1px;border:1px solid #021a40;" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default1.jpg') ?>" width="90" height="110">
						<br><font size="1" color="red"><b>Berlaku sampai<br><?= Helper::formatDateInd($anggota['tglexpired']) ?></b></font>
					</td>
				</tr>
			</table>
		</td></tr>
		</table>
	</td>
	<!--<td>&nbsp;</td>-->
	<td>
		<table  width="300" cellpadding="1" cellspacing="0" border="0" style="background:#FFF;border:1pt solid #000;-moz-border-radius:8px;padding:2px;margin-top:2px;" > <!-- style="background:#FFF;border:1pt solid #d2d2d2 double black;-moz-border-radius:5px;padding:10px;margin-top:10px;"-->
		<tr><td valign="top">
			<table cellpadding="0" cellspacing="0" height="180" width="320" border="0" align="center" >
				<tr><td align="left">
					<strong><font size="1" color="#FB0808"><u> UNTUK DIPERHATIKAN </u></font></strong><br><br>
					<font size="-3">
						1. Kartu ini hanya berlaku bagi pemiliknya.<br>
						2. Penyalahgunaan terhadap kartu ini bukan menjadi <br>&nbsp;&nbsp;&nbsp;&nbsp;tanggung-jawab Perpustakaan.<br>
						3. Mentaati tata tertib yang berlaku.<br>
						4. Kartu ini harus ditunjukkan kepada Petugas saat <br>&nbsp;&nbsp;&nbsp;&nbsp;memanfaatkan Perpustakaan.<br>
						5. Jika kartu hilang, segera melaporkan ke Petugas.<br>
					</font>
					<tr><td align="right"><font size="-3" style="margin-right:12px;">Surabaya, <?php echo Helper::formatDateInd(date("Y-m-d"))?></font><br>
					<?if(strlen($anggota['alamat']) >= 30){?>
					<img src="<?= Config::fotoUrl.'stempel.png' ?>" width="123" height="77">
					<?}else{?>
					<img src="<?= Config::fotoUrl.'stempel.png' ?>" width="120" height="70">
					<?}?>
					</td></tr>
				</td></tr>
			</table>
		</td>
		</tr>
		</table>
	</td></tr>
	</table>
	<?if($k%5==0){?>
	</div>
	<?}?>
		
<?php $k++;
}?>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>