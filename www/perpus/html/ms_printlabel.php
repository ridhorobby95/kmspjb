<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	//list klasifikasi
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");
	$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi','LT',false,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:140"');
		
?>

<html>
<head>
	<title>[PJB LIBRARY] Cetak label Pustaka</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr height="40">
		<td align="center" class="PageTitle">Cetak Label Pustaka</td>
	</tr>
</table>

<form name="perpusform" id="perpusform" method="get" action="<?= $p_filelist ?>">
<table width="320" border="0" cellspacing=0 cellpadding="4" class="GridStyle">
	<tr> 
		<td colspan=2><b><u>Masukkan NO INDUK : </u></b></td>
	</tr>
	<tr> 
		<td  width="120">Klasifikasi</td>
		<td>:&nbsp;<?= $l_klasifikasi ?></td>
	</tr>
	<tr> 
		<td  width="120">NO INDUK Awal</td>
		<td>:&nbsp;<input type="text" id="txtmulai" name="txtmulai" maxlength="20"></td>
	</tr>
	<tr> 
		<td>NO INDUK Akhir</td>
		<td>:&nbsp;<input type="text" id="txtselesai" name="txtselesai" maxlength="20"></td>
	</tr>
	<tr> 
		<td colspan=2>
		<? if($c_edit){ ?>
		<input type="button" name="btnprint" value="Cetak Label" onClick="goLblPrint()" style="cursor:pointer;width:110">
		<input type="button" name="btnprintc" value="Cetak Barcode" onClick="goCodePrint()" style="cursor:pointer;width:110">
		<? } ?>
		</td>
		
		
	</tr>
</table>
<input type="hidden" name="act" id="act">


</form>
</div>
</body>
<script type="text/javascript">
function goLblPrint() {
	if(cfHighlight("txtmulai,txtselesai")){
		var kd=document.getElementById("kdklasifikasi").value;
		var mulai=document.getElementById("txtmulai").value;
		var selesai=document.getElementById("txtselesai").value;
		
		window.open('index.php?page=cetak_label&code='+kd+'&start='+mulai+'&end='+selesai+'&c=n');
	}
}
function goCodePrint() {
	if(cfHighlight("txtmulai,txtselesai")){
		var kd=document.getElementById("kdklasifikasi").value;
		var mulai=document.getElementById("txtmulai").value;
		var selesai=document.getElementById("txtselesai").value;
		
		window.open('index.php?page=cetak_label&code='+kd+'&start='+mulai+'&end='+selesai+'&c=y');
	}
}
</script>
</html>