<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	// Auth::checkRoleAuth($conng);
	
	// include tambahan
	include_once('includes/pChart/pData.class');
	include_once('includes/pChart/pChart.class');
	
	$cw = 700;
	$ch = 250;
	
	$a_data = $_SESSION['_DATA_JUMLAHJUDUL']; 
	// $a_data_eks = $_SESSION['_DATA_JUMLAHEKSEMPLAR'];
	
	// untuk mendapatkan jumlah/total judul
	$n_data = 0;
	foreach($a_data as $t_jmlobjek)
		$n_data += $t_jmlobjek;
	
	$a_judulStatistik = $_SESSION['_DATA_BULAN'];
	
	$maxy = 0;
	$a_jmlobjek = array();
	// $a_jmlobjek_eks = array();
	
	// if($_SESSION['COUNT_DATA_BULAN'] != 1){
		foreach($a_judulStatistik as $t_Judul) {
			$n_jmlobjek = Helper::cEmChg($a_data[$t_Judul],'0');
			$a_jmlobjek[] = $n_jmlobjek;
			
			// $n_jmlobjek_eks = Helper::cEmChg($a_data_eks[$t_Judul],'0');
			// $a_jmlobjek_eks[] = $n_jmlobjek_eks;

			
			// if($n_jmlobjek > $maxy || $n_jmlobjek_eks > $maxy)
			if($n_jmlobjek > $maxy)
				$maxy = $n_jmlobjek;
		}
	// }
	// else {
		// $datastatistik = Helper::cEmChg($a_data[$a_judulStatistik],'0');
		// $a_jmlobjek[] = $datastatistik;
		// $n_jmlobjek = $datastatistik+5;
		
		// $datastatistik_eks = Helper::cEmChg($a_data[$a_judulStatistik],'0');
		// $a_jmlobjek_eks[] = $datastatistik_eks;
		// $n_jmlobjek_eks = $datastatistik_eks+5;
		
		// if($n_jmlobjek > $maxy || $n_jmlobjek_eks > $maxy)
			// $maxy = $n_jmlobjek_eks;
	// }
	if(!empty($maxy)) {
		// skala y maksimal-minimal
		$maxsc = 5;
		$minsc = 2;
		
		$mod = -1;
		$cursc = $maxsc;
		while($cursc >= $minsc) {
			$mod = $maxy%$cursc;
			if($mod == 0 or $mod == $cursc-1)
				break;
			
			$cursc--;
		}
		
		if(!empty($mod)) {
			if($cursc < $minsc)
				$cursc++; // ambil skala minimal
			$maxy += ($cursc-$mod);
		}
		$nscale = $cursc;
	}
	else {
		$maxy = 1;
		$nscale = 1;
	}
	
	// definisi data set
	$DataSet = new pData;
	$DataSet->AddPoint($a_jmlobjek,'SeriePinjam');
	$DataSet->AddPoint($a_judulStatistik,'Pinjam');
	$DataSet->AddSerie('SeriePinjam');
	$DataSet->SetAbsciseLabelSerie('Pinjam');
	$DataSet->SetSerieName('Pinjam','SeriePinjam');
	$DataSet->SetXAxisName('Bulan');
	$DataSet->SetYAxisName('Jumlah Data: '.$n_data);
	
	// Initialise the graph   
	$Chart = new pChart($cw,$ch);   
	$Chart->setFontProperties('style/tahoma.ttf',8);   
	$Chart->setGraphArea(50,30,$cw-20,$ch-50);
	$Chart->setFixedScale(0,$maxy,$nscale);
	$Chart->drawFilledRoundedRectangle(7,7,$cw-3,$ch-13,5,240,240,240);   
	$Chart->drawRoundedRectangle(5,5,$cw-1,$ch-11,5,230,230,230);   
	$Chart->drawGraphArea(200,255,200,TRUE);
	$Chart->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2,TRUE);   
	$Chart->drawGrid(4,TRUE,230,230,230,50);
	
	// gambar garis 0
	$Chart->setFontProperties('style/tahoma.ttf',6);
	$Chart->loadColorPalette('style/palette.txt');
	$Chart->drawTreshold(0,143,55,72,TRUE,TRUE);   
		
	// gambar grafik
	$Chart->drawBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(),TRUE);
	$Chart->WriteValues($DataSet->GetData(),$DataSet->GetDataDescription(),'Series1');
	$Chart->WriteValues($DataSet->GetData(),$DataSet->GetDataDescription(),'Series2');
	
	//unset session bulan
	
	
	// tampilkan chart
	$Chart->Stroke();
?>