<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Reservasi Pustaka';
	
	$p_namafile = 'rekap_reserve'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.*, p.judul from pp_reservasi r
			join pp_eksemplar e on r.ideksemplarpesan=e.ideksemplar
			join ms_pustaka p on e.idpustaka = p.idpustaka
			where r.tglreservasi between '$r_tgl1' and '$r_tgl2' order by r.tglreservasi";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Rekap Reservasi Pustaka</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Id Anggota</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
	<th width="100" align="center"><strong>Tanggal Reservasi</strong></th>
	<th width="100" align="center"><strong>Status</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= $rs['idanggotapesan'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglreservasi']) ?></td>
	<td align="center"><?= $rs['statusreservasi']==0 ? "Terealisasi" : ($rs['tglexpired']>date('Y-m-d') ? "Menunggu" : "Expired") ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=5><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>