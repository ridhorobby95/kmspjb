<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'ms_supplier';
	$p_window = '[PJB LIBRARY] Data Supplier Pustaka';
	$p_title = 'Data Supplier Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_supplier.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			
			
			$record = Helper::cStrFill($_POST);//,array('userid','nama','usertype','hints','statususer'));
			
			if($r_key == '') { // insert record	
				Helper::Identitas($record);
			 	$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
			}
			else { // update record
				Helper::Identitas($record);
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"kdsupplier",$r_key);
				
				if($err != 0){
				$errdb = 'Update data gagal.';
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist);
				exit();
				Helper::redirect(); 
				}
			}
			
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					$r_key = $record['kdsupplier'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"kdsupplier",$r_key);
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}else {
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			
		}
		
	}
	if(!$p_errdb) {
		if ($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where kdsupplier = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	}
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Supplier</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key) { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><?php include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">

	<tr> 
		<td width="35%" class="LeftColumnBG">Kode Supplier  *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('kdsupplier',$row['kdsupplier'],'ControlStyle',8,8,$c_edit); ?></td>
	</tr>	
	<tr> 
		<td class="LeftColumnBG">Nama Supplier *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('namasupplier',$row['namasupplier'],'ControlStyle',50,40,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Alamat </td>
		<td class="RightColumnBG"><?= UI::createTextBox('alamat',$row['alamat'],'ControlStyle',60,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Kode Pos</td>
		<td class="RightColumnBG"><?= UI::createTextBox('kodepos',$row['kodepos'],'ControlStyle',5,5,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Kota</td>
		<td class="RightColumnBG"><?= UI::createTextBox('kota',$row['kota'],'ControlStyle',50,20,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Telepon</td>
		<td class="RightColumnBG"><?= UI::createTextBox('telp',$row['telp'],'ControlStyle',20,15,$c_edit); 
		 
		 ?></td>
		
	</tr>
	<tr>
	<td class="LeftColumnBG">Handphone</td>
		<td class="RightColumnBG"><?= UI::createTextBox('hp',$row['hp'],'ControlStyle',20,15,$c_edit); 
		 
		 ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Fax</td>
		<td class="RightColumnBG"><?= UI::createTextBox('fax',$row['fax'],'ControlStyle',30,15,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Email</td>
		<td class="RightColumnBG"><?= UI::createTextBox('email',$row['email'],'ControlStyle',50,20,$c_edit); ?></td>
	</tr>
</table>



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("kdsupplier"))
		goSave();
}

</script>
</html>