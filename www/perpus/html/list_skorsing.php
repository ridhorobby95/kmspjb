<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_skorsing';
	$p_window = '[PJB LIBRARY] Daftar Skorsing';
	$p_title = 'Daftar Skorsing';
	$p_tbheader = '.: Daftar Skorsing :.';
	$p_col = 9;
	$p_tbwidth = 1120;
	$p_filedetail = Helper::navAddress('ms_skorsing.php');
	$p_id = "ms_skorsing";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'tglskorsing';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	// sql untuk mendapatkan isi list
	
	
	$p_sqlstr="select s.*, a.idanggota, a.namaanggota,a.tglselesaiskors from $p_dbtable s 
			   join ms_anggota a on s.idanggota=a.idanggota where 1=1";

	// pengaturan ex
	if (!empty($_POST))
	{
		$keyid=Helper::removeSpecial($_POST['cariid']);
		$keynama=Helper::removeSpecial($_POST['carinama']);	
		$keyjenis=Helper::removeSpecial($_POST['kdjenisanggota']);
		
		if ($keyid!='')
			$p_sqlstr.=" and a.idanggota = '$keyid'";
			if ($keynama!='')
				$p_sqlstr.=" and upper(a.namaanggota) like upper('%$keynama%')";
				if ($keyjenis!='')
					$p_sqlstr.=" and a.kdjenisanggota='$keyjenis' ";
	
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi=='freeskors') {
			$recbebas=array();
			$recbebas['tglselesaiskors']='';
			
			$err=Sipus::UpdateBiasa($conn,$recbebas,ms_anggota,idanggota,$r_key);
			
			if($err != 0){
				$errdb = 'Pembebasan masa skorsing gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
			else {
				$sucdb = 'Pembebasan masa skorsing berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();				
			}
		}
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
		$rs_cb = $conn->Execute("select namajenisanggota, kdjenisanggota from lv_jenisanggota order by kdjenisanggota");
		$l_jenis = $rs_cb->GetMenu2('kdjenisanggota',$keyjenis,true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:120" onchange="goSubmit()"');
		$l_jenis = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_jenis);	
	//echo "carinama ".$keynama;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	
	<table width="95%" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
	<tr height="20">
		<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
	</tr>
	<tr>
	<td width="100%"  colspan="<?= $p_col ?>" align="center">
	<? include_once('_notifikasi.php'); ?>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<? if($c_add) { ?>
				<td width="170" align="center">
				<a href="javascript:goNew('<?= $p_filedetail; ?>')" class="button"><span class="new">
				Skorsing baru</span></a></td>
				<? } ?>
				<td width="170" align="center">
				<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
				Refresh</span></a></td>
				<td align="right" valign="middle">
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
			</tr>
			</table>
			</td>	
	</tr>		
<tr>
	<td colspan="7">
	<table cellpadding=0 cellspacing="2" border=0 width="100%">
	<tr>
		<td width="120">Id Anggota</td>
		<td width="30%">: <input type="text" id="cariid" size="20" name="cariid" value="<?= $keyid ?>" onKeyDown="etrCari(event);"></td>
		</tr>
	<tr>
		<td>Nama Anggota</td>
		<td>: <input type="text" id="carinama" name="carinama" size="35" value="<?= $_POST['carinama']?>" onKeyDown="etrCari(event);"></td>
		<td>Jenis Anggota</td>
		<td>: <?= $l_jenis ?> </td>
		<td align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>		
	</tr>
	</table>
</tr>	
	<tr height="20">
		<td width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.idanggota:C');">Id Anggota  <?= $p_xsort['idanggota']; ?></td>		
		<td width="30%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.namaanggota:C');">Nama Anggota <?= $p_xsort['namaanggota']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','s.tglskorsing:D');">Tanggal Skorsing<?= $p_xsort['tglskorsing']; ?></td>
		<td width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','s.tglexpired:D');">Tanggal Expired<?= $p_xsort['tglexpired']; ?></td>
		<td width="45%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','s.keterangan:C');">Keterangan<?= $p_xsort['keterangan']; ?></td>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Edit</td>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Bebas</td>
	
		<?php
		$i = 0;

		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idskorsing'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
		<td align="center">
		<?= $row['idanggota']; ?>
		</td>
		<td align="left"><?= $row['namaanggota']; ?></td>
		<td align="center"><?= Helper::tglEng($row['tglskorsing']); ?></td>
		<td align="center"><?= Helper::tglEng($row['tglexpired']); ?></td>
		<td ><?= $row['keterangan']; ?></td>
		
		<td align="center">
		<? if($c_edit) { ?>
		<u title="Edit Skorsing" onclick="goDetail('<?= $p_filedetail; ?>','<?= $row['idskorsing']; ?>');" class="link"><img src="images/edited.gif"></u>
		<? } ?>
		</td>
		<td align="center">
		<? if($c_add and $c_edit and $c_delete) { 
		if($row['tglexpired']>=date('Y-m-d') and $row['tglselesaiskors']>=date('Y-m-d') and $row['tglselesaiskors']!=''){
		?>
		<u title="Bebas Skorsing" onclick="goFreeSkors('<?= $row['idanggota'] ?>')" class="link"><img src="images/freeskors.gif"></u>
		<? }else{ ?>
		<img src="images/freeskors2.gif">
		<? }
		}else {?>
		<img title="Hak Akses tidak diijinkan" src="images/freeskors2.gif"> 
		<? } ?>
		</td>
		
		
	</tr>
	<?php
		}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr>
</table><br>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key3" id="key3">
<input type="hidden" name="act" id="act">
<input type="hidden" name="npwd" id="npwd">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

</form>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script type="text/javascript">
function goClear(){
	document.getElementById("cariid").value='';
	document.getElementById("carinama").value='';
	document.getElementById("kdjenisanggota").value='';
	//goSubmit();
}
function goFreeSkors(key) {
	var free = confirm("Apakah anda yakin akan membebaskan skorsing Id Anggota "+key+" ?");
	if(free) {
		document.getElementById("act").value = "freeskors";
		document.getElementById("key").value = key;
		goSubmit();
	}
}
</script>
</html>