<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and (u.idunit in ($units) or a.idunit in ($units)) ";
		
	// definisi variabel halaman
	$p_dbtable = 'pp_orderpustakattb';
	$p_window = '[PJB LIBRARY] Daftar Inventarisasi';
	$p_title = 'Daftar Inventarisasi';
	$p_tbheader = '.: Daftar Inventarisasi :.';
	$p_col = 7;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('xlist_inventaris.php');
	$p_id = "lsinventaris";
	
	$p_defsort = 'idorderpustakattb';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$p_sqlstr = "select op.*,t.nottb, to_char(t.tglttb,'YYYY-mm-dd') tglttb, qtyttbdetail, idorderpustakattb, u.idunit, trim(op.judul) as judulas,
			t.jnsttb 
		from $p_dbtable p
		left join pp_ttb t on t.idttb=p.idttb
		left join pp_orderpustaka op on op.idorderpustaka=p.idorderpustaka
		left join pp_usul u on op.idusulan=u.idusulan
		left join pp_sumbangan s on s.idsumbangan =  op.idsumbangan
		left join ms_anggota a on (a.idanggota = u.idanggota or a.idanggota = s.idanggota or op.nrp1 = a.idanggota)
		where ststtbdetail=1 and stsinventaris = 0 $sqlAdminUnit";		
		
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carino']);
		$carifilter=Helper::removeSpecial($_POST['caripengarang']);
		$keyjenis=Helper::removeSpecial($_POST['jenis']);
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(op.judul) like upper('%$keyjudul%') ";
		}
		if($carifilter!=''){
			$p_sqlstr.=" and (";
			$p_sqlstr.=" (upper(op.authorfirst1||' '||op.authorlast1) like upper('%$carifilter%') or upper(op.authorfirst2||' '||op.authorlast2) like upper('%$carifilter%') or upper(op.authorfirst3||' '||op.authorlast3) like upper('%$carifilter%')) ";
			$p_sqlstr.=" or (upper(op.authorfirst1) like upper('%$carifilter%') or upper(op.authorfirst2) like upper('%$carifilter%') or upper(op.authorfirst3) like upper('%$carifilter%')) ";
			$p_sqlstr.=" or (upper(op.authorlast1) like upper('%$carifilter%') or upper(op.authorlast2) like upper('%$carifilter%') or upper(op.authorlast3) like upper('%$carifilter%')) ";
			$p_sqlstr.=" ) ";
		}
		if($keyjenis!='0'){
			$p_sqlstr.=" and t.jnsttb = '$keyjenis' ";
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
	$a_jenis = array('0' => '-- Semua --','1' => 'Pengadaan', '2' => 'Sumbangan');
	$l_jenis = UI::createSelect('jenis',$a_jenis,$keyjenis,'ControlStyle',true,'id="jenis" style="width:150" onchange="goSubmit()"');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<table border="0">
				<tr>
					<td align="center" width="250">
						<a href="index.php?page=xlistkiriminv" class="button"><span class="list">
						Detail Inventaris</span></a>
					</td>	
				</tr>
			</table>
			<div class="filterTable table-responsive">
				<table width="100%" cellpadding="4" cellspacing="0" border="0">
					<tr>
						<td class="thLeft" style="border:0 none;" width="150"><strong>Judul</strong></td>
						<td class="thLeft" style="border:0 none;" colspan="2">:&nbsp;<input type="text" name="carino" id="carino" size="50" value="<?= $_POST['carino'] ?>" onKeyDown="etrCari(event);"></td>
					</tr>
					<tr>
						<td class="thLeft" style="border:0 none;" width="150"><strong>Pengarang</strong></td>
						<td class="thLeft" style="border:0 none;" colspan="2">:&nbsp;<input type="text" name="caripengarang" id="caripengarang" size="50" value="<?= $_POST['caripengarang'] ?>" onKeyDown="etrCari(event);"></td>
					</tr>
					<tr>
						<td class="thLeft" style="border:0 none;" width="150"><strong>Jenis Inventaris</strong></td>
						<td class="thLeft" style="border:0 none;" colspan="2">:&nbsp;<?= $l_jenis ?></td>
						<td  align="right"><input type="button" value="Filter" class="ControlStyle" onClick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onClick="goClear();goFilter(false);" /></td>
					</tr>
				</table>
			</div>
			<br />
			<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
            <div class="table-responsive">
			<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			<tr height="20"> 
				<td width="120" nowrap align="center" class="SubHeaderBGAlt thLeft">No. TTP  <?= $p_xsort['nottb']; ?></td>
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">Tgl. TTP <?= $p_xsort['tglttb']; ?></td>		
				<td width="280" nowrap align="center" class="SubHeaderBGAlt thLeft">Judul  <?= $p_xsort['judul']; ?></td>
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">Jenis</td>
				<td width="180" nowrap align="center" class="SubHeaderBGAlt thLeft">Pengarang  <?= $p_xsort['authorfirst1']; ?></td>		
				<td width="55" nowrap align="center" class="SubHeaderBGAlt thLeft">Jumlah<?= $p_xsort['qtyttbdetail']; ?></td>
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" valign="middle"> 
				<td align="center"><?= $row['nottb']; ?></td>
				<td align="center"><?= Helper::formatDateInd($row['tglttb'],false); ?></td>
				<td><u onClick="goDetail('<?= $p_filedetail; ?>','<?= $row['idorderpustakattb']; ?>');"  title="Proses Inventaris" class="link"><?= $row['judulas']; ?></u></td>
				<td><?= $row['jnsttb']=='1' ? "Pengadaan" : ($row['jnsttb']=='2' ? "Sumbangan" : " ") ?></td> 
				<td><?php
					echo $row['authorfirst1']. " " .$row['authorlast1']; 
					if ($row['authorfirst2']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst2']. " " .$row['authorlast2'];
					}
					if ($row['authorfirst3']) 
					{
						echo " <strong><em>,</em></strong> ";
						echo $row['authorfirst3']. " " .$row['authorlast3'];
					}
				?></td>
				<td align="center"><?= $row['qtyttbdetail']; ?></td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col ; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
				
			</tr>
		</table>
                </div>
		<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>
		</form>
		</div>
	</div>
</div>



</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carino").value='';
	document.getElementById("jenis").value='0';
	document.getElementById("caripengarang").value='';
}
</script>

</html>