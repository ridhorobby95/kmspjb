<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanan';
	$p_window = '[PJB LIBRARY] Surat Bebas Pinjam';
	$p_title = 'SURAT BEBAS PINJAM';
	$p_tbheader = '.: SURAT BEBAS PINJAM :.';
	$p_col = 9;
	$p_tbwidth = 100;
	
	if(!empty($_POST)){
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_key=Helper::removeSpecial($_POST['key']);
		if ($r_aksi=='freeskors'){
			# get denda yang harus dibayar
			$r_bayardenda = $conn->GetOne("select a.denda from ms_anggota a where a.idanggota = '$r_key' $sqlAdminUnit ");
			
			$recbebas=array();
			$recbayar=array();
			$r_key=Helper::removeSpecial($_POST['key']);
			$recbebas['denda']=null;
			$err=Sipus::UpdateBiasa($conn,$recbebas,ms_anggota,idanggota,$r_key);
			
			#flek untuk bayar denda
			$recordbayar = array();
			$recordbayar['idanggota']=$r_key;
			$recordbayar['tglbayar']=date('Y-m-d');
			$recordbayar['nilai']=$r_bayardenda;
			Helper::Identitas($recordbayar);
			$err=Sipus::InsertBiasa($conn,$recordbayar,pp_bayardenda);
			
			//cari transaksi yang mana untuk memberikan tgl bayar
			$sql_transaksi = $conn->Execute("select * from pp_denda d 
							left join pp_transaksi t on d.idtransaksi=t.idtransaksi
							where d.tglbayar is null and (statustransaksi='0' or statustransaksi='2') and d.idanggota='$r_key'");
			$recbayar['tglbayar'] = date("Y-m-d");
			while($row_bayar = $sql_transaksi->FetchRow()){
				$err=Sipus::UpdateBiasa($conn,$recbayar,pp_denda,idtransaksi,$row_bayar['idtransaksi']);
			}
			$_SESSION['idbebaspinjaman'] = $r_key;
			
			if($err != 0){
				$errdb = 'Pembayaran denda gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembayaran denda berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();				
			}
		}
	}	

?>
<html>
<head>


	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<style>
        .ui-autocomplete{
            margin-top:35px !important;
            width: 280px;
            text-align:left;
        }
        #anggota_label{
            width: 280px;
            margin: 10px 0;
        }
    </style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('anggota_label').focus();" >


<?php include('inc_menu.php'); ?>

<div class="container">
    <div class="SideItem" id="SideItem">
<div class="LeftRibbon"> Surat Bebas Pinjam </div>
		<div align="center" style="position:relative;">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--div class="table-responsive">
		
		<table width="100%" border="1" cellpadding="6" cellspacing=0>
		<tr>
			<td width="100%" align="center">
			
			<table width="650" border="0" align="center" cellpadding="4" cellspacing=0 class="filterTable">
			  <tr height="50">
				<td valign="bottom" ><span style="font-weight:bold">ID/Nama Anggota</span></td>
				<td>
				<?/*if($_SESSION['idbebaspinjaman'] == null){?>
					<input type="text" name="txtid" id="txtid" size="25" maxlength="20" class="ControlStyleT" onKeyDown="etrR(event)">
				<?}else{?>
					<input value="<?= $_SESSION['idbebaspinjaman'];?>" type="text" name="txtid" id="txtid" size="25" maxlength="20" class="ControlStyleT" onKeyDown="etrR(event)">
				<?}*/?>
					<input type="text" name="anggota" id="anggota" >
				</td>
			  <td align="center">

				  <input type="button" name="btncek" class="buttonSmall" id="btncek" value="Periksa" onClick="proses();" style="cursor:pointer;height:25;width:100">

				</td>
			  </tr>
			</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<div id="loading" style="display:none;"><img src="images/loading.gif" alt="loading..." /></div>
				<div id="result" style="display:block;"></div>
			</td>
		</tr>
		</table>
        
        
        </div-->
        <div style="border: solid 1px grey; width:600px; padding: 10px; position:relative; ">
			<Span style="font-weight:bold">Nama / ID Anggota </Span> : <br>
			<div id="ac-wrapper" style="position:relative; width: 280px;">
			<input type="text" name="anggota_label" id="anggota_label" data-cari="anggota" data-inkey="idanggota">
			</div>
			<input type="hidden" name="idanggota" id="idanggota"><br>
			
			<input type="button" name="btncek" class="buttonSmall" id="btncek" value="Periksa" onClick="proses();" style="cursor:pointer;height:25;width:100">

			<div id="loading" style="display:none;"><img src="images/loading.gif" alt="loading..." /></div>
			<div id="result" style="display:block;"></div>			
			
        </div>
		<input type="text" name="tes" style="visibility:hidden">
		<input type="hidden" name="perlu" id="perlu">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="keterangan" id="keterangan">
		<input type="hidden" name="nosurat" id="nosurat">
		<input type="hidden" name="act" id="act">

		<input type="hidden" name="idee" id="idee" >
		</form>
		<form name="perpusx" id="perpusx" method="post"  action="<?= $i_phpfile; ?>">
		<div id="loading" style="display:none;"><img src="images/loading.gif" alt="loading..." /></div>
			<div id="result1" style="display:none;"></div>
		</form>
		</div>
		</div>
		</div>
</body>
<script type="text/javascript">
	var ajaxpage = "index.php?page=ajax";
	$(document).ready(function() {
    $("[data-cari]").not(".tt-hint").each(function() {
        var elemid  = $(this).attr('id');
        var inkey  = $(this).attr('data-inkey');
        var datacari  = $(this).attr('data-cari');

        $('#' + elemid).autocomplete({
            source: 'index.php?page=ajax&f=' + datacari,
            minLength: 2,
            select: function( event, i ) {
                $('#' + inkey).val(i.item.key);
            },
            position: { my : "right top", at: "right bottom" },
            appendTo: '#ac-wrapper'
        });
    });  
	jQuery.curCSS = function(element, prop, val) {
		return jQuery(element).css(prop, val);
	};

		$().ajaxStart(function() {
			$('#loading').show();
			$('#result').hide();
		}).ajaxStop(function() {
			$('#loading').hide();
			$('#result').fadeIn('slow');
		});
	});
	
	$(document).ready(function() {
		if('<?= $_SESSION['idbebaspinjaman']?>')
			proses();
	});
	
	function etrR(e) {
		var ev= (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13){
			document.getElementById('btncek').click();
		}
	}

	function proses() {
			$.ajax({
				type: 'POST',
				url: 'index.php?page=proses_bebas', 
				data: $('#perpusform').serialize(),
				success: function(data) {
					$('#result').html(data);
					
				}
			})
		}
		
	function goSimpan(){
		if(cfHighlight("txtnomer,txttgl,txtperlu")){
		document.getElementById("act").value='simpan';
		document.getElementById("perlu").value=document.getElementById("txtperlu").value;
		document.getElementById("nosurat").value=document.getElementById("txtnomer").value;
		document.getElementById("keterangan").value=document.getElementById("jenis").value;
		document.getElementById("btncek").disabled=true;
		document.getElementById("btncek").className="buttonSmallDis";
		proses();
		
		}
	}	
	
	function goFreeSkors2(key) {
		var free = confirm("Apakah anda yakin akan membayarkan denda Id Anggota "+key+" ?");
		if(free) {
			document.getElementById("act").value = 'freeskors';
			document.getElementById("key").value = key;
			goSubmit();
		}
	}
	
	</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

</html>
