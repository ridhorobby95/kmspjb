<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//$conns->debug = false;
	//$conn->debug = true;
	
	// koneksi ke sdm
	$conns = Factory::getConnSdm();

	// otentiksi user
	Helper::checkRoleAuth($conng);
	
	// menu dengan struktur xml
	$menu = 
	"<response>
		<item>
			<category>Satuan Kerja</category>";
	
	// satuan kerja
	//$sql = "select kdsatker idsatker, namasatker, info_lft, info_rgt from ms_satker";
	//$rs = $conn->Execute($sql);
	$rs = $conns->Execute("select kodeunit idsatker, namaunit namasatker, infoleft info_lft, inforight info_rgt, level, parentunit parent
			      from ms_unit
			      where isakademik = 'Y' and isaktif = 'Y' 
			      order by infoleft"); #dari SDM

	
	function loopfinal(&$final,&$parent,$idx) {
		if(is_array($parent[$idx]) and !empty($parent[$idx])) {
			foreach($parent[$idx] as $data) {
				$final[] = $data;
				loopfinal($final,$parent,$data['info_lft']);
			}
		}
	}
	
	// membuat xml untuk data hirarkikal urut
	//$sql = "select kdsatker idsatker, namasatker, parentsatker, info_lft, info_rgt from ms_satker where info_lft is not null order by info_lft";
	//$rs = $conn->Execute($sql);
	$rs = $conns->Execute("select kodeunit idsatker, namaunit namasatker, infoleft info_lft, inforight info_rgt, level, parentunit parent 
			      from ms_unit
			      where infoleft is not null and isakademik = 'Y' and isaktif = 'Y' 
			      order by infoleft"); #dari SDM
	
	// membuat struktur tree
	$left = array('0' => '0');
	$nama = array();
	$right = array();
	$parent = array('0');
	
	while($row = $rs->FetchRow()) {
		while (count($right) > 0 and $right[count($right)-1] < $row['info_rgt']) {
			array_pop($right);
			array_pop($parent);
		}
		
		$right[] = $row['info_rgt'];
		
		$row['level'] = count($right);
		$row['parent'] = $parent[count($parent)-1];
		
		$left[$row['idsatker']] = $row['info_lft'];
		$nama[$row['namasatker']][] = $row;
		
		$parent[] = $row['idsatker'];
	}
	
	ksort($nama);
	
	$parent = array();
	foreach($nama as $adata)
		foreach($adata as $data)
			$parent[$left[$data['parent']]][] = $data;
	
	unset($nama);
	ksort($parent);
	
	$final = array(); // array unit final

	loopfinal($final,$parent,0);

	foreach($final as $data) {
		$menu .=
			"<linx>
				<label>".$data['idsatker']." - ".htmlspecialchars($data['namasatker'])."</label>
				<value>".$data['idsatker']."</value>
				<level>".$data['level']."</level>
				<parent>".(($data['info_rgt']-$data['info_lft'] == 1) ? '0' : '1')."</parent>
			</linx>";
	}

	$menu .=
		"</item>
	</response>";

	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	echo $menu;
?>