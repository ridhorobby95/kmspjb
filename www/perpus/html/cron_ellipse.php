<?php 
	set_time_limit(0);
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');
	require_once('../classes/mail.class.php');
	$conne = Factory::getConnEllipse();
	$cek = $conn->IsConnected();

	if(!$cek){
	    //Mail::SendMail("abu_wes@yahoo.com","Perpustakaan PJB","Error Schedule Cron Perpustakaan","Eksekusi Cron dari Server Perpustakaan untuk mengambil data pegawai dari database Akademik mengalami gangguan, sehingga tidak dapat berjalan dengan baik. Mohon dilakukan check data.\n Cron Date : ".date('d-m-Y H:i:s')); 
	}
	
/********************************************************************************
    NOTE :
    kalau mau edit function _import
    jangan lupa edit juga kembarannya yakni function _cronimport di bawahnya
*****									********
***********							****************
*********************				           *********************
*****************************			   *****************************
********************************************************************************
********************************************************************************
*********************************************************************************/
	//cron ini untuk status
	$sqlstr = "select * from ellipse.knowvation_master_status u ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['kodestatus']=Helper::removeSpecial($row['table_code']);
		$record['nama'] = Helper::removeSpecial($row['table_desc']);
		
		$kodestatus = $conn->GetOne("select kodestatus from um.status where kodestatus='".trim($row['table_code'])."' ");
		if(!$kodestatus){
			$col = $conn->Execute("select * from um.status where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.status where kodestatus = '$kodestatus' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
	//cron ini untuk unit
	$sqlstr = "select * from ellipse.knowvation_master_unit u ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['kodeunit']=Helper::removeSpecial($row['table_code']);
		$record['nama'] = Helper::removeSpecial($row['table_desc']);
		
		$kodeunit = $conn->GetOne("select kodeunit from um.unit where kodeunit='".trim($row['table_code'])."' ");
		if(!$kodeunit){
			$col = $conn->Execute("select * from um.unit where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.unit where kodeunit = '$kodeunit' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
	//cron ini untuk subdit
	$sqlstr = "select kode_subdit, subdit from ellipse.moffice_datapegawai_v1 group by kode_subdit, subdit ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['kodesubdit']=Helper::removeSpecial($row['kode_subdit']);
		$record['nama'] = Helper::removeSpecial($row['subdit']);
		
		$kodesubdit = $conn->GetOne("select kodesubdit from um.subdit where kodesubdit='".trim($row['kode_subdit'])."' ");
		if(!$kodesubdit){
			$col = $conn->Execute("select * from um.subdit where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.subdit where kodesubdit = '$kodesubdit' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
	//cron ini untuk jabatan
	$sqlstr = "select position_id, posisi from ellipse.moffice_datapegawai_v1 group by position_id, posisi ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['kodejabatan']=Helper::removeSpecial($row['position_id']);
		$record['nama'] = Helper::removeSpecial($row['posisi']);
		
		$kodejabatan = $conn->GetOne("select kodejabatan from um.jabatan where kodejabatan='".trim($row['position_id'])."' ");
		if(!$kodejabatan){
			$col = $conn->Execute("select * from um.jabatan where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.jabatan where kodejabatan = '$kodejabatan' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
	//cron ini untuk direktorat
	$sqlstr = "select kode_posisi_ditbid, posisi_ditbid from ellipse.moffice_datapegawai_v1 group by kode_posisi_ditbid, posisi_ditbid ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['kodejabatan']=Helper::removeSpecial($row['kode_posisi_ditbid']);
		$record['nama'] = Helper::removeSpecial($row['posisi_ditbid']);
		
		$kodedirektorat = $conn->GetOne("select kodedirektorat from um.direktorat where kodedirektorat='".trim($row['kode_posisi_ditbid'])."' ");
		if(!$kodedirektorat){
			$col = $conn->Execute("select * from um.direktorat where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.direktorat where kodedirektorat = '$kodedirektorat' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
	//cron ini untuk user/pegawai
	$sqlstr = "select * from ellipse.moffice_datapegawai_v1 ";
	$rs = $conne->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		$record=array();
		$record['nid'] = trim($row['nid']);
		$record['nama'] = str_replace("'", "''",trim($row['nama_lengkap']));
		$record['kodeunit'] = trim($row['kode_unit']);
		$record['kodedirektorat'] = trim($row['kode_posisi_ditbid']);
		$record['kodesubdit'] = trim($row['kode_subdit']);
		$record['kodefungsi'] = null;//trim($row['FUNGSI']); /*gak dipake*/
		$record['kodestaff'] = null;//trim($row['KDSTAFF']); /*gak dipake*/
		$record['kodestatus'] = 1; /*senua yang dari ellipse dianggap tetap*/
		$record['kodejabatan'] = trim($row['position_id']);
		$record['email'] = trim($row['email']);
		
		$nid = $conn->GetOne("select nid from um.users where nid='".trim($row['nid'])."' ");
		if(!$nid){
			$col = $conn->Execute("select * from um.users where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from um.users where nid = '$nid' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
	
/*******************************   END   ***************************************
    NOTE :
    kalau mau edit function _import
    jangan lupa edit juga kembarannya yakni function _cronimport di bawahnya
*****									********
***********							****************
*********************				           *********************
*****************************			   *****************************
********************************************************************************
********************************************************************************
*********************************************************************************/	
?>