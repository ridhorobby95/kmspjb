<?php
	// gunakan buffer agar kalau ada tulisan masih bisa redirect
	ob_start();
	
	// jika sudah ada session modul
	if(!empty($_SESSION['PERPUS_USER'])) {
		if(empty($_SESSION['gate'])) {
			Helper::navigateOut();
			exit();
		}
		else {
			// hapus semua session kecuali gate
			$sesstemp = $_SESSION['gate'];
			$sesstemp2 = $_SESSION['menupage'];
			//session_unset();
			$_SESSION['gate'] = $sesstemp;
			$_SESSION['menupage'] = $sesstemp2;
		}
	}
	//print_r($_SESSION['YARSISGATE']);exit();
	// jika tidak ada session gate, redirect ke index
	if(empty($_SESSION['gate'])) {
		Helper::navigateOut();
		exit();
	}
	
	// penerjemah role
	switch($_SESSION['gate']['idrole']) {
		case 'admin':
		case 'admkeu':
		case 'admsdm': $role = 'A'; $page = 'list_pegawai.php'; break;
		case 'peg': $role = 'P'; $page = 'data_pegawai.php'; break;
		case 'pakfak': $role = 'PF'; $page = 'list_pegawai.php'; break;
		case 'pakins': $role = 'PI'; $page = 'list_pegawai.php'; break;
		case 'manajemen': $role = 'M'; $page = 'list_pegawai.php'; break;
		case 'sirk': $role='A'; $page = 'sirkulasi_baca.php';break;
		default: $role = $_SESSION['gate']['idrole']; $page = 'home.php';
	} 
	
	
	$_SESSION['PERPUS_USER'] = $_SESSION['gate']['username'];
	$_SESSION['PERPUS_NAME'] = $_SESSION['gate']['userdesc'];
	$_SESSION['PERPUS_LASTLOG'] = substr($_SESSION['gate']['lastlogin'],0,19);
	
	// $_SESSION['PERPUS_ROLE'] = $role;
	$_SESSION['PERPUS_ROLE'] = $role;//$_SESSION['gate']['idrole'];
	$_SESSION['PERPUS_GROLE'] = $_SESSION['gate']['idrole'];
	$_SESSION['PERPUS_NAMAROLE'] = $_SESSION['gate']['namarole'];
	
	$_SESSION['PERPUS_MODUL'] = $_SESSION['gate']['idmodul'];
	
	// $p_sqlstr = "select * from ms_satker where idsatker = '".$_SESSION['gate']['idsatker']."'";
	// $row = $conng->GetRow($p_sqlstr);
	
	$_SESSION['PERPUS_SATKER'] = $_SESSION['gate']['idsatker'];
	$_SESSION['PERPUS_NAMASATKER'] = $_SESSION['gate']['namasatker'];
	$_SESSION['PERPUS_SATKER_L'] = $_SESSION['gate']['satkerleft'];
	$_SESSION['PERPUS_SATKER_R'] = $_SESSION['gate']['satkerright'];
	
	$_SESSION['PERPUS_PERIODEAKAD'] = '20091'; // harusnya bisa didapatkan dari akademik
	
	$_SESSION['PERPUS_INITPAGE'] = $page;
	$_SESSION['PERPUS_ITEMAUTH'] =  $_SESSION['gate']['roleitem'];
	$_SESSION['PERPUS_MENU'] = $_SESSION['gate']['menu'];
	
	if($role == 'A' or $role == 'M' )
		$_SESSION['PERPUS_ADMIN'] = 1;
	
	unset($_SESSION['gate']);
	
	Helper::navigate('home');
?>