<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	// definisi variabel halaman
	$p_dbtable = 'pp_historymaintaining';
	$p_window = '[PJB LIBRARY] Mutasi Pustaka';
	$p_title = 'Mutasi Pustaka';
	$p_tbwidth = '750';
	
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
		if($r_aksi=='proses'){
			$mutasi1 = Helper::removeSpecial($_POST['mutasi1'],true);		
			$mutasi2 = Helper::removeSpecial($_POST['mutasi2'],true);		
			$opeks = Helper::removeSpecial($_POST['opeks'],true);
			$oppus = Helper::removeSpecial($_POST['oppus'],true);
			$opgen = Helper::removeSpecial($_POST['opgen'],true);
			
			$cPus1 = $conn->GetOne("select idpustaka from ms_pustaka where noseri='$mutasi1'");
			$cPus2 = $conn->GetOne("select idpustaka from ms_pustaka where noseri='$mutasi2'");
			
			if($cPus1 and $cPus2){
				if($opeks==3){
				$xPus = $conn->Execute("select ideksemplar from pp_eksemplar where idpustaka=$cPus1 and noseri like '%.%'");
				$cxPus = $conn->GetOne("select ideksemplar from pp_eksemplar where idpustaka=$cPus1 and noseri like '%.%' and statuseksemplar='PJM'");
				}elseif($opeks==0){
				$xPus = $conn->Execute("select ideksemplar from pp_eksemplar where idpustaka=$cPus1 and noseri not like '%.%'");
				$cxPus = $conn->GetOne("select ideksemplar from pp_eksemplar where idpustaka=$cPus1 and noseri not like '%.%' and statuseksemplar='PJM'");
				}else{
				$xPus = $conn->Execute("select ideksemplar from pp_eksemplar where idpustaka=$cPus1");
				$cxPus = $conn->GetOne("select ideksemplar from pp_eksemplar where idpustaka=$cPus1 and statuseksemplar='PJM'");
				}
				
				
				if($xPus and !$cxPus){
					$conn->StartTrans();
					while($row=$xPus->FetchRow()){
						$record['idpustaka'] = $cPus2;
						if($opgen==1)
						$record['noseri'] = Sipus::CreateSeri($conn,$mutasi2,$cPus2);
						
						Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$row['ideksemplar']);
						
					}
					if($oppus==1)
					Sipus::DeleteBiasa($conn,ms_pustaka,idpustaka,$cPus1);
					$conn->CompleteTrans();
					
				}else {
					$errdb = 'Proses Mutasi Gagal / Terdapat Eksemplar berstatus Pinjam';	
					Helper::setFlashData('errdb', $errdb);
				}
				
				
				if($conn->ErrorNo() != 0){
					$errdb = 'Proses Mutasi Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Proses Mutasi Berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect();
				}
			}
		}
	
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('mutasi1').focus();">
<?php include ('inc_menu.php'); ?>
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Proses Mutasi</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><?php include_once('_notifikasi.php'); ?>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">

	<tr> 
		<td width="190" class="LeftColumnBG">No. Induk Mutasi *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('mutasi1',$_POST['mutasi1'],'ControlStyle',12,20,$c_edit); ?>&nbsp; 
		<!-- <img src="images/popup.png" style="cursor:pointer;" title="Lihat Pustaka" onClick="popup('index.php?page=list_pustaka',700,700);"> -->
		</td>
	</tr>	
	<tr> 
		<td class="LeftColumnBG">No. Induk Tujuan Mutasi *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('mutasi2',$_POST['mutasi2'],'ControlStyle',12,20,$c_edit); ?>&nbsp; 
		<!-- <img src="images/popup.png" style="cursor:pointer;" title="Lihat Pustaka" onClick="popup('index.php?page=list_pustaka',700,700);"> -->
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Eksemplar Mutasi *</td>
		<td class="RightColumnBG">   
		  <label>
		  <input name="opeks" id="opeks1" type="radio" value="1" checked="checked" />
		  Semua NO INDUK</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <label>
		  <input name="opeks" id="opeks3" type="radio" value="3" />
		  Hanya NO INDUK Baru</label>
		  <label>
		  <input name="opeks" id="opeks2" type="radio" value="0" />
		  Hanya NO INDUK Lama</label>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Generate Ulang Eksemplar *</td>
		<td class="RightColumnBG">
		  <label>
		  <input name="opgen" id="opgen1" type="radio" value="1" checked="checked" />
		  Yes</label>
		  <label>
		  <input name="opgen" id="opgen2" type="radio" value="0" />
		  No</label>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Option Pustaka *</td>
		<td class="RightColumnBG">
		  <label>
		  <input name="oppus" id="oppus1" type="radio" value="1" checked="checked" />
		  Hapus Pustaka Dimutasi</label>
		  <label>
		  <input name="oppus" id="oppus2" type="radio" value="0" />
		  Abaikan</label>
		</td>
	</tr>
	
</table>
<table>
	<tr>
		<td><u style="color:red"><b><blink>Peringatan !!!</blink></b></u></td>
	</tr>
	<tr>
		<td>Hati-hati dalam penggunaan fasilitas mutasi ini,<br> karena jika telah dilakukan mutasi / Option Delete, tidak bisa dikembalikan lagi.</td>
	</tr>
	<tr>
		<td>Contoh : </td>
	</tr>
	<tr>
		<td>
		<table>
			<tr>
				<td width="190">No. Induk Mutasi</td>
				<td>: 27785 [ merupakan No. Induk <b>PUSTAKA</b> yang eksemplarnya akan dimutasikan ]<td>
			</tr>
			<tr>
				<td>No. Induk Tujuan Mutasi</td>
				<td>: 27784 [ Merupakan Tujuan dari eksemplar mutasi diatas ] </td>
			<tr>
				<td>Eksemplar Mutasi</td>
				<td>1. Semua NO INDUK [ No. Induk dari pustaka yang dimutasi, akan digenerate ulang mengikuti master pustaka tujuan ] <br>
					2. Hanya NO INDUK baru [ No. Induk yang akan digenerate ulang hanya No. Induk baru ( hasil dari otomasi baru ) ] <br>
					3. Hanya NO INDUK lama [ No. Induk yang akan digenerate ulang hanya No. Induk lama ( hasil dari Migrasi ) ]
				</td>
			</tr>
			<tr>
				<td>Option Pustaka</td>
				<td>1. Hapus Pustaka Dimutasi [ Master Pustaka yang dimutasi akan dihapus ]<br>
					2. Abaikan [ Master Pustaka yang dimutasi tetap tersimpan ]
				</td>
			</tr>
		</table>
				
		
		</td>
	</tr>
</table>	



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("mutasi1,mutasi2")){
		var par = confirm("Apakah anda yakin akan melakukan mutasi dengan memilih option tersebut ?");
		if(par){
		document.getElementById('act').value='proses';
		goSubmit();
		}
	}
}

</script>
</html>