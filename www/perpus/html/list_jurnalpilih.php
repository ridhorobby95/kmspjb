<?php
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
		
	// require tambahan
	
	// definisi variabel halaman
	$p_dbtable = 'pp_jurnal';
	$p_window = '[PJB LIBRARY] Daftar Jurnal Terpilih';
	$p_title = 'Daftar Jurnal Terpilih';
	$p_tbheader = '.: Daftar Jurnal Terpilih :.';
	$p_col = 15;//9;
	$p_tbwidth = 1100;
	$p_id = "lbl_jurnal";
	$p_filekirimint = Helper::navAddress('list_jurnal.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idjurnalready desc';
	$p_row = 21;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;

	// sql untuk mendapatkan isi list

	$p_sqlstr="select j.*, p.noseri from pp_jurnal j join ms_pustaka p on p.idpustaka = j.idpustaka ";
			   
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		$keypengarang=Helper::removeSpecial($_POST['caripengarang']);
		
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(j.tittle) like upper('%$keyjudul%') ";
		}
		if($keypengarang!=''){
			$p_sqlstr.=" and upper(j.creator) like upper('%$keypengarang%') ";
		}

		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idjurnalready,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
		}elseif($r_aksi == 'csv' and $c_delete) {
			$idpustakas=implode(",",$_POST['pilih']);
			$update = $conn->execute("update pp_jurnal set status = '2' where idpustaka in ($idpustakas) ");
			$datas = $conn->GetArray("select tittle, creator, subject, description, publisher, contributor, date, type, source, c_right, journal from pp_jurnal where idpustaka in ($idpustakas)") ;
			
			
			//$nfile = $_SESSION['PERPUS_USER']."__".date('Y-m-d_His');
			$nfile = "jurnal";
			$header = array(array("Title","Creator","Subject","Description","Publisher","Contributor","Date","Type","Source","Right","Journal"));
			$file = fopen("uploads/jurnalcsv/".$nfile.".csv","w+");
			$xdata = array_merge($header,$datas);
			foreach($xdata as $data){
				foreach($data as $i=>$dx){
					$data[$i] =  trim($dx);
				}
				//fputcsv($file, $data, "\t");
				fputs($file, implode($data, "\t")."\n");
			}
			fclose($file);
			header("Content-Type: text/csv");
			header('Content-Disposition: attachment; filename="jurnal.csv"');
			echo file_get_contents("uploads/jurnalcsv/".$nfile.".csv");
			exit();
		}

	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	// Group by
	//$p_sqlstr .=" group by p.judul, e.idpustaka, e.ideksemplar,e.noseri,e.statuseksemplar,k.namaklasifikasi, l.namalokasi ";
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div id="wrapper" style="width:1120;margin:0 auto;">
    <div class="SideItem" id="SideItem" style="width:1110;">
		<div align="center" style="width:<?=$p_tbwidth;?>;margin:0 auto;">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">	
			<? include_once('_notifikasi.php'); ?>
			<div class="filterTable" style="width:1085;margin:0 auto;">
			  <table width="<?=$p_tbwidth;?>">
			    <tr>
			      <td><a href="javascript:goDaftarJurnal('<?= $p_filekirimint; ?>');" class="buttonshort"><span class="list">Daftar Jurnal</span></a></td>
			    </tr>
			    <tr>
			      <td><strong>Judul Pustaka</strong></td>
			      <td>:</td>
			      <td>
				<input type="text" id="carijudul" name="carijudul" size="35" value="<?= $_POST['carijudul'] ?>" onKeyDown="etrCari(event);">
			      </td>
			      <td>&nbsp;</td>
			      <td><strong>Pengarang</strong></td>
			      <td>:</td>
			      <td><input type="text" id="caripengarang" name="caripengarang" size="35" value="<?= $_POST['caripengarang'] ?>" onKeyDown="etrCari(event);"></td>
			      <td rowspan="2"><input type="button" value="Filter" class="ControlStyle" onClick="goSubmit()">
				<input type="button" value="Refresh" class="ControlStyle" onClick="goClear(); goFilter(false);"></td>
			    </tr>
			  </table>
			</div>
			<br />
			<header style="width:<?=$p_tbwidth;?>;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
					<div class="right">
					  <div class="addButton" style="float:left;margin-left:10px; margin:right:10px;" title="Export To CSV" onClick='goCSV()'><img src="images/csv.gif"/></div>
					</div>
				</div>
			</header>
			<table width="<?=$p_tbwidth;?>" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
				<tr height="20">
				<th width="2%" nowrap align="center" class="SubHeaderBGAlt">
					<u title="Pilih Semua" onclick="goAll()" style="cursor:pointer"><input type="checkbox" name="all" id="all" value="all" onClick="goAll()"></u>
				</th>
				<th width="5%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.noseri:C');">No. Induk  <?= $p_xsort['noseri']; ?></th>
				<th width="14%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.tittle:C');">Title  <?= $p_xsort['tittle']; ?></th>
				<th width="7%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.creator:C');">Creator  <?= $p_xsort['creator']; ?></th>
				<th width="7%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.subject:C');">Subject<?= $p_xsort['subject']; ?></th>
				<th width="12%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.description:C');">Description <?= $p_xsort['description']; ?></th>
				<th width="7%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.publisher:C');">Publisher  <?= $p_xsort['publisher']; ?></th>
				<th width="7%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.contributor:C');">Contributor  <?= $p_xsort['contributor']; ?></th>
				<th width="5%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.date:C');">Date<?= $p_xsort['date']; ?></th>
				<th width="5%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.type:C');">Type  <?= $p_xsort['type']; ?></th>
				<th width="7%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.source:C');">Source  <?= $p_xsort['source']; ?></th>
				<th width="5%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.c_right:C');">Right<?= $p_xsort['c_right']; ?></th>
				<th width="15%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','j.journal:C');">Journal <?= $p_xsort['journal']; ?></th>
				<?/*?><th width="2%" nowrap align="center" class="SubHeaderBGAlt">Status</th><?*/?>
				<th width="2%" nowrap align="center" class="SubHeaderBGAlt">Aksi</th>
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['status'] == '1') $rowstyle = 'YellowBG';
						
						
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="top"> 
				<td align="center"><input type="checkbox" name="pilih[]" id="pilih" value="<?= $row['idpustaka'] ?>"></td>
				<td align="left"><?= $row['noseri']; ?></td>
				<td align="left"><?= Helper::limitS($row['tittle']); ?></td>
				<td align="left"><?= $row['creator']; ?></td>
				<td align="left"><?= $row['subject']; ?></td>
				<td align="left"><?= $row['description']; ?></td>
				<td align="left"><?= $row['publisher']; ?></td>
				<td align="left"><?= $row['contributor']; ?></td>
				<td align="left"><?= $row['date']; ?></td>
				<td align="left"><?= $row['type']; ?></td>
				<td align="left"><?= $row['source']; ?></td>
				<td align="left"><?= $row['c_right']; ?></td>
				<td align="left"><?= $row['journal']; ?></td>
				<?/*?><td align="center">
					<img src="images/<?=(($row['status']=="2" or $row['status']=="3")?"centang.gif":"Gear_32.png");?>" width=18 height=18>
				</td><?*/?>
				<td align="center">
				<?if(($row['status']=="1") and $c_delete){?>
					<img src="images/delete.png" title="Hapus Jurnal" width=18 height=18 onclick="goDeleteIP('<?= $row['idjurnalready']; ?>','<?= Helper::limitS($row['tittle']); ?>');" style="cursor:pointer" >
				<?}?>
				</td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
		<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="test" id="test" value="<?= $keykondisi=='' ? "test" : $keykondisi ?>">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>
		</form>
		</div>
	</div>
</div>



</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carijudul").value='';
	document.getElementById("caripengarang").value='';
	goSubmit();
}

function goDeleteIP(key,label) {
	var hapus = confirm('Apakah anda yakin akan menghapus data "' + label + '"?');
	if(hapus) {
		document.getElementById("key").value = key;
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goAll () {
	var aa= document.getElementById('perpusform');
	 if (aa.all.checked==true)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i = 0; i < aa.pilih.length; i++) 
	{
	 	aa.pilih[i].checked = checked;
	}
}

function goCSV() { 
	
	var a=document.getElementsByTagName("pilih[]");
	var total="";
	for(var i=0; i < document.perpusform.pilih.length; i++){
		if(document.perpusform.pilih[i].checked)
			total +=document.perpusform.pilih[i].value+"|";
	}
	if(total==''){
		alert("Harap Pilih Pustaka dahulu");
		exit();
	}
	
	document.getElementById("perpusform").target= "_blank";
	$("#act").val("csv");
	goSubmit();
	document.getElementById("perpusform").target= "_self";
	document.getElementById("act").value = "";
	goSubmit();
}

function goDaftarJurnal(file) {
	document.getElementById("perpusform").action = file;
	goSubmit();
}
</script>

</html>