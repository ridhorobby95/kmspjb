<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// definisi variabel halaman
	$p_dbtable = 'ms_pustaka';
	$p_window = '[PJB LIBRARY] Daftar Judul Pustaka';
	$p_title = 'Daftar Pustaka';
	$p_tbheader = '.: Daftar Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 600;
	//$p_filedetail = Helper::navAddress('ms_eksemplar.php');
	$p_id = "pop_title";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'kdjenispustaka';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$code = Helper::removeSpecial($_GET['code']);
	
	// sql untuk mendapatkan isi list

	$p_sqlstr="select noseri,judul,edisi, idpustaka from ms_pustaka
				where kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=0)";

	
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		$keyjenis=Helper::removeSpecial($_POST['kdjenispustaka']);
		
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(judul) like upper('%$keyjudul%') ";
		}
		if($keyjenis!=''){
			$p_sqlstr.=" and kdjenispustaka='$keyjenis' ";
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where kdjenispustaka in(".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$keyjenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:120" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="">
			<div class="filterTable">
				<table border=0 width="100%" cellpadding="4" cellspacing=0>
					<!--tr>
					<td width="120">
						<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
						Refresh</span></a></td>
					
					<td align="right" valign="middle" colspan="3">
						<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
					</td>
					</tr-->
					<tr>
					<td width="120">Judul Pustaka</td>
					<td colspan=2>:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $keyjudul ?>" onKeyDown="etrCari(event);"></td>
					
					</tr>
					<tr>
						<td>Jenis Pustaka</td>
						<td>:&nbsp;<?= $l_jenis ?></td>
						<td valign="top" align="right">
							<!--a href="javascript:goSubmit()" class="buttonshort"><span class="cari" >Cari</span></a-->
							<input type="button" value="Cari" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
					</td>
					</tr>
				</table>
			</div><br />
			<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<!--img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" /-->
						<h1><?= $p_title; ?></h1>
					</div>
				</div>
			</header>
			<table width="100%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<!--tr height="20">
				<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
			</tr-->
			<!--tr>
			<td width="100%"  colspan="6">
			
			<table border=0 width="100%" cellpadding="4" cellspacing=0>
					<tr>
					<td width="120">
						<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
						Refresh</span></a></td>
					
					<td align="right" valign="middle" colspan="3">
						<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td>
					</tr>
					<tr>
					<td width="120">Judul Pustaka</td>
					<td colspan=2>:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $keyjudul ?>" onKeyDown="etrCari(event);"></td>
					
					</tr>
					<tr>
						<td>Jenis Pustaka</td>
						<td>:&nbsp;<?= $l_jenis ?></td>
						<td valign="top" align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="cari" >Cari</span></a></td>
					</tr>
				</table>
					</td>
					</tr-->
				<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','noseri:C');">NO INDUK  <?= $p_xsort['noseri']; ?>
				<td width="90%" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>
				<td width="30" nowrap align="center" class="SubHeaderBGAlt">Pilih</td>
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
				
				<td>
				<?= $row['noseri']; ?>
				</td>
			
				<td align="left">
				<u title="Pilih Eksemplar" style="cursor:pointer;color:blue" onclick="goSend('<?= $row['noseri'] ?>','<?= $row['judul']; ?>')"><?= $row['judul']; ?><?= $row['edisi']!='' ? ' / '.$row['edisi'] : '' ?></u>
				</td>
				<td align="center">
				<img title="Pilih Eksemplar" src="images/centang.gif" style="cursor:pointer" onClick="goSend('<?= $row['noseri'] ?>','<?= $row['judul']; ?>')">
				</td>

			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= ($p_col/2)+1 ; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
			
		</table>
				<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>
	
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carijudul").value='';
	document.getElementById("kdklasifikasi").value='';
	//goSubmit();
}

function goSend(key,key2) {
		
		window.opener.document.getElementById("idpustaka").value = key;
		window.opener.document.getElementById("judul").value = key2;
		window.close();
	}
</script>

</html>