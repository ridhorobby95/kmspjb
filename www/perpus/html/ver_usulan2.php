<?php
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Pembuatan PO';
	$p_title = 'Pembuatan PO';
	$p_tbheader = '.: Pembuatan PO :.';
	$p_col = 9;
	$p_tbwidth = 560;
	$p_filelist = Helper::navAddress('list_po.php');
	//$p_filedetail = Helper::navAddress('data_usulan.php');
	//$p_filedetail2 = Helper::navAddress('data_usulans.php');
	$p_id = "mspo";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'o.idorderpustaka';
	$p_row = 20;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$pomax=$conn->GetRow("select max(nopo) as maxpo from pp_po where to_char(tglpo,'Year')=to_char(current_date,'Year')");
	$slice=explode("/",$pomax['maxpo']);
	$maxpo=$slice[0]+1;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select o.*,u.namapengusul from pp_orderpustaka o
			   join pp_usul u on o.idusulan=u.idusulan
			   where o.stspengadaan='1' and o.stspo=0";
  	
	
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		$idanggota=Helper::removeSpecial($_POST['idanggota']);
		$keyfilter=Helper::removeSpecial($_POST['supplier']);
		$jumupdate=Helper::removeSpecial($_POST['jml']);
		
		if($r_aksi=='save') {
			$hargapo=$_POST['hargapo'];
			//$j_harga=count($hargapo);
			$jumlahpo=$_POST['jumlahpo'];
			//$j_jumlah=count($jumlahpo);
			$nopo=Helper::removeSpecial($_POST['txtpo']);
			$tglpo=Helper::formatDate($_POST['tglpo']);
			$idorder=$_POST['idorderpustaka'];
			
			$sqlcheck=$conn->GetRow("select nopo from pp_po where nopo='$nopo'");
			if ($sqlcheck) {
				$errdb = 'Nomor PO sudah terpakai.';	
				Helper::setFlashData('errdb', $errdb);
			}else {
			$conn->StartTrans();
			$record=array();
			$recpo=array();
			$recpo['idpo']=Sipus::GetLast($conn,pp_po,idpo);
			$recpo['nopo']=$nopo;
			$recpo['tglpo']=$tglpo." ".date('H:i:s');
			$recpo['npkpo']=$_SESSION['PERPUS_USER'];
			
			Helper::Identitas($recpo);
			Sipus::InsertBiasa($conn,$recpo,pp_po);
			
			if ($jumupdate>=0){
				for($i=0;$i<=$jumupdate;$i++){ 
				$record['idpo']=$recpo['idpo'];
				$record['qtypo']=Helper::removeSpecial($jumlahpo[$i]);
				$record['stspo']=1;


				Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idorderpustaka,Helper::removeSpecial($idorder[$i]));
				}
			}
			$conn->CompleteTrans();	
			if($conn->ErrorNo() != 0){
					$errdb = 'Pembuatan PO Usulan gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Pembuatan PO Usulan berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
			}

		}else if ($r_aksi=='filter') {
			
		//if($keyfilter!='x')
		$p_sqlstr.=" and o.supplierdipilih='$keyfilter'";
		
		
		}else if($r_aksi=='proses') {
			$pilih=$_POST['pilih'];
			$jumlah=count($pilih)-1;
			if($pilih==''){
				$errdb = 'Pilih daftar pengadaan dengan benar.';	
				Helper::setFlashData('errdb', $errdb);
			} else {
			
			if($keyfilter!='x'){
			$p_sqlstr.=" and supplierdipilih='$keyfilter' and idorderpustaka in (";
						
			foreach($pilih AS $j => $val) {
					$data[] = $val;
				}
				$p_sqlstr .= implode(",",$data) . ")";
			}
			// $data=array();
			// $data[] = $idorder . ",'" . $val . "','" . $usulan[$j] . "','0'";
			
			}
			
		}
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$rs_cb = $conn->Execute("select s.namasupplier, o.supplierdipilih from pp_orderpustaka o
							 left join ms_supplier s on o.supplierdipilih=s.kdsupplier
							 where o.stspo=0 and o.stspengadaan=1
							 group by o.supplierdipilih,s.namasupplier");
	$l_supplier = $rs_cb->GetMenu2('supplier',$keyfilter,true,false,0,'id="supplier" class="ControlStyle" style="width:150" onchange="goSupplier()"');
	//$l_supplier = str_replace('<option></option>','<option value="x">-- Semua--</option>',$l_supplier);	
	
	$spo=$rs->RowCount();
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <link rel="stylesheet" href="style/style.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
		<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="<?= $pilih =='' ? "initButton($p_atfirst ? '1' : '0',$p_atlast ? '1' : '0');" : "" ?>" onmousemove="checkS(event)" >
<div id="main_content">
  <?php include('inc_menu.php'); ?>
  <div id="wrapper">
    <div class="SideItem" id="SideItem" align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table border=0>
	
	<tr>
	<td align="center">
		<a href="<?= $p_filelist ?>" class="buttonshort"><span class="list">Daftar PO</span></a>
	</td>
	<? if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="buttonshort"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goRef();" class="buttonshort"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="buttonshort"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
	<tr>
		<td align="center" colspan=4><? include_once('_notifikasi.php'); ?></td>
	</tr>
</table>
<header style="width:50%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
            
          </div>
        </header>
<table border="0" cellpadding="2" cellspacing=0 class="GridStyle" width="50%">
			<tr>
				<td width=150 class="LeftColumnBG">Nomor PO</td>
				<td><input type="text" name="txtpo" id="txtpo" maxlength=16 value="<?= $_POST['txtpo']=='' ? Helper::strPad($maxpo,4,0,'l')."/PO/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y') : $_POST['txtpo'] ?>"></td>
			</tr>
			<tr>
				<td class="LeftColumnBG">Tanggal PO</td>
				<td><input type="text" name="tglpo" id="tglpo" value="<?= $_POST['tglpo']=='' ? date('d-m-Y') : $_POST['tglpo'] ?>">
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal verifikasi">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglpo",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleusulan",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				
				<? } ?>
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG">Supplier</td>
				<td>
				<?= $l_supplier	?></td>
			</tr>
			
		</table><br><? if($pilih=='') { ?>
		<? if($keyfilter!=''){?>
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
<tr>
	<td colspan=9 align="center">
		<table width="100%">
			<tr>
				<td align="center" valign="bottom">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							<a href="javascript:goProses()" class="button"><span class="list" >Proses</span></a>
						</td>

						<td align="right" valign="middle">
						<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
</td>
</tr>
	<tr height="20"> 
		<td width="80" nowrap align="center" class="SubHeaderBGAlt"><u title="Pilih Semua" onclick="goAll()" style="cursor:pointer"><input type="checkbox" name="all" id="all" value="all" /></u></td>		
		<td width="300"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>		
		<td width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.namaanggota:C');">Anggota Pengusul <?= $p_xsort['namaanggota']; ?></td>
	</tr>
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['idorderpustaka'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height=20> 
		<td align="center">
		  <input type="checkbox" name="pilih[]" id="pilih" value="<?= $row['idorderpustaka'] ?>" />
		</td>		
		<td align="left"><?= $row['judul']; ?></td>
		<td align="left"><?= $row['namapengusul']; ?></td>
		
	</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Daftar usulan masih kosong.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
	</tr>
</table><br>
<? } } ?>
		<? if($pilih !='') { ?>
		<table width="590" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
			<tr>
				<td width="70" nowrap align="center" class="SubHeaderBGAlt">Id Order</td>
				<td width="350" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka</td>
				<td width="100" nowrap align="center" class="SubHeaderBGAlt">Jumlah</td>
			</tr>
			<? while ($rows=$rs->FetchRow()) {
			if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG';
			//$search=$conn->GetRow("select*from where idusulan=$pilih[$i]");
			?>
				<tr class="<?= $rowstyle ?>">
				<td align="center"><?= $rows['idorderpustaka'] ?><input type="hidden" name="idorderpustaka[]" id="idorderpustaka" value="<?= $rows['idorderpustaka'] ?>"></td>
				<td><?= $rows['judul'] ?></td>
				<td align="center"><input type="text" name="jumlahpo[]" id="jumlahpo" size="8" maxlength="5" value="<?= $rows['qtypengadaan'] ?>" onKeyDown="return onlyNumber(event,this,false,true)"></td>
				
			</tr>
			<?
			}?>

			
		</table>
		<? } ?><br>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="id" id="id">
<input type="hidden" name="idanggota" id="idanggota">
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="hidden" name="jml" id="jml" value="<?= $jumlah ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>



<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>
</form>
</div>
</div>
</div>



</body>

<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">


function saveData(){
	var check=document.getElementById("jml").value;
	var jum=document.getElementsByName("jumlahpo[]");
	if (check=='' || check<0){
		alert('Lakukan tahap proses terlebih dahulu.');
		} else {
				for(var i=0;i<jum.length;i++){
						if(jum[i].value==0 || jum[i].value==''){
							alert("Isikan kolom jumlah dengan benar");
							jum[i].focus();
							break;
						}
					
					}
				if(i==jum.length) {
				if(cfHighlight("txtpo,tglpo,supplier")){
					document.getElementById("act").value="save";
					goSubmit();
				}
				}
		}
}

function goStatus(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}

function goSupplier(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function goProses(){
	document.getElementById("act").value="proses";
	goSubmit();

}

function goRef() {
	document.getElementById("act").value="filter";
	document.getElementById("txtpo").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

checked=false;
function goAll () {
	var aa= document.getElementById('perpusform');
	 if (aa.all.checked==true)
          {
           checked = true
          }
        else
          {
          checked = false
          }

	for (var i =0; i < aa.pilih.length; i++) 
	{
	 	aa.pilih[i].checked = checked;
	}

}
</script>
</html>