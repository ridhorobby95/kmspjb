<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add=$a_auth['cancreate'];
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	
	// definisi variabel halaman
	$p_dbtable = 'lv_jurusan';
	$p_window = '[PJB LIBRARY] Data Jurusan PJB';
	$p_title = 'Data Jurusan';
	$p_tbwidth = 400;
	$p_filelist = Helper::navAddress('list_fakultas.php');
	
	$r_fak=Helper::removeSpecial($_GET['kf']);
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['namapendek'] = Helper::cStrNull($_POST['namapendek'.$r_key]);

			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,kdjurusan,$r_key);
			
			if($err != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
		
		}
	}
		$p_jur="select * from lv_jurusan where kdfakultas='$r_fak' order by kdjurusan";
		$rs = $conn->Execute($p_jur);
				
		$p_sqlstr = "select * from lv_fakultas where kdfakultas='$r_fak'";
		$row = $conn->GetRow($p_sqlstr);

		$row_idmax = $conn->GetRow("select max(kdjurusan) as maxid from lv_jurusan");
		$idmax = (int)($row_idmax['maxid'])+1;
		
		//koneksi ke akademik untuk mengambil kodeunit
		/*$conna = Factory::getConnAkad();
		$array_unit=array();
		$sql_unit = $conna->Execute("select namaunit,kodeunit from ms_unit order by kodeurutan");*/
		$array_unit=array();
		$sql_unit = $conn->Execute("select namasatker namaunit, kdsatker kodeunit from ms_satker order by kdsatker");
		while($row_unit = $sql_unit->FetchRow()){
			$array_unit[$row_unit['kodeunit']] = $row_unit['namaunit'];
		}
		//$sql_unit2 = $conna->Execute("select namaunit,kodeunit from ms_unit order by kodeurutan");
		$sql_unit2 = $conn->Execute("select namasatker namaunit, kdsatker kodeunit from ms_satker order by kdsatker");
		
		//=================================================================
		if(!$row)
			header('Location: '.$p_filelist);
		
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
<div align="center">
<table width="100" border=0>
	<tr>
	<? if($c_readlist) { ?>
	<td>
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Fakultas</span></a>
	</td>
	<? } ?>
</table>
<? include_once('_notifikasi.php') ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile."&kf=".$r_fak; ?>" enctype="multipart/form-data">
	<table width="835">
<tr>
		<td colspan=2>
		<table width="<?= $p_tbwidth ?>" border="0" cellspacing="4" cellpadding="0">
		<tr> 
			<td width="120"><b>Kode Fakultas</b> </td>
			<td width="280"><b>: <?= $row['kdfakultas']; ?></b></td>
		</tr>	
		<tr> 
			<td><b>Nama Fakultas</b></td>
			<td><b>: <?= $row['namafakultas']; ?></b></td>
		</tr>

		</table>
		</td>
	</tr>
    </table>
    <header style="width:835px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)">
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
	<table width="835" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	
	<tr height="20"> 
		<th width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">Kode Jurusan</th>
		<th width="220" nowrap align="center" class="SubHeaderBGAlt thLeft">Jurusan</th>
		<th width="160" nowrap align="center" class="SubHeaderBGAlt thLeft">Label Jurusan</th>
		<th width="80" nowrap align="center" class="SubHeaderBGAlt thLeft">Singkatan</th>
	</tr>
	<?php
		$i = 0;
		while ($rowj = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($rowj['kdjurusan'],$p_editkey)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>">
		<td align="center"><?= $rowj['kdjurusan']; ?></td>
		<td><?= $rowj['namajurusan']; ?></td>
		<td align="center"><?= $rowj['singkatan']; ?></td>
		<td align="center">
			<span id="spnAwal<?= $rowj['kdjurusan'];?>">
			<? if($c_edit) { ?>
				<a class="link" title="Sunting Data" onclick="editPendek('<?= $rowj['kdjurusan']; ?>');"><?= ($rowj['namapendek']?$rowj['namapendek']:"-"); ?></a>
			<? } else { ?>
				<?= ($rowj['namapendek']?$rowj['namapendek']:"-"); ?>
			<? } ?>
			</span>
			<span id="spnNext<?= $rowj['kdjurusan'];?>" style="display: none;">
				<?= UI::createTextBox('namapendek'.$rowj['kdjurusan'],$rowj['namapendek'],'ControlStyle',3,3,true,'onKeyDown="etrUpdate(event);"'); ?>
			</span>
		</td>
	</tr>
	<?php	} }
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="3"><b>Belum memiliki jurusan</b></td>
	</tr>
	<?php } ?>
	
</table><br>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="scroll" id="scroll">
<input type="hidden" name="idkey" id="idkey">
</form>
</div>
</div>
</div>
</div>
</body>
<script type="text/javascript">
/*function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}
function updateData(key) {
	if(cfHighlight("label")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

function insertData() {
	if(cfHighlight("i_namajurusan,i_namajurusan,i_label"))
		goInsertIP();
}*/
function editPendek(key) {
	document.getElementById("idkey").value=key;
	$('#spnAwal'+key).hide();
	$('#spnNext'+key).show();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData();
}
function updateData() {
	key = $("#idkey").val();
	if(cfHighlight("namapendek"+key)) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>