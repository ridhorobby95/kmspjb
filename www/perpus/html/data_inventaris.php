<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/pengolahan.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	$r_key = Helper::removeSpecial($_REQUEST['key']); 
	$rkey = Helper::removeSpecial($_REQUEST['rkey']); 

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// definisi variabel halaman
	$p_dbtable = 'pp_orderpustaka';
	$p_window = '[PJB LIBRARY] Data Inventaris';
	$p_title = '.: Data Inventaris :.';
	$p_tbwidth = 720;
	$p_filelist = Helper::navAddress('list_pustakainventaris.php');
	$p_listproses = Helper::navAddress('xdata_kirimeksemplar.php');

	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$rkey = Helper::removeSpecial($_POST['rkey']); 
				
		if($r_aksi == 'proses' and $c_edit) {
			$num = Helper::removeSpecial($_POST['qtyttbdetail']);
			
			$record = array();
			$record['judul'] = Helper::removeSpecial($_POST['judul']); 
			$record['judulseri'] = Helper::removeSpecial($_POST['judulseri']); 
			$record['kdbahasa'] = Helper::cStrNull($_POST['kdbahasa']); 
			$record['edisi'] = Helper::removeSpecial($_POST['edisi']);
			$record['idpenerbit']=Helper::removeSpecial($_POST['idpenerbit']);
			$record['namapenerbit']=Helper::removeSpecial($_POST['namapenerbit']);
			$record['tahunterbit']=Helper::removeSpecial($_POST['tahunterbit']);
			$record['isbn'] = Helper::cStrNull($_POST['isbn']); 
			$record['kota'] = Helper::cStrNull($_POST['kota']); 
			$record['tglperolehan'] = Helper::formatDate($_POST['tglperolehan']);
			$record['kdjenispustaka'] = Helper::cStrNull($_POST['kdjenispustaka']); 
						
			$idau1 = Helper::removeSpecial($_POST['addauthor'][0]);
			$idau2 = Helper::removeSpecial($_POST['addauthor'][1]);
			$idau3 = Helper::removeSpecial($_POST['addauthor'][2]);
			
			if ($idau1 !='')
				$au1 = $conn->GetRow("select * from ms_author where idauthor = $idau1");
			if ($idau2 !='')
				$au2 = $conn->GetRow("select * from ms_author where idauthor = $idau2");
			if ($idau3 !='')
				$au3 = $conn->GetRow("select * from ms_author where idauthor = $idau3");
				
			//untuk insersi ke author
			$record['authorfirst1'] = $au1['namadepan'];
			$record['authorfirst2'] = $au2['namadepan'];
			$record['authorfirst3'] = $au3['namadepan'];
			
			$record['authorlast1'] = $au1['namabelakang'];
			$record['authorlast2'] = $au2['namabelakang'];
			$record['authorlast3'] = $au3['namabelakang'];
			
			$recdetail = array();
			$lastid=$conn->GetRow("select max(ideksemplar) as maxid from pp_eksemplar");
			$ideks = $lastid['maxid'] + 1;
			$recdetail['ideksemplar'] = $ideks;
			//simpan kdklasifikasi dan kdperolehan dalam array
			for($j=0;$j<$num;$j++){
				$kdklasifikasi[$j] 	= Helper::cStrNull($_POST['kdklasifikasi'.$j]);
				$kdperolehan[$j] 	= Helper::cStrNull($_POST['kdperolehan'.$j]); 
				$no_induk[$j] 		= Helper::cStrNull($_POST['noinduk'.$j]);
				$tglperolehan[$j] 	= Helper::cStrNull($_POST['tglperolehan'.$j]); 
				
				$kdklasifikasicd[$j] = 'M';
			}
			
			$recdetail['harga'] = Helper::cStrNull($_POST['harga']);  
			$recdetail['tglpengadaan'] = Helper::formatDate($_POST['tglperolehan']);
			$recdetail['kdkondisi'] = 'V'; // V = Bagus
			$iscd=Helper::removeSpecial($_POST['iscd']);
			
			if ($rkey != ''){	
				$record['idpustaka'] = $rkey;
				$err = Pengolahan::updateInventaris($conn,$record,$recdetail,$num,$_POST['addauthor'],$r_key,$kdklasifikasi,$kdperolehan,$tglperolehan);
				
				if($err[0] == 0) {
					$sucdb = 'Penyimpanan Berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					
					echo '<script type="text/javascript">';
					echo 'var postdata = "key='.$r_key.'&rkey='.$rkey.'";';
					echo 'goPostX("'.$p_listproses.'", postdata)';
					echo '</script>';	
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
					$row = $_POST;
				}
			}else {
				
				$err = Pengolahan::insertInventaris($conn,$record,$recdetail,$num,$_POST['addauthor'],$r_key,$kdklasifikasi,$kdperolehan,$tglperolehan,$no_induk);
							
				if($err[0] == 0) {
					$rkey = $err[1];
					$sucdb = 'Penyimpanan Berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					
					echo '<script type="text/javascript">';
					echo 'var postdata = "key='.$r_key.'&rkey='.$rkey.'";';
					echo 'goPostX("'.$p_listproses.'", postdata)';
					echo '</script>';	
				}
				else{
					$errdb = 'Penyimpanan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
					$row = $_POST;
				}
			}			
		}
		
		if ($r_key != ''){
			$p_sqlstr = "select o.*,u.edisi,u.isbn,ot.*,t.*, to_char(tglttb, 'dd-mm-YYYY') tgl_ttb
				from pp_orderpustakattb ot 						
				left join pp_orderpustaka o on o.idorderpustaka=ot.idorderpustaka
				left join pp_usul u on o.idusulan=u.idusulan
				left join pp_ttb t on t.idttb=ot.idttb
				where ot.idorderpustakattb=$r_key";
			$row = $conn->GetRow($p_sqlstr);

			if ($rkey != ''){
				$p_sqlstr = "select p.*, b.namabahasa from ms_pustaka p
							left join pp_eksemplar e on e.idpustaka=p.idpustaka 
							left join lv_bahasa b on b.kdbahasa = p.kdbahasa
							where e.idpustaka=$rkey";
				$rowp = $conn->GetRow($p_sqlstr);
				
				$sql = "select b.idauthor, b.namadepan, b.namabelakang from ms_author b join
				pp_author a on b.idauthor = a.idauthor where a.idpustaka = '$rkey'";
				$rsd = $conn->Execute($sql);
			}
			$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal=0 order by kdjenispustaka");
			$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$rkey != '' ? $rowp['kdjenispustaka'] : $row['kdjenispustaka'],true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:140"');
			
		}
				
	}
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center"></div>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="100" border="0">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<? if ($c_edit) {?>
		<a href="javascript:goPostX('<?= $p_filelist; ?>','key=<?= $r_key; ?>')" class="button"><span class="list">Daftar </span></a>
		<? } ?>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:goProses();" class="button"><span class="validasi">Proses</span></a>
	</td>
	<? } ?>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	</tr>
</table>
<br><? include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<table width="<?= $p_tbwidth ?>" border="1" cellspacing="0" cellpadding="6" class="GridStyle">
	<tr> 
		<td align="center" class="PageTitle" colspan="2" align="center"><h1 style="font-weight:normal;color:#015593;">Data Pustaka</h1></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" align="left" width="150">Judul Pustaka *</td>
		<td class="RightColumnBG">
		<?= UI::createTextArea('judul',$rkey != '' ? $rowp['judul'] : $row['judul'],'ControlStyle',2,75,$c_edit); ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Judul Seri </td>
		<td class="RightColumnBG"><?= UI::createTextBox('judulseri',$rkey != '' ? $rowp['judulseri'] : $row['judulseri'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Edisi </td>
		<td class="RightColumnBG"><?= UI::createTextBox('edisi',$rkey != '' ? $rowp['edisi'] : $row['edisi'],'ControlStyle',60,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="150">Jenis Pustaka *</td>
		<td class="RightColumnBG"><?= $l_pustaka ?>&nbsp;
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Bahasa *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('namabahasa',$rkey != '' ? $rowp['namabahasa'] : $row['namabahasa'],'ControlRead',30,30,$c_edit,'readonly'); ?>&nbsp;&nbsp;<img src="images/tombol/breakdown.png" id="btnbahasa" title="Cari Bahasa" style="cursor:pointer" onClick="openLOV('btnbahasa', 'bahasa',-190, 40,'addBahasa',500)"><input type="hidden" name="kdbahasa" id="kdbahasa" value="<?= $rkey != '' ? $rowp['kdbahasa'] : $row['kdbahasa']?>">
        </td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Pengarang Usulan</td>
		<td class="RightColumnBG"><? if (!empty($row['authorfirst3'])) $y=3; else if (!empty($row['authorfirst2'])) $y=2; else if(!empty($row['authorfirst1'])) $y=1; else $y=0;
			for ($x=1; $x<=$y;$x++){
				echo $x.'. '.$row['authorfirst'.$x].'<br>';
				//echo $x.'. '.($rkey != '' ? $row['authorfirst'.$x] : $row['authorfirst'.$x]).' '.$rkey != '' ? $row['authorlast'.$x] : $row['authorlast'.$x].'<br>';
			}
		?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Pengarang *</td>
		<td class="RightColumnBG">
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr  id="tr_tambahau">
					<td>
						<?= UI::createTextBox('pengarang','','ControlRead',30,30,$c_edit,'readonly'); ?>&nbsp;&nbsp;<img src="images/tombol/breakdown.png" id="btnauthor" title="Cari Pengarang" style="cursor:pointer" onClick="openLOV('btnauthor', 'pengarang',-190, 40,'addPengarang',500)"><input type="hidden" name="idauthor" id="idauthor">&nbsp;<input type="button" class="ControlStyle" value="Tambah" onClick="addAuthor()"><br><br>
						<span id="span_error" style="display:none"><font color="#FF0000">Pengarang tersebut telah ditambahkan</font></span>
						<span id="span_error2" style="display:none"><font color="#FF0000">Data Pengarang tidak sesuai</font></span>
					</td>
				</tr>
				
				    <tr id="tr_bukosong"<? if(!$rsd->EOF) { ?> style="display:none" <? } ?>> 
						<td bgcolor="#EEEEEE" align="center" colspan="2"><strong>Belum memiliki pengarang</strong></td>
					</tr><? if($rkey !=''){
							  while($rowd = $rsd->FetchRow()) { ?>
					<tr> 
						<td >
							<input type="hidden" name="addauthor[]" id="addauthor" value="<?= $rowd['idauthor'] ?>">
							<?= $rowd['namadepan']." ". $rowd['namabelakang'] ?>
							<input type="hidden" name="namaauth[]" id="namaauth" value="<?= $rowd['namadepan']." ".$rowd['namabelakang'] ?>">

						</td>
						<td width="20" align="center">
						<? if($c_edit) { ?><img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer"><? } ?></td>
					</tr>
					<?	} } ?>
				
				<tr id="tr_authorhidden" style="display:none"> 
					<td colspan="2">
					</td>
				</tr>	
			</table>
			<table id="table_templateau" style="display:none">
				<tr> 
					<td bgcolor="#EEEEEE"><input type="hidden" name="addauthor[]" id="addauthor" disabled>
					<input type="hidden" name="namaauth[]" id="namaauth" disabled>
					</td>
					<td bgcolor="#EEEEEE" width="20" align="center">
					<img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer">
					</td>
				</tr>
			</table>
        </td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Penerbit Usulan</td>
		<td class="RightColumnBG"><?= $row['penerbit']; ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Penerbit *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('namapenerbit1',$rowp['namapenerbit'],'ControlStyle',200,55,$c_edit,'readOnly="readOnly"'); ?>
		&nbsp;&nbsp;<img src="images/tombol/breakdown.png" id="btnpenerbit" title="Cari Penerbit" style="cursor:pointer" onClick="openLOV('btnpenerbit', 'penerbit',-190, 40,'addPenerbit',500)">
		<input type="hidden" name="namapenerbit" id="namapenerbit" value="<?=$rowp['namapenerbit'];?>">
		<input type="hidden" name="idpenerbit" id="idpenerbit" value="<?=$rowp['idpenerbit'];?>">
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tahun Terbit </td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$rkey!='' ? $rowp['tahunterbit'] : $row['tahunterbit'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">ISBN </td>
		<td class="RightColumnBG"><?= UI::createTextBox('isbn',$rkey!='' ? $rowp['isbn'] : $row['isbn'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Kota Terbit </td>
		<td class="RightColumnBG"><?= UI::createTextBox('kota',$rkey!='' ? $rowp['kota'] : $row['kota'],'ControlStyle',200,55,$c_edit); ?></td>
	</tr>

	<?php $no_eks=1; for($k=0;$k<$row['qtyttbdetail'];$k++){?>
		<tr> 
			<td align="center" class="PageTitle" colspan="2" align="center"><h1 style="font-weight:normal;color:#015593;">Data Eksemplar <?= $no_eks?></h1></td>
		</tr>
		<tr> 
			<td class="LeftColumnBG" width="150">Label *</td>
			<?php $rs_cb_klas = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by namaklasifikasi");?>
			<td class="RightColumnBG"><?= $rs_cb_klas->GetMenu2('kdklasifikasi'.$k,$rkey != '' ? '' : $rowp['kdklasifikasi'],true,false,0,"id=\"kdklasifikasi{$k}\" class='ControlStyle' style='width:200'") ?></td>
		</tr>
		<tr> 
			<td class="LeftColumnBG" width="150">Asal Perolehan *</td>
			<?php
			if($row['jnsttb'] == 2){
				$pilihan = false;
				$rs_cb_perolehan = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan where kdperolehan = 'S' order by kdperolehan");
			}else{
				$pilihan = true;
				$rs_cb_perolehan = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by kdperolehan");
			}
			?>
			<td class="RightColumnBG"><?= $rs_cb_perolehan->GetMenu2('kdperolehan'.$k,$rkey != '' ? $rowp['kdperolehan'] : $row['kdperolehan'],$pilihan,false,0,"id=\"kdperolehan{$k}\" class='ControlStyle' style='width:200'") ?></td>
		</tr>
		<tr> 
			<td class="LeftColumnBG">Tanggal Perolehan</td>
			<td class="RightColumnBG"><?= UI::createTextBox('tglperolehan'.$k,$row['tgl_ttb'],'ControlStyle',10,10,$c_edit); ?>
			<? if($c_edit) { ?>
			<img src="images/cal.png" id="tgleperolehan<?=$k;?>" style="cursor:pointer;" title="Pilih tanggal perolehan">
			&nbsp;
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "tglperolehan<?=$k;?>",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tgleperolehan<?=$k;?>",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			[ Format : dd-mm-yyyy ]
			<? } ?>
			
			</td>
		</tr>
		<tr> 
			<td class="LeftColumnBG">Harga </td>
			<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['hargadipilih'],'ControlStyle',14,14,$c_edit,'readonly'); ?></td>
		</tr>
	<?$no_eks++;}?>
	<!-- end loop-->
	<tr> 
		<td class="LeftColumnBG">Jumlah TTP *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('qtyttbdetail',$row['qtyttbdetail'],'ControlStyle',3,3,$c_edit,'readonly'); ?></td>
	</tr>
</table>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="rkey" id="rkey" value="<?= $rkey; ?>">
</form>
<div id="newpengarang_block" class="popWindow" style="position:absolute;">
	<form method="post" onSubmit="isnewPengarang();return false;">
	<input type="hidden" name="pf" value="edit_disposisi" />
    <table cellspacing=1 cellpadding="4">
		<tr class="popWindowtr">
			<td colspan="2">
				<div style="float:left;font-weight:bold;padding:2px">Tambah Pengarang</div>
				<div style="float:right">
				<span id="loadingGif2" style="display:none"><img src="images/loading.gif" /></span>
				&nbsp;
				<input type="button" class="btn_close" value="" onClick="closePop()"/>
				</div>
			</td>
		</tr>
        <tr> 
          <td width="120">Nama Depan</td>
          <td>
		<input type="text" id="auth_namadepan" name="auth_namadepan" size="30" maxlength="100"> 
          </td>
        </tr>
        <tr> 
          <td>Nama Belakang</td>
          <td>
		<input type="text" id="auth_namabelakang" name="auth_namabelakang" size="30" maxlength="100"> 
          </td>
        </tr>
        <tr> 
          <td>Badan/Institusi/Editor</td>
          <td>
		<input type="checkbox" name="auth_isbadan" id="auth_isbadan"> 
          </td>
        </tr>
    </table>
    <div style="margin-top:5px;padding:5px;background-color:#FFD787;text-align:center;"><input type="submit" value="Tambah" class="btn" /></div>
	</form>
</div>

<div id="newpenerbit_block" class="popWindow" style="position:absolute;">
	<form method="post" onSubmit="isnewPenerbit();return false;">
	<input type="hidden" name="pf" value="edit_disposisi" />
    <table cellspacing=1 cellpadding="4">
		<tr class="popWindowtr">
			<td colspan="2">
				<div style="float:left;font-weight:bold;padding:2px">Tambah Penerbit</div>
				<div style="float:right">
				<span id="loadingGif2" style="display:none"><img src="images/loading.gif" /></span>
				&nbsp;
				<input type="button" class="btn_close" value="" onClick="closePop()"/>
				</div>
			</td>
		</tr>
		<tr> 
			<td width="150"><strong>Nama Penerbit</strong> </td>
			<td><?= UI::createTextBox('auth_namapenerbit','','ControlStyle',50,40,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Alamat</strong> </td>
			<td><?= UI::createTextBox('auth_alamat','','ControlStyle',50,50,true); ?></td>
		</tr>
			<tr> 
			<td><strong>Kota</strong> </td>
			<td><?= UI::createTextBox('auth_kota','','ControlStyle',50,30,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Kode Pos</strong></td>
			<td><?= UI::createTextBox('auth_kodepos','','ControlStyle',5,5,true,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		</tr>
		<tr> 
			<td><strong>Telepon</strong></td>
			<td><?= UI::createTextBox('auth_telp','','ControlStyle',20,15,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Fax</strong></td>
			<td><?= UI::createTextBox('auth_fax','','ControlStyle',30,15,true); ?></td>
		</tr>
		<tr> 
			<td><strong>Email</strong></td>
			<td><?= UI::createTextBox('auth_email','','ControlStyle',50,20,true); ?></td>
		</tr>
    </table>
    <div style="margin-top:5px;padding:5px;background-color:#FFD787;text-align:center;"><input type="submit" value="Tambah" class="btn" /></div>
	</form>
</div>
</body>
<script type="text/javascript" src="scripts/ajax_perpus.js"></script>
<script language="javascript">
function addBahasa(kode,nama) {
	$("#kdbahasa").val(kode);
	$("#namabahasa").val(nama);
}

function addPengarang(kode,nama) {
	$("#idauthor").val(kode);
	$("#pengarang").val(nama);
}

function addPenerbit(kode,nama) {
	$("#idpenerbit").val(kode);
	$("#namapenerbit1").val(nama);
	$("#namapenerbit").val(nama);
}

function goProses() {
	var pesan = confirm("Apakah Anda yakin akan melakukan proses ini ?");
	if(pesan){

	if(cfHighlight("judul,qtyttbdetail,kdjenispustaka,kdklasifikasi0,kdklasifikasi1,kdklasifikasi2,kdklasifikasi3,kdklasifikasi4,kdklasifikasi5,kdklasifikasi6,namabahasa,kdperolehan0,kdperolehan1,kdperolehan2,kdperolehan3,kdperolehan4,kdperolehan5,kdperolehan6")){
		var authkey = document.getElementById("addauthor").value;
		if (authkey < 1) 
			alert('Pengarang harus diisi');
		else{ 
			//ambil nilai no.induk
			var loop = $("#qtyttbdetail").val();
			var x = '';
			for($i=0;$i<loop;$i++){
				if($i==0) x += "'" + $("#noinduk"+$i).val() +"'";
				else x += ", '" + $("#noinduk"+$i).val() +"'";
			}

			file = "<?= Helper::navAddress('sys_ceknoinduk.php'); ?>";
			  $.ajax({
				  type: "post",
				  url: file,
				  data: "noseri="+ x,
				  cache: false,
				  success: function(data){

					var explode = data.split(';');
					var jml_noinduk = explode[0];
					var data_noinduk = explode[1];
					
					if(jml_noinduk > 0){
						alert("No. Induk telah terdaftar, yaitu " + data_noinduk);
					}else{
						sent = $("#perpusform").serialize();
						sent += "&act=proses";
						goPostX('<?= $i_phpfile; ?>',sent);
					}
				  }
			  });
			
			
			
		}
	}
	}
}

function addAuthor() {
	var idauthor = $("#idauthor").val();
	if(idauthor=='') {
		$("#span_error2").show();
		setTimeout('$("#span_error2").hide()',1000);
		return false; 
	} else {
		if($("[name='addauthor[]'][value='"+idauthor+"']").length > 0) {
			$("#span_error").show();
			setTimeout('$("#span_error").hide()',1000);
			return false;
		}
	}
	
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_aukosong:visible").length > 0)
		$("#tr_aukosong").hide();
	
	var newtab = $($("#table_templateau tbody").html()).insertBefore("#tr_authorhidden");
	var newtdf = newtab.find("td").eq(0);
	var addauthor = newtdf.find("[name='addauthor[]']");
	var namaauthor = $("#pengarang").val();
	
	newtdf.prepend(namaauthor);
	addauthor.val(idauthor);
	addauthor.attr("disabled",false);
}

function delAuthor(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
}

</script>
</html>
