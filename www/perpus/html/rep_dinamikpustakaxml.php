<?php
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	$conn->debug = false;
		
	// variabel request
	$r_format = "xml";
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_jenispustaka = Helper::removeSpecial($_POST['s_jenispustaka']);
	$r_jurusan = Helper::removeSpecial($_POST['s_jurusan']);
	$r_bahasa = Helper::removeSpecial($_POST['s_bahasa']);
	$r_penerbit = Helper::removeSpecial($_POST['t_penerbit']);
	$r_pengarang = Helper::removeSpecial($_POST['t_pengarang']);
	$r_seri = Helper::removeSpecial($_POST['noseri']);
	
	$p_namafile = 'laporan_pustaka'.$r_tgl1.'_'.$r_tgl2;
	
	// $sql = "select * from pp_pelayanan
			// where bayarpelayanan is not null and tglpelayanan between '$r_tgl1' and '$r_tgl2' 
			// order by tglpelayanan";	
	$sql= "select * from ms_pustaka where tglperolehan between '$r_tgl1' and '$r_tgl2' and kdjenispustaka='$r_jenispustaka' and kdbahasa='$r_bahasa'";
	if($r_penerbit!=null)
		$sql .= " and namapenerbit like '%$r_penerbit%'";
	if($r_pengarang!=null)
		$sql .= " and lower(authorfist1) like '%$r_pengarang%' or lower(authorlast1) like '%$r_pengarang%'";
	if($r_seri!=null)
		$sql .= " and noseri='$r_seri'";
	
	// var_dump($sql);
	$row = $conn->Execute($sql);
	$row2 = $conn->Execute($sql);
	
	while($rs2 = $row2->FetchRow()){
		$i=0;
		if($rs2['file1']!=null or $rs2['file1']!='')
			$i++;
		if($rs2['file2']!=null or $rs2['file2']!='')
			$i++;
		if($rs2['file3']!=null or $rs2['file3']!='')
			$i++;
		if($rs2['file4']!=null or $rs2['file4']!='')
			$i++;
		if($rs2['file5']!=null or $rs2['file5']!='')
			$i++;
		if($rs2['file6']!=null or $rs2['file6']!='')
			$i++;
		if($rs2['file7']!=null or $rs2['file7']!='')
			$i++;
		if($rs2['file8']!=null or $rs2['file8']!='')
			$i++;
		if($rs2['file9']!=null or $rs2['file9']!='')
			$i++;
		if($rs2['file10']!=null or $rs2['file10']!='')
			$i++;
		$doc[$rs2['idpustaka']] = $i;
	}
	
$format = "
<eprints xmlns='http://eprints.org/ep2/data/2.0'>
	<eprint id='http://repository.ubaya.ac.id/id/eprint/8'>
	<eprintid></eprintid>
	<rev_number></rev_number>
	<documents>";
	
	while($rs = $row->FetchRow()){
		if($doc[$rs['idpustaka']]>0){
			for($k=1;$k<=10;$k++){
				$fileke="file".$k;	
				if($rs[$fileke]!=null or $rs[$fileke]!=''){
					$namafile=explode('/', $rs[$fileke]);
		$format .= "
		<document id='http://repository.ubaya.ac.id/id/document/1'>
			<docid>1</docid>
			<rev_number>".$rs['noseri']."</rev_number>
			<files>
				<file id='http://repository.ubaya.ac.id/id/file/166'>
					<fileid>166</fileid>
					<datasetid>document</datasetid>
					<objectid>98</objectid>	
					<filename>".$namafile[3]."</filename>
					<mime_type>application/pdf</mime_type>
					<hash>841ad5d9e10fa403d0bb1452e6f57fc7</hash>
					<hash_type>MD5</hash_type>
					<filesize>1253989</filesize>
					<mtime>2011-11-10 07:07:40</mtime>
					<url>".Helper::GetPath($rs[$fileke])."</url>
				</file>
			</files>
			<eprintid>8</eprintid>
			<pos>2</pos>
			<placement>2</placement>
			<format>application/pdf</format>
			<language>en</language>
			<security>public</security>
			<main>".$namafile[3]."</main>
			<content>published</content>
		</document>";
				}
			}
		}
		
	//conversi jenispustaka ke nama lain
	if($rs['kdjenispustaka'] == 'KS')	
		$namatype = 'undergraduate_thesis';
	if($rs['kdjenispustaka'] == 'KT')
		$namatype = 'master_thesis';
	if($rs['kdjenispustaka'] == 'KD')
		$namatype = 'PhD_thesis';
	
	//mencari nama creator
	$nama_mhs = $rs['authorfirst1'];
	$nama_exp = explode(' ',$nama_mhs);
	$pnjg_nama = count($nama_exp)-1;
	$nama_family = $nama_exp[$pnjg_nama];
	$nama_given = '';
	for($j=0;$j<$pnjg_nama;$j++){
		$nama_given .= $nama_exp[$j];
	}
	
	$format .= "\n\t</documents>
	<eprint_status>archive</eprint_status>
	<userid>".$rs['nrp1']."</userid>
	<dir>disk0/00/00/00/08</dir>
	<datestamp>2011-11-10 07:15:44</datestamp>
	<lastmod>2011-11-25 06:39:56</lastmod>
	<status_changed>2011-11-25 06:39:56</status_changed>
	<type>".$namatype."</type>
	<metadata_visibility>show</metadata_visibility>
	<contact_email>pustaka@ubaya.ac.id</contact_email>
	<creators>
	  <item>
		<name>
		  <family>".$nama_family."</family>
		  <given>".$nama_given."</given>
		</name>
	  </item>
	</creators>
	<corp_creators>
	  <item>University of Surabaya</item>
	</corp_creators>
	<title></title>
	<ispublished>pub</ispublished>
	<subjects>
	  <item>RS</item>
	</subjects>
	<divisions>
	  <item>dep_pharm</item>
	</divisions>
	<full_text_status>restricted</full_text_status>
	<keywords>".$rs['keywords']."</keywords>
	<abstract>".$rs['sinopsis']."</abstract>
	<date>".$rs['tglperolehan']."</date>
	<date_type>published</date_type>
	<institution>University of Surabaya</institution>
	<department>Department of Pharmacy</department>
	<referencetext></referencetext>";
	}
	$format .= "\n</eprint>
</eprints>";

switch($r_format) {
	case 'xml' :
		header("Content-Type: text/xml");
		header('Content-Disposition: attachment; filename="'.$p_namafile.'.xml"');
		echo "<?xml version='1.0' encoding='utf-8'?>";
		echo $format;
		break;
	default : header("Content-Type: text/html");
}

?>