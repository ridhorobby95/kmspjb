<?php
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
		
	// require tambahan
	

	$r_id=Helper::removeSpecial($_GET['id']);
	$r_time=Helper::removeSpecial($_GET['time']);

	$kembali=$conn->GetRow("select idtransaksi,judul,edisi,noseri from v_trans_list
							where idtransaksi='$r_id'");	
	
	$reserve=$conn->Execute("select r.idtransaksi,a.idanggota,namaanggota,r.t_updatetime from pp_reservasi r
							 join ms_anggota a on r.idanggotapesan=a.idanggota
							 where r.idtransaksi='$r_id' order by t_updatetime");

	$user= $conng->GetRow("select userdesc from sc_user where username='".$_SESSION['PERPUS_USER']."'");
?>

<html>
<head>
<title>Nota Transaksi</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onLoad="window.print()">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>PERPUSTAKAAN PJB<br>
		</td>
	</tr>
	<tr>
		<td>RESERVASI PUSTAKA<br>
		</td>
	</tr>
	
</table>
<table cellpadding="0" cellspacing="0" width="430" border="0" >
	
	<tr>
		<td width="170">No.Induk</td>
		<td>: <?= $kembali['noseri'] ?></td>
	</tr>
	<tr>
		<td>Judul</td>
		<td>: <?= $kembali['judul'].'<br>Edisi : '. $kembali['edisi'] ?></td>
	</tr>
	<br>
	<tr>
		<td colspan="2"><br>
		<table cellpadding="0" cellspacing="0" width="450" border=0 class="nota">
		<tr height="20">
			<td width="150" style="border-bottom:1px solid">Id Anggota</td>
			<td width="150" style="border-bottom:1px solid">Nama</td>
			<td width="150" style="border-bottom:1px solid">Time</td>
		</tr>
		<? while($rowr=$reserve->FetchRow()) { ?>
		<tr height="15">
			<td><?= $rowr['idanggota']; ?></td>
			<td><?= $rowr['namaanggota']; ?></td>
			<td><?= Helper::tglEngStamp($rowr['t_updatetime']) ?></td>
		</tr>
		<?php
		}?>
		</table><br>
		</td>
	</tr>

</table>
<br>
<table width="450">
	<tr>
		<td>Terima Kasih | <?= date('d-m-Y') ?><br><br>
		Waktu awal : <?= $r_time ?> waktu akhir : <?= date('H:i:s') ?><br>
		<?= $user['userdesc'] ?></td>
	</tr>
	<tr height=30>
	    <td align="center">Perpanjangan Online http://digilib.PJB.ac.id/</td>
	</tr>
	<tr height=30>
	    <td align="center">------ Informasi : perpus.PJB@gmail.com ------</td>
	</tr>
</table>

</body>
</html>
