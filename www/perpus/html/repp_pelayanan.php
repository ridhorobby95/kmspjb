<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jurusan = Helper::removeSpecial($_REQUEST['kdjurusan']);
	$r_jenis = Helper::removeSpecial($_POST['kdpelayanan']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pelayanan Umum';
	
	$p_namafile = 'laporan_pelayanan_'.$r_jenis.'_'.$r_tgl1.'_'.$r_tgl2;
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2==''){// or $r_jenis==''
		header("location: index.php?page=home");
	}
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select ";
	if($r_jenis=='S')
	$sql .=" d.*, ";
	$sql .=" p.*,a.namaanggota from pp_pelayanan p
			join ms_anggota a on p.idanggota=a.idanggota";
			
	if($r_jenis=='S')
	$sql .=" left join pp_pelayanandetil d on p.idpelayanan=d.idpelayanan ";
	 
	$sql .=" where kdpelayanan='$r_jenis' and 
			tglpelayanan between '$r_tgl1' and '$r_tgl2'";
	
	if($r_jurusan !='')
	$sql .=" and a.idunit='$r_jurusan' ";
	
	$sql .="  order by tglpelayanan ";
	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	$rsj = $conn->GetOne("select namapelayanan from lv_jenispelayanan where kdpelayanan='$r_jenis'");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0">
  <tr>
  	<td align="center"><strong>
  	<h3>Laporan 
	<?= $rsj ?><br>Periode <?= Helper::formatDateInd($r_tgl1)." s/d ".Helper::formatDateInd($r_tgl2); ?></h3>
  	</strong></td>
  </tr>
  
</table>
<table width="660" border="1" cellpadding="2" cellspacing="0">
  
  <tr height="30">
	<th width="12" align="center"><strong>No.</strong></th>
    <th width="70" align="center"><strong>Tanggal</strong></th>
    <th width="70" align="center"><strong>Id Anggota</strong></th>
    <th width="130" align="center"><strong>Nama Anggota</strong></th>
	<? if($r_jenis!='K') { 
	   if($r_jenis!='S' and $r_jenis!='B'){
	?>
	<th width="80" align="center"><strong>Biaya</strong></th>
	<? } ?>
	<? if($r_jenis=='S' or $r_jenis=='B') {?>
	<th width="120" align="center"><strong>No. Surat</strong></th>
	<? } ?>
	<? if($r_jenis=='S') { ?>
	<th width="110" align="center"><strong>Judul Pustaka</strong></th>
	<th width="40" align="center"><strong>Jumlah</strong></th>
	<? } else { ?>
	<th width="40" align="center"><strong>Keterangan</strong></th>
	<? } ?>
	<?} if($r_jenis=='K') { ?>
	<th width="20" align="center"><strong>Komputer</strong></th>
	<th width="55" align="center"><strong>Jam Mulai</strong></th>
	<th width="55" align="center"><strong>Jam Selesai</strong></th>
	<? } ?>
	
  </tr>
  <?php
	$no=1;
	$jumlah = 0;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= Helper::tglEng($rs['tglpelayanan']) ?></td>
    <td align="left"><?= $rs['idanggota'] ?></td>
    <td align="left"><?= $rs['namaanggota'] ?></td>
	<? if($r_jenis!='K') { 
	if($r_jenis!='S' and $r_jenis!='B'){
	?>
	<td align="right"><?= $rs['bayarpelayanan']=='' ? "-" : Helper::formatNumber($rs['bayarpelayanan'],'0',true,true)  ?>&nbsp;</td>
	<? } ?>
	<? if($r_jenis=='S'  or $r_jenis=='B') {?>
	<td align="left"><?= $rs['nosurat'] ?></td>
	<? } ?>
	<? if($r_jenis=='S') { ?>
	<td align="left"><?= $rs['nama'] ?></td>
	<td align="left"><?= $rs['jumlah'] ?></td>
	<? } else {?>
	<td align="left"><?= $rs['keperluan']=='' ?  "-" : $rs['keperluan'] ?></td>
	<? }} if($r_jenis=='K') { ?>
	<td align="center"><?= $rs['idkomputer'] ?></td>
	<td align="center"><?= $rs['jamawal'] ?></td>
	<td align="center"><?= $rs['jamselesai'] ?></td>
	<? } ?>
  </tr>
	<? $no++; $jumlah += $rs['bayarpelayanan']; } ?>
	<? if($rsc<1) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak terdapat pelayanan umum</td>
	</tr>
	<? } ?>
    <? if ($jumlah!='0') { ?>
	<tr height=25 >	
	<? if ($jumlah=='0')
		$cols =9 ;
		else
		$cols =3; ?>
    <td align="left" colspan='<?= $cols ?>'><strong>Total Jumlah  : <?= $rsc; ?></strong></td>
	
    <td align="left"><strong>Total Biaya  </strong></td>
	<td align="right"><strong><?= Helper::formatNumber($jumlah,'0',true,true); ?>&nbsp;</strong></td>

  </tr>
  	<? } ?>
</table>


</div>
</body>
</html>