<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];

	// definisi variabel halaman
	$p_dbtable = 'lv_fakultas';
	$p_window = '[PJB LIBRARY] Daftar Fakultas PJB';
	$p_title = 'Daftar Fakultas PJB';
	$p_tbheader = '.: Daftar Fakultas :.';
	$p_col = 4;
	$p_tbwidth = 470;
	$p_filedetail = Helper::navAddress('ms_fakultas.php');
	$p_id = "ms_fakultas";
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by kdfakultas";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="30">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
		<header style="width:470px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20"> 
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Kode Fakultas</td>
				<td width="250" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Fakultas</td>
				<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Jurusan</td>
				
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<?= $row['kdfakultas']; ?>
				</td>
				<td><?= $row['namafakultas']; ?></td>
				<td align="center">
				<?php if($row['kdfakultas']!='0'){?>
				<u title="Detail Fakultas" onclick="window.open('<?= $p_filedetail."&kf=".$row['kdfakultas'] ?>',name='_self');" class="link"><img src="images/see.png"></u>
				<?}?>
				</td>
				
			</tr>
			<?
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr>
			<td colspan="3" class="footBG">&nbsp;
			</td>
			</tr>
		</table><br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

</html>