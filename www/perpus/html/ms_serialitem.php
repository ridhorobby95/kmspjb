<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require('classes/pdf.class.php');
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
 
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// definisi variabel halaman

	$p_window = '[PJB LIBRARY] Daftar Serial Control';
	$p_title = 'Daftar Serial Control';
	$p_tbheader = '.: Daftar Serial Control :.';
	$p_col = 9;
	$p_tbwidth = 550;
	$p_filedetail = Helper::navAddress('ms_serialcontrol.php');
	//$p_id = "pp_serialcontrol";
	$p_eksemplar = Helper::navAddress('ms_eksemplarberseri.php');
	$p_dbtable = 'pp_serialcontrol';
	
	$p_ekslist = Helper::navAddress('ms_eksemplar.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'idserial::integer';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	//parameter
	$r_key = Helper::removeSpecial($_REQUEST['key']); #idserial
	$r_jenis = Helper::removeSpecial($_REQUEST['jenis']);
	$r_ids = Helper::removeSpecial($_REQUEST['ids']); #idserialitem

	if($r_jenis == "add"){
		# get serial control
		$row = $conn->GetRow("select s.total_eksemplar, s.idpustaka, p.jumlah, p.satuan, t.judul, t.noseri, s.harga, s.kdsupplier  
				     from $p_dbtable s
				     join ms_pustaka t on t.idpustaka = s.idpustaka 
				     left join lv_periode p on p.idperiode = s.idperiode 
				     where idserial = '$r_key'");
		$nourut = Sipus::GetLastSeq($conn,pp_serialitem,seq_number," and idserial = '$r_key'");
		$nourutlast = $nourut - 1;
		$tglperkiraanLast = $conn->GetOne("select tglperkiraan from pp_serialitem where idserial = '$r_key' and seq_number = '$nourutlast' ");
		
		$kdsupplier = $row['kdsupplier'];
		
		if($row['satuan'] == "d"){
			$tambah = "+".$row['jumlah']." day";
		}elseif($row['satuan'] == "m"){
			$tambah = "+".$row['jumlah']." month";
		}elseif($row['satuan'] == "y"){
			$tambah = "+".$row['jumlah']." year";
		}
		
		if($nourut == 1){
			$tglperkiraan = date('Y-m-d');
		}else{
			$tglperkiraan = date('Y-m-d', strtotime($tglperkiraanLast . $tambah));
		}
	}elseif($r_jenis == "edit"){
		# get serial control
		$row = $conn->GetRow("select s.*, c.kdsupplier, r.kdperolehan, c.idpustaka, p.noseri    
				from pp_serialitem s 
				left join pp_serialcontrol c on c.idserial = s.idserial 
				left join pp_eksemplar r on r.idserialitem = s.idserialitem
				left join ms_pustaka p on p.idpustaka = r.idpustaka 
				where s.idserialitem = '$r_ids' and c.idserial = '$r_key'");
		$nourut = $row['seq_number'];
		$kdsupplier = $row['kdsupplier'];
		$tglperkiraan = $row['tglperkiraan'];
		$r_idpus = $row['idpustaka'];
		$rs = $conn->Execute("select * from pp_eksemplar where idserialitem = $r_ids order by noseri");

	}
	
	if(!empty($_POST)){
		$r_aksi = Helper::removeSpecial($_POST['act']);
		#untuk tambah
		if($r_aksi=='simpan' and $r_jenis=="add"){  
			$kdperolehan = Helper::removeSpecial($_POST['kdperolehan']);

			$conn->StartTrans();
			$record = array();
			$record['idserialitem'] = Sipus::GetLast($conn,pp_serialitem,idserialitem);
			$record['tglperkiraan'] = Helper::formatDate($_POST['tglperkiraan']);
			$record['tgldatang'] = Helper::formatDate($_POST['tgldatang']);
			$record['jumlah'] = Helper::removeSpecial($_POST['jumlah']);
			$record['nomor'] = Helper::removeSpecial($_POST['nomor']);
			$record['volume'] = Helper::removeSpecial($_POST['volume']);
			$record['tahun'] = Helper::removeSpecial($_POST['tahun']);
			$record['keterangan'] = Helper::removeSpecial($_POST['keterangan']);
			$record['harga'] = Helper::removeSpecial($_POST['harga']);
			$record['seq_number'] = Sipus::GetLastSeq($conn,pp_serialitem,seq_number," and idserial = '$r_key'");
			$record['idserial'] = $r_key;
			Helper::Identitas($record);
			
			# upload file	
			for ($i = 0; $i <= count ($_FILES['userfile']); $i++)
			{
				$test=($i-1)+2;
				$field = "file".$test;
				$tmp_file = $_FILES['userfile']['tmp_name'][$i];
				$filetype = $_FILES['userfile']['type'][$i];
				$filesize = $_FILES['userfile']['size'][$i];
				$filename = $_FILES['userfile']['name'][$i];
				$ext=explode(".",$filename);
				$extjum=count($ext);
				$eksten=$ext[$extjum-1];
				$fileup=Helper::removeSpecial($filename);
				$seri = $row['idpustaka'].'_'.$record['idserialitem'];
				
				$directori = 'uploads_pdfmirrorghost/file/' . $seri;
			
				if(!is_dir($directori))
						mkdir($directori,0750);
					
				$destination = $directori .'/'.$fileup;
				
				
				if ($filesize < 10000000){
					if ($filetype=='application/pdf') {

						if (copy($tmp_file,$destination))
						{	
							$record[$field]=Helper::cStrNull($destination);
							//Pdf::ghostFile($destination,$seri);
						}
					}
					
				}else {
					$errdb = 'Besar File Melebihi 10 MB.';	
					Helper::setFlashData('errdb', $errdb);
				}
				
			}


			Sipus::InsertBiasa($conn,$record,pp_serialitem);

			$recordu = array();
			$recordu['harga'] = $record['harga'];
			Helper::Identitas($recordu);
			Sipus::UpdateBiasa($conn,$recordu,pp_serialcontrol,idserial,$r_key);
			
			$eks = $record['jumlah'];
			if($eks!='' and $eks>0){
				for($i=0;$i<$eks;$i++){
				$receks['idpustaka']=$row['idpustaka'];
				$receks['kdklasifikasi']='LT';
				$receks['kdperolehan']=$kdperolehan;
				$receks['kdkondisi']='V';
				$receks['tglperolehan']=$record['tgldatang'];
				$receks['tglterbit']=$record['tgldatang'];
				$receks['noseri']=Sipus::CreateSeri($conn,$row['noseri'],$row['idpustaka']);
				$receks['statuseksemplar']='ADA';
				$receks['idserialitem']=$record['idserialitem'];
				$receks['harga']=$record['harga'];
				$receks['kdsupplier']=$kdsupplier;
				Helper::Identitas($receks);
				
				Sipus::InsertBiasa($conn,$receks,pp_eksemplar);
				}
			}else
			$error=true;
						
			if($error==true)
				$conn->CompleteTrans(false);
			else
				$conn->CompleteTrans(true);
			
			
			
			if($conn->ErrorNo() == 0) {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			} else {
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			Helper::redirect();
		}
		#untuk update
		if($r_aksi=='simpan' and $r_jenis=="edit"){
			$kdperolehan = Helper::removeSpecial($_POST['kdperolehan']);
			
			$conn->StartTrans();
			$record = array();
			$record['tglperkiraan'] = Helper::formatDate($_POST['tglperkiraan']);
			$record['tgldatang'] = Helper::formatDate($_POST['tgldatang']);
			$record['nomor'] = Helper::removeSpecial($_POST['nomor']);
			$record['volume'] = Helper::removeSpecial($_POST['volume']);
			$record['tahun'] = Helper::removeSpecial($_POST['tahun']);
			$record['keterangan'] = Helper::removeSpecial($_POST['keterangan']);
			$record['harga'] = Helper::removeSpecial($_POST['harga']);
			$record['seq_number'] = $nourut;
			$record['idserial'] = $r_key;
			Helper::Identitas($record);
			
			# upload file	
			for ($i = 0; $i <= count ($_FILES['userfile']); $i++)
			{
				$test=($i-1)+2;
				$field = "files".$test;
				$tmp_file = $_FILES['userfile']['tmp_name'][$i];
				$filetype = $_FILES['userfile']['type'][$i];
				$filesize = $_FILES['userfile']['size'][$i];
				$filename = $_FILES['userfile']['name'][$i];
				$ext=explode(".",$filename);
				$extjum=count($ext);
				$eksten=$ext[$extjum-1];
				$fileup=Helper::removeSpecial($filename);
				$seri = $row['idpustaka'].'_'.$r_ids;
				
				$directori = 'uploads_pdfmirrorghost/file/' . $seri;
			
				if(!is_dir($directori))
						mkdir($directori,0750);
					
				$destination = $directori .'/'.$fileup;
				
				
				if ($filesize < 10000000){
					if ($filetype=='application/pdf') {

						if (copy($tmp_file,$destination))
						{	
							$record[$field]=Helper::cStrNull($destination);
							//Pdf::ghostFile($destination,$seri);
						}
					}
					
				}else {
					$errdb = 'Besar File Melebihi 10 MB.';	
					Helper::setFlashData('errdb', $errdb);
				}
				
			}


			#update tabel pp_serialitem
			Sipus::UpdateBiasa($conn,$record,pp_serialitem,idserialitem,$r_ids);
			
			$recordu = array();
			$recordu['harga'] = $record['harga'];
			Helper::Identitas($recordu);
			Sipus::UpdateBiasa($conn,$recordu,pp_serialcontrol,idserial,$r_key);
			
			#update data eksemplar
			$receks['kdperolehan']=$kdperolehan;
			$receks['tglperolehan']=$record['tgldatang'];
			$receks['tglterbit']=$record['tgldatang'];
			$receks['harga']=$record['harga'];
			Helper::Identitas($receks);
			Sipus::UpdateBiasa($conn,$receks,pp_eksemplar,idserialitem,$r_ids);

			if($error==true)
				$conn->CompleteTrans(false);
			else
				$conn->CompleteTrans(true);
			
			if($conn->ErrorNo() == 0) {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			} else {
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			Helper::redirect();
		}
		#delete File
		else if($r_aksi =="delfile"){ 
			$file=Helper::removeSpecial($_POST['delfile']);
			$fkey=Helper::removeSpecial($_POST['delkey']);
			$seri=Helper::removeSpecial($_POST['delseri']); 
			unlink($file);
			$fdel = explode('.',Helper::GetPath($file));
			$hap = Helper::HapusFolder('ghostfile/'.$r_idpus."_".$seri.'/'.$fdel[0]);
			$recfile[$fkey]=null;
			Sipus::UpdateBiasa($conn,$recfile,pp_serialitem,idserialitem,$r_ids);
			
				if($conn->ErrorNo() != 0){
					$errdb = 'Penghapusan file upload gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penghapusan file upload berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&jenis=edit&ids='.$seri.'&key='.$r_key;
					Helper::redirect($url);
					
				}
		
		}
	}
	
	$rs_cb = $conn->Execute("select namaperolehan, kdperolehan from lv_perolehan order by kdperolehan");
	$l_perolehan = $rs_cb->GetMenu2('kdperolehan',$row['kdperolehan'],true,false,0,'id="kdperolehan" class="ControlStyle" style="width:200"');

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onUnload="window.opener.refreshDetail('<?=$r_key;?>');" >
<div align="center">
<table width="100">
	<tr>
	<?  if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? }?>
	</tr>
</table><br><?php include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" enctype="multipart/form-data" >
	
	<table width="<?=$p_tbwidth;?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

	<tr height="25">
		<td width="35%" class="LeftColumnBG">No. Urut</td>
		<td width="35%" class="RightColumnBG"><b><?= $nourut ?></b></td>
	<tr> 
		<td width="35%" class="LeftColumnBG">Tanggal Pesan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglperkiraan',Helper::formatDate($tglperkiraan),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglpesan" style="cursor:pointer;" title="Pilih tanggal pesan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglperkiraan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglpesan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>	
	<tr> 
		<td class="LeftColumnBG">Tanggal Datang *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tgldatang',Helper::formatDate($row['tgldatang']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglakhir" style="cursor:pointer;" title="Pilih tanggal datang">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgldatang",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglakhir",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Jumlah *</td>
		<td class="RightColumnBG"><?= $r_jenis == "edit" ? $row['jumlah'] : UI::createTextBox('jumlah',$row['total_eksemplar'],'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr height=20> 
		<td class="LeftColumnBG">Asal Perolehan *</td>
		<td class="RightColumnBG"><?= $l_perolehan; ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Nomor *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('nomor',$row['nomor'],'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Volume *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('volume',$row['volume'],'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tahun *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahun',$r_jenis == "edit"?$row['tahun']:date('Y'),'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Harga *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Catatan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('keterangan',$row['keterangan'],'ControlStyle',255,50,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">File Upload</td>
		<td class="RightColumnBG">
			<? for($i=1;$i<=10;$i++){ 
			$nama='files'.$i;
			if ($row[$nama]=='')
				continue;
			?>
			<?  if($c_edit) { ?>
			<a href="<?= $row[$nama] ?>"><?= $row[$nama]!='' ? "<img src='images/attach.gif' border=0> ".$i.'. '.Helper::GetPath($row[$nama]) : '' ?></a>
			&nbsp; <img src="images/delete.png" style="cursor:pointer" alt="Hapus file upload" onClick="goDelFile('<?= $nama ?>','<?= $row[$nama] ?>','<?= $row['idserialitem'] ?>')">
			<br>
			<?} else echo $i.'. '.$row[$nama]."<br>"; 
				
			} ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">File<br><span style="font-weight:normal">( Max Upload 5 MB )</span></td>
		<td class="RightColumnBG">
			<input type="hidden" value="2" id="theValue" />
			<br>
			1. <input type="file" name="userfile[]" size="40" style="cursor:pointer">&nbsp; &nbsp;<input type="button" name="btntext" onClick="CreateTextbox()" value="Tambah File" style="cursor:pointer">
			<p>2. <input type="file" name="userfile[]" size="40" style="cursor:pointer"></p>
			<p>3. <input type="file" name="userfile[]" size="40" style="cursor:pointer"></p>
			<div id="createTextbox"></div>
		</td>
	</tr>
</table><br>
<?php if($r_jenis == "edit"){?>
<table border=1 width="<?=$p_tbwidth;?>" style="border-collapse:collapse;border-color:#999;margin-top: 10px;">
	<tr>
		<th style="background:#ffc656;color:#545454;" align="center" colspan="5">EKSEMPLAR</th>
	</tr>
	<tr>
		<th style="background:#ffc656;color:#545454;">REG COMP</th>
		<th style="background:#ffc656;color:#545454;">Nama Rak</th>
		<th style="background:#ffc656;color:#545454;">Lokasi</th>
		<th style="background:#ffc656;color:#545454;">Status</th>
	</tr>
	<? $i=0;
		while ($roweks = $rs->FetchRow()){
			$i++;
	?>
	<tr>
		<!--<td><u title="Edit Eksemplar" style="cursor:pointer;color:blue;" onclick="javascript:goDetEks('<?= $p_ekslist; ?>','<?= $roweks['ideksemplar']; ?>','<?= $roweks['idpustaka'] ?>');" ><?= $roweks['noseri']?></u></td>-->
		<td><?= $roweks['noseri']?></td>
		<td style="color:<?= $warna ?>"><?= $roweks['namarak']?></td>
		<td style="color:<?= $warna ?>"><?= $roweks['namalokasi']?></td>
		<td style="color:<?= $warna ?>"><?= $roweks['statuseksemplar']=='ADA' ? 'ADA' : ($roweks['statuseksemplar']=='PJM' ? $roweks['idanggota'].' - '. $roweks['namaanggota'] : 'PROSES');?></td>
	</tr>
	<? } ?>
	<? if ($i==0) { ?>
	<tr>
		<td colspan="4" align="center"><b>Data Eksemplar tidak ada.</b></td>
	</tr>
	<? } ?>
</table>
<?php } ?>

<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="jenis" id="jenis" value="<?= $r_jenis ?>">
<input type="hidden" name="ids" id="ids" value="<?= $r_ids ?>">
<input type="hidden" name="key3" id="key3">
	
<input type="hidden" name="delfile" id="delfile">
<input type="hidden" name="delkey" id="delkey">
<input type="hidden" name="delseri" id="delseri">
</form>
</div>


</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xauto.js"></script>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">
function CreateTextbox()
{

	var h = document.getElementById('theValue');
	var max = document.getElementById('theValue').value;
	if (max <9){
		var i = (document.getElementById('theValue').value -1)+2;
		h.value=i;
		var x='file'+i;
		var no=i+1;
		createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type=file name='userfile[]' size='40' /> </p>";
	}

}

function goClear(){
	document.getElementById("periode").value='';
}

function saveData() {
	
	<?php if($r_jenis == "edit"){?>
		var cek = "tglperkiraan,tgldatang,nomor,volume,tahun,harga,kdperolehan";
	<?php }elseif($r_jenis == "add"){ ?>
		var cek = "tglperkiraan,tgldatang,jumlah,nomor,volume,tahun,harga,kdperolehan";
	<?php } ?>
	
	if(cfHighlight(cek)){
		document.getElementById('act').value='simpan'; 
		goSubmit();
	}
}

function goDelFile(key1,key2,key3){
	var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
	if(delfile){
		document.getElementById('act').value="delfile";
		document.getElementById('delkey').value=key1;
		document.getElementById('delfile').value=key2;
		document.getElementById('delseri').value=key3;
		goSubmit();
	}
}
</script>
</html>