<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_thn = Helper::removeSpecial($_POST['tahun']);
	$r_unit = Helper::removeSpecial($_POST['kdjurusan']);
	$r_bln = Helper::removeSpecial($_POST['bulan']);
//$conn->debug=true;
	if($r_format=='' or $r_thn==''){
		header("location: index.php?page=home");
	}

	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Data Pengunjung Perpustakaan PJB <br>Tahun '.$r_thn ;
	
	$p_namafile = 'rekap_kunjungan_user_'.$r_thn;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(*) jml, idanggota, to_char(t_updatetime,'mm') bulan, to_char(t_updatetime,'YYYY-mm-dd') tgl, to_char(t_updatetime,'dd') hari
		from pp_bukutamu 
		where to_char(t_updatetime,'YYYY') = '$r_thn' and to_char(t_updatetime,'mm') = '$r_bln' ";
	
	if(!empty($r_unit)){
		$sql .= " and idunit = '$r_unit' ";
	}

	$sql .= " group by idanggota, to_char(t_updatetime,'mm'), to_char(t_updatetime,'YYYY-mm-dd'), to_char(t_updatetime,'dd') ";

	$rs = $conn->Execute($sql);
	while($row=$rs->FetchRow()){
		$data['user'][$row['idanggota']][$row['bulan']][$row['hari']] = $row['jml'];
		$data[$row['idanggota']][$row['bulan']] = (int) $data[$row['idanggota']][$row['bulan']] + (int) $row['jml'];
		$data['hari'][$row['bulan']][$row['hari']] = (int) $data[$row['bulan']][$row['hari']] + (int) $row['jml'];		
		$data[$row['bulan']] = (int) $data[$row['bulan']] + (int) $row['jml'];
	}
	
	$bulan = array();
	$bulan[1] = "JAN";
	$bulan[2] = "FEB";
	$bulan[3] = "MAR";
	$bulan[4] = "APR";
	$bulan[5] = "MEI";
	$bulan[6] = "JUN";
	$bulan[7] = "JUL";
	$bulan[8] = "AGTS";
	$bulan[9] = "SEP";
	$bulan[10] = "OKT";
	$bulan[11] = "NOV";
	$bulan[12] = "DES";
		
	$sql_u = "select * 
			from um.users 
			where 1=1 ";
	if(!empty($r_unit)){
		$sql_u .= " and kodeunit = '$r_unit' ";
	}
	
	$sql_u .= "order by kodeunit, nama ";
	$users = $conn->Execute($sql_u);
	
	$satker = "";
	if(!empty($r_unit)){
		$satker .= $conn->GetOne("select namasatker from ms_satker where kdsatker = '$r_unit' ");
		$judul = "Laporan Data Pengunjung <br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn."<br/>".$satker;
	}else{
		$judul = "Laporan Data Pengunjung Perpustakaan<br>".strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;
	}
	
	$sql_l = "select to_char(tgllibur,'YYYY-mm-dd') tgllibur from ms_libur where to_char(tgllibur,'mm') = '$r_bln' ";
	$liburs = $conn->GetArray($sql_l);
	if(count($liburs)>0){
		$holy = array();
		foreach($liburs as $libur){
			$holy[$libur['tgllibur']] = true;
		}
	}
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=998>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<br><br>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2><?= $judul; ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="1000" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="left"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>

	<? 
	$at = date('t', strtotime($r_thn.'-'.$r_bln.'-1'));	
	?>
	<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
		<tr>
			<th width="5%" rowspan=2>NO</th>
			<th width="15%" rowspan=2>NAMA</th>
			<th width="5%" rowspan=2>NID</th>
			<th colspan="<?=$at;?>"><?=strtoupper(Helper::bulanInd($r_bln))." ".$r_thn;?></th>
			<th width="10%" rowspan=2>JUMLAH</th>
		</tr>
		<tr>
			
			<? for($t=1; $t<=$at; $t++){?>
			<th><?=$t;?></th>
			<? } ?>
			
		</tr>
		<? $u=0;foreach($users as $user){$u++;?>
		<tr height="25">
			<td align="center"><?= $u; ?></td>
			<td><?= $user['nama']; ?></td>
			<td><?= $user['nid']; ?></td>
			<? for($t=1; $t<=$at; $t++) {
				$date = $r_thn.'-'.$r_bln.'-'.Helper::plusNol($t);
				$h=date("w",strtotime($date));
				if($h==0 or $h==6){
					$bdc = '#F5A8A8';
					$nilai = '';
				}elseif($holy[$date]){
					$bdc = '#9DE6A5';
					$nilai = '';
				}else{
					$bdc = '#FFFFFF';
					$nilai = number_format($data['user'][$user['nid']][$r_bln][$t],0, ',', '.');
				}
			?>
			<td style="background-color: <?=$bdc;?>" width="30" align="right"><?= $nilai; ?></td>
			<? } ?>
			<td width="30" align="right"><?= number_format($data[$user['nid']][$r_bln],0, ',', '.'); ?></td>
		</tr>
		<? } ?>
		<tr>
			<td align="right" colspan=3><b>Jumlah</b> </td>
			
			<? for($t=1; $t<=$at; $t++){
				$date = $r_thn.'-'.$r_bln.'-'.Helper::plusNol($t);
				$h=date("w",strtotime($date));
				if($h==0 or $h==6){
					$bdc = '#F5A8A8';
					$nilai = '';
				}elseif($holy[$date]){
					$bdc = '#9DE6A5';
					$nilai = '';
				}else{
					$bdc = '#FFFFFF';
					$nilai = (number_format($data['hari'][$r_bln][$t],0, ',', '.'));
				}
			?>
			<td style="background-color: <?=$bdc;?>" align="right"><?= $nilai; ?></td>
			<? } ?>
			<td align="right"><?= number_format($data[$r_bln],0, ',', '.'); ?></td>
		</tr>
	</table><br/>
		<table width="1000" cellspacing="0" cellpadding="4"><tr><td align="left">
		<table width="200" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
			<tr>
				<th colspan="2">KETERANGAN</th>
			</tr>
			<tr>
				<td style="background-color: #F5A8A8" width="30%"></td>
				<td>Sabtu/Minggu</td>
			</tr>
				<?
					$liburan = Sipus::getLibur($conn,$r_bln,$r_thn);
					if(count($liburan)>0){
						foreach($liburan as $libur){
				?>
			<tr>
				<td valign="top" align="center" style="background-color: #9DE6A5" width="30%"><?=$libur['tgl'];?></td>
				<td><?=$libur['namaliburan'];?></td>
			</tr>
				<?
						}
					}
				?>
		</table>
		</td>
		</tr></table>
	<br/><br/>

<br><br>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="scripts/highcharts/highcharts.js"></script>
<script type="text/javascript" src="scripts/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    var chart_test;
	
    $(document).ready(function() {
		
	//testing
	Highcharts.setOptions({
		colors: ['#4AA02C', '#F88017']
	});
	
        chart_tes = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
			type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {	
				title: {
                    text: 'Bulan'
                },
                categories: [<?=$categories;?>]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Peminjam'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<strong>' + this.series.name + ': </strong>' + this.y;
                }
            },
            plotOptions: {
                column: {
					pointPadding: 0.2,
					borderWidth: 0,
					dataLabels: {
							enabled: true,
							formatter: function() {
								return '<b>'+ this.y +'</b>';
							
								//return '<b>'+ this.series.name +'</b>: '+ this.y;
						}
					}
                }
            },
            series: [
			<?
			foreach($data['jurusan'] as $jur => $value){
				$bar = array();
				for($i=1; $i<=$jumlahbulan; $i++) {
					$bar[] = number_format($data['jurusan'][$jur][$i]['jml'],0, ',', '.');
				}
			?>
				{
					name: '<?=$jur." - ".$data['jurusan'][$jur]['satker'];?>',
					data: [<?=implode(",",$bar)?>]
				},
			<?
			}
			?>
		]
        });


    });    
});

</script>
</html>