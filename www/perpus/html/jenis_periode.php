<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	

	
	// definisi variabel halaman
	$p_dbtable = 'lv_periode';
	$p_window = '[PJB LIBRARY] Daftar Periode Terbitan Berkala';
	$p_title = 'Daftar Periode Terbitan Berkala';
	$p_tbheader = '.: Daftar Periode Terbitan Berkala :.';
	$p_col = 3;
	$p_tbwidth = 100;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by idperiode";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['idperiode'] = Helper::cStrNull($_POST['i_idperiode']);
			$record['namaperiode'] = Helper::cStrNull($_POST['i_namaperiode']);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
			}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			//$record['kdjenispustaka'] = Helper::cStrNull($_POST['u_kdjenispustaka']);
			$record['namaperiode'] = Helper::cStrNull($_POST['u_namaperiode']);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idperiode,$r_key);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
			else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idperiode,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<!--table width="<?//= $p_tbwidth; ?>">
			<tr height="40">
				<td align="center" colspan="2" class="PageTitle"><?//= $p_title; ?></td>
			</tr>
		</table-->
		<? include_once('_notifikasi.php'); ?>
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20"> 
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Kode Periode</td>
				<td nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Periode</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idperiode'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<a title="Sunting Data" onclick="goEditIP('<?= $row['idperiode']; ?>');" class="link"><?= $row['idperiode']; ?></u>
				<? } else { ?>
					<?= $row['idperiode']; ?>
				<? } ?>
				</td>
				<td><?= $row['namaperiode']; ?></td>
				<td align="center">
				<? if($c_delete) { ?>
                <img src="images/delete.png" title="Hapus pelayanan" onclick="goDeleteIP('<?= $row['idperiode']; ?>','<?= $row['namaperiode']; ?>');" style="cursor:pointer">
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= $row['idperiode'] //UI::createTextBox('u_kdjenispustaka',$row['kdjenispustaka'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_namaperiode',$row['namaperiode'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center">
				<? if($c_edit) { ?>
                <img src="images/edit.png" title="Update Data" onclick="updateData('<?= $row['idperiode']; ?>');" style="cursor:pointer">
					
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_idperiode','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_namaperiode','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr>
			<td colspan="3" class="footBG">&nbsp;
			</td>
			</tr>
		</table>
                </div>
                <br>

		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namaperiode').focus();
	<? } else { ?>
	document.getElementById('i_idperiode').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_idperiode,i_namaperiode"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_namaperiode")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>