<?php 
	set_time_limit(0);
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');
	require_once('../classes/mail.class.php');

	$cek = $conn->IsConnected();

	if(!$cek){
	    //Mail::SendMail("abu_wes@yahoo.com","Perpustakaan PJB","Error Schedule Cron Perpustakaan","Eksekusi Cron dari Server Perpustakaan untuk mengambil data pegawai dari database Akademik mengalami gangguan, sehingga tidak dapat berjalan dengan baik. Mohon dilakukan check data.\n Cron Date : ".date('d-m-Y H:i:s')); 
	}
	
	$start = date('Y-m-d', strtotime('-4 days')); 
	$end = date('Y-m-d'); 
	
	//cron ini untuk pegawai
	$sqlstr = "select u.nid, u.nama as namaanggota, u.jeniskel as jk, u.alamat, u.telp, u.email, u.kodepos, 
			u.iduser, u.kodeunit, u.kodestatus, u.isactive, u.isextrauser  
			from um.users u ";
			//where TO_DATE(TO_CHAR (u.tupdatetime, 'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$start','YYYY-mm-dd') and to_date('$end','YYYY-mm-dd') ";

	$rs = $conn->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	while ($row = $rs->FetchRow()){
		if($row['isextrauser']){
			$kdjenisanggota = 'X';
		}else{
			$kdjenisanggota = '1';
		}
		
		$record=array();
		$record['idpegawai']=Helper::removeSpecial($row['iduser']);
		$record['statusanggota']=(Helper::removeSpecial($row['isactive'])?Helper::removeSpecial($row['isactive']):"0"); 
		$record['idunit']=Helper::removeSpecial($row['kodeunit']);
		$record['kdjenisanggota']=$kdjenisanggota;
		$record['idanggota']=Helper::removeSpecial(trim($row['nid']));
		$record['namaanggota']=Helper::removeSpecial($row['namaanggota']);
		$record['jk']=Helper::removeSpecial($row['jk']);
		$record['alamat']=Helper::removeSpecial($row['alamat']);
		$record['kodepos']=Helper::removeSpecial($row['kodepos']);
		if($row['email'])
			$record['email']=Helper::removeSpecial($row['email']);
		$record['telp']=Helper::removeSpecial($row['telp']);
		$record['t_user'] = 'cron';
		$record['t_updatetime'] = $now;
		$record['t_host'] = Helper::removeSpecial($_SERVER['REMOTE_ADDR']);	
		
		$cNrp = $conn->GetOne("select count(*) from ms_anggota where idanggota = '".trim($row['nid'])."' ");
		//$cNrp = $conn->GetOne("select count(*) from ms_anggota where idanggota = '".trim($row['nid'])."' and idpegawai = '".trim($row['iduser'])."' ");
		if($cNrp>0){
			$col = $conn->Execute("select * from ms_anggota where idanggota = '".trim($row['nid'])."' ");
			//$col = $conn->Execute("select * from ms_anggota where idanggota = '".trim($row['nid'])."' and idpegawai = '".trim($row['iduser'])."' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			$conn->Execute($sql);
		}else{
			$col = $conn->Execute("select * from ms_anggota where 1=-1 ");
			$sql = $conn->GetInsertSQL($col,$record,true);
			$conn->Execute($sql);
		}
	}
?>