<?php
	//$conn->debug=false;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	include_once('includes/init.php');
		
	// require tambahan
	

	$r_id=Helper::removeSpecial($_GET['id']);

	$rowl=$conn->GetRow("select p.*,a.idanggota,a.namaanggota,jp.namapelayanan from pp_pelayanan p
						 join ms_anggota a on p.idanggota=a.idanggota
						 join lv_jenispelayanan jp on p.kdpelayanan=jp.kdpelayanan
						 where p.idpelayanan=$r_id");
	$total=$rowl['bayarpelayanan']+$rowl['denda'];
	$user= $conng->GetRow("select userdesc from gate.sc_user where username='".$_SESSION['PERPUS_USER']."'");
?>

<html>
<head>
<title>Nota Transaksi</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 7pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onLoad="printpage()">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>PERPUSTAKAAN PJB<br>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="430" border="0" >
	<tr>
		<td width="80">Id Anggota</td>
		<td>: <?= $rowl['idanggota'] ?></td>
	</tr>
	<tr>
		<td>Nama Anggota</td>
		<td>: <?= $rowl['namaanggota'] ?></td>
	</tr><br>
	<tr>
		<td colspan="2"><br>
		<u>PELAYANAN UMUM</u>
		<table cellpadding="0" cellspacing="0" width="230" border=0 class="nota">
		<tr height="20">
			<td width="70" style="border-bottom:1px solid">Jns Pel.</td>
			<td width="90" style="border-bottom:1px solid">Harga</td>
			<td width="90" style="border-bottom:1px solid">Denda</td>
			<td width="90" style="border-bottom:1px solid">Total</td>
		</tr>
		<tr height="15">
			<td><?= $rowl['namapelayanan'] ?></td>
			<td><?= Helper::formatNumber($rowl['bayarpelayanan'],'0',true,true); ?></td>
			<td><?= $rowl['denda']='' ? '-' : Helper::formatNumber($rowl['denda'],'0',true,true) ?></td>
			<td><?= Helper::formatNumber($total,'0',true,true); ?></td>
		</tr>

		</table><br>
	</td>
	</tr>
</table>
		
<br>
<table>
	<tr>
		<td>Terima Kasih | <?= $rowl['tglpelayanan'] ?><br>
		<?= $user['userdesc'] ?></td>
	</tr>
</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>