<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Cetak Label Pustaka';
	$p_title = 'Cetak Label Pustaka';
	$p_tbheader = '.: Cetak Label Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_filedetail = Helper::navAddress('ms_eksemplar.php');
	$p_id = "lbl_eksemplar";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'e.noseri asc';
	$p_row = 21;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';

	// sql untuk mendapatkan isi list
	$p_sqlstr="select to_char(p.judul) judul, e.idpustaka, e.ideksemplar,e.noseri,e.statuseksemplar, p.nopanggil, k.namaklasifikasi, l.namalokasi
		from pp_eksemplar e
		join ms_pustaka p on e.idpustaka=p.idpustaka 
		left join lv_lokasi l on e.kdlokasi=l.kdlokasi
		left join lv_klasifikasi k on e.kdklasifikasi=k.kdklasifikasi
		where 1=1 $sqlAdminUnit ";
			   
	// pengaturan ex
	if (!empty($_POST))
	{		
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		$keyklasifikasi=Helper::removeSpecial($_POST['kdklasifikasi']);
		$keykondisi = Helper::removeSpecial($_POST['kdkondisi']);
		$keyjenis = Helper::removeSpecial($_POST['kdjenispustaka']);
		$keytglentri = Helper::formatDate($_POST['tglentri']);
		$keyreg = Helper::removeSpecial($_POST['carireg'],true);
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);

		if($r_aksi == "refresh"){
			unset($_SESSION['keyreg']);
		}else{
			if(!empty ($keyjudul))
				$_SESSION['keyreg'] = $keyjudul;
		}
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
				
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	if($_SESSION['keyreg']!='')
		$p_sqlstr .= !empty ($_SESSION['keyreg']) ? " and upper(to_char(p.judul)) like upper('%".$_SESSION['keyreg']."%') " : " and 1=0 " ;
	
	
	// Group by
	$p_sqlstr .=" group by to_char(p.judul), e.idpustaka, e.ideksemplar,e.noseri,e.statuseksemplar,k.namaklasifikasi, l.namalokasi, p.nopanggil ";
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list	
	$rs = $conn->GetArray($p_sqlstr);
	
	$showlist = true;
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi order by kdklasifikasi");
	$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$keyklasifikasi,true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:150" onchange="goSubmit()"');
	$l_klasifikasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_klasifikasi);	
	$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
	$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$keykondisi ,true,false,0,'id="kdkondisi" class="ControlStyle" style="width:150" onchange="goSubmit2()"');
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$keyjenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" onchange="goSubmit2()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);	

?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">	
			<? include_once('_notifikasi.php'); ?>
			<div class="filterTable table-responsive">
				<table border=0 width="100%" cellpadding="4" cellspacing=0>
					<tr>
					<td width="150">Judul Pustaka</td>
					<td colspan=2>:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $_SESSION['keyreg'] ?>" onKeyDown="etrCari(event);"></td>
					<td>
						<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">Refresh</span></a>
					</td>
					</tr>
					<tr>
						<td colspan="8">
							<img src="images/cetak-barcode.png" width=32 height=32 title="Cetak Barcode" style="cursor:pointer" onClick="goCodePrint()">&nbsp;&nbsp;
							<img src="images/cetak-label.png" width=32 height=32 title="Cetak Label" style="cursor:pointer" onClick="goLabelPrint()">&nbsp;&nbsp;
						</td>
					</tr>
				</table>
			</div><br />
			<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
            <div class="table-responsive">
			<table width="100%" border="0" cellpadding="2" cellspacing=0 class="GridStyle">
				<tr height="20"> 
				<td width="70" nowrap align="center" class="SubHeaderBGAlt thLeft">
					<u title="Pilih Semua" onclick="goAll()" style="cursor:pointer">
						<input type="checkbox" name="all" id="all" value="all" onClick="goAll()">
					</u>
				</td>
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft">NO INDUK</td>
				<td width="90%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>		
				<td width="90%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.nopanggil:C');">No. Panggil  <?= $p_xsort['nopanggil']; ?></td>	
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','k.namaklasifikasi:C');">Klasifikasi<?= $p_xsort['namaklasifikasi']; ?></td>
				<td width="190" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi <?= $p_xsort['namalokasi']; ?></td>
				
				
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					foreach ($rs as $row) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
						
						if($_SESSION['justnow'] == $row['noseri']){
							$color = "green";
						}else{
							$color = "black";
						}
			?>
			<tr class="<?= $rowstyle ?>" style="color: <?=$color;?>" height="25" valign="middle"> 
				<td align="center">
				<input type="checkbox" name="pilih[]" id="pilih" value="<?= $row['noseri'] ?>">
				</td>
				<td align="left"><?= $row['noseri']; ?></td>
				<td align="left"><?= $row['judul']; ?></td>
				<td align="left"><?= $row['nopanggil']; ?></td>
				<td align="center"><?= $row['namaklasifikasi']; ?></td>		
				<td align="left"><?= $row['namalokasi']; ?></td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" height=20 colspan="<?= $p_col; ?>"> 
				</td>
				
			</tr>
		</table>
                </div>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="test" id="test" value="<?= $keykondisi=='' ? "test" : $keykondisi ?>">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>
		</form>
		</div>
	</div>
</div>



</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

document.getElementById('carijudul').focus();

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carijudul").value='';
	document.getElementById("act").value='refresh';
	//goSubmit();
}


function goAll () {
	var aa= document.getElementById('perpusform');
	var a=document.getElementsByName("pilih[]");

	if (aa.all.checked==true){
		checked = true
	}else{
		checked = false
	}
	
	for (var i = 0; i < a.length; i++) {
		a[i].checked = checked;
	}
	
}

function goCodePrint() { 
	
	var a=document.getElementsByName("pilih[]");
	var kd='';
	var total="";
	for(var i=0; i < a.length; i++){
		if(a[i].checked)
			total +=a[i].value+"|";
	}
	if(total==''){
		alert("Harap Pilih Pustaka dahulu");
		return false;
	}

	window.open('index.php?page=cetak_label&code='+kd+'&start='+total+'&c=y');
}
function goLabelPrint() { 
	
	var a=document.getElementsByName("pilih[]");
	var kd='';
	var total="";
	for(var i=0; i < a.length; i++){
		if(a[i].checked)
			total +=a[i].value+"|";
	}
	
	if(total==''){
		alert("Harap Pilih Pustaka dahulu");
		return false;
	}

	window.open('index.php?page=cetak_label&code='+kd+'&start='+total+'&c=n&j=<?= $keyjenis ?>');
}

function goAvailable() {
	document.getElementById("act").value="available";
	goSubmit();

}

function goSubmit2() {
	goSubmit();
}
</script>

</html>
