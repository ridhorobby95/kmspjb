<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and idunit in ($units) ";
		
	// definisi variabel halaman
	$p_dbtable = 'lv_lokasi';
	$p_window = '[PJB LIBRARY] Daftar Lokasi Pustaka';
	$p_title = 'Daftar Lokasi Pustaka';
	$p_tbheader = '.: Daftar Lokasi Pustaka:.';
	$p_col = 3;
	$p_tbwidth = 100;
	$p_id = "ms_lokasi";
	$p_row = 15;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable where 1=1 $sqlAdminUnit";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['kdlokasi'] = Helper::cStrNull($_POST['i_kdlokasi']);
			$record['namalokasi'] = Helper::cStrNull($_POST['i_lokasi']);
			$record['internal'] = Helper::cNumNull($_POST['i_internal']);
			$record['idunit'] = Helper::removeSpecial($_POST['idsatker']);
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,lv_lokasi);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
			}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['kdlokasi'] = Helper::cStrNull($_POST['u_kdlokasi']);
			$record['namalokasi'] = Helper::cStrNull($_POST['u_lokasi']);
			$record['internal'] = Helper::cNumNull($_POST['u_internal']);
			$record['idunit'] = Helper::removeSpecial($_POST['u_idsatker']);
			Helper::Identitas($record);
			
			$err=Sipus::UpdateBiasa($conn,$record,lv_lokasi,kdlokasi,$r_key);
			
			if($err != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,lv_lokasi,kdlokasi,$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
		//filtering
	$keylokasi=Helper::removeSpecial($_POST['carilokasi']);
	if($keylokasi!='')
		$p_sqlstr.=" and upper(namalokasi) like upper('%$keylokasi%')";
	}else
	{
		// dapatkan nilai ex dari session
		
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr.=" order by kdlokasi";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable table-responsive">
			<table width="100%">
				<tr>
					<td align="left">Nama Lokasi : &nbsp;<input type="text" name="carilokasi" id="carilokasi" size="25" value="<?= $keylokasi ?>" onKeyDown="etrCari(event);"></td>
					<td  align="right">
						<input type="button" value="Filter" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRefresh();" /></td>
				</tr>
			</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			</tr>
			<tr height="20"> 
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft">Kode Lokasi</td>
				<td width="35%" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Lokasi</td>
				<td width="5%%" class="SubHeaderBGAlt thLeft" align="center">Internal</td>
				<td width="30%" class="SubHeaderBGAlt thLeft" align="center">Unit Kerja</td>
				<td width="5%" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['kdlokasi'],$p_editkey)) { // row tidak diupdate
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['kdlokasi']; ?>');" class="link"><?= $row['kdlokasi']; ?></u>
				<? } else { ?>
					<?= $row['kdlokasi']; ?>
				<? } ?>
				</td>
				<td><?= $row['namalokasi']; ?></td>
				<td align="center"><?= $row['internal'] == '1' ? '<img src="images/centang.gif">' : '' ; ?></td>
				<td><?= $row['idunit'] ?></td>
				<td align="center">
				<? if($c_delete) { ?>
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['kdlokasi']; ?>','<?= $row['namalokasi']; ?>');" class="link"><img src="images/deletes.png" width="14" /></u>
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= UI::createTextBox('u_kdlokasi',$row['kdlokasi'],'ControlStyle',8,8,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_lokasi',$row['namalokasi'],'ControlStyle',30,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="u_internal" id="u_internal" value="1" <?= $row['internal']=='1' ? 'checked':''; ?>></td>
				<td align="center">
				<?= UI::createTextBox('u_idsatker',$row['idunit'],'ControlStyle',6,6,true,'onKeyDown="etrUpdate(event);"'); ?>
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_satker&id=u',620,500);">
				</td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['kdlokasi']; ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center"><?= UI::createTextBox('i_kdlokasi','','ControlStyle',8,8,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_lokasi','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>		
				<td align="center"><input type="checkbox" name="i_internal" id="i_internal" value="1"></td>
				<td align="center">
				<?= UI::createTextBox('idsatker','','ControlStyle',6,6,true,'onKeyDown="etrInsert(event);"'); ?>
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_satker',620,500);">
				</td><td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="5"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
            </div>
		<?php require_once('inc_listnav.php'); ?><br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_kdlokasi').focus();
	<? } else { ?>
	document.getElementById('i_kdlokasi').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_kdlokasi,i_lokasi"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_kdlokasi,u_lokasi")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}
function goRefresh(){
document.getElementById("carilokasi").value='';
goSubmit();

}
</script>
</html>
