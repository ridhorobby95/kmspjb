<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Detail PO';
	$p_title = 'Detail PO';
	$p_tbheader = '.: Detail PO :.';
	$p_col = 9;
	$p_tbwidth = 978;
	$p_filelist = Helper::navAddress('list_po.php');
	$p_filerep = Helper::navAddress('repp_po.php');
	$p_id = "detpo";
	
	//var GET
	$nopo=Helper::removeSpecial($_GET['po']);
	
	if($nopo==''){
		header('Location: '.$p_filelist);
		exit();
	}
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'o.idorderpustaka';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list

	$p_sqlstr="select o.idorderpustaka, o.judul,u.namapengusul,o.qtypo,o.qtyttb, o.ststtb, o.supplierdipilih, po.npkpo from pp_orderpustaka o
			   join pp_usul u on o.idusulan=u.idusulan
			   join pp_po po on o.idpo=po.idpo
			   where po.nopo='$nopo'";

	
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(o.judul) like upper('%$keyjudul%') ";
		}

		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi=="batal"){
			$record['idpo']=null;
			$record['qtypo']=null;
			$record['stspo']=0;

			Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idorderpustaka,$r_key);
			
			if($conn->ErrorNo() != 0){
					$errdb = 'Pembatalan PO gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Pembatalan PO berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		}
		else if($r_aksi=="daruratttb"){
			$conn->StartTrans();
			$record['ststtb']=1;
			Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idorderpustaka,$r_key);
			
			$thpagu=Sipus::getPaguAktif($conn);
			Sipus::UpdatePagu($conn,$r_key,$thpagu);			
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
					$errdb = 'Penyelesaian TTP gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
					$sucdb = 'Penyelesaian TTP berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		}
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>

	<style>

</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<? include('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile."&po=$nopo"; ?>">
			<? include_once('_notifikasi.php') ?>
			<table width="100%">
				<tr>
					<td align="center">
						<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar PO</span></a>
					</td>
				</tr>
			</table><br />
			<div class="filterTable">
				<table width="100%">
					<tr>
						<td width="100">Judul Pustaka</td>
						<td colspan=2 width="400">:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $keyjudul ?>" onKeyDown="etrCari(event);"></td>
						<td width="200" align="right">
							<!--a href="javascript:goSubmit()" class="buttonshort"><span class="filter">Filter</span></a-->
							<input type="button" value="Filter" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
						</td>
					</tr>
				</table>
			</div><br />
			<header style="width:978px;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<h1><?= $p_title; ?></h1>
					</div>
				</div>
			</header>
			<table width="100%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

			<!--tr height="30">
				<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
			</tr-->
			<!--tr>
				<td width="100%"  colspan="8" align="center">
					<table border=0 width="100%" cellpadding="4" cellspacing=0>
						<tr>
							<td>
								<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar PO</span></a>
							</td>
							<? if($c_edit) { ?>
							<td width=10>
								<a href="index.php?page=repp_po&nopo=<?= $nopo ?>" target="_blank" class="buttonshort"><span class="print">Cetak PO</span></a>
							</td>
							<? } ?>
							<td>
							<a href="javascript:goClear();goFilter(false);" class="buttonshort"><span class="refresh">
							Refresh</span></a></td>
						
						<td align="right" valign="middle">
							<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
							<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
							<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
							<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
						</td>
						</tr>
						<tr>
						<td width="100">Judul Pustaka</td>
						<td colspan=2 width="400">:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $keyjudul ?>" onKeyDown="etrCari(event);"></td>
						
						<td width="200" align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="filter">Filter</span></a></td>
						</tr>
						
					</table>
				</td>
			</tr-->
				<tr height="20"> 
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft"  style="cursor:pointer;" onClick="PopupMenu('popPaging','o.idusulan:C');">Id Order <?= $p_xsort['idorderpustaka']; ?></td>
				<td width="90%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','o.judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>		
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','u.namapengusul:C');">Nama Pengusul<?= $p_xsort['namapengusul']; ?></td>
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','o.supplierdipilih:C');">Supplier Dipilih<?= $p_xsort['supplierdipilih']; ?></td>
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','o.qtypo:C');">Jumlah PO<?= $p_xsort['qtypo']; ?></td>
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','o.qtyttb:C');">Jumlah TTP<?= $p_xsort['qtyttb']; ?></td>
				<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','po.npkpo:C');">Petugas<?= $p_xsort['npkpo']; ?></td>
				<? if($c_edit and $c_delete) { ?>
				<td width="50" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
				<? } ?>
				<?php
				$i = 0;
				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idorderpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
				
				<td align="center">
				<?= $row['idorderpustaka']; ?></td>
				<td><?= $row['judul']; ?></td>
				<td align="left"><?= $row['namapengusul']; ?></td>
				<td><?= $row['supplierdipilih']; ?></td>
				<td align="center"><?= $row['qtypo']; ?></td>
				<td align="center"><?= $row['qtyttb']; ?></td>
				<td><?= $row['npkpo']; ?></td>
				<? if($c_edit and $c_delete) { ?>
				<td align="center">
				<? if ($row['ststtb']=='0'  and $row['qtyttb']=='0' or $row['qtyttb']==null) { ?>
				<u title="Batalkan PO" style="cursor:pointer" onclick="goBatal('<?= $row['idorderpustaka'] ?>')"><img src="images/delete.png"></u>
				<? } else { ?>
				<img src="images/delete2.png">
				<? } ?>
				<? if ($row['ststtb']=='0'  and $row['qtyttb']!='0') { ?>
				<u title="Selesei TTP [Darurat]" style="cursor:pointer" onclick="goDaruratTTB('<?= $row['idorderpustaka'] ?>')"><img src="images/check_p.gif"></u>
				<? } else { ?>
				<img src="images/uncheck_p.gif">
				<? } ?>
				</td>
				<? } ?>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= ($p_col/2)+3 ; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
			
		</table>
				<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>
	
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carijudul").value='';
	//goSubmit();
}

function goBatal(key){
	var batal=confirm("Apakah Anda yakin akan membatalkan PO ?");
	if(batal){
	document.getElementById("act").value="batal";
	document.getElementById("key").value=key;
	goSubmit();
	}
}

function goDaruratTTB(key){
	var darurat=confirm("Apakah Anda yakin akan menyelesaikan proses TTP ?");
	if(darurat){
	document.getElementById('act').value="daruratttb";
	document.getElementById('key').value=key;
	goSubmit();	
	}
}
</script>

</html>