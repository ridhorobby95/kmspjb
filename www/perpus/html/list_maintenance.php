<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	//$conn->debug=true;
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_historymaintaining';
	$p_window = '[PJB LIBRARY] Daftar Transaksi Perbaikan';
	$p_title = 'Daftar Perbaikan';
	$p_col = 9;
	$p_tbwidth = 700;
	$p_filedetail = Helper::navAddress('trans_maintenance.php');
	$p_id = "list_main";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'tgl';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select to_char(h.tglhistory,'yyyy-mm-dd') as tgl,h.no_referensi,l.namalokasi,k.namakondisi,h.keterangan from $p_dbtable h
			   join lv_lokasi l on h.kdlokasi=l.kdlokasi
			   join lv_kondisi k on h.kdkondisi=k.kdkondisi
			   where no_referensi is not null
			   group by to_char(h.tglhistory,'yyyy-mm-dd'),h.no_referensi,l.namalokasi,k.namakondisi,h.keterangan";

	// pengaturan ex
	if (!empty($_POST))
	{

		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<header style="width:700px;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
					<div class="right">
					  <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data" onClick="goNew('<?= $p_filedetail; ?>')">+</div>
					  <?	#if($c_add) { ?>
					  <div class="addButton" style="float:left; margin:right:10px;" title="refresh" onClick='goRefresh()'><img width="14" src="images/tombol/icq2.png"/></div>
					  <?	#} ?>
					</div>
				</div>
			</header>
			<table width="700" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<!--tr height="40">
				<td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?></td>
			</tr-->
			<!--tr>
			<td class="thLeft" style="border:0 none;" width="100%"  colspan="<?= $p_col ?>" align="center">
			
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<?// if($c_add) { ?>
						<td class="thLeft" style="border:0 none;" width="170" align="center">
						<a href="javascript:goNew('<?//= $p_filedetail; ?>')" class="button"><span class="new">
						Transaksi baru</span></a></td>
						<?// } ?>
						<td class="thLeft" style="border:0 none;" width="170" align="center">
						<a href="javascript:goFilter(false);" class="buttonshort"><span class="refresh">
						Refresh</span></a></td>
						<td class="thLeft" style="border:0 none;" align="right" valign="middle">
						<div style="float:right;">
						<img title="first" id="firstButton" onClick="goFirst(<?//= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?//= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?//= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?//= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
						</div>
						</td>
					</tr>
				</table>
			</td>	
			</tr-->		
			<tr height="20">
				<td width="100" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;" style="cursor:pointer;" onClick="PopupMenu('popPaging','no_referensi:C');">No. Referensi<?= $p_xsort['noreferensi']; ?></td>
				<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="text-align:center;" style="cursor:pointer;" onClick="PopupMenu('popPaging','tglhistory:D');">Tanggal <?= $p_xsort['tglhistory']; ?></td>		
				<td width="140" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','kdkondisi:C');">Kondisi<?= $p_xsort['kdkondisi']; ?></td>		
				<td width="140" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','kdlokasi:C');">Lokasi Dikirim<?= $p_xsort['kdlokasi']; ?></td>		
				<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','keterangan:C');">Keterangan<?= $p_xsort['keterangan']; ?></td>
				<?php
				$i = 0;

				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
				<td align="center"><?= $row['no_referensi']; ?></td>
				<td align="center"><u title="Detail" style="cursor:pointer;color:blue" onclick="goDetail('<?= $p_filedetail.'&key='.$row['no_referensi'] ?>','<?= $row['no_referensi'] ?>')"><?= Helper::tglEng($row['tgl']); ?></u></td>
				<td align="left"><?= $row['namakondisi'] ?></td>
				<td align="left"><?= $row['namalokasi'] ?></td>		
				<td ><?= $row['keterangan']; ?></td>
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="5"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
				
			</tr>
		</table>
		<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="key3" id="key3">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="npwd" id="npwd">

		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;
var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>
</html>