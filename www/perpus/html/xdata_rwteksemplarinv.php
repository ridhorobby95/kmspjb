<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and (us.idunit in ($units) or a.idunit in ($units)) ";
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
			
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplarolah';
	$p_window = '[PJB LIBRARY] Daftar Proses Eksemplar';
	$p_title = 'Daftar Eksemplar Baru';
	$p_tbheader = '.: Daftar Eksemplar Baru :.';
	$p_col = 6;
	$p_tbwidth = 800;
	$p_filedetail = Helper::navAddress('xedit_eksemplarinvs.php');
	$p_filelist = Helper::navAddress('list_inventaris.php');
	$p_id = "prosesinv";
	
	$p_defsort = 'e.noseri';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$p_sqlstr = "select p.*,u.judul,u.authorfirst1,u.authorlast1,u.authorfirst2,u.authorlast2,u.authorfirst3,u.authorlast3,
				k.namaklasifikasi, e.idpustaka, e.kdklasifikasi, e.kdkondisi, e.kdperolehan, e.noseri 
		from $p_dbtable p
		left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
		left join ms_pustaka u on u.idpustaka=e.idpustaka
		left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
		left join pp_orderpustakattb tb on tb.idorderpustakattb=p.idorderpustakattb
		left join pp_orderpustaka op on op.idorderpustaka=tb.idorderpustaka
		left join pp_usul us on op.idusulan=us.idusulan
		left join pp_sumbangan s on s.idsumbangan =  op.idsumbangan
		left join ms_anggota a on (a.idanggota = us.idanggota or a.idanggota = s.idanggota or op.nrp1 = a.idanggota)
		where 1=1 /*tb.npkinventaris='$_SESSION[PERPUS_USER]'*/ $sqlAdminUnit ";

	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(u.judul) like upper('%$keyjudul%') ";
			
			
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['numpage']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</style>

</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<div align="center">
	<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">	
	<div class="filterTable">
		<table width="100%" cellpadding="4" cellspacing="0" border="0">
			<tr>
				<td width="150">Judul</td>
				<td colspan="3">:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $_POST['carijudul'] ?>" onKeyDown="etrCari(event);"></td> 
				<td  align="right"><input type="button" value="Filter" class="ControlStyle" onClick="goFilt()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onClick="goClear();goFilter(false);" /></td>
			</tr>
		</table>
	</div><br />
	<header style="width:800px;margin:0 auto;">
		<div class="inner">
			<div class="left title">
			</div>
		</div>
	</header>
	<table width="<?= $p_tbwidth?>" border="0" cellpadding="4" cellspacing="0" class="GridStyle">
			<tr height="20"> 
			<td class="thLeft" style="border:0 none;" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO INDUK  <?= $p_xsort['e.noseri']; ?></td>
			<td class="thLeft" style="border:0 none;" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','judul:C');">Judul  <?= $p_xsort['judul']; ?></td>
			<td class="thLeft" width="100" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','kdklasifikasi:C');">Klasifikasi  <?= $p_xsort['kdklasifikasi']; ?></td>
			<td class="thLeft" style="border:0 none;" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Pengarang</td>				
			<td class="thLeft" style="border:0 none;" width="110" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;">Edit</td>
			<?php
			$i = 0;
			if($showlist) {
				// mulai iterasi
				while ($row = $rs->FetchRow()) 
				{
					if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
					if ($row['idpustaka'] == '0') $rowstyle = 'YellowBG';
		?>
		<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
			<td><?= $row['noseri']?></td>
			<td><?= $row['judul']; ?></td>
			<td><?= $row['namaklasifikasi']?></td>
			<td align="left"><?php
								echo $row['authorfirst1']. " " .$row['authorlast1']; 
								if ($row['authorfirst2']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo $row['authorfirst2']. " " .$row['authorlast2'];
								}
								if ($row['authorfirst3']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo $row['authorfirst3']. " " .$row['authorlast3'];
								}
							?>
			</td>
			<td align="center">
			<? if ($row['stskirimpengolahan'] != 1){?>
			<u onClick="goPostX('<?= $p_filedetail; ?>','key=<?= $r_key; ?>&rkey=<?= $row['ideksemplar'] ?>');"  title="Detail Eksemplar" class="link"><img src="images/edited.gif"></u>
			<? }else{ ?>
			<img src="images/edited2.gif">
			<? } ?>
			</td>

		</tr>
		<?php
			}
			}
			if ($i==0) {
		?>
		<tr height="20">
			<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
		</tr>
		<?php } ?>
			<tr>
				<td style="background:#015593;color:#fff;" class="footBG" colspan="5" align="right">
					Halaman <?= $p_page ?> / <?= $p_lastpage ?>
				</td>
			</tr>
	</table>
		<?php require_once('inc_listnav.php'); ?><br>

	<input type="hidden" name="numpage" id="numpage" value="<?= $p_page ?>">
	<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
	<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
	<input type="hidden" name="key" id="key">
	<input type="hidden" name="key3" id="key3">
	<input type="hidden" name="act" id="act">


	<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
		Filter Criteria <br>
		<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
	</div>



	<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
	<table width=100  class="menu-body">
		<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
			<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
		</tr>
		<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
			<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
		</tr>
		<tr>
			<td class="separator"><div class="separator-line"></div></td>
		</tr>
		<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
			<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
		</tr>
		<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
			<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
		</tr>
	</table>
	</div>
	</form>
	</div>



</body>
<script type="text/javascript" src="scripts/foreditx.js"></script>
<script type="text/javascript">


var phpself = "<?= $i_phpfile; ?>";

function etrCari(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		sent = "key=<?= $r_key; ?>&carijudul="+$("#carijudul").val();
		goPostX('<?= $i_phpfile ?>',sent);
	}
}

function goFilt(){
	sent = "key=<?= $r_key; ?>&carijudul="+$("#carijudul").val();
	goPostX('<?= $i_phpfile ?>',sent);
}

function goClear(){
	sent = "key=<?= $r_key; ?>";
	goPostX('<?= $i_phpfile ?>',sent);
}


$(function() {
	$("#litacreate").click(function() {
		goPostX('<?= Helper::navAddress('data_inventaris')?>', 'key=<?= $r_key; ?>');
	});
});
</script>

</html>