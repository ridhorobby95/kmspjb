<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Data Usulan Pustaka';
	$p_title = 'Data Usulan Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_usulanpustaka.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		$sql =$conn->GetRow("select a.idanggota, a.namaanggota, a.kdjenisanggota, j.namajenisanggota, coalesce((select sisapagu from pp_pagulh where idanggota=a.idanggota 
		and periodeakad='".date("Y")."'),0) as sisapagu from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota where a.idanggota='$r_key2' order by a.idanggota");

		if($r_aksi == 'simpan' and $c_edit) {
			
			
			$record = array();
			$record = Helper::cStrFill($_POST);
			$max=Helper::removeSpecial($_POST['paguusulan']);
			if ($sql['kdjenisanggota']=='D'){
				
				
			if ($sql['sisapagu']==0){
				$errdb = 'Jumlah pagu masih kosong.';	
				Helper::setFlashData('errdb', $errdb);
				} else{
			if ($max>$sql['sisapagu']){
				$errdb = 'Pagu usulan melebihi batas maksimal.';	
				Helper::setFlashData('errdb', $errdb);
				$row = $_POST; // direstore
				echo "sisa :".$sql['sisapagu'];
				
				}else{
					if($r_key == '') { // insert record	
					$record['idanggota']=$r_key2;
					$record['namapengusul']=$sql['namaanggota'];
					$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
					$record['statususulan']=0;
					$record['ispengadaan']=0;
					Helper::Identitas($record);
					Sipus::InsertBiasa($conn,$record,$p_dbtable);
					}
					else { // update record
						$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
						$record['namapengusul']=$sql['namaanggota'];
						Helper::Identitas($record);
						Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
					}	
				}
		}}else{
				
				if($r_key == '') { // insert record	
					$record['idanggota']=$r_key2;
					$record['namapengusul']=$sql['namaanggota'];
					$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
					$record['statususulan']=0;
					$record['ispengadaan']=0;
					Helper::Identitas($record);
					Sipus::InsertBiasa($conn,$record,$p_dbtable);
					}
					else { // update record
						$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
						$record['namapengusul']=$sql['namaanggota'];
						Helper::Identitas($record);
						Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
					}
		}
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
				}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
				$r_key = $record['idusulan'];
				}
				else {
				$row = $_POST; // direstore
				$p_errdb = true;
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idusulan,$r_key);
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		else if ($r_aksi == 'cari' and $c_edit){
			$record = Helper::cStrFill($_POST);
			$sql = $conn->GetRow("select a.idanggota, a.namaanggota, a.kdjenisanggota, j.namajenisanggota, coalesce((select sisapagu from pp_pagulh where idanggota=a.idanggota and periodeakad='".date("Y")."'),0) as sisapagu 
			from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota where a.idanggota='$record[txtcari]' order by a.idanggota");
						
			if (!empty($sql['idanggota'])){
				$rset = array();
				$r_key2 = $sql['idanggota'];
				$rset['idanggota'] = $r_key2;
				$rset['namaanggota'] = $sql['namaanggota'];
				$rset['sisapagu'] = $sql['sisapagu'];
				//print_r($rset);
			}else{			

				$sucdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
	}
	
	if ($r_key !='') {
		$p_sqlstr = "select p.*, namaanggota from $p_dbtable p 
					left join ms_anggota a on a.idanggota=p.idanggota
					where idusulan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);		
	}
	
	//echo "r_key ".$r_key;
	//echo "r_key2 ".$r_key2;
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage()">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Usulan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key != ''  and $r_key2 !='') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table>
<br>

<? if ($r_key == '' and $r_key2 ==''){ ?>
	<table width="600" border=0 align="center" cellpadding="4" cellspacing=0 class="GridStyle">
      <tr>
        <td colspan="3" class="PageTitle" align="center">Masukkan Id Anggota</th>
      </tr>
      <tr height="35">
        <td valign="center">Masukkan Id Anggota : &nbsp;<input type="text" name="txtcari" id="txtcari" size="50" onKeyDown="etrCari(event);" class="ControlStyle" value="<?= $r_key2 ?>">&nbsp;&nbsp;
			<input type="button" name="btntrans" id="btntrans" class="buttonSmall" value="Cari" style="cursor:pointer;height:25;width:80" onClick="goCari()">
		</td>
      </tr>
</table>
<br>
<? } ?><?php include_once('_notifikasi.php'); ?>
<? if ($r_key != '' or $r_key2 !=''){ ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr height=25> 
		<td class="LeftColumnBG" width="180">Id Anggota *</td>
		<td class="LeftColumnBG" width="370"><b><i><?= empty($sql['idanggota']) ? $row['idanggota'] : $sql['idanggota']; ?></i>&nbsp;&nbsp;[ <?= $sql['namajenisanggota'] ?> ]</b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG" height="30">Nama Pengusul</td>
		<td class="LeftColumnBG"><b><i><?= empty($sql['namaanggota']) ? $row['namaanggota'] : $sql['namaanggota']; ?></i></b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tanggal Usulan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglusulan',Helper::formatDate($row['tglusulan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglusulan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]<? } ?>
		</td>
	</tr>
	
	<tr height=25> 
		<td class="LeftColumnBG">Judul Pustaka</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,60,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Harga</td>
		<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',20,15,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang</td>
		<td class="RightColumnBG"><?= UI::createTextBox('author',$row['author'],'ControlStyle',100,50,$c_edit); 
		 
		 ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
	</tr>
	<? if($sql['kdjenisanggota']=='D') { ?>
	<tr height=25> 
		<td class="LeftColumnBG">Pagu Usulan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('paguusulan',$row['paguusulan'],'ControlStyle',14,15,$c_edit); ?>
		&nbsp; <?= empty($sql['sisapagu']) ? '<strong>Sisa Pagu : Rp. 0</strong>' : '<strong>Sisa Pagu : Rp. </strong>'.$sql['sisapagu']; ?>
		
		</td>
	</tr>
	<? } ?>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table><br>
<? } ?>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

</form>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function saveData() {
	if(cfHighlight("idusulan,tglusulan"))
		goSave();
}
function etrCari(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goCari();
}
function goCari(){
	if(cfHighlight("txtcari")){
		$("#act").val('cari');
		goSubmit();
	}		
}

function initPage(){
	var a=document.getElementById("key").value;
	var b=document.getElementById("key2").value;
	
	if(b==''){
		document.getElementById("txtcari").focus();
	}else {
		document.getElementById("tglusulan").focus();
	}
}
</script>
</html>