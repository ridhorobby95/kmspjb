<?php
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$c_validasi = $a_auth['canvalidasi'];

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and u.idunit in ($units) ";	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Data Usulan Unit';
	$p_title = '.: Data Usulan Unit :.';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('xlist_unitusul.php');
	
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		
		if($r_aksi == 'simpan' and $c_edit) {	
			
			$record = array();
			//$record = Helper::cStrNull($_POST);
			$record = Helper::cStrFill($_POST);
			
			$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
			$record['idanggota']=Helper::removeSpecial($_POST['idanggota']);
			$record['idunit']=Helper::removeSpecial($_POST['idunit']);
			Helper::Identitas($record);
			if ($r_key == ''){
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
	
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}	
			}
			else { // update record
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['idanggota']=Helper::removeSpecial($_POST['idanggota']);
				$record['idunit']=Helper::removeSpecial($_POST['idunit']);
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
				
				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}	
			}
			
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					//$r_key = $conn->GetOne("select lastval()");
					$r_key = $conn->GetOne("select max(idusulan) from $p_dbtable");
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		else if($r_aksi == 'delete' and $c_delete) {
			//print_r($_POST);
			$p_delsql = "delete from $p_dbtable where idusulan = '$r_key'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
		}	
	}
		
	if ($r_key !='') {
		$p_sqlstr = "select u.*,a.namaanggota from pp_usul u
					 left join ms_anggota a on u.idanggota=a.idanggota
					 where idusulan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);
	}
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/combo.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="loadauthor()">
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="javascript:goBack()" class="button"><span class="list">Daftar Usulan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key) { ?>
	<td align="center">
		<a href="javascript:gotes();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? }  ?>
	</tr>
</table>
<br>
<? include_once('_notifikasi.php'); ?>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="7" class="GridStyle">
	<tr>
		<th colspan="2" class="SubHeaderBGAlt"><?= $p_title; ?></th>
	</tr>
	<tr>
		<td class="LeftColumnBG">Unit Kerja *</td>
		<td><?= UI::createTextBox('namapengusul',$row['namapengusul'],'ControlRead',50,50,true,'readonly'); ?>
		<img src="images/popup.png" style="cursor:pointer;" title="Pilih Unit kerja" onClick="showSatKerPU();">
		<input type="hidden" name="idunit" id="idunit" value="<?= $row['idunit'] ?>"></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Penanggung Jawab *</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('namaanggota',$row['namaanggota'],'ControlRead',50,50,$c_edit); ?>
		<input type="hidden" name="idanggota" id="idanggota" value="<?= $row['idanggota'] ?>">
		<input type="hidden" name="namaanggota2" id="namaanggota2">
		<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota"  onClick="popup('index.php?page=pop_anggota&code=u',630,500);">
				
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Judul *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,63,$c_edit); ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Nama Pengarang 1 *</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst1',$row['authorfirst1'],'ControlStyle',100,30,$c_edit); ?>
		<?= UI::createTextBox('authorlast1',$row['authorlast1'],'ControlStyle',100,30,$c_edit); ?>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Nama Pengarang 2</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,30,$c_edit); ?>
		<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,30,$c_edit); ?>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Nama Pengarang 3</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,30,$c_edit); ?>
		<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,30,$c_edit); ?>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,30,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">
			<? } ?>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit,'onKeyDown="onlyNumber(event,this,false,true)"'); ?>&nbsp;&nbsp;[ Format : yyyy ]</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Harga *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('hargausulan',$row['hargausulan'],'ControlStyle',30,30,$c_edit,'onKeyDown="onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Edisi</td>
		<td class="RightColumnBG"><?= UI::createTextBox('edisi',$row['edisi'],'ControlStyle',20,20,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">ISBN</td>
		<td class="RightColumnBG"><?= UI::createTextBox('isbn',$row['isbn'],'ControlStyle',30,30,$c_edit); ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',3,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tgl. Usulan</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('tglusulan',empty($row['tglusulan']) ? date('d-m-Y') : Helper::formatDate($row['tglusulan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="e_tglusulan" style="cursor:pointer;" title="Pilih tanggal daftar">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglusulan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "e_tglusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>	
</table>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
</form>
</div>
</body>
<script type="text/javascript" src="scripts/foredit.js"></script>

<script language="javascript">
var phpself = "<?= $i_phpfile; ?>";


function showSatKerPU() {
	win = window.open("<?= Helper::navAddress('pop_satker.php'); ?>&nama=namapengusul&id=idunit","popup_satker","width=450,height=400,scrollbars=1");
	win.focus();
}

function deleteSatKer() {
	document.getElementById("namapengusul").value = "";
	document.getElementById("idunit").value = "";
}

function saveData() {
	if(cfHighlight("namapengusul,idanggota,namaanggota,judul,authorfirst1,hargausulan")){
		$("#act").val("simpan");
		$("#" + xtdid).divpost({page: phpself, sent: $("#perpusform").serializeArray()});
	}
}

function gotes() {
	$("#act").val("delete");
	$("#" + xtdid).divpost({page: phpself, sent: $("#perpusform").serializeArray()});
}

function goBack() {
	$("#" + xtdid).divpost({page: "<?= $p_filelist?>", sent: ''});
}

function clearAuthor(urut) {
	document.getElementById("idauthor"+urut).value = "";
	document.getElementById("authorfirst"+urut).value = "";
	document.getElementById("authorlast"+urut).value = "";
}

</script>

</html>
