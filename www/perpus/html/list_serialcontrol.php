<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$rkey = Helper::removeSpecial($_GET['key']);

	if($rkey=='')
		header("location: index.php?page=home");
	
	// definisi variabel halaman
	$p_dbtable = 'pp_serialcontrol';
	$p_window = '[PJB LIBRARY] Data Serial Kontrol';
	$p_title = 'Data Serial Kontrol';
	$p_tbwidth = 550;
	
	$row = $conn->GetRow("select judul,jumlah from ms_pustaka p left join lv_periode v on p.idperiode=v.idperiode where idpustaka='$rkey'");
	
	if(!$row)
		header("location: index.php?page=home");
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$conn->StartTrans();
			$record = Helper::cStrFill($_POST);//,array('userid','nama','usertype','hints','statususer'));
			
			$record['idserial'] = Sipus::GetLast($conn,$p_dbtable,idserial);
			$record['tglmulai'] = Helper::formatDate($_POST['tglmulai']);
			$record['tglberakhir'] = Helper::formatDate($_POST['tglberakhir']);
			$record['idpustaka'] = $rkey;
			Helper::Identitas($record);
			Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			$eks = Helper::removeSpecial($_POST['total_eksemplar']);
			$now = strtotime($record['tglmulai']);
			
			for($i=0;$i<$eks;$i++){
				
				$recdet['idserialitem'] = Sipus::GetLast($conn,pp_serialitem,idserialitem);
				$recdet['tglperkiraan'] = date("Y-m-d",$now) ;
				$recdet['seq_number'] = $i+1;
				$recdet['idserial'] = $record['idserial'];
				
				Sipus::InsertBiasa($conn,$recdet,pp_serialitem);
				$now = strtotime($recdet['tglperkiraan']);
				$now = $now + ($row['jumlah']*86400);
			}
			$conn->Completetrans();
			
			if($conn->ErrorNo() != 0){
			$errdb = 'Penyimpanan data gagal.';	
			Helper::setFlashData('errdb', $errdb);
			}
			else {
			$sucdb = 'Penyimpanan data berhasil.';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect(); 
			}
						
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					$r_key = $record['idserial'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}

		
	}
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onUnload="window.opener.location.reload(true);">
<div id="wrapper" style="width:auto;margin:0;padding:0;">
<div class="SideItem" id="SideItem" style="width:auto;margin-left:10px;">
<div align="center">

<table width="100">
	<tr>
	<?  if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? }?>
	</tr>
</table><br><?php include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<!--img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" /-->
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
<form name="perpusform" id="perpusform" method="post"  enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">
	<tr height="25">
		<td width="35%" class="LeftColumnBG">Judul Pustaka</td>
		<td width="35%" class="RightColumnBG"><b><?= $row['judul'] ?></b></td>
	<tr> 
		<td width="35%" class="LeftColumnBG">Tanggal Mulai *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglmulai',Helper::formatDate($rows['tglmulai']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglemulai" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglmulai",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglemulai",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>	
	<tr> 
		<td class="LeftColumnBG">Tanggal Berakhir</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglberakhir',Helper::formatDate($row['tglberakhir']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleakhir" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglberakhir",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleakhir",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]
		<? } ?>
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Periode *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('periode',$row['periode'],'ControlStyle',50,50,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Total Eksemplar *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('total_eksemplar',1,'ControlStyle',10,10,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('keterangan',$row['keterangan'],'ControlStyle',255,50,$c_edit); ?></td>
	</tr>
	
</table>



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("tglmulai,total_eksemplar,periode")){
		goSave();
		window.opener.refresh;
	}
}

</script>
</html>