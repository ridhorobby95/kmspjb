<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);
	

	// definisi variabel halaman
	$p_dbtable = 'pp_usulan';
	$p_window = '[PJB LIBRARY] Data Usulan Pustaka';
	$p_title = 'Data Usulan Pustaka';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_usulan.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			
			
			$record = Helper::cStrFill($_POST);//,array('userid','nama','usertype','hints','statususer'));
			
			if($r_key == '') { // insert record	
			$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
			$record['tglrealisasi']=Helper::formatDate($_POST['tglrealisasi']);
			$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['t_updatetime']=date('d-m-Y H:i:s');
			$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
			
				$col = $conn->Execute("select * from $p_dbtable where 1=-1");
				$p_svsql = $conn->GetInsertSQL($col,$record);
				$conn->Execute($p_svsql);	
				
				if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
			}
			else { // update record
				$col = $conn->Execute("select * from $p_dbtable where idusulan = '$r_key'");
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['tglrealisasi']=Helper::formatDate($_POST['tglrealisasi']);
				$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
				$record['t_updatetime']=date('d-m-Y H:i:s');
				$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
				$p_svsql = $conn->GetUpdateSQL($col,$record,true);
				$conn->Execute($p_svsql);
				
				if($conn->ErrorNo() != 0){
				$errdb = 'Update data gagal.';
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
			}
			
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
					$r_key = $record['idusulan'];
			}
			else {
				$row = $_POST; // direstore
				$p_errdb = true;
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where idusulan = '$r_key'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		
	}
	if(!$p_errdb) {
		if ($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where idusulan = '$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		
		if($row['statususulan']==0){
		$op1="<input type=\"radio\" name=\"statususulan\" id=\"statususulan\" Value=\"0\" checked>";
		$op2="<input type=\"radio\" name=\"statususulan\" id=\"statususulan\" value=\"1\">";
		} else {
		$op1="<input type=\"radio\" name=\"statususulan\" id=\"statususulan\" Value=\"0\">";
		$op2="<input type=\"radio\" name=\"statususulan\" id=\"statususulan\" value=\"1\" checked>";
		}	
		
		}
	}
	$sqlanggota="select idanggota, namaanggota, (select sisapagu from pp_pagulh where idanggota=ms_anggota.idanggota and periodeakad='".date("Y")."') as sisapagu from ms_anggota order by idanggota";
	$rsanggota = $conn->Execute($sqlanggota);
	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<table width="<?= $p_tbwidth ?>">
	<tr height="40">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Usulan Pustaka </span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan Data</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset Isian</span></a>
	</td>
	<? } if($c_delete and $r_key) { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus Data</span></a>
	</td>
	<? } ?>
	</tr>
</table><br><?php include_once('_notifikasi.php'); ?>
<br>
<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr> 
		<td class="HeaderBG" colspan="2" align="center">Data Usulan</td>
	</tr>
	
	<tr> 
		<td class="LeftColumnBG" width="180">Id Anggota *</td>
		<td class="RightColumnBG">
		<input type="text" name="idanggota" id="idanggota" class="ControlStyle" size="30" maxlength="50" value='<?= $row['idanggota'] ?>'>
       	</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Nama Pengusul</td>
		<td class="RightColumnBG">
		
		<input type="text" id="namapengusul" class="ControlStyle" size="50" maxlength="50">
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tanggal Usulan </td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglusulan',Helper::formatDate($row['tglusulan']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglusulan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>
		[ Format :dd-mm-yyyy ]
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tanggal Realisasi</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglrealisasi',Helper::formatDate($row['tglrealisasi']),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tglerealisasi" style="cursor:pointer;" title="Pilih tanggal perolehan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglrealisasi",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tglerealisasi",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		<? } ?>[ Format :dd-mm-yyyy ]
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Judul Pustaka</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,60,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Harga</td>
		<td class="RightColumnBG"><?= UI::createTextBox('harga',$row['harga'],'ControlStyle',20,15,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
		
	</tr>
	<tr>
	<td class="LeftColumnBG">Pengarang</td>
		<td class="RightColumnBG"><?= UI::createTextBox('pengarang',$row['pengarang'],'ControlStyle',100,50,$c_edit); 
		 
		 ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,5,$c_edit); ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Pagu Usulan</td>
		<td class="RightColumnBG"><?= UI::createTextBox('paguusulan',$row['paguusulan'],'ControlStyle',14,15,$c_edit); ?>
		&nbsp; <?= $row['sisapagu'] ?>
		
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Status Usulan</td>
		<td class="RightColumnBG">
		<? if($r_key!=''){ 
		echo $op1." Menunggu ".$op2." Terealisasi";
		} else { ?>
		<input name="statususulan" type="radio" value="0" id="statususulan" checked />  Menunggu
		<input name="statususulan" type="radio" value="1" id="statususulan" />   Terealisasi
		<? } ?>
		
		</td>
	</tr>
	<tr> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table>

<div id="div_usulan" style="display:none;">
<?	while($row = $rsanggota->FetchRow()) { ?>
	<p id="<?= $row['namaanggota'] ?>"><?= $row['idanggota'] ?></p>
<?	} ?>
</div>



<div align="left" id="div_autocomplete" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;">
	<table bgcolor="#FFFFFF" id="tab_autocomplete" cellpadding="3" cellspacing="0"></table>
</div>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

</form>
</div>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xauto.js"></script>
<script language="javascript">

function saveData() {
	if(cfHighlight("idusulan"))
		goSave();
}



$(function() {
	$("#idanggota").xauto({targetid: "namapengusul", srcdivid: "div_usulan", imgchkid: "imgusulan"});

});

</script>
</html>