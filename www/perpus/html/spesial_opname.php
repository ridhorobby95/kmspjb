<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_opname';
	$p_window = '[PJB LIBRARY] Sirkulasi Opname Pustaka [Spesial]';
	$p_title = 'Sirkulasi Opname Pustaka [Spesial]';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_opname.php');
	$p_cetak = Helper::navAddress('rep_tempopnames.php');
	$p_fileopnamed = Helper::navAddress('list_opnamed.php');
	if($r_key==''){
		header('Location: '.$p_filelist);
		exit();
	}
	
	$p_title .= " ".$conn->GetOne("select namaopname from pp_opname where idopname = '$r_key'");
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
			$noseri=Helper::removeSpecial($_POST['keyid']);
			
			$kondisi=Helper::removeSpecial($_POST['kdkondisi']);
			
		if ($r_aksi=='opname' and $c_edit) {
			$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where lower(noseri)= lower('$noseri') ");
			$ideks=$p_eks['ideksemplar'];

			$sqleks=$conn->GetRow("select e.ideksemplar
					      from pp_eksemplar e
					      left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
					      where e.ideksemplar=$ideks $sqlAdminUnit");
			if(!$sqleks){
				$errdb = 'Data Eksemplar pustaka tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);
			}else {
				$record=array();
				$sqlcheck=$conn->GetRow("select idopname from pp_detailopname where idopname=$r_key and ideksemplar=$ideks");
				$sqlcheck2=$conn->GetRow("select kdkondisi from pp_eksemplar where ideksemplar=$ideks");
				
				$record['idopname']=$r_key;
				$record['kdkondisi']=$kondisi;
				$record['tglopname']=date("Y-m-d");
				Helper::Identitas($record);
				
				$rec=array();
				$rec['idopname']=$r_key;
				$rec['ideksemplar']=$ideks;
				$rec['tglopname']=date("Y-m-d");
				$rec['kondisiopname']=$kondisi;
				Helper::Identitas($rec);
				$conn->StartTrans();
				if(!$sqlcheck){
					$rec['kondisiawal']=$sqlcheck2['kdkondisi'];
					Sipus::InsertBiasa($conn,$rec,pp_detailopname);
					
				}else{
					$col = $conn->Execute("select * from pp_detailopname where idopname = $r_key and ideksemplar=$ideks");
					$sql = $conn->GetUpdateSQL($col,$rec);
					if($sql !='')
						$conn->Execute($sql);
				}			
				
				Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$ideks);
			
				$conn->CompleteTrans();
				if($conn->ErrorNo() != 0){
					$errdb = 'Opname pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Opname Pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
			}
			
		}
	}	
	
		if($r_key!='') {
			$sql = "select p.*,m.judul, k.namakondisi as namakondisiawal, c.namakondisi as namakondisiopname, e.noseri, l.namalokasi
				from pp_detailopname p
				join lv_kondisi k on p.kondisiawal=k.kdkondisi
				join lv_kondisi c on p.kondisiopname=c.kdkondisi
				join pp_eksemplar e on p.ideksemplar=e.ideksemplar
				join ms_pustaka m on e.idpustaka=m.idpustaka
				join lv_lokasi l on e.kdlokasi=l.kdlokasi
				where p.idopname=$r_key $sqlAdminUnit order by p.t_updatetime desc";
			
			$SQLOpname = $conn->PageExecute($sql,15,1);
			$rsc=$conn->Execute($sql)->RowCount();
		}
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',($kondisi=='' ? "V" : $kondisi),false,false,0,'id="kdkondisi" class="ControlStyle" style="width:150"');
		
		$jml_opname = $conn->GetOne("select count(*) from pp_detailopname where idopname = $r_key ");
		$jml_nonopname = $conn->GetOne("select count(*) from pp_eksemplar where ideksemplar not in (select ideksemplar from pp_detailopname where idopname = $r_key) ");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0"  onload="document.getElementById('keyid').focus()">
<div id="main_content">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
<table width="50%" align="center">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Opname </span></a>
	</td>
	<td align="center">
		<a href="javascript:goRefreshOp();" class="button"><span class="refresh">Refresh</span></a>
	</td>
	<td align="center">
		<a href="<?= $p_cetak."&id=".$r_key ?>" target="_blank" class="button"><span class="print">Cetak Eksemplar</span></a>
	</td>
	<? } ?>
	</tr>
</table><br/><? include_once('_notifikasi.php'); ?>
<form name="perpusform" id="perpusform" method="post" enctype="multipart/form-data">
<div class="filterTable">
<table width="100%">
            <tr>
              <td><strong>Kondisi Opname</strong></td>
              <td>:</td>
              <td><?= $l_kondisi ?></td>
              <td>&nbsp;</td>
              <td><strong>Masukkan Nomor Seri</strong></td>
              <td>:</td>
              <td><input type="text" name="keyid" id="keyid" size=25 maxlength=20 onKeyDown="etrOp(event);"></td>
              <? if($c_edit) { ?>
		<td><input type="button" name="btneks" id="btneks" value="Mulai Opname" class="buttonSmall" style="cursor:pointer;height:25;width:120" onClick="goOpname()"></td>
		<? } ?>
            </tr>
          </table>
</div>
<br/>
<header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
		<? if ($r_key!='') { ?>
		<div class="right">
			<div class="addButton" style="float:left; margin:right:10px;"  title="Daftar Stock Opname" onClick="goHalDetail('<?= $p_fileopnamed ?>','<?= $r_key ?>')"><img src="images/edited.gif" width=15 height=15></div>
		</div>
		<? } ?>
          </div>
        </header>
		<table width="100%" cellpadding=0 class="GridStyle">
		
		<tr>
			<th width="100" align="center" class="SubHeaderBGAlt"><b>NO INDUK</b></th>
			<th width="380" align="center" class="SubHeaderBGAlt"><b>Judul Pustaka</b></th>
			<th width="150" align="center" class="SubHeaderBGAlt"><b>Kondisi Awal</b></th>
			<th width="210" align="center"  class="SubHeaderBGAlt"><b>Kondisi Opname</b></th>
			<th width="210" align="center"  class="SubHeaderBGAlt"><b>Lokasi</b></th>
		</tr>
		<? 
		$i=0;

			while ($row = $SQLOpname->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idopname'] == '0') $rowstyle = 'YellowBG';
			?>
			
		<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
			<td align="left">&nbsp;<?= $row['noseri']; ?></td>
			<td align="left">&nbsp;<?= $row['judul']; ?></td>
			<td align="left">&nbsp;<?= $row['namakondisiawal']; ?></td>
			<td align="left">&nbsp;<?= $row['namakondisiopname']; ?></td>
			<td align="left">&nbsp;<?= $row['namalokasi']; ?></td>
			<td align="center"></td>
		</tr>
		<?php
			} 
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="4">Data tidak ditemukan.</td>
			</tr>
			<? } ?>
			<tr> 
				<td class="PagerBG footBG" align="left" colspan="6" style="padding:5px;">
					<div>Total Stock Opname = <?=Helper::formatNumber($jml_opname);?> eksemplar</div>
					<div>Total Belum Stock Opname = <?=Helper::formatNumber($jml_nonopname);?> eksemplar</div>
					
				</td>
			</tr>
			<tr>
				<td colspan="5">
				<br>
				<table align="center" cellspacing="3" cellpadding="2" width="370" border=0 bgcolor="#f1f2d1">
					<tr>
						<td width=270><u><b>Keterangan : </b></u></td>
					</tr>
					<tr>
						<td>
						Maksimal daftar pustaka opname diatas adalah 15 Eksemplar
						</td>
					</tr>
				</table>
				</td>
			</tr>
			</table>
			



<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
<input type="text" name="test" id="test" style="visibility:hidden">
<input type="hidden" name="keykon" id="keykon">
</form>
</div>
</div>

</body>

<script language="javascript">
function etrOp(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btneks").click();
}

function goRefreshOp(id) {
	document.getElementById("act").value='';
	document.getElementById("kdkondisi").value='V';
	goSubmit();
}


function goOpname(id){
	if(cfHighlight("keyid")) {
	document.getElementById("act").value='opname';
	goSubmit();
	}else
	document.getElementById("keyid").focus();
}
</script>
</html>

	
	