<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// koneksi ke sdm
	$conns = Factory::getConnSdm();
		
	// variabel request
	$r_fakultas = helper::removeSpecial($_REQUEST['kdfakultas']);
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Pemakaian Dana Label Hijau';
	
	$p_namafile = 'rekap_pdlh'.$r_fakultas.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select idanggota,count(*) as jumlah,sum(harga) as harga,namaanggota,idunit from (
			select r.*,e.noseri,e.harga, p.judul,a.namaanggota,a.idunit from pp_transaksi r 
			join pp_eksemplar e on r.ideksemplar=e.ideksemplar 
			join ms_pustaka p on e.idpustaka = p.idpustaka 
			join ms_anggota a on r.idanggota=a.idanggota
			where  r.tgltransaksi between '$r_tgl1' and '$r_tgl2' 
			and r.tglpengembalian is null and e.kdklasifikasi='LH'
			) a
			group by idanggota,namaanggota,idunit order by idanggota";	
			
	$rs = $conn->Execute($sql);
	while($row = $rs->FetchRow()){
		$ArId[$row['idunit']][] = $row['idanggota'];
		$ArNama[$row['idunit']][] = $row['namaanggota'];
		$ArJum[$row['idunit']][] = $row['jumlah'];
		$ArHar[$row['idunit']][] = $row['harga'];
		$ArUnit[$row['idunit']][] = $row['idunit'];
	}
	
	$sql2 = "select idsatker,namasatker,parentsatker from ms_satker where parentsatker=(select idsatker from ms_satker where kodefakultas='$r_fakultas')";
	$rs2 = $conns->Execute($sql2);
	

	while($rows = $rs2->FetchRow()){
		$ArIS[] = $rows['parentsatker'];
		$ArPS[] = $rows['idsatker'];
		$ArNS[] = $rows['namasatker'];
	}
	
	$GetFak = $conn->GetOne("select namafakultas from lv_fakultas where kdfakultas='$r_fakultas'");
	//print_r($ArIS);
	
	//$rsc=$rs->RowCount();
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Rekap Pemakaian Dana Label Hijau</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> FAKULTAS</td>
	<td>: <?= strtoupper($GetFak) ?>
  </tr>
  <tr>
	<td> PERIODE</td>
	<td>: <?= strtoupper(Helper::tglEng($r_tgl1)) ?> s/d <?= strtoupper(Helper::tglEng($r_tgl2)) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>NPK</strong></th>
    <th width="200" align="center"><strong>Nama</strong></th>
    <th width="80" align="center"><strong>Kode Unit</strong></th>
	<th width="90" align="center"><strong>Eksemplar</strong></th>
	<th width="100" align="center"><strong>Total Dana</th>
  </tr>
  <?php
	$no=0;
	$harga = 0;
	$eks = 0;
	//echo count($ArPS)."<br>";
	for($s=0;$s<count($ArPS);$s++){ 
		
		// echo $ArPS[$s]."<br>";
		// echo count($ArId[$ArPS[$s]])."<br>";
		for($i=0;$i<count($ArId[$ArPS[$s]]);$i++)
		{  $no++;?>
			<tr height=25>
				<td align="center"><?= $no ?></td>
				<td align="left"><?= $ArId[$ArPS[$s]][$i] ?></td>
				<td align="left"><?= $ArNama[$ArPS[$s]][$i] ?></td>
				<td align="center"><?= $ArUnit[$ArPS[$s]][$i] ?></td>
				<td align="center"><?= $ArJum[$ArPS[$s]][$i] ?></td>
				<td align="right"><?= Helper::formatNumber($ArHar[$ArPS[$s]][$i],'0',true,true) ?>&nbsp;</td>	
			</tr>
		
	   <? $eks += $ArJum[$ArPS[$s]][$i];$harga += $ArHar[$ArPS[$s]][$i]; }
	}?>
	<? if($no==1) { ?>
	<tr height=25>
		<td colspan = 6 align="center"><b>Data tidak ditemukan</b></td>
	</tr>
	<? } else { ?>
	<tr height=25>
		<td colspan=4><b>Jumlah = <?= $no ?></b></td>
		<td align="center"><b><?= $eks ?></b></td>
		<td align="right"><b><?= Helper::formatNumber($harga,'0',true,true) ?></b>&nbsp;</td>
	<? } ?>	
</table>


</div>
</body>
</html>