<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_del = $a_auth['candelete'];
		
	// require tambahan
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Sirkulasi Penimjaman Unit';
	$p_title = 'Sirkulasi Peminjaman Unit';
	$p_col = 9;
	$p_tbwidth = 1120;
	$p_id = "sirkulasi_lab";
	

	
	// sql untuk mendapatkan isi list
	//header('Location: '.$p_filelist);
	$fil=Helper::removeSpecial($_REQUEST['kd']);
	
	$p_sqlstr="select * from v_trans_list where kdlokasi='$fil'";

	if(isset($_POST['btnlab'])){
		$lokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$idlogin=Helper::removeSpecial($_POST['idanggotalab']);
		$isLab=$conn->GetRow("select idanggota from ms_anggota where idanggota='$idlogin' and kdjenisanggota not in('A','P')");
		if ($isLab){
			$_SESSION['idlab']=$idlogin;
			$_SESSION['lab']=$lokasi;
			Helper::redirect();
		}else {
			$errdb = 'Data anggota tidak ditemukan.';
			Helper::setFlashData('errdb', $errdb); 	
			//Helper::redirect();
		}
	
	}
	
	if($_SESSION['idlab']!=''){
		$getLab=$conn->GetRow("select * from lv_lokasi where kdlokasi='".$_SESSION['lab']."'");	
		$anggota=$conn->GetRow("select a.namaanggota,a.idanggota,j.namajenisanggota from ms_anggota a
								join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
								where idanggota='".$_SESSION['idlab']."'");
		
		$transnow=$conn->Execute("select *,a.namaanggota from v_trans_list v
								  join ms_anggota a on v.idanggota=a.idanggota
								  where kdlokasi='".$_SESSION['lab']."' and tglpengembalian is null order by tgltransaksi desc");
		$transe=$conn->Execute("select *,a.namaanggota from v_trans_list v
								join ms_anggota a on v.idanggota=a.idanggota
								where kdlokasi='".$_SESSION['lab']."' order by tglpengembalian desc,tgltransaksi limit 15");
	}
	
	
	// pengaturan ex
	if (!empty($_POST))
	{	

		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2 = Helper::removeSpecial($_POST['key2']);
		$r_seri = Helper::removeSpecial($_POST['txtpustaka']);

			
		
		
		
		if($r_aksi=="pinjam" and $c_add ){
			$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where noseri='$r_seri'");
			$r_eks=$p_eks['ideksemplar'];
			$p_angg=$conn->GetRow("select namaanggota,tglselesaiskors,statusanggota from ms_anggota where idanggota='".$_SESSION['idlab']."'");
			$p_cek=$conn->GetRow("select statuseksemplar from pp_eksemplar e
								  join ms_pustaka p on e.idpustaka=p.idpustaka 
								  where ideksemplar=$r_eks and kdkondisi='V' and p.kdjenispustaka in (".$_SESSION['roleakses'].")");
			$p_cek2=$conn->GetRow("select tglexpired from pp_reservasi where ideksemplarpesan=$r_eks and tglexpired>=current_date");
			if (!$p_angg){
				$errdb = 'Identitas Anggota tidak ditemukan.';	
				Helper::setFlashData('errdb', $errdb);	
			}else {
			/*if($p_angg['tglselesaiskors']>=date('Y-m-d') or $p_angg['statusanggota']==0){
				$errdb = 'Peminjaman Pustaka gagal, Anggota sedang bermasalah.';	
				Helper::setFlashData('errdb', $errdb);
			
			}else { */
			if (!$p_cek2 and $p_cek['statuseksemplar']=='ADA'){
			
			$record=array();
			$record = Helper::cStrFill($_POST);
			$record['kdlokasi']=$_SESSION['lab'];
			$record['idanggota']=$_SESSION['idlab'];
			$record['kdjenistransaksi']='PJL';
			$record['tgltransaksi']=date('Y-m-d');
			$record['ideksemplar']=$r_eks;
			$record['statustransaksi']=1;
			$record['fix_status']=0;
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			$rec['statuseksemplar']='PJM';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$r_eks);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Peminjaman Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Peminjaman Pustaka berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//$url='index.php?page=sirkulasi_lab&kd='.$keyfilter;
				Helper::redirect();
			}	
			}else{
				$errdb = 'Pustaka tidak dapat dipinjam.';	
				Helper::setFlashData('errdb', $errdb);
			} //}
		}
		}else if($r_aksi=='delsession') {
		
			$conn->Execute("update pp_transaksi set fix_status='1' where kdlokasi = '".$_SESSION['lab']."'");
			unset($_SESSION['idlab']);
			unset($_SESSION['lab']);
			$sucdb = 'Proses transaksi telah selesai';	
			Helper::setFlashData('sucdb', $sucdb);
			Helper::redirect();
		
		}
		elseif($r_aksi=="kembali" and $c_edit){
			$p_eks=$conn->GetRow("select ideksemplar from pp_eksemplar where noseri='$r_key2'");
			$r_eks=$p_eks['ideksemplar'];
			$conn->StartTrans();
			$record['tglpengembalian']=date("Y-m-d");
			$record['statustransaksi']=0;
			Helper::Identitas($record);
			Sipus::UpdateBiasa($conn,$record,$p_dbtable,idtransaksi,$r_key);
			
			$rec['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$rec,pp_eksemplar,ideksemplar,$r_eks);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Pengembalian Pustaka gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pengembalian Pustaka Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				//$url='index.php?page=sirkulasi_lab&kd='.$keyfilter;
				Helper::redirect();
			}	
		}
		elseif ($r_aksi=="cari"){
			$r_key3= Helper::removeSpecial($_POST['key3']);
			if($fil==''){
				$errdb = 'Masukkan Lokasi laboratorium.';	
				Helper::setFlashData('errdb', $errdb);
			} else{
			if ($r_key3!='' and $carii!=''){
				$p_sqlstr .=" and $r_key3 ='$carii'";
			}else{
				$errdb = 'Masukkan kategori dan kata kunci dengan benar.';	
				Helper::setFlashData('errdb', $errdb);}
		}}
		elseif ($r_aksi=="batal"){
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_transaksi,idtransaksi,$r_key);
			
			$record['statuseksemplar']='ADA';
			Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$r_key2);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Pembatalan Peminjaman gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Pembatalan Peminjaman Berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
			}
			
		
		}
	}
	// list lokasi
	if($c_edit) {
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2("kdlokasi",$fil=='' ? $_POST['kdlokasi'] : $fil,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:140" onChange="goFilter()"');

		}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('txtpustaka').focus()">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<? if($_SESSION['idlab']==''){ ?>
<!--<div align="center" style="position:relative;top:-11px;">
	<div class="loginBox">
		<div class="loginTitle">SIRKULASI TUGAS AKHIR</div>
		<div class="loginContent"> -->
		<table width="400" border=0 align="center" cellpadding="6" cellspacing=0 style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
			<tr height="25">
				<th colspan="2">SIRKULASI LABORATORIUM</th>
			</tr>
			<tr>
				<td><b>LOKASI</b></td>
				<td><?= $l_lokasi ?></td>
			</tr>
			<tr>
				<td width="130"><b>ID ANGGOTA</b></td>
				<td><input type="text" maxlength="20" name="idanggotalab" class="ControlStyle" id="idanggota" value="<?= $_POST['idanggotalab'] ?>">
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota"  onClick="popup('index.php?page=pop_anggota&code=s',630,500);">
				
				</td>
			</tr>
			
			<tr>
				<td align="center" colspan=2><input type="submit" name="btnlab" id="btnlab" value="Mulai Transaksi" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:23px;width:150px;font-size:12px;"></td>
			</tr>
		</table>
	<!--	</div>
	</div>
</div> -->
<? include_once('_notifikasi.php'); ?>
<? } else { ?>
	<table cellpadding="1" cellspacing="0" cellpadding=4 align="center" width="600"  style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;margin-top:10px;">
		<tr height="25">
			<th colspan="4">Sirkulasi Laboratorium</td>
		</tr>
		<tr height="10">
			<td>&nbsp;</td>
		</tr>
		<tr height="22">
			<td ><b>Nama Lokasi</b></td>
			<td width="300"> <b>: <?= $getLab['kdlokasi']." - ". $getLab['namalokasi'] ?></b></td>
			
		</tr>
		<tr height="22">
			<td width=160 ><b>Id Anggota</b></td>
			<td><b>: <?= $anggota['idanggota'] ?></b>
			</td>
			<td rowspan=3 align="center">
			
			<img src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default1.jpg') ?>" width="80" height="80" border=1></td>
		</tr>
		<tr height="22">
			<td ><b>Nama Anggota </b></td>
			<td width="300"><b> : <?= $anggota['namaanggota'] ?></b></td>
			
		</tr>
		<tr height="22">
			<td><b>Jenis Keanggotaan</b></td>
			<td><b>: <?= $anggota['namajenisanggota'] ?></b></td>
		</tr>		

	</table>
		<table width="50" style="padding:5px 5px 3px 20px;margin:5px 0px 5px 380px;background-image:none;border-width:1px;width:220;height:40px;" class="GridStyle">
		<tr>
			<td align="center">
				<span class="out" name="btnselesai" id="btnselesai" value="Selesai" onClick="goClear()" style="padding-right:0px;cursor:pointer;"></span><u style="cursor:pointer;" onClick="goClear()">Selesai</u>
			</td>
			<td style="padding:0px 10px;">|
			</td>
			<td>
				<img src="images/printer.png" title="Cetak Nota Transaksi" onClick="popup('index.php?page=cetak_notau&id=<?= $_SESSION['lab']; ?>&date=<?= date("Y-m-d"); ?>',450,400);" style="cursor:pointer">
				<u style="cursor:pointer;position:relative;bottom:12px;" onClick="popup('index.php?page=cetak_notau&id=<?= $_SESSION['lab']; ?>&date=<?= date("Y-m-d"); ?>',450,400);">Cetak Nota</u>
			</td>
		</td>
		</table>
	<table width="99%">
	<tr>	
		<td>
		<div class="tabs" style="width:100%">
		<ul>
			<li><a id="tablink" href="javascript:void(0)" onClick="goTab(0)">Peminjaman</a></li>
			
			<li><a id="tablink" href="javascript:void(0)"  onclick="goTab(3)">Sejarah Peminjaman</a></li>
			
			<div class="a" align="right" valign="center"><? include_once('_notifikasi_trans.php'); ?><br></div>
		
		</ul>

<!-- ================================= transaksi peminjaman umum============================ -->

	<div id="items" style="position:relative;top:-2px;">
		<table border=0 width="100%" cellpadding=0 cellspacing=0>
			<tr height="35">
				<td width="170" class="LeftColumnBG" style="border:none;">Masukkan NO INDUK</td>
				<td width="180" class="LeftColumnBG" style="border:none;">: <input type="text" name="txtpustaka" id="txtpustaka" maxlength="20" size="22">
				</td>
				<td class="LeftColumnBG" width="350" style="border:none;">
				<? //if(strtotime($anggota['tglselesaiskors'])>=strtotime(date("Y-m-d")) or $anggota['statusanggota']!=1) { ?>
				<!-- <input type="button" name="btnta" id="btnta" value="Pinjam Baca" class="buttonSmallDis" style="height:23px;width:130px;font-size:12px;">-->
				<?// } else { ?>
				<input type="button" name="btnta" id="btnta" value="Pinjam" onclick="goPinjamLab()" class="buttonSmall" style="height:23px;width:130px;font-size:12px;">
				&nbsp;
				<img src="images/popup.png" style="cursor:pointer;" title="Lihat Eksemplar" onClick="popup('index.php?page=pop_ekspinjam',700,500);">
				<span id="span_eksemplar"><u title="Lihat Eksemplar" onclick="popup('index.php?page=pop_ekspinjam',700,500);" style="cursor:pointer;" class="Link">Lihat Eksemplar...</u></span>
	
				<? //} ?>
				</td>
				<td width="35%" align="right" class="LeftColumnBG" style="border:none;"><? include_once('_notifikasi_trans.php') ?></td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
				<td width="6%" nowrap align="center" class="SubHeaderBGAlt">Batal</td>
				<td width="6%" nowrap align="center" class="SubHeaderBGAlt">Kembali</td>
				<td width="8%" nowrap align="center" class="SubHeaderBGAlt">NO INDUK</td>
				<td width="25%" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka</td>
				<td width="18%" nowrap align="center" class="SubHeaderBGAlt">Penanggung Jawab</td>				
				<td width="16%" nowrap align="center" class="SubHeaderBGAlt">Pengarang</td>
				<td width="8%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Pinjam</td>
			</tr>
			<? $i=0;
			while ($row=$transnow->FetchRow()){
			if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
				if ($row['idtransaksi'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="35">
				<td align="center">
				<? if ($row['fix_status']==0) { ?>
				<u title="Batal" onclick="goBatal('<?= $row['idtransaksi'] ?>','<?= $row['noseri'] ?>');" class="link"><img title="Batal meminjam" src="images/batal.png"></u>
				<? } else { ?>
				<u title="Batal" ><img title="Batal meminjam" src="images/batal2.png"></u>
				<? } ?></td>
				<td align="center">
				<u title="Kembalikan Pinjaman" onclick="goKembali('<?= $row['idtransaksi']; ?>','<?= $row['noseri'] ?>');" class="link"><img src="images/kembalikan.png"></u>
				</td>
				<td>&nbsp; <?= $row['noseri'] ?></td>
				<td>&nbsp; <?= $row['judul'].($row['judulseri']!='' ? " : ".$row['judulseri'] : "").($row['edisi']!='' ? " / ".$row['edisi'] : "") ?></td>
				<td><?= $row['idanggota']." - ".$row['namaanggota'] ?></td>
				<td>&nbsp; <?= $row['authorfirst1']." ".$row['authorlast1']; ?></td>
				<td align="center"><?= Helper::tglEng($row['tgltransaksi']) ?></td>
			</tr>
			<? } if ($i==0) { ?>
			<tr height="20">
				<td align="center" colspan="6"><b>Belum ada peminjaman.</b></td>
			</tr>
			<?php } ?>
				
		</table>
	</div>
		
	<div id="items" style="position:relative;top:-2px"> 
		<table cellspacing="0" width="100%">
			<tr><td class="LeftColumnBG"  style="border-top:none;border-left:thin solid #999" colspan=7>&nbsp;</td></tr>
			<tr height="20"> 
				<td width="80" nowrap align="center" class="SubHeaderBGAlt">NO INDUK</td>
				<td width="30%" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka </td>
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt">Penanggung Jawab </td>
				<td width="20%" nowrap align="center" class="SubHeaderBGAlt">Pengarang</td>		
				<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Pinjam</td>
				<td width="13%" nowrap align="center" class="SubHeaderBGAlt">Tanggal Kembali</td>
				
				<?php
				$a = 0;

					// mulai iterasi
					while ($rows = $transe->FetchRow()) 
					{
						if ($a % 2) $rowstyle3 = 'NormalBG';  else $rowstyle3 = ''; $a++; 
						if ($rows['idtransaksi'] == '0') $rowstyle3 = 'YellowBG';
			?>
			<tr class="<?= $rowstyle3 ?>" height="20" valign="middle"> 
				<td><?= $rows['noseri']; ?></u></td>
				<td align="left"><?=  Helper::limitS($rows['judul'],53).($row['judulseri']!='' ? " : ".$row['judulseri'] : "").($rows['edisi']!='' ? " / ".$rows['edisi'] : "") ?></td>
				<td><?= $rows['idanggota']." - ".$rows['namaanggota'] ?></td>
				<td><?= $rows['authorfirst1']." ".$rows['authorlast1'] ?></td>
				<td align="center"><?= Helper::tglEng($rows['tgltransaksi']); ?></td>
				<td >&nbsp;
				<? if ($rows['tglpengembalian']=='')
					echo "Belum kembali";
					else
					echo Helper::tglEng($rows['tglpengembalian']);
				?></td>			
			</tr>
			<?php
				}
				if ($a==0) {
			?>
			<tr height="20">
				<td align="center" colspan="5"><b>Belum ada transaksi.</b></td>
			</tr>
			<?php } ?>
			
		</table>
		<br>
		<table align="center" cellspacing="3" cellpadding="2" width="370" border=0 bgcolor="#f1f2d1">
			<tr>
				<td width=270 align="center"><u>
				<script>
				var text="Sejarah transaksi diatas merupakan 15 transaksi terakhir";
				document.write(text.blink());
				</script>
				</u></td>
			</tr>
		</table>
		
		</div>
		</div>

		</td>
	</tr>
	</table>
	
<? } ?>
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="key3" id="key3" value="<?= $r_key3 ?>">
</form>
</div>
</body>
<script language="javascript">
$(document).ready(function() {

	
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	var key=document.getElementById("key3").value;
	
	if (key=='' || key==0)
	chooseTab(0);

	else if (key==1)
	chooseTab(1);
	else if(key==2)
	chooseTab(2);
	else
	chooseTab(3);

});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>
<script type="text/javascript">
function goPinjamLab(){
	if(cfHighlight("txtpustaka")){
	document.getElementById('act').value="pinjam";
	goSubmit();
	}
}
function goClear() {
	var conf=confirm("Apakah Anda yakin telah menyelesaikan peminjaman ?");
	if(conf){
	document.getElementById("act").value='delsession';
	goSubmit();
	}
}
function goBatal($key,$eks) {
		var batalno=confirm("Apakah Anda yakin akan membatalkan transaksi ini ?");
		if(batalno){
		document.getElementById("act").value='batal';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goKembali($key,$eks) {
		var kembalikan=confirm("Apakah Anda yakin akan melakukan proses pengembalian ?");
		if(kembalikan){
		document.getElementById("act").value='kembali';
		document.getElementById("key").value=$key;
		document.getElementById("key2").value=$eks;
		goSubmit();
		}
}
function goTab($key3) {
		document.getElementById("key3").value=$key3;
		//document.perpusform.submit();
		
}
</script>
</html>