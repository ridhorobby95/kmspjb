	<?php
		defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

		// pengecekan tipe session user
		$a_auth = Helper::checkRoleAuth($conng,false);

		// otorisasi user
		$c_add = $a_auth['cancreate'];
		$c_edit = $a_auth['canedit'];
		$c_delete = $a_auth['candelete'];

		// pengecekan tipe session user
		 $a_auth = Helper::checkRoleAuth($conng);

		
		// definisi variabel halaman
		$p_dbtable = 'ms_pustaka';
		$p_window = '[PJB LIBRARY] History Keywords';
		$p_title = 'History Keywords';
		$p_tbheader = '.: History Keywords :.';
		$p_col = 3;
		$p_tbwidth = 100;
		$p_id = "ms_key";
		$p_row = 10;
		
		// sql untuk mendapatkan isi list
		$p_sqlstr = "select distinct keywords from $p_dbtable where keywords is not null";

		// pengaturan ex
		if (!empty($_POST)) {
			$r_aksi = Helper::removeSpecial($_POST['act']);
			$p_page 	= Helper::removeSpecial($_REQUEST['page']);
			
					
		//filtering
		$keykeyword=Helper::removeSpecial($_POST['carikeyword']);
		if($keykeyword!='')
			$p_sqlstr.=" and upper(keywords) like upper('%$keykeyword%')";
		
		}
		else
		{
			// dapatkan nilai ex dari session
			if ($_SESSION[$p_id.'.page'])
				$p_page = $_SESSION[$p_id.'.page'];
		}
		if (!$p_page)
			$p_page = 1; // halaman default adalah 1
			
		// eksekusi sql list

		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		if ($rs->EOF) {
			// tidak ditemukan record atau ada kesalahan
			$p_atfirst = true;
			$p_atlast = true;
			$p_lastpage = 0;
			$p_page = 0;
		}
		else {
			// ditemukan record
			$p_atfirst = $rs->AtFirstPage();
			$p_atlast = $rs->AtLastPage();
			$p_lastpage = $rs->LastPageNo();
			$showlist = true;
		}
	?>
	<html>
	<head>
		<title><?= $p_window ?></title>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<link href="style/style.css" type="text/css" rel="stylesheet">
		<link href="style/pager.css" type="text/css" rel="stylesheet">
		<link href="style/button.css" type="text/css" rel="stylesheet">
		
		<script type="text/javascript" src="scripts/forinplace.js"></script>
		<script type="text/javascript" src="scripts/forpager.js"></script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
	<div id="wrapper" style="width:auto;margin:0;padding:0;">
		<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
			<div align="center">
			<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<div class="filterTable">
				<table border=0  cellpadding=0 cellspacing=0 width="100%">
					<tr>
					<td>Keywords : &nbsp;<input type="text" name="carikeyword" id="carikeyword" size="25" value="<?= $keykeyword ?>" onKeyDown="etrCari(event);"></td>
					<td align="right" >
						<!--a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a-->
						<input type="button" value="Cari" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRefresh();goFilter(false);" />
					</td>
					</tr>
				</table>
			</div>
			<header style="width:430px;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<!--img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" /-->
						<h1><?= $p_title; ?></h1>
					</div>
				</div>
			</header>
			<!--table width="<?= $p_tbwidth; ?>">
				<tr height="40">
					<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
				</tr>
			</table-->

			<table width="<?= $p_tbwidth ?>" border="1" cellpadding="3" cellspacing=0 class="GridStyle">
				<!--tr height="20"> 
					<td colspan="3">
					<table border=0  cellpadding=0 cellspacing=0 width="100%">
					<tr><td>
					<a href="javascript:goRefresh()" class="buttonshort"><span class="refresh">Refresh</span></a>
					</td>
					<td align="right">
					<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
					<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
					<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
					<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
				</td></tr>
				</table>
				</td>
				</tr-->
				<!--tr>
					<td colspan="3">
					<table border=0  cellpadding=0 cellspacing=0 width="100%">
					<tr><td>
					Keywords : &nbsp;<input type="text" name="carikeyword" id="carikeyword" size="25" value="<?= $keykeyword ?>" onKeyDown="etrCari(event);"></td>
					<td align="right" ><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>
					</tr>
					</table>
					</td>
				</tr-->
				<tr height="20"> 
					<td width="330" nowrap align="center" class="SubHeaderBGAlt thLeft">Keywords</td>
					<td width="50" nowrap align="center" class="SubHeaderBGAlt thLeft">Pilih</td>
				</tr>
				<?php
					$i = 0;
					while ($row = $rs->FetchRow()) 
					{
						if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					
				?>
				<tr class="<?= $rowstyle ?>">
					<td><u title="Pilih Keywords" class="link" onclick="goSend('<?= $row['keywords']?>')"><?= $row['keywords']?></u></td>
					<td align="center">
					<img title="Pilih Keywords" src="images/centang.gif" style="cursor:pointer" onclick="goSend('<?= $row['keywords']?>')">
					</td>
				</tr>
				<?php
						
					}
					if ($i==0) {
				?>
				<tr height="20">
					<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
				</tr>
				<? } ?>
				<tr> 
					<td class="PagerBG footBG" align="right" colspan="4"> 
						Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
					</td>
					
				</tr>
			</table>
				<?php require_once('inc_listnav.php'); ?><br>
			<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="key" id="key">
			</form>
			</div>
		</div>
	</div>
	</body>
	<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

	</script>
	<script type="text/javascript">


	function goRefresh(){
	document.getElementById("carikeyword").value='';
	goSubmit();

	}

	function goSend(key) {
		
		window.opener.document.getElementById("keywords").value = key;
		window.close();
	}
	</script>
	</html>