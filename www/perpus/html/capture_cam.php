<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
$r_name=Helper::removeSpecial($_GET['key']);
$r_code=Helper::removeSpecial($_GET['code']);
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>[ PJB LIBRARY ] TAKE PHOTO</title>
	<meta name="generator" content="TextMate http://macromates.com/">
	<meta name="author" content="Joseph Huckaby">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<!-- Date: 2008-03-15 -->
</head>
<body>
	<table>
	<tr>
		<td>
		<div id="upload_results"></div>
		</td>
	</tr>
	<tr><td valign=top>
	<!-- First, include the JPEGCam JavaScript Library -->
	<script type="text/javascript" src="scripts/webcam.js"></script>
	<script type="text/javascript" src="scripts/jquery.js"></script>
	
	<!-- Configure a few settings -->
	<script language="JavaScript">
		webcam.set_api_url('index.php?page=captured&key=<?= $r_name ?>&code=<?= $r_code ?>');
		webcam.set_quality( 90 ); // JPEG quality (1 - 100)
		webcam.set_shutter_sound( false ); // play shutter click sound
	</script>
	
	<!-- Next, write the movie to the page at 320x240 -->
	<script language="JavaScript">
		document.write( webcam.get_html(320, 240) );
	</script>
	
	<!-- Some buttons for controlling things -->
	<br/><form><br>
	<div align="center">
		<!-- <input type=button value="Configure..." onClick="webcam.configure()"> -->
		&nbsp;&nbsp;
		<input type=button value="Capture" onClick="webcam.freeze()">
		&nbsp;&nbsp;
		<input type=button value="Simpan" onClick="do_upload()">
		&nbsp;&nbsp;
		<input type=button value=" Reset " onClick="webcam.reset()">
		<input type="hidden" name="act" id="act">
	</form>
	</div>
	<!-- Code to handle the server response (see test.php) -->
	<script language="JavaScript">
		webcam.set_hook( 'onComplete', 'my_completion_handler' );
		
		function do_upload() {
			// upload to server
			//document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
			document.getElementById('act').value="captured";
			webcam.upload();
		}
		
		function my_completion_handler(msg) {
			// extract URL out of PHP output
			if (msg.match(/(http\:\/\/\S+)/)) {
				var image_url = RegExp.$1;
				// show JPEG image in page
				document.getElementById('upload_results').innerHTML = 
					' ';
				
				alert('Photo telah tersimpan');
				// reset camera for another shot
				//webcam.reset();
				//window.opener.document.getElementById("imgfoto").waitload({mode: "unload"});
				window.opener.document.getElementById("imgfoto").src=image_url+'?'+<?= mt_rand(1000,9999) ?>;
				//window.opener.location.reload();
				window.close();
			}
			else alert("PHP Error: " + msg);
		}
	</script>
	
	
	</tr>

	</table>
</body>
</html>