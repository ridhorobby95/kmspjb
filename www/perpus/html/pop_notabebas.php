<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	$conn->debug=false;
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$kepala = Sipus::getHeadPerpus();

	$r_id=Helper::removeSpecial($_GET['id']);

	
	//$anggota=$conn->GetRow("select idanggota, namaanggota, idunit from ms_anggota where idanggota='$r_id'");
	$anggota=$conn->GetRow("select u.*, u.nama as namaanggota, n.nama from um.users u 
				left join um.unit n on u.kodeunit = n.kodeunit where nid = '$r_id' ");
						  
?>

<html>
<head>
<title>Surat Bebas Pustaka</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
    <style type="text/css">
	
body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9pt;
	
}
    </style>
</head>
<body topmargin=0 leftmargin=3 rightmargin=0 bottommargin=0 onLoad="printpage()"> <!-- onload="printpage()-->
<table cellpadding="8" cellspacing="0" border="0" width="870">
<tr><td>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="65"><img src="images/logo_report.jpg" height="60"></td>
            <td valign="bottom"><h3 style="margin;0">PERPUSTAKAAN<br>PJB</h3></td>
		</tr>
        <tr><td>&nbsp;</td></tr>
		<tr>
			<td colspan="2" align="center"><font size="4"><b>SURAT KETERANGAN BEBAS PUSTAKA</b></font><br>
			No. Surat : <?= Helper::removeSpecial($_GET['no']) ?>
			</td>
		</tr>
		<tr height="30"><td>&nbsp;</td></tr>
		<tr>
			<td style="padding:1em 1em;" colspan="2" align="left">Yang bertanda tangan dibawah ini menerangkan bahwa:<br><br>
				Nama <?php echo str_repeat("&nbsp;",15); ?>: <?= $anggota['namaanggota'] ?><br>
				No. Anggota <?php echo str_repeat("&nbsp;",5);?>: <?= $anggota['nid'] ?><br>
				Unit <?php echo str_repeat("&nbsp;",18);?>: <?= $anggota['nama']; ?><br><br>
				Berdasarkan data kami, sudah tidak mempunyai pinjaman koleksi bahan pustaka di Perpustakaan PJB (PJB).<br>
				Demikian surat keterangan ini diberikan untuk dapat dipergunakan sebagaimana mestinya.
			</td>
		</tr>
		<tr height="30"><td>&nbsp;</td></tr>
		<tr>
			<td style="padding:.3em 10em;" colspan="2">
				<div style="float:left;margin-left:400px;">
					<span>Surabaya, <?php echo Helper::formatDateInd(date("Y-m-d"))?></span><br>
					<div style="background-size:80px 80px;background-repeat:no-repeat;">
					<span><b><?=$kepala['jabatan'];?> PJB,</b></span><br><br><br><br><br>
					<span><b><?=$kepala['namalengkap'];?></b></span><br>
					<span><b>NIP. <?=$kepala['nik'];?></b></span>
					</div>
				</div>
			</td>
		</tr>
	</table>
<br> <br> <br> 
</td></tr>
</table>

</body>
<script type="text/javascript">
function printpage()
  {
  window.print()
  }
</script>
</html>