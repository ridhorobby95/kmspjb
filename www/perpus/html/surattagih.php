<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
 
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman

	$p_window = '[PJB LIBRARY] Daftar Penagihan Pustaka';
	$p_title = 'Daftar Penagihan Pustaka';
	$p_tbheader = '.: Daftar Penagihan Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 100;
	$p_lihat = Helper::navAddress('list_tagih.php');
	$p_id = "surattagih";
	$p_baru = Helper::navAddress('ms_tagih.php');
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'p.idanggota';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list

	$p_sqlstr="select p.*, a.alamat, a.email
		from pp_tagihan p 
		join ms_anggota a on p.idanggota=a.idanggota
		where 1=1 $sqlAdminUnit ";
			
	if (!empty($_POST))
	{
		//filtering
		$keyid=Helper::removeSpecial($_POST['cariid']);
		$keynama=Helper::removeSpecial($_POST['carinama']);	
		$keyjenis=Helper::removeSpecial($_POST['kdjenisanggota']);
		$tgl1 = Helper::formatDate($_POST['tgl1']);
		$tgl2 = Helper::formatDate($_POST['tgl2']);
		
		if ($keyid!='')
			$p_sqlstr.=" and a.idanggota = '$keyid'";
			if ($keynama!='')
				$p_sqlstr.=" and upper(a.namaanggota) like upper('%$keynama%')";
				if ($keyjenis!='')
					$p_sqlstr.=" and a.kdjenisanggota='$keyjenis' ";
		
		$status = Helper::removeSpecial($_POST['status']);
		if($status==1)
		$p_sqlstr .=" and to_char(p.tglexpired,'YYYY-mm-dd') >= to_char(current_date,'YYYY-mm-dd') ";
		else if($status==2)
		$p_sqlstr .=" and to_char(p.tglexpired,'YYYY-mm-dd') < to_char(current_date,'YYYY-mm-dd') ";
		else
		$p_sqlstr .="";
		
		if($tgl1!='' and $tgl2!=''){
			$p_sqlstr .=" and p.tgltagihan between '$tgl1' and '$tgl2' ";		
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
	}else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
		if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	
	if($status==''){
		$status=1;
		$p_sqlstr .=" and p.tglexpired>= current_date ";
	}
	
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);

	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$rs_cb = $conn->Execute("select namajenisanggota, kdjenisanggota from lv_jenisanggota order by kdjenisanggota");
	$l_jenis = $rs_cb->GetMenu2('kdjenisanggota',$keyjenis,true,false,0,'id="kdjenisanggota" class="ControlStyle" style="width:120" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_jenis);	
	
	$a_status = array('0' => '-- Semua --', '1' => 'Aktif','2' => 'Tidak Aktif');
	$l_status = UI::createSelect('status',$a_status,$status,'ControlStyle',$c_edit,'id="status" style="width:120" onchange="goSubmit()"');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0"  onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div class="filterTable table-responsive">
			<table cellpadding=0 cellspacing="2" border="0" width="100%" class="GridStyle" style="border:0 none;">
			<tr>
				<td class="thLeft" style="border:0 none;" width="120">Id Anggota</td>
				<td class="thLeft" style="border:0 none;" width="35%">: <input type="text" id="cariid" size="20" name="cariid" value="<?= $keyid ?>" onkeydown="etrCari(event);"></td>
				<td class="thLeft" style="border:0 none;">Jenis Anggota</td>
				<td class="thLeft" style="border:0 none;">: <?= $l_jenis ?> </td>
				<td class="thLeft" style="border:0 none;" align="right"></td>		
				</tr>
			<tr>
				<td class="thLeft" style="border:0 none;">Nama Anggota</td>
				<td class="thLeft" style="border:0 none;">: <input type="text" id="carinama" name="carinama" size="35" value="<?= $_POST['carinama'] ?>" onkeydown="etrCari(event);"></td>
				<td class="thLeft" style="border:0 none;">Status Tagihan</td>
				<td class="thLeft" style="border:0 none;">: <?= $l_status ?> </td>
				<td class="thLeft" style="border:0 none;" colspan="2"></td>
			</tr>
			<tr height="25">
				<td class="thLeft" style="border:0 none;">Periode</td>
				<td class="thLeft" style="border:0 none;" >: <input type="text" id="tgl1" name="tgl1" size=10 value="<?= Helper::formatDate($tgl1) ?>">
				<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl1",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle1",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				s/d <input type="text" id="tgl2" name="tgl2" size=10 value="<?= Helper::formatDate($tgl2) ?>">
				<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal akhir">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tgl2",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgle2",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				</td>
				<td class="thLeft" style="border:0 none;" colspan="2">
					
				</td>
				<td class="thLeft" style="border:0 none;" align="right">
					<input type="button" value="Filter" class="ControlStyle" onclick="javascript:goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
				</td>
			</tr>
			</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title ?></h1>
					</div>
				</div>
			</header>
           <div class="table-responsive"> 
		  <table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20">
			  <td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft"  style="cursor:pointer;" onClick="PopupMenu('popPaging','p.idanggota:C');">Id Anggota  <?= $p_xsort['p.idanggota']; ?></td>
			  <td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.namaanggota:C');">Nama Anggota <?= $p_xsort['a.namaanggota']; ?></td>
			  <td width="27%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.alamat:C');">Alamat <?= $p_xsort['a.alamat']; ?></td>
			  <td width="20%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.email:C');">Email <?= $p_xsort['a.email']; ?></td>
			  <td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.tgltagihan:D');">Tgl Tagihan <?= $p_xsort['p.tgltagihan']; ?></td>
			  <td width="10%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.tglexpired:D');">Tgl Expired <?= $p_xsort['p.tglexpired']; ?></td>
			  <td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.tagihanke:C');">Tagihan Ke <?= $p_xsort['p.tagihanke']; ?></td>
			  <td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>
			<?php
				$i = 0;
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['idanggota'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="20" valign="middle">
			  <td align="left"><?= $row['idanggota']; ?></td>
			  <td align="left"><?= $row['namaanggota']; ?></td>
			  <td align="left"><?= $row['alamat']; ?></td>
			  <td align="left"><?= $row['email']; ?></td>
			  <td align="left"><?= Helper::tglEng($row['tgltagihan']); ?></td>
			  <td align="left"><?= Helper::tglEng($row['tglexpired']); ?></td>
			  <td align="center"><?= $row['tagihanke']; ?></td>
			  <td align="center"><u title="detil tagihan" style="cursor:pointer;color:blue" onclick="goLihate('<?= $p_lihat ?>','<?= $row['idtagihan'] ?>','<?= $row['idanggota'] ?>','<?= $row['namaanggota'] ?>')"><img src="images/edited.gif" width=18 height=18></u>
			  <a href="index.php?page=surat_tagih&idtagihan=<?= $row['idtagihan'] ?>" target="_blank" title="Cetak Surat Tagih"><img src="images/printer.png" width=18 height=18></a>
			  </td>
			</tr>
			<?php
		}
				
				if ($i==0) {
			?>
			<tr height="20">
			  <td align="center" colspan="<?= $p_col; ?>"><b>Penagihan Pustaka kosong</b></td>
			</tr>
			<?php } ?>
			<tr>
			  <td class="PagerBG footBG" align="right" colspan="8"> Halaman
				<?= $p_page ?>
				/
				<?= $p_lastpage ?>
				  <?= $p_status ?>
			  </td>
			</tr>
		  </table>
            </div>
		<?php require_once('inc_listnav.php'); ?>
		  <br>
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="nama" id="nama">
		<input type="hidden" name="id" id="id">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>

</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">
function goLihate(file,key,id,nama){
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value=key;
	document.getElementById("id").value=id;
	document.getElementById("nama").value=nama;
	goSubmit();
}

function goClear(){
document.getElementById("cariid").value='';
document.getElementById("carinama").value='';
document.getElementById("kdjenisanggota").value='';
document.getElementById("status").value='0';
document.getElementById("tgl1").value='';
document.getElementById("tgl2").value='';
goFilter(false);
}

</script>
</html>