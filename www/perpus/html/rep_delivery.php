<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and (pu.idunit in ($units) or a.idunit in ($units)) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Tanda Terima Pustaka';

	$p_filerep = 'repp_delivery';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select nottb, idttb 
				from pp_ttb
				where jnsttb=1
				and idttb in (
					select o.idttb 
					from pp_orderpustaka po
					left join pp_orderpustakattb o on o.idorderpustaka = po.idorderpustaka
					left join pp_usul pu on pu.idusulan=po.idusulan 
					left join ms_anggota a on a.idanggota = pu.idanggota 
					where 1=1 $sqlAdminUnit   
				)
				group by to_char(tglttb, 'YYYYMM'), nottb, idttb, tglttb
				order by tglttb desc");
	$l_nodev = $rs_cb->GetMenu2('nodev','',true,false,0,'id="nodev" class="ControlStyle" style="width:188"');
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Tanda Terima Pustaka</h1>
				</div>
			</div>
		</header>
          <div class="table-responsive">  
		<table width="<?= $p_tbwidth ?>%" cellspacing="0" cellpadding="4" class="GridStyle">
			<!--tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Tanda Terima Pustaka</td></tr-->
			<tr>
				<td width="120" class="LeftColumnBG thLeft">Nomer TTP</td>
				<td><?= $l_nodev?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
            </div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>
<script language="javascript">

function goPreSubmit() {
	if(cfHighlight("nodev"))
		goSubmit();
}

</script>
</html>
