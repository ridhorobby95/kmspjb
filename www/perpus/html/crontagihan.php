<?php
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');
	require_once('../classes/mail.class.php');
	
	$cek = $conn->IsConnected();

	if(!$cek){
	    echo '<span style="color: red;"><b>Tidak ada koneksi ke Database.</b></span>';
	    //Mail::SendMail("mfahruddinabdillah11@gmail.com","Perpustakaan PJB","Error Schedule Cron Perpustakaan","Eksekusi Cron dari Server Perpustakaan untuk mengambil data pegawai dari database Akademik mengalami gangguan, sehingga tidak dapat berjalan dengan baik. Mohon dilakukan check data.\n Cron Date : ".date('d-m-Y H:i:s')); 
		die();
	}
	
	$sqlstr = "select t.idanggota, a.namaanggota, a.email
		from pp_transaksi t
		join ms_anggota a on t.idanggota=a.idanggota
		where to_date(TO_CHAR(t.tgltenggat,'dd-mm-yyyy'),'dd-mm-yyyy') < to_date(TO_CHAR(current_date,'dd-mm-yyyy'),'dd-mm-yyyy')  
		and kdjenistransaksi='PJN' 
		and tglpengembalian is null
		and (a.email is not null or a.email <> '')
		group by t.idanggota, a.namaanggota, a.email";

	$rs = $conn->Execute($sqlstr);
	
	$i=0;	
	while ($row = $rs->FetchRow()){
		$i++;
		echo $i.". Mengirim email tagihan ke ".$row['idanggota']." - ".$row['namaanggota']." (".$row['email'].")<br/>";
		$exeMail = Mail::srtTagih($conn,$row['idanggota']);
		if(!$exeMail){
			echo '<span style="color: blue;">Email tagihan sudah terkirim.</span><br/>';
		}else{
			echo '<span style="color: red;">Email gagal terkirim.</span><br/>';
		}
	}
	
	if($i==0){
		echo "<b>Tidak ada Tagihan.</b>";
	}
	
?>
<head><title>[PJB LIBRARY] Cron Sinkronisasi - Perpustakaan Tagihan</title></head>
