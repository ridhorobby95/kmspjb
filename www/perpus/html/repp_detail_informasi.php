<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	//$conn->debug=true;
	
	$kepala = Sipus::getHeadPerpus();
	
	//definisi hari,bulan,tahun
	$bulantahun = date('M Y');
	$sekarang = date('d F Y');
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Detail Informasi di Perpustakaan PJB';
	
	$p_namafile = 'Laporan Jumlah  Buku Sampai_'.$bulantahun;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//koleksi BUKU terbanyak untuk judul.
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(distinct msp.idpustaka) as jumlah_judul
		from ms_pustaka msp
		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'

		group by substring(msp.kodeddc,1,1)
		order by jumlah_judul desc
		limit 1 ";

	$rs = $conn->GetRow($sql);
	$koleksi_judul_ddc = $rs['kode_ddc'];
	$koleksi_judul_jumlah = $rs['jumlah_judul'];
	
	//koleksi BUKU tersedikit untuk judul.
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(distinct msp.idpustaka) as jumlah_judul
		from ms_pustaka msp
		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'

		group by substring(msp.kodeddc,1,1)
		order by jumlah_judul asc
		limit 1";
	
	$rs = $conn->GetRow($sql);
	$koleksi_judul_ddcs = $rs['kode_ddc'];
	$koleksi_judul_jumlahs = $rs['jumlah_judul'];
	
	//koleksi BUKU terbanyak untuk eksemplar
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(ppe.ideksemplar) as jumlah_eksemplar
		from ms_pustaka msp left join pp_eksemplar ppe on msp.idpustaka = ppe.idpustaka
		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'

		group by substring(msp.kodeddc,1,1)
		order by jumlah_eksemplar desc
		limit 1";
	
	$rs = $conn->GetRow($sql);
	$koleksi_eksemplar_ddc = $rs['kode_ddc'];
	$koleksi_eksemplar_jumlah = $rs['jumlah_eksemplar'];
		
	//koleksi BUKU tersedikit untuk eksemplar
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(ppe.ideksemplar) as jumlah_eksemplar
		from ms_pustaka msp left join pp_eksemplar ppe on msp.idpustaka = ppe.idpustaka
		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'

		group by substring(msp.kodeddc,1,1)
		order by jumlah_eksemplar asc
		limit 1";
		
	$rs = $conn->GetRow($sql);
	$koleksi_eksemplar_ddcs = $rs['kode_ddc'];
	$koleksi_eksemplar_jumlahs = $rs['jumlah_eksemplar'];
		
	//jumlah peminjam BUKU terbanyak.
	$sql = "select count(ppt.idtransaksi) as jumlah,ppt.idanggota, msa.namaanggota as namaanggota
		from pp_transaksi ppt join ms_anggota msa on ppt.idanggota =msa.idanggota
					join pp_eksemplar ppe on ppt.ideksemplar=ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka=msp.idpustaka
		
		where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2' and msp.kdjenispustaka = 'B'
		
		group by ppt.idanggota, msa.namaanggota
		order by jumlah desc
		limit 1";
		
	$rs = $conn->GetRow($sql);
	$peminjam_terbanyak = $rs['namaanggota'];
	$peminjam_terbanyak_jumlah = $rs['jumlah'];
	
	//jumlah judul BUKU terbanyak dipinjam.
	$sql = "select count(ppt.ideksemplar) as jumlah,msp.judul
		from pp_transaksi ppt join pp_eksemplar ppe on ppt.ideksemplar = ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka = msp.idpustaka
		
		where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2' and msp.kdjenispustaka = 'B'
		
		group by msp.judul
		order by jumlah desc
		limit 1";
	
	$rs = $conn->GetRow($sql);
	$judul_terbanyakdipinjam = $rs['judul'];
	$judul_terbanyakdipinjam_jumlah = $rs['jumlah'];
	
	//jumlah BUKU kode DDC terbanyak dipinjam
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(ppt.ideksemplar) as jumlah_eksemplar
		from pp_transaksi ppt join pp_eksemplar ppe on ppt.ideksemplar = ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka = msp.idpustaka

		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'
			and
		ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2'

		group by kode_ddc
		order by jumlah_eksemplar desc
		limit 1";
	
	$rs = $conn->GetRow($sql);
	$terbanyak_ddc_dipinjam = $rs['kode_ddc'];
	$terbanyak_ddc_dipinjam_jumlah = $rs['jumlah_eksemplar'];
	
	//jumlah BUKU kode DDC tersedikit dipinjam
	$sql = "select substring(msp.kodeddc,1,1) as kode_ddc, count(ppt.ideksemplar) as jumlah_eksemplar
		from pp_transaksi ppt join pp_eksemplar ppe on ppt.ideksemplar = ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka = msp.idpustaka

		where substring(msp.kodeddc,1,1) in ('0','1','2','3','4','5','6','7','8','9') and msp.kdjenispustaka = 'B'
			and
		ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2'
		
		group by kode_ddc
		order by jumlah_eksemplar asc
		limit 1";
	
	$rs = $conn->GetRow($sql);
	$terbanyak_ddc_dipinjams = $rs['kode_ddc'];
	$terbanyak_ddc_dipinjam_jumlahs = $rs['jumlah_eksemplar'];
	
	//jumlah fakultas terbanyak meminjam BUKU
	$sql = "select count(ppt.idanggota) as jumlah, lvf.namafakultas
			from ms_anggota msa join lv_jurusan ljv on ljv.kdjurusan = msa.idunit
                                        join pp_transaksi ppt on ppt.idanggota = msa.idanggota
                                        join lv_fakultas lvf on ljv.kdfakultas = lvf.kdfakultas
					join pp_eksemplar ppe on ppt.ideksemplar=ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka=msp.idpustaka
                
		where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2' and msp.kdjenispustaka = 'B'
		
                group by lvf.namafakultas
                order by jumlah desc
                limit 1";
		
	
	$rs = $conn->GetRow($sql);
	$terbanyak_fakultas_meminjam = $rs['namafakultas'];
	$terbanyak_fakultas_meminjam_jumlah = $rs['jumlah'];
	
	 //jumlah fakultas tersedikit meminjam BUKU
	$sql = "select count(ppt.idanggota) as jumlah, lvf.namafakultas
			from ms_anggota msa join lv_jurusan ljv on ljv.kdjurusan = msa.idunit
                                        join pp_transaksi ppt on ppt.idanggota = msa.idanggota
                                        join lv_fakultas lvf on ljv.kdfakultas = lvf.kdfakultas
					join pp_eksemplar ppe on ppt.ideksemplar=ppe.ideksemplar
					join ms_pustaka msp on ppe.idpustaka=msp.idpustaka
                
		where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2' and msp.kdjenispustaka = 'B'
		   
                group by lvf.namafakultas
                order by jumlah asc
                limit 1";
		
	$rs = $conn->GetRow($sql);
	$terbanyak_fakultas_meminjams = $rs['namafakultas'];
	$terbanyak_fakultas_meminjam_jumlahs = $rs['jumlah'];
		
	
	
	//membuat array yang dibutuhkan.
	$koleksi = array();
	$gol = array();
	
	//mengisi array koleksi(bersifat tetap).
	$koleksi[0] = 'Karya Umum';
	$koleksi[1] = 'Karya Ilmu Jiwa';
	$koleksi[2] = 'Karya Keagamaan';
	$koleksi[3] = 'Ilmu Pengetahuan Sosial';
	$koleksi[4] = 'Ilmu Pengetahuan Bahasa';
	$koleksi[5] = 'Ilmu Pasti Alam';
	$koleksi[6] = 'Ilmu Pengetahuan Praktis';
	$koleksi[7] = 'Kesenian, Hiburan & Olahraga';
	$koleksi[8] = 'Kesusasteraan';
	$koleksi[9] = 'Sejarah, Geografi';
	
	//mengisi array kodeDDC (bersifat tetap).
	$gol[0] = '000';
	$gol[1] = '100';
	$gol[2] = '200';
	$gol[3] = '300';
	$gol[4] = '400';
	$gol[5] = '500';
	$gol[6] = '600';
	$gol[7] = '700';
	$gol[8] = '800';
	$gol[9] = '900';
	
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>

<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Detail Informasi Perpustakaan</h2>
  	</strong></td>
  </tr>
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()"><img src="images/printer.gif" title="Print Laporan"></a></td>
	</tr>
	<? } ?>
</table>


<table width="800" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>Informasi</strong></th> 
	<th width="10" align="center"><strong>Jumlah</strong></th>    
  </tr>
  
  <tr>
	<td> Jumlah BUKU Terbanyak untuk Judul adalah Koleksi <b><?=$koleksi[($koleksi_judul_ddc)] ?></b></td>
	<td align="center"> <?= $koleksi_judul_jumlah ?> </td>
  </tr>
  
  <tr>
	<td> Jumlah BUKU Tersedikit untuk Judul adalah Koleksi <b><?=$koleksi[($koleksi_judul_ddcs)] ?></b></td>
	<td align="center"> <?= $koleksi_judul_jumlahs ?> </td>
  </tr>
  
  <tr>
	<td> Jumlah BUKU Terbanyak untuk Eksemplar adalah Koleksi <b><?=$koleksi[($koleksi_eksemplar_ddc)] ?></b></td>
	<td align="center"> <?= $koleksi_eksemplar_jumlah ?> </td>
  </tr>
  
   <tr>
	<td> Jumlah BUKU Tersedikit untuk Eksemplar adalah Koleksi <b><?=$koleksi[($koleksi_eksemplar_ddcs)] ?></b></td>
	<td align="center"> <?= $koleksi_eksemplar_jumlahs ?> </td>
  </tr>
   
   <tr>
	<td> Peminjam BUKU terbanyak adalah <b><?= $peminjam_terbanyak ?></b></td>
	<td align="center"> <?= $peminjam_terbanyak_jumlah ?> </td>
  </tr>
   
    <tr>
	<td> Judul BUKU terbanyak dipinjam adalah <b><?= $judul_terbanyakdipinjam ?></b></td>
	<td align="center"> <?= $judul_terbanyakdipinjam_jumlah ?> </td>
  </tr>
    
  <tr>
	<td> BUKU terbanyak dipinjam adalah Koleksi <b><?= $koleksi[($terbanyak_ddc_dipinjam)] ?></b></td>
	<td align="center"> <?= $terbanyak_ddc_dipinjam_jumlah ?> </td>
  </tr>
  
  <tr>
	<td> BUKU tersedikit dipinjam adalah Koleksi <b><?= $koleksi[($terbanyak_ddc_dipinjams)] ?></b></td>
	<td align="center"> <?= $terbanyak_ddc_dipinjam_jumlahs ?> </td>
  </tr>
  
  <tr>
	<td> Fakultas terbanyak meminjam BUKU adalah <b><?= $terbanyak_fakultas_meminjam ?></b></td>
	<td align="center"> <?= $terbanyak_fakultas_meminjam_jumlah ?> </td>
  </tr>
  
   <tr>
	<td> Fakultas tersedikit meminjam BUKU adalah <b><?= $terbanyak_fakultas_meminjams ?></b></td>
	<td align="center"> <?= $terbanyak_fakultas_meminjam_jumlahs ?> </td>
  </tr>
</table>
<br><br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>