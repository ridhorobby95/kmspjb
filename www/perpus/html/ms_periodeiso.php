<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	// definisi variabel halaman
	$p_dbtable = 'lv_periodeiso';
	$p_window = '[PJB LIBRARY] Daftar Persentase Periode ISO';
	$p_title = 'Daftar Persentase Periode ISO';
	$p_tbheader = '.: Daftar Persentase Periode ISO :.';
	$p_col = 4;
	$p_tbwidth = 430;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select * from $p_dbtable order by periodemulai";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['periodemulai'] = Helper::formatDate($_POST['i_periodemulai']);
			$record['percent']=Helper::cStrNull($_POST['i_percent']);

			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$record = array();
			$record['periodemulai'] = Helper::formatDate($_POST['u_periodemulai']);
			$record['percent']=Helper::cStrNull($_POST['u_percent']);
			
			$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,idperiode,$r_key);
			
			if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); }
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idperiode,$r_key);
			
			if($err != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
					}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	}
	
	// eksekusi sql list
	$rs = $conn->Execute($p_sqlstr);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<table width="<?= $p_tbwidth; ?>">
	<tr height="40">
		<td align="center" colspan="2" class="PageTitle"><?= $p_title; ?></td>
	</tr>
</table>
<? include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
	
	<tr height="20"> 
		<td width="50" nowrap align="center" class="SubHeaderBGAlt">No.</td>
		<td width="120" nowrap align="center" class="SubHeaderBGAlt">Periode Mulai</td>
		<td width="130" nowrap align="center" class="SubHeaderBGAlt">Persentase ISO</td>
		<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
	</tr>
	<?php
		$i = 0;
		while ($row = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($row['idperiode'],$p_editkey)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>">
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Sunting Data" onclick="goEditIP('<?= $row['idperiode']; ?>');" class="link"><?= $i; ?></u>
		<? } else { ?>
			<?= $i; ?>
		<? } ?>
		</td>
		<td align="center"><?= Helper::formatDate($row['periodemulai']); ?></td>
		<td align="center"><?= $row['percent']; ?> %</td>
		<td align="center">
		<? if($c_delete) { ?>
			<u title="Hapus Data" onclick="goDeleteIP('<?= $row['idperiode']; ?>','<?= $row['periodemulai']; ?>');" class="link">[hapus]</u>
		<? } ?>
		</td>
	</tr>
	<?php
			} else { // row diupdate
	?>
	<tr class="<?= $rowstyle ?>"> 
		<td align="center"><?= $i; ?></td>
		<td align="center">
		<?= UI::createTextBox('u_periodemulai',Helper::formatDate($row['periodemulai']),'ControlStyle',10,10,true,'onKeyDown="etrUpdate(event);"'); ?>
		<? if($c_edit) { ?>
			<img src="images/cal.png" id="tglperiode1" style="cursor:pointer;" title="Pilih tanggal periode">
			&nbsp;
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "u_periodemulai",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tglperiode1",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			<? } ?>
		</td>
		<td align="center"><?= UI::createTextBox('u_percent',$row['percent'],'ControlStyle',3,5,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Update Data" onclick="updateData('<?= $row['idperiode']; ?>');" class="link">[update]</u>
		<? } ?>
		</td>
	</tr>
	<?php 	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } if($c_add) { ?>
	<tr class="LiteSubHeaderBG"> 
		<td align="center">*</td>
		<td align="center">
		<?= UI::createTextBox('i_periodemulai','','ControlStyle',10,10,true,'onKeyDown="etrInsert(event);"'); ?>
		<? if($c_edit) { ?>
			<img src="images/cal.png" id="tglperiode2" style="cursor:pointer;" title="Pilih tanggal periode">
			&nbsp;
			<script type="text/javascript">
			Calendar.setup({
				inputField     :    "i_periodemulai",
				ifFormat       :    "%d-%m-%Y",
				button         :    "tglperiode2",
				align          :    "Br",
				singleClick    :    true
			});
			</script>
			<? } ?>
		</td>
		<td align="center"><?= UI::createTextBox('i_percent','','ControlStyle',3,5,true,'onKeyDown="etrInsert(event);"'); ?></td>
		<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
	</tr>
	<?php } ?>
</table><br>

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="scroll" id="scroll">
</form>
</div>
</body>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_periodemulai').focus();
	<? } else { ?>
	document.getElementById('i_periodemulai').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_periodemulai,i_percent"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_periodemulai,u_percent")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

</script>
</html>