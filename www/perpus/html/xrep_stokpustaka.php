<?	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
	$c_edit = $a_auth['canedit'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Stok Pustaka';
	$p_title = 'Laporan Stok Pustaka';
	$p_filerep =  Helper::navAddress('rep_stokpustaka.php');
	
	$rs = $conn->Execute("select * from lv_jenispustaka order by namajenispustaka");
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<style type="text/css" media="screen">@import "style/basic.css";</style>
	<style type="text/css" media="screen">@import "style/tabs.css";</style>
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<div align="left"  style="width:500px">		
<form name="perpusform" id="perpusform" method="post" action="<?= $p_filerep; ?>" target="_blank">
<table width="100%" cellspacing="4" cellpadding="4" class="EditTabTable">
	<tr> 
		<td class="EditTabLine" colspan="3"><?= $p_title ?></td>
	</tr>
	<tr>
		<td width="25%" class="EditTabLabel">Jenis Pustaka</td>
		<td  width="75%"><? while($row = $rs->FetchRow()) { ?>
		<li><input type="checkbox" name="jenis[]" id="jenis_<?= $row['kdjenispustaka'] ?>" value="<?= $row['kdjenispustaka'] ?>" class="ControlStyle"> <label for="jenis_<?= $row['kdjenispustaka'] ?>"><?= $row['namajenispustaka'] ?></label></li>
		<? } ?></td>
	</tr>
	<tr>
		<td class="EditTabLabel">Tanggal</td>
		<td><?= UI::createTextBox('periode1',$r_periode1,'ControlStyle',10,10,$c_edit,'readonly'); ?>
		<img src="images/cal.png" id="periode1_trg" style="cursor:pointer;" title="Pilih tanggal Awal">
		<strong>s/d</strong>
		<?= UI::createTextBox('periode2',$r_periode2,'ControlStyle',10,10,$c_edit,'readonly'); ?>
		<img src="images/cal.png" id="periode2_trg" style="cursor:pointer;" title="Pilih tanggal Akhir"></td>
	</tr>
	<tr>
		<td width="25%" class="EditTabLabel">Format</td>
		<td><?= $l_format; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:showData();" class="button"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</div>
</body>

<script type="text/javascript">

Calendar.setup({
	inputField     :    "periode1",		// id of the input field
	ifFormat       :    "%d-%m-%Y",		// format of the input field
	button         :    "periode1_trg",	// trigger for the calendar (button ID)
	align          :    "Br",			// alignment (defaults to "Bl")
	singleClick    :    true
});

Calendar.setup({
	inputField     :    "periode2",		// id of the input field
	ifFormat       :    "%d-%m-%Y",		// format of the input field
	button         :    "periode2_trg",	// trigger for the calendar (button ID)
	align          :    "Br",			// alignment (defaults to "Bl")
	singleClick    :    true
});

function showData() {
	if(cfHighlight("periode1,periode2"))
		document.getElementById("perpusform").submit();
}
</script>
</html>