<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";

	// variabel request
	$r_status = Helper::removeSpecial($_REQUEST['statuse']);
	$r_label = helper::removeSpecial($_REQUEST['label']);
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_anggota = Helper::removeSpecial($_POST['txtanggota']);
	$r_jurusan = Helper::removeSpecial($_POST['kdjurusan']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_status=='' or $r_anggota=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Sirkulasi Anggota';
	
	$p_namafile = 'rekap_sirkulasi'.$r_anggota.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select r.*,e.noseri,e.harga, p.judul,p.nopanggil
		from pp_transaksi r
		join ms_anggota a on a.idanggota = r.idanggota 
		join pp_eksemplar e on r.ideksemplar=e.ideksemplar
		join ms_pustaka p on e.idpustaka = p.idpustaka
		where r.idanggota='$r_anggota' $sqlAdminUnit
			and to_date(to_char(r.tgltransaksi, 'YYYY-mm-dd'), 'YYYY-mm-dd')
				between to_date('$r_tgl1', 'YYYY-mm-dd') and to_date('$r_tgl2', 'YYYY-mm-dd') ";
				
	if ($r_status==1)
		$sql .=" and r.statustransaksi='0' ";
	if ($r_status==2)
		$sql .=" and r.statustransaksi='1'";
	
	if($r_label !='')
		$sql .=" and e.kdklasifikasi='$r_label'";
		
	if($r_lokasi)
		$sql .=" and e.kdlokasi = '$r_lokasi' ";
	
	$sql .=" order by r.tgltransaksi desc ";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	$namaanggota=$conn->GetOne("select namaanggota from ms_anggota where idanggota='$r_anggota'");
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Sirkulasi Pustaka</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> Id Anggota</td>
	<td>: <?= $r_anggota?>
  </tr>
  <tr>
	<td> Nama Anggota </td>
	<td>: <?= $namaanggota ?></td>
	</tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>No. Induk</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
    <th width="100" align="center"><strong>No. Panggil</strong></th>
	<th width="100" align="center"><strong>Tanggal Transaksi</strong></th>
	<th width="100" align="center"><strong><?= $r_label=='LH' ? "Harga" : "Tanggal Harus Kembali" ?></strong></th>
	<th width="100" align="center"><strong>Status</strong></th>
  </tr>
  <?php
	$no=1;
	$harga = 0;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $rs['noseri'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td ><?= $rs['nopanggil'] ?></td>
	<td align="center"><?= Helper::tglEngTime($rs['tgltransaksi']) ?></td>
	<td align="right"><?= $r_label=='LH' ? Helper::formatNumber($rs['harga'],'0',true,true)."&nbsp;" : ($rs['tgltenggat']!='' ? "<center>".Helper::tglEngTime($rs['tgltenggat'])."</center>" : "<center>Maximal</center>") ?></td>
	<td align="center"><?= $rs['statustransaksi']==0 ? "Kembali" : ($rs['statustransaksi']==1 ? "Pinjam" : "Perpanjang") ?></td>
	<!--<td align="center"><?= $rs['statustransaksi']==0 ? "Kembali" : ($rs['statustransaksi']==1 ? "Pinjam" : "Perpanjang") ?></td>-->
	
  </tr>
	<? $no++; $harga = $harga+$rs['harga'];} ?>
	<? if($no==1) { ?>
	<tr height=25>
		<td align="center" colspan=7 >Tidak ada sirkulasi</td>
	</tr>
	<?}else { ?>
   <tr height=25>
   <td colspan='<?= $r_label=='LH' ? '3' : '7' ?>'><b>Jumlah : <?= $rsc ?></b></td>
   <? if($r_label=='LH' and $no!=1) { ?>
   <td><b>Total</b><td align="right"><b> <?= Helper::formatNumber($harga,'0',true,true) ?>&nbsp;</b></td>
   <? } ?>
   </tr>
   <? } ?>
</table>


</div>
</body>
</html>
