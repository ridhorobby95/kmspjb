<?php
//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// definisi variabel halaman
	$p_dbtable = 'ms_author';
	$p_window = '[PJB LIBRARY] Daftar Master Author';
	$p_title = 'Daftar Master Author';
	$p_tbheader = '.: Daftar Master Author :.';
	$p_id = "ms_author";
	$p_col = 5;
	$p_tbwidth = 100;
	$p_row = 15;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select idauthor, namadepan, namabelakang, isbadan from $p_dbtable";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		//simpan session
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$record = array();
			$record['namadepan'] = Helper::cStrNull($_POST['i_namadepan']);
			$record['namabelakang'] = Helper::cStrNull($_POST['i_namabelakang']);
			$record['isBadan'] = ($_POST['i_isBadan']?1:0);
			Helper::Identitas($record);
			
			$ada = $conn->GetOne("select count(*) from ms_author where upper(namadepan) = upper('{$record['namadepan']}') and upper(namabelakang) = upper('{$record['namabelakang']}') ");
			if($ada > 0){
				$errdb = '<br/>Data Pengarang sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){
				$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
	
				if($err != 0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penyimpanan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
			}			
		}
		else if($r_aksi == 'update' and $c_edit) {

			$record['namadepan'] = Helper::cStrNull($_POST['u_namadepan']);
			$record['namabelakang'] = Helper::cStrNull($_POST['u_namabelakang']);
			$record['isBadan'] = ($_POST['u_isBadan']?1:0);
			Helper::Identitas($record);
			
			/*$ada = $conn->GetOne("select count(*) from ms_author where upper(namadepan) = upper('{$record['namadepan']}') and upper(namabelakang) = upper('{$record['namabelakang']}') ");
			if($ada > 0){
				$errdb = '<br/>Data Pengarang sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){*/
				$err=Sipus::UpdateBiasa($conn,$record,$p_dbtable,"idauthor",$r_key);

				if($err != 0){
					$errdb = 'Update data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Update data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					//Helper::redirect(); 
				}
			//}						
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,"idauthor",$r_key);
			
			if($err != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	// kondisi filter
	$keyauthor=Helper::removeSpecial($_POST['cariauthor']);
	
	if ($keyauthor!='')
		$p_sqlstr.=" where upper(namadepan||' '||namabelakang) like upper('%$keyauthor%')
		or upper(namadepan) like upper('%$keyauthor%')
		or upper(namabelakang) like upper('%$keyauthor%') ";
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	// eksekusi sql list
	$p_sqlstr.=" order by idauthor";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<? include_once('_notifikasi.php'); ?>
		<div class="filterTable table-responsive">
			<table border="0" width="100%">
				<tr>
					<td align="left">Nama Author : &nbsp;<input type="text" name="cariauthor" id="cariauthor" size="25" value="<?= $_POST['cariauthor'] ?>" onKeyDown="etrCari(event);"></td>
					<td  align="right"><input type="button" value="Filter" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goRefresh();" /></td>
				</tr>
			</table>
		</div><br />
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="<?= $p_tbwidth ?>%" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<tr height="20"> 
				<td width="60" nowrap align="center" class="SubHeaderBGAlt thLeft">Id Author</td>
				<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Depan</td>
				<td width="150" nowrap align="center" class="SubHeaderBGAlt thLeft">Nama Belakang</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft">Badan/Instansi/Editor ?</td>
				<td width="40" nowrap align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>
			<?php
				$i = 0;
				while ($row = $rs->FetchRow()) 
				{
					if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
					if(strcasecmp($row['idauthor'],$p_editkey)) { // row tidak diupdate
					
					if($row['isbadan'] == 1)
						$c_src = 'images/centang.gif';
					else
						$c_src = 'images/uncheck.gif';
			?>
			<tr class="<?= $rowstyle ?>">
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Sunting Data" onclick="goEditIP('<?= $row['idauthor']; ?>');" class="link"><?= $row['idauthor']; ?></u>
				<? } else { ?>
					<?= $row['idauthor']; ?>
				<? } ?>
				</td>
				<td><?= $row['namadepan']; ?></td>
				<td><?= $row['namabelakang']; ?></td>
				<td align="center"><img src="<?= $c_src; ?>" width="16" /></td>
				<td align="center">
				<? if($c_delete) { ?>
					<u title="Hapus Data" onclick="goDeleteIP('<?= $row['idauthor']; ?>','<?= $row['namabelakang']; ?>');" class="link">[hapus]</u>
				<? } ?>
				</td>
			</tr>
			<?php
					} else { // row diupdate
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= $row['idauthor']; ?></td>
				<td align="center"><?= UI::createTextBox('u_namadepan',$row['namadepan'],'ControlStyle',100,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('u_namabelakang',$row['namabelakang'],'ControlStyle',100,30,true,'onKeyDown="etrUpdate(event);"'); ?></td>
				<td align="center"><input <?=($row['isbadan'] ? "checked" : "");?> type="checkbox" name="u_isBadan" id="u_isBadan"></td>
				<td align="center">
				<? if($c_edit) { ?>
					<u title="Update Data" onclick="updateData('<?= $row['idauthor']; ?>');" class="link">[update]</u>
				<? } ?>
				</td>
			</tr>
			<?php 	}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>

			<?php } ?> 
			
			
			<? if($c_add) { ?>
			<tr class="LiteSubHeaderBG"> 
				<td align="center">*</td>
				<td align="center"><?= UI::createTextBox('i_namadepan','','ControlStyle',100,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><?= UI::createTextBox('i_namabelakang','','ControlStyle',100,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
				<td align="center"><input type="checkbox" name="i_isBadan" id="i_isBadan"></td>
				<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
			</tr>
			<?php }  ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="<?= $p_col; ?>"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
				</td>
				
			</tr>
		</table>
            </div>
		<?php require_once('inc_listnav.php'); ?><br>
		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="scroll" id="scroll">
		</form>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_namadepan').focus();
	<? } else { ?>
	document.getElementById('i_namadepan').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_namadepan"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_namadepan")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}

function goRefresh(){
document.getElementById("cariauthor").value='';
goSubmit()
}

</script>
</html>