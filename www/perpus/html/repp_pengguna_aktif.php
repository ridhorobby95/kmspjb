<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	//$a_auth = Helper::checkRoleAuth($conng);
	
	$kepala = Sipus::getHeadPerpus();
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['kdjenispustaka']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	$r_limit = Helper::removeSpecial($_REQUEST['limit']);

	// var_dump($_REQUEST);
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {// or $r_jenis==''
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pengguna Aktif';
	
	$p_namafile = 'rekap pengguna aktif_'.$r_jenis.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	//query ini akan menampilkan jumlah pengguna aktif per kode fakultas/kode jurusan/nama jurusan
	$sql = "select count(distinct ppt.idanggota) as pengguna_aktif,lvj.kdjurusan, lvj.kdfakultas,lvj.namajurusan,lvf.singkatan
		from pp_transaksi ppt 	join pp_eksemplar ppe on ppt.ideksemplar = ppe.ideksemplar
					join ms_anggota msa on msa.idanggota = ppt.idanggota
					join lv_jurusan lvj on lvj.kdjurusan = msa.idunit
					join lv_fakultas lvf on lvf.kdfakultas=lvj.kdfakultas
					join ms_pustaka msp on msp.idpustaka = ppe.idpustaka

		where ppt.tgltransaksi between '$r_tgl1' and '$r_tgl2'
				and
		msp.kdjenispustaka = '$r_jenis'
		group by lvj.kdfakultas, lvj.namajurusan,lvj.kdjurusan,lvf.singkatan order by pengguna_aktif desc ";

	//$conn->debug = false;
	
	// query yang sama dengan tambahan melakukan select untuk 7 teratas jumlah terbanyak pengguna aktif.
	
	$sql_top_pengguna = $sql." limit $r_limit";
	
	//melakukan execute untuk dipakai di grafik.
	$rs = $conn->Execute($sql_top_pengguna);
	$i =0;
	while($row = $rs->FetchRow())
	{
		$a_jurusan[$i] = $row['kdjurusan'];
		$a_jurusan2[$i] = $row['namajurusan'];
		$a_pengguna[$row['kdjurusan']] = $row['pengguna_aktif'];
		$i++;
	}
	
	$_SESSION['_DATA_JURUSAN'] = $a_jurusan;
	$_SESSION['_DATA_JURUSAN2'] = $a_jurusan2;
	$_SESSION['_DATA_PENGGUNA'] = $a_pengguna;
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Laporan Pengguna Aktif<br>Periode : <?= Helper::formatDateInd($r_tgl1) ?> s/d <?= Helper::formatDateInd($r_tgl2) ?></h2>
  	</strong></td>
  </tr>
	
	<? if ($r_format == 'html') { ?>
	<tr>
		<td><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
	</tr>
	<? } ?>
</table>


<table width="600" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>    
        <th width="10" align="center"><strong>Kode Jurusan</strong></th>
        <th width="10" align="center"><strong>Kode Fakultas</strong></th>
        <th align="center"><strong>Nama Fakultas / Jurusan</strong></th>
	<th width="10" align="center"><strong>Jumlah</strong></th>
  </tr>
  
  <!-- melakukan fetch data -->
  <?php
	$rs = $conn->Execute($sql);
	$count = $rs->RowCount();
	if($count >0) {
	$no=1;
	while($row=$rs->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no++ ?></td>
	<td align="center"><?= $row['kdjurusan']?></td>
	<td align="center"><?= $row['kdfakultas'] ?></td>
    <td align="left" nowrap><?= $row['singkatan'].'/'.$row['namajurusan'] ?></td>
	<td align="right"><?= $row['pengguna_aktif']?></td>
	
	<!-- melakukan sum untuk total pengguna aktif -->
	<? $totaljumlah += $row['pengguna_aktif'] ?>
</tr>
	<? } ?>
	
	<!-- untuk menampilkan total pengguna aktif. -->
	<tr>
		<td align="right" colspan="4"><b> Jumlah <b></td>
		<td align="right"><b> <?= $totaljumlah ?></b> </td>
	</tr>
	<? } else { ?>
	<tr>
		<td colspan="5" align="center">Tidak ada data yang sesuai dengan kriteria.</td>
	</tr>
	<? } ?>
</table>
<br><br>
<!-- untuk grafik -->
<? if($count > 0) { ?>
	<? if($r_format == 'html') { ?>
	<table>
		
		 <tr>
		<? if($r_grafik =='batang') { ?>
			<td align="center"> Grafik Batang <?= $r_limit?>  Pengguna Aktif teratas periode <?= Helper::formatDateInd($r_tgl1) ?> sampai dengan <?= Helper::formatDateInd($r_tgl2) ?> <br>
				<img src="<?= Helper::navAddress('img_bar_pengguna_aktif') ?>" />
			</td>
		<? } ?>
		 </tr>
			
		<tr>
		<? if($r_grafik =='pie') { ?>
			<td align="center"> Grafik Pie <?= $r_limit?> Pengguna Aktif teratas periode <?= Helper::formatDateInd($r_tgl1) ?> sampai dengan <?= Helper::formatDateInd($r_tgl2) ?> <br>
				<img src="<?= Helper::navAddress('img_pie_pengguna_aktif') ?>" />
			</td>
		<? } ?>
		</tr>
	</table>
	 <? } ?>
	 <? } ?>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan PJB masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['jabatan'];?> PJB,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>