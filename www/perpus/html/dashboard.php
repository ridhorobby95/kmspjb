<?php
	// defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// include('inc_init.php');
	// include('inc_dbconn.php');
	// include('inc_function.php');
	
	
	// definisi variabel halaman
	$p_window = '[Demo Chart] RGraph';
	$p_title = 'RGraph';
	$p_tbheader = '.: RGraph :.';
	$p_col = 3;
	$p_tbwidth = 430;
	
	$a_versi = array('1' => 'Final', '2' => 'Revisi', '3' => 'Definitif', '4' => 'Indikatif', '5' => 'Backup');
	$l_versi = UI::createSelect('tipe',$a_versi,'','ControlStyle',true,'style="width:156" id="tipe"');
	// $l_tipe = createSelect('tipe',$a_tipe,'','ControlStyle',true,'style="width:156" id="tipe"');
	
	$a_tahun = array('2009' => '2009', '2010' => '2010', '2011' => '2011', '2012' => '2012');
	$l_tahun = UI::createSelect('thang',$a_tahun,'','ControlStyle',true,'style="width:156" id="thang"');
	
	// recordset tahun aktif
	// $sql = "select thang from ms_thnanggaran";
	// $rsta = $conn->Execute($sql);
	// $l_tahun = $rsta->GetMenu2('tahun',$_SESSION['KEU_THANG'],false,false,0,'class= "ControlStyle"');
	
	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="scripts/hchart/highcharts.js"></script>
	<script type="text/javascript" src="scripts/hchart/themes/gray.js"></script>
	
	
	<script src="scripts/rgraph/RGraph.common.core.js"></script>
	<script src="scripts/rgraph/RGraph.common.context.js"></script>   <!-- Just needed for context menus -->
	<script src="scripts/rgraph/RGraph.common.effects.js"></script>   <!-- Just needed for visual effects -->
	<script src="scripts/rgraph/RGraph.common.key.js"></script>       <!-- Just needed for keys -->
	<script src="scripts/rgraph/RGraph.common.resizing.js"></script>  <!-- Just needed for resizing -->
	<script src="scripts/rgraph/RGraph.common.tooltips.js"></script>  <!-- Just needed for tooltips -->
	<script src="scripts/rgraph/RGraph.common.zoom.js"></script>      <!-- Just needed for zoom -->
	<script src="scripts/rgraph/RGraph.common.dynamic.js"></script>   <!-- Just needed for zoom -->
	
	<script src="scripts/rgraph/RGraph.bar.js"></script>              <!-- Just needed for Bar charts -->
	<script src="scripts/rgraph/RGraph.gauge.js"></script>            <!-- Just needed for Gauge charts -->
	<script src="scripts/rgraph/RGraph.hbar.js"></script>             <!-- Just needed for Horizontal Bar charts -->
	<script src="scripts/rgraph/RGraph.pie.js"></script>              <!-- Just needed for Pie AND Donut charts -->
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();">
<?php include('inc_menu.php'); ?>
<? include_once('_notifikasi.php'); ?>
<table width="900" border="0" cellpadding="4" cellspacing=0 style="margin-left:10px">
	<tr>
		<td></td>
	</tr>
	<tr style="float: left; border: 1px solid gray; border-top-left-radius: 10px; border-top-right-radius: 
			10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; opacity: 1; "> 
		<td width="110" nowrap align="left" >
			<canvas id="canvas_kau" width="1320" height="300" >[No canvas support]</canvas>
			<div id="container" style="min-width: 600px; height: 400px; margin: 0 auto"></div>
		</td>
	</tr>
</table><br>
</body>

<script>
window.onload = function ()
	{
		var pie = new RGraph.Pie('canvas_kau', [4,8,4,6,5,3,2,5]);
		pie.Set('chart.tooltips', ['Alvin-10%','Pete-20%','Hoolio-30%','Jack-40%','Kev-50%','Luis-60%','Lou-70%','Jesse-80%']);
		pie.Set('chart.labels', ['Alvin','Pete','Hoolio','Jack','Kev','Luis','Lou','Jesse']);
		// pie.Set('chart.exploded',[15,15,15,15,15,15,15,15]);
		pie.Set('chart.shadow', true);
		pie.Set('chart.shadow.offsetx', 5);
		pie.Set('chart.shadow.offsety', 5);
		pie.Set('chart.radius', 70);
		pie.Set('chart.centerx', 150);
		pie.Set('chart.centery', 170);
		pie.Set('chart.title', 'Komposisi Anggaran per MAK');
		pie.Draw();
		
		var pie_lay = new RGraph.Pie('canvas_kau', [4,8,4,6,5,3,2,5]);
		pie_lay.Set('chart.tooltips', ['Alvin-10%','Pete-20%','Hoolio-30%','Jack-40%','Kev-50%','Luis-60%','Lou-70%','Jesse-80%']);
		pie_lay.Set('chart.labels', ['Alvin','Petes','Hoolio','Jack','Kev','Luis','Lou','Jesse']);
		pie_lay.Set('chart.exploded',[0,15,0,15,15,0,0,0]);
		pie_lay.Set('chart.shadow', true);
		pie_lay.Set('chart.shadow.offsetx', 5);
		pie_lay.Set('chart.shadow.offsety', 5);
		pie_lay.Set('chart.radius', 70);
		pie_lay.Set('chart.centerx',460);
		pie_lay.Set('chart.centery', 170);
		pie_lay.Set('chart.title', 'Belanja per Layanan');
		pie_lay.Draw();
		
		var data = [120,80,60];
		var hbar = new RGraph.HBar('canvas_kau', data);
		hbar.Set('chart.labels', ['Richard', 'Alex', 'Nick']);
        hbar.Set('chart.gutter.left', 680);
        hbar.Set('chart.background.barcolor1', 'white');
        hbar.Set('chart.background.barcolor2', 'white');
        hbar.Set('chart.background.grid', true);
        hbar.Set('chart.colors', ['red']);
		hbar.Set('chart.title', 'Belanja per Kegiatan Renstra');
		hbar.Set('chart.tooltips', ['Richard-120','Alex-80','Nick-60']);  
		hbar.Set('chart.tooltips.event', 'onmousemove');      
		// hbar.Draw();
		// RGraph.Effects.jQuery.Expand(hbar);
        RGraph.Effects.HBar.Grow(hbar);
}

});
</script>
</html>