<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_eksemplar';
	$p_window = '[PJB LIBRARY] Daftar Eksemplar';
	$p_title = 'Daftar Eksemplar';
	$p_tbheader = '.: Daftar Eksemplar Pustaka :.';
	$p_col = 9;
	$p_tbwidth = 100;
	//$p_filedetail = Helper::navAddress('ms_eksemplar.php');
	$p_id = "ms_ekspinjam";
	
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'p.kdjenispustaka';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	$code = Helper::removeSpecial($_GET['code']);
	
	// sql untuk mendapatkan isi list

	$p_sqlstr="select p.judul,p.edisi, e.idpustaka, e.ideksemplar,e.noseri,k.namaklasifikasi, l.namalokasi
		from pp_eksemplar e 
		join ms_pustaka p on p.idpustaka=e.idpustaka
		join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
		join lv_lokasi l on l.kdlokasi = e.kdlokasi 
		where e.kdkondisi='V' and upper(statuseksemplar)=upper('ada') $sqlAdminUnit";

	
	// pengaturan ex
	if (!empty($_POST))
	{
		$keyjudul=Helper::removeSpecial($_POST['carijudul']);
		$keyjudul_exp = explode(' ',$keyjudul);
		$keyjudul_imp = implode('%',$keyjudul_exp); // untuk menambahkan tanda % di sekitar kata kunci agar pencarian lebih baik.
		
		$keyklasifikasi=Helper::removeSpecial($_POST['kdklasifikasi']);
		$keyjenis=Helper::removeSpecial($_POST['kdjenispustaka']);
		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		if($keyjudul!=''){
			$p_sqlstr.=" and upper(p.judul) like upper('%$keyjudul_imp%') ";
		}
		if($keyklasifikasi!=''){
			$p_sqlstr.=" and e.kdklasifikasi='$keyklasifikasi' ";
		}
		if($keyjenis!=''){
			$p_sqlstr.=" and p.kdjenispustaka='$keyjenis' ";
		}
		if($keylokasi!=''){
			$p_sqlstr.=" and e.kdlokasi='$keylokasi' ";
		}
		
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}
	//filter role
	if($keyjenis=='')
	$p_sqlstr .=" and p.kdjenispustaka in (".$_SESSION['roleakses'].")";
	
	// Group by
	//$p_sqlstr .=" group by p.judul, e.idpustaka, e.ideksemplar,e.noseri,e.statuseksemplar,k.namaklasifikasi";
	
	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namaklasifikasi, kdklasifikasi from lv_klasifikasi  order by kdklasifikasi");
	$l_klasifikasi = $rs_cb->GetMenu2('kdklasifikasi',$keyklasifikasi,true,false,0,'id="kdklasifikasi" class="ControlStyle" style="width:120" onchange="goSubmit()"');
	$l_klasifikasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_klasifikasi);
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where kdjenispustaka in(".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$keyjenis,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:120" onchange="goSubmit()"');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);
	
	$rs_lokasi = $conn->Execute("select l.namalokasi, l.kdlokasi from lv_lokasi l where 1=1 $sqlAdminUnit ");
	$l_lokasi = $rs_lokasi->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150" onchange="goFilterEx()" ');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_lokasi);
?>
<html>
<head>
	<title><?= $p_window ?></title>

	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
		<div align="center">
			<form name="perpusform" id="perpusform" method="post" action="">
				<div class="filterTable">
					<table border=0 width="100%" cellpadding="4" cellspacing=0>
						<tr>
						<td width="120">Judul Pustaka</td>
						<td colspan=2>:&nbsp;<input type="text" name="carijudul" id="carijudul" size="50" value="<?= $keyjudul ?>" onKeyDown="etrCari(event);"></td>
						
						</tr>
						<tr>
							<td>Label Pustaka</td>
							<td colspan=2>:&nbsp;<?= $l_klasifikasi ?></td>
							
						</tr>
						<tr>
							<td>Jenis Pustaka</td>
							<td colspan=2>:&nbsp;<?= $l_jenis ?></td>
						</tr>
						<tr>
							<td>Lokasi Eksemplar</td>
							<td>:&nbsp;<?= $l_lokasi ?></td>
							<td valign="top" align="right">
							<input type="button" value="Cari" class="ControlStyle" onclick="goSubmit()">&nbsp;&nbsp;<input type="button" value="Refresh" class="ControlStyle" onclick="goClear();goFilter(false);" />
							</td>
						</tr>
					</table>
				</div><br />
				<header style="width:100%;margin:0 auto;">
					<div class="inner">
						<div class="left title">
							<h1><?= $p_title ?></h1>
						</div>
					</div>
				</header>

				<table width="670" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
					<tr height="20"> 
						<td width="90" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">No.Induk  <?= $p_xsort['noseri']; ?></td>
						<td width="90%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','p.judul:C');">Judul Pustaka  <?= $p_xsort['judul']; ?></td>				
						<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','k.namaklasifikasi:C');">Label<?= $p_xsort['namaklasifikasi']; ?></td>
						<td width="110" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi<?= $p_xsort['namalokasi']; ?></td>
						<td width="30" nowrap align="center" class="SubHeaderBGAlt thLeft">Pilih</td>
						<?php
						$i = 0;
						if($showlist) {
							// mulai iterasi
							while ($row = $rs->FetchRow()) 
							{
								if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
								if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
					?>
					</tr>
					<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
						
						<td>
						<?= $row['noseri']; ?>
						</td>
					
						<td align="left">
						<? if($code=='k') { ?>
						<u title="Pilih Eksemplar" style="cursor:pointer;color:blue" onclick="goSend2('<?= $row['noseri'] ?>')"><?= $row['judul']; ?><?= $row['edisi']!='' ? ' / '.$row['edisi'] : '' ?></u>
						<? } else if($code=='f') { ?>
						<u title="Pilih Eksemplar" style="cursor:pointer;color:blue" onclick="goSend3('<?= $row['noseri'] ?>')"><?= $row['judul']; ?><?= $row['edisi']!='' ? ' / '.$row['edisi'] : '' ?></u>
						<? } else { ?>
						<u title="Pilih Eksemplar" style="cursor:pointer;color:blue" onclick="goSend('<?= $row['noseri'] ?>')"><?= $row['judul']; ?><?= $row['edisi']!='' ? ' / '.$row['edisi'] : '' ?></u>
						<? } ?>
						</td>
						<td align="center"><?= $row['namaklasifikasi']; ?></td>
						<td align="center"><?= $row['namalokasi']; ?></td>
						<td align="center">
						<? if($code=='k') { ?>
						<img title="Pilih Eksemplar" src="images/centang.gif" style="cursor:pointer" onClick="goSend2('<?= $row['noseri'] ?>')">
						<? } else if($code=='f') { ?>
						<img title="Pilih Eksemplar" src="images/centang.gif" style="cursor:pointer" onClick="goSend3('<?= $row['noseri'] ?>')">
						<? } else { ?>
						<img title="Pilih Eksemplar" src="images/centang.gif" style="cursor:pointer" onClick="goSend('<?= $row['noseri'] ?>')">
						<? } ?>
						</td>
		
					</tr>
					<?php
						}
						}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
					</tr>
					<?php } ?>
					<tr> 
						<td class="PagerBG footBG" align="right" colspan="<?= ($p_col/2)+1 ; ?>"> 
							Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
						</td>
						
					</tr>
					
				</table>
				<?php require_once('inc_listnav.php'); ?><br>	

				<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
				<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
				<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
				<input type="hidden" name="key" id="key">
				<input type="hidden" name="key3" id="key3">
				<input type="hidden" name="act" id="act">

				<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
					Filter Criteria <br>
					<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
				</div>

				<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
					<table width=100  class="menu-body">
						<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
							<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
						</tr>
						<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
							<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
						</tr>
						<tr>
							<td class="separator"><div class="separator-line"></div></td>
						</tr>
						<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
							<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
						</tr>
						<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
							<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>



</body>
	
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">

function goClear(){
	document.getElementById("carijudul").value='';
	document.getElementById("kdklasifikasi").value='';
	document.getElementById("kdlokasi").value='';
	//goSubmit();
}

function goSend(key) {
		
		window.opener.document.getElementById("txtpustaka").value = key;
		window.close();
	}
function goSend2(key) {		
		window.opener.document.getElementById("txtpustaka2").value = key;
		window.close();
	}
function goSend3(key) {		
		window.opener.document.getElementById("txtpustakafoto").value = key;
		window.close();
	}
</script>

</html>