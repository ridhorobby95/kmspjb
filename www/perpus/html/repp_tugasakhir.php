<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$conna = Factory::getConnAkad();
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_thnajaran = Helper::removeSpecial($_POST['thnajaran']);
	$r_thnterbit = Helper::removeSpecial($_POST['thnterbit']);
	$r_status = Helper::removeSpecial($_POST['status']);
	
	
	if($r_format=='' or $r_thnajaran=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Eksemplar';
	
	$p_namafile = 'lap_uploadTA_'.$r_thnajaran;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
//die("prett");
	$sql_akad = "select distinct k.periode, k.nim, h.nama, h.kodeunit as idunit, u.namaunit 
		from ak_krs k
		join ak_matakuliah m on m.kodemk = k.kodemk 
		left join ms_mahasiswa h on h.nim = k.nim 
		left join gate.ms_unit u on u.kodeunit = h.kodeunit 
		where m.tipekuliah = 'T' and k.lulus = '-1' 
		and substring(k.periode,1,4) = '$r_thnajaran' and h.kodeunit is not null
		order by k.nim  ";
	$akad = $conna->GetArray($sql_akad);
	$krs = array();
	$nim = array();
	foreach($akad as $row){
		$krs[$row['nim']]['periode'] = $row['periode'];
		$nim[] = $row['nim'];
	}
	
	$sql_upload = "select t.tahunterbit, t.idanggota as nim, a.namaanggota as nama, a.idunit, j.namajurusan as namaunit 
		from pp_uploadta t 
		left join ms_anggota a on a.idanggota = t.idanggota 
		left join lv_jurusan j on j.kdjurusan = a.idunit
		where t.statusvalstafdigitalisasi = 2 and t.tahunterbit = '$r_thnterbit'
		order by t.idanggota "; 
	$upload = $conn->GetArray($sql_upload);
	$tugasakhir = array();
	$angggota = array();
	foreach($upload as $row){
		$tugasakhir[$row['nim']]['isupload'] = true;
		$tugasakhir[$row['nim']]['tahunterbit'] = $row['tahunterbit'];
		$angggota[] = $row['nim'];
	}
	
	$semua = array();
	foreach($akad as $row){
		$semua[$row['nim']]['periode'] = $row['periode'];
		$semua[$row['nim']]['tahunterbit'] = $tugasakhir[$row['nim']]['tahunterbit'];
		$semua[$row['nim']]['isupload'] = ($tugasakhir[$row['nim']]['isupload']? true : false) ;
		$semua[$row['nim']]['nim'] = $row['nim'];
		$semua[$row['nim']]['nama'] = $row['nama'];
		$semua[$row['nim']]['idunit'] = $row['idunit'];
		$semua[$row['nim']]['namaunit'] = $row['namaunit'];
	}
	
	$anggotaupload = array_diff($angggota, $nim); #$angggota yg g ada di $nim;
	$au = implode("','",$anggotaupload);
	$sql_au = "select t.tahunterbit, t.idanggota as nim, a.namaanggota as nama, a.idunit, j.namajurusan as namaunit 
		from pp_uploadta t 
		left join ms_anggota a on a.idanggota = t.idanggota 
		left join lv_jurusan j on j.kdjurusan = a.idunit
		where t.statusvalstafdigitalisasi = 2 and t.tahunterbit = '$r_thnterbit'
		and a.idanggota in ('$au')
		order by t.idanggota ";
	$a_upload = $conn->GetArray($sql_au);
	foreach($a_upload as $row){
		$semua[$row['nim']]['periode'] = $row['periode'];
		$semua[$row['nim']]['tahunterbit'] = $row['tahunterbit'];
		$semua[$row['nim']]['isupload'] = true ;
		$semua[$row['nim']]['nim'] = $row['nim'];
		$semua[$row['nim']]['nama'] = $row['nama'];
		$semua[$row['nim']]['idunit'] = $row['idunit'];
		$semua[$row['nim']]['namaunit'] = $row['namaunit'];
	}
	
	$sudah = array();
	foreach($upload as $row){
		$sudah[$row['nim']]['periode'] = $krs[$row['nim']]['periode'];
		$sudah[$row['nim']]['tahunterbit'] = $row['tahunterbit'];
		$sudah[$row['nim']]['isupload'] = true;
		$sudah[$row['nim']]['nim'] = $row['nim'];
		$sudah[$row['nim']]['nama'] = $row['nama'];
		$sudah[$row['nim']]['idunit'] = $row['idunit'];
		$sudah[$row['nim']]['namaunit'] = $row['namaunit'];
	}
	
	$belum = array();
	$nimbelum = array_diff($nim, $angggota); #$nim yg g ada di $angggota;
	$nb = implode("','",$nimbelum);
	$sql_akad = "select distinct k.periode, k.nim, h.nama, h.kodeunit as idunit, u.namaunit 
		from ak_krs k
		join ak_matakuliah m on m.kodemk = k.kodemk 
		left join ms_mahasiswa h on h.nim = k.nim 
		left join gate.ms_unit u on u.kodeunit = h.kodeunit 
		where m.tipekuliah = 'T' and k.lulus = '-1' 
		and substring(k.periode,1,4) = '$r_thnajaran' and h.kodeunit is not null
		and h.nim in ('$nb')
		order by k.nim  ";
	$akad = $conna->GetArray($sql_akad);
	foreach($akad as $row){
		if($tugasakhir[$row['nim']]['isupload'] == false){ #false
			$belum[$row['nim']]['periode'] = $row['periode'];
			$belum[$row['nim']]['tahunterbit'] = '';
			$belum[$row['nim']]['isupload'] = false ;
			$belum[$row['nim']]['nim'] = $row['nim'];
			$belum[$row['nim']]['nama'] = $row['nama'];
			$belum[$row['nim']]['idunit'] = $row['idunit'];
			$belum[$row['nim']]['namaunit'] = $row['namaunit'];
		}else
			continue;
		
	}
	
	$rows = array();
	if($r_status == "semua"){
		$rows = $semua;
	}elseif($r_status == "belum"){
		$rows = $belum;
	}elseif($r_status == "sudah"){
		$rows = $sudah;
	}
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60 colspan=2><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60>
		<td valign="bottom" colspan=3><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=5><strong>
  	<h2>Laporan Upload Tugas Akhir</h2>
  	</strong></td>
  </tr>
	<tr>
	<td colspan=5><b>Status Upload : <?= $r_status; ?></b></td>
	</tr>
	<tr>
	<td colspan=5><b>Tahun Ajaran : <?= $r_thnajaran.'/'.($r_thnajaran+1); ?></b></td>
	</tr>
	<tr>
	<td colspan=5><b>Tahun Terbit : <?= $r_thnterbit; ?></b></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="5%" align="center"><strong>No.</strong></th>
	<th width="15%" align="center"><strong>Id Anggota</strong></th>
	<th width="30%" align="center"><strong>Nama</strong></th>
	<th width="40%" align="center"><strong>Jurusan</strong></th>
	<th width="10%" align="center"><strong>Status</strong></th>
  </tr>
  <?php
	$no=0;
	ksort($rows);
	foreach($rows as $rs) 
	{  $no ++;?>
    <tr height=25>
		<td align="center"><?= $no ?></td>
		<td align="left"><?= $rs['nim'] ?></td>
		<td align="left"><?= $rs['nama'] ?></td>
		<td align="left"><?= $rs['namaunit'] ?></td>
		<td align="left"><?= ($rs['isupload']? "sudah" : "belum") ?></td>
	</tr>
	<? } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=5 >Tidak ada data</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan=5><b>Jumlah : <?= count($rows) ?></b></td>
	</tr>
   
</table>


</div>
</body>
</html>