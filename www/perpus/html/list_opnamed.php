<?php
	//$conn->debug = true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	// definisi variabel halaman
	$p_dbtable = 'v_eksopname';
	$p_window = '[PJB LIBRARY] Daftar Opname Eksemplar';
	$p_title = 'Daftar Opname Eksemplar';
	$p_tbheader = '.: Daftar Opname Eksemplar :.';
	$p_col = 9;
	$p_tbwidth = 978;
	$p_filelist = Helper::navAddress('list_opname.php');
	$p_fileopname = Helper::navAddress('spesial_opname.php');//Helper::navAddress('eks_opname.php');
	$p_id = "list_op";
	
	$r_key=Helper::removeSpecial($_POST['key']);
	if($r_key==''){
		header('Location: '.$p_filelist);
		exit();
	}
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'p.tglopname';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	//$nomer=1;
	
	$p_title .= " ".$conn->GetOne("select namaopname from pp_opname where idopname = '$r_key'");
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="select p.*,m.judul,(e.noseri) as regcomp, k.namakondisi as namakondisiawal,c.namakondisi as namakondisiopname, e.kdlokasi, l.namalokasi
			   from pp_detailopname p
			   left join lv_kondisi k on p.kondisiawal=k.kdkondisi
			   left join lv_kondisi c on p.kondisiopname=c.kdkondisi
			   left join pp_eksemplar e on p.ideksemplar=e.ideksemplar
			   left join ms_pustaka m on e.idpustaka=m.idpustaka
			   left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			   where p.idopname=$r_key $sqlAdminUnit";

	// pengaturan ex
	if (!empty($_POST))
	{
		$keylokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$keykondisi=Helper::removeSpecial($_POST['kdkondisi']);	
				
		if ($keylokasi!='')
			$p_sqlstr.=" and e.kdlokasi = '$keylokasi'";
			if ($keykondisi!='')
				$p_sqlstr.=" and p.kondisiopname = '$keykondisi'";
	
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc"; 
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	$rsc=$conn->Execute($p_sqlstr)->RowCount();
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
		$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
		$l_lokasi = $rs_cb->GetMenu2('kdlokasi',$keylokasi,true,false,0,'id="kdlokasi" class="ControlStyle" style="width:150" onchange="goSubmit();"');
		
		$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
		$l_kondisi = $rs_cb->GetMenu2('kdkondisi',$keykondisi,true,false,0,'id="kdkondisi" class="ControlStyle" style="width:150" onchange="goSubmit();"');
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<table cellpadding="0" cellspacing="4" border="0" width="40%">
				<tr>
					<? if($c_add) { ?>
					<td width="170" align="center" style="border:0 none;">
					<a href="<?= $p_filelist; ?>" class="button"><span class="list">
					Daftar Opname</span></a></td>
					<? } ?>
					<td class="thLeft" style="border:0 none;" width="170" align="center">
					<a href="javascript:goClear();goFilter(false)" class="buttonshort"><span class="refresh">
					Refresh</span></a></td>
				</tr>
			</table>
			<div class="filterTable">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" class="GridStyle" style="border:0 none;">
				<tr>
					<td class="thLeft" style="border:0 none;" width="120">Lokasi</td>
					<td class="thLeft" style="border:0 none;" width="40%">: <?= $l_lokasi ?> 
					<img src="images/popup.png" style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi&code=op',470,450);">
					<u style="cursor:pointer;" title="Lihat Lokasi" onClick="popup('index.php?page=pop_lokasi&code=op',470,450);">Pilih Lokasi... </u>
					</td>
					<td class="thLeft" style="border:0 none;">&nbsp</td>
					</tr>
				<tr>
					<td class="thLeft" style="border:0 none;">Kondisi Teropname</td>
					<td class="thLeft" style="border:0 none;">: <?= $l_kondisi ?></td>

					<!--td class="thLeft" style="border:0 none;" align="right"><a href="javascript:goSubmit()" class="buttonshort" style="float:right;"><span class="filter" >Filter</span></a></td-->
					<td class="thLeft" style="border:0 none;" align="right"><input style="float:right;" type="button" value="Filter" class="ControlStyle" onclick="goFilterEx()"></td>		
				</tr>
				</table>
			</div>
			<br />
				<header style="width:100%;margin:0 auto;">
					<div class="inner">
						<div class="left title">
							<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
							<h1><?= $p_title ?></h1>
						</div>
						<? if ($r_key!='') { ?>
						<div class="right">
							<div class="addButton" style="float:left; margin:right:10px;"  title="tambah stock opname" onClick="goHalDetail('<?= $p_fileopname ?>','<?= $r_key ?>');"><img src="images/add.png" width=15 height=15></div>
						</div>
						<? } ?>
						
					</div>
				</header>
			<table width="100%" border="0" cellpadding="0" cellspacing=0 class="GridStyle">

			<!--tr height="30">
				<td align="center" class="PageTitle" colspan="<?//= $p_col ?>"><?//= $p_title; ?></td>
			</tr>
			<tr>
				<td width="100%"  colspan="<?//= $p_col ?>" align="center" style="border:0 none;">
					<table class="GridStyle" cellpadding="0" cellspacing="0" border="0" width="100%" style="border:0 none;">
						<tr>
							<?// if($c_add) { ?>
							<td width="170" align="center" style="border:0 none;">
							<a href="<?//= $p_filelist; ?>" class="button"><span class="list">
							Daftar Opname</span></a></td>
							<?// } ?>
							<td class="thLeft" style="border:0 none;" width="170" align="center">
							<a href="javascript:goClear();goFilter(false)" class="buttonshort"><span class="refresh">
							Refresh</span></a></td>
							<td class="thLeft" align="right" valign="middle" style="border:0 none;">
							<div style="float:right;">
							<img title="first" id="firstButton" onClick="goFirst(<?//= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
							<img title="previous" id="prevButton" onClick="goPrev(<?//= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
							<img title="next" id="nextButton" onClick="goNext(<?//= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
							<img title="last" id="lastButton" onClick="goLast(<?//= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
							</div>
							</td>
						</tr>
					</table>
				</td>	
			</tr-->		
		<!--tr>
			<td colspan="6">
			</td>
		</tr-->
			<tr height="20">
				<td width="80" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.noseri:C');">NO INDUK  <?= $p_xsort['noseri']; ?></td>		
				<td width="40%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','m.judul:C');">Judul Pustaka <?= $p_xsort['judul']; ?></td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','k.namakondisiawal:C');">Kondisi Awal<?= $p_xsort['namakondisiawal']; ?></td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','c.namakondisiopname:C');">Kondisi Teropname<?= $p_xsort['namakondisiopname']; ?></td>
				<td width="35%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','l.namalokasi:C');">Lokasi<?= $p_xsort['namalokasi']; ?></td>
				<td width="15%" nowrap align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" onClick="PopupMenu('popPaging','e.tglopname:D');">Tanggal Opname<?= $p_xsort['tglopname']; ?></td>

				<?php
				$i = 0;

				if($showlist) {
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['ideksemplar'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="25" valign="middle"> 
				<td align="left">
				<?= $row['regcomp']; ?>
				</td>
				<td align="left"><?= $row['judul']; ?></td>
				<td ><?= $row['namakondisiawal']; ?></td>
				<td ><?= $row['namakondisiopname']; ?></td>
				<td align="left"><?= $row['namalokasi']; ?></td>
				<td align="center"><?= Helper::tglEng($row['tglopname']); ?></td>
				
			</tr>
			<?php
				}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr> 
				<td class="PagerBG footBG" align="right" colspan="6" style="padding:5px;"> 
					Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?> --- <?= Helper::formatNumber($rsc)?> Record
				</td>
				
			</tr>
		</table>
		<?php require_once('inc_listnav.php'); ?><br>

		<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
		<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="act" id="act">


		<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
			Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
		</div>



		<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
		<table width=100  class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>

		</form>
		</div>
	</div>
</div>



</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;



</script>

<script type="text/javascript">
function goClear(){
document.getElementById("kdlokasi").value='';
document.getElementById("kdkondisi").value='';
//goSubmit();
}
</script>
</html>