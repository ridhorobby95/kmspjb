<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$kepala = Sipus::getHeadPerpus();
	$sekarang = date('d F Y');
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tahun = Helper::removeSpecial($_REQUEST['tahun']);
	$r_grafik = Helper::removeSpecial($_REQUEST['grafik']);
	// $conn->debug=true;
	if($r_format=='' or $r_tahun=='') {
		header("location: index.php?page=home");
	}

	// definisi variabel halaman
	$p_window = '[UNUSA LIBRARY] Laporan Statistik Koleksi Terpinjam di Perpustakaan UNUSA <br>Tahun '.$r_tahun ;
	
	$p_namafile = 'rekap_buku_dipinjam_'.$r_tahun;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select count(t.ideksemplar) jmleksemplar, count(distinct e.idpustaka) jmlpustaka, to_char(t.tgltransaksi, 'mm') bulan, j.namasatker, a.idunit 
		from pp_transaksi t 
		join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
		join ms_anggota a on a.idanggota = t.idanggota 
		left join ms_satker j on j.kdsatker = a.idunit 
		where to_char(t.tgltransaksi, 'YYYY') = '$r_tahun'
		group by to_char(t.tgltransaksi, 'mm'), j.namasatker, a.idunit
		order by a.idunit, j.namasatker, bulan asc";
	//$conn->debug = true;
	$rs = $conn->Execute($sql);

	$bulan = array();
	$bulan[1] = "JAN";
	$bulan[2] = "FEB";
	$bulan[3] = "MAR";
	$bulan[4] = "APR";
	$bulan[5] = "MEI";
	$bulan[6] = "JUN";
	$bulan[7] = "JUL";
	$bulan[8] = "AGTS";
	$bulan[9] = "SEP";
	$bulan[10] = "OKT";
	$bulan[11] = "NOV";
	$bulan[12] = "DES";
	
	$data = array();
	while($row = $rs->FetchRow()){
		$data['total']['jumlah']['bulaneks'][intval($row['bulan'])] = $data['total']['jumlah']['bulaneks'][intval($row['bulan'])] + $row['jmleksemplar'];
		$data['total']['jumlah']['bulanjdl'][intval($row['bulan'])] = $data['total']['jumlah']['bulanjdl'][intval($row['bulan'])] + $row['jmlpustaka'];
		if($jur == ($row['idunit']?$row['idunit']:"Tidak Ada Unit")){
			$data['jurusan'][$jur][intval($row['bulan'])]['eks'] = $row['jmleksemplar'];
			$data['jurusan'][$jur][intval($row['bulan'])]['jdl'] = $row['jmlpustaka'];
			$data['jurusan'][$jur]['13']['eks'] = $data['jurusan'][$jur]['13']['eks'] + $row['jmleksemplar'];
			$data['jurusan'][$jur]['13']['jdl'] = $data['jurusan'][$jur]['13']['jdl'] + $row['jmlpustaka'];
		}else{
			$jur = ($row['idunit']?$row['idunit']:"Tidak Ada Unit");
			$data['jurusan'][$jur]['satker'] = ($row['namasatker']?$row['namasatker']:"Kosong");
			$data['jurusan'][$jur][intval($row['bulan'])]['eks'] = $row['jmleksemplar'];
			$data['jurusan'][$jur][intval($row['bulan'])]['jdl'] = $row['jmlpustaka'];
			$data['jurusan'][$jur]['13']['eks'] = $row['jmleksemplar'];
			$data['jurusan'][$jur]['13']['jdl'] = $row['jmlpustaka'];
		}
		
		if($bul){
			if(intval($row['bulan']) > $bul){
				$bul = intval($row['bulan']);
				$data['jumlahbulan'] = $bul;
			}else{
				continue;
			}
		}else{
			$bul = intval($row['bulan']);
			$data['jumlahbulan'] = $bul;
		}
		
		$data['total']['jumlah']['bulaneks']['13'] = $data['total']['jumlah']['bulaneks']['13'] + $data['jurusan'][$jur]['13']['eks'];
		$data['total']['jumlah']['bulanjdl']['13'] = $data['total']['jumlah']['bulanjdl']['13'] + $data['jurusan'][$jur]['13']['jdl'];
	}
//print_r($data);die();
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>UNIVERSITAS NAHDLATUL ULAMA SURABAYA</h3></td>
	</tr>
</table>
<br><br>
<table width=800 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Statistik Koleksi Terpinjam di Perpustakaan Universitas Nahdlatul Ulama<br>Tahun <?= $r_tahun; ?></h2>
  	</strong></td>
  </tr>
</table>
<table width="1000" cellspacing="0" cellpadding="4">
	<? if ($r_format == 'html') { ?>
		<tr>
			<td align="left"><a href="javascript:window.print()"><img title='Print Laporan' src='images/printer.gif'></a></td>
		</tr>
	<? } ?>
</table>
<table width="1150" cellspacing="0" cellpadding="4" class="GridStyle" border="1">
	<!-- <tr><td class="SubHeaderBGAlt" colspan=2 align="center">Laporan Buku yang Dipinjam</td></tr> -->
	
	<tr>
		<th width="20%" rowspan="3">Jurusan</th>
		<th colspan="<?= 12*2?>" align="center"> BULAN </th>
		<th colspan="2" rowspan="2"> JUMLAH </th>
		<th colspan="2" rowspan="2"> RATA-RATA </th>
	</tr>
	
	<!-- membuat kolom bulan sesuai semester beserta kolom judul/eksemplar -->
	<tr height="25">
		<? for ($i=1;$i<=12;$i++){?>
			<th colspan="2" width="130" align="center"><strong> <?= $bulan[$i] ?> </strong></th>
		<?}?>
	</tr>
	
	<tr height="25">
		<? for($i=1; $i<=12+1; $i++) { ?>
		<th>EKS</th>
		<th>JDL</th>
		<? } ?>
		<th>EKS</th>
		<th>JDL</th>
	</tr>
	
	<!-- mengisikan jumlah peminjam untuk semester ganjil --> 
	<? foreach($data['jurusan'] as $jur => $value){?>
	<tr height="25">
		<td><?= $jur." - ".$data['jurusan'][$jur]['satker']; ?></td>
		<? for($i=1; $i<=12+1; $i++) { ?>
		<td><?= number_format($data['jurusan'][$jur][$i]['eks'],0, ',', '.'); ?></td>
		<td><?= number_format($data['jurusan'][$jur][$i]['jdl'],0, ',', '.'); ?></td>
		<? } ?>
		<!--<td><?= number_format(($data['jurusan'][$jur][13]['eks']/$data['jumlahbulan']),2, ',', '.'); ?></td>-->
		<!--<td><?= number_format(($data['jurusan'][$jur][13]['jdl']/$data['jumlahbulan']),2, ',', '.'); ?></td>-->
	</tr>
	<? } ?>
<? //var_dump($data['total']); ?>	
	<!-- untuk SUM per Bulan tiap Judul dan Eksemplar -->
	<? foreach($data['total'] as $total => $val){?>
	<tr>
		<td align="right"><b>Jumlah</b> </td>
		<? for($i=1; $i<=12+1; $i++){?> 
		<td><?= number_format($data['total']['jumlah']['bulaneks'][$i],0, ',', '.'); ?></td>
		<td><?= number_format($data['total']['jumlah']['bulanjdl'][$i],0, ',', '.'); ?></td>
		<? } ?>
		<!--<td><?= number_format(($data['total']['jumlah']['bulaneks'][13]/$data['jumlahbulan']),2, ',', '.'); ?></td>-->
		<!--<td><?= number_format(($data['total']['jumlah']['bulanjdl'][13]/$data['jumlahbulan']),2, ',', '.'); ?></td>-->
	</tr>
	<? } ?>
</table>
<br><br>
<br><br><br>
<!-- tambahan, perlu diingat nama kepala perpustakaan UNUSA masih STATIS. -->
<table width="800" border=0 >
	<tr><td><?= str_repeat("&nbsp;", 150)?>Surabaya,  <?= $sekarang ?></td></tr>
	<tr><td><?= str_repeat("&nbsp;", 150)?><b>Kabag Perpustakaan UNUSA,</b></td></tr>
	<tr height="100" valign="bottom"><td><?= str_repeat("&nbsp;", 150)?><b><?=$kepala['namalengkap'];?><br><?= str_repeat("&nbsp;", 150)?>NIP. <?=$kepala['nik'];?></b></td></tr>
</table>
</div>
</body>
</html>