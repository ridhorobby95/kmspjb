<?php
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
$conn->debug =  true;	
	require_once('../includes/init.php');
	
	$sqlstr = "select a.namaanggota, a.idanggota, count(t.idtransaksi) as tagih 
		from ms_anggota a 
		left join pp_transaksi t on a.idanggota = t.idanggota 
		where  to_date(TO_CHAR(ADD_MONTHS(t.tgltenggat,1),'dd-mm-yyyy'),'dd-mm-yyyy') < to_date(TO_CHAR(current_date,'dd-mm-yyyy'),'dd-mm-yyyy') 
			and t.tglpengembalian is null and t.kdjenistransaksi='PJN'
		group by a.idanggota,a.namaanggota";
	$rs = $conn->Execute($sqlstr);
		
	while ($row = $rs->FetchRow()){
		
		$sqlstr = "select *
			from v_srttagih
			where idanggota='$row[idanggota]' and flag=1  
			order by tgltagihan desc limit 1";	
		$rows = $conn->GetRow($sqlstr);
		
		if($rows['tagihanke']<5){
			$now = date('Y-m-d');
		
			$record = array();
			$record['idanggota'] = $row['idanggota'];
			$record['tgltagihan'] = $now;
			$record['namaanggota'] = Helper::removeSpecial($row['namaanggota']);
			$record['t_user'] = 'cron';
			$record['t_updatetime'] = $now;
			$record['t_host'] = Helper::removeSpecial($_SERVER['REMOTE_ADDR']);	
			
			$oke=false;
			if (!empty($rows['tgltagihan'])){
				if($rows['tglexpired']<=$now){
					//penentuan tanggal 2 minggu dari tglexpired
					$c_tgl = (int)strtotime($rows['tglexpired']);
					$record['tglexpired'] = date('Y-m-d',($c_tgl + (86400 * 14))); //2 minggu selanjutnya
					$record['tagihanke'] = $rows['tagihanke'] + 1;
					$col = $conn->Execute("select * from pp_tagihan where 1=-1");
					$sql = $conn->GetInsertSQL($col,$record,true);
					$conn->Execute($sql);
					
					$oke=true;
				}
			}else{	
				$record['tagihanke'] = 1;
				$record['tglexpired'] =  date('Y-m-d',(int)strtotime($now) + (86400 * 14));
				$col = $conn->Execute("select * from pp_tagihan where 1=-1");
				$sql = $conn->GetInsertSQL($col,$record,true);
				$conn->Execute($sql);
				$oke=true;
			}
			
			if($oke==true){
				$iddetail = $conn->GetOne("select max(idtagihan) from pp_tagihan");
				
				//seleksi untuk mengisi detail tagihan
				$sql = "select t.*,p.*,e.*,e.ideksemplar as ideks from pp_transaksi t
						left join pp_eksemplar e on e.ideksemplar=t.ideksemplar
						left join ms_pustaka p on p.idpustaka=e.idpustaka
						where idanggota='$row[idanggota]' and statustransaksi='1' and tglpengembalian is null
						and kdjenistransaksi='PJN'";
				$rss = $conn->Execute($sql);
				
				$recdetail = array();	
				while ($col = $rss->FetchRow()){
					$recdetail['idtagihan'] = $iddetail;
					$recdetail['idpustaka'] = $col['idpustaka'];
					$recdetail['judul'] = Helper::removeSpecial($col['judul']);
					$recdetail['authorfirst1'] = Helper::removeSpecial($col['authorfirst1']);
					$recdetail['authorlast1'] = Helper::removeSpecial($col['authorlast1']);
					$recdetail['authorfirst2'] = Helper::removeSpecial($col['authorfirst2']);
					$recdetail['authorlast2'] = Helper::removeSpecial($col['authorlast2']);
					$recdetail['authorfirst3'] = Helper::removeSpecial($col['authorfirst3']);
					$recdetail['authorlast3'] = Helper::removeSpecial($col['authorlast3']);
					$recdetail['ideksemplar'] = $col['ideks'];
					$recdetail['noseri'] = $col['noseri'];
					$recdetail['nopanggil'] = Helper::removeSpecial($col['nopanggil']);
					$recdetail['idtransaksi'] = $col['idtransaksi'];
					$recdetail['kdjenistransaksi'] = $col['kdjenistransaksi'];
					$recdetail['tgltransaksi'] = $col['tgltransaksi'];
					$recdetail['tgltenggat'] = $col['tgltenggat'];
					
					$col = $conn->Execute("select * from pp_tagihandetail where 1=-1");
					$sql = $conn->GetInsertSQL($col,$recdetail,true);
					$conn->Execute($sql);
				}
			}
		}
	}
	
?>
