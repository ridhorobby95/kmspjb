<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	if(!$isAdminPusat){	
		$sqlAdminUnit = " and o.idunit in ($units) ";
		$l_sqlAdminUnit = " and l.idunit in ($units) ";
	}else{
		$c_delete = false;
		$c_edit = false;
		$c_readlist = false;
	}
	
	// variabel esensial
	$r_key = Helper::removeSpecial($_POST['key']);
	
	// definisi variabel halaman
	$p_dbtable = 'pp_opname';
	$p_window = '[PJB LIBRARY] Sirkulasi Opname Pustaka';
	$p_title = 'Sirkulasi Opname Pustaka';
	$p_tbwidth = 900;
	$p_filelist = Helper::navAddress('list_opname.php');
	$p_fileopname = Helper::navAddress('spesial_opname.php');
	$p_fileopnamed = Helper::navAddress('list_opnamed.php');

	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2=Helper::removeSpecial($_POST['key2']);
		$lokasi=Helper::removeSpecial($_POST['kdlokasi']);
		$kondisi=Helper::removeSpecial($_POST['kdkondisi']);
		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$record['namaopname'] = Helper::removeSpecial($_POST['namaopname']);
			$record['idunit'] = $idunit;
			Helper::Identitas($record);
			
			$check = $conn->GetOne("select count(*) from pp_opname where namaopname = '".$record['namaopname']."' ");
			if($check == 0){
				if($r_key == '') { // insert record	
					Sipus::InsertBiasa($conn,$record,$p_dbtable);
					if($conn->ErrorNo() != 0){
						$errdb = 'Penyimpanan data gagal.';
						Helper::setFlashData('errdb', $errdb);
					}else{
						$sucdb = 'Penyimpanan data berhasil.';
						Helper::setFlashData('sucdb', $sucdb);
					}
				}
				else { // update record
					Sipus::UpdateBiasa($conn,$record,$p_dbtable,idopname,$r_key);
					if($conn->ErrorNo() != 0){
						$errdb = 'Update data gagal.';
						Helper::setFlashData('errdb', $errdb);
					}else{
						$sucdb = 'Update data berhasil.';
						Helper::setFlashData('sucdb', $sucdb);
					}
				}
				
				if($conn->ErrorNo() == 0) {
					if($r_key == '')
						$r_key = $conn->GetOne("select max(idopname) from pp_opname");
				}
				else {
					$row = $_POST; // direstore
					$p_errdb = true;
	
				}
			}else{
				$errdb = 'Data Sudah ada.';
				Helper::setFlashData('errdb', $errdb);
			}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_delsql = "delete from $p_dbtable where idopname = '$r_key'";
			$ok = $conn->Execute($p_delsql);
			if($ok) {
				header('Location: '.$p_filelist);
				exit();
			}
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Update data gagal.';
				Helper::setFlashData('errdb', $errdb);
			}else{
				$sucdb = 'Update data berhasil.';
				Helper::setFlashData('sucdb', $sucdb);
			}
		}
		else if($r_aksi=='batalopname' and $c_delete) {
			$record['kdkondisi']='V';
			$record['idopname']=null;
			$record['tglopname']=null;
			Sipus::UpdateBiasa($conn,$record,pp_eksemplar,ideksemplar,$r_key2);
		}
		else if($r_aksi=='mulaiopname' and $c_edit){

			$SQLOpname=$conn->Execute("select e.ideksemplar, p.judul, k.namakondisi from pp_eksemplar e join ms_pustaka p on e.idpustaka=p.idpustaka
										join lv_kondisi k on e.kdkondisi=k.kdkondisi where e.kdlokasi='$lokasi' and e.kdkondisi='$kondisi'");
			$rs_cb = $conn->Execute("select namakondisi, kdkondisi from lv_kondisi order by kdkondisi");
			$l_kondisi2 = $rs_cb->GetMenu2('kdkondisi2','',true,false,0,'id="kdkondisi2" class="ControlStyle" style="width:150"');
		}
	}
	if(!$p_errdb) {
		if ($r_key !='') {
		$p_sqlstr = "select * from $p_dbtable 
					where idopname = $r_key";
		$row = $conn->GetRow($p_sqlstr);		
		}
		
		$yn = intval(date('Y'));
		$ym = intval($yn)-5;
		$year = array();
		for($i=$ym;$i<=$yn;$i++){
			$year[$i] = $i;
		}
		$l_year = UI::createSelect('namaopname',$year,($r_key!=''? $row['namaopname'] : $yn ),'ControlStyle');
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('idopname').focus()">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		
		<div align="center">
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Opname </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<? } ?>
			</tr>
		</table>
		<br/>
		<?php include_once('_notifikasi.php'); ?>
		<header style="width:<?= $p_tbwidth ?>;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
				<? if ($r_key!='') { ?>
				<div class="right">
					<div class="addButton" style="float:left; margin:right:10px;"  title="tambah stock opname" onClick="goHalDetail('<?= $p_fileopname ?>','<?= $r_key ?>');"><img src="images/add.png" width=15 height=15></div>
					<div class="addButton" style="float:left; margin:right:10px;"  title="Daftar Stock Opname" onClick="goHalDetail('<?= $p_fileopnamed ?>','<?= $r_key ?>')"><img src="images/edited.gif" width=15 height=15></div>
				</div>
				<? } ?>
			</div>
		</header>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="4" class="GridStyle">
			<tr> 
				<td width="35%" class="LeftColumnBG thLeft">Tahun Opname *</td>
				<td class="RightColumnBG"><?= $l_year;//UI::createTextBox('namaopname',$row['namaopname'],'ControlStyle',100,50,$c_edit); ?></td>
			</tr>	
			<tr>
				<td colspan="2" class="FootBG">&nbsp;</td>
			</tr>
		</table>


		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">

		</form>
		</div>
	</div>
</div>
</body>

<script language="javascript">

function saveData() {
	if(cfHighlight("namaopname"))
		goSave();
}

</script>
</html>

	
	
