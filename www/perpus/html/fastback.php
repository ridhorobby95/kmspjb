<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	session_start();
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_transaksi';
	$p_window = '[PJB LIBRARY] Sirkulasi Pengembalian Pustaka';
	$p_title = 'SIRKULASI PENGEMBALIAN';
	$p_tbheader = '.: SIRKULASI PENGEMBALIAN :.';
	$p_col = 9;
	$p_tbwidth = 100;
	
	if (!empty($_POST)){
		$noseri=Helper::removeSpecial($_POST['txtcari']);
		$r_key=Helper::removeSpecial($_POST['key']);
		$r_ideks=Helper::removeSpecial($_POST['ideks']);
		
		if($r_key == "kembali"){
			if($noseri!=''){
				$noseri2 = Helper::RegNew($noseri);
				$p_eks=$conn->GetRow("select t.ideksemplar, t.idtransaksi, l.idunit   
						from pp_transaksi t 
						join pp_eksemplar e on t.ideksemplar=e.ideksemplar
						left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
						where (upper(e.noseri)=upper('$noseri') or upper(e.noseri)=upper('$noseri2') )
							and t.tglpengembalian is null and t.statustransaksi='1' ");
				$key=$p_eks['ideksemplar'];
				if($key){
					if($p_eks['idunit'] != $idunit){
						echo "Buku bukan berasal dari unit $unitlogin.";
					}else{
						$hariini=date('Y-m-d H:i:s');//date("Y-m-d");
						$err=Sipus::fastback($conn,$noseri,$hariini,$key,false);
						if($err != 0){
							$errdb = 'Pengembalian Pustaka gagal.';	
							Helper::setFlashData('errdb', $errdb);
						}
						else {
							$info = Sipus::getDataPeminjaman($conn,$_SESSION['idtrans']);
							$tgl= date('Y-m-d');
							if($_SESSION['reserve'] !=''){				
								$sucdb = "Pengembalian peminjaman berhasil. <u style='cursor:pointer;color:red' onClick=\"popup('index.php?page=cetak_nota&id=$_SESSION[idtrans]&date=".$tgl."&code=t',450,400);\">CETAK NOTA TRANSAKSI</u>. Pustaka Telah direservasi !. <u id='cetak' style='color:red;cursor:pointer' onclick=\"popup('index.php?page=nota_reserve&id=$_SESSION[reserve]&time=$_SESSION[wmulai]',450,400)\"><b>CETAK NOTA RESERVASI</b></u>";
							}else{
								$sucdb = "Pengembalian peminjaman berhasil. <u style='cursor:pointer;color:red' onClick=\"popup('index.php?page=cetak_nota&id=$_SESSION[idtrans]&date=".$tgl."&code=t',450,400);\">CETAK NOTA TRANSAKSI</u>";	
								$sucdb .= $info;	
							}
							Helper::setFlashData('sucdb', $sucdb);
							Helper::redirect();
						}
					}
				}else{
					$errdb = 'Data Peminjaman Pustaka tidak ada.';	
					Helper::setFlashData('errdb', $errdb);
				}
			}
		}
		
		elseif ($r_key=='freeskors') { // selanjutnya akan dikembangkan dengan sistem cicilan :D
			$r_nilai=Helper::removeSpecial($_POST['nilai']);
			$r_anggota=Helper::removeSpecial($_POST['anggota']);
			$r_bayardenda=Helper::removeSpecial($_POST['bayar']);
			if(!$r_bayardenda)
				$r_bayardenda = $conn->GetOne("select denda from ms_anggota where idanggota = '$r_anggota' ");
				
			$recbebas=array();
			$recbayar=array();
			if($r_nilai)
				$recbebas['denda']=$r_nilai;
			else
				$recbebas['denda']=null;
				
			$err=Sipus::UpdateBiasa($conn,$recbebas,ms_anggota,idanggota,$r_anggota);
			
			$recordbayar = array();
			$recordbayar['idanggota']=$r_anggota;
			$recordbayar['tglbayar']=date('Y-m-d');
			$recordbayar['nilai']=$r_bayardenda;
			Helper::Identitas($recordbayar);
			$err=Sipus::InsertBiasa($conn,$recordbayar,pp_bayardenda);
			
			//cari transaksi yang mana untuk memberikan tgl bayar
			$sql_transaksi = $conn->Execute("select * 
							from pp_denda d left join pp_transaksi t on d.idtransaksi=t.idtransaksi
									where d.tglbayar is null and (statustransaksi='0' or statustransaksi='2') and d.idanggota='$r_anggota'");
			$recbayar['tglbayar'] = date("Y-m-d");
			while($row_bayar = $sql_transaksi->FetchRow()){
				$recbayar['petugasbayar'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
				$recbayar['hostbayar'] = Helper::cStrNull($_SERVER['REMOTE_ADDR']);
				$recbayar['timesbayar'] = date('d-m-Y H:i:s');
				$err=Sipus::UpdateBiasa($conn,$recbayar,pp_denda,idtransaksi,$row_bayar['idtransaksi']);
			}
			
			if($err != 0){
				$errdb = 'Pembayaran denda gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				
				$sucdb = 'Pembayaran denda berhasil.';	
				$sucdb .= Sipus::getDataPeminjaman($conn,$_SESSION['idtrans']);

				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();				
			}
		}	
		
	}

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
		<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div class="LeftRibbon">SIRKULASI PENGEMBALIAN CEPAT</div>
		<div align="center">
		<form name="perpusform" id="perpusform" method="post">
            <div class="table-responsive">
			<table width="450" border=0 align="center" cellpadding="4" cellspacing=0 class="filterTable">
			  <tr height="50">
				<td valign="bottom" width="170"><h3>Masukkan Nomor Seri</h3></td>
				<td>
					<input type="text" name="txtcari" id="txtcari" size="15" maxlength="15" class="ControlStyleT" value="<?= $noseri ?>" onKeyDown="etrTrans(event);">
					
				</td>
			  </tr>
			  <tr  height="35">
				<td colspan="3" align="center">
				<input type="button" name="btntrans" id="btntrans" value="Kembali" onclick="goCheck()" class="buttonSmall" style="cursor:pointer;height:23px;width:130px;font-size:12px;padding-bottom:3px;">
				</td>
			 </tr
			</table>
                </div><br>
			<center><? include_once('_notifikasi_trans.php'); ?></center>

		<input type="hidden" name="key" id="key">
		<input type="hidden" name="ideks" id="ideks">
		<input type="hidden" name="nilai" id="nilai">
		<input type="hidden" name="anggota" id="anggota">

		<input type="text" name="xxx" id="xxx" style="visibility:hidden">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function initPage() {
		document.getElementById('txtcari').focus();
}
function goCheck(){
	var cek = document.getElementById("txtcari").value;
	if(cek!=''){
		document.getElementById("key").value='kembali';
		goSubmit();
	}else{
		alert('Isikan NO INDUK dengan benar')
		document.getElementById("txtcari").focus();
	}
}

function etrTrans(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById("btntrans").click();
}

function goFreeSkors(key,denda) {
	var free = confirm("Apakah anda yakin akan membayarkan denda Id Anggota "+key+" ?");
	if(free) {
		byr = document.getElementById("bayar").value;
		if(parseInt(byr) > parseInt(denda)){
			alert("Uang pembayaran lebih besar dari denda");
			return false;
		}
		
		if(byr){
			n = parseInt(denda) - parseInt(byr);
			document.getElementById("nilai").value = n;
		}

		document.getElementById("key").value = "freeskors";
		document.getElementById("anggota").value = '<?=$_SESSION['idtrans'];?>';
		goSubmit();
	}
}

function onlyNumber(e,elem,dec) {
	var code = e.keyCode || e.which;
	if ((code > 57 && code < 96) || code > 105 || code == 32) {
		if(code == 190 && dec) {
			if(elem.value == "") // belum ada isinya, titik tidak boleh didepan
				return false;
			if(elem.value.indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
				return false;
			return true;
		}
		return false;
	}
}

</script>

</html>
