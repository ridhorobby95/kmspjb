<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";

	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Surat Tagih';
	
	$p_namafile = 'rekap_surattagih'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select t.*
		from pp_tagihan t 
		where t.IDTAGIHAN in (select d.IDTAGIHAN 
				from PP_TAGIHANDETAIL d 
				join PP_EKSEMPLAR e on e.IDEKSEMPLAR = d.IDEKSEMPLAR 
				where 1=1 $sqlAdminUnit )
			and to_char(t.tgltagihan) between '$r_tgl1' and '$r_tgl2'
		order by t.tgltagihan";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Surat Tagih</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="100" align="center"><strong>Tanggal Tagihan</strong></th>
    <th width="80" align="center"><strong>Id Anggota</strong></th>
    <th width="80" align="center"><strong>Tagihan Ke</strong></th>
	<th width="100" align="center"><strong>Tanggal Expired</strong></th>
	<th width="150" align="center"><strong>Keterangan</strong></th>
   </tr>
  <?php
	$n=0;
	while($rs=$row->FetchRow()) 
	{  	$n++;
	$ket=explode("]",$rs['keterangan']);
	?>
    <tr height=25>
	<td align="center"><?= $n ?></td>
    <td align="center"><?= Helper::tglEng($rs['tgltagihan']) ?></td>
	<td align="center"><?= $rs['idanggota'] ?></td>
	<td align="center"><?= $rs['tagihanke'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglexpired']) ?></td>
	<td><?= $ket[0]."<br>".$ket[1] ?></td>
  </tr>
	<?  } ?>
	<? if($rsc==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak Ada Sirkulasi</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=6><b>Jumlah Tagihan : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>