<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	$peng = $conn->GetArray("select p.namadepan || ' ' || p.namabelakang as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor   ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	$author = Helper::removeSpecial($_REQUEST['author']);
	$sql = "select idpustaka, noseri, judul, edisi, nopanggil, authorfirst1 ,authorlast1, authorfirst2, authorlast2, authorfirst3, authorlast3
		from ms_pustaka
		where (
			(authorfirst1 ||' '||authorlast1) like ('%$author%')
			or (authorfirst2 ||' '||authorlast2) like ('%$author%')
			or (authorfirst3 ||' '||authorlast3) like('%$author%')
			or authorfirst1 like ('%$author%')
			or authorfirst2 like ('%$author%')
			or authorfirst3 like ('%$author%')
		) or (
			idpustaka in (
						select pa.idpustaka
						from pp_author pa
						join ms_author a on a.idauthor = pa.idauthor
						where upper(coalesce(a.namadepan,'')||' '||coalesce(a.namabelakang,'')) like upper('%$author%')
					)
		)
			
			";
	$p_sql = $conn->Execute($sql);	 

?>

<html>
<head>
<title>Detail Pustaka</title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	
	<link rel="stylesheet" href="style/button.css">
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0>
<table width="100%" cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse">
<tr>
	<th colspan=2  style="background:#4B6CB8;color:#fff;">DETAIL PENGARANG</th>
</tr>
<tr>
	<td valign="top">
		<table width="100%" border=1 style="border-collapse:collapse" cellpadding="4" cellspacing="0">
			<tr>
				<th width="50" align="center" style="background:#4B6CB8;color:#fff;">NO INDUK</th>
				<th width="200" align="center" style="background:#4B6CB8;color:#fff;">Judul</th>
				<th width="150" align="center" style="background:#4B6CB8;color:#fff;">Pengarang</th>
				<th align="center" style="background:#4B6CB8;color:#fff;">No Panggil</td>
			</tr>
			<? 
				$x=0;
				while($row=$p_sql->FetchRow()) { 
				$x++;
				?>
			<tr>
				<td><?= $row['noseri']?></td>
				<td><a href="index.php?page=show_pustaka&id=<?= $row['idpustaka'] ?>" title="Show Pustaka"><?= $row['judul']?><?= $row['edisi']!='' ? ' / '.$row['edisi'] : '' ?></a></td>
				
				<td>
						<?php 
							$auth = array(); 
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									echo "<a href='index.php?page=show_pauthor&author=".$a."'>".$a."</a><br/>"; 
								}
							}else{
								if($row['authorfirst1']!=''){
									echo "<a href='index.php?page=show_pauthor&author=".$row['authorfirst1'].($row['authorlast1']!='' ? ' '.$row['authorlast1'] : '')."'>".$row['authorfirst1'].($row['authorlast1']!='' ? ' '.$row['authorlast1'] : '')."</a>"; 
								}if ($row['authorfirst2']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo "<a href='index.php?page=show_pauthor&author=".$row['authorfirst2'].($row['authorlast2']!='' ? ' '.$row['authorlast2'] : '')."'>".$row['authorfirst2'].($row['authorlast2']!='' ? ' '.$row['authorlast2'] : '')."</a>";
								}
								if ($row['authorfirst3']) 
								{
									echo " <strong><em>,</em></strong> ";
									echo "<a href='index.php?page=show_pauthor&author=".$row['authorfirst3'].($row['authorlast3']!='' ? ' '.$row['authorlast3'] : '')."'>".$row['authorfirst3'].($row['authorlast3']!='' ? ' '.$row['authorlast3'] : '')."</a>";
								}
							}
						?>
				</td>
				<td><?= $row['nopanggil']?></td>
			</tr>
			<? } ?>
			<? if($x==0) { ?>
			<tr>
				<td colspan="5" align="center">Tidak terdapat pustaka</td>
			</tr>
			<? } ?>
		</table>
	</td>
</tr>
</table>
</body>
</html>