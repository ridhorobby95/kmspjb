<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_anggota = Helper::removeSpecial($_GET['id']);
	$r_periode = Helper::removeSpecial($_GET['periode']);

	
	if($r_anggota=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Track Pagu Anggota';
	
	$sql = "select pu.*,o.judul,u.namapengusul,u.tglusulan,lh.sisapagu,lh.sisasementara from pp_paguusulan pu
			 join pp_orderpustaka o on pu.idorderpustaka=o.idorderpustaka
			 join pp_usul u on o.idusulan=u.idusulan
			 join pp_pagulh lh on pu.idanggota=lh.idanggota and pu.periodeakad=lh.periodeakad
			 where pu.idanggota='$r_anggota'";
	
	if($r_periode!='')
		$sql.=" and pu.periodeakad='$r_periode'";
	
	$sql .=" order by u.tglusulan desc ";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo_warna.png' ?>" width=80 height=60></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Track Pagu Anggota</h2>
  	</strong></td>
  </tr>
    <tr>
	<td width=150> Id Anggota</td>
	<td>: <?= $row->fields['idanggota'] ?>
  </tr>
  <tr>
	<td> Nama Anggota </td>
	<td>: <?= $row->fields['namapengusul'].($r_periode!='' ? " / Tahun Periode : ".($r_periode-1)."/".$r_periode : '') ?></td>
	</tr>
  
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="30" align="center"><strong>No.</strong></th>
    <th width="80" align="center"><strong>Tanggal</strong></th>
    <th width="250" align="center"><strong>Judul Pustaka</strong></th>
	<th width="100" align="center"><strong>Pagu Dipakai</strong></th>
	<th width="150" align="center"><strong>Pengusul Utama</strong></th>
	 </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="center"><?= Helper::tglEng($rs['tglusulan']) ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td align="right">&nbsp;<?= Helper::formatNumber($rs['paguusulan'],'0',true,true) ?></td>
	<td align="left"><?= $rs['namapengusul'] ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=5 >Tidak ada track</td>
	</tr>
	<? } else { ?>
	<? if ($r_periode!=''){ ?>
   <tr height=25>
   <td colspan=6>
   <b>Jumlah Pemakaian: <?= $rsc ?>&nbsp;&nbsp;&nbsp; | 
   &nbsp;&nbsp;&nbsp;Sisa Pagu Real: 
   <?= Helper::formatNumber($row->fields['sisapagu'],'0',true,true) ?>
   &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Sisa Pagu Usulan :
   <?= Helper::formatNumber($row->fields['sisasementara'],'0',true,true) ?>
   </b></td></tr>
   <? }} ?>
</table>


</div>
</body>
</html>