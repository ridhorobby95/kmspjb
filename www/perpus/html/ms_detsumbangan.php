<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// koneksi ke sdm
	$conns = Factory::getConnSdm();
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_id = Helper::removeSpecial($_GET['id']);

	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Pustaka Sumbangan';
	$p_title = 'Pustaka Sumbangan';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('detail_sumbangan.php');
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		if($r_aksi == 'simpanpustaka' and $c_edit) {
				
				$record = array();
				$record = Helper::cStrFill($_POST);
				
				if($r_key == '') { // insert record	
				$record['idsumbangan']=$r_id;
				$record['stssumbangan']=1;
				//Helper::Identitas($record);
				
				// input pp_usul
				Sipus::InsertBiasa($conn,$record,pp_orderpustaka);

				}
				else { // update record
					
					Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idorderpustaka,$r_key);
				}	
				
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				if($r_key=='')
				Helper::redirect();
				else {
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
				}
				}
			if($conn->ErrorNo() == 0) {
				if($r_key == '')
				$r_key = $record['idusulan'];
				}
				else {
				$row = $_POST; // direstore
				$p_errdb = true;
				}
		}
		
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,pp_orderpustaka,idorderpustaka,$r_key);
			if($err==0) {
				header('Location: '.$p_filelist.'&key='.$r_id);
				exit();
			}
		}
	}
	
	if($r_id!=''){
		$p_anggota="select namapenyumbang, idunit,tglsumbangan from pp_sumbangan where idsumbangan='$r_id'";
		$rsa=$conn->GetRow($p_anggota);
		
		$unit = $conns->Execute("select idsatker,namasatker from ms_satker");
		$rowu=array();
		while($ru=$unit->FetchRow()){
		$rowu[$ru['idsatker']] = $ru['namasatker'];
		}
	}
	if($r_key!=''){
		$p_sqlstr="select * from pp_orderpustaka where idorderpustaka='$r_key'";
		$row=$conn->GetRow($p_sqlstr);
	}
	

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('judul').focus()">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>


<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist."&key=".$r_id; ?>" class="button"><span class="list">Daftar Sumbangan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:savePustaka();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table>
<br>
<?php include_once('_notifikasi.php'); ?>

<!-- ================================================ indetitas buku sumbangan ============================================ -->


<table width="500" border="0" cellspacing=0 cellpadding="3" style="background:#ffecc2;border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">

	<tr height=25> 
		<td><b>Nama Penyumbang</b></td>
		<td><b>: <?= $rsa['namapenyumbang'] ?></b></td>
	</tr>
	<tr>
		
		<td width="140"><b>Unit Kerja</b></td>
		<td><strong>: <?= $rsa['idunit']." - ".$rowu[$rsa['idunit']] ?></strong> 

		</td>
	</tr>
	<tr>
		<td height="30"><b>Tanggal</b></td>
		<td><b>: <?= Helper::tglEng($rsa['tglsumbangan']) ?></b></td>
	</tr>
</table>
<br>
	<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="6" class="GridStyle">
	<tr height=25> 
		<td width="150" class="LeftColumnBG">Judul Pustaka *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,63,$c_edit); ?></td>
	</tr>

	<tr height=25> 
		<td class="LeftColumnBG">Qty Sumbangan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('qtysumbangan',$row['qtysumbangan'],'ControlStyle',4,4,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 1 *</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst1',$row['authorfirst1'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast1',$row['authorlast1'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 2</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 3</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
</table><br>

<!-- ================================================ batas indetitas buku sumbangan ============================================ -->

<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

</form>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function saveData() {
	if(cfHighlight("idanggota,namapengusul,tglusulan,judul,harga,qtyusulan,authorfirst1"))
		goSave();
}

function goSaveSumb(){
	document.getElementById('act').value="simpansumb";
	goSubmit();
}

function goIsi(){
	document.getElementById('idanggota').value='';
	document.getElementById('namapengusul').value='';
	document.getElementById('idunit').value='';
	document.getElementById('namapengusul').focus();
	document.getElementById('namapengusul').readOnly='';
	document.getElementById('namapengusul').className='';
	
}

function isAktif(){
	document.getElementById('idanggota').readOnly=true;
	document.getElementById('idanggota').className='ControlRead';
	
	document.getElementById('namapengusul').readOnly=true;
	document.getElementById('namapengusul').className='ControlRead';
	
	document.getElementById('namasatker').readOnly=true;
	document.getElementById('namasatker').className='ControlRead';

	document.getElementById('tglsumbangan').readOnly=true;
	document.getElementById('tglsumbangan').className='ControlRead';

	document.getElementById('judul').focus();
}

function saveSumbangan(){
	if(cfHighlight("namapengusul,tglsumbangan"))
	{
		goSaveSumb();	
	}
	
}

function savePustaka(){
	if(cfHighlight("judul,qtysumbangan,authorfirst1"))
	{
		document.getElementById('act').value="simpanpustaka";
		goSubmit();
	}
}
function saveEdit(){
	if(cfHighlight("judul,qtysumbangan,authorfirst1"))
	{
		document.getElementById('act').value="simpanedit";
		goSubmit();
	}
}
</script>
</html>