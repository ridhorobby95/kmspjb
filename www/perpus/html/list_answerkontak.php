<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/referensi.class.php');
	require_once('classes/mail.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// otorisasi user
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_kontakonline';
	$p_window = '[PJB LIBRARY] Respon Kontak Online';
	$p_title = '.: Respon Kontak Online :.';
	$p_tbwidth = 100;
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_id = 'kontak';
	$p_row = 5;
	
	$r_act = $_POST['act'];
	$r_key = floor($_POST['key']);
	$rkey = floor($_POST['rkey']);
	
	if($r_act == 'respon' and $c_edit) {
		
		$record = array();
		$record['keterangankontak'] = Helper::cStrNull($_POST['keterangankontak'.$r_key]);
		$record['idkontak'] = $rkey;
		$record['idparentol'] = $r_key;
		$record['isadmin'] = 1;
		$record['jenisanggota'] = 'admin';
		$record['t_ipaddress'] = Helper::cStrNull($_SESSION['PERPUS_USER']);
		$record['t_input'] = date('Y-m-d H:i:s');
		$record['t_author'] = $_SESSION['PERPUS_USER'];
		
		Helper::Identitas($record);
		
		$err = Sipus::InsertBiasa($conn,$record,$p_dbtable);
		
		if(!$err) {
			$dataTo = $conn->GetRow("select * from $p_dbtable where idkontakol = $r_key");
			$mail = $conn->GetOne("select email from um.users where nid = '".$dataTo['t_user']."'");
			Mail::sendMail(($dataTo['email']?$dataTo['email']:$mail),$dataTo['t_author'],"Replay for ".$dataTo['judul'],$record['keterangankontak']);
			
			$p_sucdb = true;
			$sucdb = '<font color="green"><strong>Penambahan respon admin berhasil.</strong></font>';
			Helper::setFlashData('sucdb', $sucdb);
		}
		else {
			$p_errdb = true;
			$errdb = '<font color="red"><strong>Penambahan respon admin gagal, mohon diulang sekali lagi.</strong></font>';
			Helper::setFlashData('errdb', $errdb);
		}
	}
	else if($r_act == 'hapustopik' and $c_delete) {
		$err = Referensi::deleteTopik($conn,$r_key);
		
		if(!$err) {
			$sucdb = 'Penghapusan Berhasil.';	
			Helper::setFlashData('sucdb', $sucdb);$parts = Explode('/', $_SERVER['PHP_SELF']);
			$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
			Helper::redirect($url);
		}
		else {
			$errdb = 'Penghapusan Gagal.';	
			Helper::setFlashData('errdb', $errdb);
		}
	}
	else if($r_act == 'hapuscomments' and $c_delete) {
		$err = Referensi::deleteComments($conn,$r_key);
		
		if(!$err) {
			$sucdb = 'Penghapusan Berhasil.';	
			Helper::setFlashData('sucdb', $sucdb);$parts = Explode('/', $_SERVER['PHP_SELF']);
			$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
			Helper::redirect($url);
		}
		else {
			$errdb = 'Penghapusan Gagal.';	
			Helper::setFlashData('errdb', $errdb);
		}
	}
	
	$sqlkey1 = '';
	$sqlkey2 = '';
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$keytipe = Helper::removeSpecial($_POST['tipecari']);
		$keycari = Helper::removeSpecial($_POST['textcari']);
		$keyjenis = Helper::removeSpecial($_POST['idkontak']);
				
		if ($keytipe != ''){
			if ($keycari != '' and $keytipe == 'J')
				$sqlkey1 =" and  p.judul like '%$keycari%'";
			else if ($keycari != '' and $keytipe == 'I')
				$sqlkey1 =" and  p.keterangankontak like '%$keycari%'";
		}
		
		if ($keyjenis !='')
			$sqlkey2.=" and p.idkontak = $keyjenis ";
	
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	
	// mendapatkan topik
	$sql = "select * from $p_dbtable p
	left join lv_kontak k on k.idkontak=p.idkontak and k.isaktif=1
	where idparentol is null and p.idkontak <> 0 {$sqlkey1} {$sqlkey2}
	order by substr(t_input,1,10) desc";
	$rss = $conn->PageExecute($sql,$p_row,$p_page);
	
	if(!$rss or $rss->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		
	}
	else {
		// ditemukan record		
		$a_topik = array();
		$a_idtopik = array();
		while($row = $rss->FetchRow()) {
			$a_idtopik[] = $row['idkontakol'];
			$a_topik[] = $row;
		}
		
		// mendapatkan komentar topik
		$sql = "select * from pp_kontakonline
				where idparentol in (".implode(",",$a_idtopik).") order by substr(t_input,1,10), idkontakol ";
		$rs = $conn->Execute($sql);
		
		$a_komentar = array();
		//$a_komentaradmin = array();
		while($row = $rs->FetchRow()) {
			$a_komentar[$row['idparentol']][] = $row;
		}
		
		$p_atfirst = $rss->AtFirstPage();
		$p_atlast = $rss->AtLastPage();
		$p_lastpage = $rss->LastPageNo();
		$p_page = ($rss->_currentPage != '' ? $rss->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
	}
		
	$rs_cb = $conn->Execute("select namakontak, idkontak from lv_kontak where isaktif=1 and idparent is not null order by namakontak");
	$l_topik = $rs_cb->GetMenu2('idkontak',$keyjenis,true,false,0,'id="idkontak" class="ControlStyle" style="width:120" onchange="goSubmit()"');
	$l_topik = str_replace('<option></option>','<option value="">-- Semua--</option>',$l_topik);
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<link href="style/forum.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1><?= $p_title ?></h1>
				</div>
			</div>
		</header>
            <div class="table-responsive">
		<table width="100%" cellpadding="0" cellspacing="0" class="GridStyle">
			<!--tr>
				<td align="center" colspan="4" class="PageTitle"><?//= $p_title ?></td>
			</tr-->
			<tr>
				<td align="center" colspan="4" valign="bottom" style="border:0 none;">
					<table cellpadding="4" cellspacing="0" border="0" width="100%">
					<tr>
						<td class="thLeft" style="border:0 none;" width="130">
						<a href="javascript:goRefresh()" class="button"><span class="refresh" style="padding-left:18px;">
						Refresh</span></a></td>
						<td class="thLeft" style="border:0 none;" align="right" valign="middle">
						<div style="float:right;">
						<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.jpg" style="cursor:pointer;">
						<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.jpg" style="cursor:pointer;">
						<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.jpg" style="cursor:pointer;">
						<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.jpg" style="cursor:pointer;">
						</div>
						</td>
					</tr>
					</table>
			   </td>
			</tr>
			<tr>
				<td colspan="4" class="thLeft">
					<table cellpadding=0 cellspacing="2" border=0 width="100%" style="border:0 none;">
					<tr>
						<td class="thLeft" style="border:0 none;">Kata Kunci</td>
						<td class="thLeft" style="border:0 none;">: <?= UI::createSelect('tipecari',array(''=>'','J'=>'Judul','I'=>'Isi'),$keytipe,'ControlStyle',true); ?>&nbsp;<input type="text" id="textcari" name="textcari" size="35" value="<?= $keycari ?>" onKeyDown="etrCari(event);"></td>
						<td class="thLeft" style="border:0 none;">Topik</td>
						<td class="thLeft" style="border:0 none;">: <?= $l_topik; ?> </td>
						<td class="thLeft" style="border:0 none;" align="right"><a href="javascript:goSubmit()" class="buttonshort"><span class="filter" >Filter</span></a></td>		
					</tr>
					</table>
				</td>
			</tr>
		</table>
            </div>
		<br>
		<?php include_once('_notifikasi.php'); ?>
		<?	
			if ($a_topik){
			$i = 0;
			foreach($a_topik as $t_topik) {
				$i++;
				$no = (($p_page-1)*$p_row)+$i;
				
				$n_komentar = count($a_komentar[$t_topik['idkontakol']]);
		?>
            
<div class="table-responsive">
			<table width="100%" cellspacing="0" cellpadding="3" border="0" style="border-bottom: 0pt none;" class="tborder">
				<tr class="catbg3">
					<td width="2%" valign="middle" style="padding-left: 6px;">
						<img align="bottom" alt="" src="images/xx.gif">
					</td>
					<td width="13%" style="font-size:75%;"> Author</td>
					<td width="85%" valign="middle" id="top_subject" style="padding-left: 6px;font-size:75%;">
						Topik: <?= $t_topik['namakontak']; ?>
					</td>
				</tr>
            </table>
            </div>
            
            <div class="table-responsive">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="bordercolor">
				<tr>
					<td style="padding: 1px 1px 0pt;">
						<table width="100%" cellspacing="0" cellpadding="3" border="0">
							<tr>
								<td class="windowbg">
									<table width="100%" cellspacing="0" cellpadding="5" style="table-layout: fixed;">
										<tr>
											<td width="16%" valign="top" style="overflow: hidden;" rowspan="2">
												<span style="color:#AA0000;font-weight:bold;"><?= $t_topik['t_author'] ?></span>
												<br>
												Jenis Anggota &nbsp;: <?= $t_topik['jenisanggota'] ?>
												<br>
												 <?= $t_topik['email'] ?>
											</td>
											<td height="100%" width="100%" valign="top">
												<table width="100%" border="0">
												<tbody>
												<tr>
													<td valign="middle">
														<div id="subject_1" style="font-weight: bold;">
														<img border="0" alt="" src="images/xx.gif">&nbsp;&nbsp;
															<?= nl2br($t_topik['judul']) ?>
														</div>
														<div class="smalltext">
															<?= Helper::formatDateInd(substr($t_topik['t_input'],0,10)); ?>&nbsp;&nbsp;<em><?= substr($t_topik['t_input'],10,10); ?></em>
															<u style="cursor:pointer;color:#0099CC;" onclick="goDeleteList('<?= $t_topik['idkontakol']; ?>')">
															Delete </u>
														</div>
													 </td>
												</tr>
												</tbody>
												</table>
												<hr size="1" width="100%" class="hrcolor">
												<div class="post"><?= str_replace("\n","<br/>",$t_topik['keterangankontak']); ?></div>
												<hr size="1" width="100%" class="hrcolor">
											</td>
										</tr>
										<tr>
											<td width="85%" valign="bottom" class="smalltext">
											<div class="comment" id="showCm_<?= $t_topik['idkontakol']; ?>"><img src="images/comments.png">&nbsp;<a href="javascript:showComments('<?= $t_topik['idkontakol']; ?>');" title="Lihat Comments">Comments (<?= $n_komentar; ?>)</a></div>
											<div class="comment" id="hideCm_<?= $t_topik['idkontakol']; ?>" style="display:none"><img src="images/comments.png">&nbsp;<a href="javascript:hideComments('<?= $t_topik['idkontakol']; ?>');" title="Sembunyikan Comments">Hide Comments (<?= $n_komentar; ?>)</a></div>
									<table width="100%" bordercolor="#FFFFFF">
										<tr>
										<td width="100%">
									<div id="showCom_<?= $t_topik['idkontakol']; ?>" style="display:none">
									<?	if(!empty($a_komentar[$t_topik['idkontakol']])) {
											foreach($a_komentar[$t_topik['idkontakol']] as $t_komentar) { ?>
											<table width="100%" border="0" 
											style="margin-top:10px;margin-left:-5px;padding:5px;background:#ffe6c1;
											-moz-border-radius:3px;border:1px solid #c49f8f;">
												<tr>
													<td valign="top">
													<img border="0" alt="" src="images/speech_bubble.png">
													<font color="#333333" style="position:relative;bottom:10px;"><strong>&nbsp;<?= $t_komentar['t_author'].($t_komentar['isadmin'] !=1 ? '' : '&nbsp;&nbsp;<em>[Perpustakaan]</em>'); ?>&nbsp;&nbsp;</strong></font>
													<div>
													<?= Helper::formatDateInd(substr($t_komentar['t_input'],0,10)); ?>&nbsp;&nbsp;<em><?= substr($t_komentar['t_input'],10,10); ?></em>
												   <? if($c_delete){?>
													&nbsp;<u style="cursor:pointer;color:#0099CC;" onclick="goDeleteComments('<?= $t_komentar['idkontakol']; ?>')">
													Delete </u>
													<? }?>
													</div>
													</td>
												 </tr>
												 <tr>
													<td>
														<div class="post"><?= $t_komentar['keterangankontak']; ?></div>
													</td>
												  </tr>
											 </table>
											 <? }} ?>
								   </div>
											 <br>
											 <div style="color:#0099CC">Respon Perpustakaan</div>
												<?= UI::createTextArea('keterangankontak'.$t_topik['idkontakol'],'','ControlStyle',5,85,$c_edit); ?>
												 <br>
												 <input type="submit" name="comment" value="comment" onClick="responAdmin('<?= $t_topik['idkontakol'];?>','<?= $t_topik['idkontak'];?>')" style="cursor:pointer">										
									 </td>
									</tr>
								   </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</td></tr>
									<tr>
										<td>
											<table width="100%" cellspacing="0" cellpadding="0" style="border-bottom:1pt solid #725548;">
												<tr>
													<td class="middletext">  </td>
												</tr>
											</table>
										</td>
									</tr></table>
            </div>

		<? }}else echo 'Data Tidak Ditemukan'; ?>
			<input type="hidden" name="key" id="key">
			<input type="hidden" name="rkey" id="rkey">
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
		</form>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

var lastkey = 0;

function goDeleteList(key){
	$("#act").val("hapustopik");
	$("#key").val(key);
	goSubmit();
}

function goDeleteComments(key){
	$("#act").val("hapuscomments");
	$("#key").val(key);
	goSubmit();
}

function goRefresh() {
	$("#textcari").val('');
	$("#idkontak").val('');
	$("#tipecari").val('');
	document.getElementById("page").value = 1;
	goSubmit();
}

function responAdmin(key,rkey) {
	$("#act").val("respon");
	$("#key").val(key);
	$("#rkey").val(rkey);
	goSubmit();
}

function deleteBT(key) {
	var hapus = confirm("Apakah anda yakin akan menghapus data buku tamu tersebut?");
	if(hapus) {
		document.getElementById("key").value = key;
		document.getElementById("act").value = 'hapus';
		goSubmit();
	}
}

function showComments(key){
	$("#showCom_"+key).fadeIn(800);
	$("#showCm_"+key).fadeOut(800);
	$("#hideCm_"+key).fadeIn(800);
}

function hideComments(key){
	$("#showCom_"+key).fadeOut(800);
	$("#showCm_"+key).fadeIn(800);
	$("#hideCm_"+key).fadeOut(800);
}

function showComment(key) {
	var visible = $("[id='tr_komentar_"+key+"']:visible").length;
	
	if(visible == 0)
		$("[id='tr_komentar_"+key+"']").show();
	else
		$("[id='tr_komentar_"+key+"']").hide();
}

function writeAdminComment(key) {
	// yang sebelumnya dihapus
	if(lastkey != 0) {
		$("#span_admin_"+lastkey).html($("[name='respon']").val());
		$("#button_admin_"+lastkey).hide();
		$("#tr_admin_"+lastkey).hide();
	}
	
	var span = $("#span_admin_"+key);
	var val = span.html();
	
	span.html('<textarea rows="7" cols="70" name="respon">'+val+'</textarea>');
	
	$("#button_admin_"+key).show();
	$("#tr_admin_"+key).show();
	
	lastkey = key;
}

function showBT(elem) {
	var src = $(elem).attr("src");
	var arr = elem.id.split("_");
	
	$(elem).attr("src","images/kit.gif");
	
	$.ajax({
		type: "POST",
		url: "<?= Helper::navAddress('ajax') ?>",
		data: "f=showbt&key="+arr[1]+"&cek="+arr[2],
		timeout: 50000,
		
		success: function(result) {
			$(elem).attr("src",src);
			
			if(result == '1') {
				$(elem).hide();
				$("#show_"+arr[1]+"_"+(arr[2] == '1' ? 0 : 1)).show();
			}
			else
				alert("Proses setting status buku tamu gagal, coba ulangi sekali lagi.");
		},
		
		error: function(obj,err) {
			if(err == "timeout") {
				alert("Proses setting status buku tamu timeout, coba ulangi sekali lagi.");
			}
		}		
	});
}

function lockBT(elem) {
	var src = $(elem).attr("src");
	var arr = elem.id.split("_");
	
	$(elem).attr("src","images/kit.gif");
	
	$.ajax({
		type: "POST",
		url: "<?= Helper::navAddress('ajax') ?>",
		data: "f=lockbt&key="+arr[1]+"&cek="+arr[2],
		timeout: 50000,
		
		success: function(result) {
			$(elem).attr("src",src);
			
			if(result == '1') {
				$(elem).hide();
				$("#lock_"+arr[1]+"_"+(arr[2] == '1' ? 0 : 1)).show();
			}
			else
				alert("Proses setting status buku tamu gagal, coba ulangi sekali lagi.");
		},
		
		error: function(obj,err) {
			if(err == "timeout") {
				alert("Proses setting status buku tamu timeout, coba ulangi sekali lagi.");
			}
		}		
	});
}

</script>

</html>