<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_aktif = Helper::removeSpecial($_GET['aktif']);
	$r_key = Helper::removeSpecial($_GET['id']);
	$r_anggota = Helper::removeSpecial($_GET['key']);
	
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Pengusulan Label Hijau';
	$p_title = 'Pengusulan Label Hijau';
	$p_tbwidth = 600;
	$p_filelist = Helper::navAddress('list_usulandosen.php');
	
//	if($r_aktif=='' || $r_key =='')
	
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key2 = Helper::removeSpecial($_REQUEST['key2']);
		$rkey = Helper::removeSpecial($_REQUEST['rkey']);
		
		if($r_aksi == 'simpan' and $c_edit) {
				$record = array();
				$record = Helper::cStrFill($_POST);
				$record['judul'] = Helper::removeSpecial($_POST['judul']);
				//$conn->StartTrans();
				if($r_key == '') { // insert record	
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['statususulan']=0;
				$record['qtyusulan']=1;
				$record['isdenganpagu']=1;
				Helper::Identitas($record);
				
				$conn->StartTrans();
				// input pp_usul
				Sipus::InsertBiasa($conn,$record,$p_dbtable);

				$record['idusulan']=$conn->GetOne("select last_value from pp_usul_idusulan_seq");				
				Sipus::InsertBiasa($conn,$record,pp_orderpustaka);  // input pp_orderpustaka

				$spagu = Helper::removeSpecial($_POST['pagu']);
				if($spagu==0){
				$recPagu['idorderpustaka']=$conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq");				
				$recPagu['idanggota']=$record['idanggota'];
				$recPagu['paguusulan']=$record['hargausulan'];
				$recPagu['ispengusulutama']=1;
				$recPagu['periodeakad']=Sipus::getPaguAktif($conn);
				Helper::Identitas($recPagu);
				Sipus::InsertBiasa($conn,$recPagu,pp_paguusulan);

				$isPagu = $conn->GetOne("select sisasementara from pp_pagulh where idanggota='$recPagu[idanggota]' and periodeakad='$recPagu[periodeakad]'");
				
				if($isPagu>=$recPagu['paguusulan']){
				$paguUpdate['sisasementara'] = $isPagu - $recPagu['paguusulan'];
				Sipus::UpdateSpecial($conn,$paguUpdate,pp_pagulh,idanggota,$recPagu['idanggota'],periodeakad,$recPagu['periodeakad']);
				}else {
				$err = 1;
				$msg = 'Harga melebihi sisa pagu';
					}
				}
				if($err==1)				
				$conn->CompleteTrans(false);
				else
				$conn->CompleteTrans(true);
				}
				else { // update record
					$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
					//Helper::Identitas($record);
					Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
					
					Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idusulan,$r_key);	
				}	
				
				//$conn->CompleteTrans();
				
			if($conn->ErrorNo() != 0 or $err==1){
				
				$errdb = 'Penyimpanan data gagal. '.$msg;	
				Helper::setFlashData('errdb', $errdb);
				$row=$_POST;
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist);
				exit();	
				// $parts = Explode('/', $_SERVER['PHP_SELF']);
				// $url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&aktif='.$r_aktif;
				// Helper::redirect($url);
				}
				
		}
		else if ($r_aksi == 'simpanpagu' and $c_edit){
				$record = array();
				$record = Helper::cStrFill($_POST);
				$conn->StartTrans();
				/*
				if($r_key == '') { // insert record	
				$idusulan=Sipus::GetLast($conn,pp_usul_idusulan_seq,last_value);
				$record['idusulan']=$idusulan;
				$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
				$record['statususulan']=1;
				$record['isdenganpagu']=1;
				//$record['npkpetugasusul']=$_SESSION['PERPUS_USER'];
				Helper::Identitas($record);
				
				// input pp_usul
				Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				// input pp_orderpustaka
				//Sipus::InsertBiasa($conn,$record,pp_orderpustaka);
				
				}
				else { // update record
				*/
					$record['tglusulan']=Helper::formatDate($_POST['tglusulan']);
					//Helper::Identitas($record);
					Sipus::UpdateBiasa($conn,$record,$p_dbtable,idusulan,$r_key);
					
					//Sipus::UpdateBiasa($conn,$record,pp_orderpustaka,idusulan,$r_key);
					
					//$anggota=array();
					$anggota=$_POST['addpagu'];
					$jmlbaru=count($anggota);
					$usulan=$_POST['addshare'];
					$idordere=Helper::removeSpecial($_POST['idorder']);
					$thpagu=Helper::removeSpecial($_POST['th']);
					
					$check=$conn->Execute("select idorderpustaka,idanggota,paguusulan,periodeakad from pp_paguusulan where idorderpustaka=$idordere and void=0");
					
					if($check) { // bermasalah
					while($rowc=$check->FetchRow()){
					$arrA[]=$rowc['idanggota'];
					$arrU[]=$rowc['paguusulan'];
					}
					Sipus::UpdatePaguS($conn,$arrA,$arrU,$thpagu,'+');
					Sipus::DeleteBiasa($conn,pp_paguusulan,idorderpustaka,$idordere);
					}
					if($anggota!=''){
					Sipus::InsertPagu($conn,$anggota,$idordere,$usulan);					
					Sipus::UpdatePaguS($conn,$anggota,$usulan,$thpagu,'-'); // hatiiiiiiiiiiiiiii hatiiiiiiiiii warningggg
					}			
				//}	
				
				$conn->CompleteTrans();
				
				if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				header('Location: '.$p_filelist);
				exit();	
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idusulan,$r_key);
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			}
		}
		else if($r_aksi=='sunting' and $c_edit) {
			//echo "aaaaaaaaaaaaaaaaaaaaaaa";
			$p_editkey = $rkey;
		}
		else if($r_aksi=='savedetail' and $c_edit) {
			$conn->StartTrans();
			$thpagu=Sipus::getPaguAktif($conn);
			$r_anggota=Helper::removeSpecial($_POST['anggota']);
			$pagu=Helper::removeSpecial($_POST['pagu']);
			$ispagu=Helper::removeSpecial($_POST['ispagu']);
			$sisap=Sipus::SearchL($conn,pp_pagulh,"idanggota='$r_anggota' and periodeakad='$thpagu'",'sisasementara');
			echo $paguEdit=$sisap+$ispagu;
			if ($paguEdit>=$pagu){
			$rpagu['sisasementara']=$paguEdit-$pagu;
			Sipus::UpdateSpecial($conn,$rpagu,pp_pagulh,idanggota,$r_anggota,periodeakad,$thpagu);
			$recpagu['paguusulan']=$pagu;
			Helper::Identitas($recpagu);
			Sipus::UpdateBiasa($conn,$recpagu,pp_paguusulan,idpaguusulan,$rkey);
			$conn->CompleteTrans();
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect();
				}
			}else{
				$errdb = 'Jumlah Pagu Usulan melebihi sisa pagu.';	
				Helper::setFlashData('errdb', $errdb);
				}
				
		}
	}
	if ($r_key !='') {
		$p_sqlstr = "select *,o.idorderpustaka from $p_dbtable u
					 join pp_orderpustaka o
					 on o.idusulan=u.idusulan
					where u.idusulan='$r_key'";
		$row = $conn->GetRow($p_sqlstr);	
		$pagu=Sipus::getPaguAktif($conn);
		$ispagu=$conn->GetRow("select sisapagu,sisasementara from pp_pagulh where idanggota='$r_anggota' and periodeakad='$pagu'");
		
		$idorder=$row['idorderpustaka'];
		$rspagu=$conn->Execute("select p.*,a.namaanggota from pp_paguusulan p
								join ms_anggota a on p.idanggota=a.idanggota
								where idorderpustaka=$idorder order by p.idpaguusulan");
		
		// if($row['pagu']=="0"){
		// $op1="<label><input type=\"radio\" name=\"pagu\" id=\"pagu\" Value=\"0\" checked> Personal</label>";
		// $op2="<label><input type=\"radio\" name=\"pagu\" id=\"pagu\" value=\"1\"> Share Pagu</label>";
		// } else {
		// $op1="<label><input type=\"radio\" name=\"pagu\" id=\"pagu\" Value=\"0\"> Personal</label>";
		// $op2="<label><input type=\"radio\" name=\"pagu\" id=\"pagu\" value=\"1\" checked> Share Pagu</label>";
		// }
	}
		//$rs_cb = $conn->Execute("select kdfakultas ||'-'|| namajurusan, kdjurusan from lv_jurusan order by kdfakultas");
		//$l_jurusan = $rs_cb->GetMenu2('kdjurusan',$row['kdjurusan'],true,false,0,'id="kdjurusan" class="ControlStyle" style="width:250"');
		
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">

	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" enctype="multipart/form-data">
<table width="<?= $p_tbwidth ?>">
	<tr height="30">
		<td align="center" class="PageTitle"><?= $p_title ?></td>
	</tr>
</table>
<table width="100">
	<tr>
	<? if($c_readlist) { ?>
	<td align="center">
		<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Usulan</span></a>
	</td>
	<? } if($c_edit) { ?>
	<td align="center">
		<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
	</td>
	<td align="center">
		<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
	</td>
	<? } if($c_delete and $r_key != '') { ?>
	<td align="center">
		<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
	</td>
	<? } ?>
	</tr>
</table>
<br>
<?php include_once('_notifikasi.php'); ?>
<table width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="3" class="GridStyle">
	<tr height=25> 
		<td class="LeftColumnBG" width="180">Id Anggota *</td>
		<td width="370"><b><?= UI::createTextBox('idanggota',$row['idanggota'],'ControlRead',20,20,$c_edit,'readonly'); ?></b>
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=ud',620,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG" height="30">Nama Pengusul *</td>
		<td><b><?= UI::createTextBox('namapengusul',$row['namapengusul'],'ControlRead',50,50,$c_edit,'readonly'); ?></b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG" height="30">Sisa Pagu</td>
		<td><b><?= UI::createTextBox('sisapagu',$ispagu['sisasementara'],'ControlRead',20,20,$c_edit,'readonly'); ?></b></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tanggal Usulan *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tglusulan',$r_key!='' ? Helper::formatDate($row['tglusulan']) : date('d-m-Y'),'ControlStyle',10,10,$c_edit); ?>
		<? if($c_edit) { ?>
		<img src="images/cal.png" id="tgleusulan" style="cursor:pointer;" title="Pilih tanggal usulan">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tglusulan",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgleusulan",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		
		[ Format : dd-mm-yyyy ]<? } ?>
		</td>
	</tr>	
	<tr height=25> 
		<td class="LeftColumnBG">Judul Pustaka *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',200,63,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Harga *</td>
		<td class="RightColumnBG"><?= UI::createTextBox('hargausulan',Helper::formatNumber($row['hargausulan'],'0',false,false),'ControlStyle',14,14,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
	</tr>
	<? if($r_aktif!='pagu') {?>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 1 *</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst1',$row['authorfirst1'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast1',$row['authorlast1'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 2</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst2',$row['authorfirst2'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast2',$row['authorlast2'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25>
	<td class="LeftColumnBG">Pengarang 3</td>
		<td class="RightColumnBG">
		<?= UI::createTextBox('authorfirst3',$row['authorfirst3'],'ControlStyle',100,29,$c_edit); ?>
		<?= UI::createTextBox('authorlast3',$row['authorlast3'],'ControlStyle',100,28,$c_edit); ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Penerbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('penerbit',$row['penerbit'],'ControlStyle',100,50,$c_edit); ?>
		<input type="hidden" name="idpenerbit" id="idpenerbit">
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit&code=u',500,500);">
			<? } ?>
		</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Tahun Terbit</td>
		<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit); ?> [ Format : yyyy ]</td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Edisi</td>
		<td class="RightColumnBG"><?= UI::createTextBox('edisi',$row['edisi'],'ControlStyle',20,20,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">ISBN</td>
		<td class="RightColumnBG"><?= UI::createTextBox('isbn',$row['isbn'],'ControlStyle',30,30,$c_edit); ?></td>
	</tr>
	<tr height=25> 
		<td class="LeftColumnBG">Keterangan</td>
		<td class="RightColumnBG"><?= UI::createTextArea('keterangan',$row['keterangan'],'ControlStyle',2,60,$c_edit); ?></td>
	</tr>
	<?
	if($r_key==''){?>
	<tr height=25> 
		<td class="LeftColumnBG">Status Pagu</td>
		<td class="RightColumnBG">
		<label><input name="pagu" type="radio" value="0" id="pagu" checked /> Personal</label>
		<label><input name="pagu" type="radio" value="1" id="pagu" /> Share Pagu</label>
		<? } ?>
		</td>
	</tr>
	<? } ?>
</table><br>
	<? if($r_aktif=='pagu') {?>
<center>
<table width="490" border="1" class="GridStyle" cellspacing=0 cellpadding="3" align="center">
	<tr>
		<td align="center" class="PageTitle">Share Pagu</td>
	</tr>
	</tr>
		<td align="center">
						<table width="100%" cellspacing="0" align="center" cellpadding="3">
						<? if($c_edit) { ?>
						<tr> 
							<td width="150" class="LeftColumnBG">Nama Anggota</td>
							<td><input type="text" name="namapagu" size="40" id="namapagu" class="ControlRead" readonly>
								<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=sp',620,500);">
								<? } ?>
							</td>
						<tr>
						<tr>
							<td class="LeftColumnBG">Sisa Pagu</td>
							<td><input type="text" name="sisa" size="14" id="sisa"  class="ControlRead" readonly></td>
						</tr>
						<tr> 
							<td class="LeftColumnBG">Share Pagu</td>
							<td><input type="hidden" name="idpagu" size="14" id="idpagu">
								<input type="text" name="sharepagu" id="sharepagu">
								
								<input type="button" class="ControlStyle" value="Tambah" onClick="addShare()">
								</td>
						</tr>
						<tr id="error" style="display:none">
							<td colspan=3 align="center">
							<span id="span_error5" style="display:none"><font color="#FF0000">Anggota tersebut telah ditambahkan</font></span>
								 <span id="span_kurang" style="display:none"><font color="#FF0000">Share Pagu melebihi sisa pagu</font></span>
								 <span id="span_error6" style="display:none"><font color="#FF0000">Masukkan Data dengan benar</font></span>
							
							</td>
						</tr>
						<?	} ?>
		
							
					</table>
					<table width="100%" cellpadding=0 cellspacing=0 style="border:1pt solid #d2d2d2;-moz-border-radius:5px;padding:10px;">
							<tr height=20 class="AlternateBG">
								<td align="center">Anggota Share</td>
								<td align="center">Share</td>
								<td align="center">Hapus</td>
							</tr>
						    <tr height=20 id="tr_jurkosong"<? if(!$rspagu->EOF) { ?> style="display:none" <? } ?>> 
								<td bgcolor="#EEEEEE" align="center" colspan="3"><strong>Belum memiliki share pagu</strong></td>
							</tr><? if($r_key !=''){
							
									  while($rowpagu = $rspagu->FetchRow()) { 
									  if(strcasecmp($rowpagu['idpaguusulan'],$p_editkey)) {
									  ?>
										<tr height=20> 
											<td>
												<u title="Edit Pagu" onClick="editRow('<?= $rowpagu['idpaguusulan'] ?>')" style="cursor:pointer;color:blue"><?= $rowpagu['namaanggota'] ?></u>
												<input type="hidden" name="namausul[]" size="50" id="namausul" value="<?= $rowpagu['namaanggota'] ?>" class="ControlRead" readonly>
												<input type="hidden" name="addpagu[]" id="addpagu" value="<?= $rowpagu['idanggota'] ?>">
											</td>
											<td align="right">
											<?= Helper::formatNumber($rowpagu['paguusulan'],'0',true,true) ?>&nbsp;
											<input type="hidden" name="addshare[]" id="addshare" value="<?= $rowpagu['paguusulan'] ?>" class="ControlRead" readonly>
											</td>
											<td width="50" align="center">
											<? if($c_edit) { ?><img src="images/delete.png" onClick="delPagu(this)" style="cursor:pointer"><? } ?></td>
										</tr>
									<?} else { ?>
										<tr height=20> 
											<td><?= $rowpagu['namaanggota'] ?></td>
											<td>
											<?= UI::createTextBox('pagu',$rowpagu['paguusulan'],'ControlStyle',14,10,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
											</td>
											<td width="50" align="center">
											<img src="images/tombol/file.png" width="16" title="Save Detail" onClick="saveRow('<?= $rowpagu['idpaguusulan'] ?>','<?= $rowpagu['idanggota'] ?>','<?= $rowpagu['paguusulan'] ?>')" class="link"/>
										</tr>
									
									
									<? } ?>
								<?	}
								
								}  ?>

						<tr id="tr_tambahjur">
							<td colspan=2>
								
							</td>
						</tr>
					</table>
					<table border="1" id="table_templatejur" style="display:none">
						<tr  height=20> 
							<td>
							<input type="hidden" name="namausul[]" size="50" id="namausul" class="ControlRead" readonly>
							<input type="hidden" name="addpagu[]" id="addpagu" disabled>
							</td>
							<td>
							<input type="hidden" name="addshare[]" id="addshare" class="ControlRead" readonly>
							</td>
							<td bgcolor="#EEEEEE" width="20" align="center"><img src="images/delete.png" onClick="delPagu(this)" style="cursor:pointer"></td>
						</tr>
					</table>
		</td>
	</tr>
</table>
</center>
<? } ?>
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
<input type="hidden" name="rkey" id="rkey">
<input type="hidden" name="anggota" id="anggota">
<input type="hidden" name="ispagu" id="ispagu">
<input type="hidden" name="key2" id="key2"  value="<?= $r_key2 ?>">
<input type="hidden" name="th" id="th"  value="<?= $pagu?>">
<input type="hidden" name="idorder" id="idorder"  value="<?= $idorder ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

</form>
</div>
</body>
<!--<script type="text/javascript" src="scripts/jquery.js"></script>-->
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xauto.js"></script>

<script language="javascript">
$(function(){
	   $("#tglusulan").mask("99-99-9999");
	   $("#tahunterbit").mask("9999");
});

function saveData() {
	if(cfHighlight("idanggota,namapengusul,tglusulan,judul,harga,authorfirst1")){
		var x='<?= $r_aktif ?>';
		if (x=='pagu'){
		var pilih=confirm("Apakah Anda yakin akan melakukan update pada share pagu ?");
		if(pilih) {
			simpanPagu();
		}//else
			//goSave();
		}else
			goSave();
			
	}
}

function simpanPagu() {
	var check='<?= $r_aktif ?>';
		
	if (check=='pagu'){
	//var conf=confirm("Apakah Anda yakin akan melakukan update pada share pagu ?");
	//if(conf){
	var total=document.getElementById("hargausulan").value;
	var xx = document.getElementById('perpusform');
	var jumlah=0;
	for (var i =0; i < xx.addshare.length-1; i++) 
	{
		var pagu=parseInt(xx.addshare[i].value);
		jumlah +=pagu;
	}
	//alert(jumlah);
	//alert(total);
	if (jumlah==total)
		goSavePagu();
	else
		alert("Share pagu tidak sesuai");
		
	//}
	}
}

function goSavePagu() {
	document.getElementById("act").value="simpanpagu";
	goSubmit();
}

function ClearShare() {
	document.getElementById('namapagu').value='';
	document.getElementById('sisa').value='';
	document.getElementById('sharepagu').value='';
}	
</script>
<script language="javascript">

function addShare() {
	var idpagu = $("#idpagu").val();
	var sisa = parseInt($("#sisa").val());
	var pakai = parseInt($("#sharepagu").val());
	
	if (pakai>sisa || pakai==''){
		$("#error").show();
		$("#span_kurang").show();
		setTimeout('$("#error").hide()',1000);
		setTimeout('$("#span_kurang").hide()',1000);
		
		
		return false;
		}
	if (idpagu=='' || pakai==''){
		$("#error").show();
		$("#span_error6").show();
		setTimeout('$("#error").hide()',1000);
		setTimeout('$("#span_error6").hide()',1000);
		return false;
		} else {
	// cek apakah sudah ada
	if($("[name='addpagu[]'][value='"+idpagu+"']").length > 0) {
		$("#error").show();
		$("#span_error5").show();
		setTimeout('$("#error").hide()',1000);
		setTimeout('$("#span_error5").hide()',1000);
		return false;
	}}
	
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_jurkosong:visible").length > 0)
		$("#tr_jurkosong").hide();
	
	var newtab = $($("#table_templatejur tbody").html()).insertBefore("#tr_tambahjur");
	var newtdf = newtab.find("td").eq(0);
	var newtd2 = newtab.find("td").eq(1);
	var addpagu = newtdf.find("[name='addpagu[]']");
	var addshare = newtd2.find("[name='addshare[]']");
	var idpagu = $("#idpagu").val();
	var namapeng = $("#namapagu").val();
	var share = $("#sharepagu").val();
	var nama = newtdf.find("[name='namausul[]']");
	//var namajurusan = $("#kdjurusan option[value='"+kdjurusan+"']").text();

	newtdf.prepend(namapeng);
	newtd2.prepend(share);
	nama.val(namapeng);
	addpagu.val(idpagu);
	addshare.val(share);
	addpagu.attr("disabled",false);
	//nama.attr("disabled",false);
	ClearShare();

}

function delPagu(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
	
	// jika kosong, tampilkan tanda kosong
	if($("#tr_tambahjur").prev().is("#tr_jurkosong"))
		$("#tr_jurkosong").show();
}
function editRow(id){
	$("#rkey").val(id);
	$("#act").val("sunting");
	goSubmit();
}
function saveRow(id,angg,is){
	$("#rkey").val(id);
	$("#anggota").val(angg);
	$("#ispagu").val(is);
	$("#act").val("savedetail");
	goSubmit();
}
</script>

</html>