<?php	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	$c_delete = $a_auth['candelete'];
		
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_dbtable = 'pp_pelayanandetil';
	$p_window = '[PJB LIBRARY] Surat Bebas Pinjam dan Sumbangan';
	$p_title = 'SURAT PEMINJAMAN DAN SUMBANGAN';
	$p_tbheader = '.: SURAT PEMINJAMAN DAN SUMBANGAN:.';
	$p_col = 9;
	$p_tbwidth = 1135;
	$p_filelist =  Helper::navAddress('bebas_sumbangan.php');
	
	$r_key=Helper::removeSpecial($_GET['id']);
	$r_key2=Helper::removeSpecial($_GET['idp']);
	$rkey=Helper::removeSpecial($_GET['idsumb']);
	
	if($r_key=='') {
		header('Location: '.$p_filelist);
		exit();
	} else{
		$p_check=$conn->GetRow("select a.idanggota, a.namaanggota, a.idunit from ms_anggota a where a.idanggota='$r_key' ");
		
		if(!$p_check){
			header('Location: '.$p_filelist);
			exit();
		}else {
			if($p_check['idunit'] != $idunit){
				echo "Anggota bukan berasal dari unit $unitlogin.";
			}else{
				$p_check2=$conn->GetRow("select idanggota
							from v_trans_list
							where idanggota='$r_key' and statustransaksi='1' and kdjenistransaksi<>'PJL' ");
				if($p_check2){
					header('Location: '.$p_filelist);
					exit();
				}
			}
		}
		$getNomer=$conn->GetRow("select max(nosurat) as maxsurat
					from pp_pelayanan
					where kdpelayanan='S' and to_char(tglpelayanan,'Year')=to_char(current_date,'Year')");
		$slice=explode("/",$getNomer['maxsurat']);
		$isNomer=$slice['0']+1;
	}
	
	if(!empty($_POST)) {
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$r_select=Helper::removeSpecial($_POST['key']);
		$idsumbangan=Helper::removeSpecial($_POST['idsumb']);
		
		if($r_aksi == 'simpan' and $c_add) {
			$conn->StartTrans();
			
			$idsumb=Sipus::GetLast($conn,pp_sumbangan,idsumbangan);
			$recsumb['idsumbangan']=$idsumb;
			$recsumb['idanggota']=$r_key;
			$recsumb['namapenyumbang']=$p_check['namaanggota'];
			$recsumb['tglsumbangan']=Helper::formatDate($_POST['txttgl']);
			$recsumb['npksumbangan']=$_SESSION['PERPUS_USER'];
			Helper::Identitas($recsumb);
			Sipus::InsertBiasa($conn,$recsumb,pp_sumbangan);
			
			$max=$conn->GetRow("select max(idpelayanan) as idmax from pp_pelayanan");
			$idmax=$max['idmax']+1;
			$record['idpelayanan']=$idmax;
			$record['idanggota']=$r_key;
			$record['kdpelayanan']="S";
			$record['tglpelayanan']=Helper::formatDate($_POST['txttgl']);
			$record['petugas']=Helper::cStrNull($_SESSION['PERPUS_USER']);
			$record['nosurat']=Helper::removeSpecial($_POST['txtnomer']);
			$record['jenis']=$recsumb['idsumbangan'];
			Helper::Identitas($record);	
			Sipus::InsertBiasa($conn,$record,pp_pelayanan);
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$key2=$record['idpelayanan'];
				$key3=$recsumb['idsumbangan'];
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&idp='.$key2.'&idsumb='.$key3;
				Helper::redirect($url);
			}
		
		}
		else if($r_aksi == 'selesei'){
			header('Location: '.$p_filelist);
			exit();
		}
		else if($r_aksi == 'insersi' and $c_add) {
			$conn->StartTrans();
			$record['idpelayanan'] = $r_key2;
			$record['nama'] = Helper::cStrNull($_POST['i_nama']);
			$record['authorfirst1'] = Helper::cStrNull($_POST['i_auth1']);
			$record['authorlast1'] = Helper::cStrNull($_POST['i_auth2']);
			$record['edisi'] = Helper::cStrNull($_POST['i_edisi']);
			$record['jumlah'] = Helper::cStrNull($_POST['i_jumlah']);		
			
			$recorder['judul']=Helper::removeSpecial($record['nama']);
			$recorder['authorfirst1']=Helper::cStrNull($_POST['i_auth1']);
			$recorder['authorlast1'] = Helper::cStrNull($_POST['i_auth2']);
			$recorder['edisi'] = Helper::cStrNull($_POST['i_edisi']);
			$recorder['idsumbangan']=$rkey;
			$recorder['qtysumbangan']=$record['jumlah'];
			$recorder['qtyttb'] = $record['jumlah'];
			$recorder['ststtb'] = '1';
			
			Sipus::InsertBiasa($conn,$recorder,pp_orderpustaka);
			
			$idorder=$conn->GetOne("select max(idorderpustaka) from pp_orderpustaka");
			$record['idorderpustaka']=$idorder;
			Helper::Identitas($record);
			Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
			$slice=explode("/",$getttb['ttb']);
			$isttb=$slice[0]+1;
			$nottb=Helper::strPad($isttb,4,0,'l')."/TTP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y');
			$idttb=Sipus::GetLast($conn,pp_ttb,idttb);
			
			$recttb['idttb']=$idttb;
			$recttb['nottb']=$nottb;
			$recttb['tglttb']=date('Y-m-d');
			$recttb['npkttb']=$_SESSION['PERPUS_USER'];
			$recttb['jnsttb']=2;
			
			Sipus::InsertBiasa($conn,$recttb,pp_ttb);

			$recdttb['idorderpustaka']=$idorder;//$conn->GetOne("select pp_orderpustaka_id_seq.nextval FROM dual");
			$recdttb['idttb']=$recttb['idttb'];
			$recdttb['qtyttbdetail']=Helper::cStrNull($_POST['i_jumlah']);
			$recdttb['ststtbdetail']=1;			
			Sipus::InsertBiasa($conn,$recdttb,pp_orderpustakattb);
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'];
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'update' and $c_edit) {
			//$record = array();
			$record['nama'] = Helper::cStrNull($_POST['u_nama']);
			$record['authorfirst1'] = Helper::cStrNull($_POST['u_auth1']);
			$record['authorlast1'] = Helper::cStrNull($_POST['u_auth2']);
			$record['edisi'] = Helper::cStrNull($_POST['u_edisi']);
			$record['jumlah'] = Helper::cStrNull($_POST['u_jumlah']);
			Helper::Identitas($record);
			$conn->StartTrans();
			Sipus::UpdateBiasa($conn,$record,$p_dbtable,idpelayanandetil,$r_select);
			
			$keyid=Helper::removeSpecial($_POST['keyid']);
			$recorder['judul']=$record['nama'];
			$recorder['authorfirst1']=Helper::cStrNull($_POST['u_auth1']);
			$recorder['authorlast1'] = Helper::cStrNull($_POST['u_auth2']);
			$recorder['edisi'] = Helper::cStrNull($_POST['u_edisi']);
			$recorder['qtysumbangan']=Helper::cStrNull($_POST['u_jumlah']);
			$recorder['qtyttb'] = Helper::cStrNull($_POST['u_jumlah']);
			$recorder['ststtb'] = '1';
			Sipus::UpdateBiasa($conn,$recorder,pp_orderpustaka,idorderpustaka,$keyid);
			
			$recdttb['qtyttbdetail']=$recorder['qtyttb'];
			$recdttb['ststtbdetail']=1;			
			Sipus::UpdateBiasa($conn,$recdttb,pp_orderpustakattb,idorderpustaka,$keyid);
			
			$conn->CompleteTrans();
			if($conn->ErrorNo() != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'];
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$keyid=Helper::removeSpecial($_POST['keyid']);
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,$p_dbtable,idpelayanandetil,$r_select);
			Sipus::DeleteBiasa($conn,pp_orderpustaka,idorderpustaka,$keyid);
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'];
				Helper::redirect($url);
				}
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_select;
		}
	}
	
	if($r_key2!=''){
		$rows=$conn->GetRow("select * from pp_pelayanan where idpelayanan=$r_key2");
		
		$rs=$conn->Execute("select * from pp_pelayanandetil where idpelayanan=$r_key2");
		
		$sum = $conn->GetOne("select sum(jumlah) from pp_pelayanandetil where idpelayanan=$r_key2");
	}	

?>
	
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage()">
<?php include('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
    <div class="LeftRibbon"> Surat Bebas Pinjam dan Sumbangan Pustaka</div>
		<div align="center">

			<form name="perpusform" id="perpusform" method="post" action="">
			<table width='530' border="0" align='center' cellpadding='4' cellspacing=0 class="filterTable">
			<tr height=25>
				<td><b>Nama Anggota</b></td>
				<td><b>: &nbsp;<?= $p_check['namaanggota'] ?></b></td>
			</tr>
			<tr>
				<td width='123'>Nomor Surat</td>
				<td>: &nbsp;<input type='text' size='25' maxlength="25" name='txtnomer' id='txtnomer' value="<?= $r_key2 =='' ? Helper::strPad($isNomer,4,0,'l')."/P.PJB/BP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y') : $rows['nosurat'] ?>" readonly="readonly"></td>
			</tr>
				<tr>
				<td>Tanggal Surat</td>
				<td>: &nbsp;<input type='text' size='10' name='txttgl' id='txttgl' value="<?= $rows['tglpelayanan']=='' ? date("d-m-Y") : Helper::formatDate($rows['tglpelayanan']) ?>" readonly="readonly">

					<img src="images/cal.png" id="tgle" style="cursor:pointer;" title="Pilih tanggal surat">
					&nbsp;
					<script type="text/javascript">
					Calendar.setup({
						inputField     :    "txttgl",
						ifFormat       :    "%d-%m-%Y",
						button         :    "tgle",
						align          :    "Br",
						singleClick    :    true
					});
					</script>
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td>
				<td>
				<? if($r_key2=='') {?>
				<input type='button' name='btnbebas' id='btnbebas' value='Simpan' class="buttonSmall" onClick="goSimpan()" style="cursor:pointer;height:25;width:110"> &nbsp;
				<input type='button' name='btnbatal' id='btnbatal' value='Batal' class="buttonSmall" onClick="goSelesei()" style="cursor:pointer;height:25;width:110">
				<? } else { ?>
				<input type='button' name='btnprint' id='btnprint' value='Cetak Surat' class="buttonSmall" onClick="goPrintBebas()" style="cursor:pointer;height:25;width:110">&nbsp;
				<input type='button' name='btnselesai' id='btnselesai' value='Selesai' class="buttonSmall" onClick="goSelesei()" style="cursor:pointer;height:25;width:110">
					<? } ?>
				</td>
				</tr>
			</table><br>
			<? include_once('_notifikasi.php'); ?>
			<? if($r_key2 !='') { ?>
				<header style="width:800">
					<div class="inner">
					  <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
					    <h1>Daftar Pustaka Sumbangan</h1>
					  </div>
					</div>
				</header>
				<table width="800" border="0" cellpadding="4" cellspacing=0 class="GridStyle">

					<tr height="20"> 
						<td width= "50%" nowrap align="center" class="SubHeaderBGAlt">Judul Pustaka</td>
						<td width= "230" nowrap align="center" class="SubHeaderBGAlt">Pengarang</td>
						<td width= "10%" nowrap align="center" class="SubHeaderBGAlt">Edisi</td>
						<td width= "10%" nowrap align="center" class="SubHeaderBGAlt">Jumlah</td>
						<? if($c_edit or $c_delete) { ?>
						<td width="40" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
						<? } ?>
					</tr>
					<?php
						$i = 0;
						while ($row = $rs->FetchRow()) 
						{
							if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
							if(strcasecmp($row['idpelayanandetil'],$p_editkey)) { // row tidak diupdate
					?>
					<tr class="<?= $rowstyle ?>">
						<td>
						<? if($c_edit) { ?>
							<u title="Sunting Data" onclick="goEditIP('<?= $row['idpelayanandetil']; ?>');" class="link"><?= $row['nama']; ?></u>
						<? } else { ?>
							<?= $row['nama']; ?>
						<? } ?>
						</td>
						<td><?= $row['authorfirst1']." ".$row['authorlast1'] ?></td>
						<td><?= $row['edisi'] ?></td>
						<td align="center"><?= $row['jumlah']; ?></td>
						<? if($c_delete or $c_edit) { ?>
						<td align="center">
						
							<u title="Hapus Data" onclick="goDeleteSumbangan('<?= $row['idpelayanandetil']; ?>','<?= $row['idorderpustaka'] ?>','<?= $row['nama']; ?>');" class="link">[hapus]</u>
						
						</td><? } ?>
					</tr>
					<?php
							} else { // row diupdate
					?>
					<tr class="<?= $rowstyle ?>"> 
						<td><?= UI::createTextBox('u_nama',$row['nama'],'ControlStyle',200,40,true,'onKeyDown="etrUpdate(event);"'); ?></td>
						<td>
						<?= UI::createTextBox('u_auth1',$row['authorfirst1'],'ControlStyle',100,15,true,'onKeyDown="etrUpdate(event);"'); ?>
						<?= UI::createTextBox('u_auth2',$row['authorlast1'],'ControlStyle',100,15,true,'onKeyDown="etrUpdate(event);"'); ?>
						</td>
						<td>
						<?= UI::createTextBox('u_edisi',$row['edisi'],'ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?>
						</td>
						<td align="center"><?= UI::createTextBox('u_jumlah',$row['jumlah'],'ControlStyle',20,10,true,'onKeyDown="etrUpdate(event);"'); ?></td>
						<td align="center">
						<? if($c_edit) { ?>
							<u id="updatedetail" title="Update Data" onclick="updateData('<?= $row['idpelayanandetil']; ?>','<?= $row['idorderpustaka'] ?>');" class="link">[update]</u>
						<? } ?>
						</td>
					</tr>
					<?php 	}
						}
						if ($i==0) {
					?>
					<tr height="20">
						<td align="center" colspan="<?= $p_col; ?>"><b>Data sumbangan masih kosong.</b></td>
					</tr>
					<?php } if($c_add) { ?>
					<tr class="LiteSubHeaderBG"> 
						<td><?= UI::createTextBox('i_nama','','ControlStyle',200,40,true,'onKeyDown="etrInsert(event);"'); ?></td>
						<td>
						<?= UI::createTextBox('i_auth1',$row['authorfirst1'],'ControlStyle',100,15,true,'onKeyDown="etrInsert(event);"'); ?>
						<?= UI::createTextBox('i_auth2',$row['authorlast1'],'ControlStyle',100,15,true,'onKeyDown="etrInsert(event);"'); ?>
						</td>
						<td>
						<?= UI::createTextBox('i_edisi',$row['edisi'],'ControlStyle',20,10,true,'onKeyDown="etrInsert(event);"'); ?>
						</td>
						<td align="center"><?= UI::createTextBox('i_jumlah','','ControlStyle',20,10,true,'onKeyDown="return onlyNumber(event,this,false,true);etrInsert(event);"'); ?></td>
						<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
					</tr>
					<?php } ?>
				</table>

			<? } ?>
		<br>
			<input type="hidden" name="key" id="key">	
			<input type="hidden" name="keyid" id="keyid">	
			
			<input type="hidden" name="act" id="act">
			<input type="hidden" name="scroll" id="scroll">
			<input type="hidden" name="idsumb" id="idsumb" value="<?= $recsumb['idsumbangan']!='' ? $recsumb['idsumbangan'] : $idsumbangan ?>">
		</div>
		</div>
		</div>
</body>
	<script type="text/javascript" src="scripts/jquery.masked.js"></script>
	<script type="text/javascript">
	$(function(){
	   $("#txttgl").mask("99-99-9999");
	});
	
	
	function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

	function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		document.getElementById('updatedetail').click();
	}
	
	function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_nama').focus();
	<? } else { 
	if($r_key2=='') {
	?>
	document.getElementById('txtnomer').focus();
	<? } else { ?>
	document.getElementById('i_nama').focus();
	<? } 
	} ?>
	
	}
	
	function goSimpan(){
		if(cfHighlight("txtnomer,txttgl")) {
		document.getElementById("act").value="simpan";
		goSubmit();
		}
	}
	
	function insertData() {
	if(cfHighlight("i_nama,i_auth1,i_jumlah"))
		goInsertIP();
	}

	function goSelesei() {

		document.getElementById("act").value="selesei";
		goSubmit();

	}
	
	function goPrintBebas() {
	
		popup('index.php?page=pop_notabebas_sumbangan&id=<?= $r_key ?>&nomor='+document.getElementById('txtnomer').value+'&date='+document.getElementById('txttgl').value+'&idp=<?= $r_key2 ?>',530,450);
	
	}
	function goPrintKartu() {
		window.open('index.php?page=cetak_thanx&code=k&id=<?= $r_key ?>&key=<?= $rkey ?>&sum=<?= $sum ?>','_blank');
	}
	function goPrintCover() {
		window.open('index.php?page=cetak_thanx&code=a&id=<?= $r_key ?>&key=<?= $rkey ?>','_blank');
	}
	
	function updateData(key,keyid) {
	if(cfHighlight("u_nama,u_auth1,u_edisi,u_jumlah")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateSumb(key,keyid);
		}
	}
	
	function goUpdateSumb(key,keyid){
		document.getElementById("act").value="update";
		document.getElementById("key").value=key;
		document.getElementById("keyid").value=keyid;
		goSubmit();
	}
	
	function goDeleteSumbangan(key,keyid,nama){
		var hapus = confirm("Apakah Anda yakin akan menghapus sumbangan '"+nama+"'");
		if(hapus){
			document.getElementById("act").value="hapus";
			document.getElementById("key").value=key;
			document.getElementById("keyid").value=keyid;
			
			goSubmit();
		}
	}
	</script>
	<script type="text/javascript">

	var mouseX = 0; 
	var mouseY = 0;

	var ie  = document.all; 
	var ns6 = document.getElementById&&!document.all;

	var isMenuOpened  = false ;
	var menuSelObj = null ;
	var overpopupmenu = false;
	var gParam;

	var posx = 0; 
	var posy = 0;

	</script>
</html>
	