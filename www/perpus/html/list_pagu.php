<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);
 
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];	
	// require tambahan
	
	
	// definisi variabel halaman

	$p_window = '[PJB LIBRARY] Daftar Pagu';
	$p_title = 'Daftar Pagu';
	$p_tbheader = '.: Daftar Pagu :.';
	$p_col = 9;
	$p_tbwidth = 520;
	$p_lihat = Helper::navAddress('pop_paguspesial.php');
	$p_id = "pagu";

	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_row = 15;
	//$nomer=1;
	
	// sql untuk mendapatkan isi list

	$p_sqlstr="select * from pp_settingpagu";
			
	if (!empty($_POST))
	{
		$r_key=Helper::removeSpecial($_POST['act']);
		$key=Helper::removeSpecial($_POST['key']);
		if($r_key=='hapus' and $c_delete){
			$conn->StartTrans();
			Sipus::DeleteBiasa($conn,pp_pagulh,periodeakad,$key);
			
			Sipus::DeleteBiasa($conn,pp_settingpagu,periodepagu,$key);
				
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
					$errdb = 'Penghapusan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penghapusan data berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
					}
			
			
		
		}
		elseif($r_key=='change' and $c_edit){
			$tahun=Helper::removeSpecial($_POST['tahun']);
				
			$record=array();
			$record['isaktif']=false;
			Sipus::UpdateBiasa($conn,$record,pp_settingpagu,1,1);
			
			$record['isaktif']=true;
			Sipus::UpdateBiasa($conn,$record,pp_settingpagu,periodepagu,$tahun);
			
			if($conn->ErrorNo() == 0)
				Helper::redirect();
		}
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
	}else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
		if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	$p_sqlstr.=" order by periodepagu desc";
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);

	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	$period=$conn->GetRow("select periodepagu from pp_settingpagu where isaktif=true");	
	$th=Helper::removeSpecial($_POST['tahun']);
	//list combo
	$rs_cb = $conn->Execute("select periodeakad::integer-1||' - '||periodeakad as tahun,periodeakad from pp_pagulh group by periodeakad order by periodeakad desc");
	if ($th=='')
	$th=$period['periodepagu'];
	$l_akad = $rs_cb->GetMenu2('tahun',$th,false,false,0,'id="tahun" class="ControlStyle" style="width:100" onchange="javascript:goChange()"');
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0"  onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<?php include('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
			<? include_once('_notifikasi.php') ?><br />
			<header style="width:520px;">
				<div class="inner">
					<div class="left title">
						<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
						<h1><?= $p_title; ?></h1>
					</div>
				<div class="right">
				  <a href="index.php?page=pop_paguspesial" class="buttonshort" style="width:85px;text-align:right;padding-right:10px;margin-top:4px;margin-right:4px;"><span class="new">Pagu baru</span></a></td>
				</div>
				</div>
			</header>
		  <table width="520" border="0" cellpadding="4" cellspacing=0 class="GridStyle">
			<!--tr height="30">
			  <td align="center" class="PageTitle" colspan="<?= $p_col ?>"><?= $p_title; ?>	 
			  </td>
			</tr-->
			<!--tr>
				<td colspan="<?= $p_col ?>" align="center"><? include_once('_notifikasi.php') ?></td>
			</tr-->
			<!--tr>
			  <td colspan="7">
			  <table cellpadding="0" cellspacing="0" border="0" width="100%">
				  <tr>
				  <td>
				<a href="index.php?page=pop_paguspesial" class="buttonshort"><span class="new">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pagu baru</span></a></td>
					<td align="right" valign="middle">
					<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;"> 
					<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;"> 
					<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;"> 
					<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;"></td>
				  </tr>
			  </table></td>
			</tr-->
			<!--tr>
				<td colspan=5>Setting Periode Aktif : &nbsp; <?//= $l_akad ?>
				</td>>
			</tr-->
			<tr height="20">
			  <td width="100" nowrap align="center" class="SubHeaderBGAlt">Periode Pagu</td>
			  <td width="40%" nowrap align="center" class="SubHeaderBGAlt">Bulan Mulai</td>
			  <td width="30%" nowrap align="center" class="SubHeaderBGAlt">Bulan Selesai</td>
			  <td width="10%" nowrap align="center" class="SubHeaderBGAlt">Jumlah Pagu</td>
			  <!--<td width="10%" nowrap align="center" class="SubHeaderBGAlt">Status</td>-->
			  <td width="20%" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
			</tr>
			<?php
				$i = 0;
					// mulai iterasi
					while ($row = $rs->FetchRow()) 
					{
						
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++; 
						if ($row['periodepagu'] == '0') $rowstyle = 'YellowBG';
			?>
			<tr class="<?= $rowstyle ?>" height="20" valign="middle">
			  <td align="center"><?= ($row['periodepagu']-1)."-".$row['periodepagu']; ?></td>
			  <td align="left"><?= Helper::bulanInd($row['bulanstart']); ?></td>
			  <td align="left"><?= Helper::bulanInd($row['bulanend']); ?></td>
			  <td align="center"><?= Helper::formatNumber($row['besarpagu'],'0',true,true); ?></td>
			<!--  <td align="center"><?//=  $row['isaktif']=='t' ? 'Aktif' : 'Nonaktif';?></td>-->
			  <td align="center">
			  <? if ($c_delete) { ?>
			  <img src="images/batal.png" title="Hapus" onClick="goDeL('<?= $row['periodepagu']; ?>')" style="cursor:pointer">
			   <? }?> </tr>
			<?php
		}
				
				if ($i==0) {
			?>
			<tr height="20">
			  <td align="center" colspan="<?= $p_col; ?>"><b>Daftar Anggota kosong</b></td>
			</tr>
			<?php } ?>
			<tr>
			  <td class="PagerBG footBG" align="right" colspan="5"> Halaman
				<?= $p_page ?>
				/
				<?= $p_lastpage ?>
				  <?= $p_status ?>
			  </td>
			</tr>
		  </table>
				<?php require_once('inc_listnav.php'); ?>
		  <br>
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="act" id="act">
		</form>
		</div>
	</div>
</div>

</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>
<script type="text/javascript">
function goDeL(key){
	var del=confirm("Apakah Anda yakin akan melakukan hapus data ?");
	if(del){
	document.getElementById("act").value="hapus";
	document.getElementById("key").value=key;
	goSubmit();
	}
}
function goChange() {
	document.getElementById("act").value="change";
	goSubmit();
}
</script>
</html>