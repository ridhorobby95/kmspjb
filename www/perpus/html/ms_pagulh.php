<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	$c_delete = $a_auth['candelete'];

	// pengecekan tipe session user
	 $a_auth = Helper::checkRoleAuth($conng);

	// definisi variabel halaman
	$p_dbtable = 'pp_pagulh';
	$p_window = '[PJB LIBRARY] Daftar Pagu Anggota';
	$p_title = 'Daftar Pagu Anggota';
	$p_id = "ms_pagulh";
	$p_col = 4;
	$p_tbwidth = 620;
	$p_row = 15;
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select idanggota, periodeakad, sisapagu from $p_dbtable where 1=1";
  	
	// pengaturan ex
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		$r_key2= Helper::removeSpecial($_POST['key2']);
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$cari=Helper::removeSpecial($_POST['i_idanggota']);
		$jum=Helper::removeSpecial($_POST['i_periodeakad']);
		
		
		//simpan session
		$_SESSION[$p_id.'.page'] = $p_page;
		
		if($r_aksi == 'insersi' and $c_add) {
			$cari=Helper::removeSpecial($_POST['i_idanggota']);
			$check=$conn->GetRow("select a.idanggota,j.setpagu from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota where a.idanggota='$cari'");
			$c_pagu=$conn->GetRow("select besarpagu from pp_settingpagu where periodepagu='$jum'");
			if($check['setpagu']==1 and $c_pagu){
			$record = array();
			$record['idanggota'] = Helper::cStrNull($_POST['i_idanggota']);
			$record['periodeakad'] = Helper::cStrNull($_POST['i_periodeakad']);
			$record['pagulh'] = $c_pagu['besarpagu'];
			$record['sisapagu']=$record['pagulh'];
			$record['sisasementara']=$record['sisapagu'];
			
			Helper::Identitas($record);
			
			$err=Sipus::InsertBiasa($conn,$record,$p_dbtable);
			
			if($err != 0){
				$errdb = 'Penyimpanan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penyimpanan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
			}else{
				$errdb = 'Data Anggota tidak ditemukan atau tidak diijinkan memiliki pagu.';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
				}
		}
		else if($r_aksi == 'update' and $c_edit) {
			$cari=Helper::removeSpecial($_POST['u_idanggota']);
			$check=$conn->GetRow("select a.idanggota,j.setpagu from ms_anggota a join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota where a.idanggota='$cari'");
			if($check['setpagu']==1){
			$record['idanggota'] = Helper::cStrNull($_POST['u_idanggota']);
			$record['periodeakad'] = Helper::cStrNull($_POST['u_periodeakad']);
			$record['sisapagu']=Helper::cStrNull($_POST['u_sisapagu']);
			$record['sisasementara']=$record['sisapagu'];
			Helper::Identitas($record);
			
			$col = $conn->Execute("select * from $p_dbtable where idanggota='$r_key' and periodeakad='$r_key2'");
			$sql = $conn->GetUpdateSQL($col,$record);
		
		
		if($sql !='')
			$conn->Execute($sql);
			
			if($err != 0){
				$errdb = 'Update data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Update data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); 
				}
		}else{
				$errdb = 'Data Anggota tidak ditemukan atau tidak diijinkan memiliki pagu.';	
				Helper::setFlashData('errdb', $errdb);
				Helper::redirect();
				}
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$sql="delete from pp_pagulh where idanggota = '$r_key' and periodeakad='$r_key2'";
			$conn->Execute($sql);
			
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
				}
				else {
				
				$sucdb = 'Penghapusan data berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				Helper::redirect(); }
		}
		else if($r_aksi == 'sunting' and $c_edit) {
			$p_editkey = $r_key;
		}
	
	}
	
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	$period=Sipus::getPaguAktif($conn);	
	$th=Helper::removeSpecial($_POST['tahun']);
	//list combo
	$rs_cb = $conn->Execute("select periodeakad-1||' - '||periodeakad as tahun,periodeakad from pp_pagulh group by periodeakad order by periodeakad desc");
	if ($th=='')
	$th=$period;
	$l_akad = $rs_cb->GetMenu2('tahun',$th,false,false,0,'id="tahun" class="ControlStyle" style="width:100" onchange="javascript:goSubmit()"');
	
	$p_sqlstr.=" and periodeakad='$th'";
	
	$filternpk = Helper::removeSpecial($_POST['filternpk']);
	if($filternpk)
		$p_sqlstr .= " and idanggota='$filternpk' " ;
	
	// eksekusi sql list
	$p_sqlstr.=" order by idanggota";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}
	//echo "r_key".$r_key." rkey2 ".$r_key2;

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<script type="text/javascript" src="scripts/foredit.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initPage();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" >
<?php include('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">


<table width="<?= $p_tbwidth ?>" border="0" cellpadding="0" cellspacing="0" class="GridStyle">
	<tr height="30">
		<td align="center" valign="top" colspan="4"><span class="PageTitle"><?= $p_title; ?></span><br><? include_once('_notifikasi.php'); ?></td>
	</tr>
	<tr>
	<td colspan=4 align="center">
	<table cellpadding=0 cellspacing=0 width="100%" border=0>
	<tr height="20"> 
		<td  align="left" width="130">
		<a href="index.php?page=pop_paguspesial" class="buttonshort"><span class="new">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pagu baru</span></a></td>
		<td  align="left">
		<a href="javascript:goRefresh()" class="buttonshort"><span class="refresh">Refresh</span></a></td>
		<td align="right">
		<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.gif" style="cursor:pointer;">
		<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.gif" style="cursor:pointer;">
		<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.gif" style="cursor:pointer;">
		<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.gif" style="cursor:pointer;">
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr height=35>
		<td width="150" >Filter Periode Akademik </td><td>: &nbsp;<?= $l_akad; ?></td>
		
	</tr>
	<tr>
		<td>Id Anggota </td><td>: &nbsp;<input type="text" name="filternpk" id="filternpk" value="<?= $filternpk ?>"></td>
		<td><input type="button" name="btnfilter" value="Filter" onclick="goSubmit()"></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr height="20"> 
		<td width="100" nowrap align="center" class="SubHeaderBGAlt">Id Anggota</td>
		<td width="150" nowrap align="center" class="SubHeaderBGAlt">Periode Akademik</td>
		<td width="120" nowrap align="center" class="SubHeaderBGAlt">Sisa Pagu</td>
		<td width="80" nowrap align="center" class="SubHeaderBGAlt">Aksi</td>
	</tr>
	<?php
		$i = 0;
		while ($row = $rs->FetchRow()) 
		{
			if($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			if(strcasecmp($row['idanggota']."-".$row['periodeakad'],$p_editkey."-".$r_key2)) { // row tidak diupdate
	?>
	<tr class="<?= $rowstyle ?>" height=20>
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Sunting Data" onclick="goSunting('<?= $row['idanggota']; ?>','<?= $row['periodeakad'] ?>');" class="link"><?= $row['idanggota']; ?></u>
		<? } else { ?>
			<?= $row['idanggota']; ?>
		<? } ?>
		</td>
		<td align="center"><?= ($row['periodeakad']-1).' - '.$row['periodeakad']; ?></td>
		<td align="right"><?= Helper::formatNumber($row['sisapagu'],'0',true,true); ?>&nbsp;&nbsp;</td>
		<td align="center">
		<? if($c_delete) { ?>
			<u title="Hapus Data" onclick="goDelete('<?= $row['idanggota']; ?>','<?= $row['periodeakad']; ?>');" class="link">[hapus]</u>
		<? } ?>
		</td>
	</tr>
	<?php
			} else { // row diupdate
	?>
	<tr class="<?= $rowstyle ?>" height=30> 
		<td align="center"><?= UI::createTextBox('u_idanggota',$row['idanggota'],'ControlStyle',50,10,true,'onKeyDown="etrUpdate(event);"'); ?></td>
		<td align="center"><?= UI::combotahun('u_periodeakad',100,$row['periodeakad']); ?></td>
		<td align="right"><?= UI::createTextBox('u_sisapagu',$row['sisapagu'],'ControlStyle',14,10,true) ?>&nbsp;&nbsp;</td>
		<td align="center">
		<? if($c_edit) { ?>
			<u title="Update Data" onclick="goEdit('<?= $row['idanggota']; ?>','<?= $row['periodeakad']; ?>');" class="link">[update]</u>
		<? } ?>
		</td>
	</tr>
	<?php 	}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>

	<?php } ?> 
	
	
	<? if($c_add) { ?>
	<tr class="LiteSubHeaderBG"> 
		<td align="center">
		<?= UI::createTextBox('i_idanggota','','ControlStyle',50,10,true,'onKeyDown="etrInsert(event);"'); ?>
		<? if($c_edit) { ?><img src="images/popup.png" style="cursor:pointer;" title="Lihat Anggota" onClick="popup('index.php?page=pop_anggota&code=pa',620,500);">
		<? } ?>
		</td>
		<td align="center" height=30>
		<?= UI::combotahun('i_periodeakad',100,$th); ?></td>
		<td>&nbsp; </td>
		<td align="center"><input type="button" value="Simpan" onClick="insertData()" class="ControlStyle"></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="PagerBG" align="right" colspan="4"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
		
	</tr>
</table><br>
<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="act" id="act">
<input type="hidden" name="key" id="key">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="scroll" id="scroll">
</form>
</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

</script>

<script type="text/javascript">

function etrInsert(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		insertData();
}

function etrUpdate(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		updateData('<?= $p_editkey; ?>');
}

function initPage() {
	initScroll(<?= $_POST['scroll'] ? floor($_POST['scroll']) : 0; ?>);
	<? if($p_editkey != '') { ?>
	document.getElementById('u_idanggota').focus();
	<? } else { ?>
	document.getElementById('i_idanggota').focus();
	<? } ?>
}

function insertData() {
	if(cfHighlight("i_idanggota,i_periodeakad"))
		goInsertIP();
}

function updateData(key) {
	if(cfHighlight("u_idanggota,u_periodeakad")) {
		document.getElementById("scroll").value = document.body.scrollTop;
		goUpdateIP(key);
	}
}



function goEdit($key1,$key2){
if(cfHighlight("u_idanggota,u_periodeakad")) {
	document.getElementById("act").value='update';
	document.getElementById("key").value=$key1;
	document.getElementById("key2").value=$key2;
	goSubmit();
	}
}

function goSunting(key,key2){
	document.getElementById("act").value = "sunting";
	document.getElementById("key").value=key;
	document.getElementById("key2").value=key2;
	goSubmit();
}

function goDelete($key1,$key2){
	document.getElementById("act").value='hapus';
	document.getElementById("key").value=$key1;
	document.getElementById("key2").value=$key2;
	goSubmit();
}

function goRefresh(){
	document.getElementById('filternpk').value='';
	goSubmit();
}
</script>
</html>