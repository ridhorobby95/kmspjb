<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	$jabatan = Helper::getJabatan();
	$spvSdm = 'SPV SENIOR SDM';
	$namaRole = $_SESSION['PJB']['namarole'];
//var_dump($namaRole);
//var_dump($jabatan['nama']);
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and u.idunit in ($units) ";

//echo"<pre>";var_dump($_SESSION);		
	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Daftar Usulan Unit';
	$p_title = 'Daftar Usulan Unit';
	$p_tbheader = '.: Daftar Usulan Unit :.';
	$p_col = 6;
	$p_filedetail = Helper::navAddress('data_usulanunit.php');
	
	$p_id = 'unit_usul';
	$p_defsort = 'tglusulan';
	$p_row = 10;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']); 
		$id=Helper::removeSpecial($_REQUEST['key']);
	
		if($r_aksi=='proses'){
			$p_proses=$conn->GetRow("select * from pp_usul where idusulan=$id");
			$recproses=array();
			$recproses['idusulan']=$id;
			$recproses['judul']=Helper::removeSpecial($p_proses['judul']);
			$recproses['hargausulan']=Helper::removeSpecial($p_proses['hargausulan']);
			$recproses['qtyusulan']=Helper::removeSpecial($p_proses['qtyusulan']);
			$recproses['keterangan']=Helper::cStrNull($p_proses['keterangan']);
			$recproses['authorfirst1']=Helper::cStrNull($p_proses['authorfirst1']);
			$recproses['authorlast1']=Helper::cStrNull($p_proses['authorlast1']);
			$recproses['authorfirst2']=Helper::cStrNull($p_proses['authorfirst2']);
			$recproses['authorlast2']=Helper::cStrNull($p_proses['authorlast2']);
			$recproses['authorfirst3']=Helper::cStrNull($p_proses['authorfirst3']);
			$recproses['authorlast3']=Helper::cStrNull($p_proses['authorlast3']);
			$recproses['penerbit']=Helper::cStrNull($p_proses['penerbit']);
			$recproses['tahunterbit']=Helper::cStrNull($p_proses['tahunterbit']);
			$recproses['isbn']=Helper::cStrNull($p_proses['isbn']);
			$recproses['edisi']=Helper::removeSpecial($p_proses['edisi']);
			$recproses['stsusulan']=1;
			$p_check=$conn->GetRow("select idusulan from pp_orderpustaka where idusulan=$id");
			
			if(!$p_check)
			$err=Sipus::InsertBiasa($conn,$recproses,pp_orderpustaka);
			else
			$err=Sipus::UpdateBiasa($conn,$recproses,pp_orderpustaka,idusulan,$id);
			
			$record['statususulan']=1;
			$err=Sipus::UpdateBiasa($conn,$record,pp_usul,idusulan,$id);
			
			if($err != 0){
					$errdb = 'Proses Usulan Gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
			else {
			#kirim notifikasi : proses pengusulan unit
					$sucdb = 'Proses Usulan Berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					Helper::redirect(); 
				}
		}
	}
	$a_col = array('tglusulan','idunit','namapengusul','judul','hargausulan','stspengadaan','stspo','ststtb');
	$i = 0;
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'Tanggal','D',60);
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'Unit Kerja','C',160);
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'Judul','C',170);
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'Harga','C',170);
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'PE','N',50);
	Helper::setTableArray($a_tbprop[$a_col[$i++]],'TTP','N',50);
	
	// sql untuk mendapatkan isi list
	$p_sqlstr = "select u.*,o.stsusulan,o.stspengadaan,o.stspo,o.ststtb
		from pp_usul u 
		left join pp_orderpustaka o on o.idusulan=u.idusulan
		where idunit is not null $sqlAdminUnit ";
	if (!empty($_POST))
	{
		$status=Helper::removeSpecial($_POST['status']);
		
		$_SESSION[$p_id]['status'] = $status;
	}else{
		$status= $_SESSION[$p_id]['status'];
	}
	
	if($status=='0')
		$p_sqlstr.=" and o.stsusulan is null";
	elseif($status=='1')
		$p_sqlstr.=" and o.stsusulan='$status'";

	// proses filtering dari menu tree
	$a_cat = explode(',',Helper::removeSpecial($_POST['cat']));
	$a_str = explode(',',Helper::removeSpecial($_POST['str']));
	
	$n = count($a_cat);
	for($i=0;$i<$n;$i++)
		$a_tcat[$a_cat[$i]][] = $a_str[$i];
	
	foreach($a_tcat as $k => $v) {
		if($k == 'satuankerja') {
			$a_tsk = array();
			for($i=0;$i<count($v);$i++) {
				$a_sk = explode('_',$v[$i]);
				for($j=0;$j<count($a_sk);$j++)
					$a_tsk[$a_sk[$j]] = true;
			}
			$a_sk = array();
			foreach($a_tsk as $ki => $vi)
				$a_sk[] = $ki;
			$p_sqlstr .= " and idunit in ('".implode("','",$a_sk)."')";
		}
	}
  	
	// pengaturan ex
	if (isset($_REQUEST['page']) or isset($_REQUEST['sort']) or isset($_REQUEST['filter']))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
	}
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1

	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		$filterlabel = '';
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			//$filterdata = Helper::removeSpecial(rawurlencode($filterarray[$i+1]));
			$filterdata = Helper::removeSpecial($filterarray[$i+1]);
			if($filterdata == '')
				$filterdata = '*';
			$filtertype = $filterarray[$i+2];
			if($filtertype == '')
				$filtertype = 'C';
			
			// membuat label filter
			$filterlabel .= '<br>'.$a_tbprop[$filtercol]['label'].': '.$filterdata;
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterstr .= 'substr('.$filtercol.",9,2){$c_sc}'-'{$c_sc}substr(".$filtercol.",6,2){$c_sc}'-'{$c_sc}
											substr(".$filtercol.",1,4) like '".strtr(trim($filterdata),'*','%')."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc ";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$a_tbprop[$p_defsort]['sort'] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$a_tbprop[$col]['sort'] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage(); //tidak terdeteksi true atau false. Baru terdeteksi setelah onClick button Refresh.
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	$a_proses = array('x' => '-- Semua --', '0' => 'Belum Diproses', '1' => 'Telah Diproses');
	$l_proses = UI::createSelect('sts',$a_proses,$status,'ControlStyle',$c_edit,'onchange="goStatus()"');
	
?>
<html>

<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<script type="text/javascript" src="scripts/forpagerx.js"></script>
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);">
	<div align="center">
		<br><? include_once('_notifikasi.php'); ?>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<div style="width:95%;margin:0 auto;" class="filterTable">
			<table width="100%" border="0" style="border:0 none;">
				<tr>
					<td width="150">Status</td>
					<td>: &nbsp; <?= $l_proses ?> </td>
				</tr>
			</table>
		</div>
		</form>
		<br/>
		<header style="width:95%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/keanggotaan.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Pengusulan Unit</h1>
				</div>
				<div class="right">
				  <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data" onClick="goPostX('<?= $p_filedetail; ?>')">+</div>
				  <div class="addButton" style="float:left; margin:right:10px;" title="refresh" onClick='goRefresh()'><img width="14" src="images/tombol/icq2.png"/></div>
				</div>
			</div>
		</header>
		<table width="95%" cellpadding="4" cellspacing="0" class="GridStyle">
			<? $i = -1; ?>
			<tr height="20">
			<?	for($i=0;$i<$p_col;$i++) { 
					$row = $a_tbprop[$a_col[$i]]; ?>
				<td nowrap<?= empty($row['lebar']) ? '' : ' width="'.$row['lebar'].'"'; ?> align="center" class="SubHeaderBGAlt thLeft" style="cursor:pointer;" id="<?= $a_col[$i]; ?>:<?= $row['tipe']; ?>"><?= $row['label']; ?> <?= $row['sort']; ?></td>
			<?	} ?>
				<td width="60" align="center" class="SubHeaderBGAlt thLeft">Aksi</td>
			</tr>
			<?php
				$i = 0;
				if($showlist) {
					while ($row = $rs->FetchRow()) 
					{
						if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
			?>
			<tr class="<?= $rowstyle ?>"> 
				<td align="center"><?= Helper::formatDate($row['tglusulan']); ?></u></td>
				<td><?= $row['namapengusul']; ?></td>
				<td><?= $row['judul']; ?></td>
				<td align="right"><?= number_format($row['hargausulan']); ?></td>
				<td align="center"><?= $row['stspengadaan']=='1' ? "<img title=\"Proses Selesai\" src='images/centang.gif'>" : ($row['stspengadaan']==0  ? "<img title=\"Proses Menunggu\" src='images/menunggu.png'>" : "<img title=\"Pengusulan Ditolak\" src='images/uncheck.gif'>") ?></td>
				<td align="center"><?= $row['ststtb']=='1' ? "<img title=\"Proses Selesai\" src='images/centang.gif'>" : ($row['ststtb']==0  ?  "<img title=\"Proses Menunggu\" src='images/menunggu.png'>" : "<img title=\"Pengusulan Ditolak\" src='images/uncheck.gif'>") ?></td>
				<td align="center">
				<? if($c_edit){ ?>
				<u title="Detail Usulan" onclick="goSunting('<?= $row['idusulan']; ?>','<?= $p_filedetail; ?>');" class="link"><img src="images/edited.gif"></u>
				<? if ($row['stsusulan']!=1){
				if($jabatan['nama'] == $spvSdm || $namaRole == 'Administrator' || $namaRole == 'SPV SDM') { ?>
				<u onClick="goProses('<?= $row['idusulan'] ?>')"  title="Proses Usulan Unit" class="link"><img src="images/proses.png"></u>
				<? }else{?>
				<img title="Tidak Memiliki Akses" src="images/proses2.png">
				<? } } else { ?>
				<img title="Usulan Unit Sedang Diperoses" src="images/proses2.png">
				<? }} else { ?>
				<img title="Tidak Dapat Mengedit" src="images/edited2.gif">
				<img title="Tidak Dapat Diproses" src="images/proses2.png">
				<? }?>
				</td>
				</td>
			</tr>
			<?php
					}
				}
				if ($i==0) {
			?>
			<tr height="20">
				<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
			</tr>
			<?php } ?>
			<tr>
				<td class="footBG" colspan="7">
					<table width="100%" cellpadding="0" border="0" style="border:0 none;">
						<tr>
							<td class="footBG"><strong>Halaman <?= $p_page ?>/<?= $p_lastpage ?> (<?= $p_recount ?> Usulan) <?= $p_status ?></strong></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div style="width:100%;float:left;">
	<center>
		<div style="width:<?= $p_tbwidth ?>px;">
			<div class="extension bottom inright pagination">
				<div class="dataTables_paginate paging_full_numbers" id="DataTables_Table_0_paginate">
					<a class="first paginate_button <?= $p_page == 1 ? 'paginate_button_disabled' : '"' ?>" title="first" id="firstButton" onClick="goFirst()"><<</a>
					<a class="previous paginate_button <?= $p_page == 1 ? 'paginate_button_disabled' : '"' ?>" title="previous" id="prevButton" onClick="goPrev()"><</a>
					<a class="next paginate_button <?= $p_page == $p_lastpage ? 'paginate_button_disabled' : '"' ?>" title="next" id="nextButton" onClick="goNext(<?= $p_lastpage; ?>)">></a>
					<a class="last paginate_button <?= $p_page == $p_lastpage ? 'paginate_button_disabled' : '"' ?>" title="last" id="lastButton" onClick="goLast(<?= $p_lastpage; ?>)">>></a>
				</div>
			</div>
		</div>
	</center>
</div>
		<table>
			<tr>
				<td align="center">
					<table align="center" cellspacing="3" cellpadding="2" width="200" border=0>
						<tr>
							<td colspan=2><u>Keterangan :</u></td>
						</tr>	
						<tr>
							<td width=10>PE</td><td>= Proses Pengadaan</td>
						</tr>
						<tr>
							<td width=10>TTP</td><td>= Tanda Terima Pustaka</td>
						</tr>
						<tr>
							<td colspan="2" width=10>&nbsp;</td>
						</tr>
					</table>
				</td>
				<td>
					<table align="center" cellspacing="3" cellpadding="2" width="200" border=0>
						<tr>
							<td colspan=2><u>Keterangan :</u></td>
						</tr>
						<tr>
							<td width=10><img src="images/centang.gif" title="Diterima"></td><td>= Proses Selesai </td>
						</tr>
						
						<tr>
							<td width=10><img src="images/wait.gif" title="Menunggu"></td><td>= Proses Menunggu</td>
						</tr>
						<tr>
							<td width=10><img src="images/uncheck.gif" title="Ditolak"></td><td>= Proses Ditolak</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
		<input type="hidden" name="id" id="id">
		<?	if(!empty($filterlabel)) {
				echo '<table width="100%"><tr><td>';
				echo $filterlabel;
				echo '</td></tr></table>';
			} ?>

		<div id="popFilter" class="FilterDialog" style="position:absolute;display:inline;visibility:hidden;top:0px;left:0px;z-index:10000;">
		Filter Criteria <br>
			<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);">
		</div>

		<div id="popPaging" class="menubar" style="position:absolute;visibility:hidden;top:0px;left:0px;z-index:10000;">
		<table width=100 class="menu-body">
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
				<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
			</tr>
			<tr>
				<td class="separator"><div class="separator-line"></div></td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td id="tdfilter"><img align="absmiddle" id="imgfilter" src="images/addfilter.gif"> Filter ...</td>
			</tr>
			<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
				<td onClick="resetFilter();"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
			</tr>
		</table>
		</div>
	</div>

	<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
		<input type="hidden" name="key" id="key">
		<input type="hidden" name="idusul" id="idusul">
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="fmenu" id="fmenu">
	</form>

</body>
<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>
<script type="text/javascript">

cat = "<?= $_POST['cat']; ?>";
str = "<?= $_POST['str']; ?>";
page = <?= $p_page; ?>;
sorts = "<?= $p_sort; ?>";
filter = "<?= $p_filter; ?>";
xlist = "<?= $i_phpfile; ?>";

$(document).ready(function(){
	initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);
	
	$(document).click(function(e) {
		if(e.target.className != "SubHeaderBGAlt")
			$("#popPaging").css("visibility","hidden");
		if(e.target.id != "tdfilter" && e.target.id != "imgfilter" && e.target.id != "txtFilter" && e.target.id != "tdfilterdialog")
			$("#popFilter").css("visibility","hidden");
	});
	$(".SubHeaderBGAlt").click(function(e) {
		$(this).showPopup({popup: "popPaging", x: e.pageX, y: e.pageY});
		colparam = $(this).attr("id");
	});
	$("#tdfilter,#imgfilter").click(function(e) {
		$(this).showPopup({popup: "popFilter", x: e.pageX, y: e.pageY});
		$("#txtFilter").focus();
	});
});
		
	
function goSunting(key,xlist) {
	sent = "&cat=" + cat + "&str=" + str + "&page=" + page + "&sort=" + sorts + "&filter=" + filter;
	sent += "&key=" + key;
	$("#" + xtdid).divpost({page: xlist, sent: sent});
}

function goPostX(xlist) {
	sent = "&cat=" + cat + "&str=" + str + "&page=" + page + "&sort=" + sorts + "&filter=" + filter;
	$("#" + xtdid).divpost({page: xlist, sent: sent});
}

function goProses(key){
	var conf=confirm("Apakah Anda yakin akan proses usulan ini ?");
	if(conf){
		sent = "&cat=" + cat + "&str=" + str + "&page=" + page + "&sort=" + sorts + "&filter=" + filter;
		sent += "&key=" + key + "&act=proses";
		$("#" + xtdid).divpost({page: xlist, sent: sent});
	}
}

function goStatus(){
	document.getElementById("act").value="status";
	var sts = document.getElementById("sts").value;
	sent = "&cat=" + cat + "&str=" + str + "&page=" + page + "&sort=" + sorts + "&filter=" + filter;
	sent += "&status=" + sts + "&act=status";
	$("#" + xtdid).divpost({page: xlist, sent: sent});
}
</script>

</html>