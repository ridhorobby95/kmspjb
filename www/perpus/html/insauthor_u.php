<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
//$conn->debug = true;	
	// otentiksi user
	$a_auth = Helper::checkRoleAuth($conng,false);
	
	$c_add=$a_auth['cancreate'];
	$id=0;//$conn->GetOne("select last_value from ms_author_idauthor_seq");
	$r_urut = Helper::removeSpecial($_REQUEST['urut']);
	if(!empty($_POST) and $c_add){
		$nmdepan = Helper::removeSpecial($_POST['nmdepan']);
		$nmbelakang = Helper::removeSpecial($_POST['nmbelakang']);
		if ($nmdepan!=''){
			$ada = $conn->GetOne("select count(*) from ms_author where upper(namadepan) = upper('$nmdepan') and upper(namabelakang) = upper('$nmbelakang') ");
			if($ada > 0){
				$errdb = '<br/>Data Pengarang sudah ada.';	//echo $errdb;die();
				Helper::setFlashData('errdb', $errdb);
			}elseif($ada == 0){
				$record=array();
				$record['namadepan']=Helper::removeSpecial(ucfirst($_POST['nmdepan']));
				$record['namabelakang']=Helper::removeSpecial(ucfirst($_POST['nmbelakang']));
				$record['isbadan'] = ($_POST['isBadan']?1:0);

				Helper::Identitas($record); 
				$err=Sipus::InsertBiasa($conn,$record,ms_author);
				
				if($err !=0){
					$errdb = 'Penyimpanan data gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$checks=$conn->GetOne("select last_value from ms_author_idauthor_seq");
					$id = $checks;
					$new = 1;
				}
			}
		}
	}
	$auth = $conn->GetRow("select namadepan,namabelakang, isbadan from ms_author where idauthor=$id");
?>

<html>
<head>
<title>Shortcut Author Baru</title>
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
		
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">

</head>
<body>
<div id="wrapper" style="width:auto;margin:0;padding:0;">
	<div class="SideItem" id="SideItem" style="width:auto;margin:0;padding:7px;">
<form  method="post" name="insertauthor">
<div class="filterTable">
<table border="0" width="100%">
	<th height=25 colspan="2"><strong>Shortcut Author Baru<? include_once('_notifikasi.php'); ?></strong></th>
	</tr>
	<tr>
		<td align="Left" width="150">Nama Depan</td>
		<td align="center">: <?= UI::createTextBox('nmdepan','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
		
	</tr>
	<tr> 
		<td align="Left" width="30%">Nama Belakang</td>
		<td align="center">: <?= UI::createTextBox('nmbelakang','','ControlStyle',30,30,true,'onKeyDown="etrInsert(event);"'); ?></td>
	</tr>
	<tr> 
		<td align="Left" width="30%">Badan/Institusi</td>
		<td align="center">: <input type="checkbox" name="isBadan" id="isBadan"></td>
	</tr>
	<tr>
		<td><input type="button" value="Simpan" onClick="tambahauthor()" class="ControlStyle" style="cursor:pointer"></td>
	</tr>
</table>
</div>
<input type="hidden" id="idnya" name="idnya" value="<?= $id ?>">
<input type="hidden" name="urut" id="urut" value="<?= $r_urut ?>">
</form> 
</div>
</div>
</body>

<script>
	function tambahauthor(){
	
		var namadepan=document.getElementById("nmdepan").value;
		var namaeb=document.getElementById("nmbelakang").value;
		var idne=document.getElementById("idnya").value;
		if(namadepan!=''){
			document.insertauthor.submit();
		}
		else {
			alert('Masukkan Nama Author dengan benar');
			document.getElementById("nmdepan").focus;
		}
	}
	
	function etrInsert(e) {
		var ev= (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			tambahauthor();
	}
	function goSend2(key,depan,belakang,urut) {
		window.opener.document.getElementById("idauthor"+urut).value = key;
		window.opener.document.getElementById("authorfirst"+urut).value = depan.replace("_##_","'");
		window.opener.document.getElementById("authorlast"+urut).value = belakang.replace("_##_","'");
	}
</script>
</html>

<?php
	if($new==1) { ?>
		<script>
		window.opener.loadauthor();
		goSend2('<?= $id ?>','<?= $auth['namadepan']?>','<?=$auth['namabelakang'] ?>','<?=$r_urut; ?>');
		window.close();
		</script>
<? } ?>