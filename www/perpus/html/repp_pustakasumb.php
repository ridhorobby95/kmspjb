<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);

	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and a.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	$r_lokasi = Helper::removeSpecial($_REQUEST['kdlokasi']);

	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Sumbangan Pustaka';
	
	$p_namafile = 'lap_pustakasumb'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select s.*,o.judul,o.authorfirst1,o.authorlast1,o.qtysumbangan
		from pp_sumbangan s
		join pp_orderpustaka o on o.idsumbangan=s.idsumbangan
		left join ms_anggota a on a.idanggota = s.idanggota 
		where to_char(s.tglsumbangan,'YYYY-mm-dd') between '$r_tgl1' and '$r_tgl2' $sqlAdminUnit ";
		
	if($r_lokasi){
		$sql .= " and a.idunit in (select l.idunit from lv_lokasi l where l.kdlokasi = '$r_lokasi')";	
	}
		
	$sql .= "order by s.tglsumbangan";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Laporan Sumbangan Pustaka</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="150" align="center"><strong>Nama Penyumbang</strong></th>
    <th width="200" align="center"><strong>Judul Pustaka</strong></th>
	<th width="100" align="center"><strong>Pengarang</strong></th>
	<th width="100" align="center"><strong>Jumlah</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $rs['namapenyumbang'] ?></td>
	<td ><?= $rs['judul'] ?></td>
	<td align="left"><?= trim($rs['authorfirst1'].' '.$rs['authorlast1']) ?></td>
	<td align="center"><?= $rs['qtysumbangan'] ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=5><b>Jumlah : <?= $rsc ?></b></td></tr>
</table>


</div>
</body>
</html>