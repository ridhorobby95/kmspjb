<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
		
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_POST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_POST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Pembayaran Denda';
	
	$p_namafile = 'rekap_bayardenda'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}

	$sql = "select b.*, a.namaanggota  
		from pp_bayardenda b 
		join ms_anggota a on a.idanggota = b.idanggota 
		where 1=1 and b.tglbayar between '$r_tgl1' and '$r_tgl2' 
		order by b.tglbayar, a.namaanggota ";	
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount()

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="window.print()">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center" colspan=2><strong>
  	<h2>Rekap Pembayaran Denda</h2>
  	</strong></td>
  </tr>
  <tr>
	<td> Tanggal </td>
	<td>: <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="80" align="center"><strong>No. Anggota</strong></th>
	<th width="200" align="center"><strong>Nama Anggota</strong></th>
	<th width="100" align="center"><strong>Tanggal Bayar</strong></th>
	<th width="100" align="center"><strong>Nominal</strong></th>
  </tr>
  <?php
	$no=1;
	while($rs=$row->FetchRow()) 
	{  $totalnominal = $totalnominal + $rs['nilai'];?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $rs['idanggota'] ?></td>
	<td ><?= $rs['namaanggota'] ?></td>
	<td align="center"><?= Helper::tglEng($rs['tglbayar']) ?></td>
	<td align="right"><?= "Rp. ".number_format($rs['nilai'],'0',',','.'); ?></td>
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=9 >Tidak ada transaksi</td>
	</tr>
	<? } ?>
   <tr height=25><td colspan=6 align="right"><b>Total : <?= "Rp. ".number_format($totalnominal,'0',',','.'); ?></b></td></tr>
</table>


</div>
</body>
</html>