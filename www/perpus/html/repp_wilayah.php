<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_jenis = Helper::removeSpecial($_REQUEST['kdjenispustaka']);
	$r_tgl1 = Helper::removeSpecial(Helper::formatDate($_REQUEST['tgl1']));
	$r_tgl2 = Helper::removeSpecial(Helper::formatDate($_REQUEST['tgl2']));
	
	if($r_format=='' or $r_tgl1=='' or $r_tgl2=='') {
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Pustaka Perwilayah';
	
	$p_namafile = 'rekap_'.$r_jenis.'_'.$r_tgl1.'_'.$r_tgl2;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select count(*) as jumlah, e.kdlokasi,l. namalokasi
		from pp_eksemplar e
		join ms_pustaka p on e.idpustaka = p.idpustaka
		join lv_lokasi l on e.kdlokasi = l.kdlokasi
		where e.tglperolehan between to_date('$r_tgl1','yyyy-mm-dd') and to_date('$r_tgl2','yyyy-mm-dd') $sqlAdminUnit ";
	if ($r_jenis!='') {
		$sql .=" and p.kdjenispustaka='$r_jenis' ";
	}
	$sql .=" group by e.kdlokasi, l.namalokasi order by l.namalokasi";
	
	$rs = $conn->Execute($sql);
	
	$i=0;
	while($row=$rs->FetchRow()){
		$ArCount[$row['kdlokasi']]=$row['jumlah'];	
		$i+=$row['jumlah'];
	}
	
	$rslokasi = $conn->Execute("select * from lv_lokasi order by kdlokasi");
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Rekap Jumlah Pustaka Perwilayah</h2>
  	</strong></td>
  </tr>
    <tr>
	<td>Periode : <?= Helper::tglEng($r_tgl1) ?> s/d <?= Helper::tglEng($r_tgl2) ?></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
    <th width="150" align="center"><strong>Lokasi/Wilayah</strong></th>
    <th width="100" align="center"><strong>Jumlah Eksemplar</strong></th>
	
  </tr>
  <?php
	$no=1;
	while($rowl=$rslokasi->FetchRow()) 
	{  ?>
    <tr height=25>
	<td align="center"><?= $no ?></td>
    <td align="left"><?= $rowl['namalokasi'] ?></td>
	<td align="center"><?= $ArCount[$rowl['kdlokasi']]=='' ? '0' : $ArCount[$rowl['kdlokasi']] ?></td>
	
  </tr>
	<? $no++; } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=3 >Tidak ada pustaka</td>
	</tr>
	<? } else { ?>
   <tr height=25>
		<td align="center" colspan=2 >JUMLAH</td>
		<td align="center"><?= $i  ?></td>
	</tr>
	<? } ?>
</table>


</div>
</body>
</html>