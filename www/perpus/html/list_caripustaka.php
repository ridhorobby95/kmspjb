<?php
	// bagian include inisialisasi
		
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// $conn->debug=true;
	// pengecekan tipe session user
	
	$a_auth = Helper::checkRoleAuth($conng);
	// otorisasi user
	$c_add = $a_auth['cancreate'];
	
	// definisi variabel halaman
	$p_xpage = Helper::navAddress('xlist_caripustaka.php');
	$p_title = 'Pencarian Cepat Pustaka';
	$p_window = '[PJB LIBRARY] Pencarian Pustaka';
	
	//untuk mendapatkan bahasa
	$rscb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by namabahasa");
	$listBahasa = $rscb->GetMenu2('kdbahasa','',true,false,0,'id="kdbahasa"');
?>

<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style/jquery.treeview.css">
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
	<?php include('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<form name="perpusform1" id="perpusform1" method="post" action="<?= $i_phpfile; ?>">
          <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" align="center" width="100%" style="border-collapse:collapse">
			  <tr>
				<td rowspan="2" width="160" valign="top" class="searchLeft" height="300"  style="    background: #eee;padding: 10px;border-bottom: 1px solid #ddd;">
					<img src="images/a.gif">
					<table width="160" border="0" cellpadding="4" cellspacing="0">
						<tr>
						  <td valign="center"><img src="images/arrow-down.png">&nbsp;<a href="#" id="listmenu" style="text-decoration:none; color:#000; font-weight:bold;">Cari Berdasarkan :</a></td>
						</tr>
						<tr id="detailmenu">
							<td>
								<ul class="left-menu">
									<li><a href="javascript:goCari('judul')" id="listmenu">Judul</a></li>
									<li><a href="javascript:goCari('author')" id="listmenu">Pengarang</a></li>
									<li><a href="javascript:goCari('jenis')" id="listmenu">Jenis Pustaka</a></li>
								</ul>
							</td>
						</tr>
					</table>
				</td>
				<td align="center" valign="top">
					<table width="80%" border="0" cellpadding="4" cellspacing="0" style="background:#eee;border: 1px solid #ddd;padding: 10px;">
					  <tr>
						<td align="center" class="PageTitle" colspan="3"><h3><?= $p_title; ?></h3></td>
					  </tr>
						<tr>
						  <td><strong>Kata Kunci</strong></td>
						  <td align="center"><?= UI::createTextBox('keyword',$r_keyword,'SearchStyle',100,50,true,'onKeyPress="etrCari(event)"') ?></td>
						  <td align="left"><a href="javascript:goCari('')" class="buttonshort"><span class="cari">Cari</a></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
							<td style="padding: 1em 0 2em 0;"><a href="" id="litasearchadv">Pencarian Lanjutan</a><a href="" id="litasearchsimple">Pencarian Sederhana</a>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<div id="litaadvance" style="display:none;">
									<table cellpadding="4" cellspacing="0" border="0" width="100%">
										<tr height="20">
											<td align="left">Bahasa</td> 
											<td align="left"><?= $listBahasa ?></td>
										</tr>
										<tr>
											<td align="left">Topik</td>
											<td><?= UI::createSelect('tipecari',array(''=>'','T'=>'Topik','K'=>'Keyword', 'J'=>'Jenis Pustaka', 'P'=>'Pengarang', 'JP'=>'Judul Pustaka'),'','ControlStyle',true); ?></td>
										</tr>
										<tr>
											<td align="left">Tahun Terbit</td>
											<td align="left"><?= UI::createTextBox('tahun1','','ControlStyle',4,4,true,'onKeyPress="etrCari(event)"') ?> - <?= UI::createTextBox('tahun2','','ControlStyle',4,4,true,'onKeyPress="etrCari(event)"') ?> </td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			  </tr>
			  <tr>
				<br/>
				<td valign="top" align="left"><div  id="isi"></div></td>
			  </tr>
			</table>
            </div>
		</form>
			
		<div id="progressbar" style="position:absolute;visibility:hidden;left:0px;top:0px;">
			<table bgcolor="#FFFFFF" border="1" style="border-collapse:collapse;"><tr><td align="center">
			Mohon tunggu...<br><br><img src="images/progressbar.gif">
			</td></tr></table>
		</div>
	</div>
</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/forpagerx.js"></script>
<script type="text/javascript">

// untuk ajaxing list
xlist = "<?= $p_xpage; ?>";
xtdid = "isi";
expanded = true;

$(function() {
		$("#keyword").focus();
		
		$("#litasearchsimple").hide();
		$("#litasearchadv").click(function() {
			$("#litaadvance").show('blind','',500,'');
			$("#litasearchsimple").show();
			$("#litasearchadv").hide();
			return false;

		});
		
		$("#litasearchsimple").click(function() {
			//$("#litaadvance").hide();
			$("#litaadvance").hide('blind','',500,'');

			$("#litasearchsimple").hide();
			$("#litasearchadv").show();
			return false;
		});
		
		$("#listmenu").click(function() {
			$("#detailmenu").toggle('blind','',500);
		});
	});

function etrCari(e) {
	var ev = (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13){
		goCari('');
	}
}

function goCari(alt) {
	if(document.getElementById("keyword").value == ""){
		alert("PERINGATAN: Silahkan Mengisi Kata Kunci Pada Form Pencarian.");
	}else{
		sent = $("#perpusform1").serialize();
		if (alt!=''){
			if (alt=='judul')
				sent += "&tipecari=";
			if (alt == 'author')
				sent += "&tipecari=P";
			if (alt == 'jenis')
				sent += "&tipecari=J";
		}	
				
		$("#" + xtdid).divpost({page: xlist, sent: sent});
	}
}

</script>

</html>