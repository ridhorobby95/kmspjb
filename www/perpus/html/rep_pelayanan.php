<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pelayanan Umum';
	$p_filerep = 'repp_pelayanan';
	$p_tbwidth = 420;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	//list jenis peminjaman
	$rs_cb = $conn->Execute("select namapelayanan, kdpelayanan from lv_jenispelayanan order by kdpelayanan");
	$l_jenis = $rs_cb->GetMenu2('kdpelayanan','',true,false,0,'id="kdpelayanan" class="ControlStyle" style="width:156"');
	
	$rs_cb = $conn->Execute("select namajurusan, kdjurusan from lv_jurusan order by namajurusan");
	$l_jurusan= $rs_cb->GetMenu2('kdjurusan','',true,false,0,'id="kdjurusan" class="ControlStyle" style="width:156"');
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);

	
	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan pelayanan Umum</td></tr>
	<tr>
		<td class="LeftColumnBG">Jenis Pelayanan</td>
		<td class="RightColumnBG"><?= $l_jenis; ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Jurusan</td>
		<td class="RightColumnBG"><?= $l_jurusan; ?></td>
	</tr>
	<tr> 
		<td class="LeftColumnBG" width="120">Tanggal</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("kdpelayanan,tgl1,tgl2"))
		goSubmit();
}

</script>
</html>