<?php
	set_time_limit(0);
	ini_set('upload_max_filesize', '30M');
	ini_set('post_max_size', '30M');

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	require('classes/pdf.class.php');

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
		
	// variabel esensial
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	if($r_key != '') {
	    $p_foto = Config::dirFoto.'pustaka/'.trim($r_key).'.jpg';
	    $p_hfoto = Config::fotoUrl.'pustaka/'.trim($r_key).'.jpg';
	    $p_sinopsis = "/var/www/html/perpus/";
	}
	
	$connLamp = Factory::getConnLamp();
	$cek = $connLamp->IsConnected();

	$p_eksemplar = Helper::navAddress('ms_eksemplar.php');
	// definisi variabel halaman
	$p_dbtable = 'ms_pustaka';
	$p_window = '[PJB LIBRARY] Data Pustaka';
	$p_title = 'Data Pustaka';
	$p_tbwidth = 900;
	$p_filelist = Helper::navAddress('list_pustaka.php');
	if($r_key==''){
		$idpus=$conn->GetOne("select max(idpustaka) as idmax from $p_dbtable");
		$idok=$idpus + 1;
	}
	// bila ada post
	if (!empty($_POST)) {
		$r_aksi = Helper::removeSpecial($_POST['act']);

		if($r_aksi == 'simpan' and $c_edit) {
			$record = array();
			$conn->StartTrans();
			$errfile = "";
			$maxfile = Helper::removeSpecial($_POST['theValue']);
			$record = Helper::cStrFill($_POST);// proses input dari post;
			$record['tglperolehan'] = Helper::formatDate($_POST['tglperolehan']);
			$judul=Helper::removeSpecial($_POST['judul']);
			$record['judul'] = Helper::removeSpecial($_POST['judul']);
			$record['judulseri'] = Helper::removeSpecial($_POST['judulseri']);
			$record['ilustrasi']=Helper::removeSpecial($_POST['ilustrasi']);
			$record['index_buku']=Helper::removeSpecial($_POST['index_buku']);
			Helper::Identitas($record);
			
			$serie=Helper::removeSpecial($_POST['seri']);
			if($r_key==''){
				if($serie){
					$seri=$serie;
				}else{
					$seri=$noseri=Sipus::GetSeri($conn,$_POST['kdjenispustaka']);# FROM SISTEM
				}
			}else{
				$seri = $conn->GetOne("select noseri from ms_pustaka where idpustaka = $r_key ");
			}
			
			$sinfile=Sipus::UploadAbstrak($judul,$seri);
			if ($sinfile!='')
				$record['sinopsis_upload']=$sinfile;
						
			if($r_key == '') { // insert record	
				$record['idpustaka'] = $idok;
				
				$record['noseri']=$idok;//$seri;
				
				Sipus::InsertBiasa($conn,$record,$p_dbtable);
				
				// Upload Sinopsis ke BLOB datatype
				$id_sinopsis = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 2 and idlampiran = $idok ");
				$recordf = array();
				$recordf['idlampiran'] = $record['idpustaka'];
				$recordf['jenis'] = '2'; # 1= foto, 2=sinopsis, 3=file pustaka
				$recordf['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
				$recordf['filename'] = $_FILES['uploadsinopsis']['name'];
				
				$content = $_FILES['uploadsinopsis']['tmp_name'];
				if($content){
					$tmp_file = $_FILES['uploadsinopsis']['tmp_name'];
					$filetype = $_FILES['uploadsinopsis']['type'];
					$filesize = $_FILES['uploadsinopsis']['size'];
					$filename = $_FILES['uploadsinopsis']['name'];
					$s_typefile = false;
					$s_sizefile = false;
					if ($filesize < 5000000){
						$s_sizefile = true;
						if ($filetype=='application/pdf') {
							$s_typefile = true;
						}else
							$errfile .= 'Format file sinopsis tidak dikenali.[Bukan .pdf]';	
					}else {
						$errfile .= 'Besar File Melebihi 5 MB.';	
					}
				
					#sinopsis
					if($s_typefile and $s_sizefile){
						if($id_sinopsis){
							$err = Sipus::UpdateBlob($connLamp,$recordf,'lampiranperpus',$content,'idlampiranperpus','isifile',$id_sinopsis);
						}else{
							$err = Sipus::InsertBlob($connLamp,$recordf,'lampiranperpus',$content,'idlampiranperpus','isifile');
						}
					}else
						$errfile .= '<br/>';
				}
				
				#FILE UPLOAD
				for ($i = 0; $i <= $maxfile; $i++)
				{
					$r_file = array();
					$p_tmp_file = $_FILES['userfile']['tmp_name'][$i];
					$p_filetype = $_FILES['userfile']['type'][$i];
					$p_filesize = $_FILES['userfile']['size'][$i];
					$p_filename = $_FILES['userfile']['name'][$i];
					$fke = $i + 1;
					if($p_tmp_file){
						$ext=explode(".",$p_filename);
						$extjum=count($ext);
						$eksten=$ext[$extjum-1];
						$fileup=Helper::removeSpecial($p_filename);
						$directori = 'uploads/file/' . $seri;
											
						$destination = $directori .'/'.$fileup;
						$p_sizefile = false;
						$p_typefile = false;
						if ($p_filesize < 10000000){
							$p_sizefile = true;
							if ($p_filetype=='application/pdf') {
								$p_typefile = true;
							}else
								$errfile .= 'Format file pustaka ke-'.$fke.' tidak dikenali.[Bukan .pdf]';	
						}else {
							$errfile .= 'Besar File pustaka ke-'.$fke.' Melebihi 10 MB.';	
						}
						
						if($p_sizefile and $p_typefile){
							if(!is_dir($directori))
								mkdir($directori,0770);
	
							if (copy($p_tmp_file,$destination))
							{					
								$r_file['files']=Helper::cStrNull($destination);
							}					
							$r_file['idpustaka'] = $idok;
							$r_file['login'] = (Helper::removeSpecial($_POST['isLogin'][$i]) == "on" ? "1" : "0");
							$r_file['download'] = (Helper::removeSpecial($_POST['isDownload'][$i]) == "on" ? "1" : "0");
							Helper::Identitas($r_file); 
							if(!empty($r_file['files']))
								Sipus::InsertBiasa($conn,$r_file,pustaka_file);
								
							$idpustakafile = $conn->GetOne("select idpustakafile from pustaka_file where idpustaka = $idok and files = '".$r_file['files']."' ");
								
							// Upload File Pustaka ke BLOB datatype
							$id_filepus = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 3 and idlampiran = $idpustakafile ");
							$recordfp = array();
							$recordfp['idlampiran'] = $idpustakafile;
							$recordfp['jenis'] = '3'; # 1= cover, 2=sinopsis, 3=file pustaka, 4 = foto umum
							$recordfp['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
							$recordfp['filename'] = $fileup; 
							
							$p_content = $_FILES['userfile']['tmp_name'][$i];
							if($p_content){
								if($id_filepus){
									$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$p_content,'idlampiranperpus','isifile');
								}else{
									$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$p_content,'idlampiranperpus','isifile');
								}
							}
						}else
							$errfile .= '<br/>';
					}
				}
				
				$pengarang1=Helper::removeSpecial($_POST['addauthor']);
				if($pengarang1!='')
				Sipus::InsertRef($conn,pp_author,idauthor,idpustaka,$pengarang1,$record['idpustaka']);
				
				$topik1=Helper::removeSpecial($_POST['addtopik']);
				if($topik1!='')
				Sipus::InsertRef($conn,pp_topikpustaka,idtopik,idpustaka,$topik1,$record['idpustaka']);
				
				if($conn->ErrorNo() != 0){
					$errdb = 'Update data gagal.';
					$errdb .= "<br/>".$errfile;
					Helper::setFlashData('errdb', $errdb);
					$row=$_POST;
				}
				else {
					if($errfile){
						$errdb = "<br/>".$errfile;
						Helper::setFlashData('errdb', $errdb);
					}else{
						$sucdb = 'Update data Pustaka berhasil.';
						Helper::setFlashData('sucdb', $sucdb);
					}
					
				}
			}
			else { // update record
				$serie=Helper::removeSpecial($_POST['seri']);
				Sipus::UpdateBiasa($conn,$record,$p_dbtable,idpustaka,$r_key);
				
				// Upload Sinopsis ke BLOB datatype
				$id_sinopsis = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 2 and idlampiran = $r_key ");
				$recordf = array();
				$recordf['idlampiran'] = $r_key;
				$recordf['jenis'] = '2'; # 1= foto, 2=sinopsis, 3=file pustaka
				$recordf['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
				$recordf['filename'] = $_FILES['uploadsinopsis']['name'];
				
				$content = $_FILES['uploadsinopsis']['tmp_name'];
				if($content){
					$tmp_file = $_FILES['uploadsinopsis']['tmp_name'];
					$filetype = $_FILES['uploadsinopsis']['type'];
					$filesize = $_FILES['uploadsinopsis']['size'];
					$filename = $_FILES['uploadsinopsis']['name'];
					$s_typefile = false;
					$s_sizefile = false;

					if ($filesize < 5000000){
						$s_sizefile = true;
						if ($filetype=='application/pdf') {
							$s_typefile = true;
						}else
							$errfile .= 'Format file sinopsis tidak dikenali.[Bukan .pdf]';	
					}else {
						$errfile .= 'Besar File Melebihi 5 MB.';	
					}
					
					if($s_typefile and $s_sizefile){
						if($id_sinopsis){
							$err = Sipus::UpdateBlob($connLamp,$recordf,'lampiranperpus',$content,'idlampiranperpus','isifile',$id_sinopsis);
						}else{
							$err = Sipus::InsertBlob($connLamp,$recordf,'lampiranperpus',$content,'idlampiranperpus','isifile');
						}
					}else
						$errfile .= '<br/>';
				}

				#FILE UPLOAD
				for ($i = 0; $i <= $maxfile; $i++)
				{
					$r_file = array();
					$p_tmp_file = $_FILES['userfile']['tmp_name'][$i];
					$p_filetype = $_FILES['userfile']['type'][$i];
					$p_filesize = $_FILES['userfile']['size'][$i];
					$p_filename = $_FILES['userfile']['name'][$i];
					$fke = $i + 1;
					if($p_tmp_file){
						$ext=explode(".",$p_filename);
						$extjum=count($ext);
						$eksten=$ext[$extjum-1];
						$fileup=Helper::removeSpecial($p_filename);
						$directori = 'uploads/file/' . $seri;
											
						$destination = $directori .'/'.$fileup;
						$p_sizefile = false;
						$p_typefile = false;
						if ($p_filesize < 10000000){
							$p_sizefile = true;
							if ($p_filetype=='application/pdf') {
								$p_typefile = true;
							}else
								$errfile .= 'Format file pustaka ke-'.$fke.' tidak dikenali.[Bukan .pdf]';	
						}else {
							$errfile .= 'Besar File pustaka ke-'.$fke.' Melebihi 10 MB.';	
						}
						
						if($p_sizefile and $p_typefile){
							if(!is_dir($directori))
								mkdir($directori,0750);
	
							if (copy($p_tmp_file,$destination))
							{
								$r_file['files']=Helper::cStrNull($destination);
							}					
							$r_file['idpustaka'] = $r_key;
							$r_file['login'] = (Helper::removeSpecial($_POST['isLogin'][$i]) == "on" ? "1" : "0");
							$r_file['download'] = (Helper::removeSpecial($_POST['isDownload'][$i]) == "on" ? "1" : "0");
							Helper::Identitas($r_file); 
							if(!empty($r_file['files']))
								Sipus::InsertBiasa($conn,$r_file,pustaka_file);
								
							$idpustakafile = $conn->GetOne("select idpustakafile from pustaka_file where idpustaka = $r_key and files = '".$r_file['files']."' ");
								
							// Upload File Pustaka ke BLOB datatype
							$id_filepus = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 3 and idlampiran = $idpustakafile ");
							$recordfp = array();
							$recordfp['idlampiran'] = $idpustakafile;
							$recordfp['jenis'] = '3'; # 1= cover, 2=sinopsis, 3=file pustaka, 4 = foto umum
							$recordfp['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
							$recordfp['filename'] = $fileup; 
							
							$p_content = $_FILES['userfile']['tmp_name'][$i];
							if($p_content){
								if($id_filepus){
									$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$p_content,'idlampiranperpus','isifile');
								}else{
									$err = Sipus::InsertBlob($connLamp,$recordfp,'lampiranperpus',$p_content,'idlampiranperpus','isifile');
								}
							}
						}else
							$errfile .= '<br/>';
					}
				}

				$author=$conn->GetRow("select idauthor from pp_author where idpustaka=$r_key");
				if ($author['idauthor']!='')
				Sipus::DeleteBiasa($conn,pp_author,idpustaka,$r_key);
				
				if (!empty ($_POST['addauthor'])){
					
					foreach ($_POST['addauthor'] as $k => $v){
						
						$record = array();
						$record['idpustaka'] = $r_key;
						$record['idauthor'] = $v;
						Sipus::InsertBiasa($conn,$record,'pp_author');
					}
				}
				
				$topik=$conn->GetRow("select idtopik from pp_topikpustaka where idpustaka=$r_key");
				if ($topik['idtopik']!='')	
				Sipus::DeleteBiasa($conn,pp_topikpustaka,idpustaka,$r_key);

				if (!empty ($_POST['addtopik'])){
					
					foreach ($_POST['addtopik'] as $k => $v){
						
						$record = array();
						$record['idpustaka'] = $r_key;
						$record['idtopik'] = $v;
						Sipus::InsertBiasa($conn,$record,'pp_topikpustaka');
					}
				}
				
				if($conn->ErrorNo() != 0){
					$errdb = 'Update data gagal.';
					$errdb .= "<br/>".$errfile;
					Helper::setFlashData('errdb', $errdb);
					
				}
				else {
					if($errfile){
						$errdb = "<br/>".$errfile;
						Helper::setFlashData('errdb', $errdb);
					}else{
						$sucdb = 'Update data Pustaka berhasil.';
						Helper::setFlashData('sucdb', $sucdb);
					}
					
				}
			}
				$conn->CompleteTrans();
				
			if ($conn->ErrorNo() == 0){
				if(empty($r_key))
					$r_key = $record['idpustaka'];
				else
					$r_key = $r_key;
					
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				Helper::redirect($url);
			} 
		}
		else if($r_aksi == 'hapus' and $c_delete) {
			$p_file = $conn->GetArray("select file from pustaka_file where idpustaka = $r_key ");
			$nseri = $conn->GetOne("select noseri from ms_pustaka where idpustaka = $r_key ");  
			foreach($p_file as $pf){
				unlink($pf['files']);
			}
			rmdir('uploads/file/'.$nseri);
			$sql="delete from lp.lampiranperpus where idlampiran in (select idpustakafile from pustaka_file where idpustaka = $r_key)";
			$conn->Execute($sql);
			$err=Sipus::DeleteBiasa($conn,pustaka_file,idpustaka,$r_key);
			$err=Sipus::DeleteBiasa($conn,pp_eksemplar,idpustaka,$r_key);
			$err=Sipus::DeleteBiasa($conn,$p_dbtable,idpustaka,$r_key);
			
			if($err==0) {
				header('Location: '.$p_filelist);
				exit();
			} else {
				$errdb = 'Penghapusan data gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			
		}else if($r_aksi == "delphoto" and $c_delete){
			$id_photo = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 1 and idlampiran = $r_key ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_photo);
				if($connLamp->ErrorNo() != 0){
					$errdb = 'Penghapusan foto gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penghapusan foto berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
		
		}else if($r_aksi == "delsin" and $c_delete){
			$file=Helper::removeSpecial($_POST['delfile']);
			$file=Helper::removeSpecial($_POST['delfile']);
			if($file){
				unlink($p_sinopsis.$file);
			}
			$recsin['sinopsis_upload']='';
			Sipus::UpdateBiasa($conn,$recsin,ms_pustaka,idpustaka,$r_key);
			$id_sinopsis = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 2 and idlampiran = $r_key ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_sinopsis);
				if($connLamp->ErrorNo() != 0){
					$errdb = 'Penghapusan file sinopsis gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Penghapusan file sinopsis berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
		
		}else if($r_aksi == "delfilepus" and $c_delete){
			$file=Helper::removeSpecial($_POST['delfile']);
			unlink($file);
			// Menghapus file pustaka type BLOB
			$filename=Helper::removeSpecial($_POST['delfilepus']);
			$id_filepus = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 3 and idlampiran = $r_key and filename like '%$filename%' ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_filepus);
				if($connLamp->ErrorNo() != 0){
					$errdb = 'Penghapusan file pustaka gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}else {
					$sucdb = 'Penghapusan file pustaka berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
				}
		
		}else if($r_aksi =="delfile" and $c_delete){
			$file=Helper::removeSpecial($_POST['delfile']);
			$fkey=Helper::removeSpecial($_POST['delkey']);
			$seri=Helper::removeSpecial($_POST['delseri']);
			unlink($file);
			$fdel = explode('.',Helper::GetPath($file));
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiran,$fkey);
			Sipus::DeleteBiasa($conn,pustaka_file,idpustakafile,$fkey);
			if($conn->ErrorNo() != 0){
				$errdb = 'Penghapusan file upload gagal.';	
				Helper::setFlashData('errdb', $errdb);
			}
			else {
				$sucdb = 'Penghapusan file upload berhasil.';	
				Helper::setFlashData('sucdb', $sucdb);
				$parts = Explode('/', $_SERVER['PHP_SELF']);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				
			}
		}else if($r_aksi =="ubahaksesfile" and $c_edit and $r_key != ''){
			$fkey=Helper::removeSpecial($_POST['delkey']);
			$uLogin = Helper::removeSpecial($_POST['uLogin'.$fkey]);
			$uDownload = Helper::removeSpecial($_POST['uDownload'.$fkey]);
			$recfile = array();
			$recfile['login'] = ($uLogin == "on" ? "1" : "0");
			$recfile['download'] = ($uDownload == "on" ? "1" : "0");
			Sipus::UpdateBiasa($conn,$recfile,pustaka_file,idpustakafile,$fkey);
				if($conn->ErrorNo() != 0){
					$errdb = 'Update file upload gagal.';	
					Helper::setFlashData('errdb', $errdb);
				}
				else {
					$sucdb = 'Update file upload berhasil.';	
					Helper::setFlashData('sucdb', $sucdb);
					$parts = Explode('/', $_SERVER['PHP_SELF']);
					$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'] . '&key='.$r_key;
				}
		}else if($r_aksi == "simpanfoto" and $c_edit and $r_key != '') {
			if($_FILES['foto']['error'] == UPLOAD_ERR_OK)
				$sfok = UI::createFoto($_FILES['foto']['tmp_name'],$p_foto,500,500);
			else if($_FILES['foto']['error'] == UPLOAD_ERR_INI_SIZE or $_FILES['foto']['error'] == UPLOAD_ERR_FORM_SIZE)
				$sfok = -4; // ukuran file melebihi batas
			else if($_FILES['foto']['error'] == UPLOAD_ERR_NO_FILE)
				$sfok = -5; // tidak ada file yang diupload
				
			$path = $_FILES['foto']['tmp_name'];
			if($sfok>0){
				$id_poto = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 1 and idlampiran = $r_key ");
				$record = array();
				$record['idlampiran'] = $r_key;
				$record['jenis'] = '1'; # 1= cover, 2=sinopsis, 3=file pustaka, 4= foto
				$record['tiduser'] = Helper::cStrNull($_SESSION['PERPUS_USER']); #test
				$record['filename'] = $_FILES['foto']['name']; 
				if($id_poto){
					$err = Sipus::UpdateBlob($connLamp,$record,'lampiranperpus',$path,'idlampiranperpus','isifile',$id_poto);
				}else{
					# UPLOAD COVER PUSTAKA
					$err = Sipus::InsertBlob($connLamp,$record,'lampiranperpus',$path,'idlampiranperpus','isifile');
				}
			}
			
			@unlink($_FILES['foto']['tmp_name']); // hapus dan jangan tampilkan pesan error bila error
			
			switch($sfok) {
				case -1: $uploadmsg = 'Format file foto tidak dikenali.'; break;
				case -2: $uploadmsg = 'Format file foto harus GIF, JPEG, atau PNG.'; break;
				case -3: $uploadmsg = 'File foto tidak bisa diupload.'; break;
				case -4: $uploadmsg = 'Ukuran file foto melebihi batas maksimal.'; break;
				case -5: $uploadmsg = 'Tidak ada file foto yang di-upload.'; break;
				default: $uploadmsg = 'Upload foto berhasil.'; break;
			}
			
			if($sfok < 0)
				$uploadmsg = UI::message($uploadmsg,true);
			else
				$uploadmsg = UI::message($uploadmsg);
		}
		else if($r_aksi == "hapusfoto" and $c_edit and $r_key != '') {
			$dfok = unlink($p_foto);
			$id_cover = $connLamp->GetOne("select idlampiranperpus from lampiranperpus where jenis = 1 and idlampiran = '$r_key' ");
			Sipus::DeleteBiasa($connLamp,lampiranperpus,idlampiranperpus,$id_cover);
			if($dfok)
				$uploadmsg = UI::message("Penghapusan foto berhasil.");
			else
				$uploadmsg = UI::message("Penghapusan foto tidak berhasil.",true);
		}
		
		// untuk upload dan hapus foto
		if(!empty($uploadmsg)) {
			// html ditulis di iframe "upload_iframe"
			echo '<html><body><script type="text/javascript">'."\n";
			echo 'var parentdoc = window.parent.document;'."\n";
			echo "parentdoc.getElementById('uploadmsg').innerHTML = '$uploadmsg';\n";
			echo 'parent.$("#imgfoto").waitload({mode: "unload"});'."\n";
			
			if($sfok > 0 or $dfok)
				echo 'parentdoc.getElementById("imgfoto").src = "'.(is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'default.jpg').'?'.mt_rand(1000,9999).'";'."\n";
			
			echo '</script></body></html>';
			
			exit();
		}
		
	}
	if(!$p_errdb) {
		if ($r_key !='') {
		$p_sqlstr = "select p.*, j.namajenispustaka
			from $p_dbtable p 
			join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka
			where p.idpustaka = '$r_key'"; 
		$row = $conn->GetRow($p_sqlstr);
		
		// data select
		$sql = "select b.idauthor, b.namadepan, b.namabelakang, b.isbadan from ms_author b join
				pp_author a on b.idauthor = a.idauthor where a.idpustaka = '$r_key'";
		$rsd = $conn->Execute($sql);
		
		$jmlpeng = $conn->GetOne("select count(a.idauthor) from pp_author a where a.idpustaka = '$r_key' ");
		
		$sql2 = "select b.idtopik, b.namatopik from lv_topik b join
				pp_topikpustaka a on b.idtopik = a.idtopik where a.idpustaka = '$r_key'";
		$rsx = $conn->Execute($sql2);
		
		$sqlFile = "select idpustakafile, files, login, download from pustaka_file where idpustaka = '$r_key' order by idpustakafile ";
		$rsFile = $conn->GetArray($sqlFile);
		
		$sql3 = "select isifile from lampiranperpus where jenis = 1 and idlampiran = $r_key ";
		$rsLOB = $connLamp->GetOne($sql3);
		
		$sql4 = "select isifile, filename from lampiranperpus where jenis = 2 and idlampiran = $r_key ";
		$rsLOBPDF = $connLamp->getRow($sql4);	
		}
	}
		
	// daftar combo box
	if($c_edit) {
		$rs_cb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by kdbahasa");
		$l_bahasa = $rs_cb->GetMenu2('kdbahasa',$row['kdbahasa'],true,false,0,'id="kdbahasa" class="ControlStyle" style="width:255"');

		$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where islokal<>1 and kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
		$l_pustaka = $rs_cb->GetMenu2('kdjenispustaka',$row['kdjenispustaka'],true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:140"'); 
	
		$rs_cb = $conn->Execute("select namaperiode, idperiode from lv_periode order by idperiode");
		$l_periode = $rs_cb->GetMenu2('idperiode',$row['idperiode'],true,false,0,'id="idperiode" class="ControlStyle" style="width:140"');
	}
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	
	<link href="style/button.css" type="text/css" rel="stylesheet">
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/foreditx.js"></script>
	<script type="text/javascript" src="scripts/forinplace.js"></script>
	<link href="style/tabs.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<script type="text/javascript" src="scripts/combo.js"></script>

	<script type="text/javascript" >
	function CreateTextbox()
	{

		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		//if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type='checkbox' name='isLogin[]'>&nbsp;Harus Login&nbsp;<input type='checkbox' name='isDownload[]'>&nbsp;Bisa didownload&nbsp;<input type='file' onchange='if(!checkfile(this.files[0].type,this.files[0].size))$(this).val(null);' name='userfile[]' size='40' /> </p>";
		//}

	}
	

	function callnumber(){
		var jmpl=document.getElementById("jmlpeng").value;
		var ddc = $("#kodeddc").val();
		var judul=document.getElementById("judul").value;
		var np=document.getElementById("nopanggil").value;
		var isbad = $("[name='isbad[]']").eq(0).val();
		
		var author = $("[name='namaauth[]']").eq(0).val();
		if($("[name='nambel[]']").eq(0).val())
			var auth = $("[name='nambel[]']").eq(0).val();
		else
			var auth = $("[name='namdep[]']").eq(0).val();
		
		var print=(auth.substring(0,3)); 
		var title=judul.substr(0,1);
		if(np == ""){
			/*if(isbad == "1" || (parseInt(jmpl)>3))
				document.getElementById("nopanggil").value=ddc+" "+judul.substr(0,1).toUpperCase() + judul.substr(1,2).toLowerCase();
			else*/
				document.getElementById("nopanggil").value=ddc+" "+print.substr(0, 1).toUpperCase() + print.substr(1).toLowerCase()+" "+title.toLowerCase();
		}
	}


	</script>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
	<div class="SideItem" id="SideItem">
		<div align="center">
		<table width="<?= $p_tbwidth ?>">
			<tr height="20">
				<td align="center" class="PageTitle"><h1 style="font-weight:normal;color:#015593;"><?= $p_title ?></h1></td>
			</tr>
		</table>
		<?php include_once('_notifikasi.php'); ?>
		<table width="100">
			<tr>
			<? if($c_readlist) { ?>
			<td align="center">
				<a href="<?= $p_filelist; ?>" class="button"><span class="list">Daftar Pustaka </span></a>
			</td>
			<? } if($c_edit) { ?>
			<td align="center">
				<a href="javascript:saveData();" class="button"><span class="save">Simpan</span></a>
			</td>
			<td align="center">
				<a href="javascript:goUndo();" class="button"><span class="reset">Reset</span></a>
			</td>
			<? } if($c_delete and $r_key) { ?>
			<td align="center">
				<a href="javascript:goDelete();" class="button"><span class="delete">Hapus</span></a>
			</td>
			<?/*td>
				<img src="images/tombol/my-profile.png" alt="snapshot" title="snapshot" style="cursor:pointer" onClick="popup('<?= Helper::navAddress('capture_cam')?>&key=<?= $r_key; ?>&code=p',350,330);">
			</td*/?>
			<? } ?>
			</tr>
		</table><br>

		<table width="<?= $p_tbwidth ?>"><tr><td align="center"><font color="#0000CC"><?= $msgString ?></font></td></tr></table>
		<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>" enctype="multipart/form-data">
		<table width="930" border="0" cellspacing=0 cellpadding="0">
		<tr><td>
		<div class="tabs" style="width:;">
			<ul>
				<li><a id="tablink" href="javascript:void(0)">Identitas Pustaka</a></li>
				<li><a id="tablink" href="javascript:void(0)">Referensi</a></li>
				<li><a id="tablink" href="javascript:void(0)">Sinopsis</a></li>
				<li><a id="tablink" href="javascript:void(0)">Upload Pustaka</a></li>
						
			</ul><br>
			<div style="width:96.8%;height:1px;background:#20915e;position:relative;top:-2px;margin-bottom:10px;"></div>
		<div id="items" style="position:relative;top:-2px">
		<header style="width:96.8%;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Identitas Pustaka</h1>
				</div>
				<? if ($r_key!='') { ?>
				<div class="right">
				      <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data eksemplar" onClick="goEks('<?= $p_eksemplar."&idp=".$row['idpustaka']; ?>');">+</div>
				</div>
				<? } ?>
			</div>
		</header>
		<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
			<tr   height=20> 
				<td class="LeftColumnBG thLeft" width=150>KODE PUSTAKA *</td>
				<td class="RightColumnBG" >
				<? if($r_key==''){?><?= UI::createTextBox('seri',$r_key=='' ? '' : $row['noseri'],'ControlStyle',20,20,$c_edit); ?>
				<em>Kosongkan jika menggunakan Generate by system</em><?}else{ echo $row['noseri'];}?>
				</td>
				<td valign="top" align="center" rowspan="6"> 
					<? if ($r_key!='') { ?>
					<div id="uploadmsg"></div>
					<?php
						$file = Sipus::loadBlobFile($rsLOB, 'jpg');
					?>
					<img border="1" id="imgfoto" src="<?= (is_null($file)) ? Config::fotoUrl.'default.jpg' : $file ?>" width="120" height="150"><br>
					<? if($c_edit) { ?>
					<input type="hidden" name="MAX_FILE_SIZE" value="10000000">
					<input type="file" name="foto" class="ControlStyle" size="15">
					<br><input type="button" value="Simpan" class="ControlStyle" onClick="aSavePhoto();"> 
					<input type="button" value="Hapus" class="ControlStyle" onClick="goDelPhoto();">
					<? } ?>
					<? } ?>
					</td>
			</tr>
			<tr    height=20> 
				<td class="LeftColumnBG thLeft">Jenis Pustaka *</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_pustaka : $row['namajenispustaka']; ?></td>
				
			</tr>
			<tr height=20 id="jb" style="display: none;"> 
				<td class="LeftColumnBG thLeft">Jenis Buku</td>
				<td class="RightColumnBG"><?= $c_edit ? $l_buku : $row['jenisbuku']; ?>
					<input type="hidden" name="jenisbuku" id="jenisbuku" value="<?=$row['jenisbuku'];?>" />
				</td>
				
			</tr>
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Periode</td>
				<td class="RightColumnBG"><?= $l_periode ?> [ untuk terbitan berseri ] </td>	
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft"  width="150">Judul *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('judul',$row['judul'],'ControlStyle',1000,55,$c_edit); ?></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Judul Seri </td>
				<td class="RightColumnBG"><?= UI::createTextBox('judulseri',$row['judulseri'],'ControlStyle',200,55,$c_edit); ?></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Bahasa *</td>
				<td class="RightColumnBG"><?= $l_bahasa ?></td>
			</tr>
			<tr   height=20>
			<td class="LeftColumnBG thLeft">Kode DDC</td>
				<td class="RightColumnBG"><?= UI::createTextBox('kodeddc',$row['kodeddc'],'ControlStyle',50,15,$c_edit); 
				 
				 ?>&nbsp; 
				 <? if($c_edit) { ?>
					 <a href="guide/KODEDDC.pdf" target="_BLANK"><img src="images/popup.png"> Petunjuk Kode DDC</a>
				 <? } ?>
				 </td>
			</tr>
			
			<tr   height=20> 
				<td class="LeftColumnBG thLeft" valign="top" style="padding-top:10px">Topik Pustaka</td>
				<td class="RightColumnBG" colspan="2">
				
				<table width="100%" cellspacing="0" cellpadding="4">
			<? if($c_edit) { ?>
			<tr id="tr_tambahtp"> 
				<td bgcolor="#EEEEEE" colspan="2">
					<input type="text" readonly id="namatopik" class="ControlStyle" size="38" maxlength="50">
					<input type="hidden" id="idtopik" class="ControlStyle" size="30" maxlength="50">
					<input type="button" class="ControlStyle" value="Tambah" onClick="addTopik()">&nbsp;&nbsp;
					<img src="images/popup.png" style="cursor:pointer;" title="Topik baru" onClick="popup('index.php?page=pop_topik',500,480);">
					<span id="span_posisi"><u title="Topik Baru" onclick="popup('index.php?page=pop_topik',500,480);" style="cursor:pointer;" class="Link">Lihat Topik...</u></span>
					<? if ($c_delete) { ?>
					<img src="images/add.png" style="cursor:pointer;" title="Insert Topik" onClick="popup('index.php?page=instopik',500,480);">
					<span id="span_posisi"><u title="Insert Topik" onclick="popup('index.php?page=instopik',500,480);" style="cursor:pointer;" class="Link">Topik Baru...</u></span>
					<? } ?>
					<br> <span id="span_error3" style="display:none"><font color="#FF0000">Topik tersebut telah ditambahkan</font></span>
					 <span id="span_error4" style="display:none"><font color="#FF0000">Topik tidak sesuai</font></span>
				</td>
			</tr>
		  <?	} ?>
			<tr id="tr_tpkosong"<? if(!$rsx->EOF) { ?> style="display:none" <? } ?>> 
				<td bgcolor="#EEEEEE" align="center" colspan="2"><strong>Belum memiliki topik</strong></td>
			</tr><? if($r_key !=''){
					  while($rowx = $rsx->FetchRow()) { ?>
			<tr> 
				<td>
					<input type="hidden" name="addtopik[]" id="addtopik" value="<?= $rowx['idtopik'] ?>">
					<?= $rowx['namatopik'] ?>
				</td>
				<td width="20" align="center">
				<? if($c_edit) { ?><img src="images/delete.png" onClick="delTopik(this)" style="cursor:pointer"> <? } ?>
				</td>
			</tr>
			<?	}}  ?>
		</table>

		<table id="table_templatetp" style="display:none">
			<tr> 
				<td bgcolor="#EEEEEE"><input type="hidden" name="addtopik[]" id="addtopik" disabled></td>
				<td bgcolor="#EEEEEE" width="20" align="center"><img src="images/delete.png" onClick="delTopik(this)" style="cursor:pointer"></td>
			</tr>
		</table>
				</td>
			</tr>
			<tr>
			<td class="LeftColumnBG thLeft" valign="top" style="padding-top:10px">Pengarang</td>
				<td class="RightColumnBG" colspan="2">	

		<table width="100%" cellspacing="0" cellpadding="4">
			<? if($c_edit) { ?>
			<tr id="tr_tambahbu"> 
				<td bgcolor="#EEEEEE" colspan="2">
					
					<input type="text" readonly id="namaauthor" class="ControlStyle" size="38" maxlength="50">
					<input type="hidden" id="namaauthordep" class="ControlStyle" size="38" maxlength="50">
					<input type="hidden" id="namaauthorbel" class="ControlStyle" size="38" maxlength="50">
					<input type="hidden" id="idauthor" class="ControlStyle" size="30" maxlength="50">
					<input type="hidden" name="namapeng" id="namapeng">
					<input type="hidden" name="isbadan" id="isbadan">
					<input type="hidden" name="jmlpeng" id="jmlpeng" value="<?=number_format($jmlpeng,0,'.',',');?>">	
					<input type="button" class="ControlStyle" value="Tambah" onClick="addAuthor()">&nbsp;&nbsp;
					<img src="images/popup.png" style="cursor:pointer;" title="Lihat Author" onClick="popup('index.php?page=pop_author',650,500);">
					<u title="Lihat Author" onclick="popup('index.php?page=pop_author',650,500);" style="cursor:pointer;" class="Link">Lihat Author...</u>
					<? if ($c_delete) { ?>
					<img src="images/add.png" style="cursor:pointer;" title="Insert Author" onClick="popup('index.php?page=insauthor',430,150);">
					<span id="span_posisi"><u title="Insert Author" onclick="popup('index.php?page=insauthor',430,150);" style="cursor:pointer;" class="Link">Author Baru...</u></span>
					<? } ?>
					<br> <span id="span_error" style="display:none"><font color="#FF0000">author tersebut telah ditambahkan</font></span>
					 <span id="span_error2" style="display:none"><font color="#FF0000">Data Author tidak sesuai</font></span>

				</td>
			</tr>
			 
		  <?	} ?>
			<tr id="tr_bukosong"<? if(!$rsd->EOF) { ?> style="display:none" <? } ?>> 
				<td bgcolor="#EEEEEE" align="center" colspan="2"><strong>Belum memiliki author</strong></td>
			</tr><? if($r_key !=''){
					  while($rowd = $rsd->FetchRow()) { ?>
			<tr> 
				<td >
					<input type="hidden" name="addauthor[]" id="addauthor" value="<?= $rowd['idauthor'] ?>">
					<?= $rowd['namadepan']." ". $rowd['namabelakang'] ?>
					<input type="hidden" name="namaauth[]" id="namaauth" value="<?= $rowd['namadepan']." ".$rowd['namabelakang'] ?>">
					<input type="hidden" name="namdep[]" id="namdep" disabled value="<?= $rowd['namadepan'] ?>">
					<input type="hidden" name="nambel[]" id="nambel" disabled value="<?= $rowd['namabelakang'] ?>">
					<input type="hidden" name="isbad[]" id="isbad" disabled value="<?= $rowd['isbadan'] ?>">
				</td>
				<td width="20" align="center">
				<? if($c_edit) { ?><img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer"><? } ?></td>
			</tr>
			<?	} } ?>
			<tr id="tr_authorhidden" style="display:none"> 
				<td colspan="2">
				</td>
			</tr>	
		</table>

		<table id="table_templatebu" style="display:none">
			<tr> 
				<td bgcolor="#EEEEEE"><input type="hidden" name="addauthor[]" id="addauthor" disabled>
					<input type="hidden" name="namaauth[]" id="namaauth" disabled>
					<input type="hidden" name="namdep[]" id="namdep" disabled>
					<input type="hidden" name="nambel[]" id="nambel" disabled>
					<input type="hidden" name="isbad[]" id="isbad" disabled>
				</td>
				<td bgcolor="#EEEEEE" width="20" align="center"><img src="images/delete.png" onClick="delAuthor(this)" style="cursor:pointer"></td>
			</tr>
		</table>
				</td>
			</tr>
			<tr> 
				<td width="140" class="LeftColumnBG thLeft">No panggil</td>
				<td class="RightColumnBG" colspan="2">
				<?= UI::createTextBox('nopanggil',$row['nopanggil'],'ControlStyle',50,15,$c_edit,' onFocus="callnumber()"'); ?>
				</td>
			</tr>
			<tr>
			<td class="LeftColumnBG thLeft" width="150">Penerbit *</td>
				<td class="RightColumnBG" colspan="2">
				<? if($c_edit) { ?>
				<input type="text" id="namapenerbit" name="namapenerbit" size="35" class="ControlRead" readonly value="<?= $row['namapenerbit'] ?>">
				<input type="hidden" id="idpenerbit" name="idpenerbit" size="10" value="<?= $row['idpenerbit'] ?>">
				<? } else  echo $p_terbit['namapenerbit'] ?>
				
				<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Lihat Penerbit" onClick="popup('index.php?page=pop_penerbit',570,500);">
					<span id="span_posisi"><u title="Lihat Penerbit" onclick="popup('index.php?page=pop_penerbit',560,450);" style="cursor:pointer;" class="Link">Lihat Penerbit...</u></span>
					<? } ?>
					<? if ($c_delete) { ?>
					<img src="images/add.png" style="cursor:pointer;" title="Insert Penerbit" onClick="popup('index.php?page=inspenerbit',500,300);">
					<span id="span_posisi"><u title="Insert Penerbit" onclick="popup('index.php?page=inspenerbit',500,300);" style="cursor:pointer;" class="Link">Penerbit Baru...</u></span>
					<? } ?>
				</td>
			</tr>
			</td>
		</tr>
		</table>



		</div>
		<div id="items" style="position:relative;top:-2px"> <!-- ============= REFERENSI PUSTAKA ============================= -->
		<header style="width:96.8%;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Referensi Pustaka</h1>
				</div>
				<? if ($r_key!='') { ?>
				<div class="right">
				      <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data eksemplar" onClick="goEks('<?= $p_eksemplar."&idp=".$row['idpustaka']; ?>');">+</div>
				</div>
				<? } ?>
			</div>
		</header>
		<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">			
			<tr    height=20> 
				<td class="LeftColumnBG thLeft" width=150>ISBN</td>
				<td class="RightColumnBG"><?= UI::createTextBox('isbn',$row['isbn'],'ControlStyle',30,30,$c_edit); ?></td>
			</tr>
			
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Kota Terbit</td>
				<td class="RightColumnBG"><?= UI::createTextBox('kota',$row['kota'],'ControlStyle',20,20,$c_edit); ?></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Tahun Terbit</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tahunterbit',$row['tahunterbit'],'ControlStyle',4,4,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
			</tr>
			
			<tr height=20> 
				<td class="LeftColumnBG thLeft">Edisi</td>
				<td class="RightColumnBG"><?= UI::createTextBox('edisi',$row['edisi'],'ControlStyle',60,30,$c_edit); ?></td>
			</tr>
			
			<tr    height=20> 
				<td class="LeftColumnBG thLeft">Tanggal Perolehan *</td>
				<td class="RightColumnBG"><?= UI::createTextBox('tglperolehan',Helper::formatDate($row['tglperolehan']),'ControlStyle',10,10,$c_edit,'readonly="readonly"'); ?>
				<? if($c_edit) { ?>
				<img src="images/cal.png" id="tgleperolehan" style="cursor:pointer;" title="Pilih tanggal perolehan">
				&nbsp;
				<script type="text/javascript">
				Calendar.setup({
					inputField     :    "tglperolehan",
					ifFormat       :    "%d-%m-%Y",
					button         :    "tgleperolehan",
					align          :    "Br",
					singleClick    :    true
				});
				</script>
				
				[ Format : dd-mm-yyyy ]
				<? } ?>
				</td>
			</tr>
			
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Jumlah Hal. Romawi</td>
				<td class="RightColumnBG"><?= UI::createTextBox('jmlhalromawi',$row['jmlhalromawi'],'ControlStyle',6,10,$c_edit); ?> <em>Misal: iv</em></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Jumlah Halaman</td>
				<td class="RightColumnBG"><?= UI::createTextBox('jmlhalaman',$row['jmlhalaman'],'ControlStyle',10,15,$c_edit); ?> <em>Misal: 215 hal (tanpa titik)</em></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Ilustrasi? </td>
				<td class="RightColumnBG"><?= UI::createTextBox('ilustrasi',$row['ilustrasi'],'ControlStyle',10,15,$c_edit); ?> <em>Misal: ilus (tanpa titik)</em></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Index? </td>
				<td class="RightColumnBG"><?= UI::createTextBox('index_buku',$row['index_buku'],'ControlStyle',10,15,$c_edit); ?> <em>Misal: ind (tanpa titik)</em></td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft">Dimensi Pustaka</td>
				<td class="RightColumnBG"><?= UI::createTextBox('dimensipustaka',$row['dimensipustaka'],'ControlStyle',10,15,$c_edit); ?> <em>Tinggi Buku, Misal: 28 cm (tanpa titik)</em></td>
			</tr>
				<tr   height=20> 
				<td class="LeftColumnBG thLeft">Keywords</td>
				<td class="RightColumnBG"><?= UI::createTextBox('keywords',($row['keywords']),'ControlStyle',100,40,$c_edit); ?>
				<? if($c_edit) { ?>&nbsp;&nbsp;<img src="images/popup.png" style="cursor:pointer;" title="Daftar Keywords" onClick="popup('index.php?page=pop_keywords',450,400);">
					<span id="span_key"><u title="Daftar Keywords" onclick="popup('index.php?page=pop_keywords',460,410);" style="cursor:pointer;" class="Link">History Keywords...</u></span>
				<? } ?>
				</td>
			</tr>

				<tr   height=20> 
				<td class="LeftColumnBG thLeft">Links Pustaka</td>
				<td class="RightColumnBG"><?= UI::createTextBox('linkpustaka',($row['linkpustaka']),'ControlStyle',200,40,$c_edit); ?></td>
			</tr>
			</table>
			</div>
		<div id="items" style="position:relative;top:-2px"><!-- ========================== SINOPSIS PUSTAKA ============================= -->
		<header style="width:96.8%;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Sinopsis/Ringkasan Pustaka</h1>
				</div>
				<? if ($r_key!='') { ?>
				<div class="right">
				      <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data eksemplar" onClick="goEks('<?= $p_eksemplar."&idp=".$row['idpustaka']; ?>');">+</div>
				</div>
				<? } ?>
			</div>
		</header>
		<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
			<tr   height=20>
				<td class="LeftColumnBG thLeft">Sinopsis Upload<br><span style="font-weight:normal">( tipe : pdf, Max Upload 5 MB )</span></td>
				<td class="RightColumnBG">
				<?
					$sinop = Sipus::loadBlobFile($rsLOBPDF['filename'], 'url',$r_key,'sinopsis');
				?>
				<? if($c_edit) { ?><input type="file" name="uploadsinopsis" id="uploadsinopsis" size=40 class="ControlStyle"><? } ?>
				<? if($sinop!='') {?>
					<br><a href="index.php?page=download&_auto=1&_ocd=sin&key=<?= $r_key?>" title="Download sinopsis"><img src="images/attach.gif" border=0>Download Sinopsis</a>
					&nbsp; <img src="images/delete.png" style="cursor:pointer" alt="Hapus file sinopsis" onClick="goDelSin('<?= $row['sinopsis_upload'] ?>')">
				<? } ?>
				</td>
			</tr>
			<tr   height=20> 
				<td class="LeftColumnBG thLeft" width="150">Sinopsis Pustaka</td>
				<td class="RightColumnBG"><?= UI::createTextArea('sinopsis',$row['sinopsis'],'ControlStyle',15,65,$c_edit); ?></td>
			</tr>
			</table>
		</div>
		<div id="items" style="position:relative;top:-2px"><!-- ======================================== UPLOAD PUSTAKA ============================= -->
		<header style="width:96.8%;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Upload Pustaka</h1>
				</div>
				<? if ($r_key!='') { ?>
				<div class="right">
				      <div class="addButton" style="float:left; margin:right:10px;"  title="tambah data eksemplar" onClick="goEks('<?= $p_eksemplar."&idp=".$row['idpustaka']; ?>');">+</div>
				</div>
				<? } ?>
			</div>
		</header>
			<table class="GridStyle" width="<?= $p_tbwidth ?>" border="0" cellspacing=0 cellpadding="5">
			<? if($r_key != '') { ?>
				<tr  height=20>
				
				<td class="LeftColumnBG thLeft" width="150">Properti Pustaka</td>
				<td align="center" valign="top">
				<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr  height=20>
					<td width="50%" class="LeftColumnBG thLeft">Uploaded Files</td>
					
				</tr>
				<tr>
					<td width="50%" valign="top">
						<?
						$k=0;
						foreach($rsFile as $f){
							$k++;
							if($c_edit) {
						?>
						<a href="<?= $f['files'] ?>" target="_BLANK"><?= $f['files']!='' ? "<img src='images/attach.gif' border=0> ".$k.'. '.Helper::GetPath($f['files']) : '' ?></a>
						<span id="aksesShow<?=$f['idpustakafile'];?>">
							&nbsp; <img src="images/<?=($f['login']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Harus Login">&nbsp;Harus Login
							&nbsp; <img src="images/<?=($f['download']?"centang":"uncheck")?>.gif" style="cursor:pointer" alt="Bisa didownload">&nbsp;Bisa didownload
							&nbsp; <img src="images/edited.gif" style="cursor:pointer" alt="Ubah Hak Akses" onClick="goShowAkses('<?= $f['idpustakafile'] ?>')">
						</span>
						<span id="aksesHide<?=$f['idpustakafile'];?>" style="display: none;">
							&nbsp;<input <?=($f['login'] ? "checked" : "");?> type="checkbox" name="uLogin<?=$f['idpustakafile'];?>">&nbsp;Harus Login
							&nbsp;<input <?=($f['download'] ? "checked" : "");?> type="checkbox" name="uDownload<?=$f['idpustakafile'];?>">&nbsp;Bisa didownload
							&nbsp; <img src="images/save.png" style="cursor:pointer" alt="Simpan Hak Akses" onClick="goUpdateAkses('<?= $f['idpustakafile'] ?>')">
						</span>
						<? //$temp = explode("/",$f['files']);var_dump($temp);?>
						&nbsp; <img src="images/delete.png" style="cursor:pointer" alt="Hapus file upload" onClick="goDelFile('<?= $f['idpustakafile'] ?>','<?= $f['files'] ?>','<?= $row['noseri'] ?>');">
						<br>
						<?
							}else echo $i.'. '.Helper::GetPath($f['files'])."<br>"; 
						}
						?>
					</td>
				</tr>
				</table>
				</td>
			</tr><? } ?>
			<? if($c_edit) {?>
			<tr    height=20> 
				<td class="LeftColumnBG thLeft"  width="150">File Upload<br><span style="font-weight:normal">( Max filesize = 10 MB )</span></td>
				<td class="RightColumnBG">
				<input type="hidden" value="2" id="theValue" name="theValue" />
				<br>
				1. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="isDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>	
				<p>2. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="iDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>
				<p>3. <input type="checkbox" name="isLogin[]">&nbsp;Harus Login&nbsp;<input type="checkbox" name="isDownload[]">&nbsp;Bisa didownload&nbsp;<input type="file" onchange="if(!checkfile(this.files[0].type,this.files[0].size))$(this).val('');" name="userfile[]" size="40" style="cursor:pointer"></p>
				<div id="createTextbox"></div>
				<input type="button" name="btntext" onClick="CreateTextbox()" value="Tambah File" style="cursor:pointer">
				</td>
			</tr>
			<? } ?>
		</table>
		</div>
		</div>

		<iframe name="upload_iframe" style="display:none;"></iframe>
		<input type="hidden" name="act" id="act">
		<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
		<input type="hidden" name="key2" id="key2" value="<?= $r_key ?>">
		<input type="hidden" name="delfile" id="delfile">
		<input type="hidden" name="delkey" id="delkey">
		<input type="hidden" name="delseri" id="delseri">
		<input type="hidden" name="delfilepus" id="delfilepus">
			
		</td>
		</tr>
		</table>
		</form>
		</div>
	</div>
</div>


<div id="div_author" style="display:none;">
</div>

<div align="left" id="div_autocomplete" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;">
	<table bgcolor="#FFFFFF" id="tab_autocomplete" cellpadding="3" cellspacing="0"></table>
</div>
</body>

<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script type="text/javascript" src="scripts/jquery.xautox.js"></script>
<script language="javascript">
	$('#uploadsinopsis').bind('change', function() {
		alerr = "";
		sts = false;
		if(this.files[0].type != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(this.files[0].size > 5000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 5MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			$('#uploadsinopsis').val("");
		}
		
	
	});
	
	$('.t_pustakafile').bind('change', function() {
		alerr = "";
		sts = false;
		if(this.files[0].type != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(this.files[0].size > 10000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 10MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			$('.t_pustakafile').val("");
		}
		
	
	});
	
	function checkfile(jenis,ukuran){
		alerr = "";
		sts = false;
		if(jenis != "application/pdf"){
		      sts = true;
		      alerr += "Jenis file bukan .pdf";
		}
		if(ukuran > 10000000){
		      sts = true;
		      alerr += "\nUkuran File lebih dari 10MB.";
		      
		}
		
		if(sts){
			alert(alerr);
			return false;
		}else
			return true;
	}
	
$(document).ready(function() {
	$("div.tabs a[id='tablink']").click(function() {
		var index = $("div.tabs a").index(this);
		
		$("div.tabs li").removeAttr("class");
		$(this).parent("li").attr("class","selected");
		
		$("div[id='items']").hide();
		$("div[id='items']").eq(index).show();
	});
	
	chooseTab(0);
});

function chooseTab(idx) {
	$("div.tabs a").eq(idx).triggerHandler("click");
}
</script>

<script language="javascript">
function goDelPhoto(){
	var delphoto=confirm("Apakah Anda yakin akan menghapus foto ini ?");
	if (delphoto){
		document.getElementById('act').value="delphoto";
		goSubmit();
	}

}

function goDelSin(){
	var delsin=confirm("Apakah Anda yakin akan menghapus file sinopsis ?");
	if (delsin){
		document.getElementById('act').value="delsin";
		goSubmit();
	}

}

function goDelFilePus(key4){
	var delfilepus=confirm("Apakah Anda yakin akan menghapus file pustaka tersebut?");
	if (delfilepus){
		document.getElementById('act').value="delfilepus";
		document.getElementById('delfilepus').value=key4;
		goSubmit();
	}

}

function goDelFile(key1,key2,key3){
	var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
	if(delfile){
		document.getElementById('act').value="delfile";
		document.getElementById('delkey').value=key1;
		document.getElementById('delfile').value=key2;
		document.getElementById('delseri').value=key3;
		goSubmit();
	}
}

function saveData() {
	if(cfHighlight("idpustaka,judul,kdbahasa,kdjenispustaka,namapenerbit,tglperolehan")){
		
		j = document.getElementById("judul").value;
		if(j.trim() == ""){
			alert("Data Judul Tidak boleh kosong");
			exit();
		}
		
		var authkey = document.getElementById("namaauth").value;
		var authkey2 = document.getElementById("addtopik").value;
		goSave();
	}
	
}
function addAuthor() {
	var jmlp = $("#jmlpeng").val();
	var idauthor = $("#idauthor").val();
	if(idauthor=='') {
		$("#span_error2").show();
		setTimeout('$("#span_error2").hide()',1000);
		return false; 
	} 
	else {
		
		// cek apakah sudah ada
		if($("[name='addauthor[]'][value='"+idauthor+"']").length > 0) {
			$("#span_error").show();
			setTimeout('$("#span_error").hide()',1000);
			return false;
		}
	}
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_bukosong:visible").length > 0)
		$("#tr_bukosong").hide();
	
	var newtab = $($("#table_templatebu tbody").html()).insertBefore("#tr_authorhidden");
	var newtdf = newtab.find("td").eq(0);
	var addauthor = newtdf.find("[name='addauthor[]']");
	var namaauth = newtdf.find("[name='namaauth[]']");
	var namaauthor = $("#namaauthor").val();
	
	//
	var nadep = newtdf.find("[name='namdep[]']");
	var nabel = newtdf.find("[name='nambel[]']");
	var isbd = newtdf.find("[name='isbad[]']");
	var namaauthordep = $("#namaauthordep").val();
	var namaauthorbel = $("#namaauthorbel").val();
	var isbadn = $("#isbadan").val();
	nadep.val(namaauthordep);
	nabel.val(namaauthorbel);
	isbd.val(isbadn);
	
	
	newtdf.prepend(namaauthor);
	addauthor.val(idauthor);
	namaauth.val(namaauthor);
	addauthor.attr("disabled",false);
	document.getElementById("namapeng").value=namaauthor;
	
	document.getElementById("jmlpeng").value=parseInt(jmlp)+1;
}

function delAuthor(elem) {
	var jmlp = $("#jmlpeng").val();
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
	
	// jika kosong, tampilkan tanda kosong
	if($("#tr_tambahbu").prev().is("#tr_bukosong"))
		$("#tr_bukosong").show();
		
	if(jmlp>0)
		document.getElementById("jmlpeng").value=parseInt(jmlp)-1;
}
</script>
<script language="javascript">

function addTopik() {
	var idtopik = $("#idtopik").val();
	
	if (idtopik==''){
		$("#span_error4").show();
		setTimeout('$("#span_error4").hide()',1000);
		return false;
		} else {
	// cek apakah sudah ada
	if($("[name='addtopik[]'][value='"+idtopik+"']").length > 0) {
		$("#span_error3").show();
		setTimeout('$("#span_error3").hide()',1000);
		return false;
	}}
	
	// jika ada baris tanda kosong, sembunyikan
	if($("#tr_tpkosong:visible").length > 0)
		$("#tr_tpkosong").hide();
	
	var newtab = $($("#table_templatetp tbody").html()).insertAfter("#tr_tambahtp");
	var newtdf = newtab.find("td").eq(0);
	var addtopik = newtdf.find("[name='addtopik[]']");
	var namatopik = $("#namatopik").val();
	
	newtdf.prepend(namatopik);
	addtopik.val(idtopik);
	addtopik.attr("disabled",false);
}

function delTopik(elem) {
	var tr = $(elem).parents("tr").eq(0);
	
	tr.replaceWith("");
	
	// jika kosong, tampilkan tanda kosong
	if($("#tr_tambahtp").prev().is("#tr_tpkosong"))
		$("#tr_tpkosong").show();
}

</script>

<script language="javascript">
function goEks(file3) {//alert(file3);
	document.getElementById("perpusform").action = file3;
	document.getElementById("key").value = '';
	goSubmit();
}

function goShowAkses(key){ 
	$("#aksesShow"+key).hide();
	$("#aksesHide"+key).show();
}

function goUpdateAkses(key){
	document.getElementById('act').value="ubahaksesfile";
	document.getElementById('delkey').value=key;
	goSubmit();
}

</script>
</html>
