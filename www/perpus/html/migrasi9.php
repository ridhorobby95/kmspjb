<?php 
	set_time_limit(0);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');

	//$conn = Factory::getConn();
	$connExcel = Factory::getConnExcel();
//$connExcel->debug = true;
#cron ms_author
/*	echo "<b>.:Cron ms_author:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	$sqlstr = "select * from ms_author ";
	$rs = $connExcel->Execute($sqlstr);
	$a = 0;
	while ($row = $rs->FetchRow()){
		$row['namadepan'] = trim(substr(Helper::removeSpecial($row['namadepan']),0,99));
		$idauthor = $conn->GetOne("select count(*) from ms_author where lower(namadepan) = lower('".$row['namadepan']."') or idauthor = ".$row['idauthor']." ");
		if($idauthor == 0){
			$a++;
			$record=array();
			$record['idauthor']= $row['idauthor'];
			$record['namadepan'] = $row['namadepan'];
			$record['t_user'] = "MIGRASIEXCEL";
			$col = $conn->Execute("select * from ms_author where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			echo $a.".&nbsp;&nbsp;SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data ms_author gagal ditambahkan.</span><br/>';
			}else
				echo '<span style="color: blue;">Data ms_author telah ditambahkan.</span><br/>';
		}	
	}

	
	
#cron ms_penerbit
	echo "<b>.:Cron ms_penerbit:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	$sqlstr = "select * from ms_penerbit ";
	$rs = $connExcel->Execute($sqlstr);
	$p = 0;
	while ($row = $rs->FetchRow()){		
		$row['namapenerbit'] = trim(Helper::removeSpecial($row['namapenerbit']));
		$idpenerbit = $conn->GetOne("select count(*) from ms_penerbit where lower(namapenerbit) = lower('".$row['namapenerbit']."') or idpenerbit = ".$row['idpenerbit']." ");
		if($idpenerbit == 0){
			$p++;
			$record=array();
			$record['idpenerbit']= $row['idpenerbit'];
			$record['namapenerbit'] = $row['namapenerbit'];
			$record['t_user'] = "MIGRASIEXCEL";
			$col = $conn->Execute("select * from ms_penerbit where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			echo $p.".&nbsp;&nbsp;SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data ms_penerbit gagal ditambahkan.</span><br/>';
			}else
				echo '<span style="color: blue;">Data ms_penerbit telah ditambahkan.</span><br/>';
		}	
	}

	
#cron ms_pustaka
	echo "<b>.:Cron ms_pustaka:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	$sqlstr = "select * from ms_pustaka where idpustaka > 1030 ";
	$rs = $connExcel->Execute($sqlstr);
	$m = 0;
	while ($row = $rs->FetchRow()){ echo $row['judul'];
		$row['noseri'] = Helper::removeSpecial($row['noseri']);
		$idpustaka = $conn->GetOne("select count(*) from ms_pustaka where lower(noseri) = lower('".$row['noseri']."') or idpustaka = ".$row['idpustaka']." ");
		if($idpustaka == 0){
			$m++;
			$record=array();
			$record['idpustaka']= $row['idpustaka'];
			$record['kdbahasa'] = Helper::removeSpecial($row['kdbahasa']);
			$record['kdjenispustaka'] = Helper::removeSpecial($row['kdjenispustaka']);
			$record['idpenerbit'] = $row['idpenerbit'];
			$record['judul'] = Helper::removeSpecial($row['judul']);
			$record['tglperolehan'] = Helper::removeSpecial($row['tglperolehan']);
			$record['authorfirst1'] = Helper::removeSpecial($row['authorfirst1']);
			$record['kota'] = Helper::removeSpecial($row['kota']);
			$record['namapenerbit'] = Helper::removeSpecial($row['namapenerbit']);
			$record['edisi'] = Helper::removeSpecial($row['edisi']);
			$record['isbn'] = Helper::removeSpecial($row['isbn']);
			$record['tahunterbit'] = Helper::removeSpecial($row['tahunterbit']);
			$record['sinopsis'] = Helper::removeSpecial($row['keterangan']);
			$record['noseri'] = $row['noseri'];
			$record['jmlhalaman'] = Helper::removeSpecial($row['jmlhalaman']);
			$record['t_user'] = "MIGRASIEXCEL";
			$col = $conn->Execute("select * from ms_pustaka where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			echo $m.".&nbsp;&nbsp;SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			
			if($err){
				echo '<span style="color: blue;">Data ms_pustaka telah ditambahkan.</span><br/>';
				#pp_eksemplar
				echo "<b>.:Cron pp_eksemplar:.</b><br/>";
				echo "<b>".str_repeat("-",100)."</b><br/>";
				$eksemplarpustaka = $connExcel->Execute("select * from pp_eksemplar where idpustaka = ".Helper::removeSpecial($row['idpustaka'])." ");
				while($rep = $eksemplarpustaka->FetchRow()){
					$record=array();
					$record['ideksemplar']= $rep['ideksemplar'];
					$record['idpustaka']= $rep['idpustaka'];
					$record['kdklasifikasi'] = 'SR';
					$record['kdkondisi'] = 'V';
					$record['kdperolehan'] = 'B';
					$record['kdlokasi'] = 'KP';
					$record['statuseksemplar'] = 'ADA';
					$record['noseri'] = Helper::removeSpecial($rep['noseri']);
					$record['t_user'] = 'MIGRASIEXCEL'; 
					$col = $conn->Execute("select * from pp_eksemplar where 1=-1");
					$sql = $conn->GetInsertSQL($col,$record,true);
					echo "&nbsp;&nbsp;SQL : ".$sql."<br/>";
					$err = $conn->Execute($sql);
					if(!$err){
						echo '<span style="color: red;">Data pp_eksemplar gagal ditambahkan.</span><br/>';
					}else
						echo '<span style="color: blue;">Data pp_eksemplar telah ditambahkan.</span><br/>';
				}				
				
				#pp_author
				echo "<b>.:Cron pp_author:.</b><br/>";
				echo "<b>".str_repeat("-",100)."</b><br/>";
				$authorpustaka = $connExcel->Execute("select * from pp_author where idpustaka = ".Helper::removeSpecial($row['idpustaka'])." ");
				while($rap = $authorpustaka->FetchRow()){
					$record=array();
					$record['idpustaka']=Helper::removeSpecial($row['idpustaka']);
					$record['idauthor'] = Helper::removeSpecial($rap['idauthor']);
					$col = $conn->Execute("select * from pp_author where 1=-1");
					$sql = $conn->GetInsertSQL($col,$record,true);
					echo "&nbsp;&nbsp;SQL : ".$sql."<br/>";
					$err = $conn->Execute($sql);
					if(!$err){
						echo '<span style="color: red;">Data pp_author gagal ditambahkan.</span><br/>';
					}else
						echo '<span style="color: blue;">Data pp_author telah ditambahkan.</span><br/>';
				}
				
			}
			else
				echo '<span style="color: red;">Data ms_pustaka gagal ditambahkan.</span><br/>';
		}	
	}
 */	
?>