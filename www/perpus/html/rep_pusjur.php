<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Pustaka Per Jurusan';

	$p_filerep = 'repp_pusjur';
	$p_tbwidth = 480;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select namajurusan, kdjurusan from lv_jurusan order by kdjurusan, namajurusan ");
	$l_jurusan = $rs_cb->GetMenu2('kdjurusan','',true,false,0,'id="kdjurusan" class="ControlStyle" style="width:150"');
	$l_jurusan = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jurusan);
	
	$a_fil = array('=' => '=', '<' => '<', '>' => '>', 'between' => 'between');
	$l_fil = UI::createSelect('fil',$a_fil,'','ControlStyle',true,' onChange="shownext(this.value)"');
	
	//list jenis pustaka
	$rs_cb = $conn->Execute("select namajenispustaka, kdjenispustaka from lv_jenispustaka where 1=1 and kdjenispustaka in (".$_SESSION['roleakses'].") order by kdjenispustaka");
	$l_jenis = $rs_cb->GetMenu2('kdjenispustaka',$f5,true,false,0,'id="kdjenispustaka" class="ControlStyle" style="width:150" ');
	$l_jenis = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_jenis);

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="scripts/forinplace.js"></script>
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="document.getElementById('ke').focus()">
<?php include ('inc_menu.php'); ?>
<div id="wrapper">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:<?= $p_tbwidth ?>px;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Parameter Laporan Pustaka Per Jurusan</h1>
				</div>
			</div>
		</header>
		<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
			<tr>
				<td class="LeftColumnBG thLeft">Prodi/Jurusan</td>
				<td><?= $l_jurusan ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Jenis Pustaka</td>
				<td><?= $l_jenis ?></td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Tahun Terbit</td>
				<td><?= $l_fil ?>&nbsp;<input type="text" name="tahun" id="tahun" size=5 maxlength=5 onkeydown="return onlyNumber(event,this,false,true)">
					<span id="shown" style="display: none;">&nbsp;s/d&nbsp;<input type="text" name="tahun2" id="tahun2" size=5 maxlength=5 onkeydown="return onlyNumber(event,this,false,true)"></span>
				</td>
			</tr>
			<tr>
				<td class="LeftColumnBG thLeft">Format</td>
				<td class="RightColumnBG"><?= $l_format; ?></td>
			</tr>
			<tr>
				<td colspan="2" class="footBG">&nbsp;</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
		</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
function goPreSubmit() {	
	n = $("#fil").val();
	if(n == "between"){
		if(cfHighlight("tahun,tahun2"))
			goSubmit();
	}else
		goSubmit();

}

function shownext(n){
	if(n == "between")
		$("#shown").show();
	else
		$("#shown").hide();
}
</script>
</html>