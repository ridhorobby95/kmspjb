<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
//$conn->debug = true;	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng,false);

	// otorisasi user
	$c_add = $a_auth['cancreate'];
	$c_edit = $a_auth['canedit'];
		
	// require tambahan
	
	// definisi variabel halaman
	$p_dbtable = 'pp_usul';
	$p_window = '[PJB LIBRARY] Daftar Tugas Akhir Mandiri Mahasiswa';
	$p_title = 'Daftar Tugas Akhir Mandiri Mahasiswa';
	$p_tbheader = '.: Daftar Tugas Akhir Mandiri Mahasiswa :.';
	$p_col = 9;
	$p_tbwidth = 978;
	$p_filedetail = Helper::navAddress('list_verifikasita.php');
	$p_filedetailverify = Helper::navAddress('data_verifikasita.php');
	$p_filedetailtamandiri = Helper::navAddress('data_detailtamandiri.php');
	$p_id = "uploadta";
	
	// definisi variabel untuk paging, sorting, dan filtering (selanjutnya disebut ex :D)
	$p_defsort = 'iduploadta';
	$p_row = 15;
	$p_down = '<img src="images/down.gif">';
	$p_up = '<img src="images/up.gif">';
	
	// sql untuk mendapatkan isi list
	$p_sqlstr="	select c.idhistorynotifikasi,b.namaanggota,a.* from pp_uploadta a
				left join ms_anggota b on a.idanggota = b.idanggota
				left join pp_historynotifikasiuploadta c on c.iduploadta = a.iduploadta and npktujuannotifikasi = 'pusdig' and stsnotifikasi = 0
				where 1=1 and userrequestval is not null ";
  	
	//$p_order=$conn->GetRow("select * from pp_orderpustaka where 1=1");
	
	// pengaturan ex
	if (!empty($_POST))
	{
		$r_aksi=Helper::removeSpecial($_POST['act']);
		$id=Helper::removeSpecial($_POST['id']);
		$idanggota=Helper::removeSpecial($_POST['idanggota']);
		
		if($r_aksi == 'nextproses'){
			$record = array();
				//$record = Helper::cStrFill($_POST);
				
				$p_sqlstr4 = "select * from pp_uploadta
					 where iduploadta=$id";
				$rowmandiri = $conn->GetRow($p_sqlstr4);
				
				// inisialisasi kolom2 
				$record = $rowmandiri;
				
				$conn->StartTrans();
						
				$record['tgladministrasita']=date("Y-m-d H:i:s");;
				$record['qtyttb']=1;
				$record['iduploadta']=$id;
				$record['ststtb']=1;
				$record['stsadministrasita']=1;
				$record['tgldaftarta']=date("Y-m-d");;
				$record['npkadministrasita']=$_SESSION['PERPUS_USER'];
				
				$pembimbing = array();
				$pembimbing[0] = $rowmandiri['pembimbing1'];
				$pembimbing[1] = $rowmandiri['pembimbing2'];
				$pembimbing[2] = $rowmandiri['pembimbing3'];
				
				$record['pembimbing'] = implode(", ",$pembimbing);
				$record['tahuterbit']=$rowmandiri['tahuterbit'];
				//$record['penerbit']=$rowmandiri['penerbit'];
				
				/*if($rowmandiri['pembimbing3'] != '')
					$record['pembimbing'] = $rowmandiri['pembimbing1'].','.$rowmandiri['pembimbing2'].','.$rowmandiri['pembimbing3'];
				if($rowmandiri['pembimbing2'] != '')
					$record['pembimbing'] = $rowmandiri['pembimbing1'].','.$rowmandiri['pembimbing2'];
				else $record['pembimbing'] = $rowmandiri['pembimbing1'];*/
				
				Helper::Identitas($record);
				
				$iddaftarta=Sipus::GetLast($conn,pp_ta,iddaftarta);
				$record['iddaftarta']=$iddaftarta;	
				$record['npkdaftarta']=$_SESSION['PERPUS_USER'];
							
				// input pp_ta
				Sipus::InsertBiasa($conn,$record,pp_ta); 
				// input pp order
				Sipus::InsertBiasa($conn,$record,pp_orderpustaka);
				
				//input pp_orderttb n ttb
				$getttb=$conn->GetRow("select max(nottb) as ttb from pp_ttb where to_char(tglttb,'Year')=to_char(current_date,'Year')");
				$slice=explode("/",$getttb['ttb']);
				$isttb=$slice[0]+1;
				$nottb=Helper::strPad($isttb,4,0,'l')."/TTP/".Helper::bulanRomawi(date('d-m-Y'))."/".date('Y');
				$idttb=Sipus::GetLast($conn,pp_ttb,idttb);
				
				$recttb = array();
				$recttb['idttb']=$idttb;
				$recttb['nottb']=$nottb;
				$recttb['tglttb']=date('Y-m-d');
				$recttb['npkttb']=$_SESSION['PERPUS_USER'];
				$recttb['jnsttb']=3;
				
				Sipus::InsertBiasa($conn,$recttb,pp_ttb);
								
				$ttb=$recttb['idttb'];
				
				$recdttb['idorderpustaka']=$conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq");
				$recdttb['idttb']=$ttb;
				$recdttb['qtyttbdetail']=1;
				$recdttb['ststtbdetail']=1;			
				Sipus::InsertBiasa($conn,$recdttb,pp_orderpustakattb);
				
				$recordupdate = array();
				$recordupdate['iddaftarta']=$iddaftarta;
				Sipus::UpdateBiasa($conn, $recordupdate, 'pp_uploadta', " iduploadta ",$id);
				
				$conn->CompleteTrans();
		}
		
		//if ($r_aksi=='filter') {
		$filt=Helper::removeSpecial($_POST['txtid']);
		if($filt!='')
		$p_sqlstr.=" and a.idanggota='$filt' ";
		$_SESSION[$p_id.'.filteridanggota'] = $filt;
		
		//}
		// if ($r_aksi=='status'){
			$status=Helper::removeSpecial($_POST['status']);
			if($status=='x')
			$p_sqlstr.="";
			else if($status=='0')
			$p_sqlstr.=" and statusvalstafdigitalisasi <> 2 ";
			else if($status=='1')
			$p_sqlstr.=" and statusvalstafdigitalisasi = 2 ";
			
			$_SESSION[$p_id.'.status'] = $status;
		// }
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$p_sort 	= Helper::removeSpecial($_REQUEST['sort']);
		$p_filter	= Helper::removeSpecial($_REQUEST['filter'],'change');
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.sort'] = $p_sort;
		$_SESSION[$p_id.'.filter'] = $p_filter;
		
		$r_aksi = Helper::removeSpecial($_POST['act']);
		$r_key = Helper::removeSpecial($_POST['key']);
		
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
		if ($_SESSION[$p_id.'.sort'])
			$p_sort = $_SESSION[$p_id.'.sort'];
		if ($_SESSION[$p_id.'.filter'])
			$p_filter = $_SESSION[$p_id.'.filter'];
		if ($_SESSION[$p_id.'.status'])
			$status = $_SESSION[$p_id.'.status'];
		if ($_SESSION[$p_id.'.filteridanggota'])
			$status = $_SESSION[$p_id.'.filteridanggota'];
	}
  
	if (!$p_page){
		$p_page = 1; // halaman default adalah 1
	}
	// pengaturan filter ex
	if (isset($p_filter) and $p_filter != '') 
	{
		$p_status = '(filtered)';
		$filterarray = explode(':',$p_filter);
		for ($i=0;$i<count($filterarray);$i = $i + 3) 
		{
			$filterstr = '';
			$filtercol = $filterarray[$i];
			$filterdata = $filterarray[$i+1];
			$filtertype = $filterarray[$i+2];
				
			// pemeriksaan operator perbandingan
			$arrop = array('<>','<=','>=','<','>','=');
			for ($n=0;$n<count($arrop);$n++) {
				$oppos = strpos($filterdata,$arrop[$n]);
				if ($oppos !== false) { // operator perbandingan ditemukan
					$filterop = $arrop[$n];
					$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
					break;
				}
			}
			if (!$filterop)
				$filterop = '='; // default operator
		
			switch ($filtertype) {
				case 'C' : 	// char atau varchar
							$filterstr .= 'lower('.$filtercol.") like '".strtr(strtolower(trim($filterdata)),'*','%')."'";							
							break;
				case 'I' : 	// integer
				case 'N' : 	// numeric atau float
							$filterstr .= $filtercol.$filterop.$filterdata;
							break;
				case 'L' : 	// boolean
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				case 'D' : 	// date
							$filterdata = date('Y-M-d', strtotime($filterdata));
							$filterstr .= $filtercol.$filterop."'".$filterdata."'";
							break;
				default	 :	// yang lain
							$filterstr .= $filtercol.' '.$filterdata;
							break;
			}
			$p_sqlstr .= " and (" . $filterstr . ")";
		} // end for
	}

	// pengaturan sort ex
	if (isset($p_sort) and $p_sort != '')
		$p_sqlstr .= " order by $p_sort";
	else
		$p_sqlstr .= " order by $p_defsort desc";
	
	// menggambarkan indikasi sort
	if(empty($p_sort))
		$p_xsort[$p_defsort] = ' '.$p_up;
	else {
		list($col,$dir) = explode(' ',$p_sort);
		$p_xsort[$col] = ' '.($dir == 'desc' ? $p_down : $p_up);
	}
	
	// eksekusi sql list
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$showlist = true;
	}

	$a_proses = array('x' => '-- Semua --', '0' => 'Belum Valid', '1' => 'Valid');
	$l_proses = UI::createSelect('status',$a_proses,$status,'ControlStyle',$c_edit,'onchange="goStatus()"');
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	<link href="style/pager.css" type="text/css" rel="stylesheet">
	<link href="style/officexp.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/forpager.js"></script>
	<link rel="shortcut icon" href="images/favicon.ico" />
	
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtid').focus();initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<div id="main_content">
<?php include('inc_menu.php'); ?>
  <div id="wrapper">
<div class="SideItem" id="SideItem">
<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
<? include_once('_notifikasi.php'); ?>
<div class="filterTable">
          <table width="100%">
            <tr>
              <td><strong>Masukkan Id Anggota</strong></td>
              <td>:</td>
              <td><input type="text" name="txtid" id="txtid" maxlength="20" value="<?= $filt ?>"  onkeydown="etrFil(event);"> </td>
              <td>&nbsp;</td>
              <!--
                <td>Topik Pustaka</td>
                <td width="30%">: <input type="text" id="caritopik" size="35" name="caritopik" value="<?//= $f1 ?>"></td>
                -->
              <td><strong>Status</strong></td>
              <td>:</td>
              <td><?= $l_proses ?></td>
              <td rowspan="2"><input type="button" value="Filter" class="ControlStyle" onClick="javascript:goFilt()">
                <input type="button" value="Refresh" class="ControlStyle" onClick="javascript:goRef()"></td>
            </tr>
            
          </table>
        </div>
        <br/>
        <header style="width:100%">
          <div class="inner">
            <div class="left title"> <img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" onerror="loadDefaultActImg(this)">
              <h1>
                <?= $p_title ?>
              </h1>
            </div>
          </div>
        </header>
        
<table width="100%" border="0" cellpadding="2" cellspacing=0 class="GridStyle">

	<tr> 
		<th width="80" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.tgluploadta:D');">Tanggal<?= $p_xsort['tgluploadta']; ?></th>		
		<th width="120" nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','b.namaanggota:C');">Nama Mahasiswa <?= $p_xsort['namaanggota']; ?></th>
		<th width="250"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.judul:C');">Judul Tugas Akhir/Skripsi .dll<?= $p_xsort['judul']; ?></th>		
		<th width="150"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.authorfirst1:C');">Penulis  <?= $p_xsort['authorfirst1']; ?></th>		
		<th width="150"nowrap align="center" class="SubHeaderBGAlt" style="cursor:pointer;" onClick="PopupMenu('popPaging','a.pembimbing1:C');">Pembimbing<?= $p_xsort['pembimbing1']; ?></th>		
		<th width="110" nowrap align="center" class="SubHeaderBGAlt">Status</th>
		<th width="50" nowrap align="center" class="SubHeaderBGAlt">Aksi</th>
	</tr>	
		<?php
		$i = 0;
		if($showlist) {
			// mulai iterasi
			while ($row = $rs->FetchRow()) 
			{
				if ($i % 2) $rowstyle = 'NormalBG';  else $rowstyle = 'AlternateBG'; $i++;
				if ($row['iduploadta'] == '0') $rowstyle = 'YellowBG';
	?>
	<tr class="<?= $rowstyle ?>" height=20> 
		<td align="center">	<?= Helper::tglEng($row['tgluploadta'],false); ?></td>	
		<td align="left"><?= $row['idanggota'].' - '.$row['namaanggota']; ?></td>		
		<td align="left"><u onClick="goDetail2('<?= $p_filedetailtamandiri; ?>','<?= $row['iduploadta'] ?>','<?= $row['idhistorynotifikasi'] ?>');"  title="Detail Tugas Akhir" style="cursor:pointer;color:#3300FF;"><?= $row['judul']; ?></u></td>
		<td align="left">
						<?php
							echo "1. ".$row['authorfirst1']. " " .$row['authorlast1']; 
							if ($row['authorfirst2']) 
							{
								echo " <br> ";
								echo "2. ".$row['authorfirst2']. " " .$row['authorlast2'];
							}
							if ($row['authorfirst3']) 
							{
								echo "<br>";
								echo "3. ".$row['authorfirst3']. " " .$row['authorlast3'];
							}
						?>
		</td>
		<td align="left"><?php
							echo "1. ".$row['pembimbing1']; 
							if ($row['pembimbing2']) 
							{
								echo " <br> ";
								echo "2. ".$row['pembimbing2'];
							}
							if ($row['pembimbing3']) 
							{
								echo "<br>";
								echo "3. ".$row['pembimbing3'];
							}
							?>
		</td>
		<td align="center">
		<?/*?>
		<? 	if($row['pembimbing1'] != ''){
				if($row['statusvalpembimbing1'] == 2){
		?>
			<u title="Pembimbing 1 : Sudah Valid" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb1.png"></u>
		<?} else if($row['statusvalpembimbing1'] == 1){?>
			<u title="Pembimbing 1 : Perlu Perbaikan" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb1red.png"></u>
		<?} else {?>
			<u title="Pembimbing 1 : Belum di Proses" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb1off.png"></u>
		<?}}?>
		<? 	if($row['pembimbing2'] != ''){
				if($row['statusvalpembimbing2'] == 2){
		?>
			<u title="Pembimbing 2 : Sudah Valid" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb2.png"></u>
		<?} else if($row['statusvalpembimbing2'] == 1){?>
			<u title="Pembimbing 2 : Perlu Perbaikan" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb2red.png"></u>
		<?} else {?>
			<u title="Pembimbing 2 : Belum di Proses" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb2off.png"></u>
		<?}}?>
		<? 	if($row['pembimbing3'] != ''){
				if($row['statusvalpembimbing3'] == 2){
		?>
			<u title="Pembimbing 3 : Sudah Valid" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb3.png"></u>
		<?} else if($row['statusvalpembimbing3'] == 1){?>
			<u title="Pembimbing 3 : Perlu Perbaikan" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb3red.png"></u>
		<?} else {?>
			<u title="Pembimbing 3 : Belum di Proses" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/pemb3off.png"></u>
		<?}}?>
		<?*/?>
		<? if($row['statusvalstafdigitalisasi'] == 2){
		?>
			<u title="Petugas : Sudah Valid" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/petugas.png"></u>
		<?} else if($row['statusvalstafdigitalisasi'] == 1){?>
			<u title="Petugas : Perlu Perbaikan" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/petugasred.png"></u>
		<?} else {?>
			<u title="Petugas : Belum di Proses" style="cursor:pointer;color:#3300FF;"><img src="images/validasi/petugasoff.png"></u>
		<?}?>
		</td>
		
		<td align="center">
		<? if($c_edit){ /* ?>
		<u onClick="goDetail('<?= $p_filedetail; ?>','<?= $row['iduploadta'] ?>');"  title="Daftar History Verifikasi" style="cursor:pointer;color:#3300FF;"><img src="images/listed6.gif"></u>
		<?*/if($row['idhistorynotifikasi'] != ''){?>
		<u onClick="goDetail2('<?= $p_filedetailverify; ?>','<?= $row['iduploadta'] ?>','<?= $row['idhistorynotifikasi'] ?>');"  title="Proses Verifikasi" style="cursor:pointer;color:#3300FF;"><img src="images/edited.gif"></u>
		<?} else  {?>
		<u style="cursor:pointer;color:#3300FF;"><img src="images/edited2.gif"></u>
		<?}?>
		<? if($row['tglcetak'] != '' && $row['iddaftarta'] == '' ){
		?>
		<u onClick="goProses('<?= $row['iduploadta'] ?>');"  title="Proses ke Pengadaan Hardcopy TA" style="cursor:pointer;color:#3300FF;"><img src="images/tombol/arrow_active.png"></u>
		<?}else if($row['tglcetak'] != '' && $row['iddaftarta'] != '' ) {?>
		<u style="cursor:pointer;color:#3300FF;"><img src="images/tombol/arrow_non.png"></u>
		
		<? }} ?>
		</td>
	</tr>
	<?php
			}
		}
		if ($i==0) {
	?>
	<tr height="20">
		<td align="center" colspan="<?= $p_col; ?>"><b>Data tidak ditemukan.</b></td>
	</tr>
	<?php } ?>
	<tr> 
		<td class="FootBG" align="right" colspan="<?= $p_col; ?>"> 
			Halaman <?= $p_page ?>/<?= $p_lastpage ?> <?= $p_status ?>
		</td>
	</tr>
	
</table>
<?php require_once('inc_listnav.php'); ?>
<br>



<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
<input type="hidden" name="sort" id="sort" value="<?= $p_sort ?>">
<input type="hidden" name="filter" id="filter" value="<?= $p_filter ?>">
<input type="hidden" name="key" id="key">
<input type="hidden" name="idnotif" id="idnotif">
<input type="hidden" name="key2" id="key2">
<input type="hidden" name="id" id="id">
<input type="hidden" name="idanggota" id="idanggota">
<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
<input type="text" name="test" id="test" style="visibility:hidden">

<div id="popFilter" name="popFilter" class="FilterDialog" onBlur="this.style.display='none'" > 
	Filter Criteria <br>
	<input class="FilterText" type="text" name="txtFilter" id="txtFilter" size="20" onKeyDown="return doFilter(event);" onBlur="document.getElementById('popFilter').style.display='none'">
</div>

<div id="popPaging" class="menubar" style="position:absolute; display:none; top:0px; left:0px;z-index:10000;" onMouseOver="javascript:overpopupmenu=true;" onMouseOut="javascript:overpopupmenu=false;">
<table width=100  class="menu-body">
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
        <td onClick="goSort('asc');" > <img align="absmiddle" src="images/sortascending.gif"> Sort Asc</td>
  	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">       
		<td onClick="goSort('desc');"><img align="absmiddle" src="images/sortdescending.gif"> Sort Desc</td>
  	</tr>
   	<tr>
		<td class="separator"><div class="separator-line"></div></td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(true);"><img align="absmiddle" src="images/addfilter.gif"> Filter ...</td>
	</tr>
	<tr class="menu-button" onMouseMove="this.className = 'hover';" onMouseOut="this.className = '';">
		<td onClick="goFilter(false);"><img align="absmiddle" src="images/removefilter.gif"> Remove Filter</td>
	</tr>
</table>
</div>

</form>
</div>
</div>
</div>


</body>

<script src="scripts/jqModal.js"></script>


<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script type="text/javascript" src="scripts/jquery.common.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl").mask("99-99-9999");
	   $("#tgl1").mask("99-99-9999");
});
</script>

<script type="text/javascript">


var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;
</script>

<script type="text/javascript">
function etrFil(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		goFilt();
}
function goStatus(id,anggota){
	document.getElementById("act").value="status";
	goSubmit();

}
function goStatus1(id,anggota){
	document.getElementById("id").value=id;
	document.getElementById("idanggota").value=anggota;

}

function goProses(key){
	var conf=confirm("Apakah Anda Yakin Akan Proses ke Pengadaan Hardcopy TA ?");
	if(conf){
	document.getElementById("act").value="nextproses";
	document.getElementById('id').value=key;
	document.getElementById('key').value=key;
	goSubmit();
	}
}

function goValidasi(){
	document.getElementById("act").value="validasi";
	goSubmit();
}

function goValPus(){
	document.getElementById("act").value="valpus";
	goSubmit();
}
function goFilt(){
	document.getElementById("act").value="filter";
	goSubmit();
}

function goRef() {
	document.getElementById("act").value="";
	document.getElementById("txtid").value="";
	document.getElementById("page").value = 1;
	document.getElementById("sort").value = "";
	document.getElementById("filter").value = "";
	goSubmit();
}

function goDetail2(file,key,idnotif) {
	document.getElementById("perpusform").action = file;
	document.getElementById("key").value = key;
	document.getElementById("idnotif").value = idnotif;
	goSubmit();
}
</script>
</html>