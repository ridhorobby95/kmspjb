<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// variabel request
	$r_format = Helper::removeSpecial($_REQUEST['format']);
	$r_kondisi = Helper::removeSpecial($_POST['kdkondisi']);
	$r_jenis = Helper::removeSpecial($_POST['kdjenispustaka']);
	$r_lokasi = Helper::removeSpecial($_POST['kdlokasi']);
	$r_klasifikasi = Helper::removeSpecial($_POST['kdklasifikasi']);
	$r_perolehan = Helper::removeSpecial($_POST['kdperolehan']);
	$r_status = Helper::removeSpecial($_POST['statuseks']);
	$r_tgl1 = Helper::formatDate($_POST['tgl1']);
	$r_tgl2 = Helper::formatDate($_POST['tgl2']);
	
	
	if($r_format=='') {// or $r_kondisi=='' or $r_jenis==''
		header("location: index.php?page=home");
	}
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Rekap Eksemplar';
	
	$p_namafile = 'rekapeks_'.$r_kondisi.'_'.$r_jenis;
	
	switch($r_format) {
		case 'doc' :
			header("Content-Type: application/msword");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.doc"');
			break;
		case 'xls' :
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$p_namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
	
	$sql = "select e.ideksemplar,e.noseri,p.nopanggil, p.judul,l.namalokasi,e.tglperolehan, j.namajenispustaka, k.namaklasifikasi, e.statuseksemplar,
			d.namakondisi, o.namaperolehan  
		from pp_eksemplar e
		join ms_pustaka p on e.idpustaka = p.idpustaka
		join lv_jenispustaka j on j.kdjenispustaka = p.kdjenispustaka
		join lv_klasifikasi k on k.kdklasifikasi = e.kdklasifikasi
		join lv_kondisi d on d.kdkondisi = e.kdkondisi
		join lv_perolehan o on o.kdperolehan = e.kdperolehan 
		left join lv_lokasi l on e.kdlokasi = l.kdlokasi
		where to_date(to_char(e.tglperolehan,'yyyy-mm-dd'),'yyyy-mm-dd') between to_date('$r_tgl1','yyyy-mm-dd') and to_date('$r_tgl2','yyyy-mm-dd') $sqlAdminUnit ";
	if ($r_kondisi!='') {
		$sql .=" and e.kdkondisi='$r_kondisi' ";
	}
	
	if ($r_jenis!=''){
		$sql .=" and p.kdjenispustaka = '$r_jenis'";
	}
	if ($r_lokasi!=''){
		$sql .=" and e.kdlokasi = '$r_lokasi'";
	}
	if ($r_klasifikasi!=''){
		$sql .=" and e.kdklasifikasi = '$r_klasifikasi'";
	}
	if ($r_perolehan!=''){
		$sql .=" and e.kdperolehan = '$r_perolehan'";
	}
	if ($r_status == 'null'){
		$sql .=" and e.statuseksemplar is null ";
	}else if (!empty($r_status)){
		$sql .=" and e.statuseksemplar = '$r_status'";
	}
	
	$sql .=" order by e.noseri ";
	$row = $conn->Execute($sql);
	$rsc=$row->RowCount();
	$rs = $conn->GetRow("select namakondisi from lv_kondisi where kdkondisi='$r_kondisi'");
	$rsj = $conn->getRow("select namajenispustaka from lv_jenispustaka where kdjenispustaka='$r_jenis'");

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
<style>
	body,td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
	
	}
	table{
	  border-collapse : collapse;
	  border			: 1px thin black;
	}

	th{
	  background:#CCCCCC;
	  font-size: 8pt;
	  }

</style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">

<div align="center">
<table width=675>
	<tr>
		<td width=60><img src="<?= $dirIcon.'logo.png' ?>" width=100 height=50></td>
		<td valign="bottom"><h3>PERPUSTAKAAN<br>PJB</h3></td>
	</tr>
</table>
<table width=675 cellpadding="2" cellspacing="0" border=0>
  <tr>
  	<td align="center"><strong>
  	<h2>Rekap Eksemplar</h2>
  	</strong></td>
  </tr>
	<tr>
	<td><b>Kondisi : <?= $r_kondisi=='semua' ? "Semua Kondisi" : $rs['namakondisi'] ?></b></td>
	</tr>
	<tr>
	<td><b>Jenis Pustaka : <?= $r_jenis=='semua' ? "Semua Jenis" : $rsj['namajenispustaka'] ?></b></td>
	</tr>
	<tr>
	<td><b>Periode : <?= Helper::formatDateInd($r_tgl1)." - ".Helper::formatDateInd($r_tgl2) ?></b></td>
	</tr>
</table>
<table width="675" border="1" cellpadding="2" cellspacing="0">

  <tr height=25>
	<th width="10" align="center"><strong>No.</strong></th>
	<th width="10" align="center"><strong>No. Induk</strong></th>
    <th width="150" align="center"><strong>Judul</strong></th>
    <th width="150" align="center"><strong>No.Panggil</strong></th>
    <th width="150" align="center"><strong>Tgl. Perolehan</strong></th>
    <th width="100" align="center"><strong>Lokasi</strong></th>
    <th width="150" align="center"><strong>Jenis Pustaka</strong></th>
    <th width="150" align="center"><strong>Kondisi</strong></th>
    <th width="150" align="center"><strong>Asal Perolehan</strong></th>
    <th width="100" align="center"><strong>Status</strong></th>
	
  </tr>
  <?php
	$no=0;
	while($rs=$row->FetchRow()) 
	{  $no ++;?>
    <tr height=25>
		<td align="center"><?= $no ?></td>
		<td align="left"><?= $rs['noseri'] ?></td>
		<td align="left"><?= $rs['judul'] ?></td>
		<td align="left"><?= $rs['nopanggil'] ?></td>
		<td align="left"><?= Helper::formatDateInd($rs['tglperolehan']) ?></td>
		<td align="left"><?= $rs['namalokasi'] ?></td>
		<td align="left"><?= $rs['namajenispustaka'] ?></td>
		<td align="left"><?= $rs['namakondisi'] ?></td>
		<td align="left"><?= $rs['namaperolehan'] ?></td>
		<td align="left"><?= ($rs['statuseksemplar'] != "" ? $rs['statuseksemplar'] : "Proses");  ?></td>
	</tr>
	<? } ?>
	<? if($no==0) { ?>
	<tr height=25>
		<td align="center" colspan=10 >Tidak ada pustaka</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan=10><b>Jumlah : <?= $rsc ?></b></td>
	</tr>
   
</table>


</div>
</body>
</html>