<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//reload file
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	$r_key = '169009';
	
	$b_link = new Breadcrumb();
	$b_link->add($menu['BIBLIO'], Helper::navAddress('data_pustaka.php').'&key='.$r_key, 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$menu['BIBLIO'];
	$p_dbtable = "ms_pustaka";
	
	$sql = "select p.*, j.namajenispustaka,j.islokal, b.namabahasa
			from $p_dbtable p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$r_key'";
	$row = $conn->GetRow($sql);
	
	//setting untuk halaman
	$p_title = $form['DET_SEE'].' '.ucwords(strtolower($row['namajenispustaka']));
	
	// //untuk jurusan
	// $sqljur = "select * from pp_bidangjur j 
				// left join lv_jurusan lj on lj.kdjurusan=j.kdjurusan
				// where idpustaka='$r_key'";
	// $rsjur = $conn->Execute($sqljur);
	
	
	// while ($rowjur = $rsjur->FetchRow()){
			// $jurusan[$rowjur['idpustaka']][] = $rowjur['namajurusan'];
		// }
	
	//untuk pengarang
	// $sql = "select * from pp_author a 
			// left join ms_author m on a.idauthor=m.idauthor
			// where a.idpustaka='$r_key'";
	// $rsauth = $conn->Execute($sql);
	
	// while ($rowauth = $rsauth->FetchRow()){
			// $author[$rowauth['idpustaka']][] = $rowauth['namadepan'].' '.$rowauth['namabelakang'];
		// }
	
	//untuk eksemplar
	$sql = "select * from pp_eksemplar e 
			left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			left join ms_rak r on r.kdrak=e.kdrak
			left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
			where idpustaka='$r_key' and kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%') ";
	$rseks = $conn->Execute($sql);
	
	while ($roweks = $rseks->FetchRow()){
			$ideksemplar[$roweks['idpustaka']][] = $roweks['ideksemplar'];
			$seri[$roweks['idpustaka']][] = $roweks['noseri'];
			$eksrak[$roweks['idpustaka']][] = $roweks['namarak'];
			$ekslokasi[$roweks['idpustaka']][] = $roweks['namalokasi'];
			$eksstatus[$roweks['idpustaka']][] = $roweks['statuseksemplar'];
			$label[$roweks['idpustaka']][] = $roweks['namaklasifikasi'];
			$lbl[$roweks['idpustaka']][] = $roweks['kdklasifikasi'];
	}
	
	//untuk topik
	$sql = "select namatopik from lv_topik
			where kode = '".$row['kodeddc']."'";
	$rowtopik = $conn->GetOne($sql);

	$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
	$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
								
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
</head>
<body>
	<?= $b_link->output(); ?>
	<? if (isset($_SESSION['userid'])){ ?>
	<div class="notification">
		<?= Perpus::getCart(); ?>
	</div>
	<? } ?>
	<div class="bookDetail">
		<div class="contentTop">&nbsp;</div>
		<div class="contentContainer" style="padding-bottom:10px;">
			<table width="98%" cellpadding="1" cellspacing="0">
				<tr>
					<td colspan="2"><span class="menuTitle"><?= $p_title; ?></span></td>
				</tr>
				<tr height="20"><td>&nbsp;</td></tr>
				<tr height="20">
					<td class="judul"><?= ucwords(strtolower($row['judul'])) ?></td>
                    <td align="right"></td>
				</tr>
			</table><br />
			<table width="98%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" rowspan="7" width="188" align="center" style="padding-left:12px;padding-right:12px;">
						<img id="imgfoto" src="<?= (is_file($p_foto) ? $p_hfoto : Config::fotoUrl.'none.png') ?>" width="110" height="108">
					</td>
					<? if($row['islokal']==1) { ?>
					<td valign="top" class="resultSub" width="100">NO INDUK</td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['noseri']; ?></td>
					<? } else {?>
					<td valign="top" class="resultSub" width="100"><?= $form['NO_CALL']?></td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['nopanggil']; ?></td>
					<? } ?>
				</tr>
				<? if($row['islokal']!=1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['EDITION']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['edisi']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['AUTHOR']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?
								if($row['islokal']==0){
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}else{
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."','1')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."','1')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."','1')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}
								//if($
								// if (count($author[$row['idpustaka']]) > 0){
									// for($j=0;$j<count($author[$row['idpustaka']]);$j++)
										// echo $author[$row['idpustaka']][$j].'<br>';
								// } 
						?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;">Contributor</td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['pembimbing']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['PUBLISHER']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['kota'].': '.$row['namapenerbit'].', '.$row['tahunterbit']; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['DIMENSION']; ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= /*"Hal. Romawi : ".($row['jmlhalromawi']=='' ? '-' : $row['jmlhalromawi']) .", Halaman : ". ($row['jmlhalaman']=='' ? '-' : $row['jmlhalaman']) .", Panjang : ".($row['dimensipustaka']=='' ? '-' : */$row['dimensipustaka'] ; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['ISBN']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['isbn']; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['TOPIC']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $rowtopik;	
							// if (count($topik[$row['idpustaka']]) > 0){
								// for($j=0;$j<count($topik[$row['idpustaka']]);$j++)
									// echo $topik[$row['idpustaka']][$j].'<br>';
							// } 
						?>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<br />
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr height="30">
								<td><strong><?= $form['SYNOPSIS']?></strong></td>
							</tr>
							<tr>
								<td><p><?= empty($row['sinopsis']) ? $form['P_EMPTYSYNOPSIS'] : $row['sinopsis'];?></p></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="resultSub" style="padding-top:5px;">
						<br /><br /><b><?= $form['COPIES']?></b> <br />
						<table border="0" cellpadding="4" cellspacing="0" class="containerTable" width="100%">
							<tr>
								<th width="150" align="left" class="eksHead" style="padding-top:3px;">No. Induk</th>
								<th width="400" align="left" class="eksHead"><?= $form['LOCATION']?></th>
								<th width="400" align="left" class="eksHead"><?= $form['CLASSIFICATION']?></th>
								<th width="120" align="left" class="eksHead"><?= $form['STATE']?></th>
							</tr>			
							<? for($j=0;$j<count($eksrak[$row['idpustaka']]);$j++) { 
							if ($lbl[$row['idpustaka']][$j]=='LM')
								$warna = 'red';
							elseif ($lbl[$row['idpustaka']][$j]=='LH')
								$warna = 'green';
							else
								$warna = 'black';
							?>
							<tr >
								<td style="color:<?= $warna ?>" align="left" class="resultSub"><a href="javascript:showEks('<?= $ideksemplar[$row['idpustaka']][$j];?>','<?= $eksstatus[$row['idpustaka']][$j] ?>')" title="Download File"><?= $seri[$row['idpustaka']][$j];?></a></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $ekslokasi[$row['idpustaka']][$j];?></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $label[$row['idpustaka']][$j];?></td>
								<? $eksstatus[$row['idpustaka']][$j]=='ADA' ? $class = 'resultAvailable' : $class = 'resultUnavailable' ?>
								<td style="color:<?= $warna ?>" class="<?= $class; ?>"><?= $eksstatus[$row['idpustaka']][$j]=='ADA' ? $form['P_AVAILABLE']: ($eksstatus[$row['idpustaka']][$j]=='PJM' ? $form['P_LOANS'] : $form['P_RESERVED']);?></td>
							</tr>
						<? } ?>
						<? 	if (count($eksrak[$row['idpustaka']]) == 0){ ?>
						<tr>
							<td colspan=4 align="center"><b>Eksemplar untuk pustaka tersebut sedang kosong</b></td>
						</tr>
						<? } ?>
						</table>
						
					</td>
				</tr>
			</table>
		</div>
	</div>
    </div><?php /*?>
	<div class="bookDetail">
	<div class="contentTop">&nbsp;</div>
	<div class="contentContainer" style="padding-bottom:10px;">
    	<span class="menuTitle">Pustaka Terkait</span>
        <table width="98%" cellpadding="4" cellspacing="0">
        	<tr>
            	<td></td>
            </tr>
        </table>
    </div>
    </div><?php */?>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	<?php /*?>function addCart(){
		sent = "&act=add&key=<?= $r_key?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}<?php */?>
	
	function showCart(){
		sent = "";
		goLink('contents','<?= Helper::navAddress('data_cart.php')?>', sent);
	}
	
	function showEks(key, status){
		sent = "&key=" + key + "&status=" + status;
		goLink('contents','<?= Helper::navAddress('data_eksemplar.php')?>', sent);
	}
	
	function goAuthor(key,par){
		sent = "&par="+par+"&key="+ key;
		goLink('contents','<?= Helper::navAddress('xlisthasilauthor.php')?>', sent);
	}
</script>
</html>
