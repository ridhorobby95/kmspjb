<?php
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');
	require_once('../classes/mail.class.php');
	
	# H-3 
	$sqlstr = "select t.idtransaksi  
		from pp_transaksi t
		join ms_anggota a on t.idanggota=a.idanggota
		where (to_date(TO_CHAR(ADD_MONTHS(t.tgltenggat,1),'dd-mm-yyyy'),'dd-mm-yyyy') - to_date(TO_CHAR(current_date,'dd-mm-yyyy'),'dd-mm-yyyy')  ) = 3 
		and kdjenistransaksi='PJN' 
		and tglpengembalian is null 
		and (a.email is not null or a.email <> '') ";

	$rs = $conn->Execute($sqlstr);
		
	while ($row = $rs->FetchRow()){
		if($row['email']){
			Mail::alertTenggat($conn, $row['idtransaksi']);
		}
	}

?>
