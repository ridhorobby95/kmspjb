<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	define( '__PERPUS_ROW_LIMIT', 10 );
	
	//error_reporting(0);
	
	if(!Helper::xIsLoggedIn($conn)) {
		exit();
	}

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuthAjax($conn);
	if(!$a_auth)
		exit();

	if ($_GET['act']) {
		$act = Helper::removeSpecial($_GET['act']);
		
		switch ($act) {
			case 'aktivitas':
			case 'ttbinventaris':
			case 'bahasa':
			case 'pengarang':
			case 'penerbit':
			case 'supplier':
			case 'eksemplarolah':
			case 'petugas':
			case 'getUsulanBaru':
			case 'getPesanDigilib':
			case 'getKontak':
			case 'getReservasiBaru':
			case 'getPanjangBaru';
			case 'usulan':
				$act($act);
				break;
			case 'usulanunit':
				$act($act);
				break;
			case 'order':
				$act($act);
				break;
			case 'newpengaranglov':
				_newPengarangLOV();
				break;
			case 'newsupplierlov':
				_newSupplierLOV();
				break;
			case 'newpenerbitlov':
				_newPenerbitLOV();
				break;
			default:
				if (preg_match('/prev_/', $act) || preg_match('/next_/', $act) || preg_match('/reset_/', $act)) {
					_getNavigation($act);
				}
				else
					echo "Invalid Request";
		}
	}
	exit;

	function _getNavigation($act) {
		$prefix = explode('_', $act);
		$navigation = $prefix[0];
		$fungsi = $prefix[1];
		if ($prefix[0] === 'prev') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] - 10;
	
			if ($_SESSION["perpus_{$fungsi}_offset"] < 0)
				$_SESSION["perpus_{$fungsi}_offset"] = 0;			
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'next') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] + 10;
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'reset') {
			unset($_SESSION["perpus_{$fungsi}_offset"]);
			unset($_SESSION["perpus_{$fungsi}_keyword"]);
	
			return $fungsi($fungsi);
		}
	}

	function _retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}
	
	function bahasa($act){
		$fungsi = $act;
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			    $_SESSION["perpus_{$fungsi}_offset"] = 0;
			    $_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from lv_bahasa ";
				
		if ($keyword != '') {
			$sql .= " where lower(kdbahasa) like '%{$keyword}%' or lower(namabahasa) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$end = $offset + __PERPUS_ROW_LIMIT;

		$sql = "SELECT * FROM ( ";
		$sql .= " select kdbahasa, namabahasa, rownum as rnum from lv_bahasa";
		if ($keyword != '') {
			$sql .= " where lower(kdbahasa) like '%{$keyword}%' or lower(namabahasa) like '%{$keyword}%' ";
		}
		$sql .=  " order by namabahasa ";
		#update ini
		$sql .=  ") WHERE rnum > $offset and rnum <= " . $end;

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>BAHASA</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$no = $offset+$i;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td width=\"10%\">{$no}</td>";
					$ret .= "<td>{$row['namabahasa']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['kdbahasa']}','{$row['namabahasa']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function pengarang($act){
		$fungsi = $act;
		
	        $conn = Factory::getConn();
	        $entry = $_POST['entry'];
	        if ($entry === '1') {
			    $_SESSION["perpus_{$fungsi}_offset"] = 0;
			    $_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}

		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from ms_author ";
				
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$end = $offset + __PERPUS_ROW_LIMIT;

		$sql = "SELECT * FROM ( ";
		$sql .= "select idauthor, namadepan, namabelakang, rownum as rnum from ms_author";
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$sql .=  " order by case when upper(namadepan) = upper('".$keyword."') then 0 else 1 end, namadepan,namabelakang ";
		#update ini
		$sql .=  ") WHERE rnum > $offset and rnum <= " . $end;

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>PENGARANG</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$no = $offset+$i;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td width=\"10%\">{$no}</td>";
					$ret .= "<td>{$row['namadepan']}&nbsp;{$row['namabelakang']}</td>";
					$ret .= "<td width=\"10%\"><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idauthor']}','".$row['namadepan'].' '.$row['namabelakang']."');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$extra_function = array('nama'=>'newPengarang', 'label'=>'Buat baru');
		
		$ret = _headerLOV($act, '', $extra_function) . $ret;
		
		echo $ret;
	}
	
	function penerbit($act){
		$fungsi = $act;
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from ms_penerbit ";
				
		if ($keyword != '') {
			$sql .= " where lower(namapenerbit) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-__PERPUS_ROW_LIMIT)/__PERPUS_ROW_LIMIT) * __PERPUS_ROW_LIMIT;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$end = $offset + __PERPUS_ROW_LIMIT;

		$sql = "SELECT * FROM ( ";
		$sql .= "select idpenerbit, namapenerbit, rownum as rnum from ms_penerbit";
		if ($keyword != '') {
			$sql .= " where lower(namapenerbit) like '%{$keyword}%' ";
			$sql .=  " order by case when upper(namapenerbit) = upper('".$keyword."') then 0 else 1 end, namapenerbit ";
		}else{
			$sql .=  " order by namapenerbit ";
		}
		#update ini
		$sql .=  ") WHERE rnum > $offset and rnum <= " . $end;

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>PENERBIT</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$no = $offset+$i;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td width=\"10%\">{$no}</td>";
					$ret .= "<td>{$row['namapenerbit']}</td>";
					$ret .= "<td width=\"10%\"><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idpenerbit']}','".$row['namapenerbit']."');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$extra_function = array('nama'=>'newPenerbit', 'label'=>'Buat baru');
		
		$ret = _headerLOV($act, '', $extra_function) . $ret;
		
		echo $ret;
	}
	
	function supplier($act){
		$fungsi = $act;
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		list($id,$num) = explode('|',$_POST['add']);
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from ms_supplier ";
				
		if ($keyword != '') {
			$sql .= " where lower(namasupplier) like '%{$keyword}%' or lower(alamat) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select * from ms_supplier";
		if ($keyword != '') {
			$sql .= " where lower(namasupplier) like '%{$keyword}%' or lower(alamat) like '%{$keyword}%' ";
		}
		$sql .=  " order by namasupplier ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"50\">KODE</th><th>NAMA SUPPLIER</th><th>ALAMAT</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['kdsupplier']}</td>";
					$ret .= "<td>{$row['namasupplier']}</td>";
					$ret .= "<td>{$row['alamat']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['kdsupplier']}','{$row['namasupplier']}','{$id}','{$num}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$extra_function = array('nama'=>"newSupplier('$id','$num')", 'label'=>'Buat baru');
		
		$ret = _headerLOV($act, $_POST['add'] ,$extra_function) . $ret;
		
		echo $ret;
	}
	
   	function usulan($act){
		$fungsi = $act;
		
		// require tambahan
		$isAdminPusat = Helper::isAdminPusat();
		$units = Helper::getUnits();
		if(!$isAdminPusat)	
			$sqlAdminUnit = " and a.idunit in ($units) ";
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*)
		from pp_orderpustaka o
		left join pp_usul u on u.idusulan=o.idusulan
		left join ms_anggota a on a.idanggota = u.idanggota 
		where stsusulan='1' and idpengadaan is null and u.idunit is null $sqlAdminUnit";
				
		if ($keyword != '') {
			$sql .= " and lower(to_char(o.judul)) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select *
			from pp_orderpustaka o
			left join pp_usul u on u.idusulan=o.idusulan
			left join ms_anggota a on a.idanggota = u.idanggota 
			where stsusulan='1' and idpengadaan is null and u.idunit is null $sqlAdminUnit";
		if ($keyword != '') {
			$sql .= " and lower(to_char(o.judul)) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$sql .=  " order by tglusulan ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th colspan=\"8\">Daftar Pengusulan Pustaka</th>";
		$ret .=  "<tr><th width=\"80\">ID USULAN</th><th>TGL.USULAN</th><th>JUDUL</th><th>PENGARANG</th><th>PENGUSUL</th><th>HARGA</th><th>QTY</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idusulan']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglusulan'])."</td>";
					$ret .= "<td>".$row['judul']."</td>";
					$ret .= "<td>".$row['authorfirst1']." ".$row['authorlast1']."</td>";
					$ret .= "<td>".$row['namapengusul']."</td>";
					$ret .= "<td>{$row['hargausulan']}</td>";
					$ret .= "<td>{$row['qtyusulan']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idusulan']}','".Helper::formatDate($row['tglusulan'])."','".Helper::removeSpecial($row['judul'],true)."','".Helper::removeSpecial($row['authorfirst1'],true)." ".Helper::removeSpecial($row['authorlast1'],true)."','".Helper::removeSpecial($row['namapengusul'],true)."','{$row['hargausulan']}','{$row['qtyusulan']}','{$row['idorderpustaka']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"8\" align=\"center\">Daftar Usulan Kosong</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function usulanunit($act){
		$fungsi = $act;
		
		// require tambahan
		$isAdminPusat = Helper::isAdminPusat();
		$units = Helper::getUnits();
		if(!$isAdminPusat)	
			$sqlAdminUnit = " and u.idunit in ($units) ";
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*)
		from pp_orderpustaka o
		left join pp_usul u on u.idusulan=o.idusulan 
		where stsusulan='1' and idpengadaan is null and u.idunit is not null $sqlAdminUnit";
				
		if ($keyword != '') {
			$sql .= " and lower(to_char(o.judul)) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select *
			from pp_orderpustaka o
			left join pp_usul u on u.idusulan=o.idusulan 
			where stsusulan='1' and idpengadaan is null and u.idunit is not null $sqlAdminUnit";
		if ($keyword != '') {
			$sql .= " and lower(to_char(o.judul)) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$sql .=  " order by tglusulan ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th colspan=\"8\">Daftar Pengusulan Pustaka</th>";
		$ret .=  "<tr><th width=\"80\">ID USULAN</th><th>TGL.USULAN</th><th>JUDUL</th><th>PENGARANG</th><th>PENGUSUL</th><th>HARGA</th><th>QTY</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idusulan']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglusulan'])."</td>";
					$ret .= "<td>".$row['judul']."</td>";
					$ret .= "<td>".$row['authorfirst1']." ".$row['authorlast1']."</td>";
					$ret .= "<td>".$row['namapengusul']."</td>";
					$ret .= "<td>{$row['hargausulan']}</td>";
					$ret .= "<td>{$row['qtyusulan']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idusulan']}','".Helper::formatDate($row['tglusulan'])."','".Helper::removeSpecial($row['judul'],true)."','".Helper::removeSpecial($row['authorfirst1'],true)." ".Helper::removeSpecial($row['authorlast1'],true)."','".Helper::removeSpecial($row['namapengusul'],true)."','{$row['hargausulan']}','{$row['qtyusulan']}','{$row['idorderpustaka']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"8\" align=\"center\">Daftar Usulan Kosong</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function order($act){
		$fungsi = $act;
		
		// require tambahan
		$isAdminPusat = Helper::isAdminPusat();
		$units = Helper::getUnits();
		if(!$isAdminPusat)	
			$sqlAdminUnit = " and (u.idunit in ($units) or a.idunit in ($units)) ";
		
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql="select count(*) from pp_orderpustaka o
		join pp_usul u on o.idusulan=u.idusulan
		left join ms_anggota a on a.idanggota = u.idanggota 
		where o.ststtb=0 and o.stspengadaan=1 $sqlAdminUnit ";
		
		if ($keyword != '') {
			$sql .= " and lower(to_char(o.judul)) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select o.idorderpustaka, to_char(o.judul) judul, o.qtypengadaan, o.hargadipilih, u.tglusulan, u.namapengusul
			from pp_orderpustaka o
			join pp_usul u on o.idusulan=u.idusulan
			left join ms_anggota a on a.idanggota = u.idanggota 
			where o.ststtb=0 and o.stspengadaan=1 $sqlAdminUnit";
		if ($keyword != '') {
			$sql .= " and lower(o.judul) like '%{$keyword}%' or lower(u.namapengusul) like '%{$keyword}%' ";
		}
		$sql .=  " group by o.idorderpustaka, to_char(o.judul), o.qtypengadaan, u.tglusulan, u.namapengusul, o.hargadipilih ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">ID ORDER</th><th>TGL.USULAN</th><th>JUDUL</th><th>PENGUSUL</th><th>QTY</th><th>HARGA</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td align='center'>{$row['idorderpustaka']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglusulan'])."</td>";
					$ret .= "<td>{$row['judul']}</td>";
					$ret .= "<td>{$row['namapengusul']}</td>";
					$ret .= "<td>{$row['qtypengadaan']}</td>";
					$ret .= "<td>{$row['hargadipilih']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idorderpustaka']}','{$row['nopo']}','".Helper::formatDate($row['tglusulan'])."','".nl2br(str_replace(PHP_EOL," ",$row['judul']))."','".trim(Helper::removeSpecial($row['namapengusul'],true))."','".Helper::removeSpecial($row['namasupplier'])."','{$row['qtypengadaan']}','{$row['hargadipilih']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"7\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function eksemplarolah($act){
		$fungsi = $act;
		$conn = Factory::getConn();
		
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		$arrkey = $_POST['add']; //noseri#klasifikasi
		//pisahkan dulu
		$arrkey_pisah = explode('#',$arrkey);

		$arrplo = explode(',',$arrkey_pisah[0]);
		
		$arrkey = implode("','",$arrplo);
		
		if($arrkey_pisah[1]!='' or !empty($arrkey_pisah[1])) //cek apakah kdklasifikasi kosoang?jika kosong itu tandanya inputan pertama.
			$klasifikasi = " and e.kdklasifikasi='".$arrkey_pisah[1]."'";
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from pp_eksemplarolah p 
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
				left join ms_pustaka m on m.idpustaka=e.idpustaka 
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
				where stskirimpengolahan=1 and (stskirimeksternal=0 and iskirimeksternal=0)
				and m.kdjenispustaka in ($_SESSION[roleakses]) and p.stspengolahan=1 {$klasifikasi}";
				
		if ($keyword != '') {
			$sql .= " and (lower(to_char(m.judul)) like '%{$keyword}%' or lower(e.noseri) like '%{$keyword}%') ";
		}
		
		if ($arrkey != '')
			$sql .= " and e.noseri not in ('$arrkey')";
		
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select p.*, e.noseri as seri, m.judul, m.authorfirst1, m.authorlast1, m.authorfirst2, m.authorlast2, m.authorfirst3, m.authorlast3
			from pp_eksemplarolah p 
				left join pp_eksemplar e on e.ideksemplar=p.ideksemplar 
				left join ms_pustaka m on m.idpustaka=e.idpustaka 
				left join lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
				where stskirimpengolahan=1 and (stskirimeksternal=0 and iskirimeksternal=0)
				and m.kdjenispustaka in ($_SESSION[roleakses]) and p.stspengolahan=1 {$klasifikasi} ";
		if ($keyword != '') {
			$sql .= " and (lower(to_char(m.judul)) like '%{$keyword}%' or lower(e.noseri) like '%{$keyword}%') ";
		}
				
		if ($arrkey != '')
			$sql .= " and e.noseri not in ('$arrkey')";
		
		$sql .=  " order by tglkirimpengolahan desc ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">NO INDUK</th><th>JUDUL</th><th>PENGARANG</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td align='center'>{$row['seri']}</td>";
					$ret .= "<td>{$row['judul']}</td>";
					$ret .= "<td>{$row['authorfirst1']}{$row['authorlast1']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" seri=\"{$row['seri']}\" judul=\"{$row['judul']}\" onclick=\"{$ret_function}(this);closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"4\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function aktivitas($act){
		$fungsi = $act;
		$connf = Factory::getConnFin();
		
		$nowday = date('Ym');
		$r_thang = $connf->GetOne("select thang from ms_thnang where periodeawal <= '$nowday' and periodeakhir >= '$nowday'");
			
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from ke_anggarandetail s
				right join ke_subanggaran g on g.kodeunit = s.kodeunit and g.thang = s.thang and 
				g.kodeaktivitas = s.kodeaktivitas and g.nosubanggaran = s.nosubanggaran
				right join ke_anggaran a on a.kodeunit = s.kodeunit and a.thang = s.thang and a.kodeaktivitas = s.kodeaktivitas
				where s.thang = '$r_thang' and s.statusanggaran = 'S'";

		if ($keyword != '') {
			$sql .= " and lower(a.kodeaktivitas) like '%{$keyword}%' or lower(a.namaaktivitas) like '%{$keyword}%' ";
		}
		$tot = $connf->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select a.kodeaktivitas, g.nosubanggaran, s.nosubaktivitas, a.namaaktivitas, 
				g.namasubanggaran, s.namasubaktivitas, s.kodeanggaran
				from ke_anggarandetail s
				right join ke_subanggaran g on g.kodeunit = s.kodeunit and g.thang = s.thang and 
				g.kodeaktivitas = s.kodeaktivitas and g.nosubanggaran = s.nosubanggaran
				right join ke_anggaran a on a.kodeunit = s.kodeunit and a.thang = s.thang and a.kodeaktivitas = s.kodeaktivitas
				where s.thang = '$r_thang' and s.statusanggaran = 'S'";
			
		if ($keyword != '') {
			$sql .= " and lower(a.kodeaktivitas) like '%{$keyword}%' or lower(a.namaaktivitas) like '%{$keyword}%'";
		}
		$sql .=  " order by a.kodeaktivitas, s.nosubaktivitas";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th>AKTIVITAS</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $connf->Execute($sql);
		while($rows = $rs->FetchRow()) {
			if($t_kodeaktivitas != $rows['kodeaktivitas']) {
				$a_temp['kode'] = $rows['kodeaktivitas'];
				$a_temp['nama'] = $rows['namaaktivitas'];
				$a_temp['level'] = 1;
		
				if($rows['nosubanggaran'] != '')
					$a_temp['haschild'] = 1;
				else
					$a_temp['haschild'] = 0;
		
				$a_sub[] = $a_temp;
		
				$t_kodeaktivitas = $rows['kodeaktivitas'];
			}
		
			if($t_nosubanggaran != $rows['nosubanggaran']) {
				$a_temp['kode'] = $rows['nosubanggaran'];
				$a_temp['nama'] = $rows['namasubanggaran'];
				$a_temp['level'] = 2;
		
				if($rows['nosubaktivitas'] != '')
					$a_temp['haschild'] = 1;
				else
					$a_temp['haschild'] = 0;
		
				$a_sub[] = $a_temp;
		
				$t_nosubanggaran = $rows['nosubanggaran'];
			}
			if($rows['nosubaktivitas'] != '') {
				$a_temp['kode'] = $rows['kodeaktivitas'].'.'.$rows['nosubaktivitas'];
				$a_temp['nama'] = $rows['kodeanggaran'].' - '.$rows['namasubaktivitas'];
				$a_temp['haschild'] = 0;
				$a_temp['level'] = 3;
		
				$a_sub[] = $a_temp;
			}
		}	
		
		$i = 0;
		foreach($a_sub as $k=>$v){
			$i++;
			$class = ($i%2===1)?"class=\"odd\"":"";
			$ret .= "<tr {$class}>";
			if ($ret_function) {
				$ret .= "<td>".str_repeat('..',$v['level']-1).$v['nama']."</td>";
				$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$v['kode']}','{$r_thang}');closePop();resetLOV('$act');\"></td>";
			}
			$ret .= "</tr>";
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}
	
	function ttbinventaris($act){
		$fungsi = $act;
			
		$conn = Factory::getConn();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from pp_orderpustakattb o left join pp_ttb t on t.idttb=o.idttb 
				where stsinventaris=0 and stspengolahan <> 1";
				
		if ($keyword != '') {
			$sql .= " and lower(nottb) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select * from pp_orderpustakattb o left join pp_ttb t on t.idttb=o.idttb 
				where stsinventaris=0 and stspengolahan <> 1";
				
		if ($keyword != '') {
			$sql .= " and lower(nottb) like '%{$keyword}%' ";
		}
		$sql .=  " order by tglttb ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">No TTP</th><th>TGL.TTP</th><th>JUMLAH</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['nottb']}</td>";
					$ret .= "<td>".Helper::formatDate($row['tglttb'])."</td>";
					$ret .= "<td>{$row['qtyttbdetail']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['nottb']}','{$row['idorderpustaka']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"4\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
		echo $ret;
	}

	function _headerLOV($act, $add, $extra_function=null) {
		$keyword = $_SESSION["perpus_{$act}_keyword"];

		$ret = "<table class=\"popWindowtr\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".
		$ret .= "<form style=\"float:left\">&nbsp;<input type=\"text\" id=\"sevimalov_keyword\" name=\"sevimalov_keyword\" value=\"$keyword\" size=\"25\" />
			<input type=\"submit\" style=\"cursor:pointer\" onclick=\"searchLOV('$act','$add');return false;\" value=\"Cari\" />
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('reset_'+'$act','$add')\" title=\"Reset\" value=\"Reset\"> &nbsp; 
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('prev_'+'$act','$add')\" title=\"Previous\" class=\"btn_prev\">
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('next_'+'$act','$add')\" title=\"Next\" class=\"btn_next\">";
		if ($extra_function) {
			$ret .= " &nbsp; <input type=\"button\" id=\"btn_{$extra_function['nama']}\" onclick=\"{$extra_function['nama']}()\" class=\"ControlStyle\" title=\"{$extra_function['label']}\" value=\"{$extra_function['label']}\">";
		}
		$ret .=	"</form>";
		$ret .= "<span style=\"float:right\">".
				"<input type=\"button\" class=\"btn_close\" style=\"cursor:pointer\" onclick=\"closePop()\"/>".
				"</span>";
		$ret .= "</td></tr></table>";
		return $ret;
	}

	function _newPengarangLOV() {
		global $conn;

		$record = array();
		$record['namadepan'] = Helper::removeSpecial($_POST['namadepan']);
		$record['namabelakang'] = Helper::removeSpecial($_POST['namabelakang']);
		$record['isbadan'] = ($_POST['isbadan']?1:0);
		Helper::Identitas($record);
		
		$col = $conn->Execute("select * from ms_author where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record); 
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
	function _newPenerbitLOV() {
		global $conn;
		
		$record = array();
		$record['namapenerbit'] = Helper::removeSpecial($_POST['namapenerbit']);
		$record['alamat'] = Helper::removeSpecial($_POST['alamat']);
		$record['kota'] = Helper::removeSpecial($_POST['kota']);
		$record['kodepos'] = Helper::removeSpecial($_POST['kodepos']);
		$record['telp'] = Helper::removeSpecial($_POST['telp']);
		$record['fax'] = Helper::removeSpecial($_POST['fax']);
		$record['email'] = Helper::removeSpecial($_POST['email']);
		Helper::Identitas($record);
		
		$col = $conn->Execute("select * from ms_penerbit where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record); 
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
	function _newSupplierLOV() {
		global $conn;
		
		$record = array();
		$record['kdsupplier'] = $conn->GetOne("select max(kdsupplier) from ms_supplier")+1;
		$record['namasupplier'] = Helper::removeSpecial($_POST['namasupplier']);
		$record['alamat'] = Helper::removeSpecial($_POST['alamat']);
		Helper::Identitas($record);
		
		$col = $conn->Execute("select * from ms_supplier where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
	function petugas($act){
		$fungsi = $act;
		    
		$conn = Factory::getConnSdm();
		$entry = $_POST['entry'];
		if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid gridStyle\" width=\"100%\">";

		$sql = "select count(*) from ms_pegawai ";
				
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namatengah) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}

		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select nik,namadepan,namatengah,namabelakang, f_namalengkap(gelardepan,namadepan,namatengah,namabelakang,gelarbelakang) as namalengkap from ms_pegawai";
		if ($keyword != '') {
			$sql .= " where lower(namadepan) like '%{$keyword}%' or lower(namatengah) like '%{$keyword}%' or lower(namabelakang) like '%{$keyword}%' ";
		}
		$sql .=  " order by namadepan,namatengah,namabelakang ";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid gridStyle\" width=\"100%\">";
		$ret .=  "<tr><th width=\"200\">KODE</th><th>PETUGAS</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['nik']}</td>";
					$ret .= "<td>{$row['namalengkap']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['nik']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		    
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
			
		echo $ret;
	}
	
	#notifikasi
	function getUsulanBaru(){
		$conn = Factory::getConn();
		$countUsulan = $conn->Getone("select count(*) from pp_usul u where u.idunit is null and statususulan = '0' ");
		echo '<span id="notif1" class="SideSubTitle '.($countUsulan>0?" blink":"").'">'.number_format($countUsulan,0,'','').'</span>';
	}
	
	#hubungi kami
	function getPesanDigilib(){
		$conn = Factory::getConn();
		$countDigilib = $conn->Getone("select count(*) from pp_kontakonline p where idparentol is null and isaktif=1 and p.idkontak = 0
					      and (select count(*) from pp_kontakonline where idparentol = p.idkontakol) = 0  ");
		echo '<span id="notif4" class="SideSubTitle '.($countDigilib>0?" blink":"").'">'.number_format($countDigilib,0,'','').'</span>';
	}
	
	#kontak online
	function getKontak(){
		$conn = Factory::getConn();
		$countDigilib = $conn->Getone("select count(*) from pp_kontakonline p where idparentol is null and isaktif=1 and p.idkontak <> 0
					      and (select count(*) from pp_kontakonline where idparentol = p.idkontakol) = 0  ");
		echo '<span id="notif4" class="SideSubTitle '.($countDigilib>0?" blink":"").'">'.number_format($countDigilib,0,'','').'</span>';
	}
	
	function getReservasiBaru(){
		$conn = Factory::getConn();
		$countReservasi = $conn->Getone("select count(*) from pp_reservasi where statusreservasi='1' and tglexpired > current_date  ");
		echo '<span id="notif2" class="SideSubTitle '.($countReservasi>0?" blink":"").'">'.number_format($countReservasi,0,'','').'</span>';
	}
	
	function getPanjangBaru(){
		$conn = Factory::getConn();
		$countPanjang = $conn->Getone("select count(*) from pp_perpanjangan where statusreservasi='1'  ");
		echo '<span id="notif3" class="SideSubTitle '.($countPanjang>0?" blink":"").'">'.number_format($countPanjang,0,'','').'</span>';
	}
	
?>