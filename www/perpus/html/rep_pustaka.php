<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// require tambahan
	$isAdminPusat = Helper::isAdminPusat();
	$units = Helper::getUnits();
	$idunit = $_SESSION['PERPUS_SATKER'];
	$unitlogin = Helper::getNamaUnit();
	if(!$isAdminPusat)	
		$sqlAdminUnit = " and l.idunit in ($units) ";
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Jumlah Pustaka';
	$p_filerep = 'repp_pustaka';
	$p_tbwidth = 100;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	
	//query's	
	$p_pinjam = $conn->Execute("SELECT e.kdklasifikasi, k.namaklasifikasi, COALESCE(count(*), 0) AS jumlahpinjam
		FROM pp_eksemplar e 
		JOIN pp_transaksi t ON e.ideksemplar = t.ideksemplar
		join lv_klasifikasi k on k.kdklasifikasi = e.kdklasifikasi
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
		WHERE t.tglpengembalian IS NULL $sqlAdminUnit
		GROUP BY e.kdklasifikasi, k.namaklasifikasi ");
	while ($rowp=$p_pinjam->FetchRow()){
		$pinjam[$rowp['kdklasifikasi']]	= $rowp['jumlahpinjam'];
	}
	
		
	$p_eks = $conn->Execute("SELECT e.kdklasifikasi, k.namaklasifikasi, count(*) AS jumlaheks
		FROM pp_eksemplar e 
		join MS_PUSTAKA p on p.IDPUSTAKA = e.IDPUSTAKA 
		JOIN lv_klasifikasi k ON e.kdklasifikasi = k.kdklasifikasi
		left join lv_lokasi l on l.kdlokasi = e.kdlokasi
		WHERE 1=1 $sqlAdminUnit
		GROUP BY e.kdklasifikasi, k.namaklasifikasi");
	
	$pin_count=$conn->GetRow("SELECT count(*) jumlah 
			FROM pp_eksemplar e
			left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			JOIN pp_transaksi t ON e.ideksemplar = t.ideksemplar
			WHERE t.tglpengembalian IS NULL $sqlAdminUnit ");
	
	$p_total=$conn->GetRow("SELECT count(*) jumlah 
			FROM pp_eksemplar e
			left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
			WHERE 1=1 $sqlAdminUnit ");
	
	$p_kondisi=$conn->Execute("select k.namakondisi, count(*) as jum
				  from lv_kondisi k
				  left join pp_eksemplar e on k.kdkondisi=e.kdkondisi
				  left join lv_lokasi l on l.kdlokasi = e.kdlokasi 
				  join ms_pustaka p on p.idpustaka = e.idpustaka
				  where 1=1 $sqlAdminUnit 
				  group by k.kdkondisi,k.namakondisi
				  order by k.kdkondisi ");	
?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>

</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<?php include ('inc_menu.php'); ?>
<div class="container">
    <div class="SideItem" id="SideItem">
		<div align="center">
		<form name="perpusform" id="perpusform" method="post" action="<?= Helper::navAddress($p_filerep) ?>" target="_blank">
		<header style="width:100%;margin:0 auto;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="images/aktivitas/pustaka.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Laporan Jumlah Bahan Pustaka</h1>
				</div>
			</div>
		</header>
		<div class="table-responsive" style="padding-top:20px;">
			<table width=675 border=1 class="gridStyle">
				<tr height=20 class="gridStyle">
					<th rowspan=2><strong>Klasifikasi Pustaka<strong></th>
					<th colspan=2>Jumlah</th>
					<th rowspan=2>Total</th>
				</tr>
				<tr height=20 class="gridStyle">
					<th>Tersedia</th>
					<th>Terpinjam</th>
				</tr>
				<? while ($rowe=$p_eks->FetchRow()){
				?>
				<tr height=20>
					<td>&nbsp;<?= $rowe['namaklasifikasi'] ?></td>
					<td align="center"><?= (int) $rowe['jumlaheks'] - (int) $pinjam[$rowe['kdklasifikasi']] ?></td>
					<td align="center"><?= $pinjam[$rowe['kdklasifikasi']] ?> </td>
					<td align="center"><?= $rowe['jumlaheks'] ?></td>
				</tr>
					<? } ?>
				<tr>
					<td><strong>&nbsp;Jumlah</strong></td>
					<td align="center"><strong><?= ($p_total['jumlah']-$pin_count['jumlah']) ?></strong> </td>
					<td align="center"><strong><?= $pin_count['jumlah'] ?> </strong></td>
					<td align="center"><strong><?= $p_total['jumlah'] ?></strong> </td>
				</tr>
			</table><br><br><br>
			<table width=400 border=1 class="gridStyle">
				<tr height=35 class="gridStyle">
					<th>Kondisi Pustaka</th>
					<th>Jumlah</th>
				</tr>
				<? while ($rowk=$p_kondisi->FetchRow()){ ?>
				<tr height=20>
					<td>&nbsp;<?= $rowk['namakondisi'] ?> </td>
					<td align="center"><?= $rowk['jum'] ?></td>
				</tr>
				<? } ?>
			</table>
		</div>
		<br>
		<table>
			<tr>
				<td align="center">
					<a href="javascript:goSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
				</td>
			</tr>
		</table>
		</form>
		</div>
	</div>
</div>
</body>

</html>