<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng);
	
	$c_delete = $a_auth['candelete'];
	$c_edit = $a_auth['canedit'];
	$c_readlist = $a_auth['canlist'];
	
	// definisi variabel halaman
	$p_window = '[PJB LIBRARY] Laporan Borang';

	$p_filerep = 'repp_borang';
	$p_filerep2 = 'repp_rekborang';
	$p_filerep3 = 'repp_borangeks';
	$p_tbwidth = 450;
	
	// combo box
	$a_format = array('html' => 'Plain HTML', 'doc' => 'Microsoft Word Document', 'xls' => 'Microsoft Excel Spreadsheet');
	$l_format = UI::createSelect('format',$a_format,'','ControlStyle');
	$a_bidang = array('0' => '-- Semua --','1' => 'MKK', '2' => 'MKDK', '3' => 'MKDU', '4' => 'REFERENSI');
	$l_bidang = UI::createSelect('bidang',$a_bidang,'','ControlStyle');
	
	$rs_cb = $conn->Execute("select kdfakultas ||'-'|| namajurusan, rtrim(kdjurusan) from lv_jurusan where f_jur is not null order by kdfakultas");
	$l_jurusan = $rs_cb->GetMenu2('kdjurusan',$row['kdjurusan'],true,false,0,'id="kdjurusan" class="ControlStyle" style="width:250"');
	$l_jurusan = str_replace('<option></option>','<option value="0">-- Semua --</option>',$l_jurusan);
	
	$rs_cb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by namabahasa");
	$l_bahasa = $rs_cb->GetMenu2('kdbahasa','',true,false,0,'id="kdbahasa" class="ControlStyle" style="width:156"');
	$l_bahasa = str_replace('<option></option>','<option value="">-- Semua --</option>',$l_bahasa);

	$a_jenislap = array('0' => 'Detil Laporan', '1' => 'Rekap Laporan');
	$l_jenislap = UI::createSelect('jenislap',$a_jenislap,'','ControlStyle',true,'id="jenislap" style="width:156" id="jenislap" onchange="goJenisLap()"');
		

?>
<html>
<head>
	<title><?= $p_window ?></title>
	<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	
	<link rel="stylesheet" href="style/pager.css">
	<link rel="stylesheet" href="style/officexp.css">
	<link rel="stylesheet" href="style/button.css">
	<script type="text/javascript" src="scripts/foredit.js"></script>
	<script type="text/javascript" src="scripts/calendar.js"></script>
	<script type="text/javascript" src="scripts/calendar-id.js"></script>
	<script type="text/javascript" src="scripts/calendar-setup.js"></script>
	<link href="style/calendar.css" type="text/css" rel="stylesheet">
</head>
<html>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="document.getElementById('txtanggota').focus()">
<?php include ('inc_menu.php'); ?>
<div align="center">
<form name="perpusform" id="perpusform" method="post" target="_blank">

<table width="<?= $p_tbwidth ?>" cellspacing="0" cellpadding="4" class="GridStyle">
	<tr><td class="SubHeaderBGAlt" colspan=2 align="center">Parameter Laporan Borang</td></tr>
	<tr>
		<td class="LeftColumnBG">Jenis Laporan</td>
		<td class="RightColumnBG"><?= $l_jenislap ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Tampilkan Eksemplar</td>
		<td class="RightColumnBG">
		<input type="checkbox" name="eks" id="eks" value="1" />
		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Jurusan</td>
		<td class="RightColumnBG"><?= $l_jurusan ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Bidang</td>
		<td class="RightColumnBG"><?= $l_bidang ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Bahasa</td>
		<td class="RightColumnBG"><?= $l_bahasa ?></td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Option Periode</td>
		<td class="RightColumnBG">
		<label>
		  <input name="optgl" id="optgl1" type="radio" value="0" checked="checked" />
		  Tanggal Perolehan</label>
		  <label>
		  <input name="optgl" id="optgl2" type="radio" value="1" />
		  Tahun Terbit</label>
		</td>
	</tr>

	<tr> 
		<td class="LeftColumnBG" width="150">Periode / Tahun</td>
		<td class="RightColumnBG"><input type="text" name="tgl1" id="tgl1" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle1" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl1",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle1",
			align          :    "Br",
			singleClick    :    true
		});
		</script>
		s/d &nbsp;
		<input type="text" name="tgl2" id="tgl2" size=10 maxlength=10>
		<img src="images/cal.png" id="tgle2" style="cursor:pointer;" title="Pilih tanggal awal">
		&nbsp;
		<script type="text/javascript">
		Calendar.setup({
			inputField     :    "tgl2",
			ifFormat       :    "%d-%m-%Y",
			button         :    "tgle2",
			align          :    "Br",
			singleClick    :    true
		});
		</script>

		</td>
	</tr>
	<tr>
		<td class="LeftColumnBG">Format</td>
		<td class="RightColumnBG"><?= $l_format; ?></td>
	</tr>
</table>
<br>
<table>
	<tr>
		<td align="center">
			<a href="javascript:goPreSubmit();" class="buttonshort"><span class="list">Tampilkan</span></a>
		</td>
	</tr>
</table>
</form>
</div>
</body>
<script type="text/javascript" src="scripts/jquery.masked.js"></script>
<script language="javascript">
$(function(){
	   $("#tgl1").mask("99-99-9999");
	   $("#tgl2").mask("99-99-9999");
});

function goPreSubmit() {
	if(cfHighlight("tgl1,tgl2")){
		var post = document.getElementById("jenislap").value;
		if(post==0){
			if(document.getElementById('eks').checked==true)
			document.perpusform.action='<?= Helper::navAddress($p_filerep3) ?>';
			else
			document.perpusform.action='<?= Helper::navAddress($p_filerep) ?>';
		}else
			document.perpusform.action='<?= Helper::navAddress($p_filerep2) ?>';
		goSubmit();
	}
}

</script>
</html>