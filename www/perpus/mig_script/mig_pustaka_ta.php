<?php
	$conno = Factory::getConno();
	function convBahasa($str)
	{
		if(strpos('Ind,Ind.,Indonesia,In,Perpustakan',$str) !== false)
			$str = "ID";
		else if(strpos('Eng,Ing.,Indg,Ingg,Ingn',$str) !== false)
			$str = "EN";
		else if(strpos('Bld,Belanda,Bel,Dutch,Deu',$str) !== false)
			$str = "DU";
		else if(strpos('Jer,Ger',$str) !== false)
			$str = "DE";
		else if(strpos('Prc,Fra,Pra',$str) !== false)
			$str = "FR";
		else if(strpos('Drh,Jaw,Jawa',$str) !== false)
			$str = "JW";
		else if(strpos('Drh',$str) !== false)
			$str = "DH";
		else if(strpos('Jep,Jap,Jpg,Jpn',$str) !== false)
			$str = "JP";
		else
			$str = "A";
		
		return $str;
	}
	// $cono->debug = true;
	$sqlq = "select `nomor induk`,`kode`,`jenis buku`,`tanggal entri`,`bahasa`,`no klasifikasi`,`pengarang`,`judul`,
			`tahun terbit`,`penerbit`,`deskripsi fisik`,`catatan umum`,`jenis karya`,`kata kunci`,`badan pemilik`,
			`waktu entri`,`kondisi`, `kode operator`, `waktu entri`
			from ta order by `nomor induk`";
	$rso = $conno->Execute($sqlq);
	
	//mendapatkan idpustaka terakhir untuk selanjutnya di ++
	$maxid = $conn->GetOne("select max(idpustaka) as maxid from ms_pustaka");

	 if(!$rso->EOF)
	 {
		 $conn->StartTrans();
		 $col = $conn->Execute("select * from ms_pustaka where 1=-1");
	 }
	 else
		 exit('data asal kosong');
	
	
	// $conn->debug = false;
	$counter = (int)$maxid+1;
	$ar_counter['KTA'] = 1;
	$ar_counter['KSK'] = 1;
	$ar_counter['KTS'] = 1;
	
	while($rowo = $rso->FetchRow()){
		$record['idpustaka'] = $counter++;
		
		if($rowo['kode'] != '' or $rowo['kode'] != null)
			$record['flag'] = $rowo['kode'];
		if($rowo['bahasa'] != '')
			$record['kdbahasa'] = convBahasa((string)$rowo['bahasa']);
			
		if($rowo['jenis buku']=='TA')
			$record['kdjenispustaka'] = 'KS';
		else if($rowo['jenis buku']=='SKRIPSI')
			$record['kdjenispustaka'] = 'SK';
		else if($rowo['jenis buku']=='THESIS')
			$record['kdjenispustaka'] = 'KT';
		else if($rowo['jenis buku']=='KP')
			$record['kdjenispustaka'] = 'LK';
		else if($rowo['jenis buku']=='DESERTASI')
			$record['kdjenispustaka'] = 'KD';
		else
			$record['kdjenispustaka'] = 'LL';// jenis buku . dimasukkan lain-lain
		//sisa yg jenis buku = . ini blm tau masuk mana
			
		$record['noseri']	= $rowo['kode'];
			
		$record['judul'] = str_replace("'","''",$rowo['judul']);
		$record['nopanggil'] = $rowo['no klasifikasi'];
		$record['tglperolehan'] = $rowo['tanggal entri'];
		
		//pecah nama pengarang
		$ngarang = explode(',',$rowo['pengarang']);
		$record['authorfirst1'] = str_replace("'","''",$ngarang[1]);
		$record['authorlast1'] = str_replace("'","''",$ngarang[0]);
		 
		$a_deskripsi = explode(';',$rowo['deskripsi fisik']);
		if(count($a_deskripsi) > 0)
			$record['jmlhalromawi'] = substr(trim($a_deskripsi[0]),0,20);
		if(count($a_deskripsi) > 1)
			$record['dimensipustaka'] = trim($a_deskripsi[1]);//substr(trim($a_deskripsi[1]),0,30);
		
		$record['keterangan'] = $rowo['jenis karya'].'-'.$rowo['catatan umum']; 
		$record['keywords'] = $rowo['kata kunci'];
		
		$tahun = explode('-',$rowo['tanggal entri']);
		if(strlen($rowo['tahun terbit'])>4)
			$record['tahunterbit'] = $tahun[0];
		else
			$record['tahunterbit'] = $rowo['tahun terbit'];
			
		//kalau ada anomali data
		if($record['tglperolehan'] == '0000-00-00')
			$record['tglperolehan'] = 'null';
		else
			$record['tglperolehan'] = "'".$rowo['tanggal entri']."'";
			
		$ipos = strpos($rowo['penerbit'],':');
		$record['kota'] = substr($rowo['penerbit'],0,$ipos);
		$record['namapenerbit'] = trim(substr($rowo['penerbit'],$ipos+1,255));
		
		$record['t_user'] = $rowo['kode operator'];
		$record['t_updatetime'] = $rowo['waktu entri'];
		
		 $sql = $conn->GetInsertSQL($col,$record);
		 $ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		 if($ok == false)
		 {
			 echo $conn->ErrorMsg().'<br/>';
			 echo $sql.'<br/>';
		 }
	}
	
	$conn->CompleteTrans();

?>