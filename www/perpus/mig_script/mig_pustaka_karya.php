<?php
	$conno = Factory::getConno();
	
	function convBahasa($str)
	{
		if(strpos('Ind,Ind.,Indonesia,In,Perpustakan',$str) !== false)
			$str = "ID";
		else if(strpos('Eng,Ing.,Indg,Ingg,Ingn',$str) !== false)
			$str = "EN";
		else if(strpos('Bld,Belanda,Bel,Dutch,Deu',$str) !== false)
			$str = "DU";
		else if(strpos('Jer,Ger',$str) !== false)
			$str = "DE";
		else if(strpos('Prc,Fra,Pra',$str) !== false)
			$str = "FR";
		else if(strpos('Drh,Jaw,Jawa',$str) !== false)
			$str = "JW";
		else if(strpos('Drh',$str) !== false)
			$str = "DH";
		else if(strpos('Jep,Jap,Jpg,Jpn',$str) !== false)
			$str = "JP";
		else
			$str = "A";
		
		
		return $str;
	}
	
	$sqlq = "SELECT `ID`, `NOMOR INDUK`, `KODE`, `TANGGAL ENTRI`, `BAHASA`, `PRE_KLAS`, `NO KLASIFIKASI`, `PENGARANG`, 
		`PENGARANG TAMBAHAN`, `BADAN KORPORASI`, `BADAN KORPORASI TAMBAHAN`, `JUDUL`, `PENERBIT`, `TAHUN TERBIT`, `DESKRIPSI FISIK`, `JENIS`, 
		`CATATAN UMUM`, `SUBYEK`, `ASAL`, `HARGA`, `ANOTASI`, `KATA KUNCI`, `BADAN PEMILIK`, `KODE OPERATOR`, `STATUS PROSES`, `KONDISI`
		FROM karya";
	$rso = $conno->Execute($sqlq);
		
	if(!$rso->EOF)
	{
		$conn->StartTrans();
		$col = $conn->Execute("select * from ms_pustaka where 1=-1";
	}
	else
		exit('data asal kosong');
	
	
	$conn->debug = false;
	$rs_counter = $conn->GetRow("select max(idpustaka) as maxid from ms_pustaka");
	$counter = $rs_counter['maxid']+1;
	
	while($rowo = $rso->FetchRow())
	{
		$record['idpustaka'] = $counter;
		$record['noseri'] = $rowo['KODE'];
		if($rowo['BAHASA'] != '')
			$record['kdbahasa'] = convBahasa((string)$rowo['BAHASA']);
		$record['kdjenispustaka'] = 'KR';
		$record['judul'] = $rowo['JUDUL'];
		$record['judulseri'] = $rowo['SERI']; 
		$record['nopanggil'] = $rowo['NO KLASIFIKASI'];
		$record['tglperolehan'] = $rowo['TANGGAL ENTRI'];
		
		$exp_pengarang = explode(",",$rowo['PENGARANG']);
		$record['authorfirst1'] = $exp_pengarang[1];
		$record['authorlast1'] = $exp_pengarang[0];
		
		$a_author = array();
		$record['authorlast2'] =  trim($rowo['PENGARANG TAMBAHAN']);
		
		$record['namapenerbit'] = $rowo['PENERBIT'];
		$record['tahunterbit'] = $rowo['TAHUN TERBIT'];
		
		// $record['edisi'] = $rowo['edisi'];
		$record['keterangan'] = $rowo['CATATAN UMUM']; 
		$record['keywords'] = $rowo['SUBYEK'].', '.$rowo['KATA KUNCI'];
		
		$record['t_user'] = $rowo['KODE OPERATOR'];
		$record['t_updattime'] = $rowo['TANGGAL ENTRI'];
		
		$sql = $conn->GetInsertSQL($col,$record);
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>