<?php
	$conno = Factory::getConno();
	
	$sqlq = "SELECT asal,`tanggal entri`,`kode`,kondisi,`nomor induk`,`status proses`,`harga` 
				FROM majalah ORDER BY `nomor induk` ";
	$rso = $conno->Execute($sqlq);
	
	
	$conn->debug = false;
	// $counter = 1;
	$maxid = $conn->GetOne("select max(ideksemplar) as maxid from pp_eksemplar");
	$counter = $maxid+1;
	
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = str_replace("\\","\\\\",substr($rowo['nomor induk'],0,20));
		$rec['ideksemplar'] = $counter;
		// $rec['kdkondisi'] = 'V'; //hardcode dibuat kondisi BAGUS
		if($rowo['tanggal entri'] =="0000-00-00"){
			$rec['tglperolehan'] = 'null';
		}else{
			$rec['tglperolehan'] = "'".$rowo['tanggal entri']."'";
		}
		$rec['kdlokasi'] = 'PA';
		
		switch($rowo['kondisi'])
		{
			case 'BAGUS':
				$rec['kdkondisi'] = 'V'; break;
			case 'RUSAK':
				$rec['kdkondisi'] = 'R'; break;
			case 'HILANG':
				$rec['kdkondisi'] = 'M'; break;
			default:
				break;
		}
		
		switch($rowo['asal'])
		{
			case 'Beli':
				$rec['kdperolehan'] = 'B'; break;
			case 'Hadiah':
				$rec['kdperolehan'] = 'H'; break;
			default:
				$rec['kdperolehan'] = 'L';
				$rec['keterangan'] .= $rec['kdperolehan'];
				break;
		}
		
		
		$rec['kdklasifikasi'] = 'TB';
		
		if($rowo['harga'] != '')
			$rec['harga'] = $rowo['harga'];
		else
			$rec['harga'] = 'null';
		
		$sql = "insert into pp_eksemplar(idpustaka,ideksemplar,noseri,kdklasifikasi,kdkondisi,kdperolehan,keterangan,harga,tglperolehan,statuseksemplar,kdlokasi) ".
				" select p.idpustaka,'{$rec['ideksemplar']}','{$rec['noseri']}','{$rec['kdklasifikasi']}','{$rec['kdkondisi']}','{$rec['kdperolehan']}','{$rec['keterangan']}',{$rec['harga']},{$rec['tglperolehan']},'ADA','{$rec['kdlokasi']}' from ms_pustaka p ".
				" left join pp_eksemplar e on p.idpustaka=e.idpustaka and e.noseri='{$rec['noseri']}' ".
				" where p.noseri='".$rowo['kode']."' and e.noseri is null";
		
		//kalau ada anomali data
		
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>