<?php
	
	$conno = Factory::getConno();
	
	$sexc ='';
	$sqlq = "select `tanggal entri`,`kode`,kondisi,`nomor induk`,`jenis buku`,`status proses`,`copy`,`lokasi`,`harga`, asal, `kode operator`, `waktu entri`
		from buku ".$sexc. " order by `nomor induk` ";
	$rso = $conno->Execute($sqlq);
	
	// $conno->debug = true;
	
	
	$conn->debug = false;
	$counter = 1;
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = str_replace("\\","\\\\",substr($rowo['nomor induk'],0,20));
		$rec['idserialitem'] = $rowo['copy'];
		$rec['ideksemplar'] = $counter;
		$rec['kdkondisi'] = 'V'; //hardcode dibuat kondisi BAGUS
		if($rowo['tanggal entri'] =="0000-00-00"){
			$rec['tglperolehan'] = 'null';
		}else{
			$rec['tglperolehan'] = "'".$rowo['tanggal entri']."'";
		}
		
		$rec['t_user'] = $rowo['kode operator'];
		$rec['t_updatetime'] = $rowo['waktu entri'];
		$rec['subyek'] = $rowo['subyek'];
		
		switch($rowo['kondisi'])
		{
			case 'BAGUS':
				$rec['kdkondisi'] = 'V'; break;
			case 'RUSAK':
				$rec['kdkondisi'] = 'R'; break;
			case 'HILANG':
				$rec['kdkondisi'] = 'M'; break;
			default:
				break;
		}
		
		switch($rowo['asal'])
		{
			case 'BELI':
				$rec['kdperolehan'] = 'B'; break;
			case 'HADIAH':
				$rec['kdperolehan'] = 'H'; break;
			case 'ADB':
				$rec['kdperolehan'] = 'A'; break;
		}
		
		switch($rowo['lokasi'])
		{
			case 'Perpustakaan Kampus A':
				$rec['kdlokasi'] = 'PA'; break;
			case 'Perpustakaan Kampus B':
				$rec['kdlokasi'] = 'PB'; break;
			default:
				$rec['kdlokasi'] = 'PA'; break;
		}
		
		$rec['kdklasifikasi'] = 'SR';
		
		if($rowo['harga'] != '')
			$rec['harga'] = $rowo['harga'];
		else
			$rec['harga'] = 'null';
		
		if($rowo['idserialitem'] == '')
			$rec['idserialitem'] = 'null';			
			
		$sql = "insert into pp_eksemplar(idpustaka,ideksemplar,noseri,kdklasifikasi,kdkondisi,kdperolehan,keterangan,harga,tglperolehan,idserialitem,statuseksemplar,kdlokasi,t_user,t_updatetime,subyek) ".
				" select p.idpustaka,'{$rec['ideksemplar']}','{$rec['noseri']}','{$rec['kdklasifikasi']}','{$rec['kdkondisi']}','{$rec['kdperolehan']}','{$rec['keterangan']}',{$rec['harga']},{$rec['tglperolehan']},{$rec['idserialitem']},'ADA','{$rec['kdlokasi']}','{$rec['t_user']}','{$rec['t_updatetime']}','{$rec['subyek']}' from ms_pustaka p ".
				" left join pp_eksemplar e on p.idpustaka=e.idpustaka and e.noseri='{$rec['noseri']}' ".
				" where p.noseri='".$rowo['kode']."' and e.noseri is null";
		
		//kalau ada anomali data
		
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>