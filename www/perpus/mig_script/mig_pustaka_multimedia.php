<?php
	$conno = Factory::getConno();
	function convBahasa($str)
	{
		if(strpos('Ind,Ind.,Indonesia,In,Perpustakan',$str) !== false)
			$str = "ID";
		else if(strpos('Eng,Ing.,Indg,Ingg,Ingn',$str) !== false)
			$str = "EN";
		else if(strpos('Bld,Belanda,Bel,Dutch,Deu',$str) !== false)
			$str = "DU";
		else if(strpos('Jer,Ger',$str) !== false)
			$str = "DE";
		else if(strpos('Prc,Fra,Pra',$str) !== false)
			$str = "FR";
		else if(strpos('Drh,Jaw,Jawa',$str) !== false)
			$str = "JW";
		else if(strpos('Drh',$str) !== false)
			$str = "DH";
		else if(strpos('Jep,Jap,Jpg,Jpn',$str) !== false)
			$str = "JP";
		else
			$str = "A";
		
		
		return $str;
	}
	
	$sexc = "";
	$sqlq = "select `nomor induk`, kode, bahasa,
		judul,
		seri ,
		`no klasifikasi`,
		`tanggal entri`,
		pengarang,
		`pengarang tambahan`,
		penerbit /*namapenerbit */,
		edisi,
		`deskripsi fisik` /*dimensipustaka */,
		asal,
		keterangan,
		`kata kunci` /*keywords*/ ,
		subyek, `no inventaris`,
		`tahun terbit`, `kode operator`, copy, harga, `bentuk fisik`, kondisi   
		from multimedia b join 
		(select min(`nomor induk`) as noinduk from multimedia group by `nomor induk`) boo
		on b.`nomor induk`=boo.noinduk ".$sexc." ";
	$rso = $conno->Execute($sqlq);

	if(!$rso->EOF)
	{
		$conn->StartTrans();
		$col = $conn->Execute("select * from ms_pustaka where 1=-1");
	}
	else
		exit('data asal kosong');
	
	$conn->debug = true;
	$maxid = $conn->GetOne("select max(idpustaka) as maxid from ms_pustaka");
	$counter = (int)$maxid+1;
	
	while($rowo = $rso->FetchRow())
	{
		$record['idpustaka'] = $counter++;
		$record['noseri'] = 'M'.$rowo['nomor induk'];#karena adanya judul yang berbeda //substr($rowo['nomor induk'],0,-1); 
		if($rowo['bahasa'] != '')
			$record['kdbahasa'] = convBahasa((string)$rowo['bahasa']);
		//$record['kdjenispustaka'] = $rowo['kdjenispustaka'];
		switch($rowo['bentuk fisik'])
		{
			case 'KASET':
				$record['kdjenispustaka'] = 'K'; break;
			case 'CD':
				$record['kdjenispustaka'] = 'C'; break;
			case 'VCD':
				$record['kdjenispustaka'] = 'V'; break;
			case 'DVD':
				$record['kdjenispustaka'] = 'D'; break;
			default:
				break;
		}
		
		$record['judul'] = $rowo['judul'];
		$record['judulseri'] = $rowo['seri']; 
		$record['nopanggil'] = $rowo['no klasifikasi'];
		$record['tglperolehan'] = $rowo['tanggal entri'];
		
		$exp_pengarang = explode(",",$rowo['pengarang']);
		$record['authorfirst1'] = $exp_pengarang[1];
		$record['authorlast1'] = $exp_pengarang[0];
		
		$a_author = array();
		if(strpos($rowo['pengarang tambahan'],'III.') !== false)
		{
			$record['authorlast3'] = strstr($rowo['pengarang tambahan'],'III.');
			$record['authorlast2'] = substr($rowo['pengarang tambahan'],strpos($rowo['pengarang tambahan'],'II.'),strlen($rowo['pengarang tambahan']) - strpos($rowo['pengarang tambahan'],'III.'));
		}
		else if(strpos($rowo['pengarang tambahan'],'II.') !== false)
		{
			$record['authorlast3'] = strstr($rowo['pengarang tambahan'],'II.');
			$record['authorlast2'] = substr($rowo['pengarang tambahan'],0,strpos($rowo['pengarang tambahan'],'II.'));
		}
		else
		{
			$record['authorlast2'] =  trim($rowo['pengarang tambahan']);
		}
		
		$record['namapenerbit'] = $rowo['penerbit'];
		$record['edisi'] = $rowo['edisi'];

		$tambahan = ($rowo['no inventaris'] ? " No.inventari :".$rowo['no inventaris'] : "");
		$tambahan .= ($rowo['copy']>0 ? " jml copy :".$rowo['copy'] : "");
		$record['keterangan'] = $rowo['keterangan'].$tambahan; 
		$record['keywords'] = $rowo['subyek'].', '.$rowo['kata kunci'];
		
		//kalau ada anomali data
		if($record['tglperolehan'] == '0000-00-00')
			unset($record['tglperolehan']);
		
		$record['edisi'] = $rowo['edisi'];
		$record['tahunterbit'] = $rowo['tahun terbit'];
		
		$record['t_user'] = $rowo['kode operator'];
		$record['t_updatetime'] = $rowo['tanggal entri'];

		$sql = $conn->GetInsertSQL($col,$record);
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	}
	
	$conn->CompleteTrans();

?>