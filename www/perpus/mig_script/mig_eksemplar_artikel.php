<?php
	$conno = Factory::getConno();
	
	$sqlq = "select `nomor induk`, `tanggal entri`, `kode operator` from artikel order by `nomor induk` ";
	$rso = $conno->Execute($sqlq);
	$conno->debug = true;
	
	$maxid = $conn->GetOne("select max(ideksemplar) as maxid from pp_eksemplar");
	$counter = $maxid+1;
	$conn->debug = false;
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = $rowo['nomor induk'];//str_replace("\\","\\\\",substr($rowo['nomor induk'],0,20));
		$rec['ideksemplar'] = $counter;
		$rec['kdkondisi'] = 'V'; //hardcode dibuat kondisi BAGUS
		if($rowo['tanggal entri'] =="0000-00-00"){
			$rec['tglperolehan'] = 'null';
		}else{
			$rec['tglperolehan'] = "'".$rowo['tanggal entri']."'";
		}
		
		$rec['kdlokasi'] = 'PA';
		$rec['kdklasifikasi'] = 'KKI';
		$rec['kdperolehan'] = 'B';
		
		if($rowo['harga'] != '')
			$rec['harga'] = $rowo['harga'];
		else
			$rec['harga'] = 'null';
		
		if($rowo['idserialitem'] == '')
			$rec['idserialitem'] = 'null';
			
		$rec['t_user'] = $rowo['kode operator'];
		$rec['t_updatetime'] = $rowo['tanggal entri'];
		
		$idpus = $conn->GetOne("select idpustaka from ms_pustaka where noseri='{$rowo['nomor induk']}' ");
			
		$sql = "insert into pp_eksemplar(idpustaka,ideksemplar,noseri,kdklasifikasi,kdkondisi,kdperolehan,keterangan,harga,tglperolehan,idserialitem,statuseksemplar,kdlokasi,t_user,t_updatetime) ".
			" values ('{$idpus}','{$rec['ideksemplar']}','{$rec['noseri']}','{$rec['kdklasifikasi']}','{$rec['kdkondisi']}','{$rec['kdperolehan']}','{$rec['keterangan']}', ".
			"{$rec['harga']},{$rec['tglperolehan']},{$rec['idserialitem']},'ADA','{$rec['kdlokasi']}','{$rec['t_user']}','{$rec['t_updatetime']}' ) ";
		
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>