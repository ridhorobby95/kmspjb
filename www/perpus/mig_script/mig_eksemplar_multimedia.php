<?php
	$conno = Factory::getConno();
	$sqlq = "SELECT `nomor induk`, 
	`kode`, 
	`bahasa`, 
	`edisi`, 
	`no klasifikasi`, 
	`judul`, 
	`penerbit`, 
	`deskripsi fisik`, 
	`kata kunci`, 
	`subyek`, 
	`seri`, 
	`copy`, 
	`harga`, 
	`tanggal entri`, 
	`tahun terbit`, 
	`pengarang`, 
	`pengarang tambahan`, 
	`bentuk fisik`, 
	`kondisi`, 
	`asal`, 
	`keterangan`, 
	`no inventaris`, 
	`kode operator`
	 
	FROM `multimedia` order by 'nomor induk'  ";
	$rso = $conno->Execute($sqlq);
	$conno->debug = true;
	
	$maxid = $conn->GetOne("select max(ideksemplar) as maxid from pp_eksemplar");
	$counter = $maxid+1;
	$conn->debug = true;
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = 'M'.$rowo['nomor induk'];//str_replace("\\","\\\\",substr($rowo['nomor induk'],0,20));
		$rec['ideksemplar'] = $counter;
		$rec['subyek'] = $rowo['subyek'];
		switch($rowo['kondisi'])
		{
			case 'BAGUS':
				$rec['kdkondisi'] = 'V'; break;
			case 'RUSAK':
				$rec['kdkondisi'] = 'R'; break;
			case 'HILANG':
				$rec['kdkondisi'] = 'M'; break;
			default:
				break;
		}
		
		if($rowo['tanggal entri'] =="0000-00-00"){
			$rec['tglperolehan'] = 'null';
		}else{
			$rec['tglperolehan'] = "'".$rowo['tanggal entri']."'";
		}
		
		$rec['kdlokasi'] = 'PA';
		$rec['kdklasifikasi'] = 'M';
		switch($rowo['asal'])
		{
			case 'BELI':
				$rec['kdperolehan'] = 'B'; break;
			case 'HADIAH':
				$rec['kdperolehan'] = 'H'; break;
			default:
				break;
		}
		
		if($rowo['harga'] != '')
			$rec['harga'] = $rowo['harga'];
		else
			$rec['harga'] = 'null';
		
		if($rowo['idserialitem'] == '')
			$rec['idserialitem'] = 'null';
			
		$rec['t_user'] = $rowo['kode operator'];
		$rec['t_updatetime'] = $rowo['tanggal entri'];
		
		$tambahan = ($rowo['no inventaris'] ? " No.inventari :".$rowo['no inventaris'] : "");
		$tambahan .= ($rowo['copy']>0 ? " jml copy :".$rowo['copy'] : "");
		$rec['keterangan'] = $rowo['keterangan'].$tambahan; 
		
		$idpus = $conn->GetOne("select idpustaka from ms_pustaka where noseri='{$rec['noseri']}' ");
			
		$sql = "insert into pp_eksemplar(idpustaka,ideksemplar,noseri,kdklasifikasi,kdkondisi,kdperolehan,keterangan,harga,tglperolehan,idserialitem,statuseksemplar,kdlokasi,t_user,t_updatetime,subyek) ".
			" values ('{$idpus}','{$rec['ideksemplar']}','{$rec['noseri']}','{$rec['kdklasifikasi']}','{$rec['kdkondisi']}','{$rec['kdperolehan']}','{$rec['keterangan']}', ".
			"{$rec['harga']},{$rec['tglperolehan']},{$rec['idserialitem']},'ADA','{$rec['kdlokasi']}','{$rec['t_user']}','{$rec['t_updatetime']}','{$rec['subyek']}' ) ";
		
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>