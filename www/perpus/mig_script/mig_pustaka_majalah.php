<?php
	$conno = Factory::getConno();
	
	function convBahasa($str)
	{
		if(strpos('Ind,Ind.,Indonesia,In,Perpustakan',$str) !== false)
			$str = "ID";
		else if(strpos('Eng,Ing.,Indg,Ingg,Ingn',$str) !== false)
			$str = "EN";
		else if(strpos('Bld,Belanda,Bel,Dutch,Deu',$str) !== false)
			$str = "DU";
		else if(strpos('Jer,Ger',$str) !== false)
			$str = "DE";
		else if(strpos('Prc,Fra,Pra',$str) !== false)
			$str = "FR";
		else if(strpos('Drh,Jaw,Jawa',$str) !== false)
			$str = "JW";
		else if(strpos('Drh',$str) !== false)
			$str = "DH";
		else if(strpos('Jep,Jap,Jpg,Jpn',$str) !== false)
			$str = "JP";
		else
			$str = "A";
		
		
		return $str;
	}
	
	$sqlq = "SELECT kode,bahasa,judul,seri,`no klasifikasi`,`tanggal entri`,penerbit,edisi,issn,keterangan,subyek,`kata kunci`,`tanggal entri`,`tahun terbit`,`kode operator` 
				FROM majalah GROUP BY kode";
	$rso = $conno->Execute($sqlq);
	if(!$rso->EOF)
	{
		$conn->StartTrans();
		$col = $conn->Execute("select * from ms_pustaka where 1=-1");
	}
	else
		exit('data asal kosong');
	
	
	// $conn->debug = true;
	// $counter = 1;
	$rs_counter = $conn->GetRow("select max(idpustaka) as maxid from ms_pustaka");
	$counter = $rs_counter['maxid']+1;
	
	while($rowo = $rso->FetchRow())
	{
		$record['idpustaka'] = $counter;
		$record['noseri'] = $rowo['kode'];
		if($rowo['bahasa'] != '')
			$record['kdbahasa'] = convBahasa((string)$rowo['bahasa']);
			
		// var_dump($rowo['kode']);
		$record['kdjenispustaka'] = 'M';
		$record['judul'] = $rowo['judul'];
		$record['judulseri'] = $rowo['seri']; 
		$record['nopanggil'] = $rowo['no klasifikasi'];
		$record['tglperolehan'] = $rowo['tanggal entri'];
		$record['namapenerbit'] = $rowo['penerbit'];
		$record['edisi'] = $rowo['edisi'];
		$record['isbn'] = $rowo['issn'];
		
		$record['keterangan'] = $rowo['keterangan']; 
		$record['keywords'] = $rowo['subyek'].', '.$rowo['kata kunci'];
		$record['t_updatetime'] = $rowo['tanggal entri']; 
		$record['tahunterbit'] = $rowo['tahun terbit']; 
		$record['t_user'] = $rowo['kode operator']; 
		
		$sql = $conn->GetInsertSQL($col,$record);
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>