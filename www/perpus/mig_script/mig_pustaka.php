<?php
	$conno = Factory::getConno();
	
	function convBahasa($str)
	{
		if(strpos('Ind,Ind.,Indonesia,In,Perpustakan',$str) !== false)
			$str = "ID";
		else if(strpos('Eng,Ing.,Indg,Ingg,Ingn',$str) !== false)
			$str = "EN";
		else if(strpos('Bld,Belanda,Bel,Dutch,Deu',$str) !== false)
			$str = "DU";
		else if(strpos('Jer,Ger',$str) !== false)
			$str = "DE";
		else if(strpos('Prc,Fra,Pra',$str) !== false)
			$str = "FR";
		else if(strpos('Drh,Jaw,Jawa',$str) !== false)
			$str = "JW";
		else if(strpos('Drh',$str) !== false)
			$str = "DH";
		else if(strpos('Jep,Jap,Jpg,Jpn',$str) !== false)
			$str = "JP";
		else
			$str = "A";
		
		
		return $str;
	}
	
	function convTopik($str)
	{
		if(strpos('FIKSI',$str) !== false)
			$str = "6127";
		else if(strpos('PAKET',$str) !== false)
			$str = "8324";
		else if(strpos('REF',$str) !== false)
			$str = "4672";
		else if(strpos('UMUM',$str) !== false)
			$str = "8325";
		
		
		return $str;
	}
	
	$sexc = " and `kode` not in ('A','U','V')";
	
	$sqlq = "select kode,bahasa,'B' as kdjenispustaka,
				judul,
				seri ,
				`no klasifikasi`,
				`tanggal entri`,
				pengarang,
				`pengarang tambahan`,
				`kota terbit` /*kota */,
				penerbit /*namapenerbit */,
				edisi,
				`deskripsi fisik` /*dimensipustaka */,
				isbn,
				`catatan umum` /*keterangan*/,
				`kata kunci` /*keywords*/ ,
				subyek, `jenis buku`,
				`tahun terbit`, `kode operator`, `waktu entri`
				from buku b join 
				(select min(`nomor induk`) as noinduk from buku group by kode) boo
				on b.`nomor induk`=boo.noinduk ".$sexc." ";
	$rso = $conno->Execute($sqlq);
	if(!$rso->EOF)
	{
		$conn->StartTrans();
		$col = $conn->Execute("select * from ms_pustaka where 1=-1");
	}
	else
		exit('data asal kosong');
	
	
	//$conn->debug = true;
	$counter = 1;
	while($rowo = $rso->FetchRow())
	{
		$record['idpustaka'] = $counter;
		$record['noseri'] = $rowo['kode'];
		if($rowo['bahasa'] != '')
			$record['kdbahasa'] = convBahasa((string)$rowo['bahasa']);
		$record['kdjenispustaka'] = $rowo['kdjenispustaka'];
		$record['judul'] = $rowo['judul'];
		$record['judulseri'] = $rowo['seri']; 
		$record['nopanggil'] = $rowo['no klasifikasi'];
		$record['tglperolehan'] = $rowo['tanggal entri'];
		
		$exp_pengarang = explode(",",$rowo['pengarang']);
		$record['authorfirst1'] = $exp_pengarang[1];
		$record['authorlast1'] = $exp_pengarang[0];
		
		$a_author = array();
		if(strpos($rowo['pengarang tambahan'],'III.') !== false)
		{
			$record['authorlast3'] = strstr($rowo['pengarang tambahan'],'III.');
			$record['authorlast2'] = substr($rowo['pengarang tambahan'],strpos($rowo['pengarang tambahan'],'II.'),strlen($rowo['pengarang tambahan']) - strpos($rowo['pengarang tambahan'],'III.'));
		}
		else if(strpos($rowo['pengarang tambahan'],'II.') !== false)
		{
			$record['authorlast3'] = strstr($rowo['pengarang tambahan'],'II.');
			$record['authorlast2'] = substr($rowo['pengarang tambahan'],0,strpos($rowo['pengarang tambahan'],'II.'));
		}
		else
		{
			$record['authorlast2'] =  trim($rowo['pengarang tambahan']);
		}
		
		$kota_terbit = explode(":",$rowo['penerbit']);
		$record['kota'] = $rowo['kota terbit']; 
		
		$exp_penerbit = explode(",",$kota_terbit[1]);
		if($exp_penerbit[1] == null){ //jika pemisahnya bukan koma
			$exp_penerbit2 = explode(";",$kota_terbit[1]);
			$record['namapenerbit'] = $exp_penerbit2[0];
			$record['tahunterbit'] = trim($exp_penerbit2[1]);
		}else{ //pemisah adalah koma
			$record['namapenerbit'] = $exp_penerbit[0];
			$record['tahunterbit'] = trim($exp_penerbit[1]);
		}
		
		$record['edisi'] = $rowo['edisi'];
		
		$a_deskripsi = explode(';',$rowo['deskripsi fisik']);
		if(count($a_deskripsi) > 0)
			$record['jmlhalromawi'] = substr(trim($a_deskripsi[0]),0,20);
		if(count($a_deskripsi) > 1)
			$record['dimensipustaka'] = substr(trim($a_deskripsi[1]),0,30);
		
		//ada isbn yang pake garis miring, aneh
		$record['isbn'] = substr($rowo['isbn'],0,30);
		
		$record['keterangan'] = $rowo['catatan umum']; 
		$record['t_user'] = $rowo['kode operator']; 
		$record['t_updatetime'] = $rowo['waktu entri']; 
		$record['keywords'] = $rowo['subyek'].', '.$rowo['kata kunci'];
		
		//kalau ada anomali data
		if($record['tglperolehan'] == '0000-00-00')
			unset($record['tglperolehan']);
		
		if($record['edisi'] == '' and strlen($record['tahunterbit']) > 4)
		{
			$record['edisi'] = substr($rowo['tahun terbit'],5,strlen($rowo['tahun terbit']));
			$record['tahunterbit'] = substr($record['tahunterbit'],0,4);
		}
		$sql = $conn->GetInsertSQL($col,$record);
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}else{
			$jenisbuku = convTopik((string)$rowo['jenis buku']);
			$sqlT = "insert into pp_topikpustaka(idtopik,idpustaka) values ('{$jenisbuku}','{$record['idpustaka']}') ";
			$ok = $conn->Execute(mb_convert_encoding($sqlT,"UTF-8"));
			if($ok == false)
			{
				echo $conn->ErrorMsg().'<br/>';
				echo $sql.'<br/>';
			}
		}
		
		$counter++;
	}
	
	
	$conn->CompleteTrans();

?>