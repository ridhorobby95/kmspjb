<?php
	$conno = Factory::getConno();
	
	$sqlq = "select `no anggota`,`no induk`,`tgl pinjam`,`tgl kembali`,`tgl balik`,`status`,`operator_peminjaman`,`operator_pengembalian` from sir_bk_f 
				order by `tgl pinjam` desc ";
	$rso = $conno->Execute($sqlq);
	
	$conn->debug = false;
	$counter = 1;
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = $rowo['no induk'];
		$rec['tgltransaksi'] = $rowo['tgl pinjam'];
		$rec['tgltenggat'] = $rowo['tgl kembali'];
		$rec['tglpengembalian'] = $rowo['tgl balik'];
		$rec['petugaspinjam'] = $rowo['operator_peminjaman'];
		$rec['petugaskembali'] = $rowo['operator_pengembalian'];
		
		if($rowo['status'] == '1')
			$rec['statustransaksi'] = '0'; //kembali
		else
			$rec['statustransaksi'] = '1'; //pinjam
		
		
		if($rec['tgltransaksi'] == '0000-00-00' or $rec['tgltransaksi'] == '' or substr($rec['tgltransaksi'],5,2)=='00')
			$rec['tgltransaksi'] = 'null';
		else
			$rec['tgltransaksi'] = "'".$rec['tgltransaksi']."'";
			
		if($rec['tgltenggat'] == '0000-00-00' or $rec['tgltenggat'] == '' or substr($rec['tgltenggat'],5,2)=='00')
			$rec['tgltenggat'] = 'null';
		else
			$rec['tgltenggat'] = "'".$rec['tgltenggat']."'";
			
		if($rec['tglpengembalian'] == '0000-00-00' or $rec['tglpengembalian'] == '' or substr($rec['tglpengembalian'],5,2)=='00')
			$rec['tglpengembalian'] = 'null';
		else
			$rec['tglpengembalian'] = "'".$rec['tglpengembalian']."'";
			
		$getseri = trim($rec['noseri']);
		$cando = $conn->GetOne("select 1 from pp_eksemplar where noseri='".$getseri."' ");
		if($cando == '') {
			$getseri = strtoupper($getseri);
			$cando = $conn->GetOne("select 1 from pp_eksemplar where upper(noseri)='".$getseri."' ");
		}
		$getseri = strtoupper($getseri);
		if($cando != '')
		{
			$sql = "insert into pp_transaksi(kdjenistransaksi,ideksemplar,idanggota,tgltransaksi,tgltenggat,tglpengembalian,statustransaksi) ".
				" select 'PJN',p.ideksemplar,'".substr($rowo['no anggota'],0,20)."',{$rec['tgltransaksi']},{$rec['tgltenggat']},{$rec['tglpengembalian']},{$rec['statustransaksi']} ".
				" from pp_eksemplar p where upper(noseri)='".$getseri."' ";
			
			
			//kalau ada anomali data
			
			$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
			if($ok == false)
			{
				echo $conn->ErrorMsg().'<br/>';
				echo $sql.'<br/>';
				break;
			}
			$cando = '';
		}
		else
		{
			$conn->Execute("insert into aux_gagal values('".$rec['noseri']."','{$rowo['no anggota']}','sir_bk_f')");
		}
	}
	
	$conn->CompleteTrans();

?>