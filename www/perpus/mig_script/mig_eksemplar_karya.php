<?php
	$conno = Factory::getConno();
	
	$sqlq = "SELECT `ID`, `NOMOR INDUK`, `KODE`, `TANGGAL ENTRI`, `BAHASA`, `PRE_KLAS`, `NO KLASIFIKASI`, `PENGARANG`, 
		`PENGARANG TAMBAHAN`, `BADAN KORPORASI`, `BADAN KORPORASI TAMBAHAN`, `JUDUL`, `PENERBIT`, `TAHUN TERBIT`, `DESKRIPSI FISIK`, `JENIS`, 
		`CATATAN UMUM`, `SUBYEK`, `ASAL`, `HARGA`, `ANOTASI`, `KATA KUNCI`, `BADAN PEMILIK`, `KODE OPERATOR`, `STATUS PROSES`, `KONDISI`
		FROM karya";
	$rso = $conno->Execute($sqlq);
	
	$maxid = $conn->GetOne("select max(ideksemplar) as maxid from pp_eksemplar");
	$counter = $maxid+1;
	$conn->debug = true;
	while($rowo = $rso->FetchRow())
	{
		$rec = array();
		
		$rec['noseri'] = str_replace("\\","\\\\",substr($rowo['NOMOR INDUK'],0,20));
		// $rec['idserialitem'] = $rowo['copy'];
		$rec['ideksemplar'] = $counter;
		if($rowo['TANGGAL ENTRI'] =="0000-00-00"){
			$rec['tglperolehan'] = 'null';
		}else{
			$rec['tglperolehan'] = "'".$rowo['TANGGAL ENTRI']."'";
		}
		$rec['kdlokasi'] = 'PA';
		switch($rowo['KONDISI'])
		{
			case 'BAGUS':
				$rec['kdkondisi'] = 'V'; break;
			case 'RUSAK':
				$rec['kdkondisi'] = 'R'; break;
			case 'HILANG':
				$rec['kdkondisi'] = 'M'; break;
			default:
				break;
		}
		
		switch($rowo['ASAL'])
		{
			case 'Beli':
				$rec['kdperolehan'] = 'B'; break;
			case 'Hadiah':
				$rec['kdperolehan'] = 'H'; break;
			default:
				$rec['kdperolehan'] = 'L';
				$rec['keterangan'] .= $rec['kdperolehan'];
				break;
		}
		
		$rec['kdklasifikasi'] = 'KR';
		
		if($rowo['HARGA'] != '')
			$rec['harga'] = $rowo['HARGA'];
		else
			$rec['harga'] = 'null';
		
		$sql = "insert into pp_eksemplar(idpustaka,ideksemplar,noseri,kdklasifikasi,kdkondisi,kdperolehan,keterangan,harga,tglperolehan,statuseksemplar,kdlokasi) ".
				" select p.idpustaka,'{$rec['ideksemplar']}','{$rec['noseri']}','{$rec['kdklasifikasi']}','{$rec['kdkondisi']}','{$rec['kdperolehan']}','{$rec['keterangan']}',{$rec['harga']},{$rec['tglperolehan']},'ADA','{$rec['kdlokasi']}' from ms_pustaka p ".
				" left join pp_eksemplar e on p.idpustaka=e.idpustaka and e.noseri='{$rec['noseri']}' ".
				" where p.noseri='".$rowo['KODE']."' and e.noseri is null";
		
		$ok = $conn->Execute(mb_convert_encoding($sql,"UTF-8"));
		if($ok == false)
		{
			echo $conn->ErrorMsg().'<br/>';
			echo $sql.'<br/>';
		}
	$counter++;
	}
	
	$conn->CompleteTrans();

?>