<?php
class Help {
	/* FUNGSI SISTEM */
	
	
	// redirect ke halaman lain
	function redirect($url=NULL) {
			if (!$url) { 
				$currentFile = $_SERVER['PHP_SELF'];
				$parts = Explode('/', $currentFile);
				$url = $parts[count($parts) - 1];
			}
			header("Location: $url");
		exit;
	}
	
	//simpan 3 field pasti time,remote,ip
	
	function Identitas(&$record){
	$record['t_user']=Helper::cStrNull($_SESSION['PERPUS_USER']);
	$record['t_updatetime']=date('Y-m-d H:i:s');
	$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
	}
	
	function IdentitasBaca(&$record,$user){
	$record['t_user']=$user;
	$record['t_updatetime']=date('Y-m-d H:i:s');
	$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);	
	}
	
	
	# flash data untuk notifikasi setelah POST
	function setFlashData($name, $value) {
		$_SESSION['PERPUS_FLASHDATA2'][$name] = $value;
	}
	
	function getFlashData($name) {
		if (isset($_SESSION['PERPUS_FLASHDATA2'][$name]))
			return $_SESSION['PERPUS_FLASHDATA2'][$name];
		return false;
	}

	function clearFlashData() {
		unset($_SESSION['PERPUS_FLASHDATA2']);
	}

	function tglEng($tglnya) { //format harus Y-m-d
	$bulan=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	
	$format=explode("-",$tglnya);
	$tgl=substr($format[2],0,2);
	$bln=$bulan[$format[1]-1];
	$thn=$format[0];
	
	$val=$tgl." ".$bln." ".$thn;	
	return $val;
	}
	
	function tglEngStamp($tglstamp) { //format default Y-m-d time
	$datetime=explode(" ",$tglstamp);
	$date=Helper::tglEng($datetime[0]);
	$time=$datetime[1];
	
	$val=$date." ".$time;
	return $val;
	
	}
	
	function bulanInd($val){ //format angka : 8,9,10
	$bulan=array('januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
	
	$dadie=$bulan[$val-1];
	return $dadie;
	}
	
	function limitS($str,$i=55){
		$len=strlen($str);
		$string=substr($str,0,$i);
		if($len>$i)
			$string.=" ...";
		return $string;
	}
	
	function strPad($str,$val,$var,$c){
		if($c=='l')
			$string=str_pad($str,$val,$var,STR_PAD_LEFT);
		else if($c=='b')
			$string=str_pad($str,$val,$var,STR_PAD_BOTH);
		else
			$string=str_pad($str,$val,$var);
				
		return $string;
	}
	
	function getPass() {
    $length = 7;
    $characters = '01X234Y567Z89aAbBRcCdDeEfFghGiSjHklImQnoJpTqrVsUtKuvMwxNyWOzP';
    $pass ='';    

    for ($p = 0; $p < $length; $p++) {
        $pass .= $characters[mt_rand(0, strlen($characters))];
    }

    return $pass;
	}
	
	function bulanRomawi($date) { // format tanggal dd-mm-Y [ yg penting bulan ditengah
		$romawi=array ('I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
		
		$slice=explode("-",$date);
		
		$toromawi=$romawi[$slice[1]-1];
		
		return $toromawi;
	
	}
	
	function getArrParam($arparam,$parkey){
		$paritem = array();
		$recdet = array();

		if(is_array($arparam)){
			foreach($arparam as $kparam => $vparam){
				if(!empty($_POST[$vparam]))
					$paritem[$vparam] = Helper::cStrFill($_POST[$vparam]);
			}
			if(count($paritem[$parkey])>0){
				foreach($paritem[$parkey] as $i => $val){
					foreach($paritem as $paramname => $paramval){
						$recdet[$i][$paramname] = $paramval[$i];
					}						
				}
			}
		}
		return $recdet;
	}
	function dateKurangi($dformat, $endDate, $beginDate)
	{
		$date_parts1=explode($dformat, date('Y-m-d',strtotime($beginDate)));
		$date_parts2=explode($dformat, date('Y-m-d',strtotime($endDate)));
		$start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
		$end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
		return $end_date - $start_date;
	}
	function arrStatusST() {
		return array('1'=>'Disetujui','0' =>'Ditolak');
	}
	
	function arrStatusP() {
		return array('A' => 'Diajukan','S' => 'Selesai', 'B' => 'Dibatalkan', 'PP' => 'Di Proses Perpustakaan', 'PK' => 'Di Proses Keuangan', 'V' => 'Verifikasi Supplier');
	}
	
	function getArrStatusP($value) {
		$vararr = array('A' => 'Diajukan','S' => 'Selesai', 'B' => 'Dibatalkan', 'PP' => 'Di Proses Perpustakaan', 'PK' => 'Di Proses Keuangan', 'V' => 'Verifikasi Supplier');
		
		return $vararr[$value];
	}
	
	function strOnly($str){
		return str_replace(array('-', '.', '(', ')', '!', '@', '#', '$', '%', '^', '&', '*', '_', '=', '+',':','?'), '',$str);
	} 
	
	function formatNumber($num,$ndec='',$ismoney=false,$format=true) {
		if($num == '')
			return $num;
		
		if($ndec == '') {
			if($ismoney)
				$ndec = 2;
			else
				$ndec = 0;
		}
		
		if($num < 0) {
			$num = abs($num);
			$bracket = true;
		}
		
		// untuk misalnya 100.000 jadi 100000, tapi desimal disederhanakan dulu
		$num = round($num,2);
		$num = preg_replace('/\.(\d{3})/','${1}',$num);
		
		if($format)
			$num = number_format($num,$ndec,',','.');
		
		if($ismoney)
			$num = 'Rp. '.$num;
		
		if($bracket)
			return '('.$num.')';
		else
			return $num;
	}
	
		// membentuk url dengan satu pintu
		function xIPAddress() {
			if(array_key_exists('HTTP_X_FORWARDED_FOR',$_SERVER))
				return array_pop(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']));
			else
				return $_SERVER['REMOTE_ADDR'];
		}
		
		function removeSpecial($mystr,$stripapp = false) {
		// $pattern = '/[%&;()\"';
		$pattern = '/[%&;\"';
		if($stripapp == false) // tidak stripping ', tapi direplace jadi '', biasanya dipakai di nama
			$mystr = str_replace("'","''",$mystr);
		else
			$pattern .= "\'";
		if(preg_match('/[A-Za-z%\/]/',$mystr)) // kalau ada alfabet, %, atau /, strip <>
			$pattern .= '<>';
		$pattern .= ']|--/';
		
		return preg_replace($pattern, '$1', $mystr);
	}

}

?>