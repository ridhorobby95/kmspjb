<?php
	define('BASEPATH',1);
	define('ADODB_ASSOC_CASE', 0);
	require_once('../includes/adodb/adodb.inc.php');
	require_once('help.class.php');
	require_once('../../application/helpers/x_helper.php');

	session_start();

	$conn = ADONewConnection('oci8po');
	
	// if (!$conn->Connect('192.168.1.5','perpus','perpus','sevima'))
	// 	die("Error : Tidak bisa melakukan koneksi dengan database");
	if (!$conn->Connect('localhost','perpus','perpus','XE'))
		die("Error : Tidak bisa melakukan koneksi dengan database");

	$conn->SetFetchMode(ADODB_FETCH_ASSOC);
	
	$separator = " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ";
	
	$jmllokasi = $conn->GetOne("select count(*) from lv_lokasi");
	if($jmllokasi==1){
		$r_key = $conn->GetOne("select kdlokasi from lv_lokasi");
		$_SESSION['lokasi']=$r_key;
	}else{
		$r_key=Help::removeSpecial($_POST['lokasi']);
		if($r_key)
			$_SESSION['lokasi']=$r_key;
		else
			$r_key = $_SESSION['lokasi'];
	}

	

	if(isset($_POST['submit'])) {
		// var_dump($_POST);
		// die('post submit');

		$r_aksi=Help::removeSpecial($_POST['act']);
		$idanggotatamu = Help::removeSpecial($_POST['idanggota']);

		if($r_aksi=='back'){
			unset($_SESSION['lokasi']);
			unset($_SESSION['anggotatamu']);
			header('Location: index.php');
			exit();
		}elseif($r_aksi=='cek'){				
			$c_anggota=$conn->GetRow("select idanggota,kdjenisanggota,idunit,tglexpired,statusanggota 
						 from ms_anggota where idanggota='$idanggotatamu'");
			if($c_anggota){
				if($c_anggota['statusanggota'] == '1'){ //aktif
					if($c_anggota['tglexpired'] >= date('Y-m-d') or $c_anggota['tglexpired']==''){
						$_SESSION['anggotatamu']=$c_anggota['idanggota'];
						$record=array();
						$record['tanggal']=date('Y-m-d H:i:s');
						$record['idanggota']=$_SESSION['anggotatamu'];
						$record['kdlokasi']=$_SESSION['lokasi'];
						$record['baca']= (!empty($_POST['baca'])) ? $_POST['baca'] : '';
						$record['sharing']= (!empty($_POST['sharing'])) ? $_POST['sharing'] : '';
						$record['pinjam_buku']= (!empty($_POST['pinjam_buku'])) ? $_POST['pinjam_buku'] : '';
						$record['scan']=(!empty($_POST['scan'])) ? $_POST['scan'] : '';
						$record['lainlain']=(!empty($_POST['lainlain'])) ? $_POST['lainlain'] : '';
						
						if($c_anggota['idunit'])
							$record['idunit']=$c_anggota['idunit'];
						
						// var_dump($record);die();
						// insert buku tamu
						$col = $conn->Execute("select * from pp_bukutamu where 1=-1");
						$sql = $conn->GetInsertSQL($col,$record);
						$conn->Execute($sql);
						$_POST['idanggota']='';

						if($conn->ErrorNo() != 0){
							$errdb = 'Proses Absensi gagal.';	
							Help::setFlashData('errdb', $errdb);
						}
						else {
							$sucdb = 'Data Absensi telah tersimpan, Terima kasih.';	
							Help::setFlashData('sucdb', $sucdb);
							Help::redirect();
						}
						
						
					}else{
						$errdb = 'Anggota Telah Expired.';
						Help::setFlashData('errdb', $errdb); 	
						Help::redirect();
					}
				}else{
					if($c_anggota['statusanggota'] == '0')
						$errdb = 'Anggota Telah Non Aktif.';
					/*else
						$errdb = 'Anggota Telah Di Blokir.';*/
					Help::setFlashData('errdb', $errdb); 
					Help::redirect();
				}
			}else {
				$errdb = 'Identitas anggota tidak ditemukan.';	
				Help::setFlashData('errdb', $errdb);
				Help::redirect();
			}
		}
		
	}
	
	
	if ($_SESSION['anggotatamu']!=''){
		$cekA=$conn->GetRow("select a.*,j.namajenisanggota,ju.namasatker from ms_anggota a
				left join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
				left join ms_satker ju on a.idunit=ju.kdsatker
				where idanggota='".$_SESSION['anggotatamu']."'");
		// var_dump($cekA);die();
		
		$tenggat=$conn->Execute("select noseri,judul,edisi,nopanggil,TO_CHAR(tgltransaksi, 'yyyy-mm-dd') AS tgltransaksi, TO_CHAR(tgltenggat, 'yyyy-mm-dd') AS tgltenggat
					from v_trans_list where idanggota='".$_SESSION['anggotatamu']."'  and tglpengembalian is null");
		
		if($cekA['kdjenisanggota'] == "U"){#Umum
			$dirfoto = $dir_perpus.trim($_SESSION['anggotatamu']).'.jpg';
			$viewfoto = $url_perpus.trim($_SESSION['anggotatamu']).'.jpg';	
		}elseif($cekA['kdjenisanggota'] == "T"){#Dosen, Karyawan
			$dirfoto = $dir_sdm.trim($cekA['idpegawai']).'.jpg';
			$viewfoto = $url_sdm.trim($cekA['idpegawai']).'.jpg';
		}
	}
	
	$v_foto = "http://localhost/kmspjb/www/index.php/publ1c/profile?q=".xEncrypt($cekA['idpegawai'])."&s=150";
		
	$berita = $conn->Execute("select judulberita, isiberita from pp_berita where tglexpired>=current_date");
	
	$j_sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM (";
	$j_sql .= "select idtamu,a.namaanggota,TO_CHAR(b.tanggal, 'yyyy-mm-dd') AS tglkunjungan, TO_CHAR(b.tanggal, 'yyyy-mm-dd HH24:MI:SS') as jam,
		b.idanggota,j.kdsatker,j.namasatker, o.namajenisanggota, b.baca, b.sharing, b.pinjam_buku, b.scan, b.lainlain
		from pp_bukutamu b
		left join ms_anggota a on a.idanggota=b.idanggota
		left join ms_satker j on j.kdsatker=b.idunit
		left join lv_jenisanggota o on o.kdjenisanggota=a.kdjenisanggota
		where to_date(to_char(tanggal,'yyyy-mm-dd'),'yyyy-mm-dd') = to_date(to_char(current_date,'yyyy-mm-dd'),'yyyy-mm-dd') and b.kdlokasi = '$_SESSION[lokasi]'";
	$j_sql .= ")  inner_query WHERE rownum <= 10) order by idtamu DESC";
	
	$sqlj = $conn->Execute($j_sql);
	
	//$jumlah = $sqlj->RowCount();
	$jumlah = $conn->GetOne("select count(*) as jumlah_kunjungan from pp_bukutamu
				where to_date(to_char(tanggal,'yyyy-mm-dd'),'yyyy-mm-dd') = to_date(to_char(current_date,'yyyy-mm-dd'),'yyyy-mm-dd')
				and kdlokasi = '$_SESSION[lokasi]' ");
	
	$sql_total = $conn->GetRow("select count(*) as jumlah_kunjungan
				   from pp_bukutamu
				   where kdlokasi = '$_SESSION[lokasi]'
				   and to_char(tanggal,'yyyy') = to_char(current_date,'yyyy')
				");
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by kdlokasi");
	$l_lokasi = $rs_cb->GetMenu2('lokasi',$r_key,true,false,0,'id="lokasi" class="ControlStyle form-control2" style="width:220" onchange="goSubmit()"');
	
	$lokasi = $conn->GetOne("select namalokasi from lv_lokasi where kdlokasi = '$_SESSION[lokasi]' ");
?>
<html>
<head>
<title>[ PJB LIBRARY ] Buku Tamu Pengunjung</title>
<script type="text/javascript" src="../scripts/jquery.js"></script>
<script type="text/javascript" src="../scripts/jquery.dimensions.js"></script>
<script type="text/javascript" src="../scripts/jquery.positionBy.js"></script>
<script type="text/javascript" src="../scripts/jquery.jdMenu.js"></script>
<script type="text/javascript" src="../scripts/jquery.menu.js"></script>
<link href="../style/jquery.jdMenu.css" rel="stylesheet" type="text/css">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<link href="../style/menu.css" type="text/css" rel="stylesheet">
<link href="../style/pager.css" type="text/css" rel="stylesheet">
<link href="../style/officexp.css" type="text/css" rel="stylesheet">
<link href="../style/button.css" rel="stylesheet">
<link href="../style/forum.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="../images/favicon.ico" />	
<script type="text/javascript" src="../scripts/forpager.js"></script>
<script type="text/javascript" src="../scripts/foredit.js"></script>

<link rel="stylesheet" href="../style/font-awesome.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php
if(isset($_POST['idanggota'])){
	// var_dump($_POST);
	unset($_SESSION['anggotatamu']);
	if ($_SESSION['anggotatamu']!=''){
		$cekA=$conn->GetRow("select a.*,j.namajenisanggota,ju.namasatker from ms_anggota a
				left join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
				left join ms_satker ju on a.idunit=ju.kdsatker
				where idanggota='".$_SESSION['anggotatamu']."'");
		// var_dump($cekA);die();
		
		$tenggat=$conn->Execute("select noseri,judul,edisi,nopanggil,TO_CHAR(tgltransaksi, 'yyyy-mm-dd') AS tgltransaksi, TO_CHAR(tgltenggat, 'yyyy-mm-dd') AS tgltenggat
					from v_trans_list where idanggota='".$_SESSION['anggotatamu']."'  and tglpengembalian is null");
		
		if($cekA['kdjenisanggota'] == "U"){#Umum
			$dirfoto = $dir_perpus.trim($_SESSION['anggotatamu']).'.jpg';
			$viewfoto = $url_perpus.trim($_SESSION['anggotatamu']).'.jpg';	
		}elseif($cekA['kdjenisanggota'] == "T"){#Dosen, Karyawan
			$dirfoto = $dir_sdm.trim($cekA['idpegawai']).'.jpg';
			$viewfoto = $url_sdm.trim($cekA['idpegawai']).'.jpg';
		}
	}
	$anggota = $_POST['idanggota'];
	$unit = $conn->GetOne("select u.nama from um.users us inner join um.unit u on us.kodeunit=u.kodeunit where us.nid='$anggota'");
	$namaanggota = $conn->GetOne("select msa.namaanggota from perpus.ms_anggota msa where msa.idanggota='$anggota'");
	var_dump($unit);

	?>
<script>
$(document).ready(function(){
	// window.location.href = $('#pesanpop').attr('href');
	$('#popup').dialog({ title: "Aktivitas" });
});
</script>
<?php
 } 
 ?>
<!-- <script type="text/javascript" src="../scripts/bootstrap.js"></script> -->
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="document.getElementById('idanggota').focus()">

<!-- <div id="button"><a href="#popup" id="pesanpop">Pesan</a></div> -->
<div id="popup" style="display: none;">
	<h5>Nama Pengunjung : <?= $namaanggota ?></h5>
	<h5>Unit : <?= $unit ?></h5>
    <form action="" method="post">
    	<div class="form-group">
    		<div class="checkbox">
			  <label>
			    <input type="checkbox" name="baca" value="1">
			    Baca
			  </label>
			</div>
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="sharing" value="1">
			    Sharing
			  </label>
			</div>
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="pinjam_buku" value="1">
			    Pinjam Buku
			  </label>
			</div>
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="scan" value="1">
			    Scan
			  </label>
			</div>
			<div class="checkbox">
			  <label>
			    <input type="checkbox" name="lainlain" value="1">
			    Lain-lain
			  </label>
			</div>
    	</div>
    	<input type="hidden" name="idanggota" id="act" value="<?= $_POST['idanggota'] ?>">
    	<input type="hidden" name="act" id="act" value="cek">
		<input type="hidden" name="lokasi" id="lokasi" value="<?= $r_key ?>"><br>
    	<div class="form-group" align="center">
    		<button type="submit" name="submit" class="btn btn-info-masuk">Simpan</button>
    	</div>
    </form>
</div>

<div class="WorkHeader">
	<div style="width:1010px;margin:0 auto;">
		<div style="color:#fff;float:left;margin-top:10px;"> 
			User ID: Buku Tamu Pengunjung <span style="color:#fff;">|</span>
			Hak Akses: User <span style="color:#fff;">|</span>
			Tanggal: <?= date('d-m-Y'); ?>
		</div>
		<img src="../images/logosim2.png" style="vertical-align:top;float:right;position:relative;">
			<div style="clear:both;"></div>
	</div>
</div>
</div>

<div align="center">
<form name="perpusform" id="perpusform" method="post">
<div style="width:1010px;margin:0 auto;">
	<br /><br />
	<h1 style="font-size:30px;font-weight:normal;color:#015593;">Selamat Datang</h1>
	<?php if($lokasi){?>
<h1 style="font-size:30px;font-weight:normal;color:#015593;"><?=$lokasi?"Di ".strtoupper($lokasi):"";?></h1>
	<?php } ?>
</div>
<div id="wrapper" style="width:1010px;">
	<div class="SideItem" id="SideItem">
		<?php include_once('_notifikasi.php'); ?>
		<?php if(!$r_key){?>
		<table cellpadding="0" cellspacing="0" align="center" width="500"  style="background:#E0FFF3;border:1pt solid #58B793;-moz-border-radius:5px;padding:10px;margin-top:10px;">
			<tr>
				<td width="200"><strong><h1>PILIH LOKASI : </strong></h1></td>
				<td valign="top"><strong><h1><?=$l_lokasi;?></strong></h1></td>
			</tr>
		</table>
		<?php }elseif($r_key){?>
		<table cellpadding="0" cellspacing="0" align="center" width="500"  style="background:#E0FFF3;border:1pt solid #58B793;-moz-border-radius:5px;padding:10px;margin-top:10px;">
			<tr>
				<td width="130"><img border="1" id="imgfoto" src="<?= $v_foto; ?>" width="110" height="110"></td>
				<td valign="top">
					<b>Masukkan No. Anggota Anda</b><br><input type="text" name="idanggota" id="idanggota" class="ControlStyleT" maxlength="20" size="35" onkeydown="etrCek(event);" placeholder="Masukan Nomer.."><br>
					<?php if($cekA['namaanggota']){?><h2><b>Terima Kasih, <?= $cekA['namaanggota'] ?> </b></h2>
					<b>Anda pengunjung ke : <?= $jumlah; ?></b><?php } ?> <br> <b>Total Kunjungan Hingga Hari ini: <?=$sql_total['jumlah_kunjungan']?></b><br />
				</td>
				<td style="display:none"><input type="button" name="cek" id="cek" value="Masuk" onclick="goCek()" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:25px;width:130px;font-size:12px;"></td>
			</tr>
		</table><br>
		<?php if($_SESSION['anggotatamu']!='') { ?>
		<?php if($tenggat->fields['noseri']!='') { ?>
		<table cellpadding="3" cellspacing="0" class="tborder GridStyle" style="margin-bottom:20px;width:780px;">
		<tr class="catbg3" height=20>
			<td width="80" style="font-size:75%;">NO. INDUK</td>
			<td style="font-size:75%;">JUDUL</td>
			<td width="90" style="font-size:75%;" align="center">NO PANGGIL</td>
			<td width="90" style="font-size:75%;" align="center">TGL PINJAM</td>
			<td width="90" style="font-size:75%;" align="center">TGL TENGGAT</td>
		</tr>
		<?php while($row=$tenggat->FetchRow()){ ?>
		<tr height="18">
			<td>&nbsp; &nbsp;<?= $row['noseri'] ?></td>
			<td><?= $row['judul'] ?></td>
			<td>&nbsp; &nbsp; &nbsp;<?= $row['nopanggil'] ?></td>
			<td align="center"><?= Help::tglEng($row['tgltransaksi']) ?></td>
			<td align="center"><?= $row['tgltenggat']=='' ? 'Maximal' : Help::tglEng($row['tgltenggat']) ?></td>
		</tr>


		<?php } ?>
		</table><br>
		<?php }} ?>
		
		<header style="width:780px;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="../images/BIODATA.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Daftar Pengunjung</h1>
				</div>
				<?php if($jmllokasi>1){?>
				<div class="right">
				    <div class="addButton" style="float:left; margin:right:10px;"  title="Back" onClick="back()"><<</div>
				</div>
				<?php } ?>
			</div>
		</header>
		<table cellpadding="3" cellspacing="0" class="tborder GridStyle" style="margin-bottom:20px;width:780px;">
		<tr class="catbg3" height=20>
			<td rowspan="2" align="left" width="80" style="font-size:75%;">Tanggal</td>
			<td rowspan="2" align="left" width="80" style="font-size:75%;">Jam</td>
			<td rowspan="2" align="left" width="90" style="font-size:75%;" align="center">No. Anggota</td>
			<td rowspan="2" align="left" width="250" style="font-size:75%;" align="center">Nama Anggota</td>
			<td rowspan="2" align="left" width="150" style="font-size:75%;" align="center">Bagian</td>
			<td colspan="5" align="center" width="150" style="font-size:75%;" align="center">Aktivitas</td>
		</tr>
		<tr class="catbg3" height=20>
			<td align="left" width="150" style="font-size:75%;" align="center">Baca</td>
			<td align="left" width="150" style="font-size:75%;" align="center">Sharing</td>
			<td align="left" width="150" style="font-size:75%;" align="center">Pinjam Buku</td>
			<td align="left" width="150" style="font-size:75%;" align="center">Scan</td>
			<td align="left" width="150" style="font-size:75%;" align="center">Lain-lain</td>
		</tr>
		<?php $i = 0; while($isi_buku=$sqlj->FetchRow()){ $i++; ?>
		<tr height="18">
			<td align="left"><?= Help::tglEng($isi_buku['tglkunjungan']) ?></td>
			<td align="left"><?= substr($isi_buku['jam'],11,8); ?></td>
			<td align="left"><?= $isi_buku['idanggota'] ?></td>
			<td align="left" nowrap><?= Help::tglEng($isi_buku['namaanggota']) ?></td>
			<td align="left" nowrap><?= $isi_buku['namasatker']? $isi_buku['namasatker'] : $isi_buku['namajenisanggota']; ?></td>
			<td align="center" nowrap><?= $isi_buku['baca'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
			<td align="center" nowrap><?= $isi_buku['sharing'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
			<td align="center" nowrap><?= $isi_buku['pinjam_buku'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
			<td align="center" nowrap><?= $isi_buku['scan'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
			<td align="center" nowrap><?= $isi_buku['lainlain'] ? '<i class="fa fa-check" aria-hidden="true"></i>' : ''; ?></td>
			
		</tr>
		<?php }
		if ($i==0) { ?>
		<tr>
			<td align="center" colspan="10"><b>Data tidak ditemukan.</b></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="10" class="FootBG" align="right">&nbsp;</td>
		</tr>
		</table>

		<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">
		<input type="hidden" name="lokasi" id="lokasi" value="<?= $r_key ?>">
		<?php } ?>
	</div>
</div>

</form>
</div>

<div id="newsz" style="position:fixed;display:block;bottom:0px;width:100%;height:40px;border:1pt solid #58B793;background:#E0FFF3;color:000;">
<marquee behavior="scroll" direction="up" scrollamount="2" width="100%" height="40" vspace="5" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 2, 0);">
<center>
<?php if($berita->fields['judulberita']!=''){
	while($rowb=$berita->FetchRow()){
		echo "<b>".$rowb['judulberita']."</b><br>";
	}
} ?>
</center>
</marquee>

</div>

</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

if (parseInt(navigator.appVersion)>3) {
 if (navigator.appName=="Netscape") {
  winW = window.innerWidth-16;
  winH = window.innerHeight-16;
 }
 if (navigator.appName.indexOf("Microsoft")!=-1) {
  winW = document.body.offsetWidth-20;
  winH = document.body.offsetHeight-20;
 }
}
var tengah = (winW/3) + (winW*5/100);
document.getElementById('news').style.paddingLeft = tengah + 'px';
var tinggi = document.getElementById('perpusform').offsetHeight;
document.getElementById('perpusform').style.height = (tinggi+100) + 'px';
</script>
<script type="text/javascript">
	function goCek(){
		document.getElementById('act').value="cek";
		goSubmit();
	}
	
function etrCek(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;

	if (key == 13)
		document.getElementById("cek").click();
}

function back(){
	document.getElementById('act').value="back";
	goSubmit();
}
</script>
<script type="text/javascript">
$(".subnav").hover(function() {
    $(this.parentNode).addClass("borderbottom");
}, function() {
    $(this.parentNode).removeClass("borderbottom");
});

</script>
</script>
</html>
<?php Help::clearFlashData();?>
