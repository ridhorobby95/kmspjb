<?php
	require_once('../includes/adodb/adodb.inc.php');
	require_once('help.class.php');
	
	$r_key=pg_escape_string($_POST['lokasi']);
	if(!$r_key){
		header('Location: index.php');
		exit();
	}

	session_start();

	$conn = ADONewConnection('postgres8');
	$conns = ADONewConnection('postgres8');
	$conna = ADONewConnection('postgres8');
	
	if (!$conn->Connect('192.168.1.8:5433','postgres','sembarang','unusaperpus'))
		die("Error : Tidak bisa melakukan koneksi dengan database");

	$conn->SetFetchMode(ADODB_FETCH_ASSOC);
	
	if (!$conna->Connect('192.168.1.8:5433','postgres','sembarang','unusa'))
		echo "<blink style=color:red>Error : Tidak bisa melakukan koneksi dengan database Akademik</blink>";
		
	$conna->SetFetchMode(ADODB_FETCH_ASSOC);
				
	$separator = " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ";

	/*$dir = "D:/www/sevimaperpus/";
	$url = "http://localhost/sevimaperpus/";*/
	
	$dir_perpus = "D:/www/sevimaperpus/perpus/images/perpustakaan/anggota/";
	$url_perpus = "http://localhost/sevimaperpus/perpus/images/perpustakaan/anggota/";
	
	$dir_sdm = "D:/www/sevimaperpus/sdm/sdm/up_l04ds/fotopeg/";
	$url_sdm = "http://localhost/sevimaperpus/sdm/sdm/up_l04ds/fotopeg/";
	
	$dir_akad = "D:/www/sevimaperpus/siakad/siakad/uploads/fotomhs/";
	$url_akad = "http://localhost/sevimaperpus/siakad/siakad/uploads/fotomhs/";
	
	if(!empty($_POST)){
		$r_aksi=pg_escape_string($_POST['act']);
		$idanggotatamu = pg_escape_string($_POST['idanggota']);

		if($r_aksi=='cek'){			
			$c_anggota=$conn->GetRow("select idanggota,kdjenisanggota,idunit,tglexpired,statusanggota from ms_anggota where idanggota='$idanggotatamu'");
			if($c_anggota){
				if($c_anggota['statusanggota'] == '1'){ //aktif
					if($c_anggota['tglexpired'] >= date('Y-m-d') or $c_anggota['tglexpired']==''){
						$_SESSION['anggotatamu']=$c_anggota['idanggota'];
						$record=array();
						$record['tanggal']=date('Y-m-d H:i:s');
						$record['idanggota']=$_SESSION['anggotatamu'];
						if($c_anggota['idunit'])
							$record['kdjurusan']=$c_anggota['idunit'];
						
						$col = $conn->Execute("select * from pp_bukutamu where 1=-1");
						$sql = $conn->GetInsertSQL($col,$record);
						$conn->Execute($sql);
						
						if($conn->ErrorNo() != 0){
							$errdb = 'Proses Absensi gagal.';	
							Help::setFlashData('errdb', $errdb);
						}
						else {
							$sucdb = 'Data Absensi telah tersimpan, Terima kasih.';	
							Help::setFlashData('sucdb', $sucdb);
							Help::redirect();
						}
					}else{
						$errdb = 'Anggota Telah Expired.';
						Help::setFlashData('errdb', $errdb); 	
						Help::redirect();
					}
				}else{
					if($c_anggota['statusanggota'] == '0')
						$errdb = 'Anggota Telah Non Aktif.';
					/*else
						$errdb = 'Anggota Telah Di Blokir.';*/
					Help::setFlashData('errdb', $errdb); 
					Help::redirect();
				}
			}else {
				$errdb = 'Identitas anggota tidak ditemukan.';	
				Help::setFlashData('errdb', $errdb);
				Help::redirect();
			}
		}
		
	}
	
	
	if ($_SESSION['anggotatamu']!=''){
		$cekA=$conn->GetRow("select a.*,j.namajenisanggota,ju.namajurusan from ms_anggota a
				left join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
				left join lv_jurusan ju on a.idunit=ju.kdjurusan
				where idanggota='".$_SESSION['anggotatamu']."'");
		
		$tenggat=$conn->Execute("select noseri,judul,edisi,nopanggil,tgltransaksi,tgltenggat from v_trans_list where idanggota='".$_SESSION['anggotatamu']."'  and tglpengembalian is null");
		
		if($cekA['kdjenisanggota'] == "U"){#Umum
			$dirfoto = $dir_perpus.trim($_SESSION['anggotatamu']).'.jpg';//$dir."perpus/images/perpustakaan/anggota/".trim($_SESSION['anggotatamu']).'.jpg';
			$viewfoto = $url_perpus.trim($_SESSION['anggotatamu']).'.jpg';//$url."perpus/images/perpustakaan/anggota/".trim($_SESSION['anggotatamu']).'.jpg';	
		}elseif($cekA['kdjenisanggota'] == "A" or $cekA['kdjenisanggota'] == "M"){#Alumni, Mahasiswa
			#akad
			$mhs = $conna->GetRow("select substring(periodemasuk,1,4) angkatan, mhsid, nim from akademik.ms_mahasiswa where nim = '".trim($_SESSION['anggotatamu'])."'");

			$fold = $mhs['angkatan']."/".trim($_SESSION['anggotatamu']).'.jpg';
			$dirfoto = $dir_akad.$fold;//$dir."siakad/siakad/uploads/fotomhs/".$fold;
			if(!(is_file($dirfoto))){ 
				$fold = "mahasiswa/".md5($mhs['mhsid'].'asdf1234').'.jpg';
				$dirfoto = $dir_akad.$fold;//$dir."siakad/siakad/uploads/fotomhs/".$fold;
			}
			$viewfoto = $url_akad.$fold;//$url."/siakad/siakad/uploads/fotomhs/".$fold;
		}elseif($cekA['kdjenisanggota'] == "D" or $cekA['kdjenisanggota'] == "T"){#Dosen, Karyawan
			$dirfoto = $dir_sdm.trim($cekA['idpegawai']).'.jpg';//$dir."sdm/sdm/up_l04ds/fotopeg/".trim($cekA['idpegawai']).'.jpg';
			$viewfoto = $url_sdm.trim($cekA['idpegawai']).'.jpg';//$url."sdm/sdm/up_l04ds/fotopeg/".trim($cekA['idpegawai']).'.jpg';
		}

		if(is_file($dirfoto)){
			$v_foto = $viewfoto."?".mt_rand(1000,9999);
		}else{
			$v_foto = $url_perpus."default1.jpg"."?".mt_rand(1000,9999);//$url."perpus/images/perpustakaan/default1.jpg";
		}
	}
			
	$berita = $conn->Execute("select judulberita, isiberita from pp_berita where tglexpired>=current_date");
	
	$sqlj = $conn->Execute("select idtamu,a.namaanggota,date(tanggal) as tglkunjungan,tanggal as jam, b.idanggota,j.kdjurusan,j.namajurusan,
			       f.namafakultas, o.namajenisanggota  
				from pp_bukutamu b
				left join ms_anggota a on a.idanggota=b.idanggota
				left join lv_jurusan j on j.kdjurusan=b.kdjurusan
				left join lv_fakultas f on f.kdfakultas=j.kdfakultas
				left join lv_jenisanggota o on o.kdjenisanggota=a.kdjenisanggota
				where to_char(tanggal,'yyyy-mm-dd') = to_char(current_date,'yyyy-mm-dd')
				order by idtamu desc
				limit 10");
	//$jumlah = $sqlj->RowCount();
	$jumlah = $conn->GetOne("select count(*) as jumlah_kunjungan from pp_bukutamu where to_char(tanggal,'yyyy-mm-dd') = to_char(current_date,'yyyy-mm-dd')");
	
	$sql_total = $conn->GetRow("select count(*) as jumlah_kunjungan from pp_bukutamu");
	
?>
<html>
<head>
<title>[ PJB LIBRARY ] Buku Tamu Pengunjung</title>
<script type="text/javascript" src="../scripts/jquery.js"></script>
<!-- <script type="text/javascript" src="scripts/jquery.tools.min.js"></script> -->
<script type="text/javascript" src="../scripts/jquery.dimensions.js"></script>
<script type="text/javascript" src="../scripts/jquery.positionBy.js"></script>
<script type="text/javascript" src="../scripts/jquery.jdMenu.js"></script>
<script type="text/javascript" src="../scripts/jquery.menu.js"></script>
<link href="../style/jquery.jdMenu.css" rel="stylesheet" type="text/css">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<link href="../style/menu.css" type="text/css" rel="stylesheet">
<link href="../style/pager.css" type="text/css" rel="stylesheet">
<link href="../style/officexp.css" type="text/css" rel="stylesheet">
<link href="../style/button.css" rel="stylesheet">
<link href="../style/forum.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="../images/favicon.ico" />	
<script type="text/javascript" src="../scripts/forpager.js"></script>
<script type="text/javascript" src="../scripts/foredit.js"></script>
</head>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="document.getElementById('idanggota').focus()">

<div class="WorkHeader">
	<div style="width:1010px;margin:0 auto;">
		<div style="color:#fff;float:left;margin-top:10px;"> 
			User ID: Buku Tamu Pengunjung <span style="color:#fff;">|</span>
			Hak Akses: User <span style="color:#fff;">|</span>
			Tanggal: <?= date('d-m-Y'); ?>
		</div>
		<img src="../images/logosim2.png" style="vertical-align:top;float:right;position:relative;">
			<div style="clear:both;"></div>
	</div>
</div>
</div>

<div align="center">
<form name="perpusform" id="perpusform" method="post">
<div style="width:1010px;margin:0 auto;">
	<br />
	<h1 style="font-size:40px;font-weight:normal;color:#015593;">Selamat Datang</h1>
</div>
<div id="wrapper" style="width:1010px;">
	<div class="SideItem" id="SideItem">
		<? include_once('_notifikasi.php'); ?>
		<table cellpadding="0" cellspacing="0" align="center" width="500"  style="background:#E0FFF3;border:1pt solid #58B793;-moz-border-radius:5px;padding:10px;margin-top:10px;">
			<tr>
				<td width="130"><img border="1" id="imgfoto" src="<?= $v_foto; ?>?<?= mt_rand(1000,9999) ?>" width="110" height="110"></td>
				<td valign="top">
					<!--<b>Masukkan No. Anggota Anda</b><br><input type="text" name="idanggota" id="idanggota" value="<?= $_SESSION['anggotatamu'] ?>"class="ControlStyleT" maxlength="20" size="35" onkeydown="etrCek(event);" placeholder="Masukan Nomer.."><br>-->
					<b>Masukkan No. Anggota Anda</b><br><input type="text" name="idanggota" id="idanggota" class="ControlStyleT" maxlength="20" size="35" onkeydown="etrCek(event);" placeholder="Masukan Nomer.."><br>
					<?php if($cekA['namaanggota']){?><h2><b>Terima Kasih, <?= $cekA['namaanggota'] ?> </b></h2>
					<b>Anda pengunjung ke : <?= $jumlah; ?></b><?php } ?> <br> <b>Total Kunjungan Hingga Hari ini: <?=$sql_total['jumlah_kunjungan']?></b><br />
				</td>
				<td style="display:none"><input type="button" name="cek" id="cek" value="Masuk" onclick="goCek()" class="buttonSmall" style="padding-bottom:3px;cursor:pointer;height:25px;width:130px;font-size:12px;"></td>
			</tr>
		</table><br>
		<? if($_SESSION['anggotatamu']!='') { ?>
		<? if($tenggat->fields['noseri']!='') { ?>
		<table cellpadding="0" cellspacing="0" width="600" class="tborder">
		<tr class="catbg3" height=20>
			<td width="80" style="font-size:75%;">NO. INDUK</td>
			<td style="font-size:75%;">JUDUL</td>
			<td width="90" style="font-size:75%;" align="center">NO PANGGIL</td>
			<td width="90" style="font-size:75%;" align="center">TGL PINJAM</td>
			<td width="90" style="font-size:75%;" align="center">TGL TENGGAT</td>
		</tr>
		<? while($row=$tenggat->FetchRow()){ ?>
		<tr height="18">
			<td>&nbsp; &nbsp;<?= $row['noseri'] ?></td>
			<td><?= $row['judul'] ?></td>
			<td>&nbsp; &nbsp; &nbsp;<?= $row['nopanggil'] ?></td>
			<td align="center"><?= Help::tglEng($row['tgltransaksi']) ?></td>
			<td align="center"><?= $row['tgltenggat']=='' ? 'Maximal' : Help::tglEng($row['tgltenggat']) ?></td>
		</tr>


		<? } ?>
		</table><br>
		<?}} ?>
		
		<header style="width:780px;">
			<div class="inner">
				<div class="left title">
					<img id="img_workflow" width="24px" src="../images/BIODATA.png" alt="" onerror="loadDefaultActImg(this)" />
					<h1>Daftar Pengunjung</h1>
				</div>
			</div>
		</header>
		<table cellpadding="3" cellspacing="0" width="600" class="tborder GridStyle" style="margin-bottom:20px;">
		<tr class="catbg3" height=20>
			<td align="left" width="80" style="font-size:75%;">Tanggal</td>
			<td align="left" width="80" style="font-size:75%;">Jam</td>
			<td align="left" width="90" style="font-size:75%;" align="center">No. Anggota</td>
			<td align="left" width="250" style="font-size:75%;" align="center">Nama Anggota</td>
			<td align="left" width="150" style="font-size:75%;" align="center">Fakultas/Jurusan</td>
		</tr>
		<? while($isi_buku=$sqlj->FetchRow()){ ?>
		<tr height="18">
			<td align="left"><?= Help::tglEng($isi_buku['tglkunjungan']) ?></td>
			<td align="left"><?= substr($isi_buku['jam'],11,8); ?></td>
			<td align="left"><?= $isi_buku['idanggota'] ?></td>
			<td align="left" nowrap><?= Help::tglEng($isi_buku['namaanggota']) ?></td>
			<td align="left" nowrap><?= $isi_buku['namafakultas']? $isi_buku['namafakultas']." / ".$isi_buku['namajurusan'] : $isi_buku['namajenisanggota']; ?></td>
		</tr>
		<?}?>
		<tr>
			<td colspan="5" class="FootBG" align="right">&nbsp;</td>
		</tr>
		</table>

		<input type="hidden" name="act" id="act" value="<?= $r_aksi ?>">
		<input type="text" name="test" id="test" style="visibility:hidden">
		
	</div>
</div>
<input type="hidden" name="lokasi" id="lokasi" value="<?= $r_key ?>">

</form>
</div>
<!--<div style="border:1pt solid #adad68;position:fixed;display:block;bottom:40px;width:200px;padding:10px;background:#ffffba;-moz-border-radius-topright:4px;">
	<b>Anda pengunjung ke : <?= $jumlah; ?></b>
</div>-->
<div id="newsz" style="position:fixed;display:block;bottom:0px;width:100%;height:40px;border:1pt solid #58B793;background:#E0FFF3;color:000;">
<marquee behavior="scroll" direction="up" scrollamount="2" width="100%" height="40" vspace="5" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 2, 0);">
<center>
<? if($berita->fields['judulberita']!=''){
while($rowb=$berita->FetchRow()){
	echo "<b>".$rowb['judulberita']."</b><br>".Help::limitS($rowb['isiberita'],500)."<br><br>";
}
} ?>
</center>
</marquee>

</div>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

if (parseInt(navigator.appVersion)>3) {
 if (navigator.appName=="Netscape") {
  winW = window.innerWidth-16;
  winH = window.innerHeight-16;
 }
 if (navigator.appName.indexOf("Microsoft")!=-1) {
  winW = document.body.offsetWidth-20;
  winH = document.body.offsetHeight-20;
 }
}
var tengah = (winW/3) + (winW*5/100);
document.getElementById('news').style.paddingLeft = tengah + 'px';
var tinggi = document.getElementById('perpusform').offsetHeight;
document.getElementById('perpusform').style.height = (tinggi+100) + 'px';
</script>
<script type="text/javascript">
	function goCek(){
		document.getElementById('act').value="cek";
		goSubmit();
	}
	
function etrCek(e) {
	var ev= (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;

	if (key == 13)
		document.getElementById("cek").click();
}
</script>
<script type="text/javascript">
$(".subnav").hover(function() {
    $(this.parentNode).addClass("borderbottom");
}, function() {
    $(this.parentNode).removeClass("borderbottom");
});

</script>
</script>
</html>
<? Help::clearFlashData();?>