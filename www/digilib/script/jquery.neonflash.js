/*
 * JQuery "Flashing neon sign" Plugin
 * 
 * Version 1.0
 * 
 * by Christian Bruun - 25. jan 2009
 * 
 * Like it/use it? Send me an e-mail: rockechris@rockechris.com
 * 
 * License: None. Use and abuse. Comes with no warranty, of course!
 * 
 * Usage:
 * $('div/img/etc').neonFlash({ options });
 *  
 * Options: 
 * infinity:	blink until page is unloaded? overrides initialWait, blinks and pause
 * initialWait:	time to wait before first blink
 * blinks:		number of blinks (off/on)
 * pause:		pause length
 * 
 */
(function($) { 		
		$.fn.blink = function (){
			tiss = $(this);
			opp = $(tiss).css('opacity') == 0 ? 1 : 0;
			$(tiss).css('opacity', opp);
			setTimeout(function(){
				$(tiss).blink()
			}, Math.floor(Math.random()*(Math.floor(Math.random()*1500))));
		}
		
		$.fn.neonflash = function(options) {
        var defaults = {
			initialWait:	500,
			blinks:			10,
			pause:			10,
			infinity:		false
        }
        var opts = $.extend(defaults, options);		
		
        return this.each(function() {
			var theFlasher = this;
			if (opts.infinity) {
				$(theFlasher).blink();
			}
			else {

				var theArray = new Array(opts.blinks);
				var theFlasher;
				var counter = 0;
				
				var blinkOn = function(){
					$(theFlasher).css('opacity', 1);
					counter++;
					if (counter <= theArray.length) 
						window.setTimeout(blinkOff, theArray[counter]);
				}
				var blinkOff = function(){
					$(theFlasher).css('opacity', 0);
					window.setTimeout(blinkOn, opts.pause);
				}
				var sortNumber = function(a, b){
					return a - b;
				}
				
				$(theFlasher).css('opacity', 0);
				
				var multiplicator = 1000;
				for (i = 0; i < theArray.length; i++) {
					theArray[i] = Math.floor(Math.random() * multiplicator);
				}
				theArray.sort(sortNumber);
				
				window.setTimeout(blinkOn, opts.initialWait);
			}
		});
    }
})(jQuery);

