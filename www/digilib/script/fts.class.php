<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	class FTS {
	
		function cariPustaka($r_keyword, $r_tipe='', $r_jenis='') {
			$p_sqlstr = "select *,p.idpustaka from ms_pustaka p ";
			//$r_word = $r_keyword;
			$r_word = explode(' ',trim(Helper::strOnly($r_keyword)));
			$word_key = '';
			$word_key = '';
			$sql_join_options = '';
			$r_order = $r_keyword;
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x];
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'|';
					}else
						$word_key .= $r_word[$x];
				}
			}
						
			if ($r_tipe == 'T'){ //untuk kategori pencarian topik atau subjek
				$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
									left join lv_topik t on t.idtopik=pt.idtopik ";
				$keyvector = 't.namatopik';
			}
			else if ($r_tipe == 'P'){ //pencarian berdasarkan pengarang
				$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
									left join ms_author ma on ma.idauthor=pa.idauthor ";
				$keyvector = "ma.namadepan||' '||ma.namabelakang";
			}
			else if ($r_tipe == 'K'){ //pencarian berdasarkan keyword
				$keyvector = 'keywords';
			}
			else //default adalah pencarian berdasarkan judul 
				$keyvector = 'judul';
			// if($r_tipe!='P')
			// $sql_where_options = " where similarity ($keyvector, '$r_word') > 0";
			// else
			// $sql_where_options = " where ((similarity ($keyvector, '$r_word') > 0 ) or (similarity (p.authorfirst1||' '||p.authorlast1||' '||p.authorfirst2||' '||p.authorlast2||' '||p.authorfirst3||' '||p.authorlast3, '$r_word') > 0.1)) ";
				
				if($r_tipe!='P')
				$sql_where_options = "where to_tsvector($keyvector) @@ to_tsquery('$word_key')";
				else
				$sql_where_options = "where ((to_tsvector(f_replace($keyvector)) @@ to_tsquery('$word_key')) or (to_tsvector(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) @@ to_tsquery('$word_key')))";
				
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			
			if ($r_tipe == 'J')
				$sql_where_options .= " and p.kdjenispustaka='$r_jenis'";
			
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			
			$sql_where_options .=" order by p.kdjenispustaka,ts_rank(to_tsvector(f_replace($keyvector)),to_tsquery('$word_key')) desc ";
			
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
		function cariPustakata($r_keyword, $r_tipe='') {
			$p_sqlstr = "select *,p.idpustaka as idpus from ms_pustaka p ";
			// $r_word = $r_keyword;
			$r_word = explode(' ',trim(Helper::strOnly($r_keyword)));
			$word_key = '';
			$word_key = '';
			$sql_join_options = '';
			$r_order = $r_keyword;
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x];
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'|';
					}else
						$word_key .= $r_word[$x];
				}
			}
						
			if ($r_tipe == 'T'){ //untuk kategori pencarian topik atau subjek
				$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
									left join lv_topik t on t.idtopik=pt.idtopik ";
				$keyvector = 't.namatopik';
			}
			else if ($r_tipe == 'P'){ //pencarian berdasarkan pengarang
				$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
									left join ms_author ma on ma.idauthor=pa.idauthor ";
				$keyvector = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
			}
			else if ($r_tipe == 'K'){ //pencarian berdasarkan keyword
				$keyvector = 'keywords';
			}
			else if ($r_tipe == 'C'){ //pencarian berdasarkan pembimbing / COntributor
				$keyvector = 'pembimbing';
			}
			else //default adalah pencarian berdasarkan judul 
				$keyvector = 'judul';
			
			//$sql_where_options = " where similarity ($keyvector, '$r_word') > 0.1 ";
			$sql_where_options = "where to_tsvector($keyvector) @@ to_tsquery('$word_key')";
			
			
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka<>'JN') and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			$sql_where_options .=" order by ts_rank(to_tsvector($keyvector),to_tsquery('$word_key')) desc ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
		function cariJudul($key,$par){
			$p_sqlstr = "select p.*,p.idpustaka as idpus,j.namajenispustaka,j.islokal from ms_pustaka p
						 join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
						 where p.judul like '$key%'";
			if($par==true)
			$p_sqlstr .= " and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka <> 'JN')";
			$p_sqlstr .= " and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			
			$p_sqlstr .= " order by j.kdjenispustaka,p.judul ";
			return $p_sqlstr;
		}
		
		function cariAuthor($key,$par){
			$p_sqlstr = "select p.*,p.idpustaka as idpus,j.namajenispustaka,j.islokal from ms_pustaka p
						 join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
						 where upper(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')) like upper('$key%')";
			if($par==true)
			$p_sqlstr .= " and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka <> 'JN')";
			$p_sqlstr .= " and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			$p_sqlstr .= " order by j.kdjenispustaka,p.authorfirst1 ";
			return $p_sqlstr;
		}
		
		function getIdPustaka($r_keyword, $r_tipe='') {
			$p_sqlstr = "select p.idpustaka from ms_pustaka p ";
			$r_word = $r_keyword;
			// $word_key = '';
			
			// for ($x=0;$x < count($r_word);$x++){
				// if ($x == count($r_word) - 1)
					// $word_key .= $r_word[$x];
				// else{
					// if (strlen(trim($r_word[$x])) > 0){
						// $word_key .= $r_word[$x].'|';
					// }else
						// $word_key .= $r_word[$x];
				// }
			// }
						
			if (empty($r_tipe)){
				$sql_where_options = "where similarity (judul, '$r_word') > 0.1 ";
			}else{
				if ($r_tipe == 'P')
					$sql_where_options = "left join pp_author a on a.idpustaka=p.idpustaka 
										left join ms_author m on m.idauthor=a.idauthor 
										where similarity (namadepan, '$r_word') > 0.1 
										or similarity (namabelakang, '$r_word') > 0.1 ";
			}
			$sql_where_options .=" order by similarity ($keyvector, '$r_word') desc ";
			return $p_sqlstr.$sql_where_options;
		}
		
		function cariPustakaADV($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2, $r_jenis1='', $r_jenis2='')
		{
			$p_sqlstr = "select *,p.idpustaka as idpustaka from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			// $r_word1 = $r_keyword1;
			// $r_word2 = $r_keyword2;
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			//untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'|';
			}
			
			//untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'|';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
							||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'judul';
			}
			
			if (empty($r_keyword2)){
					
				//$sql_where_options = " where similarity ($keyvector1, '$r_word1') > 0.1 ";
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1')";
				
				if (!empty($r_jenis1))
					$sql_where_options .= "and p.kdjenispustaka='$r_jenis1' ";
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
								||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'judul';
				}
				else
				    $keyvector2 = 'judul';
			
				//$sql_where_options = "where similarity ($keyvector1, '$r_word1') > 0.1 ".$r_bool." similarity ($keyvector2, '$r_word2') > 0.1";
				if (!empty($r_jenis1) and $r_jenis2 == '')
					$sql_where_options .= "where (to_tsvector($keyvector1) @@ to_tsquery('$word_key1') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (to_tsvector($keyvector2) @@ to_tsquery('$word_key2'))";
				else if (!empty($r_jenis1) and !empty($r_jenis2))
					$sql_where_options .= "where (to_tsvector($keyvector1) @@ to_tsquery('$word_key1') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (to_tsvector($keyvector2) @@ to_tsquery('$word_key2') and p.kdjenispustaka='$r_jenis2')";
				else
					$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1') ".$r_bool." to_tsvector($keyvector2) @@ to_tsquery('$word_key2')";
			
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			$sql_where_options .=" order by p.kdjenispustaka,ts_rank(to_tsvector($keyvector1),to_tsquery('$word_key1')) desc";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
	function cariPustakaADVta($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2)
		{
			$p_sqlstr = "select *,p.idpustaka as idpus from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			// $r_word1 = $r_keyword1;
			// $r_word2 = $r_keyword2;
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			// untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'|';
			}
			
			// untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'|';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'J'){ //pencarian berdasarkan jenis pustaka
					$keyvector1 = 'jp.namajenispustaka';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else if ($r_topik1 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
				        $keyvector1 = 'pembimbing';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'judul';
			}
			
			if (empty($r_keyword2)){
					
				//$sql_where_options = "where similarity ($keyvector1, '$r_word1') > 0.1";
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1')";
			
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'J'){ //pencarian berdasarkan jenis pustaka
						$keyvector2 = 'jp.namajenispustaka';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else if ($r_topik2 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
						$keyvector2 = 'pembimbing';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'judul';
				}
				    else
					$keyvector2 ='judul';
			
				//$sql_where_options = "where similarity ($keyvector1, '$r_word1') > 0.1 ".$r_bool." similarity ($keyvector2, '$r_word2') > 0.1";
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1') ".$r_bool." to_tsvector($keyvector2) @@ to_tsquery('$word_key2')";
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka <>'JN') ";
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and e.kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			$sql_where_options .=" order by ts_rank(to_tsvector($keyvector1),to_tsquery('$word_key1')) desc ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
	}
?>