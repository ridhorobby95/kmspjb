// JavaScript Document

function linkURL(xdiv, ximage, xurl){
	post = '';	
	$("#" + xdiv).divpost({page: xurl, sent: post});	
	return false;	
}

function goLink(xdiv, zlist, post){
	$("#" + xdiv).divpost({page: zlist, sent: post});	
}

function goLoading(xdiv, zlist, post){
	$("#" + xdiv).divpost({page: zlist, sent: post});	
}

$(document).ready(function() {
						   
	$('#breadcrumb ul li a').click(function(){
		 var loadList = $(this).attr('href');
		 post = '';	
		 $("#contents").divpost({page: loadList, sent: post});
		 return false;
	});

});

/*function crumb(url)
	// var loadList = $(this).attr('href');
	 post = '';	
	 $("#contents").divpost({page: url, sent: post});
}*/

function cfHighlight(csv) {
	var i, err = false;
	var aid = csv.split(",");
	
	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(e != null && e.value.trim() == "") {
				//e.className = "ControlErr";
				//e.onfocus = function () { this.className = "ControlStyle"; }
				err = true;
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(e != null && e.value.trim() == "") {
			//e.className = "ControlErr";
			//e.onfocus = function () { this.className = "ControlStyle"; }
			err = true;
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang bertanda (*) terlebih dahulu.");
		return false;
	}
	return true;
}

function cmail(csv) {
	var i, err = false;
	var aid = csv.split(",");
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  

	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(!e.value.match(mailformat)) {
				err = true;
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(!e.value.match(mailformat)) {
			err = true;
		}
	}
	
	if(err) {
		alert("Mohon mengisi format email dengan benar.");
		return false;
	}
	return true;
}

