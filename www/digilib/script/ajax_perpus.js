var popWindows = new Array();
var gd_mouse_coords;
var g_sevimaNote;

function moveToClick(evt) {
	evt = (evt) ? evt : event;
	gd_mouse_coords = getPageEventCoords(evt);
}

function getPageEventCoords(evt) {
	var coords = {left:0, top:0};
	if (evt.pageX) {
		coords.left = evt.pageX;
		coords.top = evt.pageY;
	} else if (evt.clientX) {
		coords.left = 
			evt.clientX + document.body.scrollLeft - document.body.clientLeft;
		coords.top = 
			evt.clientY + document.body.scrollTop - document.body.clientTop;
		// include html element space, if applicable
		if (document.body.parentElement && document.body.parentElement.clientLeft) {
			var bodParent = document.body.parentElement;
			coords.left += bodParent.scrollLeft - bodParent.clientLeft;
			coords.top += bodParent.scrollTop - bodParent.clientTop;
		}
	}
	return coords;
}

function keyDownHandler(e)	{
    var kC  = (window.event) ?    // MSIE or Firefox?
        event.keyCode : e.keyCode;
    var Esc = (window.event) ?   
        27 : e.DOM_VK_ESCAPE // MSIE : Firefox
    
    if(kC==Esc) {
       closePop();
    }
}

function findPosX(obj) {
	var curleft = 0;
	if (document.getElementById || document.all) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
		curleft += obj.x;
	
	return curleft;
}

function findPosY(obj) {
	var curtop = 0;
	if (document.getElementById || document.all) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
		curtop += obj.y;
	return curtop;
}

function toggleDisplay(id) {
	if (document.getElementById) 
		var elem = document.getElementById(id); 
	else {
		if (document.all ) 
			var elem = document.all[ id ]; 
		else 
			var elem = new Object();
	}
	if (!elem )  
		return; 
	if (elem.style ) 
		elem = elem.style;
	if (typeof( elem.display ) == 'undefined' && !(window.ScriptEngine && ScriptEngine().indexOf( 'InScript' ) + 1)) { 
		window.alert( 'Hidden does not work in this browser.' ); 
		return; 
	}

	if (elem.display == 'none') 
		elem.display = 'block';
	else 
		elem.display = 'none';
}	

function showBlock(block_id) {
	$("#"+block_id).show();
}

function hideBlock(block_id) {
	$("#"+block_id).hide();
}

function parseAjaxVal(ret) {
	sep1 = '_##_';
	sep2 = '__|__';
	sep3 = '::';

	pairs = ret.split(sep1);
	for (i=0;i<pairs.length;i++) {
		pair = pairs[i].split(sep3);
		
		if (pair[1]) {
			val = pair[1].split(sep2);
			typeVal = val[0];
			inputVal = val[1];
			if (typeVal == 'HTML')
				$("#"+pair[0]).html(inputVal);
			else if (typeVal == 'VAL')
				$("#"+pair[0]).val(inputVal);
			else if (typeVal == 'CHK') {
				if (inputVal == '1')
					$("#"+pair[0]).attr('checked',true);
				else
					$("#"+pair[0]).attr('checked',false);
			}
		}
		
	}
}

function changeRowChecked(rowid, chkid, alt) {
	if ($("#"+chkid).attr('checked')) {
		$("#"+rowid).attr('class','checked');
	}
	else 
		$("#"+rowid).attr('class',alt);
}
 
function closePop() {
	block_id = popWindows.pop();
	if (block_id)
		$("#"+block_id).hide();
}

var dtCh= "-";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("Format tanggal harus : dd-mm-yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Format tanggal: nilai bulan salah")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Format tanggal: nilai tanggal salah")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Format tanggal: tahun harus di antara "+minYear+" dan "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Format tanggal salah")
		return false
	}
	return true
}

function delQuote(str){
	$ret =str.replace(/["']{1}/gi,"");
	alert($ret);
	return $ret;
}

function pushPopWindow(block_id) {
	//if ($("#"+block_id).css('display') != 'block') {
		popWindows.push(block_id);
		zindex = popWindows.length;
		$("#"+block_id).css('z-index',100+zindex);
	//}
}

function openLOV(button_id, act, posx, posy, ret_function, width, height, add) {
	var button = document.getElementById(button_id);
	var block_id = "sevimalov_block";
	var block = document.getElementById(block_id);
	
	if (!posx)
		posx = 0;
	if (!posy)
		posy = 0;
	if (!width)
		width = '500px';
	if (!height)
		height = '100px';
	
	pushPopWindow(block_id);
	
	$('#sevimalov_div').html('Loading...');
	
	ajaxurl = "index.php?page=ajax_perpus&act="+act;
	$("#loadinglov").show();
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"ret_function":ret_function,"add":add},
		success: function(ret){
			$("#loadinglov").hide();
			$('#sevimalov_div').html(ret);
		}
	});	
	block.style.left = findPosX(button) + posx + "px";
	block.style.top = findPosY(button) + posy + "px";
	$("#"+block_id).css("width",width);
	$("#"+block_id).css("min-height",height);

	$("#"+block_id).show();	
}

function searchLOV(act,add){
	ajaxurl = "index.php?page=ajax_perpus&act="+act;
	$("#loadinglov").show();
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"entry":1, "keyword":$("#sevimalov_keyword").val(), "add":add},
		success: function(ret){
			$("#loadinglov").hide();
			$('#sevimalov_div').html(ret);
		}
	});
}

function navigateLOV(act,add) {
	ajaxurl = "index.php?page=ajax_perpus&act="+act;
	$("#loadinglov").show();
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"add":add},
		success: function(ret){
			$("#loadinglov").hide();
			$('#sevimalov_div').html(ret);
		}
	});
}

function resetLOV(act,add){
	ajaxurl = "index.php?page=ajax_perpus&act=reset_"+act;
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"add":add},
		success: function(ret){
		}
	});
}

function generateSevimaBlock() {
	jQuery("<div />")
		.attr("id", "sevimalov_block")
		.attr("class", "popWindow")
		.appendTo(jQuery("body"));
		
	$('#sevimalov_block').append('<div id="loadinglov" style="float:left;position:absolute;right:25px;top:3px"><img src="images/loading.gif" /></div>');
	$('#sevimalov_block').append('<div id="sevimalov_div"></div>');

	/*jQuery("<div />")
		.attr("id", "sevimanote_block")
		.attr("class", "popCatatan")
		.appendTo(jQuery("body"));
	$('#sevimanote_block').append('<div id="sevimanote_tr" style="float:right;clear:both"><input type="button" class="btn_close" value="x" onclick="closePop()"/></div>');
	$('#sevimanote_block').append('<div id="sevimanote_div" style="padding:3px"></div>');*/
}

function showCatatan(button_id, posx, posy, width, height) {
	var button = document.getElementById(button_id);
	var block_id = "sevimanote_block";
	var block = document.getElementById(block_id);

	if (!posx)
		posx = -200;
	if (!posy)
		posy = 0;
	if (!width)
		width = '300px';
	if (!height)
		height = '50px';

	pushPopWindow(block_id);
	
	$("#sevimanote_div").html(g_sevimaNote);
	
	block.style.left = findPosX(button) + posx + "px";
	block.style.top = findPosY(button) + posy + "px";
	$("#"+block_id).css("width",width);
	$("#"+block_id).css("min-height",height);

	$("#"+block_id).show();	
}

$(document).ready(function() {
    document.onkeydown = keyDownHandler;
    document.onmousedown = moveToClick;
	generateSevimaBlock();
});

function newPengarang() {
	block_id = 'newpengarang_block';
	button_id = 'btnauthor';

	$("#auth_namadepan").val('');
	$("#auth_namabelakang").val('');

	posx = -300;
	posy = -30;
	pushPopWindow(block_id);

	var button = document.getElementById(button_id);
	var block = document.getElementById(block_id);

	block.style.top = findPosY(button) + posy + "px";
	block.style.left = findPosX(button) + posx + "px";
	$("#"+block_id).show();
}

function newSupplier(id,num) {
	block_id = 'newsupplier_block';
	button_id = 'btnsupplier'+num+'_'+id;

	$("#namasupplier").val('');
	$("#alamat").val('');

	posx = -300;
	posy = -30;
	pushPopWindow(block_id);

	var button = document.getElementById(button_id);
	var block = document.getElementById(block_id);

	block.style.top = findPosY(button) + posy + "px";
	block.style.left = findPosX(button) + posx + "px";
	$("#"+block_id).show();
}

function isnewPengarang() {
	ajaxurl = "index.php?page=ajax_perpus&act=newpengaranglov";
	$("#loadingGif2").show();
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"entry":1, "namadepan":$("#auth_namadepan").val(), "namabelakang":$("#auth_namabelakang").val()},
		success: function(ret){
			$("#loadingGif2").hide();
			if (ret != '-1') {
				$("#sevimalov_keyword").val($("#auth_namadepan").val())
				searchLOV('pengarang');
				closePop();
			}
			else {
				alert('Ada kesalahan data. Mohon diulangi.');
			}
		}
	});	
}

function isnewSupplier() {
	ajaxurl = "index.php?page=ajax_perpus&act=newsupplierlov";
	$("#loadingGif2").show();
	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: {"entry":1, "namasupplier":$("#namasupplier").val(), "alamat":$("#alamat").val()},
		success: function(ret){
			$("#loadingGif2").hide();
			if (ret != '-1') {
				$("#sevimalov_keyword").val($("#namasupplier").val())
				searchLOV('supplier');
				closePop();
			}
			else {
				alert('Ada kesalahan data. Mohon diulangi.');
			}
		}
	});	
}