(function($){
	$.fn.divpost = function(options) {
		var settings = $.extend({
			page: "", sent: ""
		}, options);
		
		return $(this).each(function() {
			xdiv = $(this);
			xdiv.waitload({divid: "progressbar"});
			$.post(settings.page,settings.sent,function(text) {
				xdiv.waitload({divid: "progressbar", mode: "unload"});
				xdiv.html(text);
			});
		});
	};
	
		
	$.fn.waitload = function(options) {
		var settings = $.extend({
			divid: "roller", mode: "load"
		}, options);
		
		return $(this).each(function() {
			div = $("#" + settings.divid);
			
			if(settings.mode == "load") {
				$(this).css("opacity",0.5);
				$(this).css("filter","alpha(opacity=50)");
				
				ctrofs = $(this).offset();
				divleft = ctrofs.left + ($(this).width()/2) - (div.width()/2);
				divtop = ctrofs.top;
				
				div.css("left",divleft);
				div.css("top",divtop);
				div.css("visibility","visible");
			}
			else { // sebenarnya unload
				$(this).css("opacity",1);
				$(this).css("filter",null);
				div.css("visibility","hidden");
			}
		});
	};
	
	$.fn.onlyNum = function(options) {
		var settings = $.extend({
			point: true
		}, options);
		
		$(this).keydown(function (e) {
			code = e.keyCode || e.which;
			if ((code > 57 && code < 96) || code > 105 || code == 32) {
				if((code == 190 || code == 110) && settings.point) {
					if($(this).val() == "") // belum ada isinya, titik tidak boleh didepan
						return false;
					if($(this).val().indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
						return false;
					return true;
				}
				return false;
			}
		});
	};
	
})(jQuery);