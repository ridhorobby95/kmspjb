(function($){
	var acdivh = 200; // tinggi div autocomplete
	var actdidx, actdnum;
	var strac = new Array();
	
	$.fn.xauto = function(options) {
		var tdpeak, tdmore;
		var settings = $.extend({
			targetid: "", srcdivid: "", imgchkid: "", acdivpos: 1
		}, options);
		
		var srcdiv = $("#"+settings.srcdivid);
		var imgchk_c = $("#"+settings.imgchkid+"_c");
		var imgchk_u = $("#"+settings.imgchkid+"_u");
		
		$("*:not(#tab_autocomplete td)").click(function(e) {
			$("#div_autocomplete").hide();
		});
		
		// membuat 'Contains', case insensitive 'contains' jQuery
		jQuery.expr[':'].Contains = function(a,i,m){
			return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
		};
		
		return $(this).each(function() {
			// karena target bisa beda-beda jadi ditaruh sini
			var target = "";
			if(settings.targetid == "")
				target = $(this);
			else
				target = $("#"+settings.targetid);
			
			$(this).attr("autocomplete","off");
			$(this).attr("class","ControlStyleAC");
			if(target.val() != "") {
				strac[target.attr("id")] = $(this).val();
				imgchk_c.show();
				imgchk_u.hide();
			}
			
			$(this).keyup(function(e) {
				if($(this).val() != "" && (strac[target.attr("id")] != "" && $(this).val() != strac[target.attr("id")]))
				{
					$(this).attr("class","ControlStyleAC");
					imgchk_c.hide();
					imgchk_u.show();
					
					if(settings.targetid != "")
						target.val("");
				}
				else if($(this).val() == "")
				{
					$(this).attr("class","ControlStyleAC");
					imgchk_c.hide();
					imgchk_u.hide();
					
					if(settings.targetid != "")
						target.val("");
				}
				
				if((e.keyCode < 38 || e.keyCode > 40) && e.keyCode != 13) {
					showAutoComplete($(this),target,srcdiv,imgchk_c,imgchk_u,settings.acdivpos);
				}
			});
			
			$(this).keydown(function(e) {
				if(e.keyCode == 40 || e.keyCode == 38) {
					if(e.keyCode == 40 && actdidx < (actdnum-1)) {
						actdidx++;
						tdpeak = $("#tab_autocomplete td:eq("+actdidx+")").offset().top + $("#tab_autocomplete td:eq("+actdidx+")").height();
						tdmore = tdpeak - $("#div_autocomplete").offset().top;
						if(tdmore > acdivh)
							$("#div_autocomplete").get(0).scrollTop += (tdmore-acdivh);
					}
					else if(e.keyCode == 38 && actdidx > 0) {
						actdidx--;
						tdpeak = $("#tab_autocomplete td:eq("+actdidx+")").offset().top;
						if(tdpeak < $("#div_autocomplete").offset().top && $("#div_autocomplete").get(0).scrollTop > 0)
							$("#div_autocomplete").get(0).scrollTop -= ($("#div_autocomplete").offset().top - tdpeak);
					}
					else if(e.keyCode == 38 && actdidx < 0 && settings.acdivpos == 2)
						actdidx++;
					updateAutoCompleteLight();
				}
				else if((e.keyCode == 39 || e.keyCode == 13) && actdidx >= 0) {
					execAutoCompleteLight($(this),target,imgchk_c,imgchk_u);
					return false;
				}
			});
		});
	};
	
	function showAutoComplete(jqtbox,jqkode,jqdiv,jqimgc,jqimgu,acdivpos) {
		var toffset;
		var srch = jqtbox.val();
		
		actdidx = -1;
		
		$("#tab_autocomplete").empty();
		if($("#tab_autocomplete").width() < jqtbox.width())
			$("#tab_autocomplete").width(jqtbox.width());
		
		actdnum = 0;
		jqdiv.children("p:Contains('"+srch+"')").each(function() {
			$("#tab_autocomplete").append('<tr><td id="'+$(this).attr("id")+'" nowrap style="cursor:pointer">'+$(this).html()+'</td></tr>');
			actdnum++
		});
		
		if(actdnum > 0) {
			$("#tab_autocomplete td").mouseover(function() {
				actdidx = $("#tab_autocomplete td").index($(this));
				updateAutoCompleteLight();
			});
			
			$("#tab_autocomplete td").click(function() {
				strac[jqkode.attr("id")] = jQuery.trim($(this).text());
				jqtbox.val(jQuery.trim($(this).text()));
				jqkode.val($(this).attr("id"));
				jqtbox.attr("class","ControlStyleAC");
				if(jqimgc)
					jqimgc.show();
				if(jqimgu)
					jqimgu.hide();
				$("#div_autocomplete").hide();
			});
			
			toffset = jqtbox.offset();
			$("#div_autocomplete").css("left",toffset.left+1);
			if(acdivpos == 1)
				$("#div_autocomplete").css("top",toffset.top+jqtbox.height()+5);
			$("#div_autocomplete").show();
			
			$("#div_autocomplete").get(0).scrollTop = 0;
			if($("#tab_autocomplete").height() > acdivh) {
				$("#div_autocomplete").height(acdivh);
				$("#div_autocomplete").width($("#tab_autocomplete").width()+19);
				if(acdivpos == 2)
					$("#div_autocomplete").css("top",toffset.top-acdivh-5);
			}
			else {
				$("#div_autocomplete").height($("#tab_autocomplete").height());
				$("#div_autocomplete").width($("#tab_autocomplete").width());
				if(acdivpos == 2)
					$("#div_autocomplete").css("top",toffset.top-$("#tab_autocomplete").height()-5);
			}
		}
		else
			$("#div_autocomplete").hide();
	}
	
	function updateAutoCompleteLight() {
		$("#tab_autocomplete td").css("background-color","#FFFFFF");
		$("#tab_autocomplete td:eq("+actdidx+")").css("background-color","#C4CDE0");
	}
	
	function execAutoCompleteLight(jqtbox,jqkode,jqimgc,jqimgu) {
		var tdpilih = $("#tab_autocomplete td:eq("+actdidx+")");
		
		strac[jqkode.attr("id")] = jQuery.trim(tdpilih.text());
		jqtbox.val(jQuery.trim(tdpilih.text()));
		jqkode.val(tdpilih.attr("id"));
		jqtbox.attr("class","ControlStyleAC");
		if(jqimgc)
			jqimgc.show();
		if(jqimgu)
			jqimgu.hide();
		$("#div_autocomplete").hide();
	}
})(jQuery);