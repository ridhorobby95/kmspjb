// JavaScript Document
function onlyNumber(e,elem,dec) {
	var code = e.keyCode || e.which;
	if ((code > 57 && code < 96) || code > 105 || code == 32) {
		if(code == 190 && dec) {
			if(elem.value == "") // belum ada isinya, titik tidak boleh didepan
				return false;
			if(elem.value.indexOf(".") > -1) // udah ada titik, tidak boleh ada lagi
				return false;
			return true;
		}
		return false;
	}
}

function goSubmit(xdiv,file,sent) {
	goLink(xdiv,file, sent);
}

/* fungsi aksi */

function goDelete() {
	var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
	if(hapus) {
		document.getElementById("act").value = "hapus";
		goSubmit();
	}
}

function goSave(xdiv,file,sent) {
	sent = sent;
	sent += $("#perpusform").serialize();
	sent += "&act=simpan";
	goSubmit(xdiv,file,sent);
}

function goEdit(xdiv,file,sent) {
	goSubmit(xdiv,file,sent);
}

function goDelete(xdiv,file,sent) {
	var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
	if(hapus) {
		sent = sent;
		sent += "&act=hapus";
		goSubmit(xdiv,file,sent);
	}
}

function cfHighlight(csv) {
	var i, err = false;
	var aid = csv.split(",");
	
	if(aid.length > 1) {
		for(i=0;i<aid.length;i++) {
			e = document.getElementById(aid[i]);
			if(e != null && e.value == "") {
				e.className = "ControlErr";
				e.onfocus = function () { this.className = "ControlStyle"; }
				err = true;
			}
		}
	}
	else {
		e = document.getElementById(aid);
		if(e != null && e.value == "") {
			e.className = "ControlErr";
			e.onfocus = function () { this.className = "ControlStyle"; }
			err = true;
		}
	}
	
	if(err) {
		alert("Mohon mengisi isian-isian yang berwana kuning terlebih dahulu.");
		return false;
	}
	return true;
}

function goUndo() {
	document.getElementById("perpusform").reset();
}

function popup(url,width,height) 
{
 var left   = (screen.width  - width)/2;
 var top    = (screen.height - height)/2;
 var params = 'width='+width+', height='+height;
 params += ', top='+top+', left='+left;
 params += ', directories=no';
 params += ', location=no';
 params += ', menubar=no';
 params += ', resizable=no';
 params += ', scrollbars=yes';
 params += ', status=no';
 params += ', toolbar=no';
 newwin=window.open(url,'windowname5', params);
 if (window.focus) {newwin.focus()}
 return false;
}