<?php
	//die("Maaf, kami sedang dalam proses maintenance server. Untuk Sementara alamat ini tidak bisa diakses");
	define( '__VALID_ENTRANCE', 1 );


	$site_path = realpath(dirname(__FILE__));
	define ('__SITE_PATH', $site_path);

	require_once('includes/init.php');

	$pageurl = '';

	if(isset($_GET['page']))
	{
		$pageurl = Helper::cAlphaNum($_GET['page'],'_');


		if (is_readable(__SITE_PATH.Config::pathSeparator.Config::pagePath.Config::pathSeparator.$pageurl.'.php') == false)
			$pageurl = Config::pageDef;
	}
	else
		$pageurl = Config::pageDef;

	$phpfile = $pageurl.'.php';

		$i_phpfile = Helper::navAddress($phpfile);

	define('__PAGEURL', $pageurl);

	require_once(Config::pagePath.'/'.$phpfile);

	Helper::clearFlashData();



	$conn->Close();
?>
