<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

require_once('config.class.php');
require('classes/google.translator.class.php');

class Helper {
	/* FUNGSI SISTEM */
	
	// dekripsi untuk pengiriman data
	function decrypt($c_t, $key) {
		$c_t = base64_decode(strtr($c_t,'-_~', '+/='));
		$iv = substr(md5($key), 0,mcrypt_get_iv_size (MCRYPT_CAST_256,MCRYPT_MODE_CFB));
		$p_t = mcrypt_cfb (MCRYPT_CAST_256, $key, $c_t, MCRYPT_DECRYPT, $iv);
		return trim($p_t);
	}
	
	// enkripsi untuk pengiriman data
	function encrypt($plain_text, $key) {
		$plain_text = trim($plain_text);
		$iv = substr(md5($key), 0,mcrypt_get_iv_size (MCRYPT_CAST_256,MCRYPT_MODE_CFB));
		$c_t = mcrypt_cfb (MCRYPT_CAST_256, $key, $plain_text, MCRYPT_ENCRYPT, $iv);
		return strtr(base64_encode($c_t),'+/=', '-_~');
	}
	
	// mendapatkan data session
	function getSessionData($data) {
		$data = urldecode($data);
		$data = substr($data, 0, strlen($data) - 1);
		$arr_data = explode(";",$data);
		
		$return = array();
		while (list ($key, $val) = each ($arr_data)) {
			$arr_str = explode("|", $val);
	
			$val3 = $arr_str[1];
			if (!preg_match("/a:/i", $val3)) {
				$val3 = str_replace(array("{", "}"),"",$val3);
				if (preg_match("/s:/i", $val3)) {
					$val3 = unserialize($val3);
				}
				else if (preg_match("/i:/i", $val3)) {
					$val3 = (int)substr($val3,2);
				}
			}
			
			$return[$arr_str[0]] = $val3;
		}
		
		return $return;
	}
	
	// membentuk url dengan satu pintu
	function navAddress($url) {
		if(strcasecmp(substr($url,-4),'.php') == 0)
			$url = substr($url,0,strlen($url)-4);
		return Config::webUrl.'?page='.$url;
	}
	
	// menuju suatu url dengan satu pintu
	function navigate($url) {
		Helper::redirect(Config::webUrl.'?page='.$url);
		exit;
	}
	
	// menuju gate
	function navigateOut() {
		Helper::redirect(Config::gateUrl);
		//Helper::redirect(Config::sysout);
		//Helper::redirect(Config::KMHOME);
		exit;
	}
	
	// normalisasi request
	function normalizeRequest() {
		if(get_magic_quotes_gpc()) {
			if(isset($_GET))
				$_GET=Helper::stripSlashes($_GET);
			if(isset($_POST))
				$_POST=Helper::stripSlashes($_POST);
			if(isset($_REQUEST))
				$_REQUEST=Helper::stripSlashes($_REQUEST);
			if(isset($_COOKIE))
				$_COOKIE=Helper::stripSlashes($_COOKIE);
		}
	}
	
	// this page, halaman ini
	function recentURL($includeParams=false)
	{
		if ($includeParams)
			$http = $_SERVER["REQUEST_URI"];
		else
			$http = $_SERVER["PHP_SELF"];
		return htmlentities(strip_tags($http));
	}
	
	// redirect ke halaman lain
	function redirect($url=NULL) {
		if (!$url) {
				$currentFile = $_SERVER['PHP_SELF'];
				$parts = Explode('/', $currentFile);
				$url = $parts[count($parts) - 1] . '?' . $_SERVER['QUERY_STRING'];
			}
			header("Location: $url");
		exit;
	}
	
	// melakukan stripslashes
	function stripSlashes(&$data) {
		return is_array($data)?array_map(array('Helper','stripSlashes'),$data):stripslashes($data);
	}
	
	/*FUNGSI MENGOLAH TANGGAL */
	/* Get count of years between dates */
	function yearsBetween($dateStart, $dateEnd) {
	   return date('Y', $dateEnd) - date('Y', $dateStart);
	}
	
	function monthsBetween($dateStart, $dateEnd) {
	   return date('n', $dateEnd) + (Helper::yearsBetween($dateStart, $dateEnd) * 12) - date('n', $dateStart);
	}
	
	
	/* FUNGSI MANIPULASI STRING */
	
	// strip selain alfanumerik
	function cAlphaNum($item,$allow='') {
		return preg_replace('/[^a-zA-Z0-9'.$allow.']/', '$1', $item);
	}
	
	// strip selain numerik
	function cNum($item) {
		return (float)preg_replace('/[^0-9]/', '$1', $item);
	}
	
	// untuk elemen array, lakukan cStrNull
	function cStrFill($src,$elem='',$idx=false) {
		if(is_array($elem)) {
			$retarr = array();
			for($i=0;$i<count($elem);$i++) {
				if(is_array($src[$elem[$i]])) {
					if($idx === false)
						$retarr[$elem[$i]] = Helper::cStrFill($src[$elem[$i]]);
					else
						$retarr[$elem[$i]] = Helper::cStrNull($src[$elem[$i]][$idx]);
				}
				else
					$retarr[$elem[$i]] = Helper::cStrNull($src[$elem[$i]]);
			}
		}
		else {
			$retarr = array();
			foreach($src as $key => $val) {
				if(is_array($val)) {
					if($idx === false) 
						$retarr[$key] = Helper::cStrFill($val);
					else 
						$retarr[$key] = Helper::cStrNull($val[$idx]);
				}
				else
					$retarr[$key] = Helper::cStrNull($val);
			}
		}
		
		return $retarr;
	}
	
	// format penulisan bilangan juga uang
	function formatNumber($num,$ndec='',$ismoney=false,$format=true) {
		if($num == '')
			return $num;
		
		if($ndec == '') {
			if($ismoney)
				$ndec = 2;
			else
				$ndec = 0;
		}
		
		if($num < 0) {
			$num = abs($num);
			$bracket = true;
		}
		
		// untuk misalnya 100.000 jadi 100000, tapi desimal disederhanakan dulu
		$num = round($num,2);
		$num = preg_replace('/\.(\d{3})/','${1}',$num);
		
		if($format)
			$num = number_format($num,$ndec,',','.');
		
		if($ismoney)
			$num = 'Rp. '.$num;
		
		if($bracket)
			return '('.$num.')';
		else
			return $num;
	}
	
	// format penulisan bilangan juga uang pada laporan
	function formatNumberRep($num,$format='html',$ndec='') {
		if($format == 'xls')
			return Helper::formatNumber($num,$ndec,false,false);
		else
			return Helper::formatNumber($num,$ndec,false);
	}
	
	// format menjadi bentuk bilangan
	function cStrDec($str,$ndec='',$convpoint=true) {
		if($str == '' or $str == 'null')
			return $str;
		
		if($convpoint)
			$str = str_replace('.','',$str);
		$str = str_replace(',','.',$str);
		
		if($ndec == '')
			return (float)$str;
		else
			return round($str,$ndec);
	}
	
	// bila $item kosong, set null, bila tidak, strip
	function cStrNull($item) {
		if($item == '')
			return 'null';
		else
			return Helper::removeSpecial($item);
	}
	
	// digunakan untuk laporan rtf supaya tidak ada kata2 null
	function cStrEmpty($item) {
		if($item == '')
			return '';
		else
			return Helper::removeSpecial($item);
	}
	
	// untuk elemen array, lakukan cStrEmpty
	function cStrFillEmpty($src,$elem='',$idx=false) {
		if(is_array($elem)) {
			$retarr = array();
			for($i=0;$i<count($elem);$i++) {
				if(is_array($src[$elem[$i]])) {
					if($idx === false)
						$retarr[$elem[$i]] = Helper::cStrFillEmpty($src[$elem[$i]]);
					else
						$retarr[$elem[$i]] = Helper::cStrEmpty($src[$elem[$i]][$idx]);
				}
				else
					$retarr[$elem[$i]] = Helper::cStrEmpty($src[$elem[$i]]);
			}
		}
		else {
			$retarr = array();
			foreach($src as $key => $val) {
				if(is_array($val)) {
					if($idx === false) 
						$retarr[$key] = Helper::cStrFillEmpty($val);
					else 
						$retarr[$key] = Helper::cStrEmpty($val[$idx]);
				}
				else
					$retarr[$key] = Helper::cStrEmpty($val);
			}
		}
		
		return $retarr;
	}
	
	// strip selain numerik, bila kosong null
	function cNumNull($item) {
		$item = preg_replace('/[^0-9]/', '$1', $item);
		
		if($item == '')
			return 'null';
		else
			return (float)$item;
	}
	
	// bila $item kosong, return 0, bila tidak, strip
	function cNumZero($item) {
		if($item == '')
			return 0;
		else
			return Helper::removeSpecial($item);
	}
	
	// mengubah format tanggal dari yyyy-mm-dd menjadi format indonesia
	function formatDateInd($ymd,$full=true,$dmy=false,$delim='-') {
		if($ymd == '')
			return '';
		if($ymd == 'null')
			return 'null';
		
		if($dmy)
			list($d,$m,$y) = explode($delim,substr($ymd,0,10));
		else
			list($y,$m,$d) = explode($delim,substr($ymd,0,10));
		
		return (int)$d.' '.Helper::indoMonth($m,$full).' '.$y;
	}
	
	function formatDateEN($ymd,$full=true,$dmy=false,$delim='-') {
		if($ymd == '')
			return '';
		if($ymd == 'null')
			return 'null';
		
		if($dmy)
			list($d,$m,$y) = explode($delim,substr($ymd,0,10));
		else
			list($y,$m,$d) = explode($delim,substr($ymd,0,10));
		
		return Helper::enMonth($m,$full).' '.(int)$d.', '.$y;
	}
	
	// mengubah format tanggal dari dd-mm-yyyy menjadi yyyy-mm-dd dan sebaliknya
	function formatDate($dmy,$delim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		list($y,$m,$d) = explode($delim,substr($dmy,0,10));
		return $d.$delim.$m.$delim.$y;
	}
	
	// mengubah format tanggal, tapi ada time dibelakangnya
	function formatDateTime($dmy,$delim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		return Helper::formatDate(substr($dmy,0,10)).substr($dmy,10);
	}
	
	// cek apakah suatu tanggal termasuk di array interval tanggal
	function cekTanggal($tgl,$arrint) {
		if(empty($arrint))
			return false;
		
		foreach($arrint as $int) {
			$mulai = $int['mulai'];
			$selesai = $int['selesai'];
			
			if($tgl >= $mulai and $tgl <= $selesai)
				return true;
		}
		
		return false;
	}
	
	// mengubah format hhii menjadi hh:ii
	function formatTime($hi,$delim=':') {
		if($hi == '')
			return '';
		
		$hi = str_pad($hi,2,'0',STR_PAD_LEFT);
		return substr($hi,0,2).':'.substr($hi,2,2);
	}
	
	// melakukan string stripping (untuk masalah sekuritas)
	function removeSpecial($mystr,$stripapp=false) {
		// $pattern = '/[%&;()\"';
		$pattern = '/[%&;\"';
		if($stripapp === false) // tidak stripping ', tapi direplace jadi '', biasanya dipakai di nama
			$mystr = str_replace("'","''",$mystr);
		else
			$pattern .= "\'";
		if(preg_match('/[A-Za-z%\/]/',$mystr)) // kalau ada alfabet, %, atau /, strip <>
			$pattern .= '<>';
		$pattern .= ']|--/';
		
		return preg_replace($pattern, '$1', $mystr);
	}
	
	// mengucapkan bilangan ratusan
	function spellNumP($num) {
		$num = strval($num);
		if(!ctype_digit($num))
			return '';
		
		// hanya untuk bilangan sampai ratusan
		$len = strlen($num);
		if($len > 3) {
			$num = substr($num,0,3);
			$len = 3;
		}
		
		// membaca dari belakang
		$spell = '';
		$clen = 1;
		$belas = false;
		for($i=($len-1);$i>=0;$i--) {
			// jika pernah belasan maka continue
			if($belas === true) {
				$clen++;
				$belas = false;
				continue;
			}
			
			$cnum = $num[$i]; // bilangan yang dianalisa
			
			// menyebut bilangan
			$spellb = '';
			if($cnum == '1') {
				if($clen > 1 or ($clen == 1 and $num[$i-1] == 1))
					$spellb = 'se';
				else
					$spellb = 'satu ';
			}
			else if($cnum == '2') $spellb = 'dua ';
			else if($cnum == '3') $spellb = 'tiga ';
			else if($cnum == '4') $spellb = 'empat ';
			else if($cnum == '5') $spellb = 'lima ';
			else if($cnum == '6') $spellb = 'enam ';
			else if($cnum == '7') $spellb = 'tujuh ';
			else if($cnum == '8') $spellb = 'delapan ';
			else if($cnum == '9') $spellb = 'sembilan ';
			
			// jika bukan kosong
			if($spellb != '') {
				// jika belasan
				if($clen == 1 and $num[$i-1] == 1) {
					$spellb .= 'belas ';
					$belas = true;
				}
				
				// puluhan atau ratusan
				if($clen > 2)
					$spellb .= 'ratus ';
				else if($clen > 1)
					$spellb .= 'puluh ';
			}
			
			$spell = $spellb.$spell;
			$clen++;
		}
		
		return trim($spell);
	}
	
	// mengucapkan bilangan secara utuh
	function spellNum($num) {
		$num = strval($num);
		if(!ctype_digit($num))
			return '';
		
		$len = strlen($num);
		$spell = '';
		$clen = 1;
		
		// bilangan yang panjang diproses setiap ratusan dari akhir
		$cnum = $num;
		while($cnum != '') {
			$clen = strlen($cnum);
			if(strlen($cnum) >=3) {
				$pnum = substr($cnum,-3);
				$cnum = substr($cnum,0,($clen-3));
			}
			else {
				$pnum = $cnum;
				$cnum = '';
			}
			
			$spella[] = Helper::spellNumP($pnum);
		}
		
		$n = count($spella);
		for($i=0;$i<$n;$i++) {
			$j = $i;
			
			$spellb = '';
			while($j > 0) {
				if($j >= 4) {
					$spellb = ' triliun'.$spellb;
					$j -= 4;
				}
				else if($j == 3) {
					$spellb = ' miliar'.$spellb;
					$j -= 3;
				}
				else if($j == 2) {
					$spellb = ' juta'.$spellb;
					$j -= 2;
				}
				else if($j == 1) {
					$spellb = ' ribu'.$spellb;
					$j--;
				}
			}
			
			// kalau seribu bukan satu ribu
			if($spella[$i] == 'satu' and substr($spellb,0,5) == ' ribu')
				$spell = ' se'.trim($spellb).$spell;
			else if($spella[$i] != '')
				$spell = ' '.$spella[$i].$spellb.$spell;
		}
		
		return trim($spell);
	}
	
	// mengucapkan bilangan yang disertai koma dan titik
	function spellNumC($num) {
		// penghilangan titik
		$trans = array('.' => '');
		$num = strtr($num,$trans);
		
		// dipilah-pilah sesuai koma
		$a_num = explode(',',$num);
		$n = count($a_num);
		
		$spell = '';
		for($i=0;$i<$n;$i++) {
			$bag = $a_num[$i];
			$spell .= Helper::spellNum($bag);
			
			if($i < ($n-1))
				$spell .= 'Koma ';
		}
		
		return $spell;
	}
	
	function xlsHTMLArray($html) {
		$data = array();
		
		$i = 1;
		$arrtr = explode('</tr>',$html);
		
		foreach($arrtr as $tr) {
			$j = 1;
			$arrtd = explode('</td>',$tr);
			
			foreach($arrtd as $td) {
				$posopn = strpos($td,'<td');
				if($posopn === false)
					continue;
				
				$poscls = strpos(substr($td,$posopn),'>');
				if($poscls === false)
					continue;
				
				$td = substr($td,$posopn+$poscls+1);
				
				$data[$i][$j] = $td;
				$j++;
			}
			
			$i++;
		}
		
		return $data;
	}
	
	function pembulatan100($num) {
		$num = round($num);
		
		$bulat = (int)substr($num,-2);
		$bulat = 100 - $bulat;
		
		if ($bulat != 100)
			return ($num + $bulat);
		else
			return $num;
	}
	
	/* FUNGSI PENANGGALAN */
	
	// nama hari di bahasa indonesia
	function indoDay($nhari,$full=true) {
		if($full) {
			switch($nhari) {
				case 0: return "Minggu";
				case 1: return "Senin";
				case 2: return "Selasa";
				case 3: return "Rabu";
				case 4: return "Kamis";
				case 5: return "Jumat";
				case 6: return "Sabtu";
			}
		}
		else {
			switch($nhari) {
				case 0: return "Min";
				case 1: return "Sen";
				case 2: return "Sel";
				case 3: return "Rab";
				case 4: return "Kam";
				case 5: return "Jum";
				case 6: return "Sab";
			}
		}
	}
	
	// nama bulan di bahasa indonesia
	function indoMonth($nbulan,$full=true) {
		if($full) {
			switch($nbulan) {
				case 1: return "Januari";
				case 2: return "Pebruari";
				case 3: return "Maret";
				case 4: return "April";
				case 5: return "Mei";
				case 6: return "Juni";
				case 7: return "Juli";
				case 8: return "Agustus";
				case 9: return "September";
				case 10: return "Oktober";
				case 11: return "Nopember";
				case 12: return "Desember";
			}
		}
		else {
			switch($nbulan) {
				case 1: return "Jan";
				case 2: return "Peb";
				case 3: return "Mar";
				case 4: return "Apr";
				case 5: return "Mei";
				case 6: return "Jun";
				case 7: return "Jul";
				case 8: return "Agu";
				case 9: return "Sep";
				case 10: return "Okt";
				case 11: return "Nop";
				case 12: return "Des";
			}
		}
	}
	
	function enMonth($nbulan,$full=true) {
		if($full) {
			switch($nbulan) {
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
			}
		}
		else {
			switch($nbulan) {
				case 1: return "Jan";
				case 2: return "Peb";
				case 3: return "Mar";
				case 4: return "Apr";
				case 5: return "May";
				case 6: return "Jun";
				case 7: return "Jul";
				case 8: return "Agu";
				case 9: return "Sep";
				case 10: return "Oct";
				case 11: return "Nov";
				case 12: return "Dec";
			}
		}
	}
	
	//*DB HELPER *//
	
	function validRole($conn, $kdklasifikasi, $kdjenis){
		if($_SESSION['userid'] == '') {
			Helper::navigateOut();
			exit();
		}
		else {
			$sql = "select lamaperpanjangan as lama, jumlahperpanjangan as jml, batasmaxpinjam as batas, 1 as read, isdownload as down 
					from pp_aturan where kdjenisanggota='$_SESSION[jenisanggota]' ";
			$row = $conn->GetRow($sql);
			
			$return = array();
			foreach($row as $key => $value) {
				if(empty($value))
					$return[$key] = false;
				else
					$return[$key] = true;
			}
			
			return $return;
		}
	}
	
	function xIsLoggedIn() {

		if ($_SESSION[Config::G_SESSION]['remote_addr'] == $_SERVER['REMOTE_ADDR']
			&& $_SESSION[Config::G_SESSION]['user_agent'] == $_SERVER['HTTP_USER_AGENT']
			&& $_SESSION[Config::G_SESSION]['iduser']) {
			return true;
		}

		return false;
	}	
	
	function checkRoleAuth($conn) {
		if($_SESSION['userid'] == '') {
			Helper::navigateOut();
			exit();
		}
		else {
			if ($_SESSION['starttime']!='') {
				if((time() - strtotime($_SESSION['starttime']) > 1000)) {
					echo "<script>alert('Session login telah habis !')</script>";
					session_destroy();
					echo "<script>window.location.reload();</script>";
				}else
				$_SESSION['starttime']=date('H:i:s');
			}else{
				session_destroy();
				echo "<script>window.location.reload();</script>";
			}

			$row = array();
			$row['canread'] = 1;
			$row['canedit'] = 1;
			$row['canadd'] = 1;
			
			$return = array();
			foreach($row as $key => $value) {
				if(empty($value))
					$return[$key] = false;
				else
					$return[$key] = true;
			}
			
			return $return;
		}
	}
	
	// cek otorisasi unit self dan child utk SDM
	function rideUnit($conn,$target) {
		if($target == '')
			return true;
		
		$sql = "select 1 from ms_satker where idsatker = '$target' and info_lft >= '".$_SESSION['PERPUS_SATKER_L']."'
				and info_rgt <= '".$_SESSION['PERPUS_SATKER_R']."'";
		$isride = $conn->GetOne($sql);
		
		if($isride == 1)
			return true;
		else
			return false;
	}
	
	// cek otorisasi unit self dan child utk SDM
	function rideUnitAkad($connakad,$target) {
		/*$cekfk = substr($target,0,1);
		
		if ($cekfk == 'f'){
			$idfk = substr($target,1,strlen($target)-1);
			$sql = "select 1 from fakultas where idfakultas = '$idfk'";
			$isride = $connakad->GetOne($sql);
		}*/
		
		if($target == '')
			return true;
		
		$sql = "select 1 from Fakultas where idFakultas = '$target'";
		$isride = $connakad->GetOne($sql);
		
		if($isride == 1)
			return true;
		else
			return false;
	}
	
	// tampilkan pesan error
	function pgError($conn,$tipe='simpan') {
		$errno = $conn->ErrorNo();
		$errmsg = $conn->ErrorMsg();
		
		if($fkey = strpos($errmsg,'DBSERROR:')) { // pesan tidak perlu diterjemahkan
			$start = $fkey + 9;
			$end = (strpos($errmsg,'CONTEXT')) - $start;
			$errview = trim(substr($errmsg,$start,$end));
			return UI::message("ERROR: ".$errview,true);
		}
		
		switch($errno) {
			case -1:
				if($tipe == "hapus")
					return UI::message("ERROR: Data masih dijadikan referensi.",true);
				else
					return UI::message("ERROR: Terjadi kesalahan pada referensi data.",true);
			case -5: return UI::message("ERROR: Terjadi duplikasi data.",true);
			case 0: 
				if($tipe == "simpan")
					return UI::message("Penyimpanan data berhasil.");
				else if($tipe == "hapus")
					return UI::message("Penghapusan data berhasil.");
				else
					return UI::message("Operasi data berhasil.");
			default: return UI::message("ERROR: Terjadi kesalahan pada operasi data.",true);
		}
	}
	
	// mendapatkan semester akademik dari periode yyyymm
	function getSemesterAkad($periode) {
		$thn = (int)substr($periode,0,4);
		$bln = (int)substr($periode,-2);
		
		if($bln <= 2 or $bln >= 9)
			$semester = $thn.'1';
		else
			$semester = ($thn-1).'2';
		
		return $semester;
	}
	
	// memformat kode kegiatan di penilaian
	function formatKodeKegiatan($str) {
		$str = ' '.$str; // padding untuk karakter pertama
		$a_str = str_split($str,2);
		$a_str[0] = $a_str[0][1]; // strip elemen pertama
		return implode('.',$a_str);
	}
	
	// membuat gradiasi warna
	function gradient() {
		$args = func_get_args();
		
		if(empty($args[0]))
			$scale = 5;
		else
			$scale = $args[0];
		
		$cols = array_slice($args,1);
		if(empty($cols)) {
			$cols = array();
			$cols[0] = '000000';
		}
		if(empty($cols[1])) {
			$cols[1] = 'ffffff';
		}
		
		$ncol = count($cols);
		
		// menghilangkan #
		for($i=0;$i<$ncol;$i++)
			if($cols[$i][0] == '#')
				$cols[$i] = substr($cols[$i],1);
		
		// menghitung nilai desimal dari heksadesimal
		for($i=0;$i<$ncol;$i++) {
			$r[$i] = hexdec(substr($cols[$i],0,2));
			$g[$i] = hexdec(substr($cols[$i],2,2));
			$b[$i] = hexdec(substr($cols[$i],4,2));
		}
		
		// menghitung variabel pelengkap
		if($scale <= $ncol) {
			$sint = 0;
		}
		else {
			$sint = ceil(($scale-$ncol)/($ncol-1));
			$sind = ($scale-$ncol)%($ncol-1);
			if($sind == 0)
				$sind = $ncol-1;
		}
		
		$j = 0; // untuk indeks array warna
		$k = ($sint > 0 ? 0 : -1); // untuk penanda saat init warna utk gradiasi
		$l = 1; // counter batas warna
		$retc = array();
		
		if($k == -1)
			// tanpa gradiasi karena $scale <= $ncol
			for($i=0;$i<$scale;$i++)
				$retc[$i] = $cols[$j++];
		else {
			// dengan gradiasi karena $scale > $ncol
			for($i=0;$i<$scale;$i++) {
				if($i == $k) {
					$nextc = $sint + ($l > $sind ? 0 : 1) + 1;
					$k += $nextc; // saat ganti batas warna
					$m = 0; // counter gradiasi utk setiap batas warna
					
					$col1 = $cols[$j];
					$col2 = $cols[$j+1];
					$r1 = $r[$j]; $g1 = $g[$j]; $b1 = $b[$j];
					$r2 = $r[$j+1]; $g2 = $g[$j+1]; $b2 = $b[$j+1];
					$j++;
					
					if($r1 > $r2) {
						$rd = $r1-$r2;
						$rs = -1;
					}
					else {
						$rd = $r2-$r1;
						$rs = 1;
					}
					
					if($g1 > $g2) {
						$gd = $g1-$g2;
						$gs = -1;
					}
					else {
						$gd = $g2-$g1;
						$gs = 1;
					}
					
					if($b1 > $b2) {
						$bd = $b1-$b2;
						$bs = -1;
					}
					else {
						$bd = $b2-$b1;
						$bs = 1;
					}
					
					$re = ($rd/$nextc)*$rs;
					$ge = ($gd/$nextc)*$gs;
					$be = ($bd/$nextc)*$bs;
					
					$l++;
				}
				
				$retc[$i] = '#'.dechex($r1+($m*$re)).dechex($g1+($m*$ge)).dechex($b1+($m*$be));
				$m++;
			}
		}
		
		return $retc;
	}
	
		
	/* FUNGSI ARRAY */
	
	function setTableArray(&$arr,$label,$tipe,$lebar=0) {
		$arr['label'] = $label;
		$arr['tipe'] = $tipe;
		$arr['lebar'] = $lebar;
	}
	
	// menjumlahkan elemen array
	function arrayAdd($src,$add) {
		if(!empty($add)) {
			foreach($add as $key => $val) {
				if(empty($src[$key]))
					$src[$key] = $add[$key];
				else if(is_array($add[$key]))
					$src[$key] = Helper::arrayAdd($src[$key],$add[$key]);
				else
					$src[$key] += $add[$key];
			}
		}
		return $src;
	}
	
	// jika $item kosong, return $sub
	function cEmChg($item,$sub) {
		if($item == '')
			return $sub;
		else
			return $item;
	}
	
	function getExtension($fileName) {
		$pos = strrchr($fileName, '.');

		if ($pos !== FALSE) {
			return strtolower(substr($pos, 0));
		} else {
			return '';
		}
	}
	
	//simpan 3 field pasti time,remote,ip
	
	function Identitas(&$record){
		$record['t_user']=Helper::cStrNull($_SESSION['userid']);
		$record['t_updatetime']=date('Y-m-d H:i:s');
		$record['t_host']=Helper::cStrNull($_SERVER['REMOTE_ADDR']);
	
	}
		
	# flash data untuk notifikasi setelah POST
	function setFlashData($name, $value) {
		$_SESSION['PERPUS_FLASHDATA'][$name] = $value;
	}
	
	function getFlashData($name) {
		if (isset($_SESSION['PERPUS_FLASHDATA'][$name]))
			return $_SESSION['PERPUS_FLASHDATA'][$name];
		return false;
	}

	function clearFlashData() {
		unset($_SESSION['PERPUS_FLASHDATA']);
	}
	
	function clearTrans() {
		unset($_SESSION['keyword']);

	}
	
	function tglEng($tglnya) { //format harus Y-m-d
	$bulan=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	
	$format=explode("-",$tglnya);
	$tgl=$format[2];
	$bln=$bulan[$format[1]-1];
	$thn=$format[0];
	
	$hasil=$tgl." ".$bln." ".$thn;	
	return $hasil;
	}
	
	function tglEngStamp_old($tglstamp) { //format default Y-m-d time
	$datetime=explode(" ",$tglstamp);
	$date=Helper::tglEng($datetime[0]);
	$time=$datetime[1];
	
	$val=$date." ".$time;
	return $val;
	
	}
	
	function tglEngStamp($tglstamp) { //format default Y-m-d time
		$datetime=explode(" ",$tglstamp);
		$date=$datetime[0];
		$time=$datetime[1];
		
		$val=$date;
		return $val;
	
	}
	
	function word_limiter($str, $limit = 100, $end_char = ' &#8230;')
	{
		if (trim($str) == '')
		{
			return $str;
		}
	
		preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);
			
		if (strlen($str) == strlen($matches[0]))
		{
			$end_char = '';
		}
		
		return rtrim($matches[0]).$end_char;
	}
	
	function sendMail($mailto,$toname,$subject,$body) {
		require_once("phpmailer/class.phpmailer.php");
			
		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		
		try {
			$mail->SMTPAuth = true;
			$mail->Host		= Config::SMTPHost;
			$mail->Port		= Config::SMTPPort;
			$mail->Username		= Config::SMTPUser;
			$mail->Password		= Config::SMTPPass;
			//$mail->SMTPSecure = 'tls';
			
			$mail->SetFrom(Config::AdminEmail, Config::AdminName);
			$mail->AddAddress($mailto,$toname);
			$mail->Subject = $subject;
			$mail->Body = str_replace("\n","<br/>",$body);
			
			$mail->Send();
			
			return true;
		} catch (phpmailerException $e) {
			return false;
		} catch (Exception $e) {
			return false;
		}
	}
		
	function sendMailKontakPerpus($mailfrom,$fromname,$subject,$body) {
		require_once("phpmailer/class.phpmailer.php");
		$kepala = Helper::getHeadPerpus();
		
		$isi = "";
		$isi .= "<table border='0'>";
		$isi .= "<tr><td colspan='3' bgcolor='#A9F5BC'><strong>Data Pengunjung</strong></td></tr>";
		$isi .= "<tr><td valign='top'><strong>Nama</strong></td><td valign='top'> : </td><td valign='top'>".$fromname."</td></tr>";
		$isi .= "<tr><td valign='top'><strong>Email</strong></td><td valign='top'> : </td><td valign='top'>".$mailfrom."</td></tr>";
		$isi .= "<tr><td colspan='3' bgcolor='#A9F5BC'><strong>Kritik dan Saran</strong></td></tr>";
		$isi .= "<tr><td colspan='3' valign='top'>".str_replace("\n","<br/>",$body)."</td></tr>";
		$isi .= "</table>";

		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		
		try {
			$mail->IsHTML(true);
			$mail->SMTPAuth = true;
			$mail->Host		= Config::SMTPHost;
			$mail->Port		= Config::SMTPPort;
			$mail->Username		= Config::SMTPUser;
			$mail->Password		= Config::SMTPPass;
			//$mail->SMTPSecure = 'tls';
			$mail->ClearAddresses();
			$mail->AddAddress(Config::AdminEmail,Config::AdminName);
			$mail->From = Config::SMTPUser;
			$mail->FromName = "Digilib - PJB [noreplay]";
			$mail->Subject = "Email Pengunjung [Hubungi Kami]";
			$mail->Body = str_replace("\n","<br/>",$isi);
			$mail->Send();
			
			return true;
		} catch (phpmailerException $e) {
			return false;
		} catch (Exception $e) {
			return false;
		}
	}
	
	function sendMailUsulan($conn,$idusulan) {
		require_once("phpmailer/class.phpmailer.php");
		$kepala = Helper::getHeadPerpus();
		$body = Helper::getBodyUsulan($conn,$idusulan);
		$subject = "Konfirmasi - Ada Usulan Pustaka Baru [Digilib-PJB]";
		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		
		try {
			$mail->IsHTML(true);
			$mail->SMTPAuth = true;
			$mail->Host		= Config::SMTPHost;
			$mail->Port		= Config::SMTPPort;
			$mail->Username		= Config::SMTPUser;
			$mail->Password		= Config::SMTPPass;
			//$mail->SMTPSecure = 'tls';
			$mail->ClearAddresses();
			$mail->AddAddress(Config::AdminEmail,Config::AdminName);
			$mail->From = Config::SMTPUser;
			$mail->FromName = "Digilib - PJB [noreplay]";
			$mail->Subject = $subject;
			$mail->Body = str_replace("\n","<br/>",$body);
			$mail->Send();
			
			return true;
		} catch (phpmailerException $e) {
			return false;
		} catch (Exception $e) {
			return false;
		}
	}
	
	function strOnly($str){
		return str_replace(array('-', '.', '(', ')', '!', '@', '#', '$', '%', '^', '&', '*', '_', '=', '+',':','?'), '',$str);
	} 
	
	function randPassword() {
		$length = 7;
		$characters = '01X234Y567Z89aAbBRcCdDeEfFghGiSjHklImQnoJpTqrVsUtKuvMwxNyWOzP';
		$pass ='';    
	
		for ($p = 0; $p < $length; $p++) {
			$pass .= $characters[mt_rand(0, strlen($characters))];
		}
		
		return $pass;
	}
	
	function getArrParam($arparam,$parkey){
		$paritem = array();
		$recdet = array();

		if(is_array($arparam)){
			foreach($arparam as $kparam => $vparam){
				if(!empty($_POST[$vparam]))
					$paritem[$vparam] = Helper::cStrFill($_POST[$vparam]);
			}
			if(count($paritem[$parkey])>0){
				foreach($paritem[$parkey] as $i => $val){
					foreach($paritem as $paramname => $paramval){
						$recdet[$i][$paramname] = $paramval[$i];
					}						
				}
			}
		}
		return $recdet;
	}
	
	
	function CLang($text){
	$translator=new GTranslator();  
		if ($_SESSION['lang']!='id'){
			$dari='en';
			$ke='id';
	
        $trans=@$translator->translate($text,'id','en');
        $responseData=$trans['responseData'];
        if (is_array($responseData)){  
            $hasil=$responseData['translatedText'];
        }else {
            //echo "Error : ". $responseData['responseDetails'] . "<br />"  ; 
			$hasil=$text; 
        }
		}else
		$hasil=$text;
  
    
	$valids=$translator->get_valid_language();
	//$select="";
	return $hasil;
	}
	
	function CariAlf($fungsi){
	$arAlfx=array('a'=>'A','b'=>'B','c'=>'C','d'=>'D','e'=>'E','f'=>'F','g'=>'G','h'=>'H',
				 'i'=>'I','j'=>'J','k'=>'K','l'=>'L','m'=>'M','n'=>'N','o'=>'O','p'=>'P',
				 'q'=>'Q','r'=>'R','s'=>'S','t'=>'T','u'=>'U','v'=>'V','w'=>'W','x'=>'X',
				 'y'=>'Y','z'=>'Z');
	$arAlf=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
	$alf='';
	for($i=0;$i<count($arAlf);$i++){
		if($alf=='')
			$alf="<a href=\"javascript:$fungsi('".$arAlf[$i]."')\">".$arAlf[$i]."</a>";
		else
			$alf=$alf."&nbsp; <a href=\"javascript:$fungsi('".$arAlf[$i]."')\">".$arAlf[$i]."</a>";
	}
	
	return $alf;
	}
	
	
	function GetPath($str){
		$stre = explode("/",$str);
		
		$jstr = count($stre)-1;
		
		$sstr = $stre[$jstr];
		
		return $sstr;
	}
	
	function s_get_os() {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if(($pos = strpos($agent,'Windows ')) !== false) {
			$major = substr($agent,$pos+8,2);
			
			if($major == '3.') return 'Windows 3.1';
			else if($major == '95') return 'Windows 95';
			else if($major == '98') return 'Windows 98';
			else if($major == 'Me') return 'Windows Me';
			else if($major == 'NT') {
				$version = substr($agent,$pos+11,3);
				
				if($version == '5.0') return 'Windows 2000';
				else if($version == '5.1') return 'Windows XP';
				else if($version == '5.2') return 'Windows Server 2003';
				else if($version == '6.0') return 'Windows Vista';
				else return 'Windows NT';
			}
			else return 'Windows';
		}
		else if(strpos($agent,'Linux')) return 'Linux';
		else if(strpos($agent,'Symbian')) return 'Symbian';
		else if(strpos($agent,'MIDP')) return 'MIDP';
		else if(strpos($agent,'Mac')) return 'Macintosh';
		else if(strpos($agent,'Solaris')) return 'Solaris';
		else return 'Lain-lain';
	}
	
	function s_get_browser() {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if(($pos = strpos($agent,'Opera')) !== false) $posp = $pos+5;
		else if(($pos = strpos($agent,'Flock')) !== false) $posp = $pos+5;
		else if(($pos = strpos($agent,'Firefox')) !== false) $posp = $pos+7;
		else if(($pos = strpos($agent,'Chrome')) !== false) $posp = $pos+6;
		else if(($pos = strpos($agent,'Safari')) !== false) $posp = $pos+6;
	  	else if(($pos = strpos($agent,'Konqueror')) !== false) $posp = $pos+9;
		else if(($pos = strpos($agent,'MSIE')) !== false) $posp = $pos+4;
		else return 'Lain-lain';
		
		$posv = strpos($agent,' ',$posp+1);
		
		if($posv === false)
			$return = substr($agent,$pos);
		else
			$return = substr($agent,$pos,($posv-$pos));
		
		if($return[strlen($return)-1] == ';')
			$return = substr($return,0,strlen($return)-1);
		
		return $return;
	}
	
	function addHistory($conn, $action, $updatedata){
		$record = array();
		$record['t_userid'] = ( $_SESSION['userid'] ? $_SESSION['userid'] : "guest"); //$_SESSION['userid'];
		$record['t_username'] = ( $_SESSION['username'] ? $_SESSION['username'] : "GUEST"); //$_SESSION['username'];
		$record['t_usertime'] = date('Y-m-d H:i:s');
		$record['t_userip'] = $_SERVER['REMOTE_ADDR'];
		$record['t_osname'] = Helper::s_get_os();
		$record['t_browsername'] = Helper::s_get_browser();
		
		if ($updatedata != '')
			$record['t_userupdated'] = $updatedata;
		else
			$record['t_userupdated'] = "tidak ada yang di update";
		
		$record['t_useraksi'] = $action;
		//$col = $conn->SelectLimit("select * from pp_historyaction",1);
		$col = $conn->Execute("select * from pp_historyaction where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo()) {			
			return false;
		}
	}
	
	function addRead($conn, $idpustaka){
		$record = array();
		$record['idpustaka'] = $idpustaka;
		$record['iduser'] = ( $_SESSION['userid'] ? $_SESSION['userid'] : "guest");
		$record['t_userip'] = $_SERVER['REMOTE_ADDR'];
		$record['t_usertime'] = date('Y-m-d H:i:s');
		$record['t_osname'] = Helper::s_get_os();
		$record['t_browsername'] = Helper::s_get_browser();

		$col = $conn->Execute("select * from pp_logpustaka where 1=-1");
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo()) {			
			return false;
		}
	}
	
	function readers($conn, $idpustaka){
		$readers = $conn->GetOne("select count(*) from pp_logpustaka where idpustaka = $idpustaka ");
		return $readers;
	}
	
	function canOpen($conn,$file,$seri){
				
		$row = $conn->GetRow("select kdjenispustaka from ms_pustaka where noseri='$seri'");
		
		$rfile = $conn->GetRow("select idpustaka from ms_pustaka where noseri='$seri' and (file1='".trim($file)."' or file2='".trim($file)."')");
					
		$isTrue = $row['kdjenispustaka'].'__#__'.$rfile['idpustaka'];
		
		return $isTrue;
	}
	
	function getHeadPerpus(){
		// koneksi ke sdm
		$conns = Factory::getConnSdm();
		$sql = $conns->GetRow("select nik, f_namalengkap(gelardepan,namadepan,namatengah,namabelakang,gelarbelakang) as namalengkap, email
					from ms_pegawai
					where idjstruktural='202022'");
		return $sql;
	}
	
	function getBodyUsulan($conn,$idusulan){
		$sql = $conn->GetRow("select * from pp_usul where idusulan = '$idusulan' ");
		$body = "";
		$body .= "<table border='0'>";
		$body .= "<tr><td colspan='3' bgcolor='#A9F5BC'><strong>Data Pengusul</strong></td></tr>";
		$body .= "<tr><td valign='top'><strong>ID Anggota</strong></td><td valign='top'> : </td><td valign='top'>".$sql['idanggota']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Nama Anggota</strong></td><td valign='top'> : </td><td valign='top'>".$sql['namapengusul']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Tanggal Usulan</strong></td><td valign='top'> : </td><td valign='top'>".$sql['tglusulan']."</td></tr>";
		$body .= "<tr><td colspan='3'></td></tr>";
		$body .= "<tr><td colspan='3' bgcolor='#A9F5BC'><strong>Data Pustaka Usulan</strong></td></tr>";
		$body .= "<tr><td valign='top'><strong>Judul Pustaka</strong></td><td valign='top'> : </td><td valign='top'>".$sql['judul']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Pengarang</strong></td><td valign='top'> : </td><td valign='top'>".$sql['authorfirst1']." ".$sql['authorlast1']."<br/>".$sql['authorfirst2']." ".$sql['authorlast2']."<br/>".$sql['authorfirst3']." ".$sql['authorlast3']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Penerbit</strong></td><td valign='top'> : </td><td valign='top'>".$sql['penerbit']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Tahun Terbit</strong></td><td valign='top'> : </td><td valign='top'>".$sql['tahunterbit']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Edisi</strong></td><td valign='top'> : </td><td valign='top'>".$sql['edisi']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>ISBN</strong></td><td valign='top'> : </td><td valign='top'>".$sql['isbn']."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Harga</strong></td><td valign='top'> : </td><td valign='top'>Rp. ".number_format($sql['hargausulan'])."</td></tr>";
		$body .= "<tr><td valign='top'><strong>Keterangan</strong></td><td valign='top'> : </td><td valign='top'>".str_replace("\n","<br/>",$sql['keterangan'])."</td></tr>";
		$body .= "</table>";
		return $body;
	}
	
	function repCari($data, $cari){
		echo ucfirst(str_replace(trim(strtolower($cari)), '<strong><font color="#428BCA">'.trim($cari).'</font></strong>',trim(strtolower($data))));
	}
}

?>