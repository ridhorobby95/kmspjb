<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//tidak login 
	$lang['LOGIN'] = 'Login';
	$lang['USERNAME'] = 'Pengguna';
	$lang['PASSWORD'] = 'Kata Sandi';
	$lang['FORGOTPASS'] = 'Lupa Password ?';
	$lang['SD_NEWS'] = 'Berita';
	$lang['SD_SEARCH'] = 'Cari...';
	$lang['INBOX'] = 'Kotak Masuk';
	$lang['BILL'] = 'Tagihan';
	$lang['STATISTIK'] = 'Statistik';
	$lang['ST_PENGUNJUNG'] = 'Pengunjung hari ini :';
	$lang['ST_ONLINE'] = 'Pengguna Aktif :';
	$lang['ST_TOTAL'] = 'Total Pengunjung :';
	$lang['ALERT_SEARCH'] = 'Kata Pencarian Tidak Boleh Kosong';
	$lang['ABOUT_SEVIMAPERPUS'] = 'Tentang Perpustakaan SEVIMA';
	
	$lang['INFO_USER'] = 'Informasi Pengguna';
	$lang['MEMBERNAME'] = 'Nama Anggota';
	$lang['MEMBERTYPE'] = 'Tipe Anggota';
	$lang['LOGINTIME'] = 'Waktu Masuk';
	$lang['LASTLOGIN'] = 'Masuk Terakhir';
	$lang['LOGOUTMSG'] = '[Jangan Lupa Logout]';
	
	$lang['SKORSING'] = 'Anda dalam masa skorsing !';
	$lang['TGL_SKOR'] = 'Hingga Tanggal';
	
	$lang['INFORMATION'] = 'Informasi';
	$lang['FACILITY'] = 'Fasilitas';
	$lang['SERVICE'] = 'Layanan';
	$lang['COLLECTION'] = 'Koleksi';
	$lang['MEMBERSHIP'] = 'Keanggotaan';
	$lang['CONTACT'] = 'Hubungi Kami';
	$lang['CARIPOJOK'] = 'Pencarian Pustaka :';
?>