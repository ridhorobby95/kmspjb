<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	$lang['HOME'] = 'Home';
	$lang['DAFTAR_PUSTAKA'] = 'Bibliography';
	$lang['SEMUA_DAFTAR'] = 'All List';
	$lang['RESERVASI'] = 'Reservation';
	$lang['SEARCH'] = 'Search';
	$lang['TA'] = 'Final Project';	
	$lang['INFORMATION'] = 'Information';
	$lang['NEWS'] = 'News';
	$lang['CONTACT'] = 'Contact Us';
	$lang['ONLINE'] = 'Online Contact';
	$lang['REPORT'] = 'Loans Report';
	$lang['OFFER'] = 'Request';
	$lang['OFFER_GREEN'] = 'Green Label Request';
	$lang['OFFER_PUBLIC'] = 'General Request';
	$lang['PROFILE'] = 'Profile Information';
	$lang['EXIT'] = 'Exit';
	$lang['CHOOSE'] = 'Choose Language';
	$lang['UPLOADTA'] = 'Upload Final Project';
	$lang['UPLOADFILETA'] = 'Upload File Final Project';
	$lang['TCUPLOAD'] = 'How To Upload Final Project';
	$lang['VALIDASI'] = 'Final Project Validation';
	$lang['NOTIFIKASI_TA'] = 'Validation List';
	$lang['LIST_NOTIFIKASI'] = 'Notifications List';
	$lang['LIST_MONITORING'] = 'List of Upload Independently';
	
	$lang['ABOUT_US'] = 'About Us';
	$lang['CATDIR'] = 'Category Directory';

	//breadcrumb
	$lang['CONTACT_US'] = 'Contact Us';	
	$lang['PROPOSED'] = 'General Request';	
	$lang['PROPOSED_LECTURE'] = 'Green Label Request';	
	$lang['Facility'] = 'Facility';		
	$lang['Service'] = 'Services';
	$lang['Collection'] = 'Collection';
	$lang['Membership'] = 'Membership';
	$lang['Profile'] = 'Profile';	
	$lang['TOPIC'] = 'Topic';	
	$lang['DET_NEWS'] = 'News Description';		
	$lang['CART'] = 'Cart';				
	$lang['LOAN'] = 'Loan';				
	$lang['POST_DATA'] = 'Post Data';		
	$lang['BIBLIO'] = 'Bibliography';
	$lang['B_FORMTA'] = 'Form Upload FINAL PROJECT';
	$lang['HIST_NOTIF'] = 'List of History Notifications';	
	
	//not found
	$lang['notfound'] = 'Data Not Found';
?>