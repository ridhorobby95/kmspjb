<?php  
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('config.class.php');

	class Language {
		
		function setLang($langfile='', $bahasa=''){
			$langfile = str_replace(Config::extLanguage, '', str_replace('_lang.', '', $langfile)).'_lang'.Config::extLanguage;
									
			if ($bahasa == '' and $_SESSION['lang'] == ''){
				$bahasa = Config::language;
				$_SESSION['lang'] = $bahasa;
			}else
				$bahasa = $_SESSION['lang'];
			
			switch ($bahasa){
				case 'id' :
					$file = 'indonesia';
					break;
				case 'en' :
					$file = 'english';
					break;
				default :
					$file = 'english';
			}
			
			if (is_file(Config::pathLanguage.$file.'/'.$langfile))
				include(Config::pathLanguage.$file.'/'.$langfile);
			
			if ( ! isset($lang))
			{
				return;
			}else
				return $lang;
				
		}
	}

?>