<?php

defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

require_once('config.class.php');

class Factory
{
	// Koneksi database utama
	function &getConn() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConn();

		return $instance;
	}
	
	function &getConnLamp() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConnLamp();

		return $instance;
	}
	
	function &createConn() {
		define('ADODB_ASSOC_CASE', 0);
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driver);

		if (!@$conn->Connect(Config::server,Config::user,Config::password,Config::database))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database Perpus </p>");
		/*else
			die("berhasil konek ke oracle perpus");*/
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
			
		return $conn;
	}
	
	function &createConnLamp() {
		define('ADODB_ASSOC_CASE', 0);
		require_once('adodb/adodb.inc.php');
		
		$conn = ADONewConnection(Config::driverLamp);

		if (!$conn->Connect(Config::serverLamp,Config::userLamp,Config::passwordLamp,Config::databaseLamp))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database lampiran.</p>");
		
		$conn->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql =  $conn->Execute("SET search_path TO 'public' ");		
			
		return $conn;
	}
	
	// Koneksi database gate
	function &getConnGate() {
		static $instance;

		if (!is_object($instance))
			$instance = Factory::createConnGate();

		return $instance;
	}
	
	function &createConnGate() {
		require_once('adodb/adodb.inc.php');
		
		$connGate = ADONewConnection(Config::driverGate);

		if (!$connGate->Connect(Config::serverGate,Config::userGate,Config::passwordGate,Config::databaseGate))
			die ("<p>Error : Tidak bisa melakukan koneksi dengan database " . Config::userGate. "</p>"); //8gXwashere
		
		$connGate->SetFetchMode(ADODB_FETCH_ASSOC);
		
		$sql =  $connGate->Execute("SET search_path TO 'gate' ");
			
		return $connGate;
	}
	
}

?>
