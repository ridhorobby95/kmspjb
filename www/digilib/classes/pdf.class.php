<?php 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	class Pdf {
		
		function ghostFile($file,$seri){
			/*
			require : ghostscript, imagemagick
			*/
		
			list($nama,$ext) = explode('.',Helper::GetPath($file));
			
			$pathpdf = $file;

			$directori = '../../perpus/ghostfile/'.$seri;

			//cek apakah directori sudah ada
			if (!is_dir($directori)){
				mkdir($directori,0777);
			}
			$directori = $directori.'/'.Helper::removeSpecial($nama);
			if(!is_dir($directori)){
				mkdir($directori,0777);
			}
			
			$pathoutput = $directori.'/'.Helper::removeSpecial($nama).'.png';
				
			exec("convert \"{$pathpdf}\" -colorspace RGB -geometry 1024 \"{$pathoutput}\"");
		}
		
		function ghostFileTA($file,$seri){
			/*
			require : ghostscript, imagemagick
			*/
		
			$f = explode('.',Helper::GetPath($file));
			$nama = $f[0].".".$f[1];
			$pathpdf = $file;

			$directori = '../../perpus/ghostfile/'.$seri;

			//cek apakah directori sudah ada
			if (!is_dir($directori)){
				mkdir($directori,0777);
			}
			$directori = $directori.'/'.Helper::removeSpecial($nama);
			if(!is_dir($directori)){
				mkdir($directori,0777);
			}
			
			$pathoutput = $directori.'/'.Helper::removeSpecial($nama).'.png';
				
			exec("convert \"{$pathpdf}\" -colorspace RGB -geometry 1024 \"{$pathoutput}\"");
		}
		
		
	}

?>