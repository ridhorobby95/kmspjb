<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	class FTS {
		
		function cariPustaka($r_keyword,$r_lokasi='', $r_jenis='') {
			global $conn;
			
			$rs_lokasi = $_SESSION['UNIT_PERPUS'];
			if($rs_lokasi){
				$fp_sqlstr = "and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$rs_lokasi') ";
			}
			
			$p_sqlstr = "select p.*, jp.namajenispustaka, to_char(p.judul) jdul, Row_Number() OVER (order by p.idpustaka) linenum
				from ms_pustaka p ";
			$r_word = explode(' ',trim(Helper::strOnly($r_keyword)));
			$word_key = '';
			$word_key = '';
			$sql_join_options = '';
			$r_order = $r_keyword;
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x];
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'%';
					}else
						$word_key .= $r_word[$x];
				}
			}
						
			$sql_where_options = "where 1=1 $fp_sqlstr ";
			
			if (!empty($r_jenis))
				$sql_where_options .= " and p.kdjenispustaka='$r_jenis'";
			
			#status
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";

			#filter
			$sql_where_options .= "and (
				(p.tahunterbit = '$r_keyword') 
				or (upper(to_char(p.judul)) like upper('%$r_keyword%')) 
				or (upper(p.noseri) like upper('%$r_keyword%')) 
				or (upper(p.nopanggil) like upper('%$r_keyword%'))
				or (upper(jp.namajenispustaka) like upper('%$r_keyword%'))
				or (upper(p.pembimbing) like upper('%$r_keyword%'))
				or (upper(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) like upper ('%$word_key%')) 
				or (p.idpustaka in (
						SELECT 0 idpustaka FROM DUAL
						UNION
						select idpustaka
						from pp_topikpustaka tp
						left join lv_topik t on t.idtopik = tp.idtopik
						where upper(t.namatopik) like upper('%$r_keyword%')
						/*order by case when upper(t.namatopik) = upper('".$r_keyword."') then 0 else 1 end*/ 
						)
				)
			)";
			
			#lokasi
			if($r_lokasi){
				$sql_where_options .= "and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$r_lokasi') ";
			}
			
			
			//$sql_order .=" order by p.kdjenispustaka,similarity (judul, '$r_keyword') desc,coalesce(edisi,'1') desc ";
			$sql_order .=" order by jdul, p.kdjenispustaka desc,coalesce(edisi,'1') desc "; 
			
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options.$sql_where_options1.$sql_order;
		}
		
		function cariPustakata($r_keyword, $r_tipe='') {
			global $conn;
			$rs_lokasi = $_SESSION['UNIT_PERPUS'];
			if($rs_lokasi){
				$fp_sqlstr = "and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$rs_lokasi') ";
			}

			$p_sqlstr = "select p.*, jp.namajenispustaka, as idpus, to_char(p.judul) jdul, Row_Number() OVER (order by p.idpustaka) linenum
				from ms_pustaka p ";
			$r_word = explode(' ',trim(Helper::strOnly($r_keyword)));
			$word_key = '';
			$word_key = '';
			$sql_join_options = '';
			$r_order = $r_keyword;
			
			for ($x=0;$x < count($r_word);$x++){
				if ($x == count($r_word) - 1)
					$word_key .= $r_word[$x];
				else{
					if (strlen(trim($r_word[$x])) > 0){
						$word_key .= $r_word[$x].'&';
					}else
						$word_key .= $r_word[$x];
				}
			}
						
			if ($r_tipe == 'T'){ //untuk kategori pencarian topik atau subjek
				$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
									left join lv_topik t on t.idtopik=pt.idtopik ";
				$keyvector = 't.namatopik';
			}
			else if ($r_tipe == 'P'){ //pencarian berdasarkan pengarang
				$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
									left join ms_author ma on ma.idauthor=pa.idauthor ";
				$keyvector = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
			}
			else if ($r_tipe == 'K'){ //pencarian berdasarkan keyword
				$keyvector = 'keywords';
			}
			else if ($r_tipe == 'C'){ //pencarian berdasarkan pembimbing / COntributor
				$keyvector = 'pembimbing';
			}
			else //default adalah pencarian berdasarkan judul 
				$keyvector = 'to_char(judul)';
			
			$sql_where_options = "where 1=1 $fp_sqlstr ";
			
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka<>'JN')
						and exists (select idpustaka
							from pp_eksemplar e
							where e.idpustaka=p.idpustaka 
							) ";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			$sql_order .=" order by p.kdjenispustaka,upper($keyvector) like upper('%$r_keyword%') desc,coalesce(edisi,'1') desc ";
			
			for($z=1;$z<=2;$z++){
				if($z==1){
					if($r_tipe!='P')
					$sql_where_options1 .= " and upper($keyvector) like upper('%$word_key%') ";
					else
					$sql_where_options1 .= " and (upper($keyvector) like upper('%$word_key%') or upper(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) like upper ('%$word_key%')) ";//((to_tsvector(f_replace($keyvector)) @@ to_tsquery('$word_key')) or (to_tsvector(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) @@ to_tsquery('$word_key')))";
					
					$cek = $conn->GetOne($p_sqlstr.$sql_join_options.$sql_where_options.$sql_where_options1.$sql_order);
					if($cek)
						break ;
				
				}
				else{
					$sql_where_options1 = '';
					if($r_tipe!='P')
					$sql_where_options2 .= " and to_tsvector($keyvector) @@ to_tsquery('$word_key')";
					else
					$sql_where_options2 .= " and ((to_tsvector(f_replace($keyvector)) @@ to_tsquery('$word_key')) or (to_tsvector(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) @@ to_tsquery('$word_key')))";
				}
			}
			
			return $p_sqlstr.$sql_join_options.$sql_where_options.$sql_where_options1.$sql_where_options2.$sql_order;
		}
		
		function cariJudul($key,$par){
			$p_sqlstr = "select p.*,p.idpustaka as idpus,j.namajenispustaka,j.islokal, to_char(p.judul) jdul, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p
						 join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
						 where upper(p.judul) like upper('$key%')";
			$p_sqlstr .= " and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";
			
			$p_sqlstr .= " order by j.kdjenispustaka,jdul ";
			return $p_sqlstr;
		}
		
		function cariAuthor($key,$par){
			$rs_lokasi = $_SESSION['UNIT_PERPUS'];
			if($rs_lokasi){
				$fp_sqlstr = "and p.idpustaka in (select idpustaka from pp_eksemplar where kdlokasi = '$rs_lokasi') ";
			}
			
			$p_sqlstr = "select p.*,p.idpustaka as idpus,j.namajenispustaka,j.islokal, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p
						 join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
						 where upper(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')) like upper('$key%') $fp_sqlstr";
			$p_sqlstr .= " and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";
			$p_sqlstr .= " order by j.kdjenispustaka,p.authorfirst1 ";
			return $p_sqlstr;
		}
		
		function getIdPustaka($r_keyword, $r_tipe='') {
			$p_sqlstr = "select p.idpustaka from ms_pustaka p ";
			$r_word = $r_keyword;
						
			if (empty($r_tipe)){
				$sql_where_options = "where upper(to_char(judul)) like upper('%$r_word%') ";
			}else{
				if ($r_tipe == 'P')
					$sql_where_options = "left join pp_author a on a.idpustaka=p.idpustaka 
										left join ms_author m on m.idauthor=a.idauthor 
										where upper(namadepan) like upper('%$r_word%')
										or upper(namabelakang) like upper('%$r_word%')";
			}
			$sql_where_options .=" order by upper(to_char($keyvector)) like upper('%$r_word%') desc ";
			return $p_sqlstr.$sql_where_options;
		}
		
		function cariPustakaADV($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2, $r_jenis1='', $r_jenis2='')
		{
			$p_sqlstr = "select p.*, jp.namajenispustaka, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			//untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'&';
			}
			
			//untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'&';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
							||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'judul';
			}
			
			if (empty($r_keyword2)){
					
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1')";
				
				if (!empty($r_jenis1))
					$sql_where_options .= "and p.kdjenispustaka='$r_jenis1' ";
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
								||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'to_char(judul)';
				}
				else
				    $keyvector2 = 'judul';
			
				if (!empty($r_jenis1) and $r_jenis2 == '')
					$sql_where_options .= "where (to_tsvector($keyvector1) @@ to_tsquery('$word_key1') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (to_tsvector($keyvector2) @@ to_tsquery('$word_key2'))";
				else if (!empty($r_jenis1) and !empty($r_jenis2))
					$sql_where_options .= "where (to_tsvector($keyvector1) @@ to_tsquery('$word_key1') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (to_tsvector($keyvector2) @@ to_tsquery('$word_key2') and p.kdjenispustaka='$r_jenis2')";
				else
					$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1') ".$r_bool." to_tsvector($keyvector2) @@ to_tsquery('$word_key2')";
			
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			#abu
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka ) ";
			
			$sql_where_options .=" order by p.kdjenispustaka,upper(to_char($keyvector1)) like '%$r_keyword1%' desc,coalesce(edisi,'1') desc";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
	
	function cariPustakaADVs($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2, $r_jenis1='', $r_jenis2='')
		{
			$p_sqlstr = "select p.*, jp.namajenispustaka, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			//untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'%';
			}
			
			//untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'%';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
							||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'to_char(judul)';
			}
			
			if (empty($r_keyword2)){
				$sql_where_options = "where upper($keyvector1) like upper('%$word_key1%')";
				
				if (!empty($r_jenis1))
					$sql_where_options .= "and p.kdjenispustaka='$r_jenis1' ";
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "coalesce(ma.namadepan,'')||' '||coalesce(ma.namabelakang,'')||' '||coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')
								||' '||coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')||' '||coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'to_char(judul)';
				}
				else
				    $keyvector2 = 'judul';
			
				if (!empty($r_jenis1) and $r_jenis2 == '')
					$sql_where_options .= "where (upper($keyvector1) like upper('%$word_key1%') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (upper($keyvector2) like upper('%$word_key2%'))";
				else if (!empty($r_jenis1) and !empty($r_jenis2))
					$sql_where_options .= "where (upper($keyvector1) like upper('%$word_key1%') and p.kdjenispustaka='$r_jenis1') ".$r_bool." (upper($keyvector2) like upper('%$word_key2%') and p.kdjenispustaka='$r_jenis2')";
				else
					$sql_where_options = "where upper($keyvector1) like upper('%$word_key1%') ".$r_bool." upper($keyvector2) like upper('%$word_key2%')";
			
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			#abu
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";
			$sql_where_options .=" order by p.kdjenispustaka,upper(to_char($keyvector1)) like '%$r_keyword1%' desc,coalesce(edisi,'1') desc";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
	function cariPustakaADVta($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2)
		{
			$p_sqlstr = "select p.*, jp.namajenispustaka, p.idpustaka as idpus, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			// untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'&';
			}
			
			// untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'&';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'J'){ //pencarian berdasarkan jenis pustaka
					$keyvector1 = 'jp.namajenispustaka';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else if ($r_topik1 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
				        $keyvector1 = 'pembimbing';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'to_char(judul)';
			}
			
			if (empty($r_keyword2)){
					
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1')";
			
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'J'){ //pencarian berdasarkan jenis pustaka
						$keyvector2 = 'jp.namajenispustaka';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else if ($r_topik2 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
						$keyvector2 = 'pembimbing';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'to_char(judul)';
				}
				    else
					$keyvector2 ='to_char(judul)';
			
				$sql_where_options = "where to_tsvector($keyvector1) @@ to_tsquery('$word_key1') ".$r_bool." to_tsvector($keyvector2) @@ to_tsquery('$word_key2')";
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka <>'JN') ";
			#abu
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";
			
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			$sql_where_options .=" order by p.kdjenispustaka,upper(to_char($keyvector1)) like '%$r_keyword1%' desc,coalesce(edisi,'1') desc";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
	
	function cariPustakaADVsta($r_keyword1='', $r_keyword2='', $r_bool='', $r_topik1='', $r_topik2='', $r_bahasa='', $r_tahun1='', $r_tahun2)
		{
			$p_sqlstr = "select p.*, jp.namajenispustaka,p.idpustaka as idpus, Row_Number() OVER (order by p.idpustaka) linenum from ms_pustaka p ";
			$r_word1 = explode(' ',trim(Helper::strOnly($r_keyword1)));
			$r_word2 = explode(' ',trim(Helper::strOnly($r_keyword2)));
			$word_key1 = '';
			$word_key2 = '';
			$sql_join_options = '';
			$r_order = $r_keyword1;
			
			// untuk FTS per kata kalimat 1
			for ($x=0;$x < count($r_word1);$x++){
				if ($x == count($r_word1) - 1)
					$word_key1 .= $r_word1[$x];
				else
					$word_key1 .= $r_word1[$x].'%';
			}
			
			// untuk FTS per kata kalimat 2
			for ($x=0;$x < count($r_word2);$x++){
				if ($x == count($r_word2) - 1)
					$word_key2 .= $r_word2[$x];
				else
					$word_key2 .= $r_word2[$x].'%';
			}
			
			//untuk tipe boolean yang dipakai
			if ($r_bool != 'A')
				$r_bool = 'or';
			else
				$r_bool = 'and';
			
			if (!empty($r_topik1)){
				if ($r_topik1 == 'T'){ //untuk kategori pencarian topik atau subjek
					$sql_join_options = "left join pp_topikpustaka pt on pt.idpustaka=p.idpustaka
										left join lv_topik t on t.idtopik=pt.idtopik ";
					$keyvector1 = 't.namatopik';
				}
				else if ($r_topik1 == 'J'){ //pencarian berdasarkan jenis pustaka
					$keyvector1 = 'jp.namajenispustaka';
				}
				else if ($r_topik1 == 'P'){ //pencarian berdasarkan pengarang
					$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
					$keyvector1 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
				}
				else if ($r_topik1 == 'K'){ //pencarian berdasarkan keyword
					$keyvector1 = 'keywords';
				}
				else if ($r_topik1 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
				        $keyvector1 = 'pembimbing';
				}
				else //default adalah pencarian berdasarkan judul 
					$keyvector1 = 'to_char(judul)';
			}
			
			if (empty($r_keyword2)){
					
				//$sql_where_options = "where similarity ($keyvector1, '$r_word1') > 0.1";
				$sql_where_options = "where upper($keyvector1) like upper('%$word_key1%')";
			
			}else{
				if (!empty($r_topik2)){
					if ($r_topik2 == 'T'){ 
						$keyvector2 = 't.namatopik';
					}
					else if ($r_topik2 == 'J'){ //pencarian berdasarkan jenis pustaka
						$keyvector2 = 'jp.namajenispustaka';
					}
					else if ($r_topik2 == 'P'){ //pencarian berdasarkan pengarang
						$sql_join_options = "left join pp_author pa on pa.idpustaka=p.idpustaka
										left join ms_author ma on ma.idauthor=pa.idauthor ";
						$keyvector2 = "p.authorfirst1||' '||coalesce(p.authorlast1,'')";
					}
					else if ($r_topik2 == 'K'){ //pencarian berdasarkan keyword
						$keyvector2 = 'keywords';
					}
					else if ($r_topik2 == 'C'){ //pencarian berdasarkan pembimbing / Contributor
						$keyvector2 = 'pembimbing';
					}
					else //default adalah pencarian berdasarkan judul 
						$keyvector2 = 'to_char(judul)';
				}
				    else
					$keyvector2 ='to_char(judul)';
			
				//$sql_where_options = "where similarity ($keyvector1, '$r_word1') > 0.1 ".$r_bool." similarity ($keyvector2, '$r_word2') > 0.1";
				$sql_where_options = "where upper($keyvector1) like upper('%$word_key1%') ".$r_bool." upper($keyvector2) like upper('%$word_key2%')";
			}
			
			if (!empty($r_bahasa))
				$sql_where_options .= "and p.kdbahasa='$r_bahasa' ";
			
			if (!empty($r_tahun1) and (!empty($r_tahun2)))
				$sql_where_options .= "and tahunterbit::integer between $r_tahun1 and $r_tahun2 ";
			else{
				if (!empty($r_tahun1))
					$sql_where_options .= "and tahunterbit='$r_tahun1' ";
				else if (!empty($r_tahun2))
					$sql_where_options .= "and tahunterbit='$r_tahun2' ";
			}
			$sql_where_options .=" and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1 and kdjenispustaka <>'JN') ";
			#abu
			$sql_where_options .=" and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka) ";
			$sql_join_options .= "left join lv_jenispustaka jp on jp.kdjenispustaka=p.kdjenispustaka ";
			$sql_where_options .=" order by p.kdjenispustaka,upper($keyvector1) like upper('%$r_keyword1%') desc,coalesce(edisi,'1') desc";
			
			return $p_sqlstr.$sql_join_options.$sql_where_options;
		}
		
		#tambahan file upload TA
		function jenisfileta($i){
			if($i == 1){
				$jenis = "Cover";
			}elseif($i == 2){
				$jenis = "Abstrak";
			}elseif($i == 3){
				$jenis = "Bab I";
			}elseif($i == 4){
				$jenis = "Bab II";
			}elseif($i == 5){
				$jenis = "Bab III";
			}elseif($i == 6){
				$jenis = "Bab IV";
			}elseif($i == 7){
				$jenis = "Bab V";
			}elseif($i == 8){
				$jenis = "Bab VI";
			}elseif($i == 9){
				$jenis = "Bab VII";
			}elseif($i == 10){
				$jenis = "Daftar Pustaka";
			}
			
			return $jenis;
		}
	
	}
?>