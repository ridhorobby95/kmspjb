<?php
/*
* Breadcrumb navigation class
*/

class Breadcrumb{

   var $output;
   var $crumbs = array();
   var $location;


   /*
    * Constructor
    */
   function Breadcrumb(){  
   
      if ($_SESSION['breadcrumb'] != null){
         $this->crumbs = $_SESSION['breadcrumb'];
      }  
   
   }

   /*
    * Add a crumb to the trail:
    * @param $label - The string to display
    * @param $url - The url underlying the label
    * @param $level - The level of this link.  
    *
    */
   function add($label, $url, $level){

      $crumb = array();
      $crumb['label'] = $label;
      $crumb['url'] = $url;
	  $crumb['level'] = $level;

      if ($crumb['label'] != null && $crumb['url'] != null && isset($level)){       
            
         while(count($this->crumbs) > $level){

            array_pop($this->crumbs); //prune until we reach the $level we've allocated to this page

         }

         if (!isset($this->crumbs[0]) && $level > 0){ //If there's no session data yet, assume a homepage link

            $this->crumbs[0]['url'] = Helper::navAddress("list_home.php");
            $this->crumbs[0]['label'] = "Home";

         }

         $this->crumbs[$level] = $crumb;  
               
      }

        $_SESSION['breadcrumb'] = $this->crumbs; //Persist the data
        $this->crumbs[$level]['url'] = null; //Ditch the underlying url for the current page.
   }

   /*
    * Output a semantic list of links.  See above for sample CSS.  Modify this to suit your design.
    */
   function output(){

      echo "<div id='breadcrumb'><ul>";

      foreach ($this->crumbs as $crumb){  

         if ($crumb['url'] != ''){
		 	if ($crumb['level'] > 0)
	            echo '<li><img src="images/separator.png"><span style="padding-left:5px;"> <a href="'.$crumb['url'].'" title="'.$crumb['label'].'">'.$crumb['label'].'</a></span></li>';
			else
				echo "<li><span style='padding-left:5px;'><a href='".$crumb['url']."' title='".$crumb['label']."'>".$crumb['label']."</a></span></li> ";
         } else{ 
		 	if ($crumb['level'] > 0)
           		echo '<li><img src="images/separator.png"><span style="position:relative;bottom:8px;padding-left:5px;">'.$crumb['label'].'</span></li>';
			else
				echo "<li><span style='padding-left:5px;position:relative;bottom:8px;'>".$crumb['label'].'</span>&nbsp;&nbsp;<img src="images/separator.png"></li>';
		 }
      }

      echo "</ul></div>";
   }
}
?>