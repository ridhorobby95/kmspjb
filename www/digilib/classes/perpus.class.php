<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('includes/query.class.php');
	
	class Perpus{
		
		//penyimpanan untuk data anggota
		function updateProfile($conn,$record,$key) {
			Query::recUpdate($conn,$record,'ms_anggota',"idanggota = '$key'");
		}
		
		function getPeriodePagu($conn){
			$periode = $conn->GetOne("select periodepagu
						 from pp_settingpagu where
							 current_date >= ((periodepagu::integer-1)||'-'||bulanstart||'-01')::date 
							 and current_date < ((periodepagu::integer)||'-'||bulanend::integer+1||'-14')::date ");
		
			return $periode;
		}
		
		function InsertBlob($conn,$record,$table,$path,$f_id,$f_blob){
			$err = Perpus::simpan($conn,$record,$table);
			if($err==0){
				$id = $conn->GetOne("select max($f_id) from $table");
				$conn->UpdateBlobFile($table,$f_blob,$path,"$f_id = $id");
				return $conn->ErrorNo();
			}else
				return $err;
		}
		
		function UpdateBlob($conn,$record,$table,$path,$f_id,$f_blob,$id){
			$err = Perpus::Update($conn,$record,$table,$f_id,$id);
			if($err==0){
				$conn->UpdateBlobFile($table,$f_blob,$path,"$f_id = $id");
				return $conn->ErrorNo();
			}else
				return $err;
		}
		
		function loadBlobFile($file, $type = NULL){
			if(empty($file)){
				return null;
			}else{
				if($type == 'jpg'){
					$image = new Imagick();
					$image->readimageblob($file);
					return "data:image/png;base64," .  base64_encode($image->getimageblob());
				}
			}
		}
		
		function simpan($conn,$record, $p_dbtable){
			return Query::recInsert($conn,$record,$p_dbtable);
		}
		
		function Update($conn,$record, $p_dbtable, $kondisi){
			return Query::recUpdate($conn,$record,$p_dbtable,$kondisi);
		}
		
		function Delete($conn,$p_dbtable,$kondisi){
			return Query::qDelete($conn,$p_dbtable,$kondisi);
		}
		
		function Updated($conn,$record,$key1,$key2,$key3) {
			$col = $conn->Execute("select * from pp_statistik where t_ipaddress='$key1' and t_datetime='$key2' and t_user='$key3'");
			$sql = $conn->GetUpdateSQL($col,$record);
			
			if($sql != '')
				$conn->Execute($sql);
			
			return $conn->ErrorNo();
		}
		
		function getCart(){
			$cart = $_SESSION['cart'];
			if (!$cart) {
				return 'Buku saya : 0 buku';
			} else {
				// Parse the cart session variable
				$items = explode(',',$cart);
				$s = (count($items) > 1) ? 's':'';
				return 'Buku saya : <a href="javascript:showCart()" style="font-weight:bold;text-decoration:underline;">'.count($items).' buku</a>';
			}
		}
		
		function Cart($action, $pustaka){
			$cart = $_SESSION['cart'];
			switch ($action) {
				case 'add':
					$newcart='';
					if ($cart) {
						$items = explode(',',$cart);
						for ($i=0;$i<count($items);$i++){ //pengecekkan bahwa data yang dipesan cuma 1 jenis buku saja
							$newcart = $cart;
							if ($pustaka != $items[$i]) 
								if ($cart)	
									$newcart .= ','.$pustaka;
								else
									$newcart = $pustaka;
							else
								$newcart = $cart;
						}
					} else {
						$newcart = $pustaka;
					}
					$cart = $newcart;
					break;
				case 'delete':
					if ($cart) {
						$items = explode(',',$cart);
						$newcart = '';
						foreach ($items as $item) {
							if ($pustaka != $item) {
								if ($newcart != '') {
									$newcart .= ','.$item;
								} else {
									$newcart = $item;
								}
							}
						}
						$cart = $newcart;
					}
					break;
				case 'hapuscentang':
					if ($cart) {
						foreach ($pustaka as $value) {
							$items = ($newcart != '') ? explode(',',$newcart) : explode(',',$cart);
							$newcart = '';
							for ($i=0;$i<count($items);$i++){
								if ($value != $items[$i]){
									if ($newcart != '')
										$newcart .= ','.$items[$i];
									else
										$newcart = $items[$i];
									
								}				
							}
						}
						$cart = $newcart;
					}
					break;
			}
			return $_SESSION['cart'] = $cart;
		}
				
		function saveCart($conn, $record, $p_dbtable){ 
			$tgltenggat = date('Y-m-d', strtotime('+3 days'));
						
			$tglhasil = Perpus::tglTenggat($conn, $tgltenggat);
			
			$record['tglexpired'] = $tglhasil;//date($data['tgltenggat'], strtotime("+1 day"));
			$record['ideksemplarpesan'] = $record['ideksemplar'];
			$record['statusreservasi']=1;
			$record['t_inserttime'] = date('Y-m-d H:i:s');
			
			//unset($_SESSION['cart']);
			return Query::recInsert($conn,$record,$p_dbtable);
		}
		
		function ajuanPanjang($conn, $idtrans){
			$p_dbtable = "pp_perpanjangan";
			
			$tglhasil = Perpus::tglTenggat($conn, $tgltenggat);
			$record =  array();
			$record['idtransaksi'] = $idtrans;
			$record['tglperpanjangan'] = date('Y-m-d');
			$record['statusajuan']=1;
			$record['t_inserttime'] = date('Y-m-d H:i:s');
			
			unset($_SESSION['cart']);
			return Query::recInsert($conn,$record,$p_dbtable);
		}
		
		function Perpanjang($conn,$ideksemplar, $idtrans){
			$p_dbtable = "pp_perpanjangan";
			//pengecekkan skorsing
			$issave = false;
			$cekajuan = $conn->GetOne("select count(*) from pp_perpanjangan where idtransaksi = $idtrans and statusajuan = 1 ");
			if($cekajuan > 0){
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal, transaksi sudah diajukan.',true).'</div>';
			}else{
				$dateskors = $conn->GetOne("select tglselesaiskors from ms_anggota where idanggota='$_SESSION[idanggota]'");
				if ((strtotime(date('Y-m-d')) > strtotime($dateskors)) or $dateskors == ''){
					$istanggung = $conn->GetOne("select to_char(tgltransaksi,'YYYY-MM-DD') from pp_transaksi where idanggota='$_SESSION[idanggota]' and idtransaksi=$idtrans ");
					if ($istanggung == date('Y-m-d')){
						$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal, transaksi terjadi dihari yang sama.',true).'</div>';									
					}else{
						//pengecekkan pemesanan
						$ispesan = $conn->GetOne("select 1 from pp_reservasi where ideksemplarpesan='$ideksemplar' and to_char(tglexpired,'YYYY-MM-DD') > to_char(current_date,'YYYY-MM-DD') and statusreservasi='1' and rownum <= 1");
						if ($ispesan)
							$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Maaf Buku sudah ada yang memesan',true).'</div>';	
						else {
							//mendapatkan data eksemplar
							$sql = "select kdklasifikasi, p.kdjenispustaka from pp_eksemplar e 
									left join ms_pustaka p on p.idpustaka=e.idpustaka where ideksemplar='$ideksemplar'";
							$data = $conn->GetRow($sql);
							
							if (!empty($data)){
								//pecekkan aturan ada 
								$sql = "select * from pp_aturan where kdjenisanggota='".$_SESSION['jenisanggota']."'";
								$isrole = $conn->GetRow($sql);
								if (!empty($isrole)){
									//mendapatkan transaksi 
									$trans = $conn->GetRow("select t.*, to_char(tgltenggat,'YYYY-mm-dd') tgl_tenggat,
											       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg,  
												to_char(t.tgltransaksi, 'YYYY-MM-DD') tgltrans 
												from pp_transaksi t
												where idtransaksi=$idtrans");
									$r_jamdenda=floor($trans['jamdendaskrg']);
																								
									//pengcekkan jumlah x perpanjangan 
									if (Helper::cNumZero($trans['perpanjangke']) < $isrole['jumlahperpanjangan']){
										$p_dbtable = "pp_perpanjangan";
	
										$tglhasil = Perpus::tglTenggat($conn, $tgltenggat);
										$record =  array();
										$record['idtransaksi'] = $idtrans;
										$record['tglperpanjangan'] = date('Y-m-d');
										$record['perpanjanganke'] = Helper::cNumZero($trans['perpanjangke']) + 1;
										$record['statusajuan']=1;
										$record['t_inserttime'] = date('Y-m-d H:i:s');
										Helper::Identitas($recupdate);
										Query::recInsert($conn,$record,$p_dbtable);
	
										if($conn->ErrorNo() != 0)
											$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal.',true).'</div>';	
										else{
											$issave = true;
											$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Pengajuan Perpanjangan berhasil ditambahkan.<br/>Konfirmasi pengajuan akan dikirim melalui email.').'</div>';	
										}
	
									}else
										$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Ketentuan jumlah perpanjangan berakhir',true).'</div>';	
								}else
									$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Aturan tidak ditemukan',true).'</div>';	
							}
						}
					}
				}else
					$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Anda dalam masa Skorsing',true).'</div>';			
			}
			
			if ($issave)
				Helper::addHistory($conn, "Perpanjangan Berhasil", Helper::removeSpecial(http_build_query($record)));
			else
				Helper::addHistory($conn, $confirm, "");
			
			return $confirm;						
		}
		
		function Perpanjang_0pd($conn,$ideksemplar, $idtrans){

			//pengecekkan skorsing
			$issave = false;
			$dateskors = $conn->GetOne("select tglselesaiskors from ms_anggota where idanggota='$_SESSION[idanggota]'");
			if ((strtotime(date('Y-m-d')) > strtotime($dateskors)) or $dateskors == ''){
				$istanggung = $conn->GetOne("select to_char(tgltransaksi,'YYYY-MM-DD') from pp_transaksi where idanggota='$_SESSION[idanggota]' and idtransaksi=$idtrans ");
				if ($istanggung == date('Y-m-d')){
					$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal, transaksi terjadi dihari yang sama.',true).'</div>';									
				}else{
					//pengecekkan pemesanan
					$ispesan = $conn->GetOne("select 1 from pp_reservasi where ideksemplarpesan='$ideksemplar' and to_char(tglexpired,'YYYY-MM-DD') > to_char(current_date,'YYYY-MM-DD') and statusreservasi='1' and rownum <= 1");
					if ($ispesan)
						$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Maaf Buku sudah ada yang memesan',true).'</div>';	
					else {
						//mendapatkan data eksemplar
						$sql = "select kdklasifikasi, p.kdjenispustaka from pp_eksemplar e 
								left join ms_pustaka p on p.idpustaka=e.idpustaka where ideksemplar='$ideksemplar'";
						$data = $conn->GetRow($sql);
						
						if (!empty($data)){
							//pecekkan aturan ada 
							$sql = "select * from pp_aturan where kdjenisanggota='".$_SESSION['jenisanggota']."'";
							$isrole = $conn->GetRow($sql);
							if (!empty($isrole)){
								//mendapatkan transaksi 
								$trans = $conn->GetRow("select t.*, to_char(tgltenggat,'YYYY-mm-dd') tgl_tenggat,
										       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg,  
											to_char(t.tgltransaksi, 'YYYY-MM-DD') tgltrans 
											from pp_transaksi t
											where idtransaksi=$idtrans");
								$r_jamdenda=floor($trans['jamdendaskrg']);
																							
								//pengcekkan jumlah x perpanjangan 
								if (Helper::cNumZero($trans['perpanjangke']) < $isrole['jumlahperpanjangan']){

									//Cek apakah telat/ga
									if ($trans['tgl_tenggat']<date('Y-m-d')){
										$tglhariini=date("Y-m-d");
										$p_libur_telat=$conn->GetRow("select count(tgllibur) as jmllibur
													     from ms_libur
													     where to_char(tgllibur,'YYYY-mm-dd') >= '".$trans['tgl_tenggat']."'
														and to_char(tgllibur,'YYYY-mm-dd') <= '$tglhariini'
														and to_char (tgllibur, 'D')!=0
														and to_char (tgllibur, 'D')!=6
													     ");

										$range=(strtotime($tglhariini)-strtotime($trans['tgl_tenggat']))/86400;
										
										//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
										$jmlsabtuminggu=0;
										for($k=1;$k<=$range;$k++){
											$tanggaljalan=date("Y-m-d",(strtotime($trans['tgl_tenggat'])+($k*86400)));
											if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
												$jmlsabtuminggu +=1;
											}
										}

										//insert denda ke tabel pp_denda dan update field denda di ms_anggota
										if($isrole['satuanpinjam'] == "0"){
											$jml_denda = ($range - $p_libur_telat['jmllibur'] - $jmlsabtuminggu) * (int)$isrole['denda'];
										}
										elseif($isrole['satuanpinjam'] == "1")
											$jml_denda = ($r_jamdenda - ($p_libur_telat['jmllibur']*24) - ($jmlsabtuminggu*24) ) * (int)$isrole['denda'];

										
										$_SESSION['denda'] = $jml_denda;
										$recdenda=array();
										Helper::Identitas($recdenda);
										$recdenda['idtransaksi']=$trans['idtransaksi'];
										$recdenda['keterangan']='TERLAMBAT';
										$recdenda['jml_denda']= $jml_denda;
										$recdenda['idanggota']= $trans['idanggota'];
										$recdenda['kdklasifikasi']= $trans['kdklasifikasi'];
						
										if($isrole['satuanpinjam'] == "0") #hari
											$recdenda['jmlhari'] = Perpus::hitung_telat($conn,$trans['tgl_tenggat'],$tglhariini);
										elseif($isrole['satuanpinjam'] == "1") #jam
											$recdenda['jmljam'] = Perpus::hitung_telatjam($conn,$trans['tgl_tenggat'],$tglhariini,floor($trans['jamdendaskrg']));
							
										if($jml_denda != 0){
											Query::recInsert($conn,$recdenda,'pp_denda');
										}
										
										//akumulasi jumlah denda dengan denda yang lama jika masih punya denda
										$sql_cek_denda_lama = $conn->GetRow("select denda from ms_anggota where idanggota='".$trans['idanggota']."'");
										$akumulasi_denda = (int)$sql_cek_denda_lama['denda'] + $jml_denda;
										$recdendaanggota['denda'] = $akumulasi_denda;
										if($jml_denda != 0){
											Query::recUpdate($conn,$recdendaanggota,'ms_anggota',"idanggota = '".$trans['idanggota']."'");
										}
									}
									
									if ($isrole['lamaperpanjangan']!=''){
										#per jam
										if($isrole['satuanpinjam'] == "1"){
											$tglhasil=date("Y-m-d H:i:s", mktime(date("H")+$isrole['lamaperpanjangan']));
										}
										#per hari
										elseif($isrole['satuanpinjam'] == "0"){
											$tglm=date("Y-m-d");
											$tgl=strtotime(date("Y-m-d"));
											$tgl2=($tgl)+(86400*$isrole['lamaperpanjangan']);
											$jum=0;
											$i=$tgl2; #tgl tenggat
											
											$h=date("w",$i);
											if($h==0){#minggu
													$jum +=1;
												}
												
											if($h==6){ #sabtu
													$jum +=2;
												}
											$tottgl=($tgl2+($jum*86400));
											
											$tgltenggat=date("Y-m-d",$tottgl);
											$tglrange = date("Y-m-d",($tottgl+(86400*30)));
											
											$tglhasil = Perpus::tglTenggat($conn, $tgltenggat);
										}	
									}
									
									//record penyimpanan
									$record = array();
									$record['kdjenistransaksi'] = $trans['kdjenistransaksi'];
									$record['ideksemplar'] = $ideksemplar;
									$record['idanggota'] = $_SESSION['idanggota'];
									$record['kdlokasi'] = $trans['kdlokasi'];
									$record['tgltransaksi'] = date('Y-m-d H:i:s');//$trans['tgltransaksi'];
									$record['perpanjangke'] = Helper::cNumZero($trans['perpanjangke']) + 1;
									$record['tglperpanjang'] = date('Y-m-d H:i:s');
									$record['tgltenggat'] = $tglhasil;
									$record['statustransaksi'] = 1;
									Helper::Identitas($record);
									
									//record update 
									$recupdate = array();
									$recupdate['statustransaksi'] = 2; //status perpanjang
									$recupdate['tglpengembalian'] = date('Y-m-d H:i:s');
									Helper::Identitas($recupdate);
									$pjg = $conn->GetOne("select perpanjangke from pp_transaksi where ideksemplar=$ideksemplar and idanggota='$_SESSION[idanggota]' and tglpengembalian is null and perpanjangke='$record[perpanjangke]'");
									if(!$pjg){
										Query::recInsert($conn,$record,'pp_transaksi');

										if($conn->ErrorNo() != 0)
											$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal.',true).'</div>';	
										else{
											Query::recUpdate($conn,$recupdate,'pp_transaksi',"idtransaksi = '$idtrans'");
											$issave = true;
											$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan berhasil.').'</div>';	
										}
									}
								}else
									$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Ketentuan jumlah perpanjangan berakhir',true).'</div>';	
							}else
								$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan gagal. Aturan tidak ditemukan',true).'</div>';	
						}
					}
				}
			}else
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Anda dalam masa Skorsing',true).'</div>';			
			
			if ($issave)
				Helper::addHistory($conn, "Perpanjangan Berhasil", Helper::removeSpecial(http_build_query($record)));
			else
				Helper::addHistory($conn, $confirm, "");
			
			return $confirm;						
		}
		
		function batalPesan($conn,$record,$p_dbtable,$key){
			return Query::recUpdate($conn,$record,$p_dbtable,"idreservasi = '$key'");
		}
		
		function updateHits($conn,$hits,$key){
			$record['hits'] = $hits + 1;
			return Query::recUpdate($conn,$record,'pp_kontakonline',"idkontakol = '$key'");
		}
		
		function updateRe($conn,$key){
			$rehits = $conn->GetOne("select rehit from pp_kontakonline where idkontakol = $key");
			$record['rehit'] = $rehits + 1;
			return Query::recUpdate($conn,$record,'pp_kontakonline',"idkontakol = '$key'");
		}
		
		function sendChangePass($user,$pass,$email,$nama) {
			$subject = 'Reset Password e-Digilib PJB';
						
			$body = 'Anda telah mengganti password anda. Data loginnya adalah:'."\n\n";
			$body .= 'Username: '.$user."\n";
			$body .= 'Password: '.$pass."\n\n";
			$body .= 'Anda bisa mengganti password setelah berhasil login ke e-Digilib PJB.'."\n";
			$body .= 'Untuk bisa menggunakan fasilitas e-Digilib PJB, anda harus melakukan login tersebut.'."\n\n";
			$body .= 'Terima kasih';
			
			$ok = Helper::sendMail($email,$nama,$subject,$body);
			if($ok) $err = 0; else $err = -1;
			
			return $err;
		}
		
		function insPengusulanDosen($conn,$record, $idorder, $periode) {
			$conn->BeginTrans();
			$recupdate = array();
			$periode = Perpus::getPeriodePagu($conn);
			if(count($record) > 0){
				foreach($record as $kdet => $vdet){
					$detail = $vdet;
					if ($detail['idanggota'] == $_SESSION['idanggota'])
						$detail['ispengusulutama'] = 1;
					
					Helper::Identitas($detail);
					$detail['periodeakad'] = $periode;
					$detail['idorderpustaka'] = $idorder;
					
					$isOK = $conn->GetOne("select 1 from ms_anggota where idanggota='$detail[idanggota]'");
					
					if ($isOK){
						$err = Perpus::Simpan($conn,$detail,'pp_paguusulan');
						
						$pagusementara = $conn->GetOne("select sisasementara from pp_pagulh where periodeakad='$periode' and idanggota='$detail[idanggota]'");
						
						$recupdate['sisasementara'] = $pagusementara - $detail['paguusulan'];	
						Query::recUpdate($conn,$recupdate,'pp_pagulh'," periodeakad='$periode' and idanggota='$detail[idanggota]'");
					}
				}
			}
			if($err == 0)
				$conn->CommitTrans();
			else
				$conn->RollbackTrans();	
					
			return $err;
		}
		
		function delPaguUsulan($conn,$rdel){
			$record = array();
			$pagu = $conn->GetRow("select paguusulan,periodeakad,idanggota from pp_paguusulan where idpaguusulan=$rdel");
			$isPagu = $conn->GetOne("select sisasementara from pp_pagulh where periodeakad='$pagu[periodeakad]' and idanggota='$pagu[idanggota]'");

			$record['sisasementara'] = $isPagu + $pagu['paguusulan'];
			$err = Query::recUpdate($conn,$record,'pp_pagulh'," periodeakad='$pagu[periodeakad]' and idanggota='$pagu[idanggota]'");
			
			if ($err == 0)
				Query::qDelete($conn,'pp_paguusulan'," idpaguusulan=$rdel");
				
			return $err;
		}
		
		function msgReady($conn,$ideksemplar){
			$rs = $conn->GetOne("/*untuk pesan pemberitahuan kalau buku yang dipesan sudah ada*/
					select t.idtransaksi, e.noseri, p.judul, t.tglpengembalian, l.namalokasi 
					from pp_transaksi t
					join pp_eksemplar e on e.ideksemplar = t.ideksemplar 
					join ms_pustaka p on p.idpustaka = e.idpustaka 
					join lv_lokasi l on l.kdlokasi = e.kdlokasi 
					/*status transaksi sudah kembali, eksemplar ada, reservasi ada */
					where t.ideksemplar = '$ideksemplar' and t.statustransaksi = '0' and e.statuseksemplar = 'ADA'
						and (select count(*) from pp_reservasi where statusreservasi = '1' and ideksemplarpesan = t.ideksemplar
						and tglexpired >= current_date) > 0 
					and rownum <= 1
					order by t.tglpengembalian desc ");

			$record['sisasementara'] = $isPagu + $pagu['paguusulan'];
			$err = Query::recUpdate($conn,$record,'pp_pagulh'," periodeakad='$pagu[periodeakad]' and idanggota='$pagu[idanggota]'");
			
			if ($err == 0)
				Query::qDelete($conn,'pp_paguusulan'," idpaguusulan=$rdel");
				
			return $err;
		}
		
		function hitung_telat ($conn, $tglkembali, $tglcek){
			$conn->debug = false;
			//Mencari jml hari yg diset libur
			$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur from ms_libur
					       where to_char(tgllibur,'YYYY-mm-dd') >= '$tglkembali' and to_char(tgllibur,'YYYY-mm-dd') <= '$tglcek'
					       and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6");
				
			//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
			$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;
			$jmlsabtuminggu=0;
			for($k=1;$k<=$range_sm;$k++){
				$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
				if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
					$jmlsabtuminggu +=1;
				}
			}
			
			$jml_telat = (int)$range_sm - ((int)$p_libur['jmllibur'] + $jmlsabtuminggu);
			return $jml_telat; // hitung jml hari telat
			//========================================================
					
		}
		
		function hitung_telatjam ($conn, $tglkembali, $tglcek, $jamdenda){
			$conn->debug = false;
			//Mencari jml hari yg diset libur
			$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur
					       from ms_libur where to_char(tgllibur,'YYYY-mm-dd') >= '$tglkembali' and to_char(tgllibur,'YYYY-mm-dd') <= '$tglcek'
					       and to_char (tgllibur, 'D')!=0 and to_char (tgllibur, 'D')!=6
					       ");
			
				
			//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
			$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;
			$jmlsabtuminggu=0;
			for($k=1;$k<=$range_sm;$k++){
				$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
				if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
					$jmlsabtuminggu +=1;
				}
			}
			
			$jml_telat = (int)$jamdenda - ((int)($p_libur['jmllibur']*24) + ($jmlsabtuminggu*24));
			return $jml_telat; // hitung jml hari telat
			//========================================================
					
		}
		
		function getFoto2($conn,$idanggota){
			$a = $conn->GetRow("select idpegawai, kdjenisanggota from ms_anggota where idanggota = '$idanggota' ");
			$idpegawai = $a['idpegawai'];
			$kdjenisanggota = $a['kdjenisanggota']; 
			
			if($kdjenisanggota == "U"){#Umum
				$dirfoto = Config::dirFoto."anggota/".trim($idanggota).'.jpg';
				$viewfoto = Config::fotoUrl."anggota/".trim($idanggota).'.jpg';	
			}elseif($kdjenisanggota == "A" or $kdjenisanggota == "M"){#Alumni, Mahasiswa
				// koneksi ke akademik
				$conna = Factory::getConnAkad();
				#akad
				$mhs = $conna->GetRow("select substring(periodemasuk,1,4) angkatan, mhsid, nim from akademik.ms_mahasiswa where nim = '$idanggota'");
				$fold = $mhs['angkatan']."/".trim($idanggota).'.jpg';
				$dirfoto = Config::dirFotoMhs.$fold;
	
				if(!(is_file($dirfoto))){
					$fold = "mahasiswa/".md5($mhs['mhsid'].'asdf1234').'.jpg';
					$dirfoto = Config::dirFotoMhs.$fold;
				}
				$viewfoto = Config::fotoMhsUrl.$fold;
			}elseif($kdjenisanggota == "D" or $kdjenisanggota == "T"){#Dosen, Karyawan
				$dirfoto = Config::dirFotoPeg.trim($idpegawai).'.jpg';
				$viewfoto = Config::fotoPegUrl.trim($idpegawai).'.jpg';
			}
	
			if(is_file($dirfoto)){
				$v_foto = $viewfoto;
			}else{
				$v_foto = Config::fotoUrl."default1.jpg";
			}
			
			return $v_foto;
		}
		
		function isDownload($noseri,$file){
			$conn = Factory::getConn();
			$isDownload = $conn->GetOne("select download 
						from pustaka_file f
						join ms_pustaka p on p.idpustaka = f.idpustaka 
						where p.noseri = '$noseri' and f.files = '$file' ");
			return $isDownload;
		}
		
		function isOpen($noseri,$file){
			$conn = Factory::getConn();
			$isDownload = $conn->GetOne("select login 
						from pustaka_file f
						join ms_pustaka p on p.idpustaka = f.idpustaka 
						where p.noseri = '$noseri' and f.files = '$file' ");
			return $isDownload;
		}
		
		#hitung denda per buku
		function hitungdendaperbuku($tglkembali, $tglcek, $idanggota, $ideksemplar ){
			$conn = Factory::getConn();

			$rpdenda = 0;
			//Mencari jml hari yg diset libur
			$p_libur=$conn->GetRow("select count(tgllibur) as jmllibur
				       from ms_libur
				       where to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') >= '$tglkembali'
						and to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') <= '$tglcek'
						and to_char (tgllibur, 'D')!=0
						and to_char (tgllibur, 'D')!=6
				       order by tgllibur");
			$p_trans=$conn->GetRow("select a.kdjenisanggota, idtransaksi,ideksemplar, kdjenispustaka,v.idanggota, tgltransaksi, tgltenggat, kdklasifikasi,
				       (SYSDATE - TO_DATE(to_char(tgltenggat,'yyyy-mm-dd HH24:mi:ss'),'yyyy-mm-dd HH24:mi:ss')) *24 jamdendaskrg 
				       from v_trans_list v
				       left join ms_anggota a on a.idanggota=v.idanggota
				       where ideksemplar=$ideksemplar and statustransaksi='1' ");
			
			//mendapatkan jumlah hari sabtu dan minggu untuk keterlambatan
			$range_sm=(strtotime($tglcek)-strtotime($tglkembali))/86400;
			$jmlsabtuminggu=0;
			for($k=1;$k<=$range_sm;$k++){
				$tanggaljalan=date("Y-m-d",(strtotime($tglkembali)+($k*86400)));
				if(date("w",strtotime($tanggaljalan))==0 or date("w",strtotime($tanggaljalan))==6){ //jika hari minggu atau sabtu
					$jmlsabtuminggu +=1;
				}
			}
			//========================================================
					
			$sql_denda = $conn->GetRow("select denda, satuanpinjam
					from pp_aturan
					where kdjenisanggota='".$p_trans['kdjenisanggota']."'");
			
			if($sql_denda['denda'] > 0){
				if($sql_denda['satuanpinjam'] == '0'){ #status per hari
					$hitungmldenda = ((strtotime($tglcek)-strtotime($tglkembali))/86400) - $jmlsabtuminggu; //brp hari telatnya, sdh dikurangi sabtu minggu
					if($hitungmldenda > 0){ // jika telat
						$rpdenda = ($hitungmldenda-(int)$p_libur['jmllibur']) * $sql_denda['denda'];
					}else{
						$rpdenda = 0;
					}
				}elseif($sql_denda['satuanpinjam'] == '1'){ #status per jam
					$hitungmldenda = floor($p_trans['jamdendaskrg']) - ($jmlsabtuminggu*24);
					if($hitungmldenda > 0){ // jika telat
						$rpdenda = ($hitungmldenda-((int)$p_libur['jmllibur']*24)) * $sql_denda['denda'];
					}else{
						$rpdenda = 0;
					}
				}
			}else{
				$rpdenda = 0; //bukan yang ada di tabel pp_aturan
			}
			
			return $rpdenda;
		}
		
		function tglTenggat($conn, $tgltenggat){ 
			$tottgl = strtotime($tgltenggat);
			$tglrange = date("Y-m-d",($tottgl+(86400*30)));  
	
			$p_libur=$conn->GetArray("select tgllibur
						from ms_libur
						where to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') >= to_date('$tgltenggat','YYYY-mm-dd')
							and to_date(to_char(tgllibur,'YYYY-mm-dd'),'YYYY-mm-dd') <= to_date('$tglrange','YYYY-mm-dd')
						and to_char(tgllibur, 'D')!=0 and to_char(tgllibur, 'D')!=6
						order by tgllibur ");
			
			$thk=0;
			$tgllibur = $tgltenggat;
			$libur = array();
			if(count($p_libur)>0){
				foreach($p_libur as $r_lib){
					if($r_lib['tgllibur']==$tgllibur){
						$thk +=1;
						$tgllibur=date("Y-m-d",(strtotime($tgllibur)+86400));
						if(date("w",strtotime($tgllibur))==0){
							$thk +=1;
							$tgllibur=date("Y-m-d",(strtotime($tgllibur)+86400));
						}
					}else
						break;		
				}
				
				foreach($p_libur as $key => $value){
					foreach($value as $k => $v){
						$libur[] = $v;
					}
				}
			}else
				$thk=0;
				
			$tglt=(($tottgl)+(86400*$thk)); 
			$jum=0;
			$i=$tglt;
			
			$h=date("w",$i);
			if($h==0){ #minggu
					$jum +=1;
				}
				
			if($h==6){ #sabtu
					$jum +=2;
				}
			$tglh=($tglt+($jum*86400)); //waktu max pinjm 14 + jml minggu 2 = 16
			$tglhasil=date("Y-m-d",$tglh);
	
			if(count($p_libur)>0){
				if(in_array($tglhasil,$libur)){
					return Perpus::tglTenggat($conn, $tglhasil);
				}else
					return $tglhasil;
			}
			
			
			return $tglhasil;
		}
		
		#check unit eksemplar & peminjam/user
		function checkUnit_old($conn, $idanggota, $eksemplar){
			$satker = $conn->GetOne("select idunit from ms_anggota where idanggota = '$idanggota' ");
			$unit = $conn->GetOne("select l.idunit from pp_eksemplar e
					join lv_lokasi l on l.kdlokasi = e.kdlokasi
					where e.ideksemplar = $eksemplar ");
			if($satker == $unit)
				return true;
			else
				return false;
		}
		
		function unita(){
			$unita = explode("_##_",Config::acceptUnit);
			return $unita;
		}
		
		function UnitAccept(){
			$unit = Perpus::unita();
			if(in_array($_SESSION['unituser'],$unit))
				return true;
			else
				return false;
		}
		
		function checkUnit($conn, $idanggota, $eksemplar){
			$satker = $conn->GetOne("select idunit from ms_anggota where idanggota = '$idanggota' ");
			$unit = Perpus::unita();
			if(in_array($satker,$unit))
				return true;
			else
				return false;
		}
		
	}
?>
