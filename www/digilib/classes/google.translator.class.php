<?php
	date_default_timezone_set("Asia/Jakarta");
	//In new version, char set is not valid if you forget to set a user agent!
	defined ('DEFAULT_USER_AGENT') || define('DEFAULT_USER_AGENT',
			'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7');
    if (extension_loaded('cURL')) {
		function fetch($url){
			$ch = curl_init();
			//$timeout = 30;
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			//curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
		
		function submit($url,$post_var){
			$fields_string='';
			foreach($post_var as $key=>$value) { 
				$fields_string .= $key.'='.$value.'&'; 
			}
			rtrim($fields_string,'&');

			//open connection
			$ch = curl_init();

			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_USERAGENT,DEFAULT_USER_AGENT);
			curl_setopt($ch,CURLOPT_POST,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);

			//execute post
			$data = curl_exec($ch);

			//close connection
			curl_close($ch);
			return $data;
		}		
	}else{
		require_once('Snoopy' . DIRECTORY_SEPARATOR . 'Snoopy.class.php');
		function fetch($url){
			$snoopy=new Snoopy();
			$snoopy->fetch($url);
			return $snoopy->results;
		}
		
		function submit($url,$post_var){
			var_dump($url);
			var_dump($post_var);
			$snoopy=new Snoopy();
			$snoopy->rawheaders['User-Agent']=DEFAULT_USER_AGENT;
			$snoopy->submit($url,$post_var);
			return $snoopy->results;
		}
	}
    
    if ( !function_exists('json_decode') ){
       require_once('json/JSON.php');
       function my_json_decode($text){ 
           $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
           $result=$json->decode($text);
        } 
    } 
    /*A simple class to use Google translator service in server side 
     * applications*/
    class GTranslator {
        var $_key;
        
        var $_valids=array("auto"=> "Detect language","af"=> "Afrikaans","sq"=> "Albanian","ar"=> "Arabic",
        "be"=> "Belarusia","bg"=> "Bulgarian","ca"=> "Catalan","zh-CN"=> "Chinese","hr"=> "Croatian",
        "cs"=> "Czech","da"=> "Danish","nl"=> "Dutch","en"=> "English","et"=> "Estonian","tl"=> "Filipino",
        "fi"=> "Finnish","fr"=> "French","gl"=> "Galician","de"=> "German","el"=> "Greek","iw"=> "Hebrew",
        "hi"=> "Hindi","hu"=> "Hungarian","is"=> "Icelandic","id"=> "Indonesia","ga"=> "Irish","it"=> "Italian",
        "ja"=> "Japanese","ko"=> "Korean","lv"=> "Latvian","lt"=> "Lithuania","mk"=> "Macedonia","ms"=> "Malay",
        "mt"=> "Maltese","no"=> "Norwegian","fa"=> "Persian","pl"=> "Polish","pt"=> "Portugues","ro"=> "Romanian",
        "ru"=> "Russian","sr"=> "Serbian","sk"=> "Slovak","sl"=> "Slovenian","es"=> "Spanish","sw"=> "Swahili",
        "sv"=> "Swedish","th"=> "Thai","tr"=> "Turkish","uk"=> "Ukrainian","vi"=> "Vietnames","cy"=> "Welsh",
        "yi"=> "Yiddish");
        
        var $_valid_id=array();   

        
        function GTranslator($key='notsupplied'){
            $this->__construct($key);
        }
        
        function __construct($key='notsupplied'){
            $this->_key=$key;
            $this->_valid_id=array_keys($this->_valids);
        }   
        
        function detect_language($text){
            if (strlen($text)>50) $text=substr($text,0,50);
            $url=sprintf("http://www.google.com/uds/GlangDetect?q=\"%s\"&key=%s&v=1.0",urlencode($text),$this->_key); 
            return $this->_translate_response(fetch($url));   
        }
        
        function translate($text,$from,$to){
            trigger_error("translate function is not realible. use translate2.",E_USER_NOTICE);
            if (!in_array($from,$this->_valid_id) || !in_array($to,$this->_valid_id)) return false;
            if ($to=="auto") return false;
            $from=($from=="auto")?"":$from;
            $post_var['q']=$text;
            $formats=$from.'|'.$to;
            $url=sprintf("http://www.google.com/uds/Gtranslate?langpair=%s&key=%s&v=1.0",$formats,$this->_key);
            return $this->_translate_response(submit($url,$post_var));           
        }

        function translate2($text,$from,$to){
            if (!in_array($from,$this->_valid_id) || !in_array($to,$this->_valid_id)) return false;
            if ($to=="auto") return false;
            $post_var['text']=$text;
            $url=sprintf("http://translate.google.com/translate_a/t?client=t&sl=%s&tl=%s",$from,$to);
            return $this->_translate_response(submit($url,$post_var));           
         }
        
        function _translate_response($text){
            //A simple workaround, to fix empty array element in json array.
            $text=str_replace(",,",',"",',$text);
            $result=json_decode($text,true);
            return $result; 
        }
        
        function get_name_of($id) {
            if (isset($this->_valids[$id])) return $this->_valids[$id];
            else return false;   
        }
        
        function get_valid_language(){
            return $this->_valids;   
        }
    }
?>
