<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once("classes/perpus.class.php");
	
	include_once('_language.php');
	
	//settingan web
	$p_window = "[Digilib] Digital Library PJB";
	
	//untuk mendapatkan statistik dan user yang online
	$record = array();
	$record['t_ipaddress'] = Helper::cStrNull($_SERVER['REMOTE_ADDR']);
	$record['t_datetime'] = date('Y-m-d');
	$record['t_updatetime'] = date('Y-m-d h:i:s');
	$record['time_online'] = time();
	$record['t_user'] = ($_SESSION['userid']?$_SESSION['userid']:"Pengunjung");
	$timelimit = time() - 30;
	$y = date('Y');
	//echo time();	
	//pengcekkan apakah user sudah mengakases hari ini
	$sql = "select 1 from pp_statistik where t_ipaddress='$record[t_ipaddress]' and t_datetime='$record[t_datetime]' and t_user = '$record[t_user]' ";
	$isakses = $conn->GetOne($sql);
	
	if (empty($isakses))
		Perpus::simpan($conn, $record, 'pp_statistik');
	else
		Perpus::Updated($conn,$record,Helper::cStrNull($_SERVER['REMOTE_ADDR']),date('Y-m-d'),($_SESSION['userid']?$_SESSION['userid']:"Pengunjung"));
		
	//untuk mendapatkan data pengunjung
	$pengunjung = $conn->GetOne("select count(*) from pp_statistik where t_datetime = '$record[t_datetime]'");
	$online = $conn->GetOne("select count(*) from pp_statistik where time_online > '$timelimit'");
	$totalpengunjung = $conn->GetOne("select count(*) from pp_statistik where to_char(t_datetime, 'YYYY') = '$y' ");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? /*<meta http-equiv="refresh" content="3" /> */ ?>
<title><?= $p_window; ?></title>
</head>
<body>

	<p><span class="statTitle"><?= $sidebar['ST_PENGUNJUNG']; ?> <?= $pengunjung?></span><br/>
	<span class="statTitle"><?= $sidebar['ST_ONLINE']; ?> <?= $online?></span><br/>
	<span class="statTitle"><?= $sidebar['ST_TOTAL']; ?> <?= $totalpengunjung?></span></p>
</body>
</html>
