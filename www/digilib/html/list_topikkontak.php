<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$b_link = new Breadcrumb();
	$b_link->add("Topik Kontak Online", Helper::navAddress('list_topikkontak.php'), 0);
		
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['T_CONTACT'];
	$p_title = $form['T_CONTACT'];
	$p_dbtable = "lv_kontak";
	
	//mendapatkan topik kontak untuk parent
	$p_sqlstr = "select * from $p_dbtable where isaktif=1 and idparent is null order by idkontak";
	$rs = $conn->Execute($p_sqlstr);
	
	//mendapatkan topik kontak untuk topik
	$p_sqlstr = "select k.*, ol.judul||'|'||ol.t_author||'|'||ol.t_input as last 
		from lv_kontak k
		left join pp_kontakonline ol on k.idkontak = ol.idkontak
		where k.isaktif=1 
		and k.idparent is not null
		and ol.idparentol is null 
		order by k.idparent,k.idkontak,ol.t_input desc  "; 
	$rsa = $conn->Execute($p_sqlstr);

	$data = array();
	while ($rowa = $rsa->FetchRow()){
		$data[$rowa['idparent'].'nama'][] = $rowa['namakontak'];
		$data[$rowa['idparent'].'id'][] = $rowa['idkontak'];
		$data[$rowa['idparent'].'ket'][] = $rowa['keterangan'];
		$data[$rowa['idparent'].'last'][] = $rowa['last'];
	}
	
	//untuk mendapatkan total replies
	$countre = $conn->GetOne("select count(*) from pp_kontakonline where idparentol is not null");
	//untuk mendapatkan total post
	$countpost = $conn->GetOne("select count(*) from pp_kontakonline where idparentol is null");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	<?= $b_link->output(); ?>
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $p_title; ?></h4>
        	<?= (!$confirm) ?  '' : $confirm; ?>
			<? 
				$i=0;
				while ($row = $rs->FetchRow()){ $i++;
			?>
			<div class="tborder" width="400">
				<div style="padding: 5px 5px 5px 10px;" class="catbg">
					<?= $row['namakontak']?>
				</div>
				<? for($j=0;$j<count($data[$row['idkontak'].'nama']);$j++) {
					list($judul,$nama,$time) = explode('|',$data[$row['idkontak'].'last'][$j]);
				?>
				<table width="100%" cellspacing="1" cellpadding="5" border="0" class="containerTable">
					<tbody>
						<tr>
							<td class="windowbg" valign="top">
								<b><a href="#" title="Lihat Topik" onClick="goShow('<?= $data[$row['idkontak'].'id'][$j]; ?>')"><?= $data[$row['idkontak'].'nama'][$j]; ?></a></b><br><?= $data[$row['idkontak'].'ket'][$j]; ?>
							</td>
							<td width="35%" valign="middle" class="windowbg2" style="padding:5px">
								<span class="smalltext">
								<b><?= $form['LASTEST_POST']; ?></b> <?= $time?> <br> 
								<?= $form['BY']; ?> <?= $nama; ?> <?= $form['AT']; ?> <?= $judul; ?>
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			<br/>
				<? }
				
				if(count($data[$row['idkontak'].'nama'])==0){
				
				?>
				<table width="100%" cellspacing="1" cellpadding="5" border="0" class="containerTable">
					<tbody>
						<tr>
							<td class="windowbg" colspan=2>Data tidak ada</td>
						</tr>
					</tbody>
				</table>
				<?
				}
				?>
			</div>
			<? } ?>
			<br/><br/>
			<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
				<tr>
					<th colspan="6"><img src="images/chart_curve.png" />&nbsp;<?= $sidebar['STATISTIK']; ?></th>
				</tr>
				<tr>
					<td><b><?= $form['TOT_TOPIC']; ?></b></td>
					<td><?= $rsa->RowCount();?></td>
					<td><b><?= $form['TOT_POST']; ?></b></td>
					<td><?= $countpost; ?></td>
					<td><b><?= $form['TOT_REP']; ?></b></td>
					<td><?= $countre; ?></td>
				</tr>
			</table>
		</div>
	</div>

</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	function goShow(key) {
		sent = "&key=" + key;
		goLink('contents','<?= Helper::navAddress('list_detailkontak.php');?>', sent);
	}
</script>
</html>
