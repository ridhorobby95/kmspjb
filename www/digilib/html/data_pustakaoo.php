<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//reload file
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);

	$b_link = new Breadcrumb();
	$b_link->add($menu['BIBLIO'], Helper::navAddress('data_pustaka.php').'&key='.$r_key, 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$menu['BIBLIO'];
	$p_dbtable = "ms_pustaka";
	
	$sql = "select p.*, j.namajenispustaka,j.islokal, b.namabahasa
			from $p_dbtable p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$r_key'";
	$row = $conn->GetRow($sql);
	
	$sqlFile = "select idpustakafile, files, login, download from pustaka_file where idpustaka = '$r_key' order by idpustakafile ";
	$rsFile = $conn->GetArray($sqlFile);
	
	//setting untuk halaman
	$p_title = $form['DET_SEE'].' '.ucwords(strtolower($row['namajenispustaka']));
	
	// //untuk jurusan
	// $sqljur = "select * from pp_bidangjur j 
				// left join lv_jurusan lj on lj.kdjurusan=j.kdjurusan
				// where idpustaka='$r_key'";
	// $rsjur = $conn->Execute($sqljur);
	
	
	// while ($rowjur = $rsjur->FetchRow()){
			// $jurusan[$rowjur['idpustaka']][] = $rowjur['namajurusan'];
		// }
	
	//untuk pengarang
	// $sql = "select * from pp_author a 
			// left join ms_author m on a.idauthor=m.idauthor
			// where a.idpustaka='$r_key'";
	// $rsauth = $conn->Execute($sql);
	
	// while ($rowauth = $rsauth->FetchRow()){
			// $author[$rowauth['idpustaka']][] = $rowauth['namadepan'].' '.$rowauth['namabelakang'];
		// }
	
	//untuk eksemplar
	$sql = "select * from pp_eksemplar e 
			left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			left join ms_rak r on r.kdrak=e.kdrak
			left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
			where idpustaka='$r_key' and kdkondisi='V' and e.statuseksemplar is not null /*and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')*/ ";
	$rseks = $conn->Execute($sql);
	
	while ($roweks = $rseks->FetchRow()){
			$ideksemplar[$roweks['idpustaka']][] = $roweks['ideksemplar'];
			$seri[$roweks['idpustaka']][] = $roweks['noseri'];
			$eksrak[$roweks['idpustaka']][] = $roweks['namarak'];
			$ekslokasi[$roweks['idpustaka']][] = $roweks['lantai']!='' ? $roweks['namalokasi'].' / Lt '.$roweks['lantai'] : $roweks['namalokasi'];
			$eksstatus[$roweks['idpustaka']][] = $roweks['statuseksemplar'];
			$label[$roweks['idpustaka']][] = $roweks['namaklasifikasi'];
			$lbl[$roweks['idpustaka']][] = $roweks['kdklasifikasi'];
	}
	
	//untuk topik
	// $sql = "select namatopik from lv_topik
			// where kode = '".$row['kodeddc']."'";
	
	$sql = "select namatopik from pp_topikpustaka tp
			join lv_topik t on tp.idtopik=t.idtopik
			where tp.idpustaka=$row[idpustaka]";
	$rowtopik = $conn->GetOne($sql);

	$sqlj = "select j.namajurusan from pp_bidangjur b 
			left join lv_jurusan j on j.kdjurusan = b.kdjurusan
			where b.idpustaka=$row[idpustaka]";
	$rowjurusan = $conn->GetOne($sqlj);

	$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
	$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
	
	$c_down = true;//$valid['down'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
	<?= $b_link->output(); ?>
	<? if (isset($_SESSION['userid'])){ ?>
	<div class="notification">
		<?= Perpus::getCart(); ?>
	</div>
	<? } ?>
	<div class="bookDetail">
		<div class="contentTop">&nbsp;</div>
		<div class="contentContainer" style="padding-bottom:10px;">
			<table width="98%" cellpadding="1" cellspacing="0">
				<tr>
					<td colspan="2">
						<!--<span class="menuTitle"><?= $p_title; ?></span>-->
						<h4 class="sidebar-title"><?= $p_title?></h4>
					</td>
				</tr>
				<tr height="20"><td>&nbsp;</td></tr>
				<tr height="20">
					<td class="judul"><?= $row['judul'] ?></td>
                    <td align="right"></td>
				</tr>
			</table><br />
			<table width="98%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" rowspan="7" width="188" align="center" style="padding-left:12px;padding-right:12px;">
						<img id="imgfoto" src="<?= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" width="100" height="108">
					</td>
					<? if($row['islokal']==1) { ?>
					<td valign="top" class="resultSub" width="100">KODE</td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['noseri']; ?></td>
					<? } /*else {?>
					<td valign="top" class="resultSub" width="100"><?= $form['NO_CALL']?></td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['nopanggil']; ?></td>
					<? } */?>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['NO_CALL']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['nopanggil']; ?></td>
				</tr>

				<? if($row['islokal']!=1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['EDITION']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['edisi']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['AUTHOR']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?
								if($row['islokal']==0){
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}else{
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."','1')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."','1')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."','1')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}
								//if($
								// if (count($author[$row['idpustaka']]) > 0){
									// for($j=0;$j<count($author[$row['idpustaka']]);$j++)
										// echo $author[$row['idpustaka']][$j].'<br>';
								// } 
						?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;">Contributor</td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['pembimbing']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['PUBLISHER']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['kota'].': '.$row['namapenerbit'].', '.$row['tahunterbit']; ?>
					</td>
				</tr>
				<?/*?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['DIMENSION']; ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= ($row['jmlhalromawi']=='' ? '-' : $row['jmlhalromawi']) .", ".($row['jmlhalaman']=='' ? '-' : $row['jmlhalaman']) .", ".($row['dimensipustaka']=='' ? '-' : $row['dimensipustaka']) ; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['ISBN']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['isbn']; ?>
					</td>
				</tr>
				<?*/?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['TOPIC']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $rowtopik;	
							// if (count($topik[$row['idpustaka']]) > 0){
								// for($j=0;$j<count($topik[$row['idpustaka']]);$j++)
									// echo $topik[$row['idpustaka']][$j].'<br>';
							// } 
						?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= 'Jurusan';//$form['TOPIC']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $rowjurusan;?>
					</td>
				</tr>
				<?}?>
				<tr>
					<td colspan="4">
						<br />
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr height="30">
								<td><strong><?= $form['SYNOPSIS']?></strong></td>
							</tr>
							<tr>
								<td><p><?= empty($row['sinopsis']) ? $form['P_EMPTYSYNOPSIS'] : $row['sinopsis'];?></p></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="bookDetail">
						<div class="contentTop">&nbsp;</div>
						<div class="contentContainer" style="padding-bottom:10px;">
						<span class="menuTitle">File  <font color="#FF0000" style="font-size:9px"><?= $form['P_MEMBERONLY']; ?></font></span>
						<table width="98%" cellpadding="4" cellspacing="0">
							<?
							$k=0;
							foreach($rsFile as $f){
								$k++;
							?>
							<tr>
								<td width="2%" style="padding-top:px;">
									<?
									
										if (Helper::getExtension($f['file']) == '.pdf')
											echo '<img src="images/tombol/cart/pdf.gif" />';
										else if (Helper::getExtension($f['file']) == '.doc' or Helper::getExtension($f['file']) == '.docx')
											echo '<img src="images/tombol/cart/doc.png" />';
										else if (Helper::getExtension($f['file']) == '.xls' or Helper::getExtension($f['file']) == '.xlsx')
											echo '<img src="images/tombol/cart/xls.png" />';
										else if (Helper::getExtension($f['file']) == '.ppt' or Helper::getExtension($f['file']) == '.pptx')
											echo '<img src="images/tombol/cart/ppt.png" />';
										else if (Helper::getExtension($f['file']) == '.zip')
											echo '<img src="images/tombol/cart/zip.png" />';
										else
											echo '<img src="images/tombol/cart/blank.png" />';
									?>
								</td>
								<td style="padding-top:5px;">
									<p width="100%">&nbsp;
										<?
											if($f['login'] == "1" and isset($_SESSION['userid'])) {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= $f['file']; ?>','<?= $row['noseri'] ?>');">
										<?= Helper::GetPath($f['file']); ?></u>
										<?
											}elseif($f['login'] == "0") {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= $f['file']; ?>','<?= $row['noseri'] ?>');">
										<?= Helper::GetPath($f['file']); ?></u>
										<?
											}else echo Helper::GetPath($f['file'])."<br>";
										?>
										
									</p>
								</td>
							</tr>
							<?
							}
							?>
							
							
							
							<? /*
						if(isset($_SESSION['userid']) and $c_down){
							if($row['berseri'] == 0){ 
							for($i=1;$i<=10;$i++){ 
									if ($row['file'.$i]=='')
										continue;
								?>
							<tr>
							<td width="2%" style="padding-top:px;">
								<?
								
									if (Helper::getExtension($row['file'.$i]) == '.pdf')
										echo '<img src="images/tombol/cart/pdf.gif" />';
									else if (Helper::getExtension($row['file'.$i]) == '.doc' or Helper::getExtension($row['file'.$i]) == '.docx')
										echo '<img src="images/tombol/cart/doc.png" />';
									else if (Helper::getExtension($row['file'.$i]) == '.xls' or Helper::getExtension($row['file'.$i]) == '.xlsx')
										echo '<img src="images/tombol/cart/xls.png" />';
									else if (Helper::getExtension($row['file'.$i]) == '.ppt' or Helper::getExtension($row['file'.$i]) == '.pptx')
										echo '<img src="images/tombol/cart/ppt.png" />';
									else if (Helper::getExtension($row['file'.$i]) == '.zip')
										echo '<img src="images/tombol/cart/zip.png" />';
									else
										echo '<img src="images/tombol/cart/blank.png" />';
								?>
							</td>
							<td style="padding-top:5px;">
								<p width="100%">&nbsp;					
									<u style="cursor:pointer" onclick="getLook('<?= $row['file'.$i]; ?>','<?= $row['seripus'] ?>');">
									<?= Helper::GetPath($row['file'.$i]); ?></u>
								</p>
							</td>
						    </tr>
						    <? }
							}elseif($row['berseri'] == 1){ 
								for($i=1;$i<=10;$i++){ 
									if ($row['files'.$i]=='')
										continue;
								?>
							<tr>
							<td width="2%" style="padding-top:px;">
								<?
								
									if (Helper::getExtension($row['files'.$i]) == '.pdf')
										echo '<img src="images/tombol/cart/pdf.gif" />';
									else if (Helper::getExtension($row['files'.$i]) == '.doc' or Helper::getExtension($row['files'.$i]) == '.docx')
										echo '<img src="images/tombol/cart/doc.png" />';
									else if (Helper::getExtension($row['files'.$i]) == '.xls' or Helper::getExtension($row['files'.$i]) == '.xlsx')
										echo '<img src="images/tombol/cart/xls.png" />';
									else if (Helper::getExtension($row['files'.$i]) == '.ppt' or Helper::getExtension($row['files'.$i]) == '.pptx')
										echo '<img src="images/tombol/cart/ppt.png" />';
									else if (Helper::getExtension($row['files'.$i]) == '.zip')
										echo '<img src="images/tombol/cart/zip.png" />';
									else
										echo '<img src="images/tombol/cart/blank.png" />';
								?>
							</td>
							<td style="padding-top:5px;">
								<p width="100%">&nbsp;
									<u style="cursor:pointer" onclick="getLooks('<?= $row['files'.$i]; ?>','<?= $row['seripus'] ?>','<?= $row['idsitem'] ?>');">
									<?= Helper::GetPath($row['files'.$i]); ?></u>
								</p>
							</td>
						    </tr>
							<? }
							 }
						}  */?>
						</table>
					    </div>
					    </div>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="resultSub" style="padding-top:5px;">
						<!--<br /><br /><b><?= $form['COPIES']?></b> <br />-->
						<h4 class="sidebar-title"><?= $form['COPIES'];?></h4>
						<table border="0" cellpadding="4" cellspacing="0" class="containerTable" width="100%">
							<tr>
								<th width="150" align="left" class="eksHead" style="padding-top:3px;"><?= $row['islokal']==1 ? 'KODE' : 'REG. COMP' ?></th>
								<th width="400" align="left" class="eksHead"><?= $form['LOCATION']?></th>
								<th width="200" align="left" class="eksHead"><?= $form['CLASSIFICATION']?></th>
								<th width="120" align="left" class="eksHead"><?= $form['STATE']?></th>
							</tr>			
							<? for($j=0;$j<count($eksrak[$row['idpustaka']]);$j++) { 
							/*if ($lbl[$row['idpustaka']][$j]=='LM')
								$warna = 'red';
							elseif ($lbl[$row['idpustaka']][$j]=='LH')
								$warna = 'green';
							else
								$warna = 'black';*/
							$warna = 'black';	
							?>
							<tr >
								<td align="left"><a class="judul-buku" href="javascript:showEks('<?= $ideksemplar[$row['idpustaka']][$j];?>','<?= $eksstatus[$row['idpustaka']][$j] ?>')" title="Download File"><?= $seri[$row['idpustaka']][$j];?></a></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $ekslokasi[$row['idpustaka']][$j];?></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $label[$row['idpustaka']][$j];?></td>
								<? $eksstatus[$row['idpustaka']][$j]=='ADA' ? $class = 'resultAvailable' : $class = 'resultUnavailable' ?>
								<td style="color:<?= $warna ?>" class="<?= $class; ?>"><?= $eksstatus[$row['idpustaka']][$j]=='ADA' ? $form['P_AVAILABLE']: ($eksstatus[$row['idpustaka']][$j]=='PJM' ? $form['P_LOANS'] : 'PROSES');?></td>
							</tr>
						<? } ?>
						<? 	if (count($eksrak[$row['idpustaka']]) == 0){ ?>
						<tr>
							<td colspan=4 align="center"><b><?= $form['EKS_NULL'] ?></b></td>
						</tr>
						<? } ?>
						</table>
						
					</td>
				</tr>

			</table>
		</div>
	</div>
    </div><?php /*?>
	<div class="bookDetail">
	<div class="contentTop">&nbsp;</div>
	<div class="contentContainer" style="padding-bottom:10px;">
    	<span class="menuTitle">Pustaka Terkait</span>
        <table width="98%" cellpadding="4" cellspacing="0">
        	<tr>
            	<td></td>
            </tr>
        </table>
    </div>
    </div><?php */?>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	<?php /*?>function addCart(){
		sent = "&act=add&key=<?= $r_key?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}<?php */?>
	
	function showCart(){
		sent = "";
		goLink('contents','<?= Helper::navAddress('data_cart.php')?>', sent);
	}
	
	function showEks(key, status){
		sent = "&key=" + key + "&status=" + status;
		goLink('contents','<?= Helper::navAddress('data_eksemplar.php')?>', sent);
	}
	
	function goAuthor(key,par){
		sent = "&par="+par+"&key="+ key;
		goLink('contents','<?= Helper::navAddress('xlisthasilauthor.php')?>', sent);
	}
	
	function getLook(file,seri){
		//window.open("<?= Helper::navAddress('view_pdf.php')?>&kode="+seri+"&file="+file,"_blank");
		window.open("<?= Helper::navAddress('showOff.php')?>&kode="+seri+"&file="+file,"_blank");
	}
	
	function getLooks(file,seri,det){
		window.open("<?= Helper::navAddress('view_pdfseri.php')?>&kode="+seri+"&det="+det+"&file="+file,"_blank");
	}
</script>
</html>
