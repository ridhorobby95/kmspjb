<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	if(empty($_SESSION[Config::G_SESSION]['iduser'])){
		echo '<script type="text/javascript">location.reload(true);</script>';
		exit();
	}
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($conn);
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['LOAN'], Helper::navAddress('data_rwtpeminjaman.php'), 0);
	
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['T_LOAN'];
	$p_title = $form['T_LOAN'];
	$p_id = "rwtpinjam";
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		
		if ($r_act == 'perpanjang'){
			$r_eks = Helper::removeSpecial($_REQUEST['ideksemplar']);
			$r_trans = Helper::removeSpecial($_REQUEST['idtrans']);
			
			if (isset($_SESSION['skors']))
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message($form['P_SOKRS'], true).'</div>';	
			else			
				$confirm = Perpus::Perpanjang($conn, $r_eks, $r_trans);

		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
	
	
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	//riwayat peminjaman
	$p_sqlstr = "select * from v_trans_list
		where idanggota='".$_SESSION['idanggota']."'
		order by idtransaksi desc";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
		
	//sedang di pinjam
	$sql = "select v.*, to_char(v.tgltenggat,'YYYY-mm-dd') tgl_tenggat, to_char(v.tgltransaksi,'YYYY-mm-dd') tgl_transaksi, p.statusajuan 
		from v_ekspinjam v
		left join pp_perpanjangan p on p.idtransaksi = v.idtransaksi and p.statusajuan = 1  
		where idanggota='".$_SESSION['idanggota']."'
		order by tgltransaksi desc";
	$rs_pinjam = $conn->Execute($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<?= $b_link->output(); ?>
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $form['T_LOANBIBLIO']; ?></h4>
        	<?= (!$confirm) ?  '' : $confirm; ?>
		<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
			<tr class="catbg">
				<th align="center" width="89"><?= $form['BIBLIO_TYPE']; ?></th>
				<th align="center" width="185"><?= $form['BIBLIO_TITLE']; ?></th>
				<th align="center" width="109"><?= $form['LOAN_DATE']; ?></th>
				<th align="center" width="80"><?= $form['EXPIRED_DATE']; ?></th>
				<th align="center" width="75"><?= $form['EXTENSION']; ?></th>
				<th align="center" width="75">Status Perpanjang</th>
				<th align="center" width="75">Denda</th>
			</tr>
			<? 
				$i=0;
				while ($row = $rs_pinjam->FetchRow()){ $i++; 
				$dendaperbuku = Perpus::hitungdendaperbuku($row['tgl_tenggat'],date('Y-m-d'),$row['idanggota'],$row['ideksemplar']);
			?>
			<tr>
				<td align="center" valign="top"><?= $row['namajenispustaka']?></td>
				<td valign="top"><?= $row['judul']?></td>
				<td valign="top" align="center"><?= Helper::tglEngStamp($row['tgltransaksi']);?></td>
				<td valign="top" align="center"><?= $row['tgl_tenggat']<date('Y-m-d') ? '<blink style=color:red>'.Helper::tglEngStamp($row['tgltenggat']).'</blink>' : Helper::tglEngStamp($row['tgltenggat']) ;?></td>
				<td align="center" valign="top"><span style="position:relative;bottom:6px;">(<?= Helper::cNumZero($row['perpanjangke']); ?>)</span>
				<? if($row['kdjenistransaksi']=='PJH' or $row['tgl_transaksi']==$row['tgl_tenggat'] or $row['statusajuan'] == 1 ) { ?>
					<? if($row['statusajuan'] == 1){?>
						<img src="images/tombol/time2_bg.png" title="Proses Perpanjangan.">
					<? }else{?>
						<img src="images/tombol/time2_bg.png" >
					<? } ?>
				<? }else { 
						$minperpanjangan = date('Y-m-d', strtotime('+1 days', strtotime($row['tgl_transaksi'])));
						
						// pengecekan tambahan : fach
						// 3 hari sebelum tgl batas kembali
						$datenow = date('Y-m-d');
						if ($datenow >= $minperpanjangan and $datenow <= $row['tgl_tenggat']) { ?>
							<img src="images/tombol/time_add.png" style="cursor:pointer" title="Perpanjang" onclick="goPerpanjang('<?= $row['ideksemplar']; ?>','<?= $row['idtransaksi']; ?>')" />
						<?php }else{
							echo '<br>tanggal perpanjangan <br> '.Helper::tglEng($minperpanjangan).' s/d '.Helper::tglEng($row['tgl_tenggat']);
						} ?>

				<? } ?>
				</td>
				<td align="center" valign="top">
					<? if($row['statusajuan'] == 1){?>
						<img src="images/tombol/delay.png" title="Proses">
					<? }else{?>
						- 
					<? } ?>
				</td>
				<td align="right" valign="top">Rp. <?= number_format($dendaperbuku,0,',','.');?></td>
			</tr>
			<? } if ($i == 0) {?>
			<tr>
				<td colspan="5" align="center"><?= $form['EMPTY_TABLE']; ?></td>
			</tr>
			<? } ?>
		</table>
            <h4 class="sidebar-title"><?= $form['T_LOAN']; ?></h4>
            
		<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
			<tr>
				<th width="90"><?= $form['BIBLIO_TYPE']; ?></th>
				<th width="270"><?= $form['BIBLIO_TITLE']; ?></th>
				<th width="120"><?= $form['LOAN_DATE']; ?></th>
				<th width="110"><?= $form['RETURN_DATE']; ?></th>
			</tr>
			<? 
				$i=0;
				while ($rows = $rs->FetchRow()){ $i++;
			?>
			<tr>
				<td align="center" valign="top"><?= $rows['namajenispustaka']?></td>
				<td valign="top"><?= $rows['judul'] ?></td>
				<td valign="top" align="center"><?= Helper::tglEngStamp($rows['tgltransaksi']);?></td>
				<td valign="top" align="right">
				<? if ($rows['statustransaksi'] == '0')
						echo Helper::tglEngStamp($rows['tglpengembalian']);
					else if ($rows['statustransaksi'] == '1')
						echo 'Belum Kembali';
					else{
						if (empty($rows['perpanjangke']))
							echo Helper::tglEngStamp($rows['tglpengembalian']);
						else
							echo '('.Helper::cNumZero($rows['perpanjangke']).') '.Helper::tglEngStamp($rows['tglpengembalian']);
					}										
				?>
				</td>
			</tr>
			<? } if ($i == 0) {?>
			<tr>
				<td colspan="4" align="center"><?= $form['EMPTY_TABLE']; ?></td>
			</tr>
			<? } ?>
			<tr height="20">
				<td align="left" colspan="4"><?= $form['PAGE']; ?>&nbsp;:&nbsp;
				<?	if($p_page == 0)
						echo '&nbsp;';
					else {
						$start = (($p_page > 9) ? ($p_page-9) : 1);
					$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
						$link = array();
						
						for($i=$start;$i<=$end;$i++) {
							if($i == $p_page)
								$link[] = '<strong>'.$i.'</strong>';
							else{
								if (!isset($_REQUEST['isadv']))
									$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
							}
						}
						echo implode(' ',$link);
					}
				?>
					&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
				</td>
			</tr>
		</table>
		</div>
	</div>
	
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	function goPage(npage) {
		sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goPerpanjang(ideksemplar, idtrans){
		var tanya = confirm("Apakah Anda yakin akan memperpanjang pustaka ini ?");
		if(tanya){
		sent = "&act=perpanjang&ideksemplar=" + ideksemplar + "&idtrans=" + idtrans;
		goLink('contents','<?= $i_phpfile?>', sent);
		}
	}
</script>
</html>
