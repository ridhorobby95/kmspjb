<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	require('classes/pdf.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['LIST_NOTIFIKASI'], Helper::navAddress('list_notifikasi.php'), 1);
	// $conn->debug = true;
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_UPLOAD'];
	$p_title = $form['T_UPLOAD'];
	$p_dbtable = "pp_uploadta";
	$userlogin = $_SESSION['idanggota'];
	$jenisnotif = Helper::removeSpecial($_REQUEST['jenis']);
	
	//cek di tabel pp_uploadta
	$sql_cek_mhs = "select * from pp_uploadta where idanggota='$userlogin'";
	$rs_cek = $conn->GetRow($sql_cek_mhs);
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']);
		$rkey_val = Helper::removeSpecial($_REQUEST['reditval']);
		// var_dump($rkey);
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$istrue = false;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){
			$conn->StartTrans();
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['iduploadta'] = $rkey;
			$record['tglhistoryvalidasi'] = date('Y-m-d');
			$record['npkvalidasi'] = $_SESSION['idanggota'];
			$record['stsvalidasi'] = $_POST['stsvalidasi'];
			$record['catatanvalidasi'] = $_POST['catatanvalidasi'];
			Helper::Identitas($record);
			
			// if (empty($rkey_val)) // insert
				Perpus::Simpan($conn, $record, 'pp_historyvalidasita');
			// else // update
				// $suc_pp_historyvalidasita = Perpus::Update($conn, $record, 'pp_historyvalidasita', " iduploadta=".$rkey." and npkvalidasi='".$_SESSION['idanggota']."'");
			
			//insert ke tabel pp_historynotifikasiuploadta====================================================================
			$rs_cek_nrp = $conn->GetRow("select * from pp_uploadta where iduploadta=$rkey");
			
			$rec_histnotif = array();
			$rec_histnotif['iduploadta'] = $rkey;
			$rec_histnotif['tglhistorynotifikasi'] = date('Y-m-d');
			$rec_histnotif['nrptujuannotifikasi'] = $rs_cek_nrp['nrp1'];
			$rec_histnotif['pesan'] = $record['catatanvalidasi'];
			$rec_histnotif['stsnotifikasi'] = 1;
			Helper::Identitas($rec_histnotif);
			
			// if (empty($rkey_val)){ // insert
				Perpus::Simpan($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
			//}
			// else{ // update
				// $suc_pp_historynotifikasiuploadta = Perpus::Update($conn, $record, 'pp_historynotifikasiuploadta', " iduploadta=".$rkey." and npkvalidasi='".$_SESSION['idanggota']."'");
			
			// }	
			if($record['stsvalidasi']==null or $record['stsvalidasi']==0){ //jika tidak disetujui
				$rec_uploadta = array();
				$rec_uploadta['tglrequestval'] = null;
				$rec_uploadta['userrequestval'] = null;
				
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing1'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing2'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing3'] = date("Y-m-d");
		
				Helper::Identitas($rec_uploadta);
				// if($suc_pp_historynotifikasiuploadta)
					$suc_pp_uploadta = Perpus::Update($conn, $rec_uploadta, 'pp_uploadta', " iduploadta=".$rkey);
			}
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
			}else{
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data Berhasil').'</div>';
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
		
	if (!empty($rkey)){
		$sql_cek_mhs = "select * from pp_uploadta where iduploadta=$rkey";
		$rset = $conn->GetRow($sql_cek_mhs);
		
		$sql_val = "select * from pp_historyvalidasita where iduploadta=$rkey and npkvalidasi='".$_SESSION['idanggota']."'";
		$rs_val = $conn->GetRow($sql_val);
	}
	
	//data	
	$npk = array();
	if($jenisnotif == 'D' or $jenisnotif == null){
		$p_sqlstr = "select n.t_user as idpemberi,* from pp_historynotifikasiuploadta n left join ms_anggota a on a.idanggota=n.t_user where npktujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0
					order by idhistorynotifikasi desc";
		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	}else if($jenisnotif == 'TA'){
		$p_sqlstr = "select n.t_user as idpemberi, * from pp_historynotifikasiuploadta n left join ms_anggota a on a.idanggota=n.t_user 
					left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota where nrptujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0
				order by idhistorynotifikasi desc";
		$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
		$rs_uploadta = $conn->GetRow("select * from pp_uploadta where idanggota='".$_SESSION['idanggota']."'");
		$npk[$rs_uploadta['npkpembimbing1']] = $rs_uploadta['statusvalpembimbing1']; // $npk[170003] = 2
		$npk[$rs_uploadta['npkpembimbing2']] = $rs_uploadta['statusvalpembimbing2'];
		$npk[$rs_uploadta['npkpembimbing3']] = $rs_uploadta['statusvalpembimbing3'];
		$npk[$rs_uploadta['npkvalstafdigitalisasi']] = $rs_uploadta['statusvalstafdigitalisasi'];
	}
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post" enctype="multipart/form-data">
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer">
				<span class="menuTitle" style="margin-left:0px;"><?= $form['LIST_NOTIFIKASI']; ?></span>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td>
							<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
								<tr>
									<th width="100" align="center"><?= $form['DATE_REQUEST_MHS']; ?></th>
									<?if($jenisnotif=='TA'){?>
										<th width="100" align="center"><?= $form['JABATAN']; ?></th>
									<?}?>
									<th width="105" align="center"><?= $form['NOTIFIKASI_FROM']; ?></th>
									<th width="165" align="center"><?= $form['MSG_NOTIF']; ?></th>
									<?if($jenisnotif!='TA'){?>
									<th width="10" align="center"><?= $form['ACTION']; ?></th>
									<?}?>
								</tr>
								<? 
									$i=0;
									while ($row = $rs->FetchRow()){ $i++;
									if($jenisnotif=='TA'){
										if($npk[$row['idpemberi']] == 2){ //170003
											$rec_ubah = array();
											$rec_ubah['stsnotifikasi'] = 1;
											Perpus::Update($conn, $rec_ubah, 'pp_historynotifikasiuploadta', " idhistorynotifikasi=".$row['idhistorynotifikasi']);
										}
									}
								?>
								
								<tr>
									<td align="center"><?= Helper::formatDate($row['tglhistorynotifikasi'])?></td>
									<?if($jenisnotif=='TA'){?>
										<td align="center" ><?= $row['namajenisanggota'] ?></th>
									<?}?>
									<td align="center"><?= $row['idpemberi']." - ".$row['namaanggota']?></td>
									<td title="<?= $row['pesan']?>" align="center"><?= substr($row['pesan'],0,20)?>...</td>
									<?if($jenisnotif=='TA'){?>
										<!--<td align="center"><img src="images/tombol/edited.gif" style="cursor:pointer" onclick="goForm('<?= $row['iduploadta'];?>')" title="Revisi Data Upload TA"></td>-->
									<?}else{?>
										<td align="center"><img src="images/tombol/edited.gif" style="cursor:pointer" onclick="goCekTa('<?= $row['iduploadta'];?>','<?= $row['idhistorynotifikasi']?>')" title="Cek Detail TA"></td>
									<?}?>
								</tr>
								<? }?> 
								<? if ($i == 0) {?>
								<tr>
									<td colspan="5" align="center"><?= $form['EMPTY_TABLE']; ?></td>
								</tr>
								<? } ?>
								<tr height="20">
									<td align="left" colspan="6"><strong><?= $form['PAGE']; ?></strong>&nbsp;:&nbsp;
											<?	if($p_page == 0)
													echo '&nbsp;';
												else {
													$start = (($p_page > 9) ? ($p_page-9) : 1);
												$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
													$link = array();
													
													for($i=$start;$i<=$end;$i++) {
														if($i == $p_page)
															$link[] = '<strong>'.$i.'</strong>';
														else{
															if (!isset($_REQUEST['isadv']))
																$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
														}
													}
													echo implode(' ',$link);
												}
											?>
											&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<?if($jenisnotif=='TA'){?>
					<div class="profile">
					<div class="contentTop"></div>
					<div class="contentContainer">
						<a href="javascript:goForm('<?= $row['iduploadta'];?>')" style="cursor:pointer" ><font color="red" size="5"><b><blink>Revisi/Lihat Form Upload Tugas Akhir!</blink></b></font></a>
					</div></div>
				<?}?>
			</div>
		</div>
		
	</form>
	<iframe name="upload_iframe" style="display:none;"></iframe>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>

<script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/jquery.masked.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	$(function(){
		$("#perpusform").find("#hargausulan").onlyNum();
	   $("#tahunterbit").mask("9999");
	});
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goCekTa(iduploadta,idnotifikasi){
			var sent = "&redit="+iduploadta;
			sent += "&idnotif="+idnotifikasi;
			goLink('contents','<?= Helper::navAddress('val_uploadta.php');?>',sent);
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
	
	function CreateTextbox(){
		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type=file name='userfile[]' size='40' /> </p>";
		}
	}
	
	function goDelFile(key1,key2){
		var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
		if(delfile){
			sent = '';
			sent += $("#perpusform").serialize();
			sent += "&act=delfile";
			sent += "&delkey="+key1;
			sent += "&delfile="+key2;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goRequestVal(iduploadta){
		var request=confirm("Apakah Anda yakin akan melakukan permintaan validasi?");
		if(request){
			sent = '';
			// sent += $("#perpusform").serialize();
			sent += "&act=requestval";
			sent += "&iduploadta="+iduploadta;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goForm(idupload) {
		// var sent = "&redit=" + idupload;
		goLink('contents','<?= Helper::navAddress('data_uploadta.php'); ?>', sent);
	}
	
</script>
</html>
