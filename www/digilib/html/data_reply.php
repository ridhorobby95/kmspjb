<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once("classes/perpus.class.php");
	require_once('classes/breadcrumb.class.php');
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_add = $a_auth['canadd'];
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$rkey = Helper::removeSpecial($_REQUEST['rkey']);
	
	//setting utnuk halaman
	$p_window = "[Digilib] Balas Post";
	$p_title = "Reply Post to ";
	
	$p_dbtable = 'pp_kontakonline';
	
	//mendapatkan nama kontak
	$post = $conn->GetOne("select judul from $p_dbtable where idkontakol=$r_key");
	
	$b_link = new Breadcrumb();
	$b_link->add('Reply '.Helper::word_limiter($post,20), Helper::navAddress('data_reply.php'), 3);
	
		
	if (isset($_POST)){
		$r_act = Helper::removeSpecial($_POST['act']);
		
		if ($r_act == 'simpan'){
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['idkontak'] = $rkey;
			$record['idparentol'] = $r_key;
			$record['t_input'] = date('Y-m-d H:i:s');
			$record['t_author'] = $_SESSION['usernama'];
			$record['jenisanggota'] = $_SESSION['NMjenis'];
			Helper::Identitas($record);		
			
			Perpus::updateRe($conn,$r_key);
			Perpus::simpan($conn,$record,$p_dbtable);
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Penyimpanan Gagal',true).'</div>';	
			else
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Penyimpanan Berhasil').'</div>';
				
			echo '<script type="text/javascript">';
			echo 'sent += "&key='.$r_key.'&rkey='.$rkey.'";';
			echo 'goLink("contents","'.Helper::navAddress('view_post.php').'", sent)';
			echo '</script>';	
		}
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?= $p_window; ?></title>
	<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<form name="perpusForm" id="perpusForm" action="<?= $i_phpfile?>" method="post">
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
				<span class="menuTitle" style="margin-left:-20px;"><?= $p_title; ?><b>"<?= $post; ?>"</b></span>
        		<?= (!$confirm) ?  '' : $confirm; ?><br>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td colspan="2" valign="top" height="30"><div id="wait_div" style="padding-left:15px;font-weight:bold;"></div></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">Pesan *</td>
						<td style="padding-top:5px;">&nbsp;<?= UI::createTextArea('keterangankontak',$row['keterangankontak'],'ControlStyle',20,65); ?></td>
					</tr>
					<tr height="20">
						<td style="padding-top:5px;">&nbsp;</td>
						<td valign="bottom"><strong><?= $form['VAL_CODE']?> *</strong></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td rowspan="2"><img src="includes/kcaptcha/rep.php?<?= mt_rand();?>" title="Ulangi validasi" id="captcha" style="cursor:pointer" onClick="this.src='includes/kcaptcha/rep.php?'+Math.random();"></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td><?= UI::createTextBox('validasi','','controlStyle',50,50); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td style="padding-top:5px;">&nbsp;&nbsp;
							<span name="simpan" class="buttonSubmit btn btn-primary" value="Simpan" onclick="goSave()" style="cursor:pointer;">Simpan</span>
							<span name="reset" class="buttonSubmit btn btn-success" value="Reset" style="cursor:pointer">Reset</span>
						</td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
			</div>
		</div>
	</form>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">		
	function goSave()
	{
		if(cfHighlight("keterangankontak,validasi")){
			file = "<?= Helper::navAddress('functionx.php')?>";
			sent = $("#perpusForm").serialize();
			sent += "&act=simpan&key=<?= $r_key; ?>&rkey=<?= $rkey?>";
			v = $("#validasi").val();
			$.post(file,'f=repcaptcha&p1='+v,function(retlog) {
				if(retlog == 1) { 
					goLink('contents','<?= $i_phpfile?>', sent);
				}else{
					$("#wait_div").html('<font color="red">Kode Validasi yang dimasukkan Salah</font>');
					document.getElementById("captcha").src = 'includes/kcaptcha/rep.php?'+Math.random();
				}
			});
		}
	}
	
</script>
</html>
