<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	require_once 'classes/aes.class.php';     // AES PHP implementation
			
	$f = Helper::cAlphaNum($_POST['f']);
	$p1 = Helper::removeSpecial($_POST['p1']);
	$p2 = Helper::removeSpecial($_POST['p2']);
	$p3 = rawurldecode($_POST['px']);
	$salt = Config::rpcsalt;
	$px = Aes::decrypt($p3, $p2, 256);
	$s = Helper::cAlphaNum($_GET['s']);
	
	if($f == 'salt') getSalt();
	else if($f == 'login') goLogin($p1, $p2, $px);
	else if($f == 'gantipw') goGantiPW($_SESSION['userid'], $p1, $p2);
	else if($f == 'lupapw') goLupaPW($p1, $p2);
	else if($f == 'resetpw') goResetPW($p1,$p2);
	else if($f == 'valcaptcha') goValC($p1);
	else if($f == 'contcaptcha') goContValC($p1);
	else if($f == 'hubcaptcha') goHubValC($p1);
	else if($f == 'repcaptcha') goRepValC($p1);
	else if($s == 'logbykm') logmasuk();
	else{
		Helper::navigateOut();
		exit();
	}
	
	function getSalt() {	
		global $connGate;
		
		$salt = Config::rpcsalt;
		if (!empty($salt))	
			echo $salt;
		else
			echo '-1';
	}
	
	function s_get_os() {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if(($pos = strpos($agent,'Windows ')) !== false) {
			$major = substr($agent,$pos+8,2);
			
			if($major == '3.') return 'Windows 3.1';
			else if($major == '95') return 'Windows 95';
			else if($major == '98') return 'Windows 98';
			else if($major == 'Me') return 'Windows Me';
			else if($major == 'NT') {
				$version = substr($agent,$pos+11,3);
				
				if($version == '5.0') return 'Windows 2000';
				else if($version == '5.1') return 'Windows XP';
				else if($version == '5.2') return 'Windows Server 2003';
				else if($version == '6.0') return 'Windows Vista';
				else return 'Windows NT';
			}
			else return 'Windows';
		}
		else if(strpos($agent,'Linux')) return 'Linux';
		else if(strpos($agent,'Symbian')) return 'Symbian';
		else if(strpos($agent,'MIDP')) return 'MIDP';
		else if(strpos($agent,'Mac')) return 'Macintosh';
		else if(strpos($agent,'Solaris')) return 'Solaris';
		else return 'Lain-lain';
	}
	
	function s_get_browser() {
		$agent = $_SERVER['HTTP_USER_AGENT'];
		
		if(($pos = strpos($agent,'Opera')) !== false) $posp = $pos+5;
		else if(($pos = strpos($agent,'Flock')) !== false) $posp = $pos+5;
		else if(($pos = strpos($agent,'Firefox')) !== false) $posp = $pos+7;
		else if(($pos = strpos($agent,'Chrome')) !== false) $posp = $pos+6;
		else if(($pos = strpos($agent,'Safari')) !== false) $posp = $pos+6;
	  	else if(($pos = strpos($agent,'Konqueror')) !== false) $posp = $pos+9;
		else if(($pos = strpos($agent,'MSIE')) !== false) $posp = $pos+4;
		else return 'Lain-lain';
		
		$posv = strpos($agent,' ',$posp+1);
		
		if($posv === false)
			$return = substr($agent,$pos);
		else
			$return = substr($agent,$pos,($posv-$pos));
		
		if($return[strlen($return)-1] == ';')
			$return = substr($return,0,strlen($return)-1);
		
		return $return;
	}
	
	function checkGate($uname, $pwd) {
		$connGate = Factory::getConnGate();
		$sql = "select 1 from sc_user where username = '$uname' and password = md5('$pwd')"; 
		$ispass = $connGate->GetOne($sql);
		return (empty($ispass) ? false : true);
		
	}
	
	function goLogin($uname, $pwd, $pwdldap) {
		global $conn;
		$connGate = Factory::getConnGate();
		
		if(isset($_SESSION['loginuid']) and $_SESSION['loginuid'] != $uname) {
			$_SESSION['loginuid'] = $uname;
			$_SESSION['loginattempt'] = 1;
		}
		else if(!isset($_SESSION['loginuid']))
			$_SESSION['loginuid'] = $uname;

		if(!(checkGate($uname, $pwd))){ 
			$salt = Config::rpcsalt; 
			$ps = md5($pwd.$salt); 
			$pwd1 = md5($ps.$salt);	
			$userinfo =$conn->GetRow("select idanggota,email,namaanggota,a.kdjenisanggota,namajenisanggota,setpagu
						 from ms_anggota a
						 left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
						 where idanggota='$uname' and password='$pwd1' and statusanggota='1' and j.canlogin=1
						 and (tglexpired > current_date or tglexpired is null) ");					
			
			if(empty($userinfo)){
				$x = goLoginLdap($uname, $pwdldap);#ldap
				if($x==1){
					$userinfo = $conn->GetRow("select idanggota,email,namaanggota,a.kdjenisanggota,namajenisanggota,setpagu
								  from ms_anggota a
								  left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
								  where idanggota='$uname'");
				}else{
					echo '0';
					exit;
				}
			}
		}elseif(checkGate($uname, $pwd)){ #bukan ldap
			$userinfo =$conn->GetRow("select idanggota,email,namaanggota,a.kdjenisanggota,namajenisanggota,setpagu
						 from ms_anggota a
						 left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
						 where idanggota='$uname' and statusanggota='1' and j.canlogin=1
						 and (tglexpired > current_date or tglexpired is null) ");
		}
		
		if(!empty($userinfo)) {
			
			$now = date('Y-m-d H:i:s');
				
			 //mengisi session yang penting
			$_SESSION['authenticated'] = 1;
			$_SESSION['userid'] = $userinfo['idanggota'];
			$_SESSION['username'] = $userinfo['namaanggota'];
			$_SESSION['usernama'] = $userinfo['namaanggota'];
			$_SESSION['idanggota'] = $userinfo['idanggota'];
			$_SESSION['email'] = $userinfo['email'];
			$_SESSION['logintime'] = $now;
			$_SESSION['jenisanggota'] = $userinfo['kdjenisanggota'];
			$_SESSION['NMjenis'] = $userinfo['namajenisanggota'];
			$_SESSION['ispagu'] = $userinfo['setpagu'];
			$_SESSION['WEBPERPUS_ROLE'] = $roleinfo;
			
			$lastlogin = $conn->GetOne("select * from (select waktukeluar, Row_Number() OVER (order by idvisitor) linenum
						   from pp_visitor
						   where idanggota='$userinfo[idanggota]' and waktukeluar is not null
						   order by idvisitor desc) where linenum = 1");
			$_SESSION['lastlogintime'] = $lastlogin;
			
			 //mengisi session log
			$record = array();
			$record['idanggota'] = $userinfo['idanggota'];
			$record['t_user'] = $userinfo['idanggota'];
			$record['waktuvisit'] = $now;
			$record['t_loginattempt'] = $_SESSION['loginattempt'];
			$record['t_ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$record['t_host'] = substr(gethostbyaddr($_SERVER['REMOTE_ADDR']),0,30);
			$record['t_osname'] = s_get_os();
			$record['t_browsername'] = s_get_browser();
			$_SESSION['starttime']=date('H:i:s');
			
			Perpus::simpan($conn,$record,'pp_visitor');
			
			unset($_SESSION['loginattempt']);
			
			echo '1'; // login berhasil
		}
		else {
			$_SESSION['loginattempt']++;
			
			echo '0'; // login gagal
		}
	}
	
	function logmasuk() {//session_destroy();
		global $conn;
		$connGate = Factory::getConnGate();
		
		$uid = $_SESSION[G_SESSION]['iduser'];
	
		if(isset($_SESSION['loginuid']) and $_SESSION['loginuid'] != $uid) {
			$_SESSION['loginuid'] = $uid;
			$_SESSION['loginattempt'] = 1;
		}
		else if(!isset($_SESSION['loginuid']))
			$_SESSION['loginuid'] = $uid;
			
		if(Helper::xIsLoggedIn()){ 
			$userinfo =$conn->GetRow("select a.idanggota, a.email, a.namaanggota, a.idunit, a.kdjenisanggota, j.namajenisanggota, setpagu,
							s.namasatker 
						 from ms_anggota a
						 left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
						 left join ms_satker s on s.kdsatker = a.idunit 
						 where idpegawai='$uid' and statusanggota='1' and j.canlogin=1 ");
		}
		
		if(!empty($userinfo)) {		
			$now = date('Y-m-d H:i:s');
				
			 //mengisi session yang penting
			$_SESSION['authenticated'] = 1;
			$_SESSION['userid'] = $userinfo['idanggota'];
			$_SESSION['username'] = $userinfo['namaanggota'];
			$_SESSION['usernama'] = $userinfo['namaanggota'];
			$_SESSION['idanggota'] = $userinfo['idanggota'];
			$_SESSION['unituser'] = $userinfo['idunit'];
			$_SESSION['satker'] = $userinfo['namasatker'];
			$_SESSION['email'] = $userinfo['email'];
			$_SESSION['logintime'] = $now;
			$_SESSION['jenisanggota'] = $userinfo['kdjenisanggota'];
			$_SESSION['NMjenis'] = $userinfo['namajenisanggota'];
			$_SESSION['ispagu'] = $userinfo['setpagu'];
			$_SESSION['WEBPERPUS_ROLE'] = $roleinfo;
			
			$lastlogin = $conn->GetOne("select * from (select waktukeluar, Row_Number() OVER (order by idvisitor) linenum
						   from pp_visitor
						   where idanggota='$userinfo[idanggota]' and waktukeluar is not null
						   order by idvisitor desc) where linenum = 1");
			$_SESSION['lastlogintime'] = $lastlogin;
			
			 //mengisi session log
			$record = array();
			$record['idanggota'] = $userinfo['idanggota'];
			$record['t_user'] = $userinfo['idanggota'];
			$record['waktuvisit'] = $now;
			$record['t_loginattempt'] = $_SESSION['loginattempt'];
			$record['t_ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$record['t_host'] = substr(gethostbyaddr($_SERVER['REMOTE_ADDR']),0,30);
			$record['t_osname'] = s_get_os();
			$record['t_browsername'] = s_get_browser();
			$_SESSION['starttime']=date('H:i:s');
			
			Perpus::simpan($conn,$record,'pp_visitor');
			
			unset($_SESSION['loginattempt']);
		}
		else {
			$_SESSION['loginattempt']++;
		}
		
		Helper::navigateOut();
		exit();
	}
	
	function goGantiPW222($uid, $pwlama, $pwbaru) {
		global $conn;
		
		if($uid == '') {
			echo '-1'; // session user tidak ditemukan
			exit;
		}
		
		//$salt = $connGate->GetOne("select settingvalue from settings where settingname='salt'");
		$salt = Config::rpcsalt;
		$pwlama = md5($pwlama.$salt);
		$pwbaru = md5($pwbaru.$salt);
		
		 //mengecek kebenaran password lama
		$check = $conn->GetOne("select 1 from ms_anggota where idanggota = '$_SESSION[userid]' and password = '$pwlama'");
		
		if($check != 1) {
			echo '-2'; // password lama tidak cocok
			exit;
		}
		
		$query = "update ms_anggota set password = '$pwbaru' where idanggota = '$_SESSION[userid]'";
		$ok = $conn->Execute($query);
		
		if(!$ok)
			echo '-3'; // tidak bisa mengubah password
		else
			echo '1'; // pengubahan password berhasil
	}
	
	function goGantiPW($uid, $pwlama, $pwbaru) {  
		global $conn;
		$connGate = Factory::getConnGate();
		
		if($uid == '') {
			echo '-1'; // session user tidak ditemukan
			exit;
		}
		
		if((checkGate($uid, $pwlama))){	
			$query = "update sc_user set password = md5('$pwbaru') where username = '$_SESSION[userid]'";
			$ok = $connGate->Execute($query);
		}elseif(!(checkGate($uid, $pwlama))){	
			$salt = Config::rpcsalt;
			$pwlama1 = md5($pwlama.$salt); 
			$pwbaru1 = md5($pwbaru.$salt);
			
			$pwlama2 = md5($pwlama1.$salt);
			$pwbaru2 = md5($pwbaru1.$salt); 
			$check = $conn->GetOne("select 1 from ms_anggota where idanggota = '$_SESSION[userid]' and password = '$pwlama2'");
			if($check != 1) {
				echo '-2'; // password lama tidak cocok
				exit;
			}
			$query = "update ms_anggota set password = '$pwbaru2' where idanggota = '$_SESSION[userid]'";
			$ok = $conn->Execute($query);
		}

		
		
		if(!$ok)
			echo '-3'; // tidak bisa mengubah password
		else
			echo '1'; // pengubahan password berhasil
	}
	
	/* NOTE
	* USERNAME dan EMAIL harus unique
	* karena data sekarang banyak yg lubang2 (username msh boleh sama dan email boleh null)
	* maka dilakukan pencegahan secara coding*/
	

	function goLupaPW222($email, $validasi) {
		global $conn, $config;
		
		if ($validasi !== $_SESSION['captcha_keystring']) {
			echo '-1'; // validasi salah
			exit;
		}
		
		$email = trim($email);
		if ($email === '') {
			echo '-2'; // tidak memiliki email
			exit;
		}
		 //mengecek email
		$userinfo = $conn->GetRow("select namaanggota,idanggota,email from ms_anggota where email = '$email'");
		if (empty($userinfo)) {
			echo '-2'; // tidak memiliki email
			exit;
		}
		else {
			$pass = Helper::randPassword();
			//$err = Perpus::sendChangePass($userinfo['idanggota'],$pass,$email,$userinfo['namaanggota']);
			$subject = 'Reset Password e-Digilib PJB';
						
			$body = 'Anda telah mengganti password anda. Data loginnya adalah:'."\n\n";
			$body .= 'Username: '.$userinfo['idanggota']."\n";
			$body .= 'Password: '.$pass."\n\n";
			$body .= 'Anda bisa mengganti password setelah berhasil login ke e-Digilib PJB.'."\n";
			$body .= 'Untuk bisa menggunakan fasilitas e-Digilib PJB, anda harus melakukan login tersebut.'."\n\n";
			$body .= 'Terima kasih';
			
			$ok = Helper::sendMail($userinfo['email'],$userinfo['namaanggota'],$subject,$body);
			echo $ok;
		}
	}
	
	function goLupaPW____belumcekamaperpus($email, $validasi) {
		global $conn, $config;
		$connGate = Factory::getConnGate();
		if ($validasi !== $_SESSION['captcha_keystring']) {
			echo '-1'; // validasi salah
			exit;
		}
		
		$email = trim($email);
		if ($email === '') {
			echo '-2'; // tidak memiliki email
			exit;
		}
		 //mengecek email
		$userinfo = $connGate->GetRow("select username as idanggota, userdsc as namaanggota, email from sc_user where email = '$email'");
		if (empty($userinfo)) {
			echo '-2'; // tidak memiliki email
			exit;
		}
		else {
			$pass = Helper::randPassword();
			//$err = Perpus::sendChangePass($userinfo['idanggota'],$pass,$email,$userinfo['namaanggota']);
			$subject = 'Reset Password e-Digilib PJB';
						
			$body = 'Anda telah mengganti password anda. Data loginnya adalah:'."\n\n";
			$body .= 'Username: '.$userinfo['idanggota']."\n";
			$body .= 'Password: '.$pass."\n\n";
			$body .= 'Anda bisa mengganti password setelah berhasil login ke e-Digilib PJB.'."\n";
			$body .= 'Untuk bisa menggunakan fasilitas e-Digilib PJB, anda harus melakukan login tersebut.'."\n\n";
			$body .= 'Terima kasih';
			
			$ok = Helper::sendMail($userinfo['email'],$userinfo['namaanggota'],$subject,$body);
			echo $ok;
		}
	}
	
	function goResetPW($uid, $pwbaru) {
		global $conn;
				
		//$salt = $conn->GetOne("select settingvalue from settings where settingname='salt'");
		$salt = Config::rpcsalt;
		$pwbaru = md5($pwbaru.$salt);
		
		$query = "update ms_anggota set password = '$pwbaru' where idanggota = $uid";
		$ok = $conn->Execute($query);
		
		if(!$ok)
			echo '-1'; // tidak bisa mengubah password
		else
			echo '1'; // pengubahan password berhasil
	}
	
	function goValC($validasi) {
		if ($validasi !== $_SESSION['captcha_keystring']) {
		    echo '-1'; // validasi salah
		    exit;
		}else
		    echo '1';
	}
	
	function goContValC($validasi) {
		if ($validasi !== $_SESSION['cont_captcha_keystring']) {
		    echo '-1'; // validasi salah
		    exit;
		}else
		    echo '1';
	}
	
	function goHubValC($validasi) {
		if ($validasi !== $_SESSION['hub_captcha_keystring']) {
		    echo '-1'; // validasi salah
		    exit;
		}else
		    echo '1';
	}
	
	function goRepValC($validasi) {
		if ($validasi !== $_SESSION['rep_captcha_keystring']) {
		    echo '-1'; // validasi salah
		    exit;
		}else
		    echo '1';
	}
	
	
	function goLoginLdap($account, $password) {

                $ds = ldap_connect(Config::ldap_server,Config::ldap_port);  // must be a valid LDAP server!
                ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

            if ($ds) {
               //proses mencari dn dari record yang diminta 
               $sr = ldap_search($ds,  Config::ldap_user, "uid=$account");
               $entry = ldap_first_entry($ds, $sr);
               
               // PJB
               //if(empty($entry))
		//return 0;
               
               $dn = ldap_get_dn($ds, $entry);
               
               // PJB
               if(empty($dn))
                return 0;
		
               $r = @ldap_bind($ds, $dn, $password);
               
               if ($r) {


                   //$sr = ldap_search($ds, "ou=people, dc=PJB, dc=ac, dc=id", "uid=$account");

                   //$info = ldap_get_entries($ds, $sr);

                   //$jumlah_hasil = ldap_count_entries($ds, $sr);

                   //$givenname = $info[0]["givenname"][0];
                   //$uid = $info[0]["uid"][0];

                   //$userpassword = $info[0]["userpassword"][0];
                   //echo "Nama: " . $info[0]["cn"][0] . "<br>\n";
                   //echo "given name = $givenname <br/> uid = $uid <br/> password=$userpassword";


                   ldap_close($ds);

                   return 1;
               } else {
                   //$msg = "password tidak cocok";
                   return 0;
               }
           } else {
               //$msg = "Unable to connect to LDAP server";
		return 2;
           }

	}
	
	
	function connSDM(){
			
		$conn = ADONewConnection(Config::driverSdm);
				
		if (!@$conn->Connect(Config::serverSdm,Config::userSdm,Config::passwordSdm,Config::databaseSdm)){
			echo "<blink><font color='red'>Error : Tidak bisa melakukan koneksi dengan server SDM</font></blink>";
		}else{		
		    $conn->SetFetchMode(ADODB_FETCH_ASSOC);
		}
		
		$sql =  $conn->Execute("SET search_path TO 'sdm' ");
		
		return $conn;
	
	}
	
?>
