<?
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
?>
<nav class="navbar navbar-default navbar-fixed-top head">
<div class="header-ribbon bg-red navbar-header">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-sm-3" style="padding:7px 0;">
				<span>perpustakaan.ptpjb.com</span>
				<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				</div>
				<div class="pull-right dropdown collapse navbar-collapse" align="right">
				<a class="dropli" href="<?= Config::KMHOME; ?>">KM</a>
				<ul class="menu" id="menu" style="display:inline">
					<li class="dropLi"><a href="<?= Helper::navAddress('list_home') ?>"><?= $menu['HOME']?></a></li>
					<li class="dropLi"><a href="<?= Helper::navAddress('list_berita.php'); ?>">
					  <?= $menu['INFORMATION']?>
					  </a>
					  <ul class="dropMenu">
						<li><a href="<?= Helper::navAddress('list_berita.php'); ?>" >
						  <?= $menu['NEWS']?>
						  </a></li>
						  <li><span onclick="javascript:goHelp()" >
						  Guide / Bantuan
						  </span></li>
						  <li><a href="<?= Helper::navAddress('data_kontakemail.php'); ?>"><?= $menu['CONTACT']?></a></li>
						<? if (isset($_SESSION['userid'])){  ?>
						<li><a href="<?= Helper::navAddress('list_topikkontak.php'); ?>" >
						  <?= $menu['ONLINE']?>
						  </a></li>
						<? } ?>
					  </ul>
					</li>
					<li class="dropLi"><a href="<?= Helper::navAddress('data_fasilitas.php'); ?>">
					  <?= $menu['ABOUT_US']?>
					  </a>
					  <ul class="dropMenu">
						<li onClick="linkURL('contents','progressbar','<?= $_SESSION['lang'] != 'en' ? Helper::navAddress('data_fasilitas') : Helper::navAddress('data_fasilitasen'); ?>')">
							<?= $sidebar['FACILITY']; ?>
						</li>
						<li onClick="linkURL('contents','progressbar','<?= $_SESSION['lang'] != 'en' ? Helper::navAddress('data_layanan') : Helper::navAddress('data_layananen') ?>')"><?= $sidebar['SERVICE']; ?>
						</li>
						<li onClick="linkURL('contents','progressbar','<?= $_SESSION['lang'] != 'en' ? Helper::navAddress('data_koleksi') : Helper::navAddress('data_koleksien') ?>')"><?= $sidebar['COLLECTION']; ?>
						</li>
						<li onClick="linkURL('contents','progressbar','<?= $_SESSION['lang'] != 'en' ? Helper::navAddress('data_keanggotaan') : Helper::navAddress('data_keanggotaanen') ?>')"><?= $sidebar['MEMBERSHIP']; ?>
						</li>
					  </ul>
					</li>
					<? //if (isset($_SESSION['userid'])){
						if($_SESSION[Config::G_SESSION]['iduser']){?>
					<li class="dropLi">
					Sirkulasi
					<ul class="dropMenu">
					  <li><a href="<?= Helper::navAddress('data_cart.php'); ?>" ><?= $menu['RESERVASI']?></a></li>
					  <li><a href="<?= Helper::navAddress('data_usulanbiasa.php'); ?>" ><?= $menu['OFFER']?></a></li>
					  <li><a href="<?= Helper::navAddress('data_rwtpeminjaman.php'); ?>" >Riwayat Peminjaman</a></li>
					</ul>
					</li>
					<li class="dropLi">
						<a href="<?= Helper::navAddress('data_profile.php'); ?>">
						<span class="glyphicon glyphicon-user"></span> &nbsp;<?= $_SESSION['usernama']; ?></a></a></li>
					<? } ?>
					<script type="text/javascript">
								var menu=new menu.dd("menu");
								menu.init("menu","menuhover");
							</script>
				  </ul>
				<? if($_SESSION[Config::G_SESSION]['iduser']){?>
				<a class="dropli" href="<?= Helper::navAddress('sys_logout.php'); ?>" ><?= $menu['EXIT']?></a>
				<? } ?>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="header">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4"> <a href="index.php"><img src="images/logo.png" style="display:inline;margin-top: 15px;"/></a>

	  </div>
      <div class="col-md-8 search">
	  <h4 style="color:#fff; margin-top:0;">Apa yang Anda cari?</h4>
         <?= UI::createTextBox('txtcari','','form-control txtcari',100,20,true,' onKeyPress="etrCari(event)" onFocus="clearFill();" placeholder="Ketikkan judul buku / pengarang / penerbit"') ?>
		 <button class="btn-search" onClick="goCari()"><span class="glyphicon glyphicon-search"></span> </button>
      <br/>
</div>

    </div>
  </div>
</div>
</div>
</nav>
<script type="text/javascript" src="script/jquery.neonflash.js"></script>
<script type="text/javascript">

$('.dropdown-toggle').dropdown();
		function getLang(id)
		{
			location.href = "<?= $i_phpfile; ?>&lang="+id;
		}
		$('#txtcari').autocomplete(
			'<?= Helper::navAddress('_fts') ?>',
			{
				parse: function(data){
					var parsed = [];
					for (var i=0; i < data.length; i++) {
						parsed[i] = {
							data: data[i],
							value: data[i].judul // nama field yang dicari
						};
					}
					return parsed;
				},
				formatItem: function(data,i,max){
					var str = '<div class="search_content">';
					str += data.judul;
					str += '</div>';
					return str;
				},
				width: 330,
				dataType: 'json'
			})
			.result(
				function(event,data,formated){
					$('#txtcari').val(data.judul);
				}
	);

	function goHelp(){
	    win = window.open("index.php?page=helpdesk","Digilib Helpdesk Support","width=700,height=600,scrollbars=1");
	    win.focus();
	}

	function goTagihan(xjum){
		sent = "&jum=" + xjum;
		var la = '<?= $_SESSION['lang'] ?>';
		if(la=='en')
		goLink('contents','<?= Helper::navAddress('list_tagihanen.php'); ?>', sent);
		else
		goLink('contents','<?= Helper::navAddress('list_tagihan.php'); ?>', sent);
	}

	function goTelat(xjum){
		sent = "&jum=" + xjum;
		var la = '<?= $_SESSION['lang'] ?>';
		if(la=='en')
		goLink('contents','<?= Helper::navAddress('data_rwtpeminjaman.php'); ?>', sent);
		else
		goLink('contents','<?= Helper::navAddress('data_rwtpeminjaman.php'); ?>', sent);
	}

	function clearFill(){
		if ($("#txtcari").val() == '<?= $sidebar['SD_SEARCH']; ?>')
			$("#txtcari").val("");
		else
			$("#txtcari").val();
	}

	function addFill(){
		$("#txtcari").val("<?= $sidebar['SD_SEARCH']; ?>");
	}

	$(document).ready(function() {
		$("#reload").load("<?= Helper::navAddress('_statistik.php') ?>");
		<? if (!empty($isskors)){ ?>
			$("#alert").fadeOut(800).fadeIn(800).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
		<? } ?>
		<? if (!empty($_SESSION['userid'])){ ?>
			$("#pesan").fadeOut(800).fadeIn(800).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
		<? } ?>
		   addFill();
	});


var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

var browser = BrowserDetect.browser;
var ver = BrowserDetect.version;
if(browser == 'Opera') {
	document.getElementById('txtcari').setAttribute('size','35');
	document.getElementById('magnify').style.position = 'relative';
	document.getElementById('magnify').style.pixelTop = '9';
}
else if (browser == 'Safari') {
	document.getElementById('txtcari').setAttribute('size','30');
}
			</script>
