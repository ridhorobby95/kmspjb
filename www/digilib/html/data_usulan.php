<? 
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['OFFER_GREEN'], Helper::navAddress('data_usulan.php'), 0);
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_REQUESTGREEN'];
	$p_title = $form['T_REQUESTGREEN'];
	$p_dbtable = "pp_usul";
	
	//data untuk mendapatkan pagu 
	if ($_SESSION['ispagu'])
		$periode = Perpus::getPeriodePagu($conn);
		$pagu = $conn->GetOne("select coalesce(sisasementara,0) as sisapagu from pp_pagulh where idanggota='".$_SESSION['idanggota']."' and periodeakad='$periode'");		
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){
			
			$record = array();
			$recshare = array();
			$record = Helper::cStrFill($_POST);
			$record['idanggota'] = $_SESSION['idanggota'];
			$record['namapengusul'] = $_SESSION['usernama'];
			$record['tglusulan'] = date('Y-m-d');
			$record['statususulan'] = 0;
			$record['isdenganpagu'] = 1;
			Helper::Identitas($record);
			Helper::Identitas($recshare);
			
			$issave = false;			
			//jika tipe mendapatkan pagu dan harga melebihi pagu tampilkan pesan
			if ($rkey == ''){
				Perpus::Simpan($conn, $record, $p_dbtable);
				if($conn->ErrorNo() != 0)
					$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
				else{
					$record['idusulan'] = $conn->GetOne("select last_value from pp_usul_idusulan_seq");
					Perpus::Simpan($conn, $record, 'pp_orderpustaka');
					
					if($conn->ErrorNo() != 0)
						$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
					else{ 
						$issave = true;
						//share pagu
						if ($record['ispagu'] == 1){
							$arrparam = array('idanggota','paguusulan');
							$recshare = Helper::getArrParam($arrparam,'idanggota');
							$recshare['periodeakad'] = $periode;
							$recshare['idorderpustaka'] = $conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq");
							Perpus::insPengusulanDosen($conn,$recshare,$recshare['idorderpustaka'],$periode);
						}else{
							$recshare['periodeakad'] = $periode;
							$recshare['idorderpustaka'] = $conn->GetOne("select last_value from pp_orderpustaka_idorderpustaka_seq");
							$recshare['idanggota'] = $record['idanggota'];
							$recshare['ispengusulutama'] = 1;
							$recshare['paguusulan'] = $record['hargausulan'];
							Perpus::Simpan($conn, $recshare, 'pp_paguusulan');
							
							$pagusementara = $conn->GetOne("select sisasementara from pp_pagulh where periodeakad='$periode' and idanggota='$recshare[idanggota]'");
						
							$recupdate['sisasementara'] = $pagusementara - $recshare['paguusulan'];	
							Query::recUpdate($conn,$recupdate,'pp_pagulh'," periodeakad='$periode' and idanggota='$recshare[idanggota]'");
					
						}
						
						if($conn->ErrorNo() != 0)
							$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
						else{ 
							$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data berhasil').'</div>';
						}
					}
				}
								
				if ($issave)
					Helper::addHistory($conn, "Pengusulan Label Hijau Baru Berhasil dibuat", Helper::removeSpecial(http_build_query($record)));
				else{
					Helper::addHistory($conn, "Pengusulan Label Hijau Baru Gagal dibuat", "usulan ".$rkey);
					$rset = array();
					$rset = $_POST;
				}
			}else {
				Perpus::Update($conn, $record, $p_dbtable, " idusulan=$rkey");
				if($conn->ErrorNo() != 0)
					$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
				else{
						
						$issave = true;
						//share pagu
						if ($record['ispagu'] == 1){
							$arrparam = array('idanggota','paguusulan');
							$recshare = Helper::getArrParam($arrparam,'idanggota');
							$recshare['periodeakad'] = $periode;
							$recshare['idorderpustaka'] = $conn->GetOne("select idorderpustaka from pp_orderpustaka where idusulan=$rkey");
							Perpus::insPengusulanDosen($conn,$recshare,$recshare['idorderpustaka'],$periode);
						}else{
							$recshare['periodeakad'] = $periode;
							$recshare['idorderpustaka'] = $conn->GetOne("select idorderpustaka from pp_orderpustaka where idusulan=$rkey");
							$recshare['idanggota'] = $record['idanggota'];
							$recshare['ispengusulutama'] = 1;
							$recshare['paguusulan'] = $record['hargausulan'];
							Perpus::Simpan($conn, $recshare, 'pp_paguusulan');
							
							$pagusementara = $conn->GetOne("select sisasementara from pp_pagulh where periodeakad='$periode' and idanggota='$recshare[idanggota]'");
						
							$recupdate['sisasementara'] = $pagusementara - $recshare['paguusulan'];	
							Query::recUpdate($conn,$recupdate,'pp_pagulh'," periodeakad='$periode' and idanggota='$recshare[idanggota]'");
					
						}
						
						if($conn->ErrorNo() != 0)
							$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
						else{ 
							$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data berhasil').'</div>';
						}
					}
					
					if ($issave){
						Helper::addHistory($conn, "Update Pengusulan Label Hijau Berhasil", Helper::removeSpecial(http_build_query($record)));
					}else{
						Helper::addHistory($conn, "Update Pengusulan Label Hijau Gagal", "usulan ".$rkey);
						$rset = array();
						$rset = $_POST;
					}
				}
			
		}
		else if ($r_act == 'hapus'){
			$istrue = false;
			$update = $conn->Execute("select idanggota,paguusulan from pp_paguusulan where idorderpustaka=(select idorderpustaka from pp_orderpustaka where idusulan=$rkey)");
			$periode = Perpus::getPeriodePagu($conn);
			if($update){
				while($rowh = $update->FetchRow()){
					$pagu = $conn->GetOne("select sisasementara from pp_pagulh where periodeakad='$periode' and idanggota='$rowh[idanggota]'");
					$phasil = $pagu  + $rowh['paguusulan'];
					$conn->Execute("update pp_pagulh set sisasementara=$phasil where periodeakad='$periode' and idanggota='".$rowh['idanggota']."'");
					//perpus::Update($conn,$recU, 'pp_pagulh', 'periodeakad=\\'$periode'\\ and idanggota=\\'$rowh['idanggota'].'\\');		
				}
			}
				if($conn->ErrorNo() != 0)
					$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
				else{
					$conn->Execute("delete from pp_paguusulan where idorderpustaka=(select idorderpustaka from pp_orderpustaka where idusulan=$rkey)");
					if($conn->ErrorNo() != 0)
						$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
					else{
						Perpus::Delete($conn, 'pp_orderpustaka', " idusulan=$rkey");
						if($conn->ErrorNo() != 0)
							$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
						else{ 
							Perpus::Delete($conn, $p_dbtable, " idusulan=$rkey");
							if($conn->ErrorNo() != 0)
								$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
							else{ 
								$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan Data berhasil').'</div>';
								$rconfirm = $rkey;
								$rkey='';
								$istrue = true;
							}
						}
					}
				}
			
			if ($istrue)
				Helper::addHistory($conn, "Hapus Pengusulan Label Hijau Berhasil", "idusulan=".$rconfirm);
			else
				Helper::addHistory($conn, "Penghapusan Label Hijau Berhasil", "idusulan=".$rconfirm);
		}
		else if ($r_act == 'hapusdetail'){
			$istrue = false;
			$rdel = Helper::removeSpecial($_REQUEST['rdel']);
			Perpus::delPaguUsulan($conn,$rdel);
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
			else{ 
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan Data berhasil').'</div>';
				$istrue = true;
			}
			
			if ($istrue)
				Helper::addHistory($conn, "Hapus Pagu Usulan Berhasil", "pagu ".$rkey);
			else
				Helper::addHistory($conn, "Penghapusan Pagu Berhasil", "pagu ".$rdel);
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
	
	$isTrue = false;
	if ($rkey != ''){
		$sql = "select p.*,op.idorderpustaka from $p_dbtable p 
				left join pp_orderpustaka op on op.idusulan=p.idusulan
				where p.idusulan=$rkey";
		$rset = $conn->GetRow($sql);
		
		if ($rset['idorderpustaka'] != ''){
			$isTrue = true;
			$sql = "select pu.*,a.namaanggota from pp_paguusulan pu 
					left join ms_anggota a on a.idanggota=pu.idanggota
					where idorderpustaka=$rset[idorderpustaka]";
			$rss = $conn->Execute($sql); 
		}
	}
	
	//data
	$p_sqlstr = "select *,p.judul as judulpustaka,coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'') as pengarang,
				p.penerbit as namapenerbit, p.idusulan as id 
				from $p_dbtable p 
				left join pp_orderpustaka o on p.idusulan=o.idusulan
				left join pp_pengadaan pd on o.idpengadaan=pd.idpengadaan
				where p.idunit is null and idsumbangan is null and idanggota='".$_SESSION['idanggota']."' and isdenganpagu=1 
				order by tglusulan desc";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="style/thickbox.css" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>

	<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post">
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer">
				<span class="menuTitle" style="margin-left:-20px;"><?= $p_title?></span><br>
        		<?= (!$confirm) ?  '' : $confirm; ?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td style="padding-top:5px;"><?= $form['BIBLIO_TITLE']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextArea('judul',$rset['judul'],'ControlStyle',3,50,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['PRICE']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('hargausulan',$rset['hargausulan'],'ControlStyle',20,20,$c_edit); ?>&nbsp;<?= $form['PAGU_REMAINING']; ?> :&nbsp;<?= $pagu; ?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR1']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst1',$rset['authorfirst1'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast1',$rset['authorlast1'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR2']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst2',$rset['authorfirst2'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast2',$rset['authorlast2'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR3']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst3',$rset['authorfirst3'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast3',$rset['authorlast3'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['PUBLISHER']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('penerbit',$rset['penerbit'],'ControlStyle',100,60,$c_edit); ?>&nbsp;<img src="images/tombol/popup.png" id="btnpenerbit" title="Cari Penerbit" style="cursor:pointer" onClick="openLOV('btnpenerbit', 'penerbit',-390, 22,'addPenerbit',450)"></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['YEAR_PUBLISH']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('tahunterbit',$rset['tahunterbit'],'ControlStyle',4,4,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['EDITION']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('edisi',$rset['edisi'],'ControlStyle',100,66,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['ISBN']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('isbn',$rset['isbn'],'ControlStyle',100,66,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['DESC']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextArea('keterangan',$rset['keterangan'],'ControlStyle',3,50,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['PAGU_SHARE']; ?> ? </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><input type="checkbox" name="ispagu" id="ispagu" value="1" style="cursor:pointer" onclick="showShare()" <?= $rset['isdenganpagu'] == 1 ? 'checked' : ''; ?>/>&nbsp;<em style="color:#FF0000"><?= $form['P_SHAREPAGU']; ?></em></td>
					</tr>
					<tr>
						<td colspan="3">
							<table width="100%" cellpadding="4" cellspacing="0" class="inside" id="tblshare" style="display:none">
								<tr>
									<th style="border-bottom:none;" width="75" colspan="3"><?= $form['PAGU_SHARE']; ?></th>
								</tr>
								<tr>
									<td style="padding-top:5px;"><?= $form['MEMBER_NAME']?></td>
									<td style="padding-top:5px;" width="2%">:</td>
									<td style="padding-top:5px;"><input type="hidden" name="idshare" id="idshare" /><?= UI::createTextBox('anggota','','ControlRead',100,60,$c_edit.'readonly'); ?>&nbsp;<img src="images/tombol/popup.png" id="btnanggota" title="Cari Anggota" style="cursor:pointer" onClick="openLOV('btnanggota', 'anggotapagu',-390, 22,'addAnggota',450,'','<?= $periode; ?>')"></td>
								</tr>
								<tr>
									<td style="padding-top:5px;"><?= $form['PAGU_REMAINING']?></td>
									<td style="padding-top:5px;" width="2%">:</td>
									<td style="padding-top:5px;"><?= UI::createTextBox('sisapagu','','ControlRead',100,20,$c_edit,'readonly'); ?></td>
								</tr>
								<tr>
									<td style="padding-top:5px;"><?= $form['PAGU_SHARE']?></td>
									<td style="padding-top:5px;" width="2%">:</td>
									<td style="padding-top:5px;"><?= UI::createTextBox('share','','ControlStyle',100,20,$c_edit,' onkeyDown="return etrShare(event)"'); ?></td>
								</tr>
								<tr>
									<td style="padding-top:5px;"></td>
									<td style="padding-top:5px;"></td>
									<td style="padding-top:5px;">&nbsp;&nbsp;<span name="simpan" class="buttonSubmit" value="Simpan" onclick="addShare()" style="cursor:pointer;"><?= $form['ADD']; ?></span></td>
								</tr>
								<tr>
									<td colspan="3">
										<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
											<tr id="tr_add">
												<th width="150" align="center"><?= $form['MEMBER_NAME']; ?></th>
												<th width="50" align="center"><?= $form['PAGU_SHARE']; ?></th>
												<th width="50" align="center"><?= $form['ACTION']; ?></th>
											</tr>
											<? if ($rkey and $isTrue){
												while ($rows = $rss->FetchRow()){ 
													$totalpagu += $rows['paguusulan'];
												?>
											<tr>
												<td><?= $rows['idanggota'].' - '.$rows['namaanggota']; ?><input type="hidden" name="idanggotadb[]" id="idanggotadb[]" value="<?= $rows['idanggota'] ?>"></td>
												<td align="right"><?= $rows['paguusulan']; ?><input type="hidden" name="paguusulandb[]" id="paguusulandb[]" value="<?= $rows['paguusulan']; ?>"></td>
												<td align="center"><img src="images/tombol/delete.png" width="16" title="Hapus Share" onClick="delRow('<?= $rows['idpaguusulan']?>')" style="cursor:pointer;color:#3300FF;"/></td>
											</tr>
											<? }} ?>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</td>
					<tr>
						<td style="padding-top:5px;"></td>
						<td style="padding-top:5px;"></td>
						<td style="padding-top:5px;">&nbsp;&nbsp;<span name="simpan" class="buttonSubmit" value="Simpan" onclick="goSimpan()" style="cursor:pointer;"><?= $form['SAVE']; ?></span><span name="reset" class="buttonSubmit" value="Reset" style="cursor:pointer" onclick="goUndo()"><?= $form['RESET']; ?></span></td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
				<input type="hidden" name="total" id="total"/>
				<input type="hidden" name="rdel" id="rdel" />
				<input type="hidden" name="redit" id="redit" value="<?= $rkey; ?>" />
			</div>
		</div>
	</form>
	<br />
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<span class="menuTitle" style="margin-left:-20px;"><?= $form['T_HISREQUESTGREEN']; ?></span>
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td>
						<table width="610px" cellpadding="4" cellspacing="0" class="containerTable">
							<tr>
								<th width="75" align="center"><?= $form['DATE_REQUEST']; ?></th>
								<th width="165" align="center"><?= $form['BIBLIO_TITLE']; ?></th>
								<th width="103" align="center"><?= $form['AUTHOR']; ?></th>
								<th width="95" align="center"><?= $form['PUBLISHER']; ?></th>
								<th width="89" align="center"><?= $form['REQ_DATE']; ?></th>
								<th width="89" align="center"><?= $form['ACTION']; ?></th>
							</tr>
							<? 
								$i=0;
								while ($row = $rs->FetchRow()){ $i++;
							?>
							<tr>
								<td align="center" valign="top"><?= Helper::formatDate($row['tglusulan'])?></td>
								<td valign="top"><?= $row['judulpustaka']?></td>
								<td valign="top"><?= $row['pengarang'];?></td>
								<td valign="top"><?= $row['namapenerbit'];?></td>
								<td valign="top" align="center">
								<? 
									if ($row['statususulan'] != '0'){
										if ($row['ststtb'] == 1){
											echo "<img src='images/tombol/realisasi.png' title='Realisasi'>"; 
										}else{
											if ($row['stspo'] == 1){
												echo "<img src='images/tombol/PO.png' title='Pembuatan PO'>"; 
											}else{
												if ($row['stspengadaan'] == 1){
													if ($row['statuspengadaan'] == 'PP')
														echo "<img src='images/tombol/A.png' title='Verifikasi Perpustakaan'>";
													else if ($row['statuspengadaan'] == 'PK')
														echo "<img src='images/tombol/A.png' title='Verifikasi Keuangan'>";
													else if ($row['statuspengadaan'] == 'V')
														echo "<img src='images/tombol/supplier.gif' title='Verifikasi Supplier'>";
												}
											}
										}
									}else
										echo "<img src='images/tombol/delay.png' title='Menunggu'>";
								?>
								</td>
								<td><? if ($row['statususulan'] != '0'){ ?><img src="images/tombol/disedited.gif" title="Tidak bisa Edit"><? }else{ ?><img src="images/tombol/edited.gif" style="cursor:pointer" onclick="goEdit('contents','<?= $i_phpfile?>','&redit=<?= $row['id'];?>')" title="Edit"><span style="position:relative;bottom:8px;"> |</span> <img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelete('contents','<?= $i_phpfile?>','&redit=<?= $row['id'];?>')" title="Hapus"><? } ?></td>
							</tr>
							<? } if ($i == 0) {?>
							<tr>
								<td colspan="6" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
							<tr height="20">
								<td align="left" colspan="6"><strong><?= $form['PAGE']; ?></strong>&nbsp;:&nbsp;
										<?	if($p_page == 0)
												echo '&nbsp;';
											else {
												$start = (($p_page > 9) ? ($p_page-9) : 1);
											$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
												$link = array();
												
												for($i=$start;$i<=$end;$i++) {
													if($i == $p_page)
														$link[] = '<strong>'.$i.'</strong>';
													else{
														if (!isset($_REQUEST['isadv']))
															$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
													}
												}
												echo implode(' ',$link);
											}
										?>
										&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="profile" style="padding-top:0px;">
		<div class="contentContainer">
			<table width="100%" cellpadding="4" cellspacing="0" class="containerTable" style="margin-top:0px;">
				<tr>
					<th style="border-bottom:none;" width="75" colspan="3"><?= $form['DESC']; ?></th>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/delay.png"></td>
								<td width="1">:</td>
								<td>Menunggu</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/PO.png"></td>
								<td width="1">:</td>
								<td>Pembuatan PO</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/A.gif"></td>
								<td width="1">:</td>
								<td>Verifikasi Perpustakaan</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/TTB.png"></td>
								<td width="1">:</td>
								<td>Delivery Order</td>	
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/supplier.gif"></td>
								<td width="1">:</td>
								<td>Verifikasi Supplier</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/realisasi.png"></td>
								<td width="1">:</td>
								<td>Realisasi</td>
							</tr>
						</table>
					</td>
					<td>&nbsp;
						
					</td>
				</tr>
			</table>
		</div>
	</div>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/jquery.masked.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	$(function(){
		$("#perpusform").find("#hargausulan,#share").onlyNum();
	   $("#tahunterbit").mask("9999");
	   showShare();
	});
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
	
	function addAnggota(id,nama,pagu) {
		$("#idshare").val(id);
		$("#anggota").val(nama);
		$("#sisapagu").val(pagu);
	}
	
	function showShare(){
		if ($("#ispagu").attr("checked"))
			$("#tblshare").fadeIn(800);
		else
			$("#tblshare").fadeOut(800);
	} 
	
	function etrShare(e){
		var ev = (window.event) ? window.event: e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key==13)
			addShare();
	}
	
	function addShare()
	{
		var n = 0;
		var numdet = 0;
		if(cfHighlight("anggota,share,hargausulan")){
			if (parseInt($("#share").val()) <= parseInt($("#sisapagu").val())){
				$("input[name^=idanggota]").each(function(i){
					if(parseInt($("#idshare").val()) == parseInt($("input[name^=idanggota]").eq(i).val())){
						$(this).addClass("ControlErr");
						n++;
					}else{
						$(this).removeClass("ControlErr");
					}
					numdet++;
				});
				
				if(n > 0){
					alert("Anggota Sudah Ada");
				}else{
					var baris = '';	
					numdet += 1;
					
					baris = '<tr valign="top" id="tr_detail'+numdet+'">'  + "\n" +
							'	<td>' + $("#idshare").val() + ' - ' + $("#anggota").val() + '</td>'  + "\n" +
							'	<td align="right">' + $("#share").val() + '</td>'  + "\n" +
							'	<td align="center">'  + "\n" +
							'		<input type="hidden" name="idanggota[]" id="idanggota[]" value="' + $("#idshare").val() + '">' + "\n" +
							'		<input type="hidden" name="paguusulan[]" id="paguusulan[]" value="' + $("#share").val() + '">' + "\n" +
							'		<img src="images/tombol/delete.png" width="16" title="Hapus Share" onClick="delDetail(\''+numdet+'\')" style="cursor:pointer;color:#3300FF;"/>'  + "\n" +     
							'	</td>'  + "\n" +
							'</tr>'  + "\n";
					$("#tr_add").after(baris);
					$("#id").val('');
					$("#anggota").val('');
					$("#sisapagu").val('');
					$("#share").val('');
				}
			}else{
				alert("Pagu Melebihi Sisa Pagu Sementara !!");
				$("#share").focus();
			}
		}
	}
	
	function delDetail(row){
		$("#tr_detail"+row).remove(); //hapus row
	}
	
	function goSimpan() {
		if (cfHighlight('judul,hargausulan,authorfirst1')){
			if ($("#ispagu").attr("checked")){
				hitTotal();
				if (parseInt($("#total").val()) >= parseInt($("#hargausulan").val())){
					valid = false;
					$("input[name^=idanggota]").each(function(i){
						if ($("input[name^=idanggota]").eq(i).val() == <?= $_SESSION['idanggota']; ?>)
							valid = true;						
					});
					
					$("input[name^=idanggotadb]").each(function(i){
						if ($("input[name^=idanggotadb]").eq(i).val() == <?= $_SESSION['idanggota']; ?>)
							valid = true;						
					});
					
					if (valid)
						goSave('contents','<?= $i_phpfile?>','');
					else
						alert("Maaf Pagu Anda Belum dimasukkan!");
				}else
					alert("Maaf Total Pagu Anda Belum Memenuhi!!");
			}else{
				if (parseInt(<?= $pagu?>) >= parseInt($("#hargausulan").val())){
					goSave('contents','<?= $i_phpfile?>','');
					$("#contents").divpost({page: '<?= $i_phpfile?>'});
				}
				else
					alert("Maaf Pagu Anda Tidak Mencukupi !!");
			}
		}
	}
	
	function hitTotal(){
		var total = 0;
		var totaldb = 0;
		var nominal = 0;
		var nominaldb = 0;
		$("input[name^=paguusulan]").each(function(i){
			var nominal = parseInt($("input[name^=paguusulan]").eq(i).val());
			total += nominal;	
		});
		
		$("input[name^=paguusulandb]").each(function(i){
			var nominaldb = parseInt($("input[name^=paguusulandb]").eq(i).val());
			totaldb += nominaldb;	
		});
		
		$("#total").val(total+totaldb);
	}
	
	function delRow(id) {
		var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
		if(hapus) {
			sent = $("#perpusform").serialize();
			sent += "&act=hapusdetail&rdel="+id;		
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
</script>
</html>
