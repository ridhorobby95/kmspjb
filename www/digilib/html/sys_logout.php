<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$now = date('Y-m-d H:i:s');

	$id = $_SESSION['userid'];
	
	$idvisit = $conn->GetOne("select max(idvisitor) from pp_visitor where idanggota='$id' ");

	$record = array();
	$record['waktukeluar'] = $now;
	
	$conn->AutoExecute('pp_visitor',$record,'UPDATE',"idvisitor = ".$idvisit);
		
	session_unset();
	
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
	}
	unset($_COOKIE[session_name()]);
	
	session_destroy();
	
	header('Location: '. Helper::navAddress('home.php'));
?>