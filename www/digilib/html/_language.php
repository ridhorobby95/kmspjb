<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//semua yang berhubungan dengan bahasa
	if (isset($_REQUEST['lang']))
		$_SESSION['lang'] = Helper::removeSpecial($_REQUEST['lang']);
	
	//inisialisasi bahasa
	$menu = Language::setLang('menu', $_SESSION['lang']);
	$sidebar = Language::setLang('sidebar', $_SESSION['lang']);
	$form = Language::setLang('form', $_SESSION['lang']);
?>