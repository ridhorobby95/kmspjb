<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//tambahan
	require_once('classes/fts.class.php');
	require_once('classes/breadcrumb.class.php');
		
	$b_link = new Breadcrumb();

	$r_key = Helper::removeSpecial(Helper::strOnly(trim($_REQUEST['key'])));
	$r_alf = Helper::removeSpecial($_REQUEST['alf']);
	
	//post dari halaman adv
	$r_page = Helper::removeSpecial($_REQUEST['numpage']);
	$r_key1 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword1']));
	$r_key2 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword2']));
	$r_bahasa = Helper::removeSpecial($_REQUEST['kdbahasa']);
	$r_topik = Helper::removeSpecial($_REQUEST['topik']);
	$r_topik1 = Helper::removeSpecial($_REQUEST['topik1']);
	$r_topik2 = Helper::removeSpecial($_REQUEST['topik2']);
	$r_tahun1 = Helper::removeSpecial($_REQUEST['tahun1']);
	$r_tahun2 = Helper::removeSpecial($_REQUEST['tahun2']);
	$r_bool = Helper::removeSpecial($_REQUEST['bool']);
	
	$word = $_REQUEST['isadv'];
	
//echo $_REQUEST;die();	
	//settingan untuk halaman
	$p_window = "[Digilib] Hasil Pencarian";
	$p_title = "Hasil Pencarian";
	$p_id = "ta";
		
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 10; //default
		
			
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['paging']);
		
		// simpan session ex
		$_SESSION[$p_id.'.paging'] = $p_page;
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.paging'])
			$p_page = $_SESSION[$p_id.'.paging'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	if ($r_alf==''){
	if (!isset($_REQUEST['isadv']))
		$p_sqlstr = FTS::cariPustakata($r_key,$r_topik);
	else{
		//$p_sqlstr = FTS::cariPustakaADVta($r_key1, $r_key2, $r_bool,$r_topik1,$r_topik2,$r_bahasa,$r_tahun1,$r_tahun2);
	        $p_sqlstr = FTS::cariPustakaADVsta($r_key1, $r_key2, $r_bool,$r_topik1,$r_topik2,$r_bahasa,$r_tahun1,$r_tahun2);
	    	if(!$conn->GetOne($p_sqlstr))
	    		$p_sqlstr = FTS::cariPustakaADVta($r_key1, $r_key2, $r_bool,$r_topik1,$r_topik2,$r_bahasa,$r_tahun1,$r_tahun2);
	}
	}elseif ($r_alf==1)
		$p_sqlstr = FTS::cariJudul($r_key,true);
	elseif ($r_alf==2)
		$p_sqlstr = FTS::cariAuthor($r_key,true);
		
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	if (!isset($_REQUEST['isadv']))									
	$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcarita.php').'&paging='.$p_page.'&alf='.$r_alf.'&topik='.$r_topik.'&key='.$r_key, 0);
	else
	$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcarita.php').'&paging='.$p_page.'&numpage='.$r_page.'&keyword1='.$r_key1.'&keyword2='.$r_key2.'&kdbahasa='.$r_bahasa.'&topik1='.$r_topik1.'&topik2='.$r_topik2.'&tahun1='.$r_tahun1.'&tahun2='.$r_tahun2.'&bool='.$r_bool.'&isadv=1',0);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="style/pager.css" />
<script src="script/searchhi.js" type="text/javascript"></script>
<title><?= $p_window;?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body onLoad="JavaScript:loadSearchHighlight();">
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
			<td><h4 class="sidebar-title"><?= $form['SEARCH_RESULT']; ?></h4></td>
		</tr>
		<tr height="20">
			<td>&nbsp;</td>
		</tr>
		<tr height="20">
			<td><?= $form['PAGE']?> <?= $p_page ?> <?= $form['FROM_RESULT']?> <strong><?= $p_recount ?></strong> <?= $form['RESULT_FOR']?> <?= UI::message(($word?$r_key1:$r_key),true)?></td>
		</tr>
	<? while ($row = $rs->FetchRow()){?>
		<tr>
			<td>
				<table width="100%" class="result" border="0" cellpadding="4" cellspacing="0" onMouseOver="this.className='resultH'" onMouseOut="this.className='result'" style="border-top:1pt dotted #d2d2d2;">
					<tr>
						<td class="resultTitle" colspan="2"><img src="images/book_open.png" />&nbsp;&nbsp;<a class="judul-buku" title="Lihat" onClick="goLihat('<?= $row['idpus']?>')"><?= $row['judul'] != '' ? $row['judul'] : '*'?></a>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td class="resultDesc"><strong>REG. COMP</strong></td><td>: <?= $row['noseri'] ?></td>
					</tr>
					<tr>
						<td class="resultSubB" width="80" valign="top"><strong><?= $form['AUTHOR']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= $row['authorfirst1'].' '.$row['authorlast1'];?></td>
					</tr>
					<tr>
						<td class="resultSubB" width="90" valign="top"><strong>Contributor</strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= $row['pembimbing'] ?></td>
					</tr>
					<tr>
						<td class="resultSubB" width="80" valign="top"><strong><?= $form['DIRECTORY']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= $row['namajenispustaka'];?>]</td>
					</tr>
				</table>
				<br />
			</td>
		</tr>
	<? } ?>
		<tr>
			<td align="center"><?= $form['PAGE']?>&nbsp;:&nbsp;
					<?	if($p_page == 0)
							echo '&nbsp;';
						else {
							$start = (($p_page > 9) ? ($p_page-9) : 1);
						$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
							$link = array();
							
							for($i=$start;$i<=$end;$i++) {
								if($i == $p_page)
									$link[] = '<strong>'.$i.'|</strong>';
								else{
									if (!isset($_REQUEST['isadv']))
										$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a>|</strong>';
									else
										$link[] = '<strong><a href="javascript:goPageADV('.$i.')">'.$i.'</a>|</strong>';
								}
							}
							echo implode(' ',$link);
						}
					?>
			</td>
		</tr>
	</table>
</body>
<script type="text/javascript">
	listhasil = "<?= Helper::navAddress('xlist_hasilcarita.php'); ?>";
	databp = "<?= Helper::navAddress('data_pustaka.php'); ?>";
	
	function goPage(npage) {
		var sent = "&topik=<?= $r_topik; ?>&key=<?= $r_key?>&alf=<?= $r_alf ?>&paging=" + npage;
		$("#resultSearch").divpost({page: listhasil, sent: sent});
	}
	
	function goPageADV(npage) {
		var sent = "&keyword1=<?= $r_key1 ?>&keyword2=<?= $r_key2 ?>&kdbahasa=<?= $r_bahasa; ?>&topik1=<?= $r_topik1; ?>&topik2=<?= $r_topik2; ?>&tahun1=<?= $r_tahun1 ?>&tahun2=<?= $r_tahun2 ?>&bool=<?= $r_bool; ?>&isadv=1&numpage=<?= $r_page; ?>&paging=" + npage;
		$("#resultSearch").divpost({page: listhasil, sent: sent});
	}
	 
	function goLihat(key){
		var sent = "&key=" + key;
		$("#contents").divpost({page: databp, sent: sent});
	}
</script>
</html>
