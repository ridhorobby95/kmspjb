<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	//tambahan
	require_once('classes/fts.class.php');
	require_once('classes/breadcrumb.class.php');
		
	$b_link = new Breadcrumb();
	$r_key = Helper::removeSpecial(trim($_REQUEST['key']));
	$rkey = trim($_REQUEST['key']);
	$r_alf = Helper::removeSpecial(trim($_REQUEST['alf']));
	
	$connLamp = Factory::getConnLamp();
	$connLamp->debug = false;
	$cek = $connLamp->IsConnected();
	
	//post dari halaman adv
	$r_page = Helper::removeSpecial($_REQUEST['numpage']);
	$r_key1 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword1']));
	$r_key2 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword2']));
	$r_bahasa = Helper::removeSpecial($_REQUEST['kdbahasa']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenispustaka']);
	$r_jenis1 = Helper::removeSpecial($_REQUEST['jenispustaka1']);
	$r_jenis2 = Helper::removeSpecial($_REQUEST['jenispustaka2']);
	$r_topik = Helper::removeSpecial($_REQUEST['topik']);
	$r_topik1 = Helper::removeSpecial($_REQUEST['topik1']);
	$r_topik2 = Helper::removeSpecial($_REQUEST['topik2']);
	$r_tahun1 = Helper::removeSpecial($_REQUEST['tahun1']);
	$r_tahun2 = Helper::removeSpecial($_REQUEST['tahun2']);
	$r_bool = Helper::removeSpecial($_REQUEST['bool']);
	
	$r_lokasi = Helper::removeSpecial($_REQUEST['lokasi']);
	
	$peng = $conn->GetArray("select p.namadepan || ' ' || p.namabelakang as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor   ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	$topk = $conn->GetArray("select namatopik, tp.idpustaka
			from pp_topikpustaka tp
			join lv_topik t on tp.idtopik=t.idtopik ");
	$topik = array();
	foreach($topk as $t){
		$topik[$t['idpustaka']][] = $t['namatopik'];
	}
	
	//settingan untuk halaman
	$p_window = "[Digilib] Hasil Pencarian";
	$p_title = "Hasil Pencarian";
	$p_id = "scommon";
		
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 10; //default
			
	if (!empty($_POST))
	{
		
		if (!empty($_REQUEST['paging'])){
			$p_page 	= Helper::removeSpecial($_REQUEST['paging']);
			// simpan session ex
			$_SESSION[$p_id.'.paging'] = $p_page;
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.paging'])
			$p_page = $_SESSION[$p_id.'.paging'];
	}
	  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
	if ($r_alf==''){
	if (!isset($_REQUEST['isadv']))
		$p_sqlstr = FTS::cariPustaka($r_key,$r_lokasi,$r_jenis);
	else{
		$p_sqlstr = FTS::cariPustakaADVs($r_key1, $r_key2, $r_bool,$r_topik1,$r_topik2,$r_bahasa,$r_tahun1,$r_tahun2,$r_jenis1,$r_jenis2);
		    if(!$conn->GetOne($p_sqlstr))
			$p_sqlstr = FTS::cariPustakaADV($r_key1, $r_key2, $r_bool,$r_topik1,$r_topik2,$r_bahasa,$r_tahun1,$r_tahun2,$r_jenis1,$r_jenis2);
		
	}
	}elseif ($r_alf==1)
		$p_sqlstr = FTS::cariJudul($r_key,false);
	elseif($r_alf==2)
		$p_sqlstr = FTS::cariAuthor($r_key,false);
			
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);

	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	if (!isset($_REQUEST['isadv']))	{								
		$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcari.php').'&paging='.$p_page.'&alf='.$r_alf.'&lokasi='.$r_lokasi.'&key='.$r_key, 0);
	}else
	$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcari.php').'&paging='.$p_page.'&numpage='.$r_page.'&keyword1='.$r_key1.'&keyword2='.$r_key2.'&kdbahasa='.$r_bahasa.'&topik1='.$r_topik1.'&topik2='.$r_topik2.'&jenispustaka1='.$r_jenis1.'&jenispustaka2='.$r_jenis2.'&tahun1='.$r_tahun1.'&tahun2='.$r_tahun2.'&bool='.$r_bool.'&isadv=1',0);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="style/pager.css" />
<script src="script/searchhi.js" type="text/javascript"></script>
<script type="text/javascript" src="script/auth.js"></script>
<title><?= $p_window;?></title>
</head>
<body onLoad="JavaScript:loadSearchHighlight();">

    <div class="primary">
      <div>
	
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
			<td><h4 class="sidebar-title"><?= $form['SEARCH_RESULT']; ?></h4></td>
		</tr>
		<tr height="20">
			<td>&nbsp;</td>
		</tr>
		<tr height="20">
			<td><?= $form['PAGE']?> <?= $p_page ?> <?= $form['FROM_RESULT']?> <strong><?= $p_recount ?></strong> <?= $form['RESULT_FOR']?>
			<strong><font color="#428BCA"><?=$rkey;?></font></strong>
			</td>
		</tr>
	<? while ($row = $rs->FetchRow()){ ?>
		<tr>
			<td>
				<table width="100%" class="result" border="0" cellpadding="4" cellspacing="0" onMouseOver="this.className='resultH'" onMouseOut="this.className='result'" style="border-top:1pt dotted #d2d2d2;">
					<tr>
						<?
							$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ".$row['idpustaka'];
							$rsCover = $connLamp->GetOne($sqlCover);
							$cover = Perpus::loadBlobFile($rsCover, 'jpg'); /*aktifkan lagi nanti*/
						?>
						<td class="resultTitle" rowspan="6" width="120" align="center" style="padding-left:12px;padding-right:12px;">
							<img id="imgfoto" src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" width="100" height="108">
							<!--img id="imgfoto" src="<?//= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" width="100" height="108"-->
							<br/>
						</td>
						<td class="resultSubB" colspan="2">
							<a class="judul-buku" title="Lihat" onClick="goLihat('<?= $row['idpustaka']?>')"><?= $row['judul'] != '' ? Helper::repCari($row['judul'],$rkey) : '*'; ?><?= $row['edisi'] != '' ? ' / '.Helper::repCari($row['edisi'],$rkey) : ''; ?></a>
						</td>
					</tr>
					<tr>
						<td class="resultDesc"><strong><?= $form['NO_CALL']?></strong> </td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= Helper::repCari($row['nopanggil'],$rkey) ?></td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['AUTHOR']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">:
						<?php
							$auth = array();
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									$auth[] = $a;
								}
								echo Helper::repCari(implode(", ",$auth),$rkey);
							}else
								echo Helper::repCari($row['authorfirst1'],$rkey).' '.Helper::repCari($row['authorlast1'],$rkey);
						?>
						</td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['DIRECTORY']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= Helper::repCari($row['namajenispustaka'],$rkey);?>]</td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['YEAR_PUBLISH']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= Helper::repCari($row['tahunterbit'],$rkey);?>]</td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['TOPIC']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: 
						<?php
							$auth = array();
							if(!empty($topik[$row['idpustaka']])){
								foreach($topik[$row['idpustaka']] as $tp){
									$topic[] = $tp;
								}
								echo Helper::repCari(implode(", ",$topic),$rkey);
							}
						?>
						</td>
					</tr>
					<?php if($row['linkpustaka']){?>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong>LINK</strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: 
						<a href="<?=Helper::repCari($row['linkpustaka'],$rkey);?>" target="_blank">KLIK DI SINI</a>
						</td>
					</tr>
					<?php } ?>
				</table>
				<br />
			</td>
		</tr>
	<? } ?>
		<tr>
			<td align="center"><?= $form['PAGE']?>&nbsp;:&nbsp;
					<?	if($p_page == 0)
							echo '&nbsp;';
						else {
							$start = (($p_page > 9) ? ($p_page-9) : 1);
						$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
							$link = array();
							
							for($i=$start;$i<=$end;$i++) {
								if($i == $p_page)
									$link[] = '<strong>'.$i.'|</strong>';
								else{
									if (!isset($_REQUEST['isadv']))
										$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a>|</strong>';
									else
										$link[] = '<strong><a href="javascript:goPageADV('.$i.')">'.$i.'</a>|</strong>';
								}
							}
							echo implode(' ',$link);
						}
					?>
			</td>
		</tr>
	</table>

  </div>
  
</div>
</body>
<script type="text/javascript">
	listhasil = "<?= Helper::navAddress('xlist_hasilcari.php'); ?>";
	databp = "<?= Helper::navAddress('data_pustaka.php'); ?>";
	
	function goPage(npage) {
		var sent = "&topik=<?= $r_topik; ?>&key=<?= $r_key?>&alf=<?= $r_alf ?>&jenispustaka=<?= $r_jenis ?>&paging=" + npage;
		$("#resultSearch").divpost({page: listhasil, sent: sent});
	}
	
	function goPageADV(npage) {
		var sent = "&keyword1=<?= $r_key1 ?>&keyword2=<?= $r_key2 ?>&kdbahasa=<?= $r_bahasa; ?>&jenispustaka1=<?= $r_jenis1; ?>&jenispustaka2=<?= $r_jenis2; ?>&topik1=<?= $r_topik1; ?>&topik2=<?= $r_topik2; ?>&tahun1=<?= $r_tahun1 ?>&tahun2=<?= $r_tahun2 ?>&bool=<?= $r_bool; ?>&isadv=1&numpage=<?= $r_page; ?>&paging=" + npage;
		$("#resultSearch").divpost({page: listhasil, sent: sent});
	}
	 
	function goLihat(key){
		var sent = "&key=" + key;
		$("#contents").divpost({page: databp, sent: sent});
	}
</script>
</html>
