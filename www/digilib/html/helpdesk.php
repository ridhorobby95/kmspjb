<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	//$conn->debug=true;

	if($_SESSION['lang']=='en')
		$rs = $conn->Execute("select idhelp,namahelpen as menu from pp_helpdesk where tampil='1' order by idhelp");
	else
		$rs = $conn->Execute("select idhelp,namahelp as menu from pp_helpdesk where tampil='1' order by idhelp");

	$r_key = Helper::removeSpecial($_POST['key']);
	
	if($r_key=='')
		$r_key = 1;
	
	if($r_key){
		if($_SESSION['lang']=='en')
			$ket = $conn->GetOne("select keteranganen as ket from pp_helpdesk where idhelp=$r_key");
		else
			$ket = $conn->GetOne("select keterangan as ket from pp_helpdesk where idhelp=$r_key");
	}
?>

<html>
	<head>
		<title>
		Digilib Guide Support
		</title>
		<link href="style/style.css" rel="stylesheet" type="text/css"  />
		<style type="text/css">
		body{padding-top:0 !important;}
		body,td {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 10pt;
		
		}
		.header h2{padding-right:20px; color:#fff; font-size:15px;}
		.header h1{color:#fff; line-height:30px; font-size:30px;}
		.menu-red{background:#219aaa; color:#fff; padding:5px;}
		.menu-red a{color:#fff;}
		</style>
	</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" style="background-color:#f0f0f0;">

<div class="header">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="images/logo.png" style="float:left" >
			<h1>PERPUSTAKAAN<br/> PJB</h1>
		</td>
		<td align="right">
		<h2>Guide / Help</h2>&nbsp;&nbsp;
		</td>
	</tr>
</table>
</div>
<table width="100%" cellpadding=0 cellspacing=0>
	<tr>
		<td class="menu-red" height="23">
			<?
			while($row = $rs->FetchRow()){ ?>
			[ <a href="javascript:goGuide('<?= $row['idhelp'] ?>')" title="Detail Guide" ><?= $row['menu'] ?></a> ] 
			<? } ?>
		</td>
	</tr>
	<tr>
		<td style="background:#f0f0f0; padding:10px;">
		<br>
		<?= $ket ?>
		</td>
	</tr>
</table>
<form name="digiform" id="digiform" method="post">
	<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
</form>

</body>
<script type="text/javascript">
	function goGuide(idhelp) {
		document.getElementById('key').value = idhelp;
		document.digiform.submit();
	}
</script>
</html>