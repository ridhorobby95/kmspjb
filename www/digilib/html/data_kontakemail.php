<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once("classes/perpus.class.php");
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['CONTACT_US'], Helper::navAddress('data_kontakemail.php'),0);
		
	//setting utnuk halaman
	$p_window = "[Digilib] ".$menu['CONTACT_US'];
	$p_title = $menu['CONTACT_US'];
	
	$p_dbtable = 'pp_kontakonline';
	
	if (isset($_POST)){
		$r_act = Helper::removeSpecial($_POST['act']);
		
		if ($r_act == 'kirim'){
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['judul'] = 'Email dari '.$record['t_author'];
			$record['t_input'] = date('Y-m-d H:i:s');
			$record['idkontak'] = 0;
			Helper::Identitas($record);		
			
			$err = Perpus::Simpan($conn,$record,$p_dbtable);
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Pengiriman Gagal',true).'</div>';	
			else{
				//Helper::sendMail(Config::AdminEmail,$record['t_author'],'Kontak Email',$record['keterangankontak']);
				//if ($record['email']=='') $record['email'] = 'noemail@unknown.com';
				//Helper::sendMailKontakPerpus($record['email'],$record['t_author'],'Kontak Email [dari '.$record['t_author'].']',$record['keterangankontak']);
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Pengiriman Berhasil').'</div>';	
			}
			
		}
	}
	
	
	$rs_cb = $conn->Execute("select namajenisanggota as label, namajenisanggota from lv_jenisanggota order by namajenisanggota");
	$l_jenis = $rs_cb->GetMenu2('jenisanggota','',true,false,0,'class="ControlStyle  form-control" style="width:200"');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<form name="frmemail" id="frmemail" action="<?= $i_phpfile?>" method="post">
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
				<h4 class="sidebar-title"><?= $menu['CONTACT_US']?></h4>
        		<?= (!$confirm) ?  '' : $confirm; ?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td colspan="3" valign="top" height="30"><div id="wait_div" style="padding-left:15px;font-weight:bold;"></div></td>
					</tr>
					<tr>
						<td width="120px" style="padding-top:5px;"><?= $form['NAME']; ?> *</td>
						<td width="20px" style="padding-top:5px;">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('t_author',$row['t_author'],'ControlStyle form-control',50,43,true); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['TELP']; ?></td>
						<td width="20px" style="padding-top:5px;">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('telp',$row['telp'],'ControlStyle form-control',50,43,true); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['MEMBER_TYPE']; ?></td>
						<td width="20px" style="padding-top:5px;">:</td>
						<td style="padding-top:5px;"><?= $l_jenis; ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['EMAIL']; ?> *</td>
						<td width="20px" style="padding-top:5px;">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('email',$row['email'],'ControlStyle  form-control',50,43,true); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['ADVICE']; ?> *</td>
						<td width="20px" style="padding-top:5px;">:</td>
						<td style="padding-top:5px;" valign="top"><?= UI::createTextArea('keterangankontak',$row['keterangankontak'],'ControlStyle form-control',3,50,true); ?></td>
					</tr>
					<tr>
						<td rowspan="3" colspan="2"><img src="includes/kcaptcha/hub.php?<?= mt_rand();?>" title="Ulangi validasi" id="captcha" style="cursor:pointer" onClick="this.src='includes/kcaptcha/hub.php?'+Math.random();"></td>
						<td height="20" valign="bottom"><strong><?= $form['VAL_CODE']?></strong> *</td>
					</tr>
					<tr>
						<td><?= UI::createTextBox('validasi','','controlStyle  form-control',50,50); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><span name="simpan" class="buttonSubmit  btn btn-sm btn-primary" value="Kirim" onclick="goSave()" style="cursor:pointer;"><?= $form['SENT']; ?></span></td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
			</div>
		</div>
	</form>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	$(function(){
		$("#frmemail").find("#telp").onlyNum();
	});
	
        function ValidateEmail(inputText)  
        {  
            var mailformat = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";  
            if(inputText.value.match(mailformat))  
            {  
                document.form1.text1.focus();  
                return true;  
            }  
            else  
            {  
                alert("You have entered an invalid email address!");  
                document.form1.text1.focus();  
                return false;  
            }  
        }  	
		
	function goSave()
	{
		if(cfHighlight("t_author,email,keterangankontak,validasi")){
			file = "<?= Helper::navAddress('functionx.php')?>";
			if (cmail("email")){
				sent = $("#frmemail").serialize();
				sent += "&act=kirim";
				v = $("#validasi").val();
				$.post(file,'f=hubcaptcha&p1='+v,function(retlog) {
					//alert(retlog);
					if(retlog == 1) { 
						goLink('contents','<?= $i_phpfile?>', sent);
					}else{
						$("#wait_div").html('<font color="red">Kode Validasi yang dimasukkan Salah</font>');
						document.getElementById("captcha").src = 'includes/kcaptcha/hub.php?'+Math.random();
					}
				});
			}
		}
	}
</script>
</html>
