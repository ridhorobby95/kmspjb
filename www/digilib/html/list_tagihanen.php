<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);
		
	require_once('classes/perpus.class.php');
	
	$r_sum = Helper::removeSpecial($_REQUEST['jum']);
		
	//setting utnuk halaman
	$p_window = "[Digilib] Daftar Tagihan";
	$p_title = "List Your Bill";
	
	$sql = "select t.*,p.*,e.*,m.*,e.ideksemplar as eksemplar,e.noseri from pp_transaksi t 
			left join pp_eksemplar e on t.ideksemplar=e.ideksemplar
			left join ms_pustaka p on e.idpustaka=p.idpustaka
			left join pp_author a on a.idpustaka=p.idpustaka
			left join ms_author m on m.idauthor=a.idauthor
			where t.idanggota='$_SESSION[idanggota]' 
			and t.tgltenggat < current_date and statustransaksi='1' and t.kdjenistransaksi='PJN'";
	$rs = $conn->Execute($sql);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
				<span class="menuTitle" style="margin-left:-20px;"><?= $p_title; ?></span><br /><br />
			<? if ($r_sum > 0){?>
				<table width="98%" cellpadding="4" cellspacing="0" style="padding-top:20px">
					<tr>
						<td>Dear, </td>
					</tr>
					<tr>
						<td><p>To record the return Administration loan books that have been borrowed by library users Surabaya University, let us remind, that according to the databases held by us, you still have the loan book as follows : </p><br />
						<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
							<tr>
								<th rowspan="2" align="center">No.</th>
								<th rowspan="2" align="center">TITLE</th>
								<th rowspan="2" align="center">AUTHOR</th>
								<th rowspan="2" align="center">REG. COMP</th>
								<th rowspan="2" align="center">CALL NUMBER</th>
								<th colspan="2" align="center">DATE</th>
								<th rowspan="2" align="center">DENDA</th>
							</tr>
							<tr>
								<th align="center">LOANS</th>
								<th align="center">BACK</th>
							</tr>
							<? $i=0;while ($row = $rs->FetchRow()){$i++;
							$dendaperbuku = Perpus::hitungdendaperbuku($row['tgltenggat'],date('Y-m-d'),$row['idanggota'],$row['ideksemplar']);
							
							?>
							<tr>
								<td><?= $i; ?></td>
								<td><?= $row['judul']?></td>
								<td><?= $row['namadepan'].' '.$row['namabelakang']?></td>
								<td><?= $row['noseri']?></td>
								<td><?= $row['nopanggil']?></td>
								<td><?= Helper::formatDate($row['tgltransaksi']);?></td>
								<td><?= Helper::formatDate($row['tgltenggat'])?></td>
								<td align="right"><?= number_format($dendaperbuku,0,',','.');?></td>
							</tr>
							<? }if ($i == 0){ ?>
							<tr>
								<td colspan="8" align="center">Can't be found</td>
							</tr>
							<? } ?>
						</table>
						</td>
					</tr>
				</table>
			<? }else{ ?>
				<table width="98%" cellpadding="4" cellspacing="0" style="padding-top:20px">
					<tr>
						<td align="center">Can't be found</td>
					</tr>
				</table>
			<? } ?>
			</div>
		</div>
		
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
</html>
