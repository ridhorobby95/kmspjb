<?php 
	set_time_limit(0);
	define( '__VALID_ENTRANCE', 1 );
	chdir(dirname(__FILE__));
	error_reporting(E_ERROR | E_WARNING);
	
	require_once('../includes/init.php');
	require_once('../classes/mail.class.php');

	$cek = $conn->IsConnected();

	if(!$cek){
	    echo '<span style="color: red;"><b>Tidak ada koneksi ke Database.</b></span>';
	    Mail::SendMail("abu_wes@yahoo.com","Perpustakaan PJB","Error Schedule Cron Perpustakaan","Eksekusi Cron dari Server Perpustakaan untuk mengambil data pegawai dari database Akademik mengalami gangguan, sehingga tidak dapat berjalan dengan baik. Mohon dilakukan check data.\n Cron Date : ".date('d-m-Y H:i:s')); 
		die();
	}
	
	
#cron ini untuk unit
	$sqlstr = "select * from um.unit u ";
	$rs = $conn->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	$u = 0;
	echo "<b>.:Cron Data Unit:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	while ($row = $rs->FetchRow()){
		$u++;
		$record=array();
		$record['kdsatker']=Helper::removeSpecial($row['kodeunit']);
		$record['namasatker'] = Helper::removeSpecial($row['nama']);
		
		$kdsatker = $conn->GetOne("select kdsatker from ms_satker where kdsatker='".trim($row['kodeunit'])."' ");
		if(!$kdsatker){
			echo $u.". Ubah Data Unit ".$row['kodeunit']." - ".$row['nama']."<br/>";
			$col = $conn->Execute("select * from ms_satker where 1=-1");
			$sql = $conn->GetInsertSQL($col,$record,true);
			echo "SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data unit gagal ditambahkan.</span><br/>';
			}else
				echo '<span style="color: blue;">Data Unit telah ditambahkan.</span><br/>';
			
		}else{
			echo $u." Tambah Unit ".$row['kodeunit']." - ".$row['nama']."<br/>";
			$idunit = trim($row['idunit']);
			$col = $conn->Execute("select * from ms_satker where kdsatker = '$kdsatker' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			echo "SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data unit gagal diubah.</span><br/>';
			}else
				echo '<span style="color: blue;">Data Unit telah diubah.</span><br/>';
		}
	}
	
	if($u==0)
		echo "<b>.:Tidak ada data unit yang ditambahkan/diubah:.</b>";
		
	echo "<br/><br/><br/>";
	
#cron ini untuk pegawai
	$start = date('Y-m-d', strtotime('-4 days')); 
	$end = date('Y-m-d'); 
	$sqlstr = "select u.nid, u.nama as namaanggota, u.jeniskel as jk, u.alamat, u.telp, u.email, u.kodepos, 
			u.iduser, u.kodeunit, u.kodestatus, u.isactive  
			from um.users u ";
			//where TO_DATE(TO_CHAR (u.tupdatetime, 'YYYY-mm-dd'),'YYYY-mm-dd') between to_date('$start','YYYY-mm-dd') and to_date('$end','YYYY-mm-dd') ";

	$rs = $conn->Execute($sqlstr); 
	$now = date('Y-m-d H:i:s');
	$p = 0;
	echo "<b>.:Cron Data User:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	while ($row = $rs->FetchRow()){
		$p++;
		$record=array();
		$record['idpegawai']=Helper::removeSpecial($row['iduser']);
		$record['statusanggota']=(Helper::removeSpecial($row['isactive'])?Helper::removeSpecial($row['isactive']):"0"); 
		$record['idunit']=Helper::removeSpecial($row['kodeunit']);
		$record['kdjenisanggota']=(Helper::removeSpecial(trim($row['kodestatus'])) <> "" ? Helper::removeSpecial(trim($row['kodestatus'])) : "T");
		$record['idanggota']=Helper::removeSpecial(trim($row['nid']));
		$record['namaanggota']=Helper::removeSpecial($row['namaanggota']);
		$record['jk']=Helper::removeSpecial($row['jk']);
		$record['alamat']=Helper::removeSpecial($row['alamat']);
		$record['kodepos']=Helper::removeSpecial($row['kodepos']);
		$record['email']=Helper::removeSpecial($row['email']);
		$record['telp']=Helper::removeSpecial($row['telp']);
		$record['t_user'] = 'cron';
		$record['t_updatetime'] = $now;
		$record['t_host'] = Helper::removeSpecial($_SERVER['REMOTE_ADDR']);	
		
		$cNrp = $conn->GetOne("select count(*) from ms_anggota where idanggota = '".trim($row['nid'])."' ");
		if($cNrp>0){
			echo $p.". Ubah Data Users ".$row['nid']."<br/>";
			$col = $conn->Execute("select * from ms_anggota where idanggota = '".trim($row['nid'])."' ");
			$sql = $conn->GetUpdateSQL($col,$record,true);
			echo "SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data user gagal diubah.</span><br/>';
			}else
				echo '<span style="color: blue;">Data user telah diubah.</span><br/>';
		}else{
			echo $p.". Tambah Data Users ".$row['nid']."<br/>";
			$col = $conn->Execute("select * from ms_anggota where 1=-1 ");
			$sql = $conn->GetInsertSQL($col,$record,true);
			echo "SQL : ".$sql."<br/>";
			$err = $conn->Execute($sql);
			if(!$err){
				echo '<span style="color: red;">Data user gagal ditambah.</span><br/>';
			}else
				echo '<span style="color: blue;">Data user telah ditambah.</span><br/>';
		}
	}
	
	if($p==0)
		echo "<b>.:Tidak ada data user yang ditambahkan/diubah:.</b>";
		
	echo "<br/><br/><br/>";
	
#cron tagihan
	$sqlstr = "select a.namaanggota, a.idanggota, count(t.idtransaksi) as tagih 
		from ms_anggota a 
		left join pp_transaksi t on a.idanggota = t.idanggota 
		where  to_date(TO_CHAR(ADD_MONTHS(t.tgltenggat,1),'dd-mm-yyyy'),'dd-mm-yyyy') < to_date(TO_CHAR(current_date,'dd-mm-yyyy'),'dd-mm-yyyy') 
			and t.tglpengembalian is null and t.kdjenistransaksi='PJN'
		group by a.idanggota,a.namaanggota";
	$rs = $conn->Execute($sqlstr);
	$t=0;
	echo "<b>.:Cron Data Tagihan:.</b><br/>";
	echo "<b>".str_repeat("-",100)."</b><br/>";
	while ($row = $rs->FetchRow()){
		$t++;
		$sqlstr = "select *
			from v_srttagih
			where idanggota='$row[idanggota]' and flag=1  
			order by tgltagihan desc limit 1";	
		$rows = $conn->GetRow($sqlstr);
		
		if($rows['tagihanke']<5){
			$now = date('Y-m-d');
		
			$record = array();
			$record['idanggota'] = $row['idanggota'];
			$record['tgltagihan'] = $now;
			$record['namaanggota'] = Helper::removeSpecial($row['namaanggota']);
			$record['t_user'] = 'cron';
			$record['t_updatetime'] = $now;
			$record['t_host'] = Helper::removeSpecial($_SERVER['REMOTE_ADDR']);	
			
			$oke=false;
			if (!empty($rows['tgltagihan'])){
				if($rows['tglexpired']<=$now){
					echo $t.". Tambah Data Tagihan ke-".($rows['tagihanke'] + 1)." untuk user ".$row['idanggota']."<br/>";
					//penentuan tanggal 2 minggu dari tglexpired
					$c_tgl = (int)strtotime($rows['tglexpired']);
					$record['tglexpired'] = date('Y-m-d',($c_tgl + (86400 * 14))); //2 minggu selanjutnya
					$record['tagihanke'] = $rows['tagihanke'] + 1;
					$col = $conn->Execute("select * from pp_tagihan where 1=-1");
					$sql = $conn->GetInsertSQL($col,$record,true);
					echo "SQL : ".$sql."<br/>";
					$err = $conn->Execute($sql);
					if(!$err){
						echo '<span style="color: red;">Data tagihan gagal ditambah.</span><br/>';
					}else
						echo '<span style="color: blue;">Data tagihan telah ditambah.</span><br/>';
					
					$oke=true;
				}
			}else{	
				echo $t.". Tambah Data Tagihan ke-1 untuk user ".$row['idanggota']."<br/>";
				$record['tagihanke'] = 1;
				$record['tglexpired'] =  date('Y-m-d',(int)strtotime($now) + (86400 * 14));
				$col = $conn->Execute("select * from pp_tagihan where 1=-1");
				$sql = $conn->GetInsertSQL($col,$record,true);
				echo "SQL : ".$sql."<br/>";
				$err = $conn->Execute($sql);
				if(!$err){
					echo '<span style="color: red;">Data tagihan gagal ditambah.</span><br/>';
				}else
					echo '<span style="color: blue;">Data tagihan telah ditambah.</span><br/>';
					
				$oke=true;
			}
			
			if($oke==true){
				$iddetail = $conn->GetOne("select max(idtagihan) from pp_tagihan");
				
				//seleksi untuk mengisi detail tagihan
				$sql = "select t.*,p.*,e.*,e.ideksemplar as ideks from pp_transaksi t
						left join pp_eksemplar e on e.ideksemplar=t.ideksemplar
						left join ms_pustaka p on p.idpustaka=e.idpustaka
						where idanggota='$row[idanggota]' and statustransaksi='1' and tglpengembalian is null
						and kdjenistransaksi='PJN'";
				$rss = $conn->Execute($sql);
				
				$recdetail = array();
				$td=0;
				while ($col = $rss->FetchRow()){
					$td++;
					echo "&nbsp;&nbsp;".$t.".".$td.". Tambah Data Detail Tagihan untuk user ".$row['idanggota']."<br/>";
					$recdetail['idtagihan'] = $iddetail;
					$recdetail['idpustaka'] = $col['idpustaka'];
					$recdetail['judul'] = Helper::removeSpecial($col['judul']);
					$recdetail['authorfirst1'] = Helper::removeSpecial($col['authorfirst1']);
					$recdetail['authorlast1'] = Helper::removeSpecial($col['authorlast1']);
					$recdetail['authorfirst2'] = Helper::removeSpecial($col['authorfirst2']);
					$recdetail['authorlast2'] = Helper::removeSpecial($col['authorlast2']);
					$recdetail['authorfirst3'] = Helper::removeSpecial($col['authorfirst3']);
					$recdetail['authorlast3'] = Helper::removeSpecial($col['authorlast3']);
					$recdetail['ideksemplar'] = $col['ideks'];
					$recdetail['noseri'] = $col['noseri'];
					$recdetail['nopanggil'] = Helper::removeSpecial($col['nopanggil']);
					$recdetail['idtransaksi'] = $col['idtransaksi'];
					$recdetail['kdjenistransaksi'] = $col['kdjenistransaksi'];
					$recdetail['tgltransaksi'] = $col['tgltransaksi'];
					$recdetail['tgltenggat'] = $col['tgltenggat'];
					
					$col = $conn->Execute("select * from pp_tagihandetail where 1=-1");
					$sql = $conn->GetInsertSQL($col,$recdetail,true);
					echo "&nbsp;&nbsp;SQL : ".$sql."<br/>";
					$err = $conn->Execute($sql);
					if(!$err){
						echo '&nbsp;&nbsp;<span style="color: red;">Data Detail tagihan gagal ditambah.</span><br/>';
					}else
						echo '&nbsp;&nbsp;<span style="color: blue;">Data Detail tagihan telah ditambah.</span><br/>';
					
				}
			}
		}
	}
	
	if($t==0)
		echo "<b>.:Tidak ada data tagihan yang ditambahkan/diubah:.</b>";
		
	echo "<br/><br/><br/>";
?>