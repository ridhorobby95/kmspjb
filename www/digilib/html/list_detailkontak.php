<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	//mendapatkan nama kontak
	$namakontak = $conn->GetOne("select namakontak from lv_kontak where idkontak=$r_key");
	
	$b_link = new Breadcrumb();
	$b_link->add($namakontak, Helper::navAddress('list_detailkontak.php').'&key='.$r_key, 1);
	
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['T_CONTACT'];
	$p_title = $form['T_CONTACT'];
	$p_dbtable = "pp_kontakonline";
	$p_id = "detkontak";
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		
		if ($r_act == 'perpanjang'){
			$r_eks = Helper::removeSpecial($_REQUEST['ideksemplar']);
			$r_trans = Helper::removeSpecial($_REQUEST['idtrans']);
			
			if (isset($_SESSION['skors']))
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Anda Terkena Skorsing', true).'</div>';	
			else			
				Perpus::Perpanjang($conn, $r_eks, $r_trans);
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan Gagal',true).'</div>';	
			else
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Perpanjangan Berhasil').'</div>';	
			
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
	
	
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	//mendapatkan topik kontak untuk parent
	$p_sqlstr = "select *
				from $p_dbtable where isaktif=1 and idkontak = $r_key and idparentol is null order by t_input desc";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<?= $b_link->output(); ?>
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $p_title.' '.$namakontak; ?></h4>
        	<?= (!$confirm) ?  '' : $confirm; ?>
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td><span name="tambah" class="buttonSubmit  btn btn-success" value="Tambah" onclick="goAdd('<?= $r_key; ?>')"><span class="glyphicon glyphicon-comment"></span> <?= $form['NEW_POST']; ?></td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
							<tr>
								<th>Judul Topik</th>
								<th>Replies</th>
								<th>View</th>
							</tr>
							<? 
								$i=0;
								while ($row = $rs->FetchRow()){ $i++;
									list($nama,$time) = explode('|',$row['last']);
							?>
							<tr>
								<td class="windowbg"><img src="images/tombol/folder_page.png" title="post" />&nbsp;<a title="Lihat" style="cursor:pointer" onClick="goShow('<?= $row['idkontakol']; ?>', '<?= $row['t_user']; ?>')"><?= $row['judul']?></a></td>
								<td><?= $row['rehit']; ?></td>
								<td><?= $row['hits'];?></td>
							</tr>
							<? } if ($i == 0) {?>
							<tr>
								<td colspan="4" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
							<tr height="20" class="windowbg2">
								<td align="left" colspan="3"><?= $form['PAGE']; ?>&nbsp;:&nbsp;
										<?	if($p_page == 0)
												echo '&nbsp;';
											else {
												$start = (($p_page > 9) ? ($p_page-9) : 1);
											$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
												$link = array();
												
												for($i=$start;$i<=$end;$i++) {
													if($i == $p_page)
														$link[] = '<strong>'.$i.'</strong>';
													else{
														if (!isset($_REQUEST['isadv']))
															$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
													}
												}
												echo implode(' ',$link);
											}
										?>
										&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	

</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>

</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	function goPage(npage) {
		sent = "&page=" + npage + "&numpage=" + $("#numpage").val() + "&key=<?= $r_key; ?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		sent = "&numpage=" + $("#numpage").val() + "&key=<?= $r_key; ?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goAdd(key){
		sent = "&key=" + key + "&rkey=<?= $r_key; ?>";;
		goLink('contents','<?= Helper::navAddress('data_post.php'); ?>', sent);
		
	}
	
	function goShow(key, user){
		sent = "&key=" + key + "&rkey=<?= $r_key; ?>&user=" + user;
		goLink('contents','<?= Helper::navAddress('view_post.php'); ?>', sent);
		
	}
</script>
</html>
