<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	if(empty($_SESSION[Config::G_SESSION]['iduser'])){
		echo '<script type="text/javascript">location.reload(true);</script>';
		exit();
	}
	
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add("Pemesanan", Helper::navAddress('data_cart.php'), 0);
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);
	
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['T_RESERVASI'];
	$p_title = $form['T_RESERVASI'];
	$p_title2 = $form['RES_LIST'];
	$p_dbtable = "perpus.pp_reservasi";
	if (!empty($_POST)){
		$r_act = Helper::removeSpecial($_POST['act']);
		
		if ($r_act == 'simpan'){
			
			$cart = $_SESSION['cart'];
			$data = explode(',',$cart);
			
			$record = array();
			$record['tglreservasi'] = date('Y-m-d');
			$record['idanggotapesan'] = $_SESSION['idanggota'];
			$logeksemplar = "";
			Helper::Identitas($record);

			$cartnow = array();
			
			if (!empty($_POST['items'])){
				foreach ($_POST['items'] as $value) {
					$record['ideksemplar'] = $value;
					$logeksemplar .= $value;
					
					if (strlen($logeksemplar) > 0)
						$logeksemplar .= ", "; 
					Perpus::saveCart($conn,$record,$p_dbtable);

					foreach ($data as $key => $id){
						
						if ($id == $record['ideksemplar'])
							continue;
							
						$cartnow[] = $id;
					}
				}
			}

			$resultcart = implode(',',$cartnow);
			$_SESSION['cart'] = $resultcart;

			if($conn->ErrorNo() != 0){
				Helper::addHistory($conn, "Pemesanan Gagal", "eksemplar ".$logeksemplar);
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message("Pemesanan Gagal",true).'</div>';
			}else{
				Helper::addHistory($conn, "Pemesanan Berhasil", "eksemplar ".$logeksemplar);
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message("Pemesanan Berhasil").'</div>';
			}
				
		}else if ($r_act == 'hapuscentang'){
			if (!empty($_POST['items'])){
				Perpus::Cart($r_act,$_POST['items']);
				
				Helper::addHistory($conn, "Pembatalan Pemesanan", "eksemplar ".Helper::removeSpecial(http_build_query($_POST['items'])));
				$confirm = '<div align="center style="padding-top:20px;" class="success">'.UI::message($form['P_DELSUCCESS']).'</div>';
			}else
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Centang Data yang mau dihapus',true).'</div>';
				
		}else if ($r_act == 'batalcentang'){ //untuk pembatalan pemesanan versi centang
			if (!empty($_POST['itemsb'])){
				foreach ($_POST['itemsb'] as $value) {
					$record['statusreservasi'] = 2;
					Perpus::batalPesan($conn,$record,'pp_reservasi',$value);
				}
				$confirmb = '<div align="center style="padding-top:20px;" class="success">'.UI::message($form['P_CANCELSUCCESS']).'</div>';
			}else
				$confirmb = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Centang Data yang mau dibatalkan',true).'</div>';
				
		}else if ($r_act == 'delete'){
			$rkey = Helper::removeSpecial($_POST['rkey']);
			
			Perpus::Cart($r_act, $rkey);
			
			Helper::addHistory($conn, "Pembatalan Pemesanan", "eksemplar ".$r_key);
			
			$confirm = '<div class="success">'.UI::message('Penghapusan Berhasil').'</div>';
		}else if ($r_act == 'batal'){  //untuk pembatalan versi one to one
			$rkey = Helper::removeSpecial($_POST['rkey']);
			$record = array();
			$record['statusreservasi'] = 2;
			Perpus::batalPesan($conn,$record,'pp_reservasi',$rkey);
			
			if($conn->ErrorNo() != 0)
				$confirmb = '<div align="center" style="padding-top:20px;" class="error">'.UI::message($form['P_CANCELFAIL'],true).'</div>';
			else
				$confirmb = '<div align="center" style="padding-top:20px;" class="success">'.UI::message($form['P_CANCELSUCCESS']).'</div>';
		}
	}
	
	//mengetahui variabel session ada atau tidak
	$cart = $_SESSION['cart'];
	$sql = "select e.*,e.noseri, m.judul, j.namajenispustaka, to_char(tglexpired,'YYYY-mm-dd') tgl_expired, statusreservasi from $p_dbtable p 
			left join perpus.pp_eksemplar e on e.ideksemplar=p.ideksemplarpesan
			left join perpus.ms_pustaka m on m.idpustaka=e.idpustaka
			left join perpus.lv_jenispustaka j on j.kdjenispustaka=m.kdjenispustaka 
			where to_char(tglexpired,'YYYY-mm-dd') >='".date('Y-m-d')."' and statusreservasi='1'
			and p.idanggotapesan='$_SESSION[idanggota]' order by tglreservasi desc";
			
	$rs = $conn->Execute($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<form name="perpusform" id="perpusform" method="post" action="<?= $i_phpfile; ?>">
	<?= $b_link->output(); ?>
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $p_title?></h4>
        	<?= (!$confirm) ?  '' : $confirm; ?>
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td>
						<table width="610px;" cellpadding="4" cellspacing="0" title="1" class="table table-bordered table-striped">
							<tr>
								<th width="2">&nbsp;</th>
								<th width="377"><?= $form['BIBLIO_TITLE']; ?></th>
								<th width="84" align="center"><?= $form['BIBLIO_TYPE']; ?></th>
								<th width="40" align="center"><?= $form['DELETE']; ?></th>
							</tr>
							<? 	if ($cart){	
								$items = explode(',',$cart);
								for ($i=0;$i<count($items);$i++){ 
									$row = $conn->GetRow("select * from pp_eksemplar e left join ms_pustaka p on e.idpustaka=p.idpustaka left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka where e.ideksemplar='$items[$i]'");
							?>		
							<tr>
								<td>
									<input type="checkbox" name="items[]" id="items[]" value="<?= $items[$i]; ?>" class="controlStyle" />
									<input type="hidden" name="ideksemplar[]" id="ideksemplar[]" value="<?= $items[$i]; ?>" />
								</td>
								<td><?= $row['judul']; ?></td>
								<td align="center"><?= $row['namajenispustaka']; ?></td>
								<td align="center"><img src="images/tombol/cart/delete.gif" style="cursor:pointer" onclick="delCart('<?= $items[$i]; ?>')" title="Batalkan Item di Cart" /></td>
							</tr>
							<? } ?>
							<tr>
								<td>&nbsp;</td>
								<td align="right" colspan="3">
									<span class="buttonSubmit btn btn-success" name="simpan" id="simpan" onclick="goSave()" style="cursor:pointer"><?= $form['B_RES']; ?></span>
									<span class="buttonSubmit btn btn-primary" name="hapuscentang" id="hapuscentang" onclick="hapusCentang()" style="cursor:pointer"><?= $form['B_CANCELCART']; ?></span>
								</td>
							</tr>
							<? }else{ ?>
							<tr>
								<td colspan="4" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</form>
	<form name="perpusform1" id="perpusform1" method="post" action="<?= $i_phpfile; ?>">
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $p_title2?></h4>
        	<?= (!$confirmb) ?  '' : $confirmb; ?>
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td>
						<table width="610px;" cellpadding="4" cellspacing="0" title="1" class="containerTable">
							<tr>
								<th width="20%"><?= $form['REG_COMP']; ?></th>
								<th><?= $form['BIBLIO_TITLE']; ?></th>
								<th><?= $form['BIBLIO_TYPE']; ?></th>
							</tr>
							<? $i=0;
								while ($row = $rs->FetchRow()){$i++;
							?>
							<tr>
								<td><?= $row['noseri']; ?></td>
								<td><?= $row['judul']; ?></td>
								<td><?= $row['namajenispustaka']; ?></td>
							</tr>
							<? }
								if ($i==0){?>
							<tr>
								<td colspan="5" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
						</table>
						*) <i>Untuk pembatalan pemesanan dapat menghubungi Petugas atau email ke pustaka@PJB.ac.id.</i>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</form>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	function goSave() {
		sent = $("#perpusform").serialize();
		sent += "&act=simpan";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function hapusCentang() {
		sent = $("#perpusform").serialize();
		sent += "&act=hapuscentang";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function batalCentang() {
		sent = $("#perpusform1").serialize();
		sent += "&act=batalcentang";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function delCart(rkey){
		sent += "&act=delete&rkey=" + rkey;
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function batalCart(rkey){
		sent += "&act=batal&rkey=" + rkey;
		goLink('contents','<?= $i_phpfile?>', sent);
	}
</script>
</html>
