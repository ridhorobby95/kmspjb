<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add("Berita", Helper::navAddress('list_berita.php'), 0);
	
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['NEWS'];
	$p_title = $menu['NEWS'];
	$p_dbtable = "pp_berita";
	$p_id = "idberita";
	
	
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
	
	
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	//riwayat peminjaman
	$p_sqlstr = "select *
		from $p_dbtable
		where (to_date(to_char(sysdate,'yyyy-mm-dd'),'yyyy-mm-dd') <= to_date(to_char(tglexpired,'yyyy-mm-dd'),'yyyy-mm-dd')
		or tglexpired is null) and judulberita is not null 
		order by tglberita desc";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
<?= $b_link->output(); ?>
			<h4 class="sidebar-title"><?= $p_title; ?></h4>
			<div class="col-md-12">
			<? 
				$i=0;
				while ($row = $rs->FetchRow()){
				$sqlCover = "select isifile from lp.lampiranperpus where jenis = 4 and idlampiran = ".$row['idberita'];
				$rsCover = $conn->GetOne($sqlCover);
				$cover = Perpus::loadBlobFile($rsCover, 'jpg');
			?>
			
			<div class="row" style="background:#bed8df; padding:10px; color:#fff; border-bottom:1px solid #bed8df; cursor:pointer">
				<div class="col-sm-3">
					<img id="imgfoto" style="max-width:100%;" src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" width="100" height="108">
				</div>
				<div class="col-sm-9" style="padding:0;">
				<h4 class="newsTitle" style="color:#FFFF00" onClick="linkURL('contents','progressbar','<?= Helper::navAddress('data_berita') ?>&key=<?= $row['idberita']?>')" title="Lihat"><?= $row['judulberita']?></h4>
				<small style="color:#959595;">Ditulis <?= $form['DATE']; ?> :&nbsp;<?= Helper::formatDateInd($row['tglberita'])?></small>
				<p><?= Helper::word_limiter(strip_tags($row['isiberita']),20)?></p>
				</div>
			</div>
			<? $i++;} if ($i == 0) {?>
				<?= $form['EMPTY_TABLE']; ?>
			<? } ?>
			</div> 
			<br/>
			<!-- paging -->
			<?= $form['PAGE']; ?>&nbsp;:&nbsp;
			<?	if($p_page == 0)
					echo '&nbsp;';
				else {
					$start = (($p_page > 9) ? ($p_page-9) : 1);
					$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
					$link = array();
					
					for($i=$start;$i<=$end;$i++) {
						if($i == $p_page)
							$link[] = '<strong>'.$i.'</strong>';
						else{
							if (!isset($_REQUEST['isadv']))
								$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
							}
						}
							echo implode(' ',$link);
						}
			?>
			Tampilkan&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?> berita
			
			
		</div>
	</div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
</script>
</html>
