<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once("classes/perpus.class.php");
	require_once('classes/breadcrumb.class.php');
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_add = $a_auth['canadd'];
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	$b_link = new Breadcrumb();
	$b_link->add($menu['POST_DATA'], Helper::navAddress('data_post.php'), 2);
	
	//setting utnuk halaman
	$p_window = "[Digilib] ".$form['T_ADDNEWPOST'];
	$p_title = $form['T_ADDNEWPOST'];
	
	$p_dbtable = 'pp_kontakonline';
	
	if (isset($_POST)){
		$r_act = Helper::removeSpecial($_POST['act']);
		
		if ($r_act == 'simpan'){
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['idkontak'] = $r_key;
			$record['t_input'] = date('Y-m-d H:i:s');
			$record['t_author'] = $_SESSION['usernama'];
			$record['jenisanggota'] = $_SESSION['NMjenis'];
			Helper::Identitas($record);		
			
			Perpus::simpan($conn,$record,$p_dbtable);
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message($form['P_SAVEFAIL'],true).'</div>';	
			else
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message($form['P_SAVESUCCESS']).'</div>';
				
			echo '<script type="text/javascript">';
			echo 'sent += "&key='.$r_key.'";';
			echo 'goLink("contents","'.Helper::navAddress('list_detailkontak.php').'", sent)';
			echo '</script>';	
		}
	}
	
	//mendapatkan nama kontak
	$namakontak = $conn->GetOne("select namakontak from lv_kontak where idkontak=$r_key");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>

	<?= $b_link->output(); ?>
	<form name="perpusForm" id="perpusForm" action="<?= $i_phpfile?>" method="post">
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
				<h4 class="sidebar-title"><?= $p_title; ?> di <?= $namakontak; ?></h4>
        		<?= (!$confirm) ?  '' : $confirm; ?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td colspan="2" valign="top" height="30"><div id="wait_div" style="padding-left:15px;font-weight:bold;"></div></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['TITLE']; ?> *</td>
						<td style="padding-top:5px;">&nbsp;<?= UI::createTextBox('judul',$row['judul'],'ControlStyle form-control',100,90); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['MESSAGE']?> *</td>
						<td style="padding-top:5px;">&nbsp;<?= UI::createTextArea('keterangankontak',$row['keterangankontak'],'ControlStyle form-control',20,65); ?></td>
					</tr>
					<tr height="20">
						<td style="padding-top:5px;">&nbsp;</td>
						<td valign="bottom"><strong><?= $form['VAL_CODE']?> *</strong></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td rowspan="2"><img src="includes/kcaptcha/cont.php?<?= mt_rand();?>" title="Ulangi validasi" id="captcha" style="cursor:pointer" onClick="this.src='includes/kcaptcha/cont.php?'+Math.random();"></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td><?= UI::createTextBox('validasi','','controlStyle form-control',50,50); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">&nbsp;</td>
						<td style="padding-top:5px;">&nbsp;&nbsp;<span name="simpan" class="buttonSubmit btn btn-primary" value="Simpan" onclick="goSave()" style="cursor:pointer;"><?= $form['SAVE']; ?></span>&nbsp;<span name="reset" class="buttonSubmit btn btn-success" value="Reset" style="cursor:pointer"><?= $form['RESET']; ?></span></td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
			</div>
		</div>
	</form>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/pajhash.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">		
	function goSave()
	{
		if(cfHighlight("judul,keterangankontak,validasi")){
			file = "<?= Helper::navAddress('functionx.php')?>";
			sent = $("#perpusForm").serialize();
			sent += "&act=simpan&key=<?= $r_key; ?>";
			v = $("#validasi").val();
			$.post(file,'f=contcaptcha&p1='+v,function(retlog) {
				if(retlog == 1) { 
					goLink('contents','<?= $i_phpfile?>', sent);
				}else{
					$("#wait_div").html('<font color="red">Kode Validasi yang dimasukkan Salah</font>');
					document.getElementById("captcha").src = 'includes/kcaptcha/cont.php?'+Math.random();
				}
			});
		}
	}
</script>
</html>
