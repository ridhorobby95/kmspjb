<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$r_lokasi = $_SESSION['UNIT_PERPUS'];
	if($r_lokasi){
		$f_sql2 = "and p.idpustaka in (select e.idpustaka from pp_eksemplar e where e.kdlokasi = '$r_lokasi')";
	}
	
	$sql2 = "select j.kdjenispustaka,j.namajenispustaka,
			(
				select count(*)
				from ms_pustaka p 
				where p.kdjenispustaka=j.kdjenispustaka
				$f_sql2
			) as jumlah 
		from lv_jenispustaka j
		order by namajenispustaka";
	$rsjenis = $conn->Execute($sql2);
	
	/*$rspt = $conn->GetArray("select count(*) as jml, p.kdjenispustaka, b.kdjurusan, p.tahunterbit, j.namajurusan  
				from ms_pustaka p
				left join pp_bidangjur b on p.idpustaka = b.idpustaka 
				left join lv_jurusan j on j.kdjurusan = b.kdjurusan 
				group by p.kdjenispustaka, b.kdjurusan, p.tahunterbit, j.namajurusan 
				order by p.kdjenispustaka, b.kdjurusan, p.tahunterbit, j.namajurusan  ");

	$folder = array();
	foreach($rspt as $r){
		# folder pustaka
		# folder[kdjenispustaka][[kdpus],[jumlah]]
		$fpus = $r['kdjenispustaka'];
		if($pusfol == $fpus){
			$folder[$fpus]['jumlah'] = $folder[$fpus]['jumlah']+intval($r['jml']?$r['jml']:0);
		}else{
			$pusfol = $fpus;
			$folder[$fpus]['kdpus'] = $r['kdjenispustaka'];
			$folder[$fpus]['jumlah'] = intval($r['jml']?$r['jml']:0);
		}
		
		# folder jurusan
		# folder[kdjenispustaka][detjur][kdjurusan][[jurusan],[jumlah]]
		$fjur = ($r['kdjurusan']?$r['kdjurusan']:"-");		
		if($jurfol == $fjur){
			$folder[$fpus]['detjur'][$fjur]['jumlah'] = $folder[$fpus]['detjur'][$fjur]['jumlah']+intval($r['jml']?$r['jml']:0);
		}else{
			$jurfol = $fjur;
			$folder[$fpus]['detjur'][$fjur]['kdjurusan'] = ($r['kdjurusan']?$r['kdjurusan']:"-");
			$folder[$fpus]['detjur'][$fjur]['jurusan'] = ($r['namajurusan']?$r['namajurusan']:"-");
			$folder[$fpus]['detjur'][$fjur]['jumlah'] = intval($r['jml']?$r['jml']:0);
		}
		
		# folder tahun
		# folder[kdjenispustaka][kdjurusan][detthn][tahunterbit][[tahun],[jumlah]]
		$fthn = ($r['tahunterbit']?$r['tahunterbit']:"-");
		if($thnfol == $fthn){
			$folder[$fpus][$fjur]['detthn'][$fthn]['jumlah'] = $folder[$fpus][$fjur]['detthn'][$fthn]['jumlah']+intval($r['jml']?$r['jml']:0);
		}else{
			$thnfol = $fthn;
			$folder[$fpus][$fjur]['detthn'][$fthn]['tahun'] = ($r['tahunterbit']?$r['tahunterbit']:"-");
			$folder[$fpus][$fjur]['detthn'][$fthn]['jumlah'] = intval($r['jml']?$r['jml']:0);
		}
	}*/
	
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('lokasi',$r_lokasi,true,false,0,'class="ControlStyle  form-control" style="width:200" onChange="goPerpus(this.value)"');
	$l_lokasi = str_replace('<option></option>','<option value="">-- Pilih Unit --</option>',$l_lokasi);	
?>
<script type="text/javascript" src="script/auth.js"></script>
<script type="text/javascript" src="script/lockfixed/jquery.lockfixed.js"></script>
<div id="sidebar">
<? if (isset($_SESSION['userid'])) {?>
<h4 class="sidebar-title">
  <?= $menu['INFORMATION']; ?>
</h4>
<div class="sidebar-content">
	<div class="alert alert-danger" style="font-size:13px;">
		<span class="glyphicon glyphicon-book"></span>&nbsp;
		<?= Perpus::getCart(); ?>
	</div>
  <?
	//pengecekkan user terkena skorsing atau tidak
	$sql = "select tglselesaiskors from ms_anggota where idanggota='$_SESSION[idanggota]'";
	$isskors = $conn->GetOne($sql);
	
	if ($isskors>=date('Y-m-d')){ $_SESSION['skors'] = $isskors;
?>
  <div id="alert" class="alert alert-danger">
    <?= $sidebar['SKORSING']; ?>
  </div>
  <div id="alert" class="alert alert-danger">
    <?= $sidebar['TGL_SKOR']; ?>
    &nbsp;
    <?= Helper::formatDateInd($isskors); ?>
  </div>
  <? } ?>
</div>
<h4 class="sidebar-title">
  <?= $sidebar['INFO_USER']; ?>
</h4>
<div class="sidebar-content">
  <table cellpadding="3" cellspacing="0">
    <tr>
      <td width="95"><?= $sidebar['MEMBERNAME']; ?></td>
      <td width="10">:</td>
      <td><?= $_SESSION['usernama']; ?></td>
    </tr>
    <tr>
      <td width="95">Unit</td>
      <td width="10">:</td>
      <td><?= $_SESSION['satker']; ?></td>
    </tr>
    <tr>
      <td><?= $sidebar['MEMBERTYPE']; ?></td>
      <td>:</td>
      <td><?= $_SESSION['NMjenis']; ?></td>
    </tr>
    <tr>
      <td><?= $sidebar['LOGINTIME']; ?></td>
      <td>:</td>
      <td><?= Helper::tglEngStamp($_SESSION['logintime']); ?></td>
    </tr>
    <tr>
      <td><?= $sidebar['LASTLOGIN']; ?></td>
      <td>:</td>
      <td><?= Helper::tglEngStamp($_SESSION['lastlogintime']); ?></td>
    </tr>
    <tr height="60">
    	<td colspan="3"><a style="cursor:pointer" onclick="linkURL('contents','progressbar','<?= Helper::navAddress('data_profile.php');?>')">
          <div class="buttonSubmit btn btn-xs btn-primary"><?= $menu['PROFILE']?></div>
          </a><a href="<?= Helper::navAddress('sys_logout.php'); ?>" style="cursor:pointer">
          <div class="buttonSubmit btn btn-xs btn-success"><?= $menu['EXIT']?></div>
          </a></td>
    </tr>
  </table>
</div>
<?/* 
	$sql = "select count(*) from pp_tagihan where idanggota='".$_SESSION['idanggota']."' and tglexpired >= current_date";
	$jumTagihan = $conn->GetOne($sql);
*/?>
<h4 class="sidebar-title"><?= $sidebar['INBOX']; ?></h4>
<!--div class="sidebar-content">
	<a onclick="goTagihan('<?= Helper::cNumZero($jumTagihan) ?>')">
		<?= $sidebar['BILL']; ?>
		<span id="notiftagihan" class="badge<?=($jumTagihan>0?" blink":"");?>"><?= Helper::cNumZero($jumTagihan) ?></span>
	</a>
<br/><br/-->
<? 
	$sqlt = "select count(*) 
		from pp_transaksi 
		where idanggota ='".$_SESSION['idanggota']."' 
		and to_char(tgltenggat,'dd-mm-YYYY') < to_char(current_date,'dd-mm-YYYY')
		and tglpengembalian is null";
	$jumTelat = $conn->GetOne($sqlt);
?>
	<a onclick="goTelat('<?= Helper::cNumZero($jumTelat) ?>')">
		Keterlambatan
		<span id="notiftelat" class="badge<?=($jumTelat>0?" blink":"");?>"><?=Helper::cNumZero($jumTelat);?></span>
	</a>
</div>

<? } ?>
<div class="directory">
<h4 class="sidebar-title">
  <?= $form['DIRECTORY']?>
</h4>
<div class="sidebar-content">
  <?/*new folder*/?>
  <div class="items" style="display:block !important;">
    <? $i=0;while ($rowjenis = $rsjenis->FetchRow()){$i++;?>
    <p>
	<? if($rowjenis['kdjenispustaka'] == "KT" or $rowjenis['kdjenispustaka'] == "SR"){?>
	<a href="javascript:openFold('jur','<?=$rowjenis['kdjenispustaka'];?>')" style="cursor:pointer;">
	<img src="images/tombol/folder.png" title="Direktori" id="jps<?=$rowjenis['kdjenispustaka'];?>" />
	</a>
	
	<a href="javascript:closeFold('jur','<?=$rowjenis['kdjenispustaka'];?>')" style="cursor:pointer;">
	<img src="images/tombol/folder_page.png" title="Direktori" id="jph<?=$rowjenis['kdjenispustaka'];?>" style="display: none;" />
	</a>
	<?}else{?>
	<img src="images/tombol/folder.png" title="Direktori" id="jps<?=$rowjenis['kdjenispustaka'];?>" />
	<?}?>
	&nbsp;<a href="javascript:goDirect('J','<?= $rowjenis['kdjenispustaka']; ?>')" style="cursor:pointer;">
      <?= ucwords(strtolower($rowjenis['namajenispustaka'])); ?>
      &nbsp;(
      <?= $rowjenis['jumlah'];?>
      )</a></p>
	
    <? } ?>
  </div>
  <br/>
<div class="col-md-12"><?=$l_lokasi;?></div>

  </div>
</div>


</div>
<br/><br/><br/><br/>
<script type="text/javascript">
	function goPerpus(lokasi){
		var sent = "&lokasi=" + lokasi + "&act=lokasiperpus";
		goLink('contents','<?= Helper::navAddress('list_home.php');?>',sent);
	}


<? if (isset($_SESSION['userid'])) { ?>	
	$(document).ready(function() {
		var refreshId = setInterval(function() {getInfoTelat()}, 50000);
	});
	function getInfoTelat(){
		file = "<?= Helper::navAddress('getNotif.php'); ?>";

		$.ajax({
		    type: "post",
		    url: file+"&act=getTagihan", 
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notiftagihan').innerHTML = data;
		    }
		});
		
		$.ajax({
		    type: "post",
		    url: file+"&act=getTelat", 
		    cache: false,
		    success: function(data){ 
				  document.getElementById('notiftelat').innerHTML = data;
		    }
		});
	}
	
<? } ?>
(function($) {
	
	$.lockfixed("#sidebar .directory",{offset: {top: 140, bottom: 1000}});
})(jQuery);

</script>
