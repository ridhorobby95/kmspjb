<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_tipe = Helper::removeSpecial($_REQUEST['tipe']);
	
	$r_jur = Helper::removeSpecial($_REQUEST['jur']);
	$r_thn = Helper::removeSpecial($_REQUEST['thn']);
	
	$connLamp = Factory::getConnLamp();
	$connLamp->debug = false;
	$cek = $connLamp->IsConnected();
	
	$b_link = new Breadcrumb();
	$b_link->add($form['DIRECTORY'], Helper::navAddress('list_kategori.php').'&key='.$r_key.'&tipe='.$r_tipe.'&jur='.$r_jur.'&thn='.$r_thn, 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] Direktori";
	$p_title = $menu['CATDIR'];//"Kategori Direktori";  
	$p_dbtable = "ms_pustaka";
	$p_row = 10;
	

	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
		
		
	$r_lokasi = $_SESSION['UNIT_PERPUS'];
	if($r_lokasi){
		$fp_sqlstr = "and p.idpustaka in (select e.idpustaka from pp_eksemplar e where e.kdlokasi = '$r_lokasi')";
	}
		
		
	$sql = "select * from ( ";
	
	if ($r_tipe == 'T')
		$p_sqlstr = "Select p.*, j.namajenispustaka, tp.idtopik, to_char(p.judul) jdul from $p_dbtable p
					left join pp_topikpustaka tp on tp.idpustaka=p.idpustaka
					left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
					where tp.idtopik='$r_key' $fp_sqlstr";
	else if ($r_tipe == 'JU')
		$p_sqlstr = "Select p.*, j.namajenispustaka, pj.kdjurusan, to_char(p.judul) jdul  from $p_dbtable p
					left join pp_bidangjur pj on pj.idpustaka=p.idpustaka
					left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
					where pj.kdjurusan='$r_key' $fp_sqlstr";
	else if ($r_tipe == 'J' and empty($r_jur) and empty($r_thn))
		$p_sqlstr = "Select p.*, j.namajenispustaka, to_char(p.judul) jdul  from $p_dbtable p
					left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
		 			where p.kdjenispustaka='$r_key' $fp_sqlstr"; // and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
	
	else if ($r_tipe == 'J' and (!empty($r_jur) or !empty($r_thn))){
		$p_sqlstr = "Select p.*, j.namajenispustaka, r.namajurusan, b.kdjurusan, to_char(p.judul) jdul  from $p_dbtable p
					left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
					left join pp_bidangjur b on p.idpustaka = b.idpustaka 
					left join lv_jurusan r on r.kdjurusan = b.kdjurusan 
		 			where p.kdjenispustaka='$r_key' $fp_sqlstr"; // and exists (select idpustaka from pp_eksemplar e where e.idpustaka=p.idpustaka and kdkondisi='V' and (e.kdlokasi in('TG','NG','HM','S2','OL') or e.kdlokasi like 'X%' or e.kdlokasi like 'Y%')) ";
		if($r_jur){
			if($r_jur == "-")
				$p_sqlstr .=" and (r.kdjurusan is null or r.kdjurusan = '') ";
			elseif($r_jur != "-")
				$p_sqlstr .=" and r.kdjurusan = '$r_jur' ";
		}
		
		if($r_thn){
			if($r_thn == "-")
				$p_sqlstr .=" and (p.tahunterbit is null or p.tahunterbit = '') ";
			elseif($r_thn != "-")
				$p_sqlstr .=" and p.tahunterbit = '$r_thn' ";
		}
	}
	$p_sqlstr .=" order by jdul ";
	
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	$peng = $conn->GetArray("select p.namadepan || ' ' || p.namabelakang as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor   ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>

<?= $b_link->output(); ?>
    <h4 class="sidebar-title"><?= $p_title; ?></h4>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr height="20" align="right">
			<td>Halaman <?= $p_page ?> dari sekitar <strong><?= $p_recount ?></strong> hasil</td>
		</tr>
	<?  $i = 0;
		while ($row = $rs->FetchRow()){ $i++;?>
		<tr>
			<td>
				<table width="100%" class="result" border="0" cellpadding="4" cellspacing="0" onMouseOver="this.className='resultH'" onMouseOut="this.className='result'" style="border-top:1pt dotted #d2d2d2;">
					<tr>
						<?
							$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ".$row['idpustaka'];
							$rsCover = $connLamp->GetOne($sqlCover);
							$cover = Perpus::loadBlobFile($rsCover, 'jpg'); /*aktifkan lagi nanti*/
						?>
						<td valign="top" width="75"><img id="imgfoto" src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" height="110"></td>
						<!--td valign="top" width="75"><img id="imgfoto" src="<?//= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" height="110"></td-->
						<td>
							<table>
								<tr>
									<td class="resultTitle" colspan="2"> <a class="judul-buku" style="cursor:pointer;" title="Lihat" onClick="goLihat('<?= $row['idpustaka']?>')"><?= $row['judul']; ?></a>
									</td>
								</tr>
								<? if($row['islokal']==1) { ?>
								<tr>
									<td class="resultDesc"><strong>REG. COMP</strong></td><td>: <?= $row['noseri'] ?></td>
								</tr>
								<? } else { ?>
								<tr>
									<td class="resultDesc"><strong><?= $form['NO_CALL']?></strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= $row['nopanggil'] ?></td>
								</tr>
								<? } ?>
								<tr>
									<td class="resultSubB" width="120" valign="top"><strong><?= $form['AUTHOR']?></strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">:
									<?php 
										$auth = array();
										if(!empty($pengarang[$row['idpustaka']])){
											foreach($pengarang[$row['idpustaka']] as $a){
												$auth[] = $a;
											}
											echo implode(", ",$auth);
										}else
											echo $row['authorfirst1'].' '.$row['authorlast1'];
									?>
									<?//= $row['authorfirst1'].' '.$row['authorlast1'];?></td>
								</tr>
								<? if($row['islokal']==1) { ?>
								<tr>
									<td class="resultSubB" width="120" valign="top"><strong>Contributor</strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= $row['pembimbing'] ?></td>
								</tr>
								<? } ?>
								<tr>
									<td class="resultSubB" width="120" valign="top"><strong><?= $form['DIRECTORY']?></strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= $row['namajenispustaka'];?>]</td>
								</tr>
								<tr>
									<td class="resultSubB" width="120" valign="top"><strong><?= $form['YEAR_PUBLISH']?></strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= $row['tahunterbit'];?>]</td>
								</tr>
								<?php if($row['linkpustaka']){?>
								<tr>
									<td class="resultSubB" width="120" valign="top"><strong>LINK</strong></td>
									<td class="resultSubB" align="left" valign="top" style="color:#666666">: 
									<a href="<?=$row['linkpustaka'];?>" target="_blank">KLIK DI SINI</a>
									</td>
								</tr>
								<?php } ?>
							</table>
						</td>
					</tr>
					
				</table>
				<br />
			</td>
		</tr>
	<? }if ($i == 0) {?>
		<tr>
			<td align="center"><?= $form['EMPTY_TABLE']?>
			</td>
		</tr>
	<? } ?>
		<tr>
			<td align="center"><?= $form['PAGE']?>&nbsp;:&nbsp;
					<?	if($p_page == 0)
							echo '&nbsp;';
						else {
							$start = (($p_page > 9) ? ($p_page-9) : 1);
						$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
							$link = array();
							
							for($i=$start;$i<=$end;$i++) {
								if($i == $p_page)
									$link[] = '<strong>'.$i.'|</strong>';
								else{
									if (!isset($_REQUEST['isadv']))
										$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a>|</strong>';
									else
										$link[] = '<strong><a href="javascript:goPageADV('.$i.')">'.$i.'</a>|</strong>';
								}
							}
							echo implode(' ',$link);
						}
					?>
			</td>
		</tr>
	</table>
	
</div></div>
  </div>
  <div class="col-md-3" style="border-left:1px solid #ddd;">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>

</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	self = "<?= Helper::navAddress('list_kategori.php'); ?>";
	function goPage(npage) {
		var sent = "&tipe=<?= $r_tipe; ?>&key=<?= $r_key?>&jur=<?=$r_jur?>&thn=<?=$r_thn?>&page=" + npage;
		$("#contents").divpost({page: self, sent: sent});
	}
</script>
</html>
