<?php
	// bagian include inisialisasi
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//$conns->debug = false;
	//$conn->debug = true;
	// menu dengan struktur xml
	$menu = 
	"<response>
		<item>
			<category>Satuan Kerja</category>";
	
	// jenis pustaka
	$rs = $conns->Execute("select j.kdjenispustaka,j.namajenispustaka,(select count(*) from ms_pustaka p 
										where p.kdjenispustaka=j.kdjenispustaka) as jumlah 
				from lv_jenispustaka j
				order by namajenispustaka"); 

	
	function loopfinal(&$final,&$parent,$idx) {
		if(is_array($parent[$idx]) and !empty($parent[$idx])) {
			foreach($parent[$idx] as $data) {
				$final[] = $data;
				loopfinal($final,$parent,$data['info_lft']);
			}
		}
	}
	
	// membuat xml untuk data hirarkikal urut
	$rs = $conns->Execute("select kodeunit idsatker, namaunit namasatker, infoleft info_lft, inforight info_rgt, level, parentunit parent 
			      from ms_unit
			      where infoleft is not null
			      order by infoleft"); #dari SDM
	
	// membuat struktur tree
	$left = array('0' => '0');
	$nama = array();
	$right = array();
	$parent = array('0');
	
	while($row = $rs->FetchRow()) {
		while (count($right) > 0 and $right[count($right)-1] < $row['info_rgt']) {
			array_pop($right);
			array_pop($parent);
		}
		
		$right[] = $row['info_rgt'];
		
		$row['level'] = count($right);
		$row['parent'] = $parent[count($parent)-1];
		
		$left[$row['idsatker']] = $row['info_lft'];
		$nama[$row['namasatker']][] = $row;
		
		$parent[] = $row['idsatker'];
	}
	
	ksort($nama);
	
	$parent = array();
	foreach($nama as $adata)
		foreach($adata as $data)
			$parent[$left[$data['parent']]][] = $data;
	
	unset($nama);
	ksort($parent);
	
	$final = array(); // array unit final

	loopfinal($final,$parent,0);

	foreach($final as $data) {
		$menu .=
			"<linx>
				<label>".$data['idsatker']." - ".htmlspecialchars($data['namasatker'])."</label>
				<value>".$data['idsatker']."</value>
				<level>".$data['level']."</level>
				<parent>".(($data['info_rgt']-$data['info_lft'] == 1) ? '0' : '1')."</parent>
			</linx>";
	}

	$menu .=
		"</item>
	</response>";

	header('Content-type: text/xml');
	echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	echo $menu;
?>