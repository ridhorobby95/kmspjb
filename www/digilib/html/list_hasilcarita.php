<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_alf = Helper::removeSpecial($_REQUEST['alf']);
	$p_page = Helper::removeSpecial($_REQUEST['paging']);
	$r_adv = Helper::removeSpecial($_REQUEST['isadv']);
	$r_topik = Helper::removeSpecial($_REQUEST['topik']);
	
	$b_link = new Breadcrumb();
	if($r_adv=='')
		//$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcarita.php').'&paging='.$p_page.'&key='.$r_key.'&topik='.$r_topik.'&alf='.$r_alf, 0);
		$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcarita.php').'&paging='.$p_page.'&key='.$r_key.'&alf='.$r_alf, 0);
	else {
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_key1 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword1']));
		$r_key2 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword2']));
		$r_bahasa = Helper::removeSpecial($_REQUEST['kdbahasa']);
		$r_topik1 = Helper::removeSpecial($_REQUEST['topik1']);
		$r_topik2 = Helper::removeSpecial($_REQUEST['topik2']);
		$r_tahun1 = Helper::removeSpecial($_REQUEST['tahun1']);
		$r_tahun2 = Helper::removeSpecial($_REQUEST['tahun2']);
		$r_bool = Helper::removeSpecial($_REQUEST['bool']);
		$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcarita.php').'&paging='.$p_page.'&numpage='.$r_page.'&keyword1='.$r_key1.'&keyword2='.$r_key2.'&kdbahasa='.$r_bahasa.'&topik1='.$r_topik1.'&topik2='.$r_topik2.'&tahun1='.$r_tahun1.'&tahun2='.$r_tahun2.'&bool='.$r_bool.'&isadv=1',0);
	}
	//konfigurasi halaman
	$p_title = $form['RESULT_SEARCH'];
	
	//untuk mendapatkan bahasa
	$rscb = $conn->Execute("select namabahasa, kdbahasa from lv_bahasa order by namabahasa");
	$listBahasa = $rscb->GetMenu2('kdbahasa',$r_bahasa,true,false,0,'id="kdbahasa" class="controlStyle  form-control2"');
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/style.css" rel="stylesheet" type="text/css" />
<title><?= $p_title; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
	<?= $b_link->output(); ?>
	<div class="searchBox col-md-12">
	<div class="simpleSearch" id="Simple">
		<div class="contentTop"></div>
		<div class="contentContainer">
        <h3 class="search-title"><?= $menu['SEARCH']." ".$menu['TA'] ?></h3>
			<div class="simpleSearch">
            <div class="form-group row">
      			<label class="col-sm-3"><?= $form['KEYWORD']; ?></label>
				<div class="col-sm-9"><?= UI::createTextBox('txtcariln',$r_key,'controlStyle form-control',100,100,true,'onKeyPress="etrCari(event)"') ?></div>
             </div>
	    <?/*?>
             <div class="form-group row">
				<label class="col-sm-3"><?= $form['CLASSIFICATION']; ?></label>
				<div class="col-sm-9"><?= UI::createSelect('topik',array(''=>'', 'JP'=>$form['BIBLIO_TITLE'], 'P'=>$form['AUTHOR'], 'K'=>$form['KEYWORD'], 'T'=>$form['TOPIC'], 'C'=>'Contributor'),$r_topik,'controlStyle  form-control',true); ?></div>
             </div>
             <?*/?>
             <div class="form-group row">
      <div class="col-md-12" style="text-align:right;">
	<?/*?><a href="javascript:goAdvance()"><?= $form['ADV_SEARCH']; ?></a><?*/?>
	<a href="javascript:goCariLn()" style="cursor:pointer"><span class="buttonSubmit btn btn-sm btn-primary">
        <?= $form['B_SEARCH']; ?>
        </a>  </div>
    </div>
    <div class="row">
      <label class="col-sm-3">
        <?= $form['s_title'] ?>
      </label>
      <div class="col-sm-9">
        <?= Helper::CariAlf('goCariJudul') ?>
      </div>
    </div>
    <div class="row">
      <label class="col-sm-3">
        <?= $form['s_author'] ?>
      </label>
      <div class="col-sm-9">
        <?= Helper::CariAlf('goCariAuthor') ?>
      </div>
    </div>
  </div>
				
		</div>
	</div>
	<?/*?>
	<div class="searchBoxCom" id="Advance">
		<div class="contentTop"></div>
		<div class="contentContainerCom">
			<h3 class="search-title"><?= $menu['SEARCH']." ".$menu['TA'] ?></h3>
				
				<form name="advform" id="advform" method="post" action="<?= $i_phpfile ?>">
                <div class="form-group row">
			<label class="col-sm-3"><?= $form['KEYWORD']?></label>
			<div class="col-md-9">
				<?= UI::createTextBox('keyword1',$r_key1,'controlStyle form-control2',80,80,true,'onKeyPress="etrCari2(event)" class="controlStyle form-control2"') ?>
				<?= UI::createSelect('topik1',array('JP'=>$form['BIBLIO_TITLE'], 'P'=>$form['AUTHOR'], 'K'=>$form['KEYWORD'], 'J'=>$form['BIBLIO_TYPE'], 'T'=>$form['TOPIC']),$r_topik1,'controlStyle form-control2',true,'onChange="showJenis1()"'); ?>
				<?= $listJenis1; ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3">&nbsp;</label>
			<div class="col-md-9">
				<?= UI::createSelect('bool',array('A'=>'AND','O'=>'OR'),$r_bool,'controlStyle form-control2',true); ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['KEYWORD']?></label>
			<div class="col-md-9">
				<?= UI::createTextBox('keyword2',$r_key2,'controlStyle form-control2',80,100,true,'onKeyPress="etrCari2(event)"') ?>
				<?= UI::createSelect('topik2',array(''=>'', 'JP'=>$form['BIBLIO_TITLE'], 'P'=>$form['AUTHOR'], 'K'=>$form['KEYWORD'], 'T'=>$form['TOPIC'], 'C'=>'Contributor'),$r_topik2,'controlStyle form-control2',true); ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['LANGUAGE']?></label>
			<div class="col-md-9 ">
				<?= $listBahasa; ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['YEAR']; ?></label>
			<div class="col-md-9">
				<?= UI::createTextBox('tahun1',$r_tahun1,'controlStyle form-control2',4,4,true) ?>
				&nbsp;-&nbsp;
				<?= UI::createTextBox('tahun2',$r_tahun2,'controlStyle form-control2',4,4,true) ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['PAGE']; ?></label>
			<div class="col-md-9">
				<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle form-control2',true); ?>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-12" style="text-align:right;">
				<a href="javascript:goSimple()"><?= $form['SIMPLE_SEARCH']; ?></a>
				<a href="javascript:goCariADV()" style="cursor:pointer"><span class="buttonSubmit btn btn-sm btn-primary"><?= $form['B_SEARCH']; ?></span></a>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['s_title']; ?></label>
			<div class="col-md-9">
				<?= Helper::CariAlf('goCariJudul') ?>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3"><?= $form['s_author']; ?></label>
			<div class="col-md-9">
				<?= Helper::CariAlf('goCariAuthor') ?>
			</div>
		</div>
		<input type="hidden" name="isadv" id="isadv" value="1" />
		</form>
		</div>
	</div>
	<?*/?>
</div>	
<div style="float:left;width:100%;margin-top:15px;">
		<div id="resultSearch" style="border-top:1pt dashed #a1a0a0;"></div>
	</div>

<!--</body>-->
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	//halaman untuk ajaxin
	listhasil = "<?= Helper::navAddress('xlist_hasilcarita.php'); ?>";
	
	$(function() {
		<? if($r_adv==1) { ?>
		var keysearch = "keyword1=<?= $r_key1 ?>&keyword2=<?= $r_key2 ?>&kdbahasa=<?= $r_bahasa ?>&topik1=<?= $r_topik1 ?>&topik2=<?= $r_topik2 ?>&tahun1=<?= $r_tahun1 ?>&tahun2=<?= $r_tahun2 ?>&bool=<?= $r_bool ?>&isadv=1&paging=<?= $p_page ?>";
		goAdvance();
		<? } else { ?>
		//var keysearch = "key=<?= $r_key ?>&alf=<?= $r_alf ?>&paging=<?= $p_page ?>&topik=<?= $r_topik ?>";
		var keysearch = "key=<?= $r_key ?>&alf=<?= $r_alf ?>&paging=<?= $p_page ?>";
		goSimple()
		<? } ?>
		$("div.tabs a[class='tablink']").click(function() {
				var index = $("div.tabs a").index(this);
				
				$("div.tabs li").removeAttr("class");
				$(this).parent("li").attr("class","selected");
				
				$("div[class='items']").hide();
				$("div[class='items']").eq(index).show();
			});
		
			//chooseTab(0);
			var which = $(location).attr('hash').substring(1,100);
		
				chooseTab(0);
		<? if ($r_key != '' or $r_key1!='') {?>
		goLink('resultSearch', listhasil, keysearch);
		<? } ?>
		//$("#Advance").hide();		
	});
	
	function etrCari(e) {
		var ev = (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			goCariLn();
	}
	
	function etrCari2(e) {
		var ev = (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			goCariADV();
	}
	function goCariJudul(x){
			var keysearch = "key=" + x +"&alf=1";
			goLink('resultSearch', listhasil, keysearch);
	}
	
	function goCariAuthor(x){
			var keysearch = "key=" + x +"&alf=2";
			goLink('resultSearch', listhasil, keysearch);
	}
	
	function goCariLn()
	{
		var str = $.trim(document.getElementById("txtcariln").value);
		
		if(parseInt(str.length) > 0){
			//var keysearch = "key=" + document.getElementById("txtcariln").value + "&topik=" + document.getElementById("topik").value;
			var keysearch = "key=" + document.getElementById("txtcariln").value; 
			goLink('resultSearch', listhasil, keysearch);
		}else{
			alert("<?= $sidebar['ALERT_SEARCH']; ?>");
		}
	}
	
	function chooseTab(idx) {
		$("div.tabs a").eq(idx).triggerHandler("click");
	}
	
	function goCariADV()
	{
		var str = $.trim(document.getElementById("keyword1").value);
		if(parseInt(str.length) > 0){
		var keysearch =  $("#advform").serialize();
		//$("#resultSearch").divpost({page: listhasil, sent: keysearch});
		goLink('resultSearch', listhasil, keysearch);
		}else{
			alert("<?= $sidebar['ALERT_SEARCH']; ?>");
		}
	}
	
	function goAdvance()
	{
		var str = $.trim(document.getElementById("keyword1").value);
		document.getElementById("keyword1").value = str;
		$("#Simple").hide();
		$("#Advance").show();
	}
	
	function goSimple()
	{
		var str = $.trim(document.getElementById("txtcariln").value);
		document.getElementById("txtcariln").value = str;
		$("#Simple").show();
		$("#Advance").hide();
	}
	
	function goDirect(tipe,key){
		var sent = "&tipe=" + tipe + "&key=" + key;
		goLink('contents','<?= Helper::navAddress('list_kategori.php');?>',sent);
	}
</script>
</html>
