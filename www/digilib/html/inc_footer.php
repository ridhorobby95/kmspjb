<?
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
?>
<script type="text/javascript" src="script/auth.js"></script>
<div class="footer">
	<div class="container">
    	<div class="col-md-4">
        	<h5 class="footer-title">Kontak Kami</h5>
            <div class="footer-content">
							<b>PT PJB</b><br/>
				Jl. Ketintang Baru No. 11,<br/>
				Surabaya, Jawa Timur<br/>
				Indonesia (60231)<br/>
            </div>
        </div>
        <div class="col-md-4">
        	<h5 class="footer-title">Pengunjung</h5>
            <div class="footer-content">
            	<div id="reload"></div>
            </div>
        </div>
        <div class="col-md-4">
        	<h5 class="footer-title">Link Terkait</h5>
            <div class="footer-content">
            	<a href="http://www.facebook.com/ptpjb/?ref=br_rs" target="_BLANK"><img src="images/fb.png"> PT PJB &nbsp; &nbsp;</a>
            	
            	<a href="http://twitter.com/ptpjb?lang=en" target="_BLANK"><img src="images/tw.png"> @ptpjb<br/></a>

		<span class="glyphicon glyphicon-phone"></span> (031) 8283180<br/>
		<span class="glyphicon glyphicon-globe"></span> <a href="http://www.ptpjb.com/index.php/id/" target="_BLANK">www.ptpjb.com</a><br/>
		<span class="glyphicon glyphicon-envelope"></span> info@ptpjb.com<br/><br/>
            </div>
        </div>
    </div>
</div>
<div class="bottom-footer bg-red">
	<div class="container">
    	<div class="col-md-12" style="text-align:center;">
        	Copyright &copy; 2015 Perpustakaan PJB.
        </div>
    </div>
</div>

<script type="text/javascript" src="script/bootstrap/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	$("#reload").load("<?= Helper::navAddress('_statistik.php') ?>");
});
</script>
