<?php
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

if (!strstr($_SERVER['HTTP_REFERER'], 'show')){
    header('Location: '. Helper::navAddress('index.php'));
    exit();
}

$file = urldecode($_REQUEST['file']);
$noseri = urldecode($_REQUEST['kode']);

$open = Perpus::isOpen($noseri,$file);
if($open == 1){
	if(!$_SESSION[idanggota]){
		header('Location: '. Helper::navAddress('index.php'));
		exit();
	}
}

header("Content-type:application/pdf");

$data = file_get_contents($file);
echo $data;

?>