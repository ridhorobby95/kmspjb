<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	require('classes/pdf.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['B_FORMTA'], Helper::navAddress('val_uploadta.php'), 2);
	// print_r($_REQUEST);
	// var_dump($i_phpfile);
	// $conn->debug=true;
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_UPLOAD'];
	$p_title = $form['T_UPLOAD'];
	$p_dbtable = "pp_uploadta";
	$issave = Helper::removeSpecial($_REQUEST['issave']);
	
	//cek di tabel pp_uploadta
	$userlogin = $_SESSION['idanggota'];
	$sql_cek_mhs = "select * from pp_uploadta where idanggota='$userlogin'";
	$rs_cek = $conn->GetRow($sql_cek_mhs);
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']); //iduploadta
		$idnotifikasi = Helper::removeSpecial($_REQUEST['idnotif']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$istrue = false;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){
			$conn->StartTrans();
			$rs_cek_val = $conn->GetRow("select * from pp_historyvalidasita where iduploadta=$rkey and npkvalidasi='".$_SESSION['idanggota']."'");
			
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['iduploadta'] = $rkey;
			$record['tglhistoryvalidasi'] = date('Y-m-d');
			$record['npkvalidasi'] = $_SESSION['idanggota'];
			$record['stsvalidasi'] = $_POST['stsvalidasi'];
			$record['catatanvalidasi'] = $_POST['catatanvalidasi'];
			Helper::Identitas($record);
			if(empty($rs_cek_val))
				Perpus::Simpan($conn, $record, 'pp_historyvalidasita');
			else
				Perpus::Update($conn, $record, 'pp_historyvalidasita', " iduploadta=".$rkey." and npkvalidasi='".$_SESSION['idanggota']."'");
			
			//insert ke tabel pp_historynotifikasiuploadta====================================================================
			$rs_cek_nrp = $conn->GetRow("select * from pp_uploadta where iduploadta=$rkey");
			
			$rec_histnotif = array();
			$rec_histnotif['iduploadta'] = $rkey;
			$rec_histnotif['tglhistorynotifikasi'] =  date("Y-m-d H:i:s");
			$rec_histnotif['nrptujuannotifikasi'] = $rs_cek_nrp['idanggota'];
			$rec_histnotif['pesan'] = $_POST['catatanvalidasi'];
			$rec_histnotif['stsnotifikasi'] = 0;
			Helper::Identitas($rec_histnotif);
			Perpus::Simpan($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
			
			$recupdate_histnotif = array();
			$recupdate_histnotif['stsnotifikasi'] = 1;
			Perpus::Update($conn, $recupdate_histnotif, 'pp_historynotifikasiuploadta', " idhistorynotifikasi=".$idnotifikasi);
			
			if($_POST['stsvalidasi']==null or $_POST['stsvalidasi']==0){ //jika tidak disetujui dan pernah di cek			
				$rec_uploadta = array();
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing1'] = 1;
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing2'] = 1;
				else if($rs_cek_nrp['npkpembimbing3'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing3'] = 1;
				
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing1'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing2'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing3'] = date("Y-m-d");
		
				// Helper::Identitas($rec_uploadta);
				Perpus::Update($conn, $rec_uploadta, 'pp_uploadta', " iduploadta=".$rkey);
				
			}else if($_POST['stsvalidasi']==1){ //jika disetujui/validasi
				$rec_uploadta = array();
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing1'] = 2;
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing2'] = 2;
				else if($rs_cek_nrp['npkpembimbing3'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing3'] = 2;
			
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing1'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing2'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing3'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing3'] = date("Y-m-d");
			
				// Helper::Identitas($rec_uploadta);
				Perpus::Update($conn, $rec_uploadta, 'pp_uploadta', " iduploadta=".$rkey);
			
			}
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
			}else{
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data Berhasil').'</div>';
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
		
	if (!empty($rkey)){
		$sql_cek_mhs = "select * from pp_uploadta where iduploadta=$rkey";
		$rset = $conn->GetRow($sql_cek_mhs);
		
		$sql_val = "select * from pp_historyvalidasita where iduploadta=$rkey and npkvalidasi='".$_SESSION['idanggota']."'";
		$rs_val = $conn->GetRow($sql_val);
		
		$rset_notif = $conn->GetRow("select count(*) as jml from pp_historynotifikasiuploadta where iduploadta=$rkey and npktujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0");
		
		$rset_hist = $conn->Execute("select a.namaanggota as dosen, b.namaanggota as mhs, c.namaanggota as mhs_penerima, h.* from pp_historynotifikasiuploadta h left join ms_anggota a on a.idanggota=h.npktujuannotifikasi left join ms_anggota b on b.idanggota=h.t_user
									left join ms_anggota c on c.idanggota=h.nrptujuannotifikasi
									where iduploadta=$rkey and (npktujuannotifikasi='".$_SESSION['idanggota']."' or h.t_user='".$_SESSION['idanggota']."')
									order by idhistorynotifikasi");
							
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
	<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post" >
	<?= $b_link->output(); ?>
		<?if(!empty($rkey)){?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer">
				<span class="menuTitle" style="margin-left:0px;"><?= $p_title?></span><br>
        		<? if($confirm) echo $confirm; 
					else{ 
						if($_REQUEST['r_confirm'] != ''){
							if($_REQUEST['r_confirm']=='1')
								echo '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data Berhasil.').'</div>';
							else
								echo '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal.',true).'</div>';
						}else	
							echo '';
					}
				?>
				<table width="100%" cellpadding="4" cellspacing="0" border=0>
					<tr>
						<td style="padding-top:10px;"><?= $form['BIBLIO_TITLE']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['judul'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['AUTHOR1']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['authorfirst1']." ".$rset['authorlast1'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['NRP1']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['nrp1'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['AUTHOR2']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['authorfirst2']." ".$rset['authorlast2'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['NRP2']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['nrp2'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['AUTHOR3']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['authorfirst3']." ".$rset['authorlast3'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['NRP3']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['nrp3'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['PUBLISHER']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['penerbit'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['YEAR_PUBLISH']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['tahunterbit'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['CONTRIBUTOR1']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['npkpembimbing1']." - ".$rset['pembimbing1'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['CONTRIBUTOR2']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['npkpembimbing2']." - ".$rset['pembimbing3'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;"><?= $form['CONTRIBUTOR3']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['npkpembimbing3']." - ".$rset['pembimbing3'];?></td>
					</tr>
					<tr>
						<td style="padding-top:10px;" width="150"><?= $form['DESC']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= $rset['keterangan'];?></td>
					</tr>
				</table>
				<table width="100%" cellpadding="4" cellspacing="0" border=0>
					<tr><td colspan="3">
						<div class="profile" style="position:relative;top:-2px;"><!-- ======================================== UPLOAD PUSTAKA ============================= -->
							<table width="100%" border="0" cellspacing=0 cellpadding="5">
								<tr height=30> 
									<td class="SubHeaderBGAlt" colspan="2" align="Left"><strong>Upload File</strong></td>
								</tr>
								<tr height=20> 
									<td class="RightColumnBG"><input type="hidden" value="2" id="theValue" /><br>
										<?if($c_edit){?>
											<?if($rset['file1']==''){?>
												1. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file1'] ?>"><?= $rset['file1']!='' ? "<img src='images/attach.gif' border=0> 1. ".Helper::GetPath($rset['file1']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file2']==''){?>
												2. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file2'] ?>"><?= $rset['file2']!='' ? "<img src='images/attach.gif' border=0> 2. ".Helper::GetPath($rset['file2']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file3']==''){?>
												3. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file3'] ?>"><?= $rset['file3']!='' ? "<img src='images/attach.gif' border=0> 3. ".Helper::GetPath($rset['file3']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file4']==''){?>
												4. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file4'] ?>"><?= $rset['file4']!='' ? "<img src='images/attach.gif' border=0> 4. ".Helper::GetPath($rset['file4']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file5']==''){?>
												5. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file5'] ?>"><?= $rset['file5']!='' ? "<img src='images/attach.gif' border=0> 5. ".Helper::GetPath($rset['file5']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file6']==''){?>
												6. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file6'] ?>"><?= $rset['file6']!='' ? "<img src='images/attach.gif' border=0> 6. ".Helper::GetPath($rset['file6']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file7']==''){?>
												7. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file7'] ?>"><?= $rset['file7']!='' ? "<img src='images/attach.gif' border=0> 7. ".Helper::GetPath($rset['file7']) : '' ?></a><br><br>
											<?}?>
											
											<?if($rset['file8']==''){?>
												8. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file8'] ?>"><?= $rset['file8']!='' ? "<img src='images/attach.gif' border=0> 8. ".Helper::GetPath($rset['file8']) : '' ?></a><br><br>
												<?}?>
											
											<?if($rset['file9']==''){?>
												9. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file9'] ?>"><?= $rset['file9']!='' ? "<img src='images/attach.gif' border=0> 9. ".Helper::GetPath($rset['file9']) : '' ?></a><br><br>
												<?}?>
											
											<?if($rset['file10']==''){?>
												10. File Kosong.<br><br>
											<?}else{?>
												<a href="<?= $rset['file10'] ?>"><?= $rset['file10']!='' ? "<img src='images/attach.gif' border=0> 10. ".Helper::GetPath($rset['file10']) : '' ?></a><br><br>
											<?}?>
										<?}else{
											echo "1. ".$rset['file1']."<br><br>"; 
											echo "2. ".$rset['file2']."<br><br>"; 
											echo "3. ".$rset['file3']."<br><br>"; 
											echo "4. ".$rset['file4']."<br><br>"; 
											echo "5. ".$rset['file5']."<br><br>"; 
											echo "6. ".$rset['file6']."<br><br>"; 
											echo "7. ".$rset['file7']."<br><br>"; 
											echo "8. ".$rset['file8']."<br><br>"; 
											echo "9. ".$rset['file9']."<br><br>"; 
											echo "10. ".$rset['file10']."<br><br>"; 
										}?>
									</td>
								</tr>
							
							</table>
						</div></td>
					</tr>
				</table><br><br>
				<table width="100%" cellpadding="4" cellspacing="0" border=0>
					<span class="menuTitle" style="margin-left:0px;">History Notifikasi</span><br><br>
					<?while($row_notif = $rset_hist->FetchRow()){ 
						if($row_notif['dosen']=='')
							$penerima = $row_notif['nrptujuannotifikasi'].' - '.$row_notif['mhs_penerima'];
						else
							$penerima = $row_notif['npktujuannotifikasi'].' - '.$row_notif['dosen'];
					?>
						<div>
							<div style="padding:5px;margin-bottom:15px">
								<div style="border-bottom:1px dashed #999;margin-bottom:5px;padding-bottom:5px">
									<span style="float:left">
										<?php echo 'Pengirim #'. $row_notif['t_user'].' - '.$row_notif['mhs']?><br>
										<?php echo 'Penerima #'. $penerima ?>
									</span>
									
									<span style="float:right;font-size:smaller">
										<?php echo Helper::formatDateTime($row_notif['tglhistorynotifikasi'])?>
									</span>
									<div style="clear:both"></div>
								</div>
								<div>
									<img style="float:right;border:2px solid gray" width="50" height="50" src="../PJB/images/perpustakaan/anggota/<?= $row_notif['t_user'].'.jpg'?>" />
									<?php echo $row_notif['pesan']; ?>
									<div style="clear:both"></div>
								</div>
								
							</div>
						</div>
					<?}?>
					<tr>
						<td style="padding-top:10px;" width="150"><?= $form['ISOK']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>&nbsp;&nbsp;
						<?if($rset_notif['jml']==0){?>
						<td style="padding-top:5px;">&nbsp;&nbsp;<?= $rs_val['stsvalidasi']=='1'?'Valid':'Tidak Valid'?></td>
						<?}else{?>
						<div>
							Pesan Notifikasi: <br><br><?php echo UI::createTextArea('catatanvalidasi','','ControlStyle',3,60,$c_edit); ?>
							<div style="clear:both"></div>
						</div>
						<td style="padding-top:5px;">&nbsp;&nbsp;<input type="checkbox" name="stsvalidasi" id="stsvalidasi" value="1" <?= $rs_val['stsvalidasi']==1 ? 'checked' : ''?>></td>
						<?}?>
					</tr>
					<?if($rs_val['stsvalidasi'] == 0){?>
						<?if($issave!=1){
							if($rset_notif['jml']>0){
						?>
					<tr>
						<td colspan="3" align="center" style="padding-top:5px;">
							<span name="simpan" class="buttonSubmit" value="Simpan" onclick="goSimpan('<?= $idnotifikasi;?>')" style="cursor:pointer;"><?= $form['SENT']; ?></span>
							<span name="reset" class="buttonSubmit" value="Reset" style="cursor:pointer" onclick="goUndo()"><?= $form['RESET']; ?></span>
						</td>
					</tr>
					<?}}}?>
				</table>
				<input type="hidden" name="act" id="act" />
				<input type="hidden" name="redit" id="redit" value="<?= $rkey; ?>" />
				<input type="hidden" name="delfile" id="delfile">
				<input type="hidden" name="delkey" id="delkey">
			</div>
		</div>
		<?}?>
			
	</form>
	<iframe name="upload_iframe" style="display:none;"></iframe>
</body>

<script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/jquery.masked.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	$(function(){
		$("#perpusform").find("#hargausulan").onlyNum();
	   $("#tahunterbit").mask("9999");
	});
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goSimpan(idnotif){
		// alert($("#perpusform").serialize());
		var save=confirm("Apakah Anda yakin?");
		if(save){
			if (cfHighlight('catatanvalidasi')){
				sent = "";
				sent += $("#perpusform").serialize();
				sent += "&act=simpan";
				sent += "&idnotif="+idnotif;
				goSubmit('contents','<?= $i_phpfile?>',sent);
			}
		}
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
	
	function CreateTextbox(){
		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type=file name='userfile[]' size='40' /> </p>";
		}
	}
	
	function goDelFile(key1,key2){
		var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
		if(delfile){
			sent = '';
			sent += $("#perpusform").serialize();
			sent += "&act=delfile";
			sent += "&delkey="+key1;
			sent += "&delfile="+key2;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		
			// document.getElementById('act').value="delfile";
			// document.getElementById('delkey').value=key1; //file1
			// document.getElementById('delfile').value=key2; //isi file1
			// // document.getElementById('delseri').value=key3;
			// document.getElementById('perpusform').submit();
			// // goSubmit();
		}
	}
	
	function goRequestVal(iduploadta){
		var request=confirm("Apakah Anda yakin akan melakukan permintaan validasi?");
		if(request){
			sent = '';
			// sent += $("#perpusform").serialize();
			sent += "&act=requestval";
			sent += "&iduploadta="+iduploadta;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
</script>
</html>
