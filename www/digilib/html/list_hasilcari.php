<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_alf = Helper::removeSpecial($_REQUEST['alf']);
	$p_page = Helper::removeSpecial($_REQUEST['paging']);
	$r_adv = Helper::removeSpecial($_REQUEST['isadv']);
	$r_topik = Helper::removeSpecial($_REQUEST['topik']);
	$r_jenis = Helper::removeSpecial($_REQUEST['jenispustaka']);
	
	$r_lokasi = Helper::removeSpecial($_REQUEST['lokasi']);
	
	$b_link = new Breadcrumb();
	if($r_adv=='')
		$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcari.php').'&paging='.$p_page.'&key='.$r_key.'&lokasi='.$r_lokasi.'&jenispustaka='.$r_jenis.'&alf='.$r_alf, 0);
	else {
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_key1 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword1']));
		$r_key2 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword2']));
		$r_bahasa = Helper::removeSpecial($_REQUEST['kdbahasa']);
		$r_topik1 = Helper::removeSpecial($_REQUEST['topik1']);
		$r_topik2 = Helper::removeSpecial($_REQUEST['topik2']);
		$r_tahun1 = Helper::removeSpecial($_REQUEST['tahun1']);
		$r_tahun2 = Helper::removeSpecial($_REQUEST['tahun2']);
		$r_jenis1 = Helper::removeSpecial($_REQUEST['jenispustaka1']);
		$r_jenis2 = Helper::removeSpecial($_REQUEST['jenispustaka2']);
		$r_bool = Helper::removeSpecial($_REQUEST['bool']);
		$b_link->add($menu['SEARCH'], Helper::navAddress('list_hasilcari.php').'&paging='.$p_page.'&numpage='.$r_page.'&keyword1='.$r_key1.'&keyword2='.$r_key2.'&kdbahasa='.$r_bahasa.'&jenispustaka1='.$r_jenis1.'&jenispustaka2='.$r_jenis2.'&topik1='.$r_topik1.'&topik2='.$r_topik2.'&jenispustaka1='.$r_jenis1.'&jenispustaka2='.$r_jenis2.'&tahun1='.$r_tahun1.'&tahun2='.$r_tahun2.'&bool='.$r_bool.'&isadv=1',0);
	}
	//konfigurasi halaman
	$p_title = $form['RESULT_SEARCH'];
	
?>
<div class="container">
  <div class="col-md-9" style="border-right:1px solid #ddd;">
    <div class="primary">
      <div>

<?= $b_link->output(); ?>

	<form name="advform" id="advform" method="post" action="<?= $i_phpfile ?>">
		<input type="hidden" name="isadv" id="isadv" value="1" />
	</form>
	<div style="float:left;width:100%;margin-top:15px;">
		<div id="resultSearch"></div>
	</div>

</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>

<script src="script/linkis.js" type="text/javascript"></script> 
<script type="text/javascript">
	//halaman untuk ajaxin
	listhasil = "<?= Helper::navAddress('xlist_hasilcari.php'); ?>";
	
	$(function() {
		keysearch = "key="+ document.getElementById("txtcari").value;
		$("div.tabs a[class='tablink']").click(function() {
				var index = $("div.tabs a").index(this);
				
				$("div.tabs li").removeAttr("class");
				$(this).parent("li").attr("class","selected");
				
				$("div[class='items']").hide();
				$("div[class='items']").eq(index).show();
			});
		
			//chooseTab(0);
			var which = $(location).attr('hash').substring(1,100);
		
				chooseTab(0);
		if(keysearch)
			goLink('resultSearch', listhasil, keysearch);
		showJenis1();
		showJenis2();		
	});
	
	function etrCariln(e) {
		var ev = (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			goCariLn();
	}
	function etrCari2(e) {
		var ev = (window.event) ? window.event : e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			goCariADV();
	}
	
	function goCariJudul(x){
			var keysearch = "key=" + x +"&alf=1";
			goLink('resultSearch', listhasil, keysearch);
	}
	function goCariAuthor(x){
			var keysearch = "key=" + x +"&alf=2";
			goLink('resultSearch', listhasil, keysearch);
	}
	
	function goCariLn()
	{
		var str = $.trim(document.getElementById("txtcariln").value);
		
		if(parseInt(str.length) > 0){
			var keysearch = "key=" + document.getElementById("txtcariln").value + "&lokasi=" + document.getElementById("lokasi").value + "&jenispustaka=" + document.getElementById("jenispustaka").value;
			goLink('resultSearch', listhasil, keysearch);
		}else{
			alert("<?= $sidebar['ALERT_SEARCH']; ?>");
		}
	}
	
	function chooseTab(idx) {
		$("div.tabs a").eq(idx).triggerHandler("click");
	}
	
	function goCariADV()
	{
		var str = $.trim(document.getElementById("keyword1").value);
		if(parseInt(str.length) > 0){
		var keysearch =  $("#advform").serialize();
		goLink('resultSearch', listhasil, keysearch);
		}else{
			alert("<?= $sidebar['ALERT_SEARCH']; ?>");
		}
	}
	
	function goAdvance()
	{
		var str = $.trim(document.getElementById("keyword1").value);
		document.getElementById("keyword1").value = str;
		$("#Simple").hide();
		$("#Advance").show();
	}
	
	function goSimple()
	{
		var str = $.trim(document.getElementById("txtcariln").value);
		document.getElementById("txtcariln").value = str;
		$("#Simple").show();
		$("#Advance").hide();
	}
	
	function showJenis()
	{
		if ($("#topik").val() == 'J')
			$("#jenispustaka").show();
		else
			$("#jenispustaka").hide();
	}
	
	function showJenis1()
	{
		if ($("#topik1").val() == 'J')
			$("#jenispustaka1").show();
		else
			$("#jenispustaka1").hide();
	}
	
	function showJenis2()
	{
		if ($("#topik2").val() == 'J')
			$("#jenispustaka2").show();
		else
			$("#jenispustaka2").hide();
	}
	
	function showPus(key){
		sent = "&key=" + key;
		goLink('contents','<?= Helper::navAddress('data_pustaka.php')?>', sent);
	}
</script> 
