<?php 
	
	//if($_SERVER['REMOTE_ADDR'] == "110.139.66.122")
	//$conn->debug=true;
	
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	$file = $_REQUEST['file'];
	$seri = $_REQUEST['kode'];
	$det = $_REQUEST['det'];
	$cOpen = Helper::canOpen($conn,$file,$seri);
	
	$cOpens=explode('__#__',$cOpen);

	list($nama,$ext) = explode('.',Helper::GetPath($file));
	
	if ($ext != 'pdf'){
		exit();
		}

	$directori = 'ghostfile/'.$seri.'_'.$det.'/'.$nama;

	Helper::addHistory($conn, "Buka File ", "File ".Helper::GetPath($file));
	
	//cek directori ada atau tidak
	if (!is_dir($directori)){
		if($cOpens[0]=='JN'){
		    if($_SESSION['userid']=='' and (substr($_SERVER['REMOTE_ADDR'],0,3) != '192'))
		    exit();
		}
		else{
		    if($cOpens[1]=='' and $_SESSION['userid']=='')
		    exit();
		}
		
		$_SESSION['xfiledownd']='upl_link/file/'.str_replace('uploads/file/','',$file);
		header('Location: '.Helper::navAddress('down.php'));
		exit();
	}
	else {//var_dump($cOpens);
		//echo "test";die();
		if($cOpens[0]=='JN'){
		    if($_SESSION['userid']=='' and (substr($_SERVER['REMOTE_ADDR'],0,3) != '192'))
		    exit();
		}
		else{
		    if($cOpens[1]=='' and $_SESSION['userid']=='')
		    exit();
		}
	}
	
	$i = 0;
	//menentukan jumlah file yang ada di folder tsb
	if ($handle = opendir($directori)){ 
   		 while (false !== ($fileread = readdir($handle))) {
			if ($fileread != "." && $fileread != "..") 
				$i++;
		}
		closedir($handle);
	}

	//konfigurasi page
	$title = $file;
	$p_id = 'v_'.$file;
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
		
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	if (Helper::removeSpecial($_REQUEST['numpage'])){
		$p_page = Helper::removeSpecial($_REQUEST['numpage']);
		if ($p_page > $i)
			$p_page = $i;
	}
		
	if ($i == 0){
		echo 'Data Tidak Ditemukan';
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = $i;
		$p_page = 0;
	}else{
		$p_atfirst = (($p_page == $i || $p_page!=1) ? 0 : 1);
		$p_atlast = ($p_page == $i ? 1 : 0);
		$p_lastpage = $i;
	}
			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/pager.css" type="text/css" rel="stylesheet">
<script src="script/jquery.js" type="text/javascript"></script>
<script src="script/forpager.js" type="text/javascript"></script>
<link href="images/favicon.ico" rel="shortcut icon" />
<title>View Download File</title>
</head>
<body style="margin:0px;" onLoad="initButton(<?= $p_atfirst ? '1' : '0'; ?>,<?= $p_atlast ? '1' : '0'; ?>);" onmousemove="checkS(event)" >
<form name="perpusform" id="perpusform" method="post" action="index.php?page=view_pdfseri&kode=<?= $seri ?>&file=<?= $file; ?>&det=<?= $det; ?>">
	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="padding:4px 10px 10px 10px;position:fixed;top:0px;left:0px;background:url(images/bg_viewer.gif) repeat-x;height:51px;">
		<tr>
			<td><img src="images/logo_small.png" alt=""></td>
			<td align="right" valign="middle" style="padding-top:4px;"><span style="color:#fff;position:relative;bottom:7px;">Go To Page : </span><input type="text" name="numpage" id="numpage" maxlength="3" style="padding-right:3px;width:22px;position:relative;bottom:7px;" onKeyPress="etrGoPage(event)">
				<img src="images/go.png" style="cursor:pointer;margin-right:20px;" onclick="goPage()">
				<span style="color:#fff;position:relative;bottom:7px;margin:20px;">Page <?= $p_page; ?>/<?= $i; ?>&nbsp;</span>
				<img title="first" id="firstButton" onClick="goFirst(<?= $p_page; ?>)" src="images/first.png" style="cursor:pointer;width:31px;">
				<img title="previous" id="prevButton" onClick="goPrev(<?= $p_page; ?>)" src="images/prev.png" style="cursor:pointer;width:31px;">
				<img title="next" id="nextButton" onClick="goNext(<?= $p_page.','.$p_lastpage; ?>)" src="images/next.png" style="cursor:pointer;width:31px;">
				<img title="last" id="lastButton" onClick="goLast(<?= $p_page.','.$p_lastpage; ?>)" src="images/last.png" style="cursor:pointer;width:31px;">
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="margin-top:51px;">
		<tr>
			<td colspan="2" align="center">
			<? if($i>1){  
				if (is_file($directori.'/'.Helper::GetPath($nama).'-'.($p_page-1).'.png')){ ?>
					<img src="<?= $directori.'/'.Helper::GetPath($nama).'-'.($p_page-1).'.png'; ?>" align="absmiddle" />
				<? } elseif(is_file($directori.'/'.Helper::GetPath($nama).'-'.($p_page-1).'.jpg')) { ?>
					<img src="<?= $directori.'/'.Helper::GetPath($nama).'-'.($p_page-1).'.jpg'; ?>" align="absmiddle"/>
				<? }else echo 'Data tidak Ditemukan'; 
			}else{?>
				<? if (is_file($directori.'/'.Helper::GetPath($nama).'.png')){ ?>
					<img src="<?= $directori.'/'.Helper::GetPath($nama).'.png'; ?>" align="absmiddle" />
				<?} elseif(is_file($directori.'/'.Helper::GetPath($nama).'.jpg')) { ?>
					<img src="<?= $directori.'/'.Helper::GetPath($nama).'.jpg'; ?>" align="absmiddle" />
				<? } ?>
			<? } ?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="page" id="page" value="<?= $p_page ?>">
</form>
</body>
<script type="text/javascript">

var mouseX = 0; 
var mouseY = 0;

var ie  = document.all; 
var ns6 = document.getElementById&&!document.all;

var isMenuOpened  = false ;
var menuSelObj = null ;
var overpopupmenu = false;
var gParam;

var posx = 0; 
var posy = 0;

function etrGoPage(e) {
	var ev= (window.event) ? window.event: e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;
	
	if (key == 13)
		$("#perpusform").submit();
}

function goPage(){
	$("#perpusform").submit();
}


</script>
</html>
