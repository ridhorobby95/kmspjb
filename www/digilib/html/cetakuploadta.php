<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	// $conn->debug=true;
	require_once('classes/perpus.class.php');
	$rkey = Helper::removeSpecial($_REQUEST['key']);
	
	//untuk proses penyimpanan
	$rs = $conn->GetRow("select * from pp_uploadta where iduploadta=$rkey");

	$record = array();
	$record['tglcetak'] = date("Y-m-d");
	Perpus::Update($conn, $record, 'pp_uploadta', " iduploadta=".$rkey);
?>

<html>
	<head>
		<title>
		Cetak Tanda Terima
		</title>
		<style type="text/css">
		body,td {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 8pt;
		}
		
		table .containerTable2 {
			border:1px solid #6699cc;
			border-collapse:collapse;
			font-size:11px;
			margin:20px 0px 10px 0px;
			text-align:left;
		}
		</style>
	</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="printpage()" style="background-color:#f0f0f0;">

<div class="profile">
	<div class="contentTop"></div>
	<div class="contentContainer" style="background:#FFFFFF">
		<table width="100%" class="containerTable2" cellpadding="4" cellspacing="0">
			<table width="100%" border=0><tr>
				<td valign="top"><img src="images/kop.png" width="100%" height="100"></img></td>
				</td>
			</tr>
			</table>
			<table width="100%" cellpadding="4" cellspacing="0" border=0>
			<tr height="50"><td colspan="2"></td></tr>
			<tr><td colspan="2" height="20">Perpustakaan PJB menyatakan bahwa mahasiswa dibawah ini:</td></tr>
			<tr>
				<td width="70" valign="top" style="padding-top:10px;">NAMA&nbsp;&nbsp;&nbsp;:</td>
				<td height="10" style="padding-top:5px;">
				<?if($rs['nrp1']!='') echo '1. '.$rs['authorfirst1']." ".$rs['authorlast1']."<br><br>";
					if($rs['nrp2']!='') echo '2. '.$rs['authorfirst2']." ".$rs['authorlast2']."<br><br>";
					if($rs['nrp3']!='') echo '3. '.$rs['authorfirst3']." ".$rs['authorlast3']."<br><br>";
				?></td>
			</tr>
			<tr>
				<td valign="top" style="padding-top:10px;">NRP&nbsp;&nbsp;&nbsp;:</td>
				<td style="padding-top:5px;">
				<?if($rs['nrp1']!='') echo '1. '.$rs['nrp1']."<br><br>";
					if($rs['nrp2']!='') echo '2. '.$rs['nrp2']."<br><br>";
					if($rs['nrp3']!='') echo '3. '.$rs['nrp3']."<br><br>";
				?></td>
			</tr>
			<tr><td colspan="2" height="30">Telah melakukan Upload File Tugas Akhir dan telah di-verifikasi oleh:</td></tr>
			<tr>
				<td valign="top" style="padding-top:10px;">DOSEN&nbsp;&nbsp;&nbsp;:</td>
				<td style="padding-top:5px;">
				<?if($rs['npkpembimbing1']!='') echo '1. '.$rs['pembimbing1']."<br><br>";
					if($rs['npkpembimbing2']!='') echo '2. '.$rs['pembimbing2']."<br><br>";
					if($rs['npkpembimbing3']!='') echo '3. '.$rs['pembimbing3']."<br><br>";
				?></td>
			</tr>
			<tr>
				<td valign="top" style="padding-top:10px;">NPK&nbsp;&nbsp;&nbsp;:</td>
				<td style="padding-top:5px;">
				<?if($rs['npkpembimbing1']!='') echo '1. '.$rs['npkpembimbing1']."<br><br>";
					if($rs['npkpembimbing2']!='') echo '2. '.$rs['npkpembimbing2']."<br><br>";
					if($rs['npkpembimbing3']!='') echo '3. '.$rs['npkpembimbing3']."<br><br>";
				?></td>
			</tr>
			<tr><td height="20" colspan="2">&nbsp;</td></tr>
			<tr height="30">
				<td colspan="2" style="padding-right:20px;" align="right">Surabaya, <?= helper::formatDateInd(date("Y-m-d"))?>
				</td>
			</tr>
			<tr><td colspan="2" style="padding-right:20px;" align="right"><img width="170" height="110" src="images/stempel.png"></td></tr>
			<tr><td>&nbsp;</td></tr>
			</table>
		</table>
	</div>
</div>
</body>
<script type="text/javascript">
	function printpage(){
		 window.print();
	}
</script>
</html>