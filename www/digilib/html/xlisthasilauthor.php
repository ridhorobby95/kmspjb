<? 
	//$conn->debug=true;
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//tambahan
	require_once('classes/fts.class.php');
	require_once('classes/breadcrumb.class.php');
	require_once('classes/perpus.class.php');
		
	
	$r_key = Helper::removeSpecial(trim($_REQUEST['key']));
	$par = Helper::removeSpecial(trim($_REQUEST['par']));
	$r_alf = Helper::removeSpecial(trim($_REQUEST['alf']));

	$b_link = new Breadcrumb();

	$connLamp = Factory::getConnLamp();
	$connLamp->debug = false;
	$cek = $connLamp->IsConnected();
	
	//post dari halaman adv
	$r_page = Helper::removeSpecial($_REQUEST['numpage']);
	$r_key1 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword1']));
	$r_key2 = Helper::removeSpecial(Helper::strOnly($_REQUEST['keyword2']));
	$r_bahasa = Helper::removeSpecial($_REQUEST['kdbahasa']);
	$r_topik = Helper::removeSpecial($_REQUEST['topik']);
	$r_topik1 = Helper::removeSpecial($_REQUEST['topik1']);
	$r_topik2 = Helper::removeSpecial($_REQUEST['topik2']);
	$r_tahun1 = Helper::removeSpecial($_REQUEST['tahun1']);
	$r_tahun2 = Helper::removeSpecial($_REQUEST['tahun2']);
	$r_bool = Helper::removeSpecial($_REQUEST['bool']);
	
	$peng = $conn->GetArray("select p.namadepan || ' ' || p.namabelakang as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor   ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	//settingan untuk halaman
	$p_window = "[Digilib] Hasil Pencarian";
	$p_title = "Hasil Pencarian";
		
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 10; //default
		
			
	if (!empty($_POST))
	{
		$p_page 	= Helper::removeSpecial($_REQUEST['page']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
	}
  
	if (!$p_page)
		$p_page = 1; // halaman default adalah 1
	
	$p_sqlstr = "select p.*,p.idpustaka as idpus,j.namajenispustaka,j.islokal
		from ms_pustaka p
		join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka 
		where (
			upper(coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'')) like upper('%$r_key%') or
			upper(coalesce(p.authorfirst2,'')||' '||coalesce(p.authorlast2,'')) like upper('%$r_key%') or
			upper(coalesce(p.authorfirst3,'')||' '||coalesce(p.authorlast3,'')) like upper('%$r_key%')
		) or (
			p.idpustaka in (
						select pa.idpustaka
						from pp_author pa
						join ms_author a on a.idauthor = pa.idauthor
						where upper(coalesce(a.namadepan,'')||' '||coalesce(a.namabelakang,'')) like upper('%$r_key%')
					)
		)";
	if($par==1)
		$p_sqlstr .= " and p.kdjenispustaka in (select kdjenispustaka from lv_jenispustaka where islokal=1)";
	
	$p_sqlstr .= " order by j.kdjenispustaka ";
		
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
		
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	$b_link->add('Author', Helper::navAddress('xlisthasilauthor.php').'&par='.$par.'&key='.$r_key, 2);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="style/pager.css" />
<script type="text/javascript" src="script/auth.js"></script>
<script src="script/searchhi.js" type="text/javascript"></script>
<title><?= $p_window;?></title>
</head>
<body onLoad="JavaScript:loadSearchHighlight();">
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<?= $b_link->output(); ?>
	<? /*if (isset($_SESSION['userid'])){?>
	<div class="notification">
		<?= Perpus::getCart(); ?>
	</div><br><br>
	<? }*/ ?>
	<table width="100%" cellpadding="4" cellspacing="0">
		<tr>
			<td><h4 class="sidebar-title"><?= $form['SEARCH_RESULT']; ?></h4></td>
		</tr>
		<tr height="20">
			<td>&nbsp;</td>
		</tr>
		<tr height="20">
			<td>
				<?= $form['PAGE']?> <?= $p_page ?> <?= $form['FROM_RESULT']?>
				<strong><?= $p_recount ?></strong> <?= $form['RESULT_FOR']?>
				<strong><font color="#428BCA"><?= $r_key;?></font></strong>
			</td>
		</tr>
	<? while ($row = $rs->FetchRow()){?>
		<tr>
			<td>
				<table width="100%" class="result" border="0" cellpadding="4" cellspacing="0" onMouseOver="this.className='resultH'" onMouseOut="this.className='result'" style="border-top:1pt dotted #d2d2d2;">
					<tr>
						<?
							$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ".$row['idpustaka'];
							$rsCover = $connLamp->GetOne($sqlCover);
							$cover = Perpus::loadBlobFile($rsCover, 'jpg');
						?>
						<td class="resultTitle" rowspan="5" width="120" align="center" style="padding-left:12px;padding-right:12px;">
							<img id="imgfoto" src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" width="100" height="108">
							<!--img id="imgfoto" src="<?//= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" width="100" height="108"-->
						</td>
						<td class="resultSubB" colspan="2">
							<a class="judul-buku" title="Lihat" onClick="goLihat('<?= $row['idpustaka']?>')"><?= $row['judul'] != '' ? Helper::repCari($row['judul'],$r_key) : '*'; ?><?= $row['edisi'] != '' ? ' / '.Helper::repCari($row['edisi'],$r_key) : ''; ?></a>
						</td>
					</tr>
					<tr>
						<td class="resultDesc" colspan="2"><strong><?= $form['NO_CALL']?></strong> <?= $row['nopanggil'] ?>, <strong>Edisi</strong> <?= $row['edisi']?></td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['AUTHOR']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">:
						<?php
							$auth = array();
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									$auth[] = $a;
								}
								echo Helper::repCari(implode(", ",$auth),$r_key);
							}else
								echo Helper::repCari($row['authorfirst1'],$r_key).' '.Helper::repCari($row['authorlast1'],$r_key);
						?>
						</td>
					</tr>
					<? if($row['islokal']==1) { ?>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong>Contributor</strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: <?= $row['pembimbing'] ?></td>
					</tr>
					<? } ?>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['DIRECTORY']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= $row['namajenispustaka'];?>]</td>
					</tr>
					<tr>
						<td class="resultSubB" width="120" valign="top"><strong><?= $form['YEAR_PUBLISH']?></strong></td>
						<td class="resultSubB" align="left" valign="top" style="color:#666666">: [<?= $row['tahunterbit'];?>]</td>
					</tr>
				</table>
				<br />
			</td>
		</tr>
	<? } ?>
		<tr>
			<td align="center"><?= $form['PAGE']?>&nbsp;:&nbsp;
					<?	if($p_page == 0)
							echo '&nbsp;';
						else {
							$start = (($p_page > 9) ? ($p_page-9) : 1);
						$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
							$link = array();
							
							for($i=$start;$i<=$end;$i++) {
								if($i == $p_page)
									$link[] = '<strong><font color="#FF403C">'.$i.'</font>|</strong>';
								else{
									if (!isset($_REQUEST['isadv']))
										$link[] = '<strong><font color="#FF403C"><a href="javascript:goPage('.$i.')">'.$i.'</a></font>|</strong>';
									else
										$link[] = '<strong><font color="#FF403C"><a href="javascript:goPageADV('.$i.')">'.$i.'</a></font>|</strong>';
								}
							}
							echo implode(' ',$link);
						}
					?>
			</td>
		</tr>
	</table>
	
</div></div>
  </div>
  
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	listhasil = "<?= Helper::navAddress('xlisthasilauthor.php'); ?>";
	databp = "<?= Helper::navAddress('data_pustaka.php'); ?>";
	
	function goPage(npage) {
		var sent = "&topik=<?= $r_topik; ?>&key=<?= $r_key?>&alf=<?= $r_alf ?>&page=" + npage; 
		$("#contents").divpost({page: listhasil, sent: sent});
	}
	
	function goPageADV(npage) {
		var sent = "&keyword1=<?= $r_key1 ?>&keyword2=<?= $r_key2 ?>&kdbahasa=<?= $r_bahasa; ?>&topik1=<?= $r_topik1; ?>&topik2=<?= $r_topik2; ?>&tahun1=<?= $r_tahun1 ?>&tahun2=<?= $r_tahun2 ?>&bool=<?= $r_bool; ?>&isadv=1&numpage=<?= $r_page; ?>&page=" + npage;
		$("#resultSearch").divpost({page: listhasil, sent: sent});
	}
	 
	function goLihat(key){
		var sent = "&key=" + key;
		$("#contents").divpost({page: databp, sent: sent});
	}
</script>
</html>
