<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
	if(empty($_SESSION[Config::G_SESSION]['iduser'])){
		echo '<script type="text/javascript">location.reload(true);</script>';
		exit();
	}
	
	$connLamp = Factory::getConnLamp();
	//$cek = $connLamp->IsConnected();
//$conn->debug=true;
//$connLamp->debug=true;
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add("Pengusulan", Helper::navAddress('data_usulanbiasa.php'), 0);
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_REQUESTPUBLIC'];
	$p_title = $form['T_REQUESTPUBLIC'];
	$p_dbtable = "pp_usul";
	//$p_foto = Config::dirCover;
			
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']);
		
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$istrue = false;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['idanggota'] = $_SESSION['idanggota'];
			$record['namapengusul'] = $_SESSION['usernama'];
			$record['tglusulan'] = date('Y-m-d');
			$record['statususulan'] = 0;
			Helper::Identitas($record);
			
			//jika tipe mendapatkan pagu dan harga melebihi pagu tampilkan pesan
			if ($rkey == ''){
				Perpus::Simpan($conn, $record, $p_dbtable);
				$a_ins = true;
			}
			else
				Perpus::Update($conn, $record, $p_dbtable, " idusulan=$rkey");
				
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
			else{
			
				#script upload cover belum disesuaikan
				if($a_ins){
					$rkey = $conn->GetOne("select max(idusulan) from pp_usul ");
				}
				
				$p_foto = Config::dirCover.trim($rkey).'.jpg';

				if($_FILES['coverp']['error'] == UPLOAD_ERR_OK)
					$sfok = UI::createFoto($_FILES['coverp']['tmp_name'],$p_foto,500,500);
				else if($_FILES['coverp']['error'] == UPLOAD_ERR_INI_SIZE or $_FILES['coverp']['error'] == UPLOAD_ERR_FORM_SIZE)
					$sfok = -4; // ukuran file melebihi batas
				else if($_FILES['coverp']['error'] == UPLOAD_ERR_NO_FILE)
					$sfok = -5; // tidak ada file yang diupload

				$path = $_FILES['coverp']['tmp_name'];
				if($sfok>0){
					$id_poto = $connLamp->GetOne("select idlampiranusulan from lampiranusulan where idusulan = $rkey ");
					
					$record = array();
					$record['idusulan'] = $rkey;
					$record['tiduser'] = Helper::cStrNull($_SESSION['userid']);
					$record['filename'] = $_FILES['coverp']['name']; 
					if($id_poto){
						$err = Perpus::UpdateBlob($connLamp,$record,'lampiranusulan',$path,'idlampiranusulan','isifile',$id_poto);
					}else{
						# UPLOAD COVER PUSTAKA
						$err = Perpus::InsertBlob($connLamp,$record,'lampiranusulan',$path,'idlampiranusulan','isifile');
					}
				}
				
				@unlink($_FILES['coverp']['tmp_name']); // hapus dan jangan tampilkan pesan error bila error
				
				switch($sfok) {
					case -1: $uploadmsg = 'Format file foto tidak dikenali.'; break;
					case -2: $uploadmsg = 'Format file foto harus GIF, JPEG, atau PNG.'; break;
					case -3: $uploadmsg = 'File foto tidak bisa diupload.'; break;
					case -4: $uploadmsg = 'Ukuran file foto melebihi batas maksimal.'; break;
					case -5: $uploadmsg = 'Tidak ada file foto yang di-upload.'; break;
					default: $uploadmsg = 'Upload foto berhasil.'; break;
				}
				
				if($sfok < 0)
					$uploadmsg = UI::message($uploadmsg,true);
				else
					$uploadmsg = UI::message($uploadmsg);
			
				
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data berhasil').'</div>';
				$istrue = true;
			}
			
			if($connLamp->ErrorNo() != 0){
				$mconfirm = '0';
			}else{
				$mconfirm='1';
			}
		
			$rset = array();
			$rset = $_POST;
			
			if ($istrue){ ?>
			<html>
			<script type="text/javascript" src="scripts/jquery.js"></script>
			<script type="text/javascript" src="scripts/jquery.common.js"></script>
			<script src="script/linkis.js" type="text/javascript"></script>
			<script src="script/foredit.js" type="text/javascript"></script>
			<script type="text/javascript">
				var msg = 'r_confirm='+<?= $mconfirm?>;
				window.parent.goLoading('contents','<?= $i_phpfile?>',msg);
			</script>
			</html>
		<?php
				exit();
				
			}	
				
		}
		else if ($r_act == 'hapus'){
			unlink(Config::dirCover.trim($rkey).'.jpg');
			Perpus::Delete($connLamp, lampiranusulan, " idusulan=$rkey");
			Perpus::Delete($conn, $p_dbtable, " idusulan=$rkey");
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
			else 
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan Data berhasil').'</div>';
			
			if ($istrue)
				Helper::addHistory($conn, "Penghapusan Pengusulan Biasa Berhasil", "idusulan=".$rkey);
			else
				Helper::addHistory($conn, "Gagal Mengahapus Pengusulan Biasa", "idusulan=".$rkey);
				
			$rkey = null;
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
		
	if ($rkey != ''){
		$sql = "select * from $p_dbtable where idusulan=$rkey";
		$rset = $conn->GetRow($sql);
		
		$sqlCover = "select isifile from lampiranusulan where idusulan = $rkey";
		$rsCover = $connLamp->GetOne($sqlCover);
		//$cover = Perpus::loadBlobFile($rsCover, 'jpg');
	}
	
	//data
	$p_sqlstr = "select p.*,p.judul as judulpustaka,coalesce(p.authorfirst1,'')||' '||coalesce(p.authorlast1,'') as pengarang,
				p.penerbit as namapenerbit, p.idusulan as id, o.ststtb, o.stspengadaan  
		from $p_dbtable p 
		left join pp_orderpustaka o on p.idusulan=o.idusulan
		left join pp_pengadaan pd on o.idpengadaan=pd.idpengadaan
		where p.idunit is null and idsumbangan is null and idanggota='".$_SESSION['idanggota']."'
		order by tglusulan desc";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>

	<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post" enctype="multipart/form-data">
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer">
				<h4 class="sidebar-title"><?= $p_title?></h4>
        		<?= (!$confirm) ?  '' : $confirm; ?>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td style="padding-top:5px;"><?= $form['BIBLIO_TITLE']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('judul',$rset['judul'],'ControlStyle',50,50,$c_edit); ?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['PRICE']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('hargausulan',$rset['hargausulan'],'ControlStyle',20,20,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?>
						</td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR1']; ?> *</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst1',$rset['authorfirst1'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast1',$rset['authorlast1'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR2']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst2',$rset['authorfirst2'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast2',$rset['authorlast2'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['AUTHOR3']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('authorfirst3',$rset['authorfirst3'],'ControlStyle',100,30,$c_edit); ?>&nbsp;&nbsp;<?= UI::createTextBox('authorlast3',$rset['authorlast3'],'ControlStyle',100,30,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['PUBLISHER']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('penerbit',$rset['penerbit'],'ControlStyle',100,60,$c_edit,'readOnly="readOnly"'); ?>&nbsp;<img src="images/tombol/popup.png" id="btnpenerbit" title="Cari Penerbit" style="cursor:pointer" onClick="openLOV('btnpenerbit', 'penerbit',-390, 22,'addPenerbit',450)"></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['YEAR_PUBLISH']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('tahunterbit',$rset['tahunterbit'],'ControlStyle',4,4,$c_edit,'onkeydown="return onlyNumber(event,this,false,true)"'); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['EDITION']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('edisi',$rset['edisi'],'ControlStyle',100,66,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['ISBN']; ?></td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextBox('isbn',$rset['isbn'],'ControlStyle',100,66,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><?= $form['DESC']; ?> </td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;"><?= UI::createTextArea('keterangan',$rset['keterangan'],'ControlStyle',3,50,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;">Cover</td>
						<td style="padding-top:5px;" width="2%">:</td>
						<td style="padding-top:5px;">
							<?php if($rkey){ ?>
							<img src="<?= Config::coverUrl.trim($rkey).'.jpg'; ?>" width="40%">
							<?php }?>
							<input type="file" name="coverp" id="coverp" size="30" style="cursor:pointer" ></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"></td>
						<td style="padding-top:5px;"></td>
						<td style="padding-top:5px;">&nbsp;&nbsp;
						<span name="simpan" class="buttonSubmit btn btn-success" value="Simpan" onclick="goSimpan()" style="cursor:pointer;"><?= $form['SAVE']; ?></span>
						<span name="reset" class="buttonSubmit btn btn-primary" value="Reset" style="cursor:pointer" onclick="goUndo()"><?= $form['RESET']; ?></span></td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
				<input type="hidden" name="redit" id="redit" value="<?= $rkey; ?>" />
			</div>
		</div>
	</form>
	<iframe name="upload_iframe" style="display:none;"></iframe>
	<br />
	<div class="profile">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<h4 class="sidebar-title"><?= $form['T_HISREQUESTPUBLIC']?></h4>
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
							<tr>
								<th width="75" align="center"><?= $form['DATE_REQUEST']; ?></th>
								<th width="165" align="center"><?= $form['BIBLIO_TITLE']; ?></th>
								<th width="103" align="center"><?= $form['AUTHOR']; ?></th>
								<th width="95" align="center"><?= $form['PUBLISHER']; ?></th>
								<th width="89" align="center"><?= $form['REQ_DATE']; ?></th>
								<th width="89" align="center"><?= $form['ACTION']; ?></th>
							</tr>
							<? 
								$i=0;
								while ($row = $rs->FetchRow()){ $i++;
							?>
							<tr>
								<td align="center" valign="top"><?= Helper::formatDate($row['tglusulan'])?></td>
								<td valign="top"><?= $row['judulpustaka']?></td>
								<td valign="top"><?= $row['pengarang'];?></td>
								<td valign="top"><?= $row['namapenerbit'];?></td>
								<td valign="top" align="center">
								<? 
									if ($row['statususulan'] != '0'){
										if ($row['ststtb'] == 1){
											echo "<img src='images/tombol/realisasi.png' title='Realisasi'>"; 
										}else{
											if ($row['stspengadaan'] == 1)
												echo "<img src='images/tombol/A.gif' title='Verifikasi Perpustakaan'>";
											else
												echo "<img src='images/tombol/PO.png' title='Pembuatan PO'>"; 
										}
									}else
										echo "<img src='images/tombol/delay.png' title='Menunggu'>";
								?>
								</td>
								<td><? if ($row['statususulan'] != '0'){ ?><img src="images/tombol/disedited.gif" title="Tidak bisa Edit"><? }else{ ?><img src="images/tombol/edited.gif" style="cursor:pointer" onclick="goEdit('contents','<?= $i_phpfile?>','&redit=<?= $row['id'];?>')" title="Edit"><span style="position:relative;bottom:8px;"> |</span> <img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelete('contents','<?= $i_phpfile?>','&redit=<?= $row['id'];?>')" title="Hapus"><? } ?></td>
							</tr>
							<? } if ($i == 0) {?>
							<tr>
								<td colspan="6" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
							<tr height="20">
								<td align="left" colspan="6"><strong><?= $form['PAGE']; ?></strong>&nbsp;:&nbsp;
										<?	if($p_page == 0)
												echo '&nbsp;';
											else {
												$start = (($p_page > 9) ? ($p_page-9) : 1);
											$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
												$link = array();
												
												for($i=$start;$i<=$end;$i++) {
													if($i == $p_page)
														$link[] = '<strong>'.$i.'</strong>';
													else{
														if (!isset($_REQUEST['isadv']))
															$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
													}
												}
												echo implode(' ',$link);
											}
										?>
										&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="profile" style="padding-top:0px;">
		<div class="contentContainer">
			<table width="100%" cellpadding="4" cellspacing="0" class="containerTable" style="margin-top:0px;">
				<tr>
					<th style="border-bottom:none;" width="75" colspan="3"><?= $form['DESC']; ?></th>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/delay.png"></td>
								<td width="1">:</td>
								<td><?= $form['Wait'] ?></td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/PO.png"></td>
								<td width="1">:</td>
								<td><?= $form['PO'] ?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/A.gif"></td>
								<td width="1">:</td>
								<td><?= $form['verpus'] ?></td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td width="25px" align="center"><img src="images/tombol/realisasi.png"></td>
								<td width="1">:</td>
								<td><?= $form['realisasi'] ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>

<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goSimpan(){
		if (cfHighlight('judul,hargausulan,authorfirst1')){
			//goSave('contents','<?= $i_phpfile?>','');
			var formid = "perpusform";
			document.getElementById(formid).target = "upload_iframe";
			document.getElementById("act").value = "simpan";
			document.getElementById(formid).submit();
		}
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
</script>
</html>
