<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	
	require_once('classes/breadcrumb.class.php');
	
	//setting untuk halaman
	$p_window = "[Digilib] E-Journals &amp; E-Books";
	
	$b_link = new Breadcrumb();
	$b_link->add('E-Journals &amp; E-Books', Helper::navAddress('list_jurnal.php'), 1);
	
	$bln = date('m');
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
<script type="text/javascript" src="script/coin-slider.js"></script>
<link rel="stylesheet" href="">

</head>

<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<?= $b_link->output(); ?>				
	<div class="linkTerkait">
		<div class="contentContainer">
			<h4 class="sidebar-title">E-Journals &amp; E-Books</h4>
			<? include('journal.html') ?>
		</div>
	</div>

</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
	
</body>

<script type="text/javascript"> 
var outtime = 20000;
	$(document).ready(function(){
		$("span[rel]").overlay();
		$('#coin-slider').coinslider({width:400,height:150,navigation:true,links:false,delay:4000,spw:9,sph:5,sDelay:1});	
		
	});
	
</script>
</script>	
</html>
