<?php
//$file = 'uploads/file/testt/helloworld.pdf';
defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

//echo $_SERVER['HTTP_REFERER'];
//var_dump($_SERVER);
//die();
//die(Config::webUrl);

/*if($_SERVER['HTTP_REFERER'] != Config::webUrl."?page=home"){
    header('Location: '. Helper::navAddress('index.php'));
    exit();
}*/

$file = urldecode($_REQUEST['file']); 
$noseri = urldecode($_REQUEST['kode']);

$open = Perpus::isOpen($noseri,$file); 
if($open == 1){
	if(!$_SESSION[idanggota]){
		header('Location: '. Helper::navAddress('index.php'));
		exit();
	}
}

$down = Perpus::isDownload($noseri,$file);
if($down=="1")
	$_SESSION['xfiledownd'] = $file;
// $fp = @fopen(preg_replace("/\[(.*?)\]/i", "",$file),"r");
if (!$fp = @fopen($file,"r")) {
        echo 'failed opening file ';
}
else {
	
        $max=0;
        while(!feof($fp)) {
                $line = fgets($fp,255);//die($line);
                if (preg_match('/\/Count [0-9]+/', $line, $matches)){
                        preg_match('/[0-9]+/',$matches[0], $matches2);
                        if ($max<$matches2[0]) $max=$matches2[0];
                }
        }
        fclose($fp);
	
	if($max==0){
	    $im = new imagick($file);
	    $max=$im->getNumberImages();
	}
}

    
?>

<!doctype html>
<html>

<head>
  <script type="text/javascript" src="pdfshow/src/shared/util.js"></script>
  <script type="text/javascript" src="pdfshow/src/shared/colorspace.js"></script>
  <script type="text/javascript" src="pdfshow/src/shared/function.js"></script>
  <script type="text/javascript" src="pdfshow/src/shared/annotation.js"></script>
  <script type="text/javascript" src="pdfshow/src/display/api.js"></script>
  <script type="text/javascript" src="pdfshow/src/display/metadata.js"></script>
  <script type="text/javascript" src="pdfshow/src/display/canvas.js"></script>
  <script type="text/javascript" src="pdfshow/src/display/pattern_helper.js"></script>
  <script type="text/javascript" src="pdfshow/src/display/font_loader.js"></script>
  <script type="text/javascript" src="pdfshow/showfile.js"></script>
  <script type="text/javascript">
    PDFJS.workerSrc = './pdfshow/src/worker_loader.js';
    
    function loadfirst(){
      document.getElementById("hal").value = 1;
      goPage();
    }
    
    function goPage(){
	npage = document.getElementById("hal").value;
	showFile(npage);
    }
    
    function goFirst(){
	document.getElementById("hal").value = "1";
	showFile("1");
    }
    
    function goPrev(){
	npage = parseInt(document.getElementById("hal").value);
	if(npage>1)
		np = npage-1;
	else
		np = npage;
		
	document.getElementById("hal").value = np;
	showFile(np);
    }
    
    function goNext(){
	npage = parseInt(document.getElementById("hal").value);
	if(npage < parseInt('<?=$max;?>'))
		np = npage+1;
	else
		np = npage;
		
	document.getElementById("hal").value = np;
	showFile(np);
    }
    
    function goLast(){
	document.getElementById("hal").value = <?=$max;?>;
	showFile(<?=$max;?>);
    }
        
    function showFile(npage){ 
      nfile = '<?=Config::webUrl?>?page=test&file=<?=urlencode($file);?>&kode=<?=urlencode($noseri);?>';
      showTime(npage,nfile);
    }
    
<?if($down){?>
	function dload(){
		window.open("<?= Helper::navAddress('down.php')?>","_blank");
	}
<?}?>
    
  </script>
  <link href="images/favicon.ico" rel="shortcut icon" />
<title>View Download File</title>
<link type="text/css" rel="stylesheet" href="style/bootstrap.css" />
</head>

<body onload="loadfirst();" style="background-color: #ddd;">
	<div align="center">
<?php
if ($fp = @fopen($file,"r")) {
?>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border-bottom:5px solid #CC0000; color:#fff; padding:0 10px;margin:0;position:fixed;top:0px;left:0px;background:#003366; height:45px">
			<tr>
				<td><img src="images/logo_.png" alt="" height="45px"></td>
				<td align="right" valign="middle" style="padding-top:4px; padding-right:10px;" class="form-inline input-group-sm">
					<label>Go To Page : </label>
					<input type="text" name="hal" id="hal" maxlength="3" class="form-control" style="width:40px">
					<button class="btn btn-success btn-sm" onclick="goPage()">Go</button>
					<label>From <?=$max;?> Page<?=($max>1?"s":"");?>&nbsp;</label>
					<button class="btn btn-primary btn-sm" id="firstButton" onClick="goFirst()"><span class="glyphicon glyphicon-step-backward"></span></button>
					<button class="btn btn-primary btn-sm" id="prevButton" onClick="goPrev()"><span class="glyphicon glyphicon-arrow-left"></span></button>
					<button class="btn btn-primary btn-sm" id="nextButton" onClick="goNext()"><span class="glyphicon glyphicon-arrow-right"></span></button>
					<button class="btn btn-primary btn-sm" id="lastButton" onClick="goLast()"><span class="glyphicon glyphicon-step-forward"></span></button>
					<?if($down){?>
						<button class="btn btn-success btn-sm" id="saveButton" onClick="dload()"><span class="glyphicon glyphicon-download"></span> Download</button>
					<?}?>
				</td>
			</tr>
		</table>
		<br/>
		<div align="center" style="margin-top:45px">
			<canvas id="the-canvas" style="box-shadow: 0px 0px 5px #888888; width: 700; height: 900;" />
		</div>
<?php } ?>
	</div>
</body>

</html>
