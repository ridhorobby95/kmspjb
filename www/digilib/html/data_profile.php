<? 
	$conn->debug=false;
	define('BASEPATH',1);
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once("classes/perpus.class.php");
	require_once('classes/breadcrumb.class.php');
	
	require_once('../application/helpers/x_helper.php');
	
	$b_link = new Breadcrumb();
	$b_link->add($menu['Profile'], Helper::navAddress('data_profile.php'), 0);
	
	if(empty($_SESSION[Config::G_SESSION]['iduser'])){
		echo '<script type="text/javascript">location.reload(true);</script>';
		exit();
	}
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting utnuk halaman
	$p_window = "[Digilib] Edit Profil";
	$p_title = "Edit Profile";
	$p_titlepass = "Ganti Password";
	
	$p_dbtable = 'ms_anggota';
	
	if (isset($_POST)){
		$r_act = Helper::removeSpecial($_POST['act']);
		
		if ($r_act == 'simpan'){
			$record = array();
			$record = Helper::cStrFill($_POST);
			Helper::Identitas($record);		
				
			if (isset($_SESSION['skors']))
				$confirm = '<div align="center" style="padding-top:20px;">'.UI::message('Anda Terkena Skorsing', true).'</div>';	
			else		
				Perpus::updateProfile($conn,$record,$_SESSION['idanggota']);
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Gagal',true).'</div>';	
			else
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Berhasil').'</div>';	
		}
	}
	
	//riwayat peminjaman
	$sql = "select a.namaanggota, a.idanggota, s.namasatker, a.idunit, j.namajenisanggota, a.jk, a.alamat, a.telp, a.kota, a.kodepos, a.email, a.hp 
		from $p_dbtable a
		left join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota
		left join ms_satker s on s.kdsatker = a.idunit 
		where idanggota='".$_SESSION['idanggota']."'";
	$row = $conn->GetRow($sql);
	
	$gambar = Perpus::getFoto2($conn,$_SESSION['idanggota']);
	
	$p_foto = Config::dirFoto.'anggota/'.trim($row['idanggota']).'.jpg';
	$p_hfoto = Config::fotoUrl.'anggota/'.trim($row['idanggota']).'.jpg';
	$p_mhsfoto = Config::dirFotoMhs.trim($row['idanggota']).'.jpg';
	$p_pegfoto = Config::dirFotoPeg.trim($row['idanggota']).'.jpg';
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<form name="frmProfile" id="frmProfile" action="<?= $i_phpfile?>" method="post">

		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
				<h4 class="sidebar-title"><?= $form['EDIT_PROFILE']?></h4>
        		<?= (!$confirm) ?  '' : $confirm; ?><br>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td width="150px" style="padding-top:5px;"><strong><?= $form['MEMBER_NAME']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['namaanggota'] ?></td>
						<td rowspan="9" width="200" align="center" valign="top" style="padding-top:5px;">
						<img border="1" id="imgfoto" src="<?= Config::pictProf.xEncrypt($row['idpegawai']).'&s=300'; ?>" width="150" height="200">
						</tr>
					<tr>
						<td style="padding-top:5px;"><strong>Unit</strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['namasatker']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['MEMBER_TYPE']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['namajenisanggota']?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['GENDER']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['jk'] == 'P' ? 'Perempuan' : 'Laki-Laki'; ?></td>
					</tr>
					<tr>
						<td valign="top" style="padding-top:5px;"><strong><?= $form['ADDRESS']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['alamat']//UI::createTextArea('alamat',$row['alamat'],'ControlStyle',3,32,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['CITY']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['kota'] //UI::createTextBox('kota',$row['kota'],'ControlStyle',50,43,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['POSTAL_CODE']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['kodepos']//UI::createTextBox('kodepos',$row['kodepos'],'ControlStyle',6,5,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['TELP']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['telp']//UI::createTextBox('telp',$row['telp'],'ControlStyle',15,12,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['HP']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['hp']//UI::createTextBox('hp',$row['hp'],'ControlStyle',50,43,$c_edit); ?></td>
					</tr>
					<tr>
						<td style="padding-top:5px;"><strong><?= $form['EMAIL']; ?></strong></td>
						<td style="padding-top:5px;">:&nbsp;<?= $row['email']//UI::createTextBox('email',$row['email'],'ControlStyle',50,43,$c_edit); ?></td>
					</tr>
				</table>
				<input type="hidden" name="act" id="act" />
			</div>
		</div>
	</form>
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/pajhash.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">	
	$(function(){
		$("#frmProfile").find("#kodepos,#telp,#hp").onlyNum();
	});
	
	function goSave()
	{
		sent = $("#frmProfile").serialize();
		sent += "&act=simpan";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function procGanti() {
		// pengecekan konfirmasi password baru
		if(document.getElementById("passbaru1").value != document.getElementById("passbaru2").value) {
			document.getElementById("ret_div").innerHTML = '<font color="red">Password baru dan konfirmasinya tidak sama</font>';
			document.getElementById("passbaru1").value = "";
			document.getElementById("passbaru2").value = "";
			document.getElementById("passbaru1").focus();
			return false;
		}
		
		file = "<?= Helper::navAddress('functionx.php'); ?>";
				
		$.post(file,'f=salt',function(salt) {
			if(salt == '-1') {
				$("#ret_div").html('<font color="red">Ada masalah pada proses penggantian password</font>');
				return false;
			}
						
			//pl = hex_md5($("#passlama").val()+salt);
			//pb = hex_md5($("#passbaru1").val()+salt);
			pl = $("#passlama").val();
			pb = $("#passbaru1").val();
						
			$.post(file,'f=gantipw&p1='+pl+'&p2='+pb,function(retlog) {
				if(retlog == 1) {
					$("#ret_div").html('<font color="#339933">Penggantian password sukses</font>');
				}
				else if(retlog == -1) {
					$("#ret_div").html('<font color="red">Data user tidak valid</font>');
				}
				else if(retlog == -2) {
					$("#ret_div").html('<font color="red">Password lama tidak tepat</font>');
				}
				else if(retlog == -3) {
					$("#ret_div").html('<font color="red">Penggantian password gagal</font>');
				}
				
				$("#passlama").val("");
				$("#passbaru1").val("");
				$("#passbaru2").val("");
			});
			$("#ret_div").html('<img src="images/progressbar.gif">');
		});
		
		return false;
	}
</script>
</html>
