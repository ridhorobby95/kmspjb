<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['Service'], Helper::navAddress('data_layanan.php'), 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] Data Layanan";	
	$p_title = "Layanan";
	$ket = $conn->GetOne("select keteranganen from pp_tools where idtools = '2' and tampil='1' ") ;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	
	<?= $b_link->output(); ?>
	<div class="profile" id="start">
		<div class="contentTop"></div>
		<div class="contentContainer">
			<?=$ket;?>
		</div>
	</div>
	
</div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
</html>
