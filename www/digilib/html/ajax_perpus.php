<?php

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	define( '__PERPUS_ROW_LIMIT', 10 );
	
	//$conn->debug = false;
	
	error_reporting(0);

	// pengecekan tipe session user
	$a_auth = Helper::checkRoleAuth($conng, 'home');

    if ($_GET['act']) {
		$act = Helper::removeSpecial($_GET['act']);
		
		switch ($act) {
			case 'anggotapagu':
			case 'penerbit':
				$act($act);
				break;
			case 'dospem':
				$act($act);
				break;
			case 'newpengaranglov':
				_newPengarangLOV();
				break;
			default:
				if (preg_match('/prev_/', $act) || preg_match('/next_/', $act) || preg_match('/reset_/', $act)) {
					_getNavigation($act);
				}
				else
					echo "Invalid Request";
		}
    }
    exit;

	function _getNavigation($act) {
		$prefix = explode('_', $act);
		$navigation = $prefix[0];
		$fungsi = $prefix[1];
		if ($prefix[0] === 'prev') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] - 10;
	
			if ($_SESSION["perpus_{$fungsi}_offset"] < 0)
				$_SESSION["perpus_{$fungsi}_offset"] = 0;			
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'next') {
			if (!$_SESSION["perpus_{$fungsi}_offset"])
				$_SESSION["perpus_{$fungsi}_offset"] = 0;
			
			$_SESSION["perpus_{$fungsi}_offset"] = $_SESSION["perpus_{$fungsi}_offset"] + 10;
	
			return $fungsi($fungsi);
		}
		elseif ($prefix[0] === 'reset') {
			unset($_SESSION["perpus_{$fungsi}_offset"]);
			unset($_SESSION["perpus_{$fungsi}_keyword"]);
	
			return $fungsi($fungsi);
		}
	}

	function _retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}
	
	function penerbit($act){
		$fungsi = $act;
		
        $conn = Factory::getConn(); //$conn->debug = true;
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));
		//echo 'Keyword : '.$keyword;die();
		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ms_penerbit ";
				
		if ($keyword != '') {
			//$sql .= " where lower(namapenerbit) like '%{$keyword}%' or lower(idpenerbit::text) like '%{$keyword}%' ";
			$sql .= " where lower(namapenerbit) like '%{$keyword}%' or lower(cast(idpenerbit as varchar2(10))) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select * from ms_penerbit";
		if ($keyword != '') {
			//$sql .= " where lower(namapenerbit) like '%{$keyword}%' or lower(idpenerbit::text) like '%{$keyword}%' ";
			$sql .= " where lower(namapenerbit) like '%{$keyword}%' or lower(cast(idpenerbit as varchar2(10))) like '%{$keyword}%' ";
		}
		$sql .=  " order by namapenerbit";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">KODE</th><th>PENERBIT</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idpenerbit']}</td>";
					$ret .= "<td>{$row['namapenerbit']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['namapenerbit']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function dospem($act){
		$fungsi = $act;
		
        $conn = Factory::getConnSdm();
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ms_pegawai where isdosen = '-1'";
				
		if ($keyword != '') {
			$sql .= " where lower(nama) like '%{$keyword}%' or lower(nik) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select *, sdm.f_namalengkap(gelardepan,namadepan,namatengah,namabelakang,gelarbelakang) as namalengkap from ms_pegawai where isdosen = '-1'";
		if ($keyword != '') {
			$sql .= " where lower(nama) like '%{$keyword}%' or lower(nik) like '%{$keyword}%' ";
		}
		$sql .=  " order by nik";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"80\">NPK</th><th>NAMA DOSEN</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['nik']}</td>";
					$ret .= "<td>{$row['namalengkap']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['namalengkap']}','{$row['nik']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,'') . $ret;
		
        echo $ret;
	}
	
	function anggotapagu($act){
		$fungsi = $act;
		
        $conn = Factory::getConn();
		
        $entry = $_POST['entry'];
        if ($entry === '1') {
			$_SESSION["perpus_{$fungsi}_offset"] = 0;
			$_SESSION["perpus_{$fungsi}_keyword"] = $_POST['keyword'];
		}
		$keyword = $_SESSION["perpus_{$fungsi}_keyword"]?$_SESSION["perpus_{$fungsi}_keyword"]:'';
		$keyword = strtolower(Helper::removeSpecial($keyword));

		if ($_POST['ret_function'])
			$_SESSION["perpus_{$fungsi}_retfunction"] = $_POST['ret_function'];
		
		$ret_function = $_SESSION["perpus_{$fungsi}_retfunction"];
		$periode = $_POST['add'];
		
		$rows = array();
		$offset = 0;
		$ret = "<table class=\"dataGrid\" width=\"100%\">";

		$sql = "select count(*) from ms_anggota a
				left join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota and setpagu=1 
				left join pp_pagulh g on g.idanggota=a.idanggota and periodeakad='$periode' ";
				
		if ($keyword != '') {
			$sql .= " where lower(a.namaanggota) like '%{$keyword}%' or lower(a.idanggota) like '%{$keyword}%' ";
		}
		$tot = $conn->GetOne($sql);

		$offset = $_SESSION["perpus_{$fungsi}_offset"]?$_SESSION["perpus_{$fungsi}_offset"]:0;
		if ($offset > $tot) {
			$offset = floor(($offset-10)/10) * 10;
			$_SESSION["perpus_{$fungsi}_offset"]=$offset;
		}
		
		$sql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ( ";
		$sql .= "select a.*,g.sisasementara from ms_anggota a
				join lv_jenisanggota j on a.kdjenisanggota=j.kdjenisanggota and setpagu=1 
				left join pp_pagulh g on g.idanggota=a.idanggota and periodeakad='$periode' ";
				
		if ($keyword != '') {
			$sql .= " where lower(a.namaanggota) like '%{$keyword}%' or lower(a.idanggota) like '%{$keyword}%' ";
		}
		$sql .=  " order by namaanggota";
		$sql .=  ")  inner_query WHERE rownum <=" . __PERPUS_ROW_LIMIT . ")";

		$ret .=  "<table class=\"dataGrid\" width=\"100%\">";
		$ret .=  "<tr><th width=\"100\">ID ANGGOTA</th><th>NAMA</th><th width=\"40\">&nbsp;</th></tr>";

		$rs = $conn->Execute($sql);
		$i = 0;
		if ($rs->RecordCount()) {
			foreach($rs as $row) {
				$i++;
				$class = ($i%2===1)?"class=\"odd\"":"";
				$ret .= "<tr {$class}>";
				if ($ret_function) {
					$ret .= "<td>{$row['idanggota']}</td>";
					$ret .= "<td>{$row['namaanggota']}</td>";
					$ret .= "<td><input type=\"button\" style=\"width:30px; cursor:pointer;\" value=\"OK\" class=\"ControlStyle\" onclick=\"{$ret_function}('{$row['idanggota']}','{$row['namaanggota']}','{$row['sisasementara']}');closePop();resetLOV('$act');\"></td>";
				}
				$ret .= "</tr>";
			}
		}
		if ($i==0) {
			$ret .= "<tr><td colspan=\"3\" align=\"center\">Tidak ada data</td></tr>";
		}
		
		$ret .= "</table>";
		$ret = _headerLOV($act,$periode) . $ret;
		
        echo $ret;
	}
	
	function _headerLOV($act, $add, $extra_function) {
		$keyword = $_SESSION["perpus_{$act}_keyword"];
		
		$ret = "<table class=\"popWindowtr\" width=\"100%\" cellpadding=\"3\" cellspacing=\"0\"><tr><td>".
		$ret .= "<form style=\"float:left\">&nbsp;<input type=\"text\" id=\"sevimalov_keyword\" name=\"sevimalov_keyword\" value=\"$keyword\" size=\"25\" />
			<input type=\"submit\" style=\"cursor:pointer\" onclick=\"searchLOV('$act','$add');return false;\" value=\"Cari\"  />
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('reset_'+'$act','$add')\" title=\"Reset\" value=\"Reset\" > &nbsp; 
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('prev_'+'$act','$add')\" title=\"Previous\" class=\"btn_prev\">
			<input type=\"button\" style=\"cursor:pointer\" onclick=\"navigateLOV('next_'+'$act','$add')\" title=\"Next\" class=\"btn_next\">";
		if ($extra_function) {
			$ret .= " &nbsp; <input type=\"button\" id=\"btn_{$extra_function['nama']}\" onclick=\"{$extra_function['nama']}()\" class=\"ControlStyle\" title=\"{$extra_function['label']}\" value=\"{$extra_function['label']}\">";
		}
		$ret .=	"</form>";
		$ret .= "<span style=\"float:right\">".
				"<input type=\"button\" class=\"btn_close\" style=\"cursor:pointer\" onclick=\"closePop()\"/>".
				"</span>";
		$ret .= "</td></tr></table>";
		return $ret;
	}

	function _newPengarangLOV() {
		global $conn;
		
		$record = array();
		$record['namadepan'] = Helper::removeSpecial($_POST['namadepan']);
		$record['namabelakang'] = Helper::removeSpecial($_POST['namabelakang']);
		Helper::Identitas($record);
		
		//$col = $conn->SelectLimit("select * from ms_author",1);
		$col = $conn->Execute("select * from ms_author where 1=-1"); 
		$sql = $conn->GetInsertSQL($col,$record);
		$conn->Execute($sql);
		if ($conn->ErrorNo())
			echo '-1';
		
	}
	
?>