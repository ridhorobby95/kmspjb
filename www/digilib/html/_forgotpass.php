<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add('Forgot Password', Helper::navAddress('_forgotpass.php'), 0);
	
	//untuk halaman web
	$p_window = '[DIgilib] Lupa Password';
	$p_title = $form['FORGOT_PASS'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_title; ?></title>
</head>
<body>
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer" style="padding-bottom:10px;">
			<span class="menuTitle" style="margin-left:-20px;"><?= $p_title; ?></span>
			<br />
			<table width="100%" cellpadding="4" cellspacing="0">
				<tr>
					<td colspan="3" valign="top" height="30"><div id="wait_div" style="padding-left:15px;font-weight:bold;"></div></td>
				</tr>
				<tr>
					<td valign="top" colspan="3" style="padding-left:15px;padding-bottom:15px">
						<strong><?= $form['FP_TEXT']?></strong>
					</td>
				</tr>
				<tr>
					<td width="340">
						<table width="100%" height="56" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><span style="padding-left:15px;"><strong><?= $form['EMAIL_ADDRESS']?></strong></span></td><td width="10">:</td>
								<td height="20"><?= UI::createTextBox('email','','controlStyle',50,50,'onkeydown="etrLupa(event)"'); ?></td>
							</tr>
							<tr>
								<td rowspan="3" colspan="2"><img src="includes/kcaptcha/index.php?<?= mt_rand();?>" title="Ulangi validasi" id="captcha" style="cursor:pointer" onClick="this.src='includes/kcaptcha/index.php?'+Math.random();"></td>
								<td height="20" valign="bottom"><strong><?= $form['VAL_CODE']?></strong></td>
							</tr>
							<tr>
								<td><?= UI::createTextBox('validasi','','controlStyle',50,50,'onkeydown="etrLupa(event)"'); ?></td>
							</tr>
							<tr>
								<td style="padding-top:5px;">&nbsp;&nbsp;<span name="simpan" class="buttonSubmit" value="Simpan" onclick="procLupa()" style="cursor:pointer;"><?= $form['SENT'] ?></span>&nbsp;&nbsp;<span name="reset" class="buttonSubmit" value="Simpan" onclick="goReset()" style="cursor:pointer;"><?= $form['RESET'] ?></span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
		</div>
</body>
<script type="text/javascript">
	function goReset(){
		$("input[type='text']").val("");
	}
	
	function etrLupa(e) {
		var ev= (window.event) ? window.event: e;
		var key = (ev.keyCode) ? ev.keyCode : ev.which;
		
		if (key == 13)
			procLupa();
	}
	
	function procLupa() {	
		// pengecekan email
		var email = document.getElementById("email").value.replace(/^\s+|s\+$/g,'');
		if(email == "") {
			document.getElementById("wait_div").innerHTML = '<font color="red"><?= $form['FP_EMAIL']?></font>';
			document.getElementById("email").focus();
			return false;
		}
		
		file = "<?= Helper::navAddress('functionx.php')?>";
		$.post(file,'f=salt',function(salt) {
			if(salt == '-1') {
				$("#wait_div").html('<font color="red"><?= $form['FP_PROBLEM']?></font>');
				return false;
			}
	
			e = $("#email").val();
			v = $("#validasi").val();
			
			$.post(file,'f=lupapw&p1='+e+'&p2='+v,function(retlog) {
				if(retlog == 1) {
					$("#wait_div").html('<font color="#339933"><?= $form['FP_SENT']?> (' + e + ')</font>');
					$("#email").val("");
					$("#validasi").val("");
					document.getElementById("email").focus();
					document.getElementById("captcha").src = 'includes/kcaptcha/index.php?'+Math.random();
				}
				else if(retlog == -1) {
					$("#wait_div").html('<font color="red"><?= $form['FP_VALERROR']?></font>');
				}
				else if(retlog == -2) {
					$("#wait_div").html('<font color="red"><?= $form['FP_EMAILERROR']?></font>');
				}
				else if(retlog == -3) {
					$("#wait_div").html('<font color="red"><?= $form['FP_RESETERROR']?></font>');
				}
				//$("#ret_div").html(retlog);
			});
			$("#wait_div").html('<img src="images/load.gif">');
		});
		
		return false;
	}
</script>
</html>
