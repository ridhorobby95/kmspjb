<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
//$conn->debug = true;		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	require('classes/pdf.class.php');
	require('classes/fts.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['B_FORMTA'], Helper::navAddress('data_uploadta.php'), 0);
	// $conn->debug = true;
	// var_dump($_SESSION);
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_UPLOAD'];
	$p_title = $form['T_UPLOAD'];
	$p_dbtable = "pp_uploadta";
	$userlogin = $_SESSION['idanggota'];
	//cek di tabel pp_uploadta
	$sql_cek_mhs = "select * from pp_uploadta where idanggota='$userlogin'";
	$rs_cek = $conn->GetRow($sql_cek_mhs);
	
	//$lokfile = '../../perpus/uploads/uploadta/';

	//cek apakah termausk contributor dua atau tiga?
	$sql_cont = "select * from pp_uploadta where nrp2='".$_SESSION['idanggota']."' or nrp3='".$_SESSION['idanggota']."'";
	$rs_cont = $conn->GetRow($sql_cont);
	
	$iduploadta = $rs_cek['iduploadta']?$rs_cek['iduploadta']:$rs_cont['iduploadta'];
	
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']);
		
		$maxfile = Helper::removeSpecial($_POST['theValue']);

		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$istrue = false;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){ 
			$record = array();
			$conn->StartTrans();
			$record = Helper::cStrFill($_POST);
			$record['idanggota'] = $_SESSION['idanggota'];
			$record['tgluploadta'] = date('Y-m-d');
			Helper::Identitas($record);

			if (empty($rs_cek)) // insert
				Perpus::Simpan($conn, $record, $p_dbtable);
			else // update
				Perpus::Update($conn, $record, $p_dbtable, " iduploadta=".$rs_cek['iduploadta']);
			
			//simpan file upload ta
			$iduploadta = $conn->GetOne("select iduploadta from pp_uploadta where idanggota='$userlogin'");
			for ($i = 0; $i <= $maxfile; $i++){
				$r_file = array();
				//$test=($i-1)+2;
				//$field = "file".$test;
				$tmp_file = $_FILES['userfile']['tmp_name'][$i];
				$filetype = $_FILES['userfile']['type'][$i];
				$filesize = $_FILES['userfile']['size'][$i];
				$filename = $_FILES['userfile']['name'][$i];
				$ext=explode(".",$filename);
				$extjum=count($ext);
				$eksten=$ext[$extjum-1];
				
				#NIM_NAMA_JENIS
				//if($filename)
					//$fname = $_SESSION['idanggota']." ".$_SESSION['usernama']." ".fts::jenisfileta($i+1).".".$eksten;
					
				//$fileup=Helper::removeSpecial(str_replace(" ","_",$fname));
				$fileup=Helper::removeSpecial(str_replace(" ","_",$filename)); 
				$seri = $_SESSION['idanggota'];
				
				$seri_exp = str_replace("/","_",$seri);
				//$directori = $lokfile . $seri_exp;
				$directori = 'uploads/uploadtamandiri/' . $seri_exp;
				if(!is_dir($directori))
						mkdir($directori,0750);
					
				$destination = $directori .'/'.$fileup;
				
				if($filesize < 10000000){
					if ($filetype=='application/pdf') {
						if (copy($tmp_file,$destination))
						{					
							$r_file['file']=Helper::cStrNull($destination);
						}
					}
				}
				
				$r_file['iduploadta'] = $iduploadta;
				$r_file['login'] = "1";
				$r_file['download'] = "0";
				Helper::Identitas($r_file); //die($r_file);
				if(!empty($r_file['file']))
					Perpus::Simpan($conn, $r_file, 'uploadta_file');
				//else
					//continue;
			}
				
			if($conn->ErrorNo() != 0){
				$confirm = '0';
			}else{
				$confirm='1';
			}
			
			$conn->CompleteTrans();
//die();			
			// $rset = array();
			// $rset = $_POST;
			if($confirm2 != '2'){
				?>
			<html>
			<script type="text/javascript" src="scripts/jquery.js"></script>
			<script type="text/javascript" src="scripts/jquery.common.js"></script>
			<script src="script/linkis.js" type="text/javascript"></script>
			<script src="script/foredit.js" type="text/javascript"></script>
			<script type="text/javascript">
				var msg = 'r_confirm='+<?= $confirm?>;
				window.parent.goLoading('contents','<?= $i_phpfile?>',msg);
			</script>
			</html>
		<?php
				exit();
			}
		}
		else if ($r_act == 'hapus'){
			Perpus::Delete($conn, $p_dbtable, " idusulan=$rkey");
			
			if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan Data Gagal',true).'</div>';
			else 
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan Data berhasil').'</div>';
			
			if ($istrue)
				Helper::addHistory($conn, "Penghapusan Pengusulan Biasa Berhasil", "idusulan=".$rkey);
			else
				Helper::addHistory($conn, "Gagal Mengahapus Pengusulan Biasa", "idusulan=".$rkey);
		}
		
		/*else if($r_act =="delfile"){
			$file=Helper::removeSpecial($_POST['delfile']);
			$fkey=Helper::removeSpecial($_POST['delkey']);
			
			unlink($file);
			$fdel = explode('.',Helper::GetPath($file));
			
			//$hap = Helper::HapusFolder('upl_link/uploadtamandiri/'.$_SESSION['idanggota'].'/'.$fdel[0]);
			$recfile[$fkey]=null;
			Perpus::Update($conn, $recfile, $p_dbtable, " iduploadta=".$rs_cek['iduploadta']);
			
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan file upload gagal.',true).'</div>';
			}
			else {
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan file upload berhasil.').'</div>';
			}*/
		else if($r_act =="delfile"){
			$file=Helper::removeSpecial($_POST['delfile']);
			$fkey=Helper::removeSpecial($_POST['delkey']);
			unlink($file);
			$fdel = explode('.',Helper::GetPath($file));
			Perpus::Delete($conn, 'uploadta_file', " iduploadtafile=".$fkey);
			Perpus::Delete($conn, 'uploadta_file', " iduploadta=".$iduploadta." and file='$file' ");
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan file upload gagal.',true).'</div>';
			}
			else {
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan file upload berhasil.').'</div>';
			}
		}
		
		else if($r_act =="delkabeh"){
			$idupload=Helper::removeSpecial($_POST['id_del']);
			
			
			Perpus::Delete($conn, 'pp_historynotifikasiuploadta', " iduploadta=".$idupload);
			Perpus::Delete($conn, 'pp_historyvalidasita', " iduploadta=".$idupload);
			Perpus::Delete($conn, 'pp_uploadta', " iduploadta=".$idupload);
			
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penghapusan data gagal.',true).'</div>';
			}
			else {
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penghapusan data berhasil.').'</div>';
			}
		}
		
		else if($r_act =="requestval"){
			$id=Helper::removeSpecial($_POST['iduploadta']);
			$notif = Helper::removeSpecial($_REQUEST['pesan']);

			//insert ke table pp_historynotifikasi
			$rs_npk = $conn->GetRow("select * from pp_uploadta where iduploadta='$id'");
			$rs_notif = $conn->Execute("select * from pp_historynotifikasiuploadta where nrptujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0");
			$rs_countnotif = $conn->GetRow("select count(*) as jml from pp_historynotifikasiuploadta where nrptujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0");
			$rec_histnotif = array();
			$rec_histnotif2 = array();
			$rec_stsnotif = array();
			$rec_stsnotif2 = array();
			
			if($rs_countnotif['jml']==0){ //jika tidak ada notif dari petugas dan dospem, lakukan input notifikasi ke semua dospem+petugas
				$rec_request['tglrequestval'] = date("Y-m-d");
				$rec_request['userrequestval'] = $_SESSION['idanggota'];
				Perpus::Update($conn, $rec_request, $p_dbtable, " iduploadta=$id");			
				
				if($conn->ErrorNo() != 0){
					$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Permintaan Validasi Gagal.',true).'</div>';
				}
				else {
					$rec_histnotif = array();
					for($i=1;$i<=3;$i++){
						if(!empty($rs_npk['npkpembimbing'.$i])){
							$rec_histnotif['iduploadta'] = $id;
							$rec_histnotif['tglhistorynotifikasi'] =  date("Y-m-d H:i:s");
							$rec_histnotif['npktujuannotifikasi'] = $rs_npk['npkpembimbing'.$i];
							$rec_histnotif['pesan'] = $notif;
							$rec_histnotif['stsnotifikasi'] = 0;
							Helper::Identitas($rec_histnotif);
							Perpus::Simpan($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
						}
					}	
					// insert untuk notifikasi petugas digilitasi
					$rec_histnotif2['iduploadta'] = $id;
					$rec_histnotif2['tglhistorynotifikasi'] =  date("Y-m-d H:i:s");
					$rec_histnotif2['npktujuannotifikasi'] = 'pusdig';
					$rec_histnotif2['pesan'] = $notif;
					$rec_histnotif2['stsnotifikasi'] = 0;
					Helper::Identitas($rec_histnotif2);
					Perpus::Simpan($conn, $rec_histnotif2, 'pp_historynotifikasiuploadta');
					
					$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Permintaan Validasi Berhasil.').'</div>';
				}
			}
			else if($rs_countnotif['jml']>0){
				while($row = $rs_notif->FetchRow()){ //misal 2 record
					$k=0;
					for($i=1;$i<=3;$i++){ //dosen pembimbing	
						if($rs_npk['npkpembimbing'.$i] == $row['t_user']){
							if($rs_npk['statusvalpembimbing'.$i]!=2){												
								$rec_histnotif['iduploadta'] = $id;
								$rec_histnotif['tglhistorynotifikasi'] =  date("Y-m-d H:i:s");
								$rec_histnotif['npktujuannotifikasi'] = $rs_npk['npkpembimbing'.$i];
								$rec_histnotif['pesan'] = $notif;
								$rec_histnotif['stsnotifikasi'] = 0;
								Helper::Identitas($rec_histnotif);
								Perpus::Simpan($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
								
								//update status notifikasi menjadi 1
								$rec_stsnotif['stsnotifikasi'] = 1;
								Perpus::Update($conn, $rec_stsnotif, 'pp_historynotifikasiuploadta', " idhistorynotifikasi=".$row['idhistorynotifikasi']);		
							}$k++;
						}
					}
					if($k==0){ //jika t_user bukan salah satu dari pembimbing berarti lak petugas.
						$rec_histnotif2['iduploadta'] = $id;
						$rec_histnotif2['tglhistorynotifikasi'] =  date("Y-m-d H:i:s");
						$rec_histnotif2['npktujuannotifikasi'] = 'pusdig';
						$rec_histnotif2['pesan'] = $notif;
						$rec_histnotif2['stsnotifikasi'] = 0;
						Helper::Identitas($rec_histnotif2);
						Perpus::Simpan($conn, $rec_histnotif2, 'pp_historynotifikasiuploadta');
						
						//update status notifikasi menjadi 1
						$rec_stsnotif2['stsnotifikasi'] = 1;
						Perpus::Update($conn, $rec_stsnotif2, 'pp_historynotifikasiuploadta', " idhistorynotifikasi=".$row['idhistorynotifikasi']);
					}
				}
				if($conn->ErrorNo() != 0)
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Permintaan Validasi Gagal.',true).'</div>';
			else
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Permintaan Validasi Berhasil.').'</div>';
			}
		}
		
	}
	
	//data
	if($rs_cont){
		$sql_cek_mhs2 = "select * from pp_uploadta where idanggota='".$rs_cont['idanggota']."'";
		$rset = $conn->GetRow($sql_cek_mhs2);
		
		$sql_hist_val = "select a.namaanggota as mhs, b.namaanggota as dosen, h.* from pp_historynotifikasiuploadta h left join ms_anggota a on a.idanggota=h.t_user 
						left join ms_anggota b on b.idanggota=h.npktujuannotifikasi where iduploadta=".$rset['iduploadta'].' order by idhistorynotifikasi asc';
		$rset_val = $conn->Execute($sql_hist_val);
	}else{
		$sql_cek_mhs2 = "select * from pp_uploadta where idanggota='$userlogin'";
		$rset = $conn->GetRow($sql_cek_mhs2);
		
		$sql_hist_val = "select a.namaanggota as mhs, b.namaanggota as dosen, h.* from pp_historynotifikasiuploadta h left join ms_anggota a on a.idanggota=h.t_user 
						left join ms_anggota b on b.idanggota=h.npktujuannotifikasi where iduploadta=".$rs_cek['iduploadta'].' order by idhistorynotifikasi asc';
		$rset_val = $conn->Execute($sql_hist_val);
		
		$rset_notifikasi = $conn->GetRow("select count(*) as jml from pp_historynotifikasiuploadta where nrptujuannotifikasi='".$_SESSION['idanggota']."' and stsnotifikasi=0");
	}
	
	$sqlFile = "select iduploadtafile, file, login, download
		from uploadta_file
		where iduploadta = '".($iduploadta?$iduploadta:0)."'
		order by iduploadtafile ";
	$rsFile = $conn->GetArray($sqlFile);
	
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>
<?= $p_window ?>
</title>
<script type="text/javascript" src="script/auth.js"></script>
</head>

<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post" enctype="multipart/form-data">
  <?= $b_link->output(); ?>
  <div class="profile">
    <div class="contentTop"></div>
    <div class="contentContainer">
      <?if($rset['statusvalpembimbing1']=='1' or $rset['statusvalpembimbing2']=='1' or $rset['statusvalpembimbing1']=='1' or $rset['statusvalstafdigitalisasi']=='1'){?>
      <div class="alert alert-danger"><strong>Status:</strong> [dalam tahap verifikasi Petugas Digitalisasi]</div>
      <?}else if($rset['userrequestval']==''){?>
      <div class="alert alert-danger"><strong>Status:</strong> [Pengubahan Data Upload Tugas Akhir Mandiri]</div>
      <?}else if($rset['statusvalpembimbing1']=='2' or $rset['statusvalpembimbing2']=='2' or $rset['statusvalpembimbing3']=='2' or $rset['statusvalstafdigitalisasi']=='2'){?>
      <? $jml_pemb =0; 
	if($rset['npkpembimbing1']!='') $jml_pemb++;
	if($rset['npkpembimbing2']!='') $jml_pemb++;
	if($rset['npkpembimbing3']!='') $jml_pemb++;

	if($jml_pemb == 1){
		if($rset['statusvalpembimbing1']==2 and $rset['statusvalstafdigitalisasi']==2){ ?>
			<font color="green" size="5"><blink>Status: [Data Upload Tugas Akhir Mandiri telah di Setujui, lakukan Cetak Bebas TA]</blink></font>
      <? }else{?>
      <font color="red" size="5"><blink>Status: [dalam tahap verifikasi Petugas Digitalisasi]</blink></font>
      <?	}
			}else if($jml_pemb == 2){
				if($rset['statusvalpembimbing1']==2 and ($rset['statusvalpembimbing2']==2 or $rset['statusvalpembimbing3']==2) and $rset['statusvalstafdigitalisasi']==2){ ?>
      <font color="green" size="5"><blink>Status: [Data Upload Tugas Akhir Mandiri telah di Setujui, lakukan Cetak Bebas TA]</blink></font>
      <?			}else{?>
      <font color="red" size="5"><blink>Status: [dalam tahap verifikasi Petugas Digitalisasi]</blink></font>
      <?		}
		}
		else if($jml_pemb == 3){
			if($rset['statusvalpembimbing1']==2 and $rset['statusvalpembimbing2']==2 and $rset['statusvalpembimbing3']==2 and $rset['statusvalstafdigitalisasi']==2){ ?>
      <font color="green" size="5"><blink>Status: [Data Upload Tugas Akhir Mandiri telah di Setujui, lakukan Cetak Bebas TA]</blink></font>
      <?		}else{?>
      <font color="red" size="5"><blink>Status: [dalam tahap verifikasi Petugas Digitalisasi]</blink></font>
      <?		}
							}
						?>
      <?}?>
      <h4 class="sidebar-title"><?= $p_title?></h4>
      <? if($confirm) echo $confirm; 
	else{ 
		if($_REQUEST['r_confirm'] != ''){
			if($_REQUEST['r_confirm']=='1')
				echo '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data Berhasil.').'</div>';
			else
				echo '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal.',true).'</div>';
		}else	
			echo '';
	}
	?>
      <table width="100%" cellpadding="4" cellspacing="0" border=0>
        <tr>
          <td style="padding-top:10px; font-weight:bold;"><?= $form['BIBLIO_TITLE']; ?>
            <span class="required">*</span></td>
          <td style="padding-top:5px;" width="2%">:</td>
          <td style="padding-top:5px;">
		<? if($rs_cont) echo '&nbsp;&nbsp;'.$rset['judul']; 
		else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
			if($rset_notifikasi['jml']>0)	echo UI::createTextArea('judul',$rset['judul'],'ControlStyle form-control',3,50,$c_edit); 
			else echo '&nbsp;&nbsp;'.$rset['judul'];   
		}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0))  echo '&nbsp;&nbsp;'.$rset['judul'];   
		else echo UI::createTextArea('judul',$rset['judul'],'ControlStyle form-control',3,50,$c_edit);
		?>
	</td>
        </tr>
        <tr>
		<td style="padding-top:10px; font-weight:bold;"><?= $form['PENULIS']; ?> <span class="required">*</span></td>
		<td style="padding-top:5px;" width="2%">:</td>
		<td style="padding-top:5px;">
		<?
			if($rs_cont){
				echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
						<tr>
							<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
							<td style="padding-top:10px;" width : 10px;>:</td>
							<td style="padding-top:5px;">'.$rset['authorfirst1'].'</td>
						</tr>
						<tr>
							<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
							<td style="padding-top:10px;" width : 10px;>:</td>
							<td style="padding-top:5px;">'.$rset['authorlast1'].'</td>
						</tr>
					</table>';
			}
			else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
				if($rset_notifikasi['jml']>0){
					echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorfirst1',$rset['authorfirst1'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorlast1',$rset['authorlast1'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
						</table>';
				}
				else{
					echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.$rset['authorfirst1'].'</td>
							</tr>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.$rset['authorlast1'].'</td>
							</tr>
						</table>';
				}
			}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)){
				echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
						<tr>
							<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
							<td style="padding-top:10px;" width : 10px;>:</td>
							<td style="padding-top:5px;">'.$rset['authorfirst1'].'</td>
						</tr>
						<tr>
							<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
							<td style="padding-top:10px;" width : 10px;>:</td>
							<td style="padding-top:5px;">'.$rset['authorlast1'].'</td>
						</tr>
					</table>';
			}else{
				if($rset){
					echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorfirst1',$rset['authorfirst1'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorlast1',$rset['authorlast1'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
						</table>';
				}
				else{
					echo '<table width="100%" cellpadding="4" cellspacing="0" border=0>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Depan</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorfirst1',$_SESSION['usernama'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
							<tr>
								<td style="padding-top:10px; font-weight:bold;">Nama Belakang</td>
								<td style="padding-top:10px;" width : 10px;>:</td>
								<td style="padding-top:5px;">'.UI::createTextBox('authorlast1',$rset['authorlast1'],'ControlStyle form-control2',100,30,$c_edit).'</td>
							</tr>
						</table>';
				}
			}
		?>
	      </td>
        </tr>
        <tr>
		<td style="padding-top:10px; font-weight:bold;"><?= $form['NRP1']; ?><span class="required">*</span></td>
		<td style="padding-top:5px;" width="2%">:</td>
		<td style="padding-top:5px;">
		<? if($rs_cont) echo '&nbsp;&nbsp;'.$rset['nrp1']; 
		else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){
			if($rset_notifikasi['jml']>0) echo UI::createTextBox('nrp1',$rset['nrp1'],'ControlStyle 
			form-control2',20,25,$c_edit); 
			else echo '&nbsp;&nbsp;'.$rset['nrp1']; 
		}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0))   echo '&nbsp;&nbsp;'.$rset['nrp1'];  
		else echo UI::createTextBox('nrp1',$_SESSION['idanggota'],'ControlStyle form-control2',20,25,$c_edit); 
		?>
		</td>
        </tr>
        <tr>
		<td style="padding-top:10px; font-weight:bold;"><?= $form['PUBLISHER']; ?></td>
		<td style="padding-top:5px;" width="2%">:</td>
		<td style="padding-top:5px;">
		<? if($rs_cont) echo '&nbsp;&nbsp;'.$rset['penerbit']; 
		else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){
			if($rset_notifikasi['jml']>0)  echo UI::createTextBox('penerbit',$rset['penerbit'],'ControlStyle form-control2',100,60,$c_edit);  
			else echo '&nbsp;&nbsp;'.$rset['penerbit']; 
		}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0))  echo '&nbsp;&nbsp;'.$rset['penerbit']; 
		else echo UI::createTextBox('penerbit',$rset['penerbit'],'ControlStyle form-control2',100,60,$c_edit); 
		?>
		</td>
        </tr>
        <tr>
		<td style="padding-top:10px; font-weight:bold;"><?= $form['YEAR_PUBLISH']; ?> <span class="required">*</span></td>
		<td style="padding-top:5px;" width="2%">:</td>
		<td style="padding-top:5px;">
		<? if($rs_cont) echo '&nbsp;&nbsp;'.$rset['tahunterbit']; 
			else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
				if($rset_notifikasi['jml']>0) echo UI::createTextBox('tahunterbit',$rset['tahunterbit'],'ControlStyle form-control2',4,4,$c_edit);  
				else echo '&nbsp;&nbsp;'.$rset['tahunterbit'];
			}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)) echo '&nbsp;&nbsp;'.$rset['tahunterbit']; 
			else echo UI::createTextBox('tahunterbit',$rset['tahunterbit'],'ControlStyle form-control2',4,4,$c_edit); 
		?>
		</td>
        </tr>
        <tr>
          <td style="padding-top:10px; font-weight:bold;"><?= $form['CONTRIBUTOR1']; ?>
            <span class="required">*</span></td>
          <td style="padding-top:5px;" width="2%">:</td>
          <td style="padding-top:5px;">
	<?
	if($rs_cont)
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing1']." - ".$rset['pembimbing1']; 
	else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
		if($rset_notifikasi['jml']>0){
			echo UI::createTextBox('pembimbing1',$rset['pembimbing1'],'ControlStyle form-control2',100,35,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
            &nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem1',450)">&nbsp;&nbsp;
            </div>
            <div class='form-group'><?= $form['NPK']; ?>
	<? 		echo UI::createTextBox('npkpembimbing1',$rset['npkpembimbing1'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"'); 
	?>	
		<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('1')" title="Hapus Pembimbing 1">
	<?	
		}else
			echo '&nbsp;&nbsp;'.$rset['npkpembimbing1']." - ".$rset['pembimbing1'];
	?>
        <?
	} else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)){
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing1']." - ".$rset['pembimbing1'];
	} else{
		echo UI::createTextBox('pembimbing1',$rset['pembimbing1'],'ControlStyle form-control2',100,30,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
            &nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem1',450)"> &nbsp;&nbsp;
            <?= $form['NPK']; ?>
            &nbsp;&nbsp;:&nbsp;&nbsp;
            <?= UI::createTextBox('npkpembimbing1',$rset['npkpembimbing1'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"'); ?>
	<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('1')" title="Hapus Pembimbing 1">
	<?}?>
	
	    </td>
        </tr>
        <tr>
          <td style="padding-top:10px; font-weight:bold;"><?= $form['CONTRIBUTOR2']; ?></td>
          <td style="padding-top:5px;" width="2%">:</td>
          <td style="padding-top:5px;">
	  <?
	if($rs_cont)
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing2']." - ".$rset['pembimbing2']; 
	else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
		if($rset_notifikasi['jml']>0){
			echo UI::createTextBox('pembimbing2',$rset['pembimbing2'],'ControlStyle form-control2',100,30,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
            &nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem2',450)">&nbsp;&nbsp;
            <?= $form['NPK']; ?>
            &nbsp;&nbsp;:&nbsp;&nbsp;
        <?
			echo UI::createTextBox('npkpembimbing2',$rset['npkpembimbing2'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
		<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('2')" title="Hapus Pembimbing 2">
	<?
		}else
			echo '&nbsp;&nbsp;'.$rset['npkpembimbing2']." - ".$rset['pembimbing2'];
						?>
        <?
	}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)){
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing2']." - ".$rset['pembimbing2'];
	} else{
		echo UI::createTextBox('pembimbing2',$rset['pembimbing2'],'ControlStyle form-control2',100,30,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
		&nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem2',450)"> &nbsp;&nbsp;
		<?= $form['NPK']; ?>
		&nbsp;&nbsp;:&nbsp;&nbsp;
		<?= UI::createTextBox('npkpembimbing2',$rset['npkpembimbing2'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"'); ?>
		<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('2')" title="Hapus Pembimbing 2">
	<?
	}
	?>
	    </td>
        </tr>
        <tr>
          <td style="padding-top:10px; font-weight:bold;"><?= $form['CONTRIBUTOR3']; ?></td>
          <td style="padding-top:5px;" width="2%">:</td>
          <td style="padding-top:5px;">
	<?
	if($rs_cont)
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing3']." - ".$rset['pembimbing3']; 
	else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){ 
		if($rset_notifikasi['jml']>0){
			echo UI::createTextBox('pembimbing3',$rset['pembimbing3'],'ControlStyle form-control2',100,30,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
			&nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem3',450)">&nbsp;&nbsp;
			<?= $form['NPK']; ?>
			&nbsp;&nbsp;:&nbsp;&nbsp;
        <?
			echo UI::createTextBox('npkpembimbing3',$rset['npkpembimbing3'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"'); 
	?>
			<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('3')" title="Hapus Pembimbing 3">
	<?
		}else echo '&nbsp;&nbsp;'.$rset['npkpembimbing3']." - ".$rset['pembimbing3']; 
	?>
        <?
	}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)){
		echo '&nbsp;&nbsp;'.$rset['npkpembimbing3']." - ".$rset['pembimbing3'];
	} else{ echo UI::createTextBox('pembimbing3',$rset['pembimbing3'],'ControlStyle form-control2',100,30,$c_edit,'readonly style="background:#e0e2e3; width:40%;"');
	?>
		&nbsp;<img src="images/tombol/popup.png" id="btndospem" title="Cari Dosen Pembimbing" style="cursor:pointer" onClick="openLOV('btndospem', 'dospem',-390, 22,'addDospem3',450)"> &nbsp;&nbsp;
		<?= $form['NPK']; ?>
		&nbsp;&nbsp;:&nbsp;&nbsp;
		<?= UI::createTextBox('npkpembimbing3',$rset['npkpembimbing3'],'ControlStyle form-control2',6,10,$c_edit,'readonly style="background:#e0e2e3; width:40%;"'); ?>
		<img src="images/tombol/cross.png" style="cursor:pointer;padding-bottom:2px;" onclick="goDelPem('3')" title="Hapus Pembimbing 3">
	<?
	}
	?>
		</td>
        </tr>
        <tr>
          <td style="padding-top:10px; font-weight:bold;" width="150"><?= $form['DESC']; ?></td>
          <td style="padding-top:5px;" width="2%">:</td>
          <td style="padding-top:5px;"><? if($rs_cont) echo '&nbsp;&nbsp;'.$rset['keterangan']; 
		else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==1 or $rset['statusvalpembimbing2']==1 or $rset['statusvalpembimbing3']==1 or $rset['statusvalstafdigitalisasi']==1)){
			if($rset_notifikasi['jml']>0) echo UI::createTextArea('keterangan',$rset['keterangan'],'ControlStyle form-control',3,50,$c_edit);   
			else echo '&nbsp;&nbsp;'.$rset['keterangan']; 
		}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalpembimbing1']==0 or $rset['statusvalpembimbing2']==0 or $rset['statusvalpembimbing3']==0 or $rset['statusvalstafdigitalisasi']==0)) echo '&nbsp;&nbsp;'.$rset['keterangan']; 
		else echo UI::createTextArea('keterangan',$rset['keterangan'],'ControlStyle form-control',3,50,$c_edit); 
		?></td>
        </tr>
      </table>
      <table width="100%" cellpadding="4" cellspacing="0" border=0>
        <tr>
          <td colspan="3"><div class="profile" style="position:relative;top:-2px;"><!-- ======================================== UPLOAD PUSTAKA ============================= -->
              <table width="100%" border="0" cellspacing=0 cellpadding="5">
                <tr height=20>
                  <td class="SubHeaderBGAlt" colspan="2" align="Left"><strong>Upload File</strong></td>
                </tr>
                <tr>
                  <!--<td class="LeftColumnBG" width="150"><span style="font-weight:normal">( Max filesize = 10 MB, type: pdf, doc, png, dan jpg )</span></td>-->
		  <td class="LeftColumnBG" width="150"><span style="font-weight:normal">( Max filesize = 10 MB, type: pdf)</span></td>
                </tr>
                <tr height=20>
                  <td class="RightColumnBG">
			<input type="hidden" value="2" id="theValue" name="theValue" />
                    <?if($c_edit){?>
		    <?$i=0;?>
		    <?foreach($rsFile as $f){?>
			    <a href="<?= $f['file'] ?>">
				    <?= $f['file']!='' ? "<img src='images/attach.gif' border=0> $i. ".Helper::GetPath($f['file']) : '' ?>
			    </a>
			    <?if(!$rs_cont and $rset['userrequestval']==null){?>
				&nbsp;<img title="Hapus File TA" src="images/delete.png" style="cursor:pointer" alt="Hapus file upload" onClick="goDelFile('<?= $f['iduploadtafile'] ?>','<?= $f['file'] ?>')"><br>
				<br>
			    <?}else if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and ($rset['statusvalstafdigitalisasi'] == 1)){?>
				<? if($rset_notifikasi['jml']>0){?>
					&nbsp;<img title="Hapus File TA" src="images/delete.png" style="cursor:pointer" alt="Hapus file upload" onClick="goDelFile('<?= $f['iduploadtafile'] ?>','<?= $f['file'] ?>')"><br>
					<br>
				<?}else{ echo '<br><br>';}?>
			    <?}else{ echo "<br><br>"; }?>
		    <?$i++;?>
		    <?}?>
			<?if(!$rs_cont and $rset['userrequestval']==null){?>
			    &nbsp; &nbsp;<input type="button" name="btntext" onClick="CreateTextbox()" value="Tambah File" style="cursor:pointer"><br/>
			<?}?>
			<?if(($rset['userrequestval']!=null and $rset['tglrequestval']!=null) and ($rset['statusvalstafdigitalisasi'] == 1)){?>
			    <? if($rset_notifikasi['jml'] > 0){?>
			    &nbsp; &nbsp;<input type="button" name="btntext" onClick="CreateTextbox()" value="Tambah File" style="cursor:pointer"><br/>
			<?}}?>

			    <?if($rset['userrequestval']!=null and $rset['tglrequestval']!=null and $rset['statusvalstafdigitalisasi']==1){?>
				1. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
				2. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
				3. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
			    <?}?>
			<?if(!$rs_cont and $rset['userrequestval']==null){?>
				1. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
				2. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
				3. File Upload <input type="file" name="userfile[]" id="userfile[]" size="30" style="cursor:pointer" >&nbsp; &nbsp;<br><br>
			<?}?>
			
                    <?#endofwedit
                    }else{
			$k=0;
			foreach($rsFile as $f){
				$k++;
				echo $k.'. '.Helper::GetPath($f['file'])."<br>";
			}
		}?>
		<div id="createTextbox"></div>
		  </td>
                </tr>
              </table>
            </div></td>
        </tr>
        <tr class="navigasi">
          <td colspan="2" style="padding-top:5px"></td>
          <td align="center" style="padding-top:5px;">&nbsp;&nbsp;
            <?if(!$rs_cont and $rset['userrequestval']==null){?>
            <div class="buttonSubmit btn btn-success" value="Simpan" onClick="goSimpan()" style="cursor:pointer;">
              <?= $form['SAVE']; ?>
            </div>
            <span name="reset" class="buttonSubmit btn btn-primary" value="Reset" style="cursor:pointer" onClick="goUndo()">
            <?= $form['RESET']; ?>
            </span>
            <?}?>
            <?if(($rset['userrequestval']!=null and $rset['tglrequestval']!=null) and ($rset['statusvalstafdigitalisasi'] == 1)){?>
            <? if($rset_notifikasi['jml'] > 0){?>
            <span name="simpan" class="buttonSubmit btn btn-success" value="Simpan" onClick="goSimpan()" style="cursor:pointer;">
            <?= $form['SAVE']; ?>
            </span> <span name="reset" class="buttonSubmit btn btn-primary" value="Reset" style="cursor:pointer" onClick="goUndo()">
            <?= $form['RESET']; ?>
            </span>
            <?}}?>
	    </td>
        </tr>
      </table>
      <br>
      <br>
      <table width="100%" cellpadding="4" cellspacing="0" border=0>
        <?if($rset_val){?>
        <!--<span class="menuTitle" style="margin-left:0px;">History Notifikasi</span><br>-->
	<h4 class="sidebar-title">History Notifikasi</h4>
        <?while($row_notif = $rset_val->FetchRow()){
						if($row_notif['dosen']=='') $dosen='Petugas Digitalisasi';
						else $dosen=$row_notif['npktujuannotifikasi'].' - '.$row_notif['dosen'] ;
					?>
        <div>
          <div style="padding:5px;margin-bottom:15px">
            <div style="border-bottom:1px dashed #999;margin-bottom:5px;padding-bottom:5px"> <span style="float:left"> <?php echo 'Pengirim #'. $row_notif['t_user'].' - '.$row_notif['mhs']?><br>
              <?php echo 'Penerima #'. $dosen ?> </span> <span style="float:right;font-size:smaller"> <?php echo Helper::formatDateTime($row_notif['tglhistorynotifikasi'])?> </span>
              <div style="clear:both"></div>
            </div>
            <div>
              <? $p_foto = Config::dirFoto.'anggota/'.trim($row_notif['t_user']).'.jpg';
		$p_foto = Config::dirFoto.'anggota/'.trim($row_notif['t_user']).'.jpg';
		$p_hfoto = Config::fotoUrl.'anggota/'.trim($row_notif['t_user']).'.jpg';
		$p_mhsfoto = Config::dirFotoMhs.trim($row_notif['t_user']).'.jpg';
		$p_pegfoto = Config::dirFotoPeg.trim($row_notif['t_user']).'.jpg';
		$rs_jenisanggota = $conn->GetRow("select * from ms_anggota a left join lv_jenisanggota j on j.kdjenisanggota=a.kdjenisanggota
							where idanggota='".$row_notif['t_user']."'");
	
		$gambar = Perpus::getFoto2($conn,$row_notif['t_user']);
	?>
              <img style="float:right;border:2px solid gray" width="50" height="50" src="<?= $gambar ?>" />
	      <?/*if(is_file($p_foto)){?>
              <img style="float:right;border:2px solid gray" width="50" height="50" src="<?= $p_hfoto ?>" />
              <?}else{
			if(strchr(strtolower($rs_jenisanggota['namajenisanggota']),'dosen')!='' or strchr(strtolower($rs_jenisanggota['namajenisanggota']),'karyawan')!='' ){	
		?>
              <img style="float:right;border:2px solid gray" width="50" height="50" src="<?= $p_pegfoto ?>" />
              <? } elseif (strchr(strtolower($rs_jenisanggota['namajenisanggota']),'mahasiswa') !='') { ?>
              <img style="float:right;border:2px solid gray" width="50" height="50" src="<?= $p_mhsfoto ?>" />
              <? } else { ?>
              <img style="float:right;border:2px solid gray" width="50" height="50" src="<?= Config::fotoUrl.'default1.jpg' ?>" />
              <?}}*/?>
              <?php echo $row_notif['pesan']; ?>
              <div style="clear:both"></div>
            </div>
          </div>
        </div>
        <?}}?>
        <?if(!$rs_cont and $rset['userrequestval']==null){ //insert pertama kali
						if(!empty($rs_cek)){
					?>
        <div> Pesan Notifikasi: <br>
          <br>
          <?php echo UI::createTextArea('pesan',$_SESSION['idanggota'].' mohon verifikasi untuk judul TA "'.$rset['judul'].'"','ControlStyle form-control2',3,60,$c_edit); ?>
          <div style="clear:both"></div>
        </div>
        <?}}?>
	<??>
        <?if(($rset['userrequestval']!=null and $rset['tglrequestval']!=null) and ($rset['statusvalstafdigitalisasi'] == 1)){?>
        <? if($rset_notifikasi['jml'] > 0){?>
        <div> Pesan Notifikasi: <br>
          <br>
          <?php echo UI::createTextArea('pesan',$_SESSION['idanggota'].' mohon verifikasi untuk judul TA "'.$rset['judul'].'"','ControlStyle form-control',3,60,$c_edit); ?>
          <div style="clear:both"></div>
        </div>
        <?}}?>
	<??>
        <tr>
          <td colspan="2" style="padding-top:5px"></td>
          <td align="center" style="padding-top:5px;">&nbsp;&nbsp;
            <?if(!$rs_cont and $rset['userrequestval']==null){?>
            <?if(!empty($rs_cek)){?>
            <span name="request" class="buttonSubmit btn btn-success" value="Request Validation" onClick="goRequestVal('<?= $rset['iduploadta']?>')" style="cursor:pointer;">
            <?= $form['REQUEST']; ?>
            </span>
            <?}}?>
            <?if(($rset['userrequestval']!=null and $rset['tglrequestval']!=null) and ($rset['statusvalstafdigitalisasi'] == 1)){?>
            <? if($rset_notifikasi['jml'] > 0){?>
            <span name="request" class="buttonSubmit btn btn-success" value="Request Validation" onClick="goRequestVal('<?= $rset['iduploadta']?>')" style="cursor:pointer;">
            <?= $form['REQUEST']; ?>
            </span>
            <?}}?>
            <? $jml_pem =0; 
		if($rset['npkpembimbing1']!='') $jml_pem++;
		if($rset['npkpembimbing2']!='') $jml_pem++;
		if($rset['npkpembimbing3']!='') $jml_pem++;
	
			if($rset['statusvalstafdigitalisasi']==2){ ?>
            <span name="cetak" class="buttonSubmit btn btn-success" value="cetak_bebasta" onClick="goBebasta('<?= $rset['iduploadta']?>')" style="cursor:pointer;">
            <?= $form['PRINT']; ?>
            </span>
            <?	}?></td>
        </tr>
        <?if($_SESSION['idanggota']=='1060120'){?>
        <tr>
          <td><span name="hapus" class="buttonSubmit btn btn-success" value="Hapus" onClick="goHapusKabeh('<?= $rset['iduploadta']?>')" style="cursor:pointer;">Hapus</span></td>
        </tr>
        <?}?>
      </table>
      <input type="hidden" name="act" id="act" />
      <input type="hidden" name="redit" id="redit" value="<?= $rkey; ?>" />
      <input type="hidden" name="delfile" id="delfile">
      <input type="hidden" name="delkey" id="delkey">
      <input type="hidden" name="delseri" id="delseri">
    </div>
  </div>
  <?if(!empty($rs_cek)){?>
  <div class="profile" style="padding-top:0px;"> 
    <!--<div class="contentContainer">-->
    <table width="100%" cellpadding="4" cellspacing="0" class="containerTable" style="margin-top:0px;">
      <tr>
        <th style="border-bottom:1px;" align="left" colspan="2"><?= $form['STATE_VAL']; ?></th>
      </tr>
      <?/*?>
      <?$jml_pembimbing=0; for($i=1;$i<=3;$i++){
						if($rset['npkpembimbing'.$i]!=''){?>
      <tr>
        <td>Validasi Dosen Pembimbing
          <?= $i?></td>
        <?		if($rset['statusvalpembimbing'.$i]=='2'){ //disetujui ?>
        <td><img src="images/centang.gif"> <font color="green">Setuju</font></img></td>
        <?		}else
							echo '<td><font color="red">Belum disetujui</font></td>';
						}?>
      </tr>
      <?}?>
      <?*/?>
      <tr>
        <td>Validasi Petugas Digitalisasi</td>
        <?if($rset['statusvalstafdigitalisasi'] == 2){?>
        <td><img src="images/centang.gif"> <font color="green">Setuju</font></img></td>
        <?}else{?>
        <td><font color="red">Belum disetujui</font></td>
        <?}?>
      </tr>
    </table>
    <!--</div>--> 
  </div>
  <?}?>
</form>
<iframe name="upload_iframe" style="display:none;"></iframe>
</body><script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/jquery.masked.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	$(function(){
		$("#perpusform").find("#hargausulan").onlyNum();
	   $("#tahunterbit").mask("9999");
	});
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goSimpan(){
		var texts = 'judul,authorfirst1,nrp1,pembimbing1,npkpembimbing1,tahunterbit';
		if($('#nrp3').val() != ''){
			texts+=',nrp2,authorfirst2,authorfirst3';
		}
		else if($('#npkpembimbing3').val() != ''){
			texts+=',npkpembimbing2,pembimbing2';
		}
		else if($('#nrp2').val() != ''){
			texts+=',authorfirst2';
		}
		else if($('#authorfirst2').val() != ''){
			texts+=',nrp2';
		}
		else if($('#authorfirst3').val() != ''){
			texts+=',nrp3';
		}
		
		if (cfHighlight(texts)){
			// goSave('contents','<?= $i_phpfile?>','');
			var formid = "perpusform";
			document.getElementById(formid).target = "upload_iframe";
			document.getElementById("act").value = "simpan";
			document.getElementById(formid).submit();
		}
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
	
	function CreateTextbox(){
		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		//if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". File Upload Tambahan <input type=file name='userfile[]' size='40' /> </p><br/>";
		//}
	}
	
	function goDelFile(key1,key2){
		var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
		if(delfile){
			sent = '';
			sent += $("#perpusform").serialize();
			sent += "&act=delfile";
			sent += "&delkey="+key1;
			sent += "&delfile="+key2;
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goHapusKabeh(idupload){
		var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
		if(delfile){
			sent = '';
			sent += "&act=delkabeh";
			sent += "&id_del="+idupload;
			goLink('contents','<?= $i_phpfile?>', sent);
			// goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goRequestVal(iduploadta){
		var request=confirm("Apakah Anda yakin akan melakukan permintaan validasi?");
		if(request){
			sent = '';
			sent += $("#perpusform").serialize();
			sent += "&act=requestval";
			sent += "&iduploadta="+iduploadta;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goBebasta(iduploadta){
		// var cetak=confirm("Apakah Anda yakin akan melakukan cetak bukti upload TA?");
		// if(cetak){
			win = window.open("index.php?page=cetakuploadta&key="+iduploadta,"Cetak Tanda Terima","width=700,height=600,scrollbars=1");
			win.focus();
			// sent = '';
			// sent += "&act=cetak";
			// sent += "&iduploadta="+iduploadta;
			
			// goSubmit('contents','<?= Helper::navAddress('cetakbuktiupload.php');?>',sent);
		// }
	}
	
	function addDospem1(nama,npk) {
		$("#pembimbing1").val(nama);
		$("#npkpembimbing1").val(npk);
	}
	
	function addDospem2(nama,npk) {
		$("#pembimbing2").val(nama);
		$("#npkpembimbing2").val(npk);
	}
	
	function addDospem3(nama,npk) {
		$("#pembimbing3").val(nama);
		$("#npkpembimbing3").val(npk);
	}
	
	function goDelPem(jenis) {
		$("#pembimbing"+jenis).val('');
		$("#npkpembimbing"+jenis).val('');
	}
</script>
</html>
