<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	//reload file
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');

	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_status = Helper::removeSpecial($_REQUEST['status']);
	
	Helper::addRead($conn,$r_key);
	$readers = Helper::readers($conn,$r_key);
	$b_link = new Breadcrumb();
	$b_link->add($menu['BIBLIO'], Helper::navAddress('data_pustaka.php').'&key='.$r_key, 1);
	
	$connLamp = Factory::getConnLamp();
	$connLamp->debug = false;
	$cek = $connLamp->IsConnected();
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$menu['BIBLIO'];
	$p_dbtable = "ms_pustaka";
	
	if (!empty($_REQUEST)){
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		
		if ($r_act == 'add'){
			$e_key = $conn->GetOne("select e.ideksemplar
					       from pp_eksemplar e
					       join lv_lokasi l on l.kdlokasi = e.kdlokasi 
					       where e.idpustaka='$r_key' and e.statuseksemplar='ADA' and e.kdkondisi = 'V' "); //and l.idunit = '".$_SESSION['unituser']."'
			$accept = Perpus::UnitAccept();
			if(!$accept){
				$confirm = '<div class="error">'.UI::message('Maaf Semua Buku ini tidak ada yang available di unit anda.',true).'</div>';									
			}
			else if(!$e_key){
				$confirm = '<div class="error">'.UI::message('Maaf Semua Buku ini tidak ada yang available di unit anda.',true).'</div>';									
			}else{
			//if ($r_status != 'ADA'){
				$isreservasi = $conn->GetOne("select count(*) from pp_reservasi where ideksemplarpesan='$e_key' and idanggotapesan = '".$_SESSION['idanggota']."' and statusreservasi='1' and to_char(tglexpired,'YYYY-mm-dd') > to_char(current_date,'YYYY-mm-dd') ");
				if($isreservasi>0){
					$confirm = '<div class="error">'.UI::message('Maaf pustaka sudah dipesan oleh anda sebelumnya',true).'</div>';
				}
				elseif (!isset($_SESSION['skors'])){
					$data = $conn->GetRow("select kdklasifikasi, p.kdjenispustaka,p.idpustaka
							      from pp_eksemplar e
							      left join ms_pustaka p on p.idpustaka=e.idpustaka
							      where ideksemplar='$e_key' ");
					$sql = "select * from pp_aturan where kdjenisanggota='".$_SESSION['jenisanggota']."'";
					$isrole = $conn->GetRow($sql);
					$issave = false;
					if (!empty($isrole)){
						if($_SESSION['cart']!='')						
							$p_check=$conn->GetOne("select * from (select idpustaka from pp_eksemplar where ideksemplar in ($_SESSION[cart])) cek where idpustaka=".$data['idpustaka']."");
						else
							$p_check='';
						
						$p_trans = $conn->GetOne("select idanggota from pp_transaksi where ideksemplar = $e_key and tglpengembalian is null");
						if($p_trans!=$_SESSION['idanggota']) {

						if(!$p_check){
							$istanggung = $conn->GetOne("select 1 from pp_transaksi where ideksemplar='$e_key' and tgltenggat < current_date and statustransaksi='1'");
							if ($istanggung)
								$confirm = '<div class="error">'.UI::message('Maaf Buku ini tidak bisa dipinjam karena keterlambatan pengembalian',true).'</div>';									
							else{
								/*$jmlpinjam = $conn->GetOne("select count(*) from pp_transaksi where tglpengembalian is null and idanggota='$_SESSION[idanggota]'");
								$jmlreservasi = $conn->GetOne("select count(*) from pp_reservasi where idanggotapesan='$_SESSION[idanggota]' and statusreservasi='1' and tglexpired > current_date");
								$maxi = intval($jmlpinjam) + intval($jmlreservasi);
								
								$txtpinjam = ($jmlpinjam ? $jmlpinjam." pustaka pinjam" : "");
								$txtreservasi = ($jmlreservasi ? $jmlreservasi." pustaka reservasi" : "");
								$txt = (($jmlpinjam and $jmlreservasi) ? " dan " : "");

								if ($maxi < $isrole['batasmaxpinjam'])
								{*/								
									$items = explode(',',$_SESSION['cart']);
									if($_SESSION['cart']==NULL)
										$count = 0;
									else
										$count = count($items);
									
									//if ($count < $isrole['batasmaxpinjam']){
										$isvalid = true;
										//pengecekkan untuk waiting list maks 3 pemesan
										/*$count = $conn->GetOne("select count(*) from pp_reservasi where ideksemplarpesan='$e_key' and statusreservasi='1' and tglexpired > current_date");
										if ($count > 2)
											$isvalid = false;*/
																
										if ($isvalid){
											$issave = true;
											Perpus::Cart($r_act,$e_key);
											$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penambahan Pemesanan Berhasil').'</div>';
										}/*else 
											$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Daftar antrian pesan sudah penuh',true).'</div>';				
									}else
										$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Pemesanan anda sudah maksimal. <br/>Anda memiliki '.$txtreservasi.$txt.$txtpinjam,true).'</div>';	
								}else
									$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Pemesanan anda sudah maksimal. <br/>Anda memiliki '.$txtreservasi.$txt.$txtpinjam,true).'</div>';									
							*/}
						}else
						$confirm = '<div class="error">'.UI::message('Maaf judul pustaka tersebut tidak dapat dipesan ulang',true).'</div>';									
					}
						else
							$confirm = '<div class="error">'.UI::message('Maaf pustaka tidak dapat dipesan oleh Peminjamnya',true).'</div>';
					}else{
						if(empty($_SESSION['userid'])){
							$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Anda Belum Login, Silahkan Login terlebih dahulu',true).'</div>';		
							$_SESSION['xideksemplar'] = $e_key;
							$_SESSION['xstatus'] = $r_status;
						}else
							$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Aturan Belum Ada. Hubungi Perpustakaan PJB',true).'</div>';		
					}	
				}else							
					$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Anda Terkena Skors.',true).'</div>';
				
			if ($issave)
				Helper::addHistory($conn, "Pemesanan Eksemplar ", "eksemplar ".$_SESSION['cart']);
			else
				Helper::addHistory($conn, $confirm, "eksemplar ".$ekey);
				
							
			}

		}
	}
	
	$sql = "select p.*, j.namajenispustaka,j.islokal, b.namabahasa
			from $p_dbtable p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$r_key'";
	$row = $conn->GetRow($sql);
	
	$peng = $conn->GetArray("select coalesce(p.namadepan,'') || ' ' || coalesce(p.namabelakang,'') as pengarang, t.idpustaka 
				from ms_author p 
				left join pp_author t on t.idauthor = p.idauthor
				where t.idpustaka='$r_key' ");
	$pengarang = array();
	foreach($peng as $p){
		$pengarang[$p['idpustaka']][] = $p['pengarang'];
	}
	
	$sqlFile = "select * from pustaka_file where idpustaka = '$r_key' order by idpustakafile ";
	$rsFile = $conn->GetArray($sqlFile);
	
	//setting untuk halaman
	$p_title = $form['DET_SEE'].' '.ucwords(strtolower($row['namajenispustaka']));
		
	//untuk eksemplar
	$sql = "select e.ideksemplar, e.idpustaka, e.noseri, r.namarak, r.lantai, l.namalokasi, e.statuseksemplar, k.namaklasifikasi,
			k.kdklasifikasi, l.idunit
		from pp_eksemplar e 
		left join lv_lokasi l on e.kdlokasi=l.kdlokasi
		left join ms_rak r on r.kdrak=e.kdrak
		left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
		where E.idpustaka='$r_key' and E.kdkondisi='V' and e.statuseksemplar is not null ";
	$rseks = $conn->Execute($sql);
	
	while ($roweks = $rseks->FetchRow()){
		$ideksemplar[$roweks['idpustaka']][] = $roweks['ideksemplar'];
		$seri[$roweks['idpustaka']][] = $roweks['noseri'];
		$eksrak[$roweks['idpustaka']][] = $roweks['namarak'];
		$ekslokasi[$roweks['idpustaka']][] = $roweks['lantai']!='' ? $roweks['namalokasi'].' / Lt '.$roweks['lantai'] : $roweks['namalokasi'];
		$eksstatus[$roweks['idpustaka']][] = $roweks['statuseksemplar'];
		$label[$roweks['idpustaka']][] = $roweks['namaklasifikasi'];
		$lbl[$roweks['idpustaka']][] = $roweks['kdklasifikasi'];
		$unit[$roweks['idpustaka']][] = $roweks['idunit'];
	}
	
	//untuk topik
	// $sql = "select namatopik from lv_topik
			// where kode = '".$row['kodeddc']."'";
	
	$sql = "select namatopik from pp_topikpustaka tp
			join lv_topik t on tp.idtopik=t.idtopik
			where tp.idpustaka=$row[idpustaka]";
	$rowtopik = $conn->GetOne($sql);

	$sqlj = "select j.namajurusan from pp_bidangjur b 
			left join lv_jurusan j on j.kdjurusan = b.kdjurusan
			where b.idpustaka=$row[idpustaka]";
	$rowjurusan = $conn->GetOne($sqlj);
	
	$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = $r_key ";
	$rsCover = $connLamp->GetOne($sqlCover);
	
	$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
	$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
	
	$c_down = true;//$valid['down'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
<style>
	.tooltip{display:none !important;}
</style>
</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	<?= $b_link->output(); ?>
	
	<div class="bookDetail">
		<div class="contentTop">&nbsp;</div>
		<div class="contentContainer" style="padding-bottom:10px;">
			<h4 class="sidebar-title"><?= $p_title?></h4>
			<?= (!$confirm) ?  '' : $confirm; ?>
			<? /*if (isset($_SESSION['userid'])) {?>
				<button class="btn btn-success pull-right" style="cursor:pointer" onclick="addCart()"><span class="glyphicon glyphicon-check"></span>&nbsp; Pinjam Buku Ini</button>
			<?php }*/	?>
			<div class="clearfix"></div>
			<br/>
			<table width="98%" cellpadding="10" cellspacing="0" border="0" class="well">
				<tr>
					<td colspan=2>Total Viewer : <?=($readers?$readers:0);?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Judul</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['judul']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b><?= $form['EDITION']?></b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['edisi']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b><?= $form['NO_CALL']?></b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['nopanggil']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>ISBN/ISSN</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['isbn']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b><?= $form['AUTHOR']?></b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?php 
							$auth = array(); 
							if(!empty($pengarang[$row['idpustaka']])){
								foreach($pengarang[$row['idpustaka']] as $a){
									echo "<u style=\"cursor:pointer;padding-left:4px;\" onclick=\"goAuthor('".$a."')\">".$a."</a><br>";
								}
							}else{
								if($row['authorfirst1']!=''){
									echo "<u style=\"cursor:pointer;padding-left:4px;\" onclick=\"goAuthor('".$row['authorfirst1']." ".$row['authorlast1']."')\">".$row['authorfirst1']." ".$row['authorlast1']."</a><br>";
								}if($row['authorfirst2']!=''){
									echo "<u style=\"cursor:pointer;padding-left:4px;\" onclick=\"goAuthor('".$row['authorfirst2']." ".$row['authorlast2']."')\">".$row['authorfirst2']." ".$row['authorlast2']."</a><br>";
								}
								
								if($row['authorfirst3']!=''){
									echo "<u style=\"cursor:pointer;padding-left:4px;\" onclick=\"goAuthor('".$row['authorfirst3']." ".$row['authorlast3']."')\">".$row['authorfirst3']." ".$row['authorlast3']."</a><br>";
								}
							}
						?>
					</td>
				</tr>
				<?/*?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Subyek/Subjek</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['subyek']; ?></td>
				</tr>
				
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Klasifikasi</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['namaklasifikasi']; ?></td>
				</tr>
				<?*/?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Judul Seri</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['edisi']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>GMD</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['namajenispustaka']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Bahasa</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['namabahasa']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b><?= $form['PUBLISHER']?></b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['namapenerbit'].', '.$row['tahunterbit']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Tahun Terbit</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['tahunterbit']; ?></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Tempat Terbit</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['kota']; ?></td>
				</tr>
				<?/*?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Kolasi</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['kota']; ?></td>
				</tr>
				<?*/?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"><b>Catatan</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<!-- <p style="margin:0"><?= empty($row['sinopsis']) ? $form['P_EMPTYSYNOPSIS'] : $row['sinopsis'];?></p> -->
						<p style="margin:0"><?= empty($row['sinopsis']) ? $form['P_EMPTYSYNOPSIS'] : $row['sinopsis'];?></p>
						<!-- <p style="margin:0"><?= empty($row['sinopsis_upload']) ? '' : $row['sinopsis_upload'];?></p> -->
					</td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:15%"></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<? if ($row['sinopsis_upload']) { ?>
						<object data="<?= 'uploads/sinopsis/'.$row['sinopsis_upload'] ?>" type="application/pdf" width="600" height="600">
							<p>Alternative text - include a link <a href="<?= 'uploads/sinopsis/'.$row['sinopsis_upload'] ?>">to the PDF!</a></p>
						</object>
						<? } ?>
					</td>
				</tr>
				<tr>
					<td colspan=2><h4 class="sidebar-title">Detail Spesifik</h4></td>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;width:20%"><b>Gambar Sampul</b></td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<div class="item">
							<?
								$rsCover = $connLamp->GetOne($sqlCover);
								$cover = Perpus::loadBlobFile($rsCover, 'jpg');
							?>
							<img class="lazyOwl"  src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" class="book" onclick="showPus('<?=$row['idpustaka']?>')" width="300" data-container="body" data-toggle="tooltip" data-placement="bottom"  title="<?=$row['noseri'].' - '.$row['judul'];?>">
							<!--img class="lazyOwl"  src="<?//= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" class="book" onclick="showPus('<?//=$rowtop['idpustaka']?>')" width="90%" data-container="body" data-toggle="tooltip" data-placement="bottom"  title="<?//=$rowtop['noseri'].' - '.$rowtop['judul'];?>"-->
						</div>
					</td>
				</tr>
				
			</table>
			<table>
				<tr>
					<td colspan="4">
						<div class="bookDetail">
						<div class="contentContainer">
						<span class="menuTitle"><h4 class="sidebar-title">File</h4></span>
						<table width="98%" cellpadding="4" cellspacing="0">
							<?
							$k=0;
							foreach($rsFile as $f){
								$k++;
							?>
							<tr>
								<td width="2%" style="padding-top:px;">
									<?
									
										if (Helper::getExtension($f['files']) == '.pdf')
											echo '<img src="images/tombol/cart/pdf.gif" />';
										else if (Helper::getExtension($f['files']) == '.doc' or Helper::getExtension($f['files']) == '.docx')
											echo '<img src="images/tombol/cart/doc.png" />';
										else if (Helper::getExtension($f['files']) == '.xls' or Helper::getExtension($f['files']) == '.xlsx')
											echo '<img src="images/tombol/cart/xls.png" />';
										else if (Helper::getExtension($f['files']) == '.ppt' or Helper::getExtension($f['files']) == '.pptx')
											echo '<img src="images/tombol/cart/ppt.png" />';
										else if (Helper::getExtension($f['files']) == '.zip')
											echo '<img src="images/tombol/cart/zip.png" />';
										else
											echo '<img src="images/tombol/cart/blank.png" />';
									?>
								</td>
								<td style="padding-top:5px;">
									<p width="100%">&nbsp;
										<?
											if($f['login'] == "1" and isset($_SESSION['userid'])) {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= urlencode($f['files']); ?>','<?= urlencode($row['noseri']) ?>');">
										<?= Helper::GetPath($f['files']); ?></u>
										<?
											}elseif($f['login'] == "0") {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= urlencode($f['files']); ?>','<?= urlencode($row['noseri']) ?>');">
										<?= Helper::GetPath($f['files']); ?></u>
										<?
											}else echo Helper::GetPath($f['files'])."<br>";
										?>
										
									</p>
								</td>
							</tr>
							<?
							}
							?>
						</table>
					    </div>
					    </div>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="resultSub" style="padding-top:5px;">
						<h4 class="sidebar-title"><?= $form['COPIES'];?></h4>
						<table border="0" cellpadding="4" cellspacing="0" class="containerTable" width="100%">
							<tr>
								<th width="150" align="left" class="eksHead" style="padding-top:3px;"><?= $row['islokal']==1 ? 'KODE' : 'REG. COMP' ?></th>
								<th width="400" align="left" class="eksHead"><?= $form['LOCATION']?></th>
								<th width="200" align="left" class="eksHead"><?= $form['CLASSIFICATION']?></th>
								<th width="120" align="left" class="eksHead"><?= $form['STATE']?></th>
							</tr>			
							<? for($j=0;$j<count($eksrak[$row['idpustaka']]);$j++) { 
							$warna = 'black';	
							?>
							<tr >
								<td align="left">
									<?php if($unit[$row['idpustaka']][$j] == $_SESSION['unituser'] && $eksstatus[$row['idpustaka']][$j]=='ADA'){?>
									<a class="judul-buku" href="javascript:showEks('<?= $ideksemplar[$row['idpustaka']][$j];?>','<?= $eksstatus[$row['idpustaka']][$j] ?>')" title="Detail Eksemplar">
										<?= $seri[$row['idpustaka']][$j];?>
									</a>
									<?php }else{?>
										<?= $seri[$row['idpustaka']][$j];?>
									<?php }?>
								</td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $ekslokasi[$row['idpustaka']][$j];?></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $label[$row['idpustaka']][$j];?></td>
								<? $eksstatus[$row['idpustaka']][$j]=='ADA' ? $class = 'resultAvailable' : $class = 'resultUnavailable' ?>
								<td style="color:<?= $warna ?>" class="<?= $class; ?>"><?= $eksstatus[$row['idpustaka']][$j]=='ADA' ? $form['P_AVAILABLE']: ($eksstatus[$row['idpustaka']][$j]=='PJM' ? $form['P_LOANS'] : 'PROSES');?></td>
							</tr>
						<? } ?>
						<? 	if (count($eksrak[$row['idpustaka']]) == 0){ ?>
						<tr>
							<td colspan=4 align="center"><b><?= $form['EKS_NULL'] ?></b></td>
						</tr>
						<? } ?>
						</table>
						
					</td>
				</tr>

			</table>
		</div>
	</div>
    </div>
    
    </div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>

</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	function addCart(){
		sent = "&act=add&key=<?= $r_key?>&status=<?= $r_status; ?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function showCart(){
		sent = "";
		goLink('contents','<?= Helper::navAddress('data_cart.php')?>', sent);
	}
	
	function showEks(key, status){
		sent = "&key=" + key + "&status=" + status;
		goLink('contents','<?= Helper::navAddress('data_eksemplar.php')?>', sent);
	}
	
	function goAuthor(key,par){
		sent = "&par="+par+"&key="+ key;
		goLink('contents','<?= Helper::navAddress('xlisthasilauthor.php')?>', sent);
	}
	
	function getLook(file,seri){
		window.open("<?= Helper::navAddress('showOff.php')?>&kode="+seri+"&file="+file,"_blank");
	}
	
	function getLooks(file,seri,det){
		window.open("<?= Helper::navAddress('view_pdfseri.php')?>&kode="+seri+"&det="+det+"&file="+file,"_blank");
	}
</script>
</html>
