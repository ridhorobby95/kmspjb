<?
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Perpustakaan PJB, Digital Library of Sentra Vidya Utama Company, Digilib PJB, Library PJB" />
<title>PJB - Digital Library</title>
<link href="style/bootstrap.css" rel="stylesheet" type="text/css"  />
<link href="style/style.css" rel="stylesheet" type="text/css"  />
<link rel="shortcut icon" href="images/favicon.ico" />
<link href="style/jquery.autocomplete.css" type="text/css" rel="stylesheet">
<link href="script/slick/slick.css" type="text/css" rel="stylesheet">
<link href="script/slick/slick-theme.css" type="text/css" rel="stylesheet">

<script src="script/owl-carousel/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="script/newsticker.js" type="text/javascript"></script>
<script type="text/javascript" src="script/auth.js"></script>
<script src="script/ajaxin.js" type="text/javascript"></script>
<script src="script/dropdown.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="script/jquery.tools.min.js"></script>-->
<script type="text/javascript" src="script/slick/slick.js"></script>
<script type="text/javascript" src="script/jquery.xauto.js"></script>
<script type="text/javascript" src="script/jquery.autocomplete.js"></script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-7964997-2']);
     _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl'  : 'http://www')  + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
     })();
</script>
</head>
<body>
<?php include ('inc_menu.php'); ?>
      <div id="contents">
      </div>

<?php include ('inc_footer.php'); ?>
<div id="progressbar" style="position:absolute;visibility:hidden;left:0px;top:0px;">
		<table bgcolor="#FFFFFF" border="1" style="border-collapse:collapse;"><tr><td align="center">
		<?= $form['WAIT']; ?>...<br><br><img src="images/progressbar.gif">
		</td></tr></table>
	</div>
	<div id="progress" style="position:absolute;visibility:hidden;left:0px;top:0px;">
	</div>
</body>


<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/pajhash.js"></script>
<script type="text/javascript" src="script/linkis.js"></script>
<script type="text/javascript" src="script/aes.js"></script>
<script type="text/javascript" src="script/jquery.neonflash.js"></script>
<script type="text/javascript">

xlistcari = "<?= Helper::navAddress('list_hasilcari.php'); ?>";
listhome = "<?= Helper::navAddress('list_home.php'); ?>";
xtdid = "contents";

$(document).ready(function() {
	sent = "";
	ide = '<?= $_SESSION['xideksemplar'] ?>';
	idpus = '<?= $_REQUEST['pustaka'] ?>';

	cr = '<?= $_REQUEST['cr']; ?>';

	if (cr != ''){
		m = '<?= $_REQUEST['m']; ?>';
		if (m)
			goCDTA(cr);
		else{
			$("#txtcari").val(cr);
			goCari();
		}
	}else{
		if(ide !='')
			goLink(xtdid, 'index.php?page=data_eksemplar&key=<?= $_SESSION['xideksemplar'] ?>&status=<?= $_SESSION['xstatus'] ?>', '');
		else{
			if (idpus != '')
				goLink(xtdid, 'index.php?page=data_pustaka&key='+idpus, '');
			else
				goLink(xtdid, listhome, sent);
		}
	}
});

function etrCari(e) {
	var ev = (window.event) ? window.event : e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;

	if (key == 13)
		goCari('');
}

function etrLogin(e) {
	var ev= (window.event) ? window.event: e;
	var key = (ev.keyCode) ? ev.keyCode : ev.which;

	if (key == 13)
		goLogin();
}

function goCari() {
	var str = $.trim(document.getElementById("txtcari").value);

	if(parseInt(str.length) > 0){
		var sent = "&key=" + document.getElementById("txtcari").value;
		$("#contents").divpost({page: xlistcari, sent: sent, images: "progressbar"});
	}else{
		alert("<?= $sidebar['ALERT_SEARCH']; ?>");
	}
}

function goCDTA(x){
	var post = "key=" + x +"&alf=1";
	$("#contents").divpost({page: '<?= Helper::navAddress('list_hasilcarita.php'); ?>', sent: post});
	var keysearch = "key=" + x +"&alf=1";
}

file = "<?= Helper::navAddress('functionx.php'); ?>";

function goLogin()
{
	var u, p;

	$.post(file,'f=salt',function(salt) {
		if(salt == '-1') {
			$("#ret_div").html('<font color="red"><?= $form['LOG_ERR']; ?></font>');
			return false;
		}

		u = $("#username").val();
		p = $("#passwd").val();
		px = encodeURIComponent(Aes.Ctr.encrypt($("#passwd").val(), p, 256));

		$.post(file,'f=login&p1='+u+'&p2='+p+'&px='+px,function(retlog) {
			if(retlog == 1) {
				$("#ret_div").html('<font color="#339933"><?= $form['LOG_SUCC']; ?></font>');

				location.href = "<?= $i_phpfile; ?>";
			}
			else {
				$("#ret_div").html('<font color="red"><?= $form['LOG_FAIL']; ?></font>');
			}
		});
	});
	$("#ret_div").html('<img src="images/load.gif">');
}
	$('#txtcari').autocomplete(
			'<?= Helper::navAddress('_fts') ?>',
			{
				parse: function(data){
					var parsed = [];
					for (var i=0; i < data.length; i++) {
						parsed[i] = {
							data: data[i],
							value: data[i].judul // nama field yang dicari
						};
					}
					return parsed;
				},
				formatItem: function(data,i,max){
					var str = '<div class="search_content">';
					str += data.judul;
					str += '</div>';
					return str;
				},
				width: 330,
				dataType: 'json'
			})
			.result(
				function(event,data,formated){
					$('#txtcari').val(data.judul);
				}
	);

	function goHelp(){
	    win = window.open("index.php?page=helpdesk","Digilib Helpdesk Support","width=700,height=600,scrollbars=1");
	    win.focus();
	}

	function goTagihan(xjum){
		sent = "&jum=" + xjum;
		var la = '<?= $_SESSION['lang'] ?>';
		if(la=='en')
		goLink('contents','<?= Helper::navAddress('list_tagihanen.php'); ?>', sent);
		else
		goLink('contents','<?= Helper::navAddress('list_tagihan.php'); ?>', sent);
	}

	function clearFill(){
		if ($("#txtcari").val() == '<?= $sidebar['SD_SEARCH']; ?>')
			$("#txtcari").val("");
		else
			$("#txtcari").val();
	}

	function addFill(){
		$("#txtcari").val("<?= $sidebar['SD_SEARCH']; ?>");
	}

	$(document).ready(function() {
		 $("#reload").load("<?= Helper::navAddress('_statistik.php') ?>");
	  <? if (!empty($isskors)){ ?>
		$("#alert").fadeOut(800).fadeIn(800).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
	<? } ?>
	<? if (!empty($_SESSION['userid'])){ ?>
		$("#pesan").fadeOut(800).fadeIn(800).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
		$(".notif").fadeOut(800).fadeIn(800).fadeOut(400).fadeIn(400).fadeOut(400).fadeIn(400);
	<? } ?>
	   addFill();
	});


var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

var browser = BrowserDetect.browser;
var ver = BrowserDetect.version;
if(browser == 'Opera') {
	document.getElementById('txtcari').setAttribute('size','35');
	document.getElementById('magnify').style.position = 'relative';
	document.getElementById('magnify').style.pixelTop = '9';
}
else if (browser == 'Safari') {
	document.getElementById('txtcari').setAttribute('size','30');
}
	$(document).ready(function(){
		$("span[rel]").overlay();
		$('#coin-slider').coinslider({width:400,height:150,navigation:true,links:false,delay:4000,spw:9,sph:5,sDelay:1});
	});

	function goLihat(key){
		var sent = "&key=" + key;
		goLink('contents','<?= Helper::navAddress('data_pustaka.php');?>',sent);
	}

	function goDirect(tipe,key){
		var sent = "&tipe=" + tipe + "&key=" + key;
		goLink('contents','<?= Helper::navAddress('list_kategori.php');?>',sent);
	}

	function goDirectSub(tipe,key,jur,thn){
		var sent = "&tipe=" + tipe + "&key=" + key + "&jur=" + jur + "&thn=" + thn;
		goLink('contents','<?= Helper::navAddress('list_kategori.php');?>',sent);
	}

	function goListVal(jenis){
		var sent = "&jenis=" + jenis;
		goLink('contents','<?= Helper::navAddress('list_notifikasi.php');?>',sent);
	}
	
	function goPerpus(lokasi){alert(lokasi);
		var sent = "&lokasi=" + lokasi + "&act=lokasiperpus";
		goLink('contents','<?= Helper::navAddress('list_home.php');?>',sent);
	}
	</script>

<script type="text/javascript">
	function openFold(jenis,idk){
		if(jenis == "jur"){
			$("#jph"+idk).show();
			$("#jps"+idk).hide();
		}else if(jenis == "thn"){
			$("#jrh"+idk).show();
			$("#jrs"+idk).hide();
		}
		$("."+jenis+idk).show();
	}

	function closeFold(jenis,idk){
		if(jenis == "jur"){
			$("#jph"+idk).hide();
			$("#jps"+idk).show();

			$(".thn"+idk).hide();

			$(".jrh"+idk).hide();
			$(".jrs"+idk).show();
		}else if(jenis == "thn"){
			$("#jrh"+idk).hide();
			$("#jrs"+idk).show();
		}
		$("."+jenis+idk).hide();
	}

	function showPus(key){
		sent = "&key=" + key;
		goLink('contents','<?= Helper::navAddress('data_pustaka.php')?>', sent);
	}
</script>
</html>
