<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//reload file
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);

	$b_link = new Breadcrumb();
	$b_link->add($menu['BIBLIO'], Helper::navAddress('tools.php').'&key='.$r_key, 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$menu['BIBLIO'];
	$p_dbtable = "ms_pustaka";
	
	$sql = "select p.*, j.namajenispustaka,j.islokal, b.namabahasa
			from $p_dbtable p 
			left join lv_bahasa b on b.kdbahasa=p.kdbahasa
			left join lv_jenispustaka j on j.kdjenispustaka=p.kdjenispustaka
			left join ms_penerbit mp on mp.idpenerbit=p.idpenerbit
			where p.idpustaka='$r_key'";
	$row = $conn->GetRow($sql);
	
	$sqlFile = "select * from pustaka_file where idpustaka = '$r_key' order by idpustakafile ";
	$rsFile = $conn->GetArray($sqlFile);
	
	//setting untuk halaman
	$p_title = $form['DET_SEE'].' '.ucwords(strtolower($row['namajenispustaka']));
		
	//untuk eksemplar
	$sql = "select * from pp_eksemplar e 
			left join lv_lokasi l on e.kdlokasi=l.kdlokasi
			left join ms_rak r on r.kdrak=e.kdrak
			left join lv_klasifikasi k on k.kdklasifikasi=e.kdklasifikasi
			where idpustaka='$r_key' and kdkondisi='V' and e.statuseksemplar is not null ";
	$rseks = $conn->Execute($sql);
	
	while ($roweks = $rseks->FetchRow()){
			$ideksemplar[$roweks['idpustaka']][] = $roweks['ideksemplar'];
			$seri[$roweks['idpustaka']][] = $roweks['noseri'];
			$eksrak[$roweks['idpustaka']][] = $roweks['namarak'];
			$ekslokasi[$roweks['idpustaka']][] = $roweks['lantai']!='' ? $roweks['namalokasi'].' / Lt '.$roweks['lantai'] : $roweks['namalokasi'];
			$eksstatus[$roweks['idpustaka']][] = $roweks['statuseksemplar'];
			$label[$roweks['idpustaka']][] = $roweks['namaklasifikasi'];
			$lbl[$roweks['idpustaka']][] = $roweks['kdklasifikasi'];
	}
	
	//untuk topik
	// $sql = "select namatopik from lv_topik
			// where kode = '".$row['kodeddc']."'";
	
	$sql = "select namatopik from pp_topikpustaka tp
			join lv_topik t on tp.idtopik=t.idtopik
			where tp.idpustaka=$row[idpustaka]";
	$rowtopik = $conn->GetOne($sql);

	$sqlj = "select j.namajurusan from pp_bidangjur b 
			left join lv_jurusan j on j.kdjurusan = b.kdjurusan
			where b.idpustaka=$row[idpustaka]";
	$rowjurusan = $conn->GetOne($sqlj);

	$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
	$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
	
	$c_down = true;//$valid['down'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>

</head>
<body>
<div class="container">
  <div class="col-md-9">
    <div class="primary">
	
	<?= $b_link->output(); ?>
	<? if (isset($_SESSION['userid'])){ ?>
	<div class="notification">
		<?= Perpus::getCart(); ?>
	</div>
	<? } ?>
	<div class="bookDetail">
		<div class="contentTop">&nbsp;</div>
		<div class="contentContainer" style="padding-bottom:10px;">
			<h4 class="sidebar-title"><?= $p_title?></h4>
			<table width="98%" cellpadding="1" cellspacing="0">
				<tr height="20"><td>&nbsp;</td></tr>
				<tr height="20">
					<td class="judul"><?= $row['judul'] ?></td>
                    <td align="right"></td>
				</tr>
			</table><br />
			<table width="98%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="top" rowspan="<?=($row['islokal']==1?"7":"6");?>" width="188" align="center" style="padding-left:12px;padding-right:12px;">
						<a href="<?= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" class="zoom">
						<img id="imgfoto" src="<?= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" width="100" height="108">
						</a>
					</td>
					<? if($row['islokal']==1) { ?>
					<td valign="top" class="resultSub" width="100">REG. COMP</td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['noseri']; ?></td>
					<? }?>
				</tr>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['NO_CALL']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub"><?= $row['nopanggil']; ?></td>
				</tr>

				<? if($row['islokal']!=1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['EDITION']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['edisi']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;"><?= $form['AUTHOR']?></td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?
								if($row['islokal']==0){
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}else{
								echo "<u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst1'].' '.$row['authorlast1']."','1')\">".$row['authorfirst1'].' '.$row['authorlast1']."</u>";						
								if($row['authorfirst2']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst2'].' '.$row['authorlast2']."','1')\">".$row['authorfirst2'].' '.$row['authorlast2']."</u>";
								
								if($row['authorfirst3']!='')
									echo ", <u style=\"cursor:pointer\" onclick=\"goAuthor('".$row['authorfirst3'].' '.$row['authorlast3']."','1')\">".$row['authorfirst3'].' '.$row['authorlast3']."</u>";
								
								}
						?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" valign="top" style="padding-top:5px;">Contributor</td>
					<td class="resultSub" valign="top" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
					<?= $row['pembimbing']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['PUBLISHER']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['kota'].': '.$row['namapenerbit'].', '.$row['tahunterbit']; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['TOPIC']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $rowtopik;?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['PRODI']?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $rowjurusan;?>
					</td>
				</tr>
				<?}?>
				<tr>
					<td colspan="4">
						<br />
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr height="30">
								<td><strong><?= $form['SYNOPSIS']?></strong></td>
							</tr>
							<tr>
								<td><p><?= empty($row['sinopsis']) ? $form['P_EMPTYSYNOPSIS'] : $row['sinopsis'];?></p></td>
							</tr>
						</table>
					</td>
				</tr>
				<?/*?><tr>
					<td colspan="4">
						<br />
						<table width="100%" cellpadding="4" cellspacing="0">
							<tr>
								<td><strong>URL</strong></td>
							</tr>
							<tr>
								<td><p><a href="<?= Config::webUrl."?pustaka=".$row['idpustaka'];?>"><?= Config::webUrl."?pustaka=".$row['idpustaka'];?></a></p></td>
							</tr>
						</table>
					</td>
				</tr><?*/?>

				<tr>
					<td colspan="4">
						<div class="bookDetail">
						<div class="contentTop">&nbsp;</div>
						<div class="contentContainer" style="padding-bottom:10px;">
						<span class="menuTitle">File  <font color="#FF0000" style="font-size:9px"><?= $form['P_MEMBERONLY']; ?></font></span>
						<table width="98%" cellpadding="4" cellspacing="0">
							<?
							$k=0;
							foreach($rsFile as $f){
								$k++;
							?>
							<tr>
								<td width="2%" style="padding-top:px;">
									<?
									
										if (Helper::getExtension($f['files']) == '.pdf')
											echo '<img src="images/tombol/cart/pdf.gif" />';
										else if (Helper::getExtension($f['files']) == '.doc' or Helper::getExtension($f['files']) == '.docx')
											echo '<img src="images/tombol/cart/doc.png" />';
										else if (Helper::getExtension($f['files']) == '.xls' or Helper::getExtension($f['files']) == '.xlsx')
											echo '<img src="images/tombol/cart/xls.png" />';
										else if (Helper::getExtension($f['files']) == '.ppt' or Helper::getExtension($f['files']) == '.pptx')
											echo '<img src="images/tombol/cart/ppt.png" />';
										else if (Helper::getExtension($f['files']) == '.zip')
											echo '<img src="images/tombol/cart/zip.png" />';
										else
											echo '<img src="images/tombol/cart/blank.png" />';
									?>
								</td>
								<td style="padding-top:5px;">
									<p width="100%">&nbsp;
										<?
											if($f['login'] == "1" and isset($_SESSION['userid'])) {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= urlencode($f['files']); ?>','<?= urlencode($row['noseri']) ?>');">
										<?= Helper::GetPath($f['files']); ?></u>
										<?
											}elseif($f['login'] == "0") {
										?>
										<u style="cursor:pointer" onclick="getLook('<?= urlencode($f['files']); ?>','<?= urlencode($row['noseri']) ?>');">
										<?= Helper::GetPath($f['files']); ?></u>
										<?
											}else echo Helper::GetPath($f['files'])."<br>";
										?>
										
									</p>
								</td>
							</tr>
							<?
							}
							?>
						</table>
					    </div>
					    </div>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="resultSub" style="padding-top:5px;">
						<!--<br /><br /><b><?= $form['COPIES']?></b> <br />-->
						<h4 class="sidebar-title"><?= $form['COPIES'];?></h4>
						<table border="0" cellpadding="4" cellspacing="0" class="containerTable" width="100%">
							<tr>
								<th width="150" align="left" class="eksHead" style="padding-top:3px;"><?= $row['islokal']==1 ? 'KODE' : 'REG. COMP' ?></th>
								<th width="400" align="left" class="eksHead"><?= $form['LOCATION']?></th>
								<th width="200" align="left" class="eksHead"><?= $form['CLASSIFICATION']?></th>
								<th width="120" align="left" class="eksHead"><?= $form['STATE']?></th>
							</tr>			
							<? for($j=0;$j<count($eksrak[$row['idpustaka']]);$j++) { 
							$warna = 'black';	
							?>
							<tr >
								<td align="left"><a class="judul-buku" href="javascript:showEks('<?= $ideksemplar[$row['idpustaka']][$j];?>','<?= $eksstatus[$row['idpustaka']][$j] ?>')" title="Download File"><?= $seri[$row['idpustaka']][$j];?></a></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $ekslokasi[$row['idpustaka']][$j];?></td>
								<td style="color:<?= $warna ?>" class="resultSub"><?= $label[$row['idpustaka']][$j];?></td>
								<? $eksstatus[$row['idpustaka']][$j]=='ADA' ? $class = 'resultAvailable' : $class = 'resultUnavailable' ?>
								<td style="color:<?= $warna ?>" class="<?= $class; ?>"><?= $eksstatus[$row['idpustaka']][$j]=='ADA' ? $form['P_AVAILABLE']: ($eksstatus[$row['idpustaka']][$j]=='PJM' ? $form['P_LOANS'] : 'PROSES');?></td>
							</tr>
						<? } ?>
						<? 	if (count($eksrak[$row['idpustaka']]) == 0){ ?>
						<tr>
							<td colspan=4 align="center"><b><?= $form['EKS_NULL'] ?></b></td>
						</tr>
						<? } ?>
						</table>
						
					</td>
				</tr>

			</table>
		</div>
	</div>
    </div>
    
    </div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/zoom/easyzoom.js"></script>
<style>
#easy_zoom{
	width:600px;
	height:400px;	
	border:5px solid #eee;
	background:#fff;
	color:#333;
	position:absolute;
	top:60px;
	left:400px;
	overflow:hidden;
	-moz-box-shadow:0 0 10px #777;
	-webkit-box-shadow:0 0 10px #777;
	box-shadow:0 0 10px #777;
	/* vertical and horizontal alignment used for preloader text */
	line-height:400px;
	text-align:center;
	}

</style>

<script type="text/javascript">
jQuery(function($){
	$('a.zoom').easyZoom();
});
</script>
<link href="style/zoom/default.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript">
	function showCart(){
		sent = "";
		goLink('contents','<?= Helper::navAddress('data_cart.php')?>', sent);
	}
	
	function showEks(key, status){
		sent = "&key=" + key + "&status=" + status;
		goLink('contents','<?= Helper::navAddress('data_eksemplar.php')?>', sent);
	}
	
	function goAuthor(key,par){
		sent = "&par="+par+"&key="+ key;
		goLink('contents','<?= Helper::navAddress('xlisthasilauthor.php')?>', sent);
	}
	
	function getLook(file,seri){
		//window.open("<?= Helper::navAddress('view_pdf.php')?>&kode="+seri+"&file="+file,"_blank");
		window.open("<?= Helper::navAddress('showOff.php')?>&kode="+seri+"&file="+file,"_blank");
	}
	
	function getLooks(file,seri,det){
		window.open("<?= Helper::navAddress('view_pdfseri.php')?>&kode="+seri+"&det="+det+"&file="+file,"_blank");
	}
</script>
</html>
