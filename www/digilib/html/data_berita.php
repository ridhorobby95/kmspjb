<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	require_once('classes/breadcrumb.class.php');
	$b_link = new Breadcrumb();
	$b_link->add($menu['DET_NEWS'], Helper::navAddress('data_berita.php'), 1);
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$sidebar['NEWS'];	
	$p_title = $sidebar['NEWS'];
	
	//komponen halaman
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	
	$sql = "select * from pp_berita where idberita='$r_key'";
	$row = $conn->GetRow($sql);
	
	$sqlCover = "select isifile from lp.lampiranperpus where jenis = 4 and idlampiran = ".$row['idberita'];
	$rsCover = $conn->GetOne($sqlCover);
	$cover = Perpus::loadBlobFile($rsCover, 'jpg');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script type="text/javascript" src="script/auth.js"></script>
<title><?= $p_window ?></title>
</head>
<body>
<div class="container">
  <div class="col-md-9">
  	<?= $b_link->output(); ?>
	<div class="berita-detail">

	<div class="page-header"><h3 style="margin:0;"><?= $_SESSION['lang'] != 'en' ? $row['judulberita'] : $row['judulberitaen']; ?></h3></div>
	<div class="date-user"><span class="glyphicon glyphicon-calendar"></span>&nbsp;<?= $form['DATE'] ?> : <?= $_SESSION['lang'] != 'en' ? Helper::formatDateInd($row['tglberita']) : Helper::formatDateEN($row['tglberita']); ?> | <span class="glyphicon glyphicon-user"></span>&nbsp;<?= $form['POST_BY']; ?> : <?= $row['petugas']; ?></div>
	<br/>
	<p><img id="imgfoto" align="left" style="margin-right:10px;max-width:100%;" src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" width="100" height="108">
	<?= $_SESSION['lang'] != 'en' ? strip_tags($row['isiberita']) : strip_tags($row['isiberitaen']);?></p>
	</div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
</body>
<script src="script/linkis.js" type="text/javascript"></script>
</html>
