<? 

	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );

	require_once('classes/breadcrumb.class.php');
	require_once('classes/perpus.class.php');
	
	$b_link = new Breadcrumb();
	$b_link->add($menu['HOME'], Helper::navAddress('list_home.php').'&key=1', 0);

	//setting untuk halaman
	$p_window = "[Digilib] ".$form['T_HOME'];
	
	$connLamp = Factory::getConnLamp();
	$cek = $connLamp->IsConnected();
//$conn->debug = true;	
	$bln = date('m');
	
	if (isset($_POST)){
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		if($r_act == "lokasiperpus"){
			session_start();
			
			$r_lokasi = Helper::removeSpecial($_REQUEST['lokasi']);
			$_SESSION['UNIT_PERPUS'] = $r_lokasi;
		}else
			$r_lokasi = $_SESSION['UNIT_PERPUS'];
	}else{
		$r_lokasi = $_SESSION['UNIT_PERPUS'];
	}
	
	
	
	
	//perintah2 sql
	$sql = "select b.* 
		from pp_berita b 
		where b.tglexpired >= sysdate and rownum <= 5
		order by b.idberita desc";
	$rsberita = $conn->Execute($sql);
	
	
	if($r_lokasi){
		$f_sql2 = "and p.idpustaka in (select e.idpustaka from pp_eksemplar e where e.kdlokasi = '$r_lokasi')";
		$f_sqlate = " and e.kdlokasi = '$r_lokasi' "; 
	}
	$sql2 = "select j.kdjenispustaka,j.namajenispustaka,
			(
				select count(*)
				from ms_pustaka p 
				where p.kdjenispustaka=j.kdjenispustaka $f_sql2
				) as jumlah 
		from lv_jenispustaka j
		order by namajenispustaka";
	$rsjenis = $conn->Execute($sql2);
	
	#top buku terbaru
	$sql = "
	select * from (
	SELECT p.*, to_char(judul) as jdul 
		FROM ms_pustaka p 
		WHERE p.tglperolehan is not null $f_sql2
		order by p.tglperolehan desc
	) where ROWNUM <= 10 ";
	$rspustaka = $conn->Execute($sql);

	#keterlambatan
	$sqlate = "select p.idpustaka, a.idanggota, a.namaanggota, e.noseri, p.judul, to_char(t.tgltransaksi,'YYYY-mm-dd') tglpinjam, 
			to_char(t.tgltenggat,'YYYY-mm-dd') tglharuskembali,  to_char(current_date,'YYYY-mm-dd') tglskrg,
			(TO_DATE(to_char(current_date,'YYYY-mm-dd'),'yyyy-mm-dd') - TO_DATE(to_char(t.tgltenggat,'yyyy-mm-dd'),'yyyy-mm-dd')) telathari
		from pp_transaksi t
		join ms_anggota a on a.idanggota = t.idanggota
		join pp_eksemplar e on e.ideksemplar = t.ideksemplar $f_sqlate
		join ms_pustaka p on p.idpustaka = e.idpustaka 
		where tglpengembalian is null 
		and to_date(to_char(tgltenggat,'YYYY-mm-dd'),'YYYY-mm-dd') <= to_date(to_char(current_date,'YYYY-mm-dd'),'YYYY-mm-dd')
		order by telathari desc ";
	$rslate = $conn->Execute($sqlate);
	
	#top buku bulan ini
	$sqltop = "
	select * from (
	select p.noseri, to_char(p.judul) judul, p.idpustaka, count(e.ideksemplar) as jumlahpinjam  
		from pp_transaksi t 
		join pp_eksemplar e on e.ideksemplar = t.ideksemplar $f_sqlate
		join ms_pustaka p on p.idpustaka = e.idpustaka  
		where to_char(t.tgltransaksi,'YYYY-mm') = to_char(current_date,'YYYY-mm')
		group by p.noseri, to_char(p.judul), p.idpustaka
		order by jumlahpinjam desc
	) where ROWNUM <= 10 "; 
	$rstop = $conn->Execute($sqltop);
	
	if(isset($_SESSION['userid'])) {
		$sql_cek_ta2 = "select statusta,kdjenisanggota from ms_anggota where idanggota='".$_SESSION['idanggota']."'";
		$rs_cek_ta2 = $conn->GetRow($sql_cek_ta2);
	}
	
	if($_SESSION['lang']=='en')
		$ket = $conn->GetOne("select keteranganen from pp_tools where idtools = '6' and tampil='1' ") ;
	else
	       $ket = $conn->GetOne("select keterangan from pp_tools where idtools = '6' and tampil='1' ") ;
	       
	$rs_cb = $conn->Execute("select namalokasi, kdlokasi from lv_lokasi order by namalokasi");
	$l_lokasi = $rs_cb->GetMenu2('lokasi',$r_lokasi,true,false,0,'class="ControlStyle  form-control" style="width:200" onChange="goPerpus(this.value)"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window; ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>

<div class="wood">
	
<div class="container">
	<div class="col-md-12">
		<div class="row rack">
			<h2 class="home-title" style="color:#fff; text-align:center;">Koleksi Terbaru
			</h2><br/>
			<? $p=0;foreach($rspustaka as $pustaka){
				$p++;
				//$p_foto = Config::dirFoto.'pustaka/'.trim($pustaka['idpustaka']).'.jpg';
				//$p_hfoto = Config::fotoUrl.'pustaka/'.trim($pustaka['idpustaka']).'.jpg';
				$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ".$pustaka['idpustaka'];
			?>
				<?php if($p==6){?>
		</div>
		<div class="row rack">
				<?php } ?>
			<div class="col-md-2 col-sm-3 col-xs-6 <?=($p==1 or $p==6) ? "col-md-offset-1" : "" ?>">
				<?
					$rsCover = $connLamp->GetOne($sqlCover);
					$cover = Perpus::loadBlobFile($rsCover, 'jpg'); /*aktifkan lagi nanti*/
				?>
				<?php if(is_null($cover)){?>
					<div class="none" onclick="showPus('<?=$pustaka['idpustaka']?>')" title="<?=$pustaka['noseri'].' - '.trim($pustaka['judul']);?>" data-toggle="tooltip" data-placement="top">
						<?=Helper::word_limiter($pustaka['judul'],3)?>
					</div>					
				<?php }else{ ?>
					<img src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" class="book" onclick="showPus('<?=$pustaka['idpustaka']?>')" data-toggle="tooltip" data-placement="top" title="<?=$pustaka['noseri'].' - '.$pustaka['judul'];?>">
				<? } ?>
				<?php /*if(is_file($p_foto)){?>
					<img src="<?= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" class="book" onclick="showPus('<?=$pustaka['idpustaka']?>')" data-toggle="tooltip" data-placement="top" title="<?=$pustaka['noseri'].' - '.$pustaka['judul'];?>">
				<?php }else{ ?>
					<div class="none" onclick="showPus('<?=$pustaka['idpustaka']?>')" title="<?=$pustaka['noseri'].' - '.trim($pustaka['judul']);?>" data-toggle="tooltip" data-placement="top">
						<?=Helper::word_limiter($pustaka['judul'],3)?>
					</div>
				<? } */?>
			</div>
			<? } ?>
		</div>
		
		
	</div>
	
</div>
</div>

<div class="bg-red peminjam-wrapper">
	<div class="container">
		<div class="col-md-2" style="margin: 20px 0;">
		<h4 style="margin:0">Peminjam Wajib <br/>Kembalikan Buku</h4>
		</div>
		<div class="col-md-10 bg-grey">
		<div id="owl-demo2" class="owl-carousel">
		<? $i=0;while($rowlate = $rslate->FetchRow()){$i++;?>
		  <div class="item" style="padding-right: 5px;">
		  <?=$rowlate['namaanggota'].' - '.$rowlate['idanggota'];?><br/><br/>
			<div title="<?=$rowlate['noseri'].' - '.$rowlate['judul'];?>"><a href="javascript:showPus('<?=$rowlate['idpustaka']?>')"><?=$rowlate['noseri'].' - '.Helper::word_limiter($rowlate['judul'],7);?></a></div>
			
			<span class="red">Terlambat <?=$rowlate['telathari'];?> Hari</span>
		  </div>
		<?}?>
		<?if($i==0){?>
		<div class="item" style="padding-right: 5px;">
			<br/><br/>
			Tidak ada data keterlambatan
			<br/><br/>
		</div>
		<?}?>
		</div>
		
		</div>
	</div>
</div>
<div class="container">	
	  <div class="col-md-3 kategori-left">
		<h3 class="home-title"><span class="red">Koleksi</span> Buku<br/>
		<small>Kategori</small>
		</h3>
		  <?/*new folder*/?>
		  <div class="items" style="display:block !important;">
			<? $i=0;while ($rowjenis = $rsjenis->FetchRow()){$i++;?>
			<p>
			<? if($rowjenis['kdjenispustaka'] == "KT" or $rowjenis['kdjenispustaka'] == "SR"){?>
			<a href="javascript:openFold('jur','<?=$rowjenis['kdjenispustaka'];?>')" style="cursor:pointer;">
			<img src="images/tombol/folder.png" title="Direktori" id="jps<?=$rowjenis['kdjenispustaka'];?>" />
			</a>
			
			<a href="javascript:closeFold('jur','<?=$rowjenis['kdjenispustaka'];?>')" style="cursor:pointer;">
			<img src="images/tombol/folder_page.png" title="Direktori" id="jph<?=$rowjenis['kdjenispustaka'];?>" style="display: none;" />
			</a>
			<?}else{?>
			<img src="images/tombol/folder.png" title="Direktori" id="jps<?=$rowjenis['kdjenispustaka'];?>" />
			<?}?>
			&nbsp;<a href="javascript:goDirect('J','<?= $rowjenis['kdjenispustaka']; ?>')" style="cursor:pointer;">
			  <?= ucwords(strtolower($rowjenis['namajenispustaka'])); ?>
			  &nbsp;(
			  <?= $rowjenis['jumlah'];?>
			  )</a></p>
			<? if($rowjenis['kdjenispustaka'] == "KT" or $rowjenis['kdjenispustaka'] == "SR"){?>
				<!-- Jurusan-->
				<? $i=0;
				foreach($folder[$rowjenis['kdjenispustaka']]['detjur'] as $rowjur){
					$i++;?>
				<p class="jur<?=$rowjenis['kdjenispustaka'];?>" style="display: none;margin-left: 15px;">
					<a href="javascript:openFold('thn','<?=$rowjenis['kdjenispustaka'].$rowjur['kdjurusan'];?>')" style="cursor:pointer;">
						<img src="images/tombol/folder.png" class="jrs<?=$rowjenis['kdjenispustaka'];?>" id="jrs<?=$rowjenis['kdjenispustaka'].$rowjur['kdjurusan'];?>" title="Direktori" />
					</a>
					<a href="javascript:closeFold('thn','<?=$rowjenis['kdjenispustaka'].$rowjur['kdjurusan'];?>')" style="cursor:pointer;">
						<img src="images/tombol/folder_page.png" class="jrh<?=$rowjenis['kdjenispustaka'];?>" id="jrh<?=$rowjenis['kdjenispustaka'].$rowjur['kdjurusan'];?>" title="Direktori" style="display: none;" />
					</a>
					&nbsp;<a href="javascript:goDirectSub('J','<?= $rowjenis['kdjenispustaka']; ?>','<?= $rowjur['kdjurusan']; ?>','')" style="cursor:pointer;">
				  <?= ucwords(strtolower($rowjur['jurusan'])); ?>
				  &nbsp;(
				  <?= $rowjur['jumlah'];?>
				  )</a></p>
					<!-- tahun-->
					<? $i=0;foreach ($folder[$rowjenis['kdjenispustaka']][$rowjur['kdjurusan']]['detthn'] as $rowthn){ $i++;?>
					<p class="thn<?=$rowjenis['kdjenispustaka'].$rowjur['kdjurusan'];?> thn<?=$rowjenis['kdjenispustaka'];?>" style="display: none;margin-left: 30px;">
						<img src="images/tombol/folder.png" title="Direktori" />
						&nbsp;<a href="javascript:goDirectSub('J','<?= $rowjenis['kdjenispustaka']; ?>','<?= $rowjur['kdjurusan']; ?>','<?=$rowthn['tahun'];?>')" style="cursor:pointer;">
					  <?= ucwords(strtolower($rowthn['tahun'])); ?>
					  &nbsp;(
					  <?= $rowthn['jumlah'];?>
					  )</a></p>
					<? } ?>
					<!-- End of Tahun -->
				<? } ?>
				<!-- End of Jurusan -->
			<? } ?>
			<??>
			
			
			
			
			<? } ?>
		  </div>
			<br/>
		  	<div class="col-md-12"><?=$l_lokasi;?></div>

	  </div>
	  
	  <div class="col-md-9">
		
		<h3 class="home-title"><span class="red">Top</span> Buku<br/>
		<small>Bulan <span class="red"><?=Helper::indoMonth(date('m'),true).' '.date('Y')?></span> </small>
		</h3>
		<div id="owl-demo" class="owl-carousel">
			<? $t=0;while($rowtop = $rstop->FetchRow()){
				$t++;
				//$p_foto = Config::dirFoto.'pustaka/'.trim($rowtop['idpustaka']).'.jpg';
				//$p_hfoto = Config::fotoUrl.'pustaka/'.trim($rowtop['idpustaka']).'.jpg';
				$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ".$rowtop['idpustaka'];
			?>
				<div class="item">
					<?
						$rsCover = $connLamp->GetOne($sqlCover);
						$cover = Perpus::loadBlobFile($rsCover, 'jpg'); /*aktifkan lagi nanti*/
						if($cover){
					?>
					<img class="lazyOwl"  src="<?= ( is_null($cover) ) ? Config::fotoUrl.'none.png' : $cover ?>" onclick="showPus('<?=$rowtop['idpustaka']?>')" width="90%" data-container="body" data-toggle="tooltip" data-placement="bottom"  title="<?=$rowtop['noseri'].' - '.$rowtop['judul'];?>">
					<?		
						}else{
					?>
					<div class="none" onclick="showPus('<?=$rowtop['idpustaka']?>')" class="book" data-container="body" data-toggle="tooltip" data-placement="bottom"  title="<?=$rowtop['noseri'].' - '.$rowtop['judul'];?>" style="box-shadow:none; font-size:20px;">
						<?=Helper::word_limiter($rowtop['judul'],3)?>
					</div>					
					<?
						}
					?>
					<!--img class="lazyOwl"  src="<?//= ( is_file($p_foto) ? $p_hfoto : (Config::fotoUrl.'none.png')) ?>" class="book" onclick="showPus('<?//=$rowtop['idpustaka']?>')" width="90%" data-container="body" data-toggle="tooltip" data-placement="bottom"  title="<?//=$rowtop['noseri'].' - '.$rowtop['judul'];?>"-->
				</div>
			<? } ?>
		</div>
	  </div>
	</div>
      
</div>

</body>

<script>
	$(document).ready(function() {

	  $('[data-toggle="tooltip"]').tooltip();

  $('#owl-demo').slick({
    slidesToShow: 5,
	slidesToScroll: 1,
	autoplay: true,
	dots: true,
	speed: 100
  });
  $('#owl-demo2').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  autoplay: true,
	  arrows: false,
	  speed: 100
  });
});
	
	function goPerpus(lokasi){
		var sent = "&lokasi=" + lokasi + "&act=lokasiperpus";
		goLink('contents','<?= Helper::navAddress('list_home.php');?>',sent);
	}
</script>
 </html>
   