<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
		
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	require('classes/pdf.class.php');
	$b_link = new Breadcrumb();
	
	//perngecekkan user
	$a_auth = Helper::checkRoleAuth($connGate);	
	$c_edit = $a_auth['canedit'];
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['LIST_MONITORING'];
	$p_title = $form['LIST_MONITORING'];
	$p_dbtable = "pp_uploadta";
	
	//cek di tabel pp_uploadta
	$userlogin = $_SESSION['idanggota'];
	$sql_cek_mhs = "select * from pp_uploadta where idanggota='$userlogin'";
	$rs_cek = $conn->GetRow($sql_cek_mhs);
	// var_dump($_REQUEST);exit;
	if (!empty($_POST))
	{
		$p_page = Helper::removeSpecial($_REQUEST['page']);
		$r_page = Helper::removeSpecial($_REQUEST['numpage']);
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		$rkey = Helper::removeSpecial($_REQUEST['redit']); //iduploadta
		$idnotifikasi = Helper::removeSpecial($_REQUEST['idnotif']);
		// var_dump($rkey);
		// simpan session ex
		$_SESSION[$p_id.'.page'] = $p_page;
		$_SESSION[$p_id.'.numpage'] = $r_page;
		
		$istrue = false;
		
		//untuk proses penyimpanan
		if ($r_act == 'simpan'){
			$conn->StartTrans();
			$record = array();
			$record = Helper::cStrFill($_POST);
			$record['iduploadta'] = $rkey;
			$record['tglhistoryvalidasi'] = date('Y-m-d');
			$record['npkvalidasi'] = $_SESSION['idanggota'];
			$record['stsvalidasi'] = $_POST['stsvalidasi'];
			$record['catatanvalidasi'] = $_POST['catatanvalidasi'];
			Helper::Identitas($record);
			Perpus::Simpan($conn, $record, 'pp_historyvalidasita');
			
			//insert ke tabel pp_historynotifikasiuploadta====================================================================
			$rs_cek_nrp = $conn->GetRow("select * from pp_uploadta where iduploadta=$rkey");
			
			$rec_histnotif = array();
			$rec_histnotif['iduploadta'] = $rkey;
			$rec_histnotif['tglhistorynotifikasi'] = date('Y-m-d');
			// $rec_histnotif['nrptujuannotifikasi'] = $rs_cek_nrp['nrp1'];
			$rec_histnotif['nrptujuannotifikasi'] = $rs_cek_nrp['idanggota'];
			$rec_histnotif['pesan'] = $record['catatanvalidasi'];
			$rec_histnotif['stsnotifikasi'] = 0;
			Helper::Identitas($rec_histnotif);
			Perpus::Simpan($conn, $rec_histnotif, 'pp_historynotifikasiuploadta');
			
			$recupdate_histnotif = array();
			$recupdate_histnotif['stsnotifikasi'] = 1;
			Perpus::Update($conn, $recupdate_histnotif, 'pp_historynotifikasiuploadta', " idhistorynotifikasi=".$idnotifikasi);
			
			if($record['stsvalidasi']==null or $record['stsvalidasi']==0){ //jika tidak disetujui
				$rec_uploadta = array();
				$rec_uploadta['tglrequestval'] = null;
				$rec_uploadta['userrequestval'] = null;
				
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing1'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing2'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing3'] = date("Y-m-d");
		
				Helper::Identitas($rec_uploadta);
				Perpus::Update($conn, $rec_uploadta, 'pp_uploadta', " iduploadta=".$rkey);
			}else{
				$rec_uploadta = array();
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing1'] = 1;
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing2'] = 1;
				else if($rs_cek_nrp['npkpembimbing3'] == $_SESSION['idanggota'])
					$rec_uploadta['statusvalpembimbing3'] = 1;
			
				if($rs_cek_nrp['npkpembimbing1'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing1'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing2'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing2'] = date("Y-m-d");
				else if($rs_cek_nrp['npkpembimbing3'] == $_SESSION['idanggota'])
					$rec_uploadta['tgllastvalpembimbing3'] = date("Y-m-d");
			
				Helper::Identitas($rec_uploadta);
				Perpus::Update($conn, $rec_uploadta, 'pp_uploadta', " iduploadta=".$rkey);
			}
			
			$conn->CompleteTrans();
			
			if($conn->ErrorNo() != 0){
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Penyimpanan Data Gagal',true).'</div>';
			}else{
				$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penyimpanan Data Berhasil').'</div>';
			}
		}
	}
	else
	{
		// dapatkan nilai ex dari session
		if ($_SESSION[$p_id.'.page'])
			$p_page = $_SESSION[$p_id.'.page'];
			
		if ($_SESSION[$p_id.'.numpage'])
			$r_page = $_SESSION[$p_id.'.numpage'];
	}
	
	if ($r_page != '')
		$p_row = $r_page;
	else
		$p_row = 5; 
		
	//data
	$p_sqlstr = "select * from pp_uploadta u left join pp_historynotifikasiuploadta h  on h.iduploadta=u.iduploadta where (npkpembimbing1 ='".$_SESSION['idanggota']."' or npkpembimbing2 ='".$_SESSION['idanggota']."' or npkpembimbing3 ='".$_SESSION['idanggota']."')
					and userrequestval is not null and h.npktujuannotifikasi='".$_SESSION['idanggota']."' and h.stsnotifikasi=0";
	$rs = $conn->PageExecute($p_sqlstr,$p_row,$p_page);
	
	if($rkey){
		$rs_detailhist = $conn->Execute("select * from pp_historynotifikasiuploadta where iduploadta=$rkey");
	}
	
	if ($rs->EOF) {
		// tidak ditemukan record atau ada kesalahan
		$p_atfirst = true;
		$p_atlast = true;
		$p_lastpage = 0;
		$p_page = 0;
		$p_recount = 0;
	}
	else {
		// ditemukan record
		$p_atfirst = $rs->AtFirstPage();
		$p_atlast = $rs->AtLastPage();
		$p_lastpage = $rs->LastPageNo();
		$p_page = ($rs->_currentPage != '' ? $rs->_currentPage : $p_page); // untuk mencegah penulisan halaman salah
		$p_recount = $rs->_maxRecordCount;
		$showlist = true;
	}
	
	$b_link->add($menu['LIST_MONITORING'], Helper::navAddress('list_uploadmandiri.php').'&paging='.$p_page, 0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
	<form name="perpusform" id="perpusform" action="<?= $i_phpfile?>" method="post" >
	<?= $b_link->output(); ?>
		<div class="profile">
			<div class="contentTop"></div>
			<div class="contentContainer">
				<span class="menuTitle" style="margin-left:0px;"><?= $form['LIST_MONITORING']; ?></span>
				<table width="100%" cellpadding="4" cellspacing="0">
					<tr>
						<td>
							<table width="100%" cellpadding="4" cellspacing="0" class="containerTable">
								<tr>
									<th width="20" align="center">No.</th>
									<th width="105" align="center"><?= $form['BIBLIO_TITLE_TA']; ?></th>
									<th width="250" align="center"><?= $form['NRP']." - ".$form['PENULIS']; ?></th>
									<!--<th width="100" align="center"><?= $form['PENULIS']; ?></th>-->
									<th width="100" align="center"><?= $form['YEAR']; ?></th>
									<th width="10" align="center"><?= $form['ACTION']; ?></th>
								</tr>
								<? 
									$i=0;
									while ($row = $rs->FetchRow()){ $i++;
								?>
								
								<tr>
									<td align="center"><?= $i?></td>
									<td align="center"><?= $row['judul']?></td>
									<td align="left">
										<?if($row['nrp1']!=''){
											echo '1. '.$row['nrp1'].' - '.$row['authorfirst1'].' '.$row['authorlast1'].'<br>';
										}if($row['nrp2']!=''){
											echo '2. '.$row['nrp2'].' - '.$row['authorfirst2'].' '.$row['authorlast2'].'<br>';
										}if($row['nrp3']!=''){
											echo '3. '.$row['nrp3'].' - '.$row['authorfirst3'].' '.$row['authorlast3'];
										}
										?>
									</td>
									<td align="center"><?= $row['tahunterbit']?></td>
									<td align="center"><img src="images/tombol/edited.gif" style="cursor:pointer" onclick="goCekTa('<?= $row['iduploadta'];?>','<?= $row['idhistorynotifikasi']?>')" title="Cek Detail TA"></td>
								</tr>
								<? }?> 
								<? if ($i == 0) {?>
								<tr>
									<td colspan="5" align="center"><?= $form['EMPTY_TABLE']; ?></td>
								</tr>
								<? } ?>
								<tr height="20">
									<td align="left" colspan="6"><strong><?= $form['PAGE']; ?></strong>&nbsp;:&nbsp;
											<?	if($p_page == 0)
													echo '&nbsp;';
												else {
													$start = (($p_page > 9) ? ($p_page-9) : 1);
												$end = ((($p_page+9) < $p_lastpage) ? ($p_page+9) : $p_lastpage);
													$link = array();
													
													for($i=$start;$i<=$end;$i++) {
														if($i == $p_page)
															$link[] = '<strong>'.$i.'</strong>';
														else{
															if (!isset($_REQUEST['isadv']))
																$link[] = '<strong><a href="javascript:goPage('.$i.')">'.$i.'</a></strong>';
														}
													}
													echo implode(' ',$link);
												}
											?>
											&nbsp;<?= UI::createSelect('numpage',array('5'=>'5','10'=>'10', '15'=>'15', '20'=>'20'),$r_page,'controlStyle',true,'onChange="goChange()"'); ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</form>
	<iframe name="upload_iframe" style="display:none;"></iframe>
</body>

<script type="text/javascript" src="script/foredit.js"></script>
<script type="text/javascript" src="script/jquery.common.js"></script>
<script type="text/javascript" src="script/jquery.masked.js"></script>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript" src="script/ajax_perpus.js"></script>
<script type="text/javascript">
	$(function(){
		$("#perpusform").find("#hargausulan").onlyNum();
	   $("#tahunterbit").mask("9999");
	});
	
	function goPage(npage) {
		var sent = "&page=" + npage + "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goChange(npage) {
		var sent = "&numpage=" + $("#numpage").val();
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function goHistory(idupload){
		datahist = "<?= Helper::navAddress('list_historynotifikasi.php'); ?>";
		var sent = "&key=" + idupload;
		$("#contents").divpost({page: datahist, sent: sent});
	}
	
	function addPenerbit(nama) {
		$("#penerbit").val(nama);
	}
	
	function CreateTextbox(){
		var h = document.getElementById('theValue');
		var max = document.getElementById('theValue').value;
		if (max <9){
			var i = (document.getElementById('theValue').value -1)+2;
			h.value=i;
			var x='file'+i;
			var no=i+1;
			createTextbox.innerHTML = createTextbox.innerHTML +"<p>"+no+". <input type=file name='userfile[]' size='40' /> </p>";
		}
	}
	
	function goCekTa(iduploadta,idnotifikasi){
			var sent = "&redit="+iduploadta;
			sent += "&idnotif="+idnotifikasi;
			goLink('contents','<?= Helper::navAddress('val_uploadta.php');?>',sent);
	}
	
	function goDelFile(key1,key2){
		var delfile=confirm("Apakah Anda yakin akan menghapus file upload ?");
		if(delfile){
			sent = '';
			sent += $("#perpusform").serialize();
			sent += "&act=delfile";
			sent += "&delkey="+key1;
			sent += "&delfile="+key2;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
	function goRequestVal(iduploadta){
		var request=confirm("Apakah Anda yakin akan melakukan permintaan validasi?");
		if(request){
			sent = '';
			// sent += $("#perpusform").serialize();
			sent += "&act=requestval";
			sent += "&iduploadta="+iduploadta;
			
			goSubmit('contents','<?= $i_phpfile?>',sent);
		}
	}
	
</script>
</html>
