<? 
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	
	//reload file
	require_once('classes/perpus.class.php');
	require_once('classes/breadcrumb.class.php');
	
	$r_key = Helper::removeSpecial($_REQUEST['key']);
	$r_status = Helper::removeSpecial($_REQUEST['status']);
	
	
	
	$b_link = new Breadcrumb();
	$b_link->add($form['COPIES'], Helper::navAddress('data_eksemplar.php').'&key='.$r_key, 2);
	
	$connLamp = Factory::getConnLamp();
	$connLamp->debug = false;
	$cek = $connLamp->IsConnected();
	
	//setting untuk halaman
	$p_window = "[Digilib] ".$form['COPIES'];
	$p_dbtable = "pp_eksemplar";
	$p_title = $form['COPIES'];

	if (!empty($_POST)){
		
		$r_act = Helper::removeSpecial($_REQUEST['act']);
		
		if ($r_act == 'add'){
			$isreservasi = $conn->GetOne("select count(*) from pp_reservasi where ideksemplarpesan='$r_key' and statusreservasi='1' and to_char(tglexpired,'YYYY-mm-dd') > to_char(current_date,'YYYY-mm-dd') ");
			if($isreservasi>0){
				$confirm = '<div class="error">'.UI::message('Maaf pustaka sudah dipesan oleh anggota perpustakaan',true).'</div>';
			}
			elseif (!isset($_SESSION['skors'])){
				$data = $conn->GetRow("select kdklasifikasi, p.kdjenispustaka,p.idpustaka
						      from pp_eksemplar e
						      left join ms_pustaka p on p.idpustaka=e.idpustaka
						      where ideksemplar='$r_key' ");
				$sql = "select * from pp_aturan where kdjenisanggota='".$_SESSION['jenisanggota']."'";
				$isrole = $conn->GetRow($sql);
				$issave = false;
				if (!empty($isrole)){
					if($_SESSION['cart']!='')						
						$p_check=$conn->GetOne("select * from (select idpustaka from pp_eksemplar where ideksemplar in ($_SESSION[cart])) cek where idpustaka=".$data['idpustaka']."");
					else
						$p_check='';
					
					$p_trans = $conn->GetOne("select idanggota from pp_transaksi where ideksemplar = $r_key and tglpengembalian is null");
					if($p_trans!=$_SESSION['idanggota']) {

					if(!$p_check){
						$istanggung = $conn->GetOne("select 1 from pp_transaksi where ideksemplar='$r_key' and tgltenggat < current_date and statustransaksi='1'");
						if ($istanggung)
							$confirm = '<div class="error">'.UI::message('Maaf Buku ini tidak bisa dipinjam karena keterlambatan pengembalian',true).'</div>';									
						else{
							$jmlpinjam = $conn->GetOne("select count(*) from pp_transaksi where ideksemplar = $r_key and tglpengembalian is null ");
							$max = $conn->GetOne("select count(*) from pp_reservasi where idanggotapesan='$_SESSION[idanggota]' and statusreservasi='1' and tglexpired > current_date");
							$maxi = intval($jmlpinjam) + intval($max);
							if ($maxi < $isrole['batasmaxpinjam'])
							{								
								$items = explode(',',$_SESSION['cart']);
								if($_SESSION['cart']==NULL)
									$count = 0;
								else
									$count = count($items);
								
								if ($count < $isrole['batasmaxpinjam']){
									$isvalid = true;
									//pengecekkan untuk waiting list maks 3 pemesan
									$count = $conn->GetOne("select count(*) from pp_reservasi where ideksemplarpesan='$r_key' and statusreservasi='1' and tglexpired > current_date");
									if ($count > 2)
										$isvalid = false;
															
									if ($isvalid){
										$issave = true;
										Perpus::Cart($r_act,$r_key);
										$confirm = '<div align="center" style="padding-top:20px;" class="success">'.UI::message('Penambahan Pemesanan Berhasil').'</div>';
									}else 
										$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Daftar antrian pesan sudah penuh',true).'</div>';				
								}else
									$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Pemesanan anda sudah maksimal. Anda masih pesan '.$max.' pustaka',true).'</div>';	
							}else
								$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Pemesanan anda sudah maksimal. Anda masih pesan '.$max.' pustaka',true).'</div>';									
						}
					}else
					$confirm = '<div class="error">'.UI::message('Maaf judul pustaka tersebut tidak dapat dipesan ulang',true).'</div>';									
				}
					else
						$confirm = '<div class="error">'.UI::message('Maaf pustaka tidak dapat dipesan oleh Peminjamnya',true).'</div>';
				}else{
					if(empty($_SESSION['userid'])){
						$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Anda Belum Login, Silahkan Login terlebih dahulu',true).'</div>';		
						$_SESSION['xideksemplar'] = $r_key;
						$_SESSION['xstatus'] = $r_status;
					}else
						$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Aturan Belum Ada. Hubungi Perpustakaan PJB',true).'</div>';		
				}	
			}else							
				$confirm = '<div align="center" style="padding-top:20px;" class="error">'.UI::message('Maaf Anda Terkena Skors.',true).'</div>';
				
			if ($issave)
				Helper::addHistory($conn, "Pemesanan Eksemplar ", "eksemplar ".$_SESSION['cart']);
			else
				Helper::addHistory($conn, $confirm, "eksemplar ".$r_key);
		}
	}
	
	$p_sqlstr = "select e.*,e.noseri,p.noseri as seripus,p.kdjenispustaka,j.islokal, p.judul,v.namalokasi
		from pp_eksemplar e 
		left join ms_pustaka p on p.idpustaka=e.idpustaka
		left join lv_lokasi v on v.kdlokasi=e.kdlokasi
		left join lv_jenispustaka j on p.kdjenispustaka=j.kdjenispustaka
		left join ms_rak r on e.kdrak = r.kdrak
		where e.ideksemplar='$r_key'";
	$row = $conn->GetRow($p_sqlstr);

	if (isset($_SESSION['userid'])){
		if ($r_status == 'PJM'){
			$sql = "select * from ms_anggota a 
					left join pp_transaksi t on t.idanggota=a.idanggota 
					where t.ideksemplar='$r_key' and statustransaksi='1' limit 1";
			$data = $conn->GetRow($sql);
			
			$sql = "select * from pp_reservasi r 
					left join ms_anggota a on a.idanggota=r.idanggotapesan
					where ideksemplarpesan='$r_key' and r.tglexpired > current_date and statusreservasi='1' order by tglreservasi";
			$rs = $conn->Execute($sql);
		}

		$c_down = true;
		
	}
	
	$sqlCover = "select isifile from lampiranperpus where jenis = 1 and idlampiran = ". $row['idpustaka'];
	$rsCover = $connLamp->GetOne($sqlCover);
	
	$p_foto = Config::dirFoto.'pustaka/'.trim($row['idpustaka']).'.jpg';
	$p_hfoto = Config::fotoUrl.'pustaka/'.trim($row['idpustaka']).'.jpg';
		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $p_window ?></title>
<script type="text/javascript" src="script/auth.js"></script>
</head>
<body>	
<div class="container">
  <div class="col-md-9">
    <div class="primary">
      <div>
	<?= $b_link->output(); ?>
	
	<div class="bookDetail">
		<div class="contentTop">&nbsp;</div>
		<div class="contentContainer" style="padding-bottom:10px;">
		<h4 class="sidebar-title pull-left"><?= $p_title;?></h4>
        	<?= (!$confirm) ?  '' : $confirm; ?>
			<? if (isset($_SESSION['userid'])) {
						?>
						<button class="btn btn-success pull-right" style="cursor:pointer" onclick="addCart()"><span class="glyphicon glyphicon-check"></span>&nbsp; Pinjam Buku Ini</button>
						<?php	
							}					
						?>
			<div class="clearfix"></div>
			<br/>
			<div class="well">
			<table width="98%" cellpadding="10" cellspacing="10" border="0">
				<tr>
					<td colspan="4" class="judul"><b><?= $row['judul'] ?></b></td>
				</tr>
				<tr>
					<td valign="top" rowspan="9" width="105" align="center" style="padding-left:12px;padding-right:12px;">
						<?php
							$cover = Perpus::loadBlobFile($rsCover, 'jpg');
						?>
						<img id="imgfoto" src="<?= (is_null($cover)) ? Config::fotoUrl.'none.png' : $cover ?>" width="100" height="108">
					</td>
					<td valign="top" class="resultSub" width="150"><?= $rowp['islokal']==1 ? 'KODE' : $form['REG_COMP'] ?></td>
					<td valign="top" class="resultSub" width="1">:</td>
					<td valign="top" width="427" style="padding-left:5px;" class="resultSub"><?= $row['noseri']; ?></td>
				</tr>
				<? if($row['islokal']!=1) { ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['NO_CALL'] ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['nopanggil']; ?>
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['ISBN'] ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $row['isbn']; ?>
					</td>
				</tr>
				<? } ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['LOCATION'] ."<br>" ?></td>
					<td class="resultSub" style="padding-top:5px;">:<br></td>
					<td style="padding-top:2px;padding-left:5px;line-height:15px" class="resultSub">
						<?= $row['namalokasi']; ?>
					</td>
				</tr>
				
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['STATE'] ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= $r_status =='ADA' ? $form['P_AVAILABLE'] : $form['P_LOANS']; ?>
					</td>
				</tr>
				<? if($row['islokal']==1) { ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;">&nbsp;</td>
					<td class="resultSub" style="padding-top:5px;">&nbsp;</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">&nbsp;
					
					</td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;">&nbsp;</td>
					<td class="resultSub" style="padding-top:5px;">&nbsp;</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">&nbsp;
					
					</td>
				</tr>
				<? } ?>
				<? if ($r_status == 'PJM' and isset($_SESSION['userid'])){ ?>
				<tr>
					<td class="resultSub" style="padding-top:5px;"><?= $form['EXPIRED_DATE'] ?></td>
					<td class="resultSub" style="padding-top:5px;">:</td>
					<td valign="top" style="padding-top:5px;padding-left:5px;" class="resultSub">
						<?= Helper::formatDate($data['tgltenggat']); ?>
					</td>
				</tr>
				<? } ?>
			</table><br />
			</div>
			<? 
			if ($r_status == 'PJM' and isset($_SESSION['userid'])){ ?>
			<table width="98%" cellpadding="1" cellspacing="0">
				<tr>
					<td colspan="2">
						<h4 class="sidebar-title"><?= $form['P_LOANMEMBERDATA'];?></h4>
					</td>
				</tr>
				<tr>
					<td class="resultSub" width="200" style="padding-top:5px;padding-left:12px">NRP</td>
					<td class="resultSub" style="padding-top:5px;">: <?= $data['idanggota']?></td>
				</tr>
				<tr>
					<td class="resultSub" style="padding-top:5px;padding-left:12px"><?= $form['MEMBER_NAME'] ?></td>
					<td class="resultSub" style="padding-top:5px;">: <?= $data['namaanggota']?></td>
				</tr>
			</table>
			<br />
			<table width="98%" cellpadding="1" cellspacing="0">
				<tr>
					<td colspan="2">
						<h4 class="sidebar-title"><?= $form['P_RESMEMBERDATA'];?></h4>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:12px">
						<table width="100%" cellpadding="2" cellspacing="0" class="containerTable">
							<tr>
								<th align="center" width="120"><?= $form['MEMBER_NAME'] ?></th>
								<th align="center" width="130"><?= $form['EMAIL'] ?></th>
								<th align="center" width="109"><?= $form['RESERVATION_DATE'] ?></th>
							</tr>
							<? 	$i=0;
								while ($rows = $rs->FetchRow()) { $i++;?>
							<tr>
								<td><?= $rows['namaanggota']?></td>
								<td><?= $rows['email']?></td>
								<td align="center"><?= Helper::formatDate($rows['tglreservasi']);?></td>
							</tr>
							<? }if ($i == 0){ ?>
							<tr>
								<td colspan="3" align="center"><?= $form['EMPTY_TABLE']; ?></td>
							</tr>
							<? } ?>
						</table>
					</td>
				</tr>
			</table>
			<? } ?>
		</div>
	</div>
	
    </div></div>
  </div>
  <div class="col-md-3">
  <?php include ('inc_sidebar.php'); ?>
  </div>
</div>
  
</body>
<script src="script/linkis.js" type="text/javascript"></script>
<script type="text/javascript">
	function addCart(){
		sent = "&act=add&key=<?= $r_key?>&status=<?= $r_status; ?>";
		goLink('contents','<?= $i_phpfile?>', sent);
	}
	
	function showCart(){
		sent = "";
		goLink('contents','<?= Helper::navAddress('data_cart.php')?>', sent);
	}
	
	function getLook(file,seri){
		window.open("<?= Helper::navAddress('showOff.php')?>&kode="+seri+"&file="+file,"_blank");
	}
	
	function getLooks(file,seri,det){
		window.open("<?= Helper::navAddress('view_pdfseri.php')?>&kode="+seri+"&det="+det+"&file="+file,"_blank");
	}
</script>
</html>
<?
if(isset($_SESSION['xideksemplar']) and isset($_SESSION['userid'])){
	unset($_SESSION['xideksemplar']);
	unset($_SESSION['xstatus']);
}
?>