<?php
	defined( '__VALID_ENTRANCE' ) or die( 'Akses terbatas' );
	//$conn->debug=true;

	if($_SESSION['lang']=='en')
		$rs = $conn->Execute("select idhelp,namahelpen as menu from pp_helpdesk where tampil='1' order by idhelp");
	else
		$rs = $conn->Execute("select idhelp,namahelp as menu from pp_helpdesk where tampil='1' order by idhelp");

	$r_key = Helper::removeSpecial($_POST['key']);
	
	if($r_key=='')
		$r_key = 1;
	
	if($r_key){
		if($_SESSION['lang']=='en')
			$ket = $conn->GetOne("select keteranganen as ket from pp_helpdesk where idhelp=$r_key");
		else
			$ket = $conn->GetOne("select keterangan as ket from pp_helpdesk where idhelp=$r_key");
	}
?>

<html>
	<head>
		<title>
		Digilib Guide Support
		</title>
		<style type="text/css">
		body,td {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 8pt;		
		}
		</style>
	</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border-bottom:5px solid #CC0000; color:#fff; padding:0 10px;margin:0;position:fixed;top:0px;left:0px;background:#003366; height:45px">
			<tr>
				<td><img src="images/logo_.png" alt="" height="45px"></td>
				<td align="right" valign="middle" style="padding-top:4px; padding-right:10px;">
					<h3>Guide / Help</h3>
				</td>
			</tr>
		</table>
		<br/>
<table width="100%" cellpadding=0 cellspacing=0>
	<tr>
		<td height="23">
			<?
			while($row = $rs->FetchRow()){ ?>
			[ <a href="javascript:goGuide('<?= $row['idhelp'] ?>')" title="Detail Guide" ><?= $row['menu'] ?></a> ] 
			<? } ?>
		</td>
	</tr>
	<tr>
		<td style="padding:20px; text-align:justify">
		<br>
		<?= $ket ?>
		</td>
	</tr>
</table>

<form name="digiform" id="digiform" method="post">
	<input type="hidden" name="key" id="key" value="<?= $r_key ?>">
</form>

</body>
<script type="text/javascript">
	function goGuide(idhelp) {
		document.getElementById('key').value = idhelp;
		document.digiform.submit();
	}
</script>
</html>