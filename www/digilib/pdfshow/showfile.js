function showTime(npage,nfile){
  PDFJS.getDocument(nfile).then(function(pdf) {
  // Using promise to fetch the page
    if(npage)
      np = npage;
    else
      np = 1;
    
    pdf.getPage(np).then(function(page) {
      var scale = 1;
      var viewport = page.getViewport(scale);
  
      //
      // Prepare canvas using PDF page dimensions
      //
      var canvas = document.getElementById('the-canvas');
      var context = canvas.getContext('2d');
      canvas.height = 900;//viewport.height;
      canvas.width = 600;//viewport.width;
  
      //
      // Render PDF page into canvas context
      //
      var renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      page.render(renderContext);
    });
  });

}
