<?php

class Sqldebug
{

    public function showDebug() {
        $ci = &get_instance();

		$error = $ci->load->error_sql;
		
		if ($ci->config->item('debug_db') and $ci->db->queries) {
			foreach ($ci->db->queries as $x => $sql) {
				echo '<div style="border-bottom:1px solid black;padding:3px">' . $sql . '</div>';
				if(!empty($error[$x]))
					echo '<div style="border-bottom:1px solid red;color:red;padding:3px">' . $error[$x] . '</div>';
			}
		}

    }
    
}
