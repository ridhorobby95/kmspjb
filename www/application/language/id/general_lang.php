<?php

$lang['page_title'] = 'Knowledge Management PJB';

$lang['data_updated'] = 'Data berhasil diubah';
$lang['data_updated_fail'] = 'Data gagal diubah';
$lang['data_added'] = 'Data berhasil ditambahkan';
$lang['data_added_fail'] = 'Data gagal ditambahkan';
$lang['data_deleted'] = 'Data berhasil dihapus';
$lang['data_deleted_fail'] = 'Data gagal dihapus';
$lang['are_you_sure_delete'] = 'Anda yakin akan menghapus data ini?';
$lang['filter_on'] = 'Filter ON';
$lang['login_fail'] = 'Login gagal';
$lang['your_account_inactive'] = 'Akun anda dinonaktifkan. Silakan hubungi Admin.';
$lang['too_many_login_trial'] = 'Percobaan login terlalu banyak. Silakan tutup dan buka kembali browser anda.';

$lang['kembali'] = 'Kembali';
$lang['home'] = 'Home';
$lang['menu'] = 'Menu';
$lang['role'] = 'Role';
$lang['user'] = 'User';
$lang['modul'] = 'Modul';
$lang['menumodul'] = 'Menu Modul';
$lang['departemen'] = 'Departemen';

$lang['no_data_found'] = 'Tidak ada data';
$lang['no_page'] = 'Halaman tidak tersedia';
$lang['no_page_message'] = 'Halaman yang diakses tidak tersedia, mohon dicek kembali url anda.';
$lang['no_data_no_right'] = 'Tidak ada data yang bisa ditampilkan, atau anda tidak berhak mengakses halaman ini.';
