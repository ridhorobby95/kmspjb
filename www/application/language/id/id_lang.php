<?php

$lang['data_added'] = 'Data sukses ditambahkan';
$lang['data_updated'] = 'Data sukses diubah';
$lang['data_deleted'] = 'Data sukses dihapus';

$lang['data_added_fail'] = 'Data gagal ditambahkan. Mohon dicek data yang dimasukkan.';
$lang['data_updated_fail'] = 'Data gagal diubah. Mohon dicek data yang dimasukkan.';
$lang['data_deleted_fail'] = 'Data gagal dihapus';
