<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pdf {
    
    function &getPdf() {
		static $instance;

		if (!is_object($instance)) {
            require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
            //require_once(APPPATH.'libraries/tcpdf/config/lang/eng.php');

			//$page_layout = array(600, 700);
			//new TCPDF('p', 'pt', $pageLayout, true, 'UTF-8', false);
			//$instance = new TCPDF('p', 'px', $page_layout, true, 'UTF-8', false);
			$instance = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			//$instance = new TCPDF(PDF_PAGE_ORIENTATION, 'cm', 'A4', true, 'UTF-8', false);
		}

		return $instance;
    }


}

