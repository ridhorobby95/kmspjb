<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class PHPMailer{
    
    function PHPMailer() {
        if(!class_exists('PHPMailer')) {
            require_once(APPPATH.'libraries/phpmailer/class.phpmailer.php');
            require_once(APPPATH.'includes/phpmailer/class.smtp.php');
        }
            
    }

    function &getMailer() {
		static $instance;

		if (!is_object($instance)) {
			$instance = & PHPMailer();
		}

		return $instance;
    }


}

