<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mailer{
    
    function &getMailer() {
		static $instance;

		if (!is_object($instance)) {
            require_once(APPPATH.'libraries/phpmailer/class.phpmailer.php');
            require_once(APPPATH.'libraries/phpmailer/class.smtp.php');

			$instance = new PHPMailer();
		}

		return $instance;
    }


}

