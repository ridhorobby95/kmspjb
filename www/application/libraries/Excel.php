<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Excel{

    function &getExcel() {
		static $instance;

		if (!is_object($instance)) {
			require_once(APPPATH.'libraries/phpexcel/PHPExcel.php');
			require_once(APPPATH.'libraries/phpexcel/PHPExcel/IOFactory.php');
			$instance = new PHPExcel();
		}

		return $instance;
    }

}

