<?php
    /**
     * Kumpulan fungsi compress file
     * @author Sevima
     * @version 1.0
     */
    class Zip 
    {
        /**
        * Digunakan untuk extract file zip
        * @param String source file url 
        * @param String destination file url 
        * @param String crete zip name 
        * @param Boolean overwrite 
        */    
        function unzip($src_file, $dest_dir=false, $create_zip_name_dir=true, $overwrite=true)
        {
            if ( !is_file($src_file) ) 
                return array(1, 'File tidak ada');
            
            $zip = zip_open($src_file);
            
            if ( !is_numeric($zip) )
            {
                
                $splitter = ($create_zip_name_dir === true) ? "." : "/";
                if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter))."/";

                // Create the directories to the destination dir if they don't already exist
                static::create_dirs($dest_dir);
                
                // For every file in the zip-packet
                while ($zip_entry = zip_read($zip))
                {
                    // If the file is not in the root dir
                    $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
                    
                    if ($pos_last_slash !== false)
                    {
                      // Create the directory where the zip-entry should be saved (with a "/" at the end)
                      static::create_dirs($dest_dir.substr(zip_entry_name($zip_entry), 0, $pos_last_slash+1));
                    }

                    // Open the entry
                    if (zip_entry_open($zip,$zip_entry,"r"))
                    {
                      // The name of the file to save on the disk
                      $file_name = $dest_dir.zip_entry_name($zip_entry);
                     
                      // Check if the files should be overwritten or not
                      if ($overwrite === true || $overwrite === false && !is_file($file_name))
                      {
                          // Get the content of the zip entry
                          $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

                          $z = fopen($file_name, "w");
                          fwrite($z, $fstream);
                          fclose($z);

                          chmod($file_name, 0777);
                      }
                     
                      // Close the entry
                      zip_entry_close($zip_entry);
                    }      
                }
                // Close the zip-file
                zip_close($zip);
            
                return array(false, 'File berhasil diextract', $dest_dir);
            }
            else
            {
                return array(true, 'File tidak dapat dibaca');
            }
        }
        
        /**
        * This function creates recursive directories if it doesn't already exist
        *
        * @param String  The path that should be created
        * 
        * @return  void
        */
        function create_dirs($path)
        {
            if (!is_dir($path))
            {
                $directory_path = "";
                $directories = explode("/",$path);
                
                array_pop($directories);

                foreach($directories as $directory)
                {
                    $directory_path .= $directory."/";
                    
                    if (!is_dir($directory_path))
                    {
                        mkdir($directory_path);
                        
                        chmod($directory_path, 0777);
                    }
                }
            }
        }
    }
?>