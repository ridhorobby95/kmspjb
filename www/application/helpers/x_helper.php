<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function xIsLoggedIn() {
		$ci = &get_instance();
		$ci->data['logged_in'] = false;

		if ($_SESSION[G_SESSION]['remote_addr'] == $_SERVER['REMOTE_ADDR']
			&& $_SESSION[G_SESSION]['user_agent'] == $_SERVER['HTTP_USER_AGENT']
			&& $_SESSION[G_SESSION]['iduser']) {
			
			$ci->data['logged_in'] = true;
			return true;
		}

		return false;
	}
	
	function xIsAuth($page, $modul='1', $access='ISREAD') {
		$ci = &get_instance();
		return $ci->isAuth($page, $modul, $access);
	}
	
	function xUserLog($keterangan) {
		$ci = &get_instance();
		
		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser'];
		$record['namauser'] = substr($_SESSION[G_SESSION]['nama'],0,100);
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		$record['keterangan'] = substr($keterangan,0,300);
		dbInsert("{$ci->um}.userlog", $record);
	}
	
	function xKMLog($idknowledge, $keterangan) {
		$ci = &get_instance();

		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser'];
		$record['namauser'] = substr($_SESSION[G_SESSION]['nama'],0,100);
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		$record['keterangan'] = substr($keterangan,0,300);
		$record['idknowledge'] = $idknowledge;
		dbInsert("{$ci->km}.kmlog", $record);
	}
	
	function xLihatLog($idknowledge) {
		$ci = &get_instance();
		
		$iduser = $_SESSION[G_SESSION]['iduser'];
		
		$sql = "select nvl(extract(hour from (sysdate - max(tinserttime))),1) from {$ci->km}.lihat where idknowledge='$idknowledge' and iduser=$iduser ";
		$hour = dbGetOne($sql);
		
		if ($hour < 1)
			return false;
		
		$record = array();
		$record['idknowledge'] = $idknowledge;
		$record['iduser'] = $iduser;
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		dbInsert("{$ci->km}.lihat", $record);
	}

	function xDownloadLampiranLog($idlampiran) {
		$ci = &get_instance();
		
		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser'];
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		$record['idlampiran'] = $idlampiran;
		dbInsert("{$ci->km}.downloadlampiran", $record);
	}
	
	function xDownloadFotoLog($idfoto) {
		$ci = &get_instance();
		
		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser'];
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		$record['idfoto'] = $idfoto;
		dbInsert("{$ci->km}.downloadfoto", $record);
	}
	
	function xLastPhotos() {
		$ci = &get_instance();
		
		$num = 6;
		$sql = "select f.idfoto, f.namafile, k.nama as namaknowledge, k.judul as judulknowledge, RAWTOHEX(k.idknowledge) as idknowledge_h 
			from {$ci->km}.foto f 
			join {$ci->km}.knowledge k on k.idknowledge=f.idknowledge and k.isdelete=0
			join {$ci->lp}.lampiranfotokm l on f.idfoto=l.idfoto where rownum<=$num order by f.tinserttime desc";
		return dbGetRows($sql);
	}

	function xLastFiles() {
		$ci = &get_instance();
		
		$num = 5;
		$sql = "select f.idlampiran, f.namafile, k.nama as namaknowledge, k.judul as judulknowledge, k.idjeniskm, j.nama as namajeniskm, j.singkat, RAWTOHEX(k.idknowledge) as idknowledge_h 
			from {$ci->km}.lampiran f 
			join {$ci->km}.knowledge k on k.idknowledge=f.idknowledge and k.isdelete=0
			join {$ci->km}.jeniskm j on j.idjeniskm=k.idjeniskm
			join {$ci->lp}.lampirankm l on f.idlampiran=l.idlampiran where rownum<=$num order by f.tinserttime desc";
		return dbGetRows($sql);
	}
	
	function xPopulerKM() {
		$ci = &get_instance();
		
		$num = 5;
		$sql = "select * from (select j.singkat,k.nama as namaknowledge, to_char(createdtime, 'DD Mon YYYY HH24:MI') as createdtime_h, k.judul as judulknowledge, k.idjeniskm, j.nama as namajeniskm, RAWTOHEX(k.idknowledge) as idknowledge_h 
			from {$ci->km}.populerkm p
			join {$ci->km}.knowledge k on k.idknowledge=p.idknowledge and k.isdelete=0
			join {$ci->km}.jeniskm j on j.idjeniskm=k.idjeniskm order by p.skor desc)
			where rownum<=$num";
		$rows = dbGetRows($sql);
		foreach ($rows as &$row) {
			$sql = "select f.* from {$ci->km}.lampiran f JOIN {$ci->lp}.lampirankm l on f.idlampiran=l.idlampiran where f.idknowledge='{$row['IDKNOWLEDGE_H']}' and rownum=1";
			$row2 = dbGetRow($sql);
			$row['IDLAMPIRAN'] = $row2['IDLAMPIRAN'];
			$row['NAMAFILE'] = $row2['NAMAFILE'];
		}
		return $rows;
	}

	function xLastActivitySess() {
		$ci = &get_instance();
		
		$secs = $ci->config->item('sess_expiration');
		
		if (isset($_SESSION['G_SESSION']['LAST_ACTIVITY']) && (time() - $_SESSION['G_SESSION']['LAST_ACTIVITY'] > $secs)) {
			session_unset();
			session_destroy();
		}
		$_SESSION['G_SESSION']['LAST_ACTIVITY'] = time();		
	}
	
	function xGetUnits() {
		$ci = &get_instance();

		$sql = "select kodeunit, nama from {$ci->um}.unit order by nama";
		return dbGetRows($sql);
	}

	function xRemoveSpecial($mystr,$stripapp=true) {
		// $pattern = '/[%&;()\"';
		$pattern = '/[%&;\"';
		if($stripapp === false) // tidak stripping ', tapi direplace jadi '', biasanya dipakai di nama
			$mystr = str_replace("'","''",$mystr);
		else
			$pattern .= "\'";
		if(preg_match('/[A-Za-z%\/]/',$mystr)) // kalau ada alfabet, %, atau /, strip <>
			$pattern .= '<>';
		$pattern .= ']|--/';
		
		return preg_replace($pattern, '$1', $mystr);
	}
	
	function xStatusAktif($status, $on=false, $off=false) {
		if ($on && $off)
			return $status?'<span class="label label-success">'.$on.'</span>':'<span class="label label-danger">'.$off.'</span>';
		return $status?'<span class="label label-success">Aktif</span>':'<span class="label label-danger">Tidak Aktif</span>';
	}
	
	function xFormatNumber($num) {
		return number_format($num,0,',',',');
	}

	function xFormatNumberDec($num) {
		return number_format($num,2,'.',',');
	}
	
	function xStripNumber($number, $str=',') {
		return str_replace($str,'',$number);
	}

	function xSetList($sql) {
		$list = array();
		$rs = dbGetRows($sql);
		foreach ($rs as $row) {
			$list[$row['ID']] = $row['VAL'];
		}
		return $list;
	}
	
	function xIsInRangeTime($start, $end, $now=null) {
		if (!$now) {
			$now = date('Y-m-d');
		}
		
		if ($start > $now || $end < $now)
			return false;
		
		return true;
	}
	
	function xIsValidDate($str) {
		if (preg_match('/-/',$str)) {
			$date = explode("-", $str);
			$month = (int)$date[1];
			$day = (int)$date[2];
			$year = (int)$date[0];
			return checkdate($month,$day,$year);
		}
		return false;
	}
	
	function xGetDiffDate($date1, $date2) {
		$diff = abs($date2 - $date1);
		return floor($diff / (60*60*24));
	}
	
	function xCheckDateKeyExist($key_date_list, $start, $end) {
		$date1 = strtotime($start);
		$date2 = strtotime($end);
		$diff = xGetDiffDate($date1, $date2);
		
		$idx = 0;
		$begin = $date1;
		for ($i=0;$i<$diff;$i++) {
			$date = strtotime("+$i day", $begin);
			$date_str = date('Y-m-d',$date);
			
			if (!key_exists($date_str, $key_date_list)) {
				return false;
			}
		}
		return true;
	}
	
	function xPageTitle($page_title=null) {
		$ret = '';
		if ($page_title){
			$ret = "<div class=\"panel panel-heading\"><h3 style=\"font-family:Georgia,serif;margin:0;margin-bottom:10px\">{$page_title}</h3></div>";
		}
		return $ret;
	}

	function xHTMLTextBox($nameid,$value='',$maxlength='',$size='',$edit=true,$class='',$add='',$password=false) {
		if(!empty($edit)) {
			if ($password)
				$tb = '<input type="password" name="'.$nameid.'" id="'.$nameid.'"';
			else
				$tb = '<input type="text" name="'.$nameid.'" id="'.$nameid.'"';
			if($value != '') $tb .= ' value="'.$value.'"';
			if($class != '') $tb .= ' class="'.$class.'"';
			if($maxlength != '') $tb .= ' maxlength="'.$maxlength.'"';
			if($size != '') $tb .= ' size="'.$size.'"';
			if($add != '') $tb .= ' '.$add;
			$tb .= '>';
		}
		else if($value == '')
			$tb = '&nbsp;';
		else
			$tb = $value;
		
		return $tb;
	}

	function xHTMLTextArea($nameid,$value='',$rows='',$cols='',$edit=true,$class='',$add='') {
		if(!empty($edit)) {
			$ta = '<textarea wrap="soft" name="'.$nameid.'" id="'.$nameid.'"';
			if($class != '') $ta .= ' class="'.$class.'"';
			if($rows != '') $ta .= ' rows="'.$rows.'"';
			if($cols != '') $ta .= ' cols="'.$cols.'"';
			if($add != '') $ta .= ' '.$add;
			$ta .= '>';
			if($value != '') $ta .= $value;
			$ta .= '</textarea>';
		}
		else if($value == '')
			$ta = '&nbsp;';
		else
			$ta = nl2br($value);
		
		return $ta;
	}
	
	function xHTMLSelect($nameid,$arrval='',$value='',$edit=true,$class='',$add='',$emptyrow=false) {
		if(!empty($edit)) {
			$slc = '<select name="'.$nameid.'" id="'.$nameid.'"';
			if($class != '') $slc .= ' class="'.$class.'"';
			if($add != '') $slc .= ' '.$add;
			$slc .= ">\n";
			if($emptyrow)
				$slc .= '<option></option>'."\n";
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					$slc .= '<option value="'.$key.'"'.(!strcasecmp($value,$key) ? ' selected' : '').'>';
					$slc .= $val.'</option>'."\n";
				}
			}
			$slc .= '</select>';
		}
		else {
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					if(!strcasecmp($value,$key)) {
						$slc = $val;
						break;
					}
				}
			}
			if(!isset($slc))
				$slc = '&nbsp;';
		}
		
		return $slc;
	}

	function xHTMLCheckBox($nameid,$valuecontrol='',$value='',$edit=true,$class='',$add='') {
		if (!$edit && $value == $valuecontrol) {
			return '<span class="label label-success"><i class="glyphicon glyphicon-ok glyphicon-white"></i></span>';
		}

		$tb = '<input type="checkbox" name="'.$nameid.'" id="'.$nameid.'"';
		if($valuecontrol != '') {
			$tb .= ' value="'.$valuecontrol.'"';
			if ($value == $valuecontrol)
				$tb .= ' checked ';
		}
		if($class != '') $tb .= ' class="'.$class.'"';
		if($add != '') $tb .= ' '.$add;
		if(!$edit)
			$tb .= ' disabled ';
		$tb .= '>';
		
		return $tb;
	}
	
	function xHTMLCheckBoxLabel($nameid,$valuecontrol='',$value='',$edit=true,$class='',$add='',$label='') {
		$style = '';
		if (!$edit && $value)
			$style = ' style="margin:0;padding:0"';
	
		echo "<label class=\"checkbox\" for=\"$nameid\" $style >";
		echo xHTMLCheckBox($nameid,$valuecontrol,$value,$edit);
		echo " $label</label>";
	}
	
	function xHTMLRadio($nameid,$arrval='',$value='',$edit=true,$br=false,$class='',$add='') {
		$radio = '';
		
		if(empty($class)) {
			if($br)
				$class = 'radio';
			else
				$class = 'radio inline';
		}
		
		if(!empty($edit)) {
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					// $radio .= '<input type="radio" name="'.$nameid.'" id="'.$nameid.'_'.$key.'" value="'.$key.'"'.(!strcasecmp($value,$key) ? ' checked' : '').' '.$add.'> ';
					// $radio .= '<label for="'.$nameid.'_'.$key.'">'.$val.'</label>'.($br ? '<br>' : ' &nbsp; ')."\n";
					
					$radio .= '<label'.(empty($class) ? '' : ' class="'.$class.'"').'><input type="radio" name="'.$nameid.'" value="'.$key.'"'.(!strcasecmp($value,$key) ? ' checked' : '').' '.$add.'> '.$val.'</label>'.($br ? '<br>' : ' &nbsp; ')."\n";
				}
			}
		}
		else {
			if(is_array($arrval)) {
				foreach($arrval as $key => $val) {
					if(!strcasecmp($value,$key)) {
						$radio = $val;
						break;
					}
				}
			}
		}
		
		return $radio;
	}

	// mengubah format tanggal dari dd-mm-yyyy menjadi yyyy-mm-dd dan sebaliknya
	function xFormatDate($dmy,$delim='-',$todelim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		list($y,$m,$d) = explode($delim,substr($dmy,0,10));
		return $d.$todelim.$m.$todelim.$y;
	}
	
	// mengubah format tanggal dari yyyy-mm-dd menjadi format indonesia
	function xFormatDateInd($ymd,$full=true,$dmy=false,$delim='-',$withtime=false) {
		if ($withtime)
			$time = substr($ymd,11,5);

		if($ymd == '')
			return '';
		if($ymd == 'null')
			return 'null';
		
		if($dmy)       
			list($d,$m,$y) = explode($delim,substr($ymd,0,10));
		else
			list($y,$m,$d) = explode($delim,substr($ymd,0,10));
		
		return (int)$d.' '.xIndoMonth($m,$full).' '.$y. ' ' . $time;
	}
	
	// mengubah format tanggal, tapi ada time dibelakangnya
	function xFormatDateTime($dmy,$delim='-') {
		if($dmy == '')
			return '';
		if($dmy == 'null')
			return 'null';
		return xFormatDate(substr($dmy,0,10)).substr($dmy,10,6);
	}
	
	// mengubah format hhii menjadi hh:ii
	function xFormatTime($hi,$delim=':') {
		if($hi == '')
			return '';
		
		$hi = str_pad($hi,2,'0',STR_PAD_LEFT);
		return substr($hi,0,2).':'.substr($hi,2,2);
	}

	// nama hari di bahasa indonesia
	function xIndoDay($nhari,$full=true) {
		if($full) {
			switch($nhari) {
				case 0: return "Minggu";
				case 1: return "Senin";
				case 2: return "Selasa";
				case 3: return "Rabu";
				case 4: return "Kamis";
				case 5: return "Jumat";
				case 6: return "Sabtu";
			}
		}
		else {
			switch($nhari) {
				case 0: return "Min";
				case 1: return "Sen";
				case 2: return "Sel";
				case 3: return "Rab";
				case 4: return "Kam";
				case 5: return "Jum";
				case 6: return "Sab";
			}
		}
	}
	
	// nama bulan di bahasa indonesia
	function xIndoMonth($nbulan,$full=true) {
		if($full) {
			switch($nbulan) {
				case 1: return "Januari";
				case 2: return "Februari";
				case 3: return "Maret";
				case 4: return "April";
				case 5: return "Mei";
				case 6: return "Juni";
				case 7: return "Juli";
				case 8: return "Agustus";
				case 9: return "September";
				case 10: return "Oktober";
				case 11: return "November";
				case 12: return "Desember";
			}
		}
		else {
			switch($nbulan) {
				case 1: return "Jan";
				case 2: return "Feb";
				case 3: return "Mar";
				case 4: return "Apr";
				case 5: return "Mei";
				case 6: return "Jun";
				case 7: return "Jul";
				case 8: return "Agu";
				case 9: return "Sep";
				case 10: return "Okt";
				case 11: return "Nov";
				case 12: return "Des";
			}
		}
	}
	
	function xGetDayIndex($str) {
		if ($str == 'monday')
			return '1';
		elseif ($str == 'tuesday')
			return '2';
		elseif ($str == 'wednesday')
			return '3';
		elseif ($str == 'thursday')
			return '4';
		elseif ($str == 'friday')
			return '5';
		elseif ($str == 'saturday')
			return '6';
		elseif ($str == 'sunday')
			return '7';

		return false;
	}

	function xGetDayString($idx) {
		if ($idx == '1')
			return 'monday';
		elseif ($idx == '2')
			return 'tuesday';
		elseif ($idx == '3')
			return 'wednesday';
		elseif ($idx == '4')
			return 'thursday';
		elseif ($idx == '5')
			return 'friday';
		elseif ($idx == '6')
			return 'saturday';
		elseif ($idx == '7')
			return 'sunday';

		return false;
	}
	
	function xTime2String($date, $format='constant1', $time_zone=0) {
		$ci = &get_instance();
		
		if (!is_numeric($date))
			return false;

		$num_year = 31104000;
		$num_mon = 2592000;
		$num_day = 86400;
		$num_hour = 3600;

		if ($format === 'short') {
			$time_temp = time() - $date;

			if ($time_temp > $num_year) {
				$years = (int)($time_temp / $num_year);
				$months = (int) (($time_temp % $num_year) / $num_mon);
				$date_ret = "{$years} " . lang('g_years') . "&nbsp;&nbsp;{$months} " . lang('g_months');
			}
			elseif ($time_temp > $num_mon) {
				$months = (int)($time_temp / $num_mon);
				$days = (int) (($time_temp % $num_mon) / $num_day);
				$date_ret = "{$months} " . lang('g_months');
				if ($days)
					$date_ret .= "&nbsp;&nbsp;{$days} " . lang('g_days');
			}
			elseif ($time_temp > $num_day && $time_temp < $num_mon) {
				$days = (int)($time_temp / $num_day);
				$hours = (int) (($time_temp % $num_day) / $num_hour);
				$date_ret = "{$days} " . lang('g_days');
				if ($hours)
					$date_ret .= "&nbsp;&nbsp;{$hours} ". lang('g_hours');
			}
			elseif ($time_temp > $num_hour && $time_temp < $num_day)
		    	$date_ret = "<font color=\"#CC0000\">" . (int)($time_temp / $num_hour) . ' ' . lang('g_hours') . ' ' . (int)(($time_temp % $num_hour) / 60) . ' ' . lang('g_minutes')."</font>";
		  	else
		    	$date_ret = "<font color=\"#CC0000\">" . (int)($time_temp / 60) . ' ' . lang('g_minutes')."</font>";

		    return $date_ret;
		}
		elseif ($format === 'constant1') {
			$cur_stime = $date;
			if ( (int) (strftime("%Y", time())) == (int) (strftime("%Y", $cur_stime)) ) {
				if ( (int) (strftime("%Y%m%d", time())) == (int) (strftime("%Y%m%d", $cur_stime)) )
					$date_ret = $date?strftime("%H:%M", $cur_stime):"&nbsp;";
				else
					$date_ret = $date?strftime("%d %b", $cur_stime):"&nbsp;";
			}
			else {
				$date_ret = $date?strftime("%d %b %Y", $cur_stime):"&nbsp;";
			}				
		}
		else
			$date_ret = strftime($format, $date + $time_zone - date("Z"));
			
		return $date_ret;
	}
	
	function xNotification($message, $class='alert-success') {
		$ret = "
		<div class=\"alert $class\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
			$message
		</div>";
		return $ret;
	}
	
	function xDebug($txt, $file='/var/www/html/application/logs/log.txt') {
		file_put_contents($file, "\n" . $txt, FILE_APPEND);
	}
	
	###############################################################################

	function xSetFilterTable($ctl, &$field, $extra_style='', $extra_attr='') {
		$init_style = '';
		$init_attr = "onclick=\"setFilterTable('{$field['name']}:{$field['type']}')\"";

		$field['allow_filter'] = !isset($field['allow_filter'])?true:$field['allow_filter'];
		$field['sort'] = '';

		if (!empty($field['width']))
			$init_style .= "width:{$field['width']};";
		if (!empty($field['text-align']))
			$init_style .= "text-align:{$field['text-align']};";

		if ($field['allow_filter']!==false) {
			$f='';
			$s='';
			if (isset($_SESSION['G_SESSION'][$ctl]['filter_sort'])) {
				list($f, $s) = explode(' ',$_SESSION['G_SESSION'][$ctl]['filter_sort']);
				if ($field['name'] === $f) {
					if ($s === 'asc')
						$field['sort'] = "<img src='".base_assets()."images/up.gif' />";
					elseif ($s === 'desc')
						$field['sort'] = "<img src='".base_assets()."images/down.gif' />";
				}
			}

			$init_style .= "cursor:pointer;";
			$init_attr .= ' class="filtertable_header" ';
		}
		
		$style = "style=\"$init_style$extra_style\"";
		
		return $style . ' ' . $init_attr . ' ' . $extra_attr;
	}
	
	function xShowButtonMode($mode, $key=null) {
		$str = '';
		if ($mode === 'lst' || $mode === 'index' || $mode === 'daftar') {
			$str .= xGetButton('add');
			$str .= xGetButton('reset');
			return $str;
		}

		if ($mode === 'edit') {
			$str .= xGetButton('lst');
			return $str;
		}

		if ($mode === 'add') {
			$str .= xGetButton('lst');
			return $str;
		}

		if ($mode === 'detail') {
			$str .= xGetButton('edit', $key);
			$str .= xGetButton('add');
			$str .= xGetButton('delete', $key);
			$str .= xGetButton('lst');
			return $str;
		}

		if ($mode === 'save') {
			$str .= xGetButton('save');
			$str .= xGetButton('batal', $key);
			return $str;
		}
	}
	
	function xGetButton($id, $key=null) {
		$ci = &get_instance();
		
		if ($id === 'add' && in_array('c_create',$ci->auth) ) {
			return '<button type="button" class="btn btn-sm btn-primary" onclick="return goEdit()"><i class="glyphicon glyphicon-plus glyphicon-white"></i> Tambah</button> ';
		}

		if ($id === 'edit' && in_array('c_update',$ci->auth) ) {
			return '<button type="button" class="btn btn-default btn-sm" onclick="return goEdit(\''.$key.'\')"><i class="glyphicon glyphicon-pencil"></i> Ubah</button> ';
		}

		if ($id === 'delete' && in_array('c_delete',$ci->auth) ) {
			return '<button type="button" class="btn btn-sm btn-danger" onclick="return goDelete(\''.$key.'\')"><i class="glyphicon glyphicon-remove glyphicon-white"></i> Hapus</button> ';
		}

		if ($id === 'lst' || $id === 'index') {
			return '<button type="button" class="btn btn-default btn-sm" onclick="return goList()"><i class="glyphicon glyphicon-list"></i> Daftar</button> ';
		}

		if ($id === 'save') {
            return '<button type="submit" class="btn btn-sm btn-primary" onclick="return goSave()"><i class="glyphicon glyphicon-ok glyphicon-white"></i> Simpan</button> ';
        }

        if ($id === 'batal') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goBatal(\''.$key.'\')"><i class="glyphicon glyphicon-repeat"></i> Batal</button> ';
        }

        if ($id === 'reset') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goReset()"><i class="glyphicon glyphicon-refresh"></i> Reset</button> ';
        }
        
        if ($id === 'cetak') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goCetak(\''.$key.'\')"><i class="glyphicon glyphicon-print"></i> Cetak</button> ';
        }
        
        if ($id === 'cetak-ijin') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goCetakIjin(\''.$key.'\')"><i class="glyphicon glyphicon-print"></i> Cetak Izin</button> ';
        }
        
        if ($id === 'tolak') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goTolak(\''.$key.'\')"><i class="glyphicon glyphicon-thumbs-down"></i> Tolak</button> ';
        }
        
        if ($id === 'kembali') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goKembali(\''.$key.'\')"><i class="glyphicon glyphicon-backward"></i> Kembalikan</button> ';
        }
        
        if ($id === 'terima') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goTerima(\''.$key.'\')"><i class="glyphicon glyphicon-thumbs-up"></i> Terima</button> ';
        }
        
        if ($id === 'proses') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goProses(\''.$key.'\')"><i class="glyphicon glyphicon-check"></i> Proses</button> ';
        }
        
        if ($id === 'execute') {
            return '<button type="button" class="btn btn-default btn-sm" onclick="return goExecute(\''.$key.'\')"><i class="glyphicon glyphicon-tag"></i> Setujui</button> ';
        }

	}
	
	function xButton($type='', $label='', $action='') {
		if ($type == 'x_closepop') {
			return '<button type="button" class="btn btn-mini btn-danger" onclick="closePop()"><i class="glyphicon glyphicon-remove icon-white"></i></button>';
		}
		elseif ($type == 'x_cancelpop') {
			return '<button type="button" class="btn btn-sm" onclick="closePop()">Batal</button>';
		}
		//
	}
	
	function xEditList($url) {
		return '<a title="Edit Data" class="btn btn-xs btn-warning" href="'. $url .'"><i class="glyphicon glyphicon-pencil"></i></a>';
	}
	function xOptionList($url,$icon = '',$title='',$jenis = 'default') {
		return '<a class="btn btn-xs btn-'.$jenis.'" href="'. $url .'" title="'.$title.'"><i class="glyphicon glyphicon-'.$icon.'"></i></a>';
	}

	function xDeleteList($id, $label) {
		return "<a class=\"btn btn-xs btn-danger\" href=\"javascript:void()\" onclick=\"deleteList('$id', '$label');return false\"><i class=\"glyphicon glyphicon-remove icon-white\"></i></a>";
	}
	
	function xEncrypt($str, $urlencode=true, $key=''){
		if (!$key) {
			$key = 'P1J2B3S';
			if (phpversion() > '5.4')
				$key .= '123456789';
		}
				
		$encrypted = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $str, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
		if ($urlencode)
			return urlencode($encrypted);
		return $encrypted;
	}
	
	function xDecrypt($str, $urldecode=true, $key=''){
		if (!$key) {
			$key = 'P1J2B3S';
			if (phpversion() > '5.4')
				$key .= '123456789';
		}

		if ($urldecode)
			$str = rawurldecode($str);
		
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($str), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
		return $decrypted;
	}
	
	function xGetJSON($data,$cvalue='',$clabel='') {
		$object = array('items' => array());
		
		if(!empty($data)) {
			foreach($data as $row) {
				if(empty($cvalue) and empty($clabel)) {
					$value = current($row);
					$label = next($row);
				}
				else {
					$value = $row[strtoupper($cvalue)];
					$label = $row[strtoupper($clabel)];
				}
				
				$object['items'][] = array('value' => $value, 'label' => $label);
			}
		}
		
		return json_encode($object);
	}
	
	function xFilterPaging($pagination, $filter_string, $num_start, $num_end, $num_all) {
		$str = "<div class=\"clear\"></div>";
		$str .= "<div style=\"margin-top:3px\">";
		$str .= "<span class=\"pagination pagination-sm\" style=\"margin-top:0;margin-bottom:0px;padding-bottom:0px\">$pagination</span>";
		if ($filter_string) {
			$str .= "<span class=\"label label-danger\" style=\"font-size:14px\">Fiter: ON</span>";
		}
		if ($num_all) {
			$num_start = number_format($num_start);
			$num_end = number_format($num_end);
			$num_all = number_format($num_all);
			$str .= "<span style=\"float:right\">$num_start-$num_end dari $num_all</span>";
		}
			
		$str .= "</div><div class=\"clear\"></div>";
		
		return $str;
	}
	
	function xSaveIcon() {
		return '<i class="glyphicon glyphicon-floppy-disk"></i>';
	}
	function xBatalIcon() {
		return '<i class="glyphicon glyphicon-repeat"></i>';
	}
	function xRemoveIcon() {
		return '<i class="glyphicon glyphicon-remove"></i>';
	}
	function xRefreshIcon() {
		return '<i class="glyphicon glyphicon-refresh"></i>';
	}
	function xPrintIcon() {
		return '<i class="glyphicon glyphicon-print"></i>';
	}
	function xEditIcon() {
		return '<i class="glyphicon glyphicon-pencil"></i>';
	}
	function xListIcon() {
		return '<i class="glyphicon glyphicon-list"></i>';
	}	
	function xPlusIcon() {
		return '<i class="glyphicon glyphicon-plus"></i>';
	}	
	function xCheckIcon() {
		return '<i class="glyphicon glyphicon-ok"></i>';
	}	
	function xTrashIcon() {
		return '<i class="glyphicon glyphicon-trash"></i>';
	}	
	function xEnvelopeIcon() {
		return '<i class="glyphicon glyphicon-envelope"></i>';
	}		
	
	function xNoImage($ext='') {
		$ci = &get_instance();
		
		$profile_path = $ci->config->item('static_path') . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR;
		if (!$ext) {
			$file = $profile_path . "noimage_150.jpg";
			header('Content-type: image/jpeg');
			echo file_get_contents($file);
			exit;
		}

		$ext = strtolower($ext);
		
		if (in_array($ext, array('doc', 'docx')))
			$ext = 'word';
		if (in_array($ext, array('ppt', 'pptx')))
			$ext = 'ppt';
		if (in_array($ext, array('xls', 'xlsx')))
			$ext = 'excel';
		
		$file = $profile_path . "noimage_{$ext}.jpg";
		if (!file_exists($file))
			$file = $profile_path . "noimage_files.jpg";
		header('Content-type: image/jpeg');
		echo file_get_contents($file);
		exit;
	}
	
	function xHighlightHomeSearch(&$row, $param) {
		foreach ($param as $k)
			$row[$k] = highlight_phrase($row[$k], $_SESSION[G_SESSION]['cari_km'], '<span style="background-color:yellow;">', '</span>');
		
	}
	
	function xResizeCropImage($src, $dest=false, $thumb_width = 200, $thumb_height = 200) {
		if (!$dest) 
			$dest = tempnam(sys_get_temp_dir(), 'pjbs_');
		
		if (file_exists($src)) {
			if ($src == $_FILES['foto_profile']['tmp_name'])
				$ext = strtolower(substr(strrchr($_FILES['foto_profile']['name'],"."),1));
			else
				$ext = strtolower(substr(strrchr($src,"."),1));
			
			if (preg_match('/jpg|jpeg/i',$ext))
				$image=imagecreatefromjpeg($src);
			else if (preg_match('/png/i',$ext))
				$image=imagecreatefrompng($src);
			else if (preg_match('/gif/i',$ext))
				$image=imagecreatefromgif($src);
			else if (preg_match('/bmp/i',$ext))
				$image=imagecreatefromwbmp($src);
			else
				return false;
		}
		else {
			$image = imagecreatefromstring($src);
			imagejpeg($image, $dest, 80);
			$image=imagecreatefromjpeg($dest);
		}

		$width = imagesx($image);
		$height = imagesy($image);

		$original_aspect = $width / $height;
		$thumb_aspect = $thumb_width / $thumb_height;

		if ($width < $thumb_width || $height < $thumb_height) {
			$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
			$background = imagecolorallocate($thumb, 255, 255, 255); 
			imagefill($thumb,0,0,$background); // fill the background with white

			imagecopyresampled($thumb, $image, ($thumb_width - $width) / 2, ($thumb_height - $height) / 2, 0, 0, $width, $height, $width, $height); // copy the image to the background
			imagejpeg($thumb, $dest, 80);
		}
		else {
			if ( $original_aspect >= $thumb_aspect ) {
				$new_height = $thumb_height;
				$new_width = $width / ($height / $thumb_height);
			}
			else
			{
			   $new_width = $thumb_width;
			   $new_height = $height / ($width / $thumb_width);
			}
			$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

			imagecopyresampled($thumb,
							   $image,
							   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
							   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
							   0, 0,
							   $new_width, $new_height,
							   $width, $height);
			imagejpeg($thumb, $dest, 80);
		}

		$thumbnail = file_get_contents($dest);	
		unlink($dest);
		return $thumbnail;
	}
	
	function xTriwulan($bln, $isreturn = false){
		$bln = (int) $bln;
		
		if(in_array($bln, array(1,2,3,4))){
			if($isreturn)
				return "1"; 
			else
				echo "1";
		}elseif(in_array($bln, array(5,6,7,8))){
			if($isreturn)
				return "2"; 
			else
				echo "2";
		}elseif(in_array($bln, array(9,10,11,12))){
			if($isreturn)
				return "3"; 
			else
				echo "3";
		}
	}
