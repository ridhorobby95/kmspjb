<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	# return all rows
	function dbGetRows($sql, $limit='', $offset='', $db=false, $binds=false) {
		$limit = (int) $limit;
		$offset = (int) $offset;
		
		if ($limit || $offset) {
			$limit = $offset + $limit;
			$newsql = "SELECT * FROM (select inner_query.*, rownum rnum FROM ($sql) inner_query WHERE rownum <= $limit)";
	
			if ($offset != 0)
			{
				$newsql .= " WHERE rnum > $offset";
			}
			$sql = $newsql;
		}

		$result = dbQuery($sql, $db, $binds);
		if (!$result)
			return null;
		
		if ($result->num_rows()) {
			return $result->result_array();
		}
		return null;
	}
	
	function dbGetRowsBinds($sql, $binds) {
		return dbGetRows($sql, '', '', false, $binds);
	}

	# return the first row
	function dbGetRow($sql, $db=false, $binds=false) {
		$result = dbQuery($sql, $db, $binds);

		if (!$result)
			return null;

		if ($result->num_rows()) {
			return $result->row_array();
		}
		return null;
	}
	
	function dbGetRowBinds($sql, $binds) {
		return dbGetRow($sql, false, $binds);
	}

	# return the first column
	function dbGetOne($sql, $db = false) {
		$result = dbQuery($sql, $db);

		if (!$result)
			return null;

		if ($result->num_rows()) {
			$ret = $result->row_array();
			foreach ($ret as $val) {
				return $val;
			}
		}
		else
			return null;
	}
	
	# insert
	function dbInsert($table, $fields, $db=false) {
		$ci = &get_instance();
		if (!$db) {
			$db = $ci->db;
		}
		
		$sql = "insert into $table (";
		$fieldnames = "";
		$fieldvals = "";
	
		reset($fields);
		while(list($f, $v) = each($fields)) {
			$fieldnames .= $f . ", ";

			if (preg_match_all("/{{(.*?)}}/", $v, $m)) {
				$fieldvals .= $m[1][0] . ", ";
			}
			else {
				$fieldvals .= ":$f, ";
			}
			// echo "fieldname:".$fieldnames." || fieldvals:".$vals."<br>";
		}
		$fieldnames = substr($fieldnames, 0, -2);
		$fieldvals = substr($fieldvals, 0, -2);

		$sql .= $fieldnames . ") values (" .$fieldvals . ")";

		
		$stmt = oci_parse($db->conn_id, $sql);
		reset($fields);
		while(list($f, $v) = each($fields)) {
			if (!preg_match_all("/{{(.*?)}}/", $v, $m)) {
				$$f = $v;
				oci_bind_by_name($stmt, ":$f", $$f);
			}
		}
		$ret = oci_execute($stmt);
		//xDebug(serialize(oci_error($stmt)) . '-' . oci_error() . '-');
		return $ret;
	}
	
	# update
	function dbUpdate($table, $fields, $where, $db=false) {
		$ci = &get_instance();
		if (!$db) {
			$db = $ci->db;
		}
		
		$sql = "update $table set ";
		$vars = "";
		reset($fields);

		while(list($f, $v) = each($fields)) {
			if (preg_match_all("/{{(.*?)}}/", $v, $m)) {
				$vars .= "$f = " . $m[1][0] . ", ";
			}
			else  {
				$vars .= "$f = :$f, ";
			}
		}
		$sql .= substr($vars, 0, -2);
		$sql .= " where $where";
		
		$stmt = oci_parse($db->conn_id, $sql);
		reset($fields);
		while(list($f, $v) = each($fields)) {
			if (!preg_match_all("/{{(.*?)}}/", $v, $m)) {
				$$f = $v;
				oci_bind_by_name($stmt, ":$f", $$f);
			}
		}
		$ret = oci_execute($stmt);	
		//xDebug(serialize(oci_error($stmt)) . '-' . oci_error() . '-');
		return $ret;
	}

	function dbQueryBlob($sql, $bind_names, $files) {
		$ci = &get_instance();
		if (!$db) {
			$db = $ci->db;
		}
		$stmt = oci_parse($db->conn_id, $sql);
		$lobs = array();
		foreach ($bind_names as $k=>$bind_name) {
			$lobs[$k] = oci_new_descriptor($db->conn_id, OCI_D_LOB);
			oci_bind_by_name($stmt, $bind_name, $lobs[$k], -1, OCI_B_BLOB);
		}
		oci_execute($stmt,OCI_DEFAULT);
		//xDebug (serialize(oci_error($stmt)) . '-' . oci_error() . '-');
		foreach ($bind_names as $k=>$bind_name) {
			$ret = $lobs[$k]->save($files[$k]);
		}
		
		oci_commit($db->conn_id);
		//xDebug (serialize(oci_error($stmt)) . '-' . oci_error() . '-');
		if ($ret)
			return true;
		
		return false;
	}
	
	# Delete
	function dbDelete($table, $where, $db=false) {
		$sql = "delete from $table $where ";

		$ret = dbQuery($sql, $db);
		
		return $ret;
	}
	
	function dbQuery($sql, $db=false, $binds=false) {
		$ci = &get_instance();
		if (!$db) {
			$db = $ci->db;
		}
		
		$ret = $db->query($sql,$binds);
		
		/*
		$ci->last_error_sql = $db->getErrorMessage();
		if ($ci->config->item('debug_db')) {
			$ci->list_sql[] = $sql.($binds === false ? '' : ' -- '.implode(', ',$binds));
			$ci->error_sql[] = $db->getErrorMessage();
		}
		*/
		
		return $ret;
	}
	
	function dbGuid() {
		$sql = "select RAWTOHEX(sys_guid()) as id from dual";
		return dbGetOne($sql);
	}
	
	function dbIntoLog($table, $sql, $db=false) {
		return true;
		
		$ci = &get_instance();
		if (!$db) {
			$db = $ci->db;
		}

		if (in_array(strtolower($table), $ci->config->item('log_db_table_exception')))
			return false;

		$data = array('TABLENAME'=>$table,
					  'SQL'=>$sql,
					  'TUSERNAME' =>$_SESSION[G_SESSION]['username'],
					  'TIPADDRESS' =>$_SERVER['REMOTE_ADDR']
					  );

		$newsql = $db->insert_string('SQLLOG', $data);
		return $db->query($newsql);
	}
	
	function dbGetRecordDate($string,$format='DD-MM-YYYY') {
		if(empty($string))
			return '{{null}}';
		
		return "{{to_date('$string','$format')}}";
	}

