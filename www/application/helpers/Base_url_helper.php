<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function base_assets() {
		return base_url() . 'application/assets/';
	}

	function base_upload() {
		return base_url() . 'upload/';
	}

