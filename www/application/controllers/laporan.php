<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends Base_Controller {

	protected $model_p = 'laporan_model';
	protected $list_limit = 100;
	
    function __construct() {
		parent::__construct();
	}
	
	function index() {
		if ($_POST['act_laporan']) { 
			
			
			$_SESSION[G_SESSION]['report']['selected_jenis'] = $_POST['idjeniskm'];

			unset($_SESSION[G_SESSION]['report']['selected_unit']);
			unset($_SESSION[G_SESSION]['report']['selected_subyek']);
			unset($_SESSION[G_SESSION]['report']['selected_tahun']);
			unset($_SESSION[G_SESSION]['report']['selected_bulan']);
			
			if ($_POST['idjeniskm'] == '1'){
				$_SESSION[G_SESSION]['report']['selected_unit'] = $_POST['kodeunit'];
				$_SESSION[G_SESSION]['report']['selected_subyek'] = $_POST['idsubyek'];
				$_SESSION[G_SESSION]['report']['selected_tahun'] = $_POST['tahun'];
				$_SESSION[G_SESSION]['report']['selected_bulan'] = $_POST['bulan'];
				
			}else if ($_POST['idjeniskm'] == '2'){
				$_SESSION[G_SESSION]['report']['selected_unit'] = $_POST['kodeunit'];
				$_SESSION[G_SESSION]['report']['selected_subyek'] = $_POST['idsubyek'];
				$_SESSION[G_SESSION]['report']['selected_tahun'] = $_POST['tahun'];
				$_SESSION[G_SESSION]['report']['selected_bulan'] = $_POST['bulan'];
				
			}else if ($_POST['idjeniskm'] == '3'){
				$_SESSION[G_SESSION]['report']['selected_tahun'] = $_POST['tahun'];
				
			}else if ($_POST['idjeniskm'] == '3'){
				$_SESSION[G_SESSION]['report']['selected_unit'] = $_POST['kodeunit'];
				$_SESSION[G_SESSION]['report']['selected_tahun'] = $_POST['tahun'];
				
			}else if ($_POST['idjeniskm'] == 'P' or $_POST['idjeniskm'] == 'L' or $_POST['idjeniskm'] == 'D'){
				$_SESSION[G_SESSION]['report']['selected_subyek'] = $_POST['idsubyek'];
				$_SESSION[G_SESSION]['report']['selected_mode_waktu'] = $_POST['waktu'];
				$_SESSION[G_SESSION]['report']['selected_tahunwaktu'] = $_POST['tahunwaktu'];
				$_SESSION[G_SESSION]['report']['selected_bulanwaktu'] = $_POST['bulanwaktu'];
				$_SESSION[G_SESSION]['report']['use_bulanwaktu'] = $_POST['with_bulan'];
				$_SESSION[G_SESSION]['report']['tanggal1'] = $_POST['tanggal1'];
				$_SESSION[G_SESSION]['report']['tanggal2'] = $_POST['tanggal2'];
				
			}
			 
			
			redirect('laporan/index?q=submit&a='.$_POST['act_laporan']);
		}
		$this->load->model('km_model', 'KM');
		$this->data['list_subyek'] = $this->KM->getListSubyekKM();
		$this->data['list_tahun'] = $this->KM->getListTahunKM();
		$this->data['list_jenis'] = $this->KM->getListJenisKM();
		
		/*$this->load->model('unit_model', 'UNIT');
		$this->data['list_unit'] = $this->UNIT->getList();*/
		$this->load->model('unit_model', 'UNIT');
		
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$this->data['list_unit'] = $this->UNIT->getList();
		
		$this->data['page_title'] = 'Laporan Statistik';
		
		if ($_GET['q'] == 'submit') {
			$this->_report();
		}
		
		$this->renderView('laporan');	
	}
	

	function _report() {
		if (in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('P','L','D'))) {
			return $this->_reportP();
		}
		
		$tahun_str = implode(',', $_SESSION[G_SESSION]['report']['selected_tahun']);
		if ($tahun_str) {
			$tahun_str2 = "'" . implode("','", $_SESSION[G_SESSION]['report']['selected_tahun']) . "'";
			$this->data['tahun_str'] = 'TAHUN ' . $tahun_str;
		}
		
		if ($tahun_str2) {
			if (in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array(1,2,4)))
				$filter_tahun = " and (to_char(k.waktupelaksanaan,'YYYY') in ($tahun_str2)) ";
			else
				$filter_tahun = " and (k.tahuntulis in ($tahun_str2)) ";
		}
		
		$bulan =  str_pad($_SESSION[G_SESSION]['report']['selected_bulan'], 2, '0', STR_PAD_LEFT);
		if ($_SESSION[G_SESSION]['report']['selected_bulan']) {
			$this->data['bulan_str'] = 'BULAN ' . strtoupper(xIndoMonth($_SESSION[G_SESSION]['report']['selected_bulan']));
			if (in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array(1,2,4)))
				$filter_bulan = " and (to_char(k.waktupelaksanaan,'MM') = '$bulan') ";
			else
				$filter_bulan = " and (to_char(k.tahuntulis,'MM') = '$bulan') ";
		}

		$subyeks = array();
		foreach ($this->data['list_subyek'] as $subyek) {
			if (in_array($subyek['IDSUBYEK'], $_SESSION[G_SESSION]['report']['selected_subyek']))
				$subyeks[] = $subyek['NAMA'];
		}
		$subyek_str = implode(',', $subyeks);
		if ($subyek_str)
			$this->data['subyek_str'] = 'STREAM ' . $subyek_str;
			
		$idsubyek_str = implode(',', $_SESSION[G_SESSION]['report']['selected_subyek']);
		$join_subyek = " left join {$this->km}.kmsubyek ks on k.idknowledge=ks.idknowledge 
						left join {$this->km}.subyek s on ks.idsubyek=s.idsubyek ";
		if ($idsubyek_str) {
			$filter_subyek = " and ks.idsubyek in ($idsubyek_str) ";
		}

		$units = array();
		$kodeunits = array();
		foreach ($this->data['list_unit'] as $unit) {
			if (in_array($unit['KODEUNIT'], $_SESSION[G_SESSION]['report']['selected_unit'])) {
				$units[] = $unit['NAMA'];
				$kodeunits[] = "'{$unit['KODEUNIT']}'";
			}
		}
		$unit_str = implode(',', $units);
		if ($unit_str)
			$this->data['unit_str'] = 'UNIT ' . $unit_str;

		$idunit_str = implode(',', $kodeunits);
		if ($idunit_str == '-')
			$idunit_str = '';
		if ($idunit_str)
			$filter_unit = " and k.kodeunit in ($idunit_str) ";
		
		$filter_idjeniskm = " and k.idjeniskm = {$_SESSION[G_SESSION]['report']['selected_jenis']} ";
		$sql = "select nama from {$this->km}.jeniskm where idjeniskm={$_SESSION[G_SESSION]['report']['selected_jenis']}";
		$this->data['jeniskm'] = strtoupper(dbGetOne($sql));

/*		
		$sql = "select k.idjeniskm, j.nama as namajeniskm, count(*) as sum 
			from {$this->km}.knowledge k 
			join {$this->km}.jeniskm j on k.idjeniskm=j.idjeniskm 
			$join_subyek
			where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm
			group by k.idjeniskm, j.nama ";
*/			
		# group by subyek
		$sql = "select ks.idsubyek, nvl(s.nama, 'Tanpa Subyek') as namasubyek, count(*) as sum 
			from {$this->km}.knowledge k 
			join {$this->km}.jeniskm j on k.idjeniskm=j.idjeniskm 
			$join_subyek ";
		$sql_staf = $sql_struktural = $sql;
		$sql .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm
			group by ks.idsubyek, s.nama";
		$sql_staf .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm and upper(k.jabatanpemateri) like '%STAF%'
			group by ks.idsubyek, s.nama";
		$sql_struktural .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm and upper(k.jabatanpemateri) not like '%STAF%'
			group by ks.idsubyek, s.nama";
		if ($_GET['a'] == 'chart') {
			$this->data['tot_stat_subyek'] = dbGetRows($sql);
			$this->data['tot_stat_subyek_staf'] = dbGetRows($sql_staf);
			$this->data['tot_stat_subyek_struktural'] = dbGetRows($sql_struktural);
		}

		# group by jabatan
		$sql = "select nvl(k.jabatanpemateri, 'Tamu') namajabatan, count(*) as sum 
			from {$this->km}.knowledge k 
			join {$this->km}.jeniskm j on k.idjeniskm=j.idjeniskm 
			$join_subyek ";
		$sql_staf = $sql_struktural = $sql;
		$sql .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm
			group by nvl(k.jabatanpemateri, 'Tamu') ";
		$sql_staf .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm and upper(k.jabatanpemateri) like '%STAF%'
			group by nvl(k.jabatanpemateri, 'Tamu') ";
		$sql_struktural .= "where 1=1 $filter_unit $filter_subyek $filter_tahun $filter_idjeniskm and upper(k.jabatanpemateri) not like '%STAF%'
			group by nvl(k.jabatanpemateri, 'Tamu') ";
		if ($_GET['a'] == 'chart') {
			$this->data['tot_stat_jabatan'] = dbGetRows($sql);
			$this->data['tot_stat_jabatan_staf'] = dbGetRows($sql_staf);
			$this->data['tot_stat_jabatan_struktural'] = dbGetRows($sql_struktural);
		}
		
		#subject
		$sql = "select s.idsubyek, s.nama, k.idknowledge, RAWTOHEX(k.idknowledge) idknowledge_h
			from {$this->km}.subyek s
			join {$this->km}.kmsubyek k on k.idsubyek = s.idsubyek ";
		$kmsubyek = dbGetRows($sql);
		$kmsbyk = array();
		foreach($kmsubyek as $p){
			$kmsbyk[$p['IDKNOWLEDGE_H']][] = $p['NAMA'];
		}
		$this->data['kmsubyek'] = $kmsbyk;
		
		$sql = "select k.*, u.nama as namaunit, us.nama as namapemateri, us.nid, j.nama as namajabatan, d1.nama as namasubdit,
				d2.nama as namadirektorat, f.nama as sebutanjabatan,
				RAWTOHEX(k.idknowledge) as idknowledge_h, e.nama as jkm, e.singkat as sjkm,
				to_char(k.waktupelaksanaan, 'MM') as waktupelaksanaan_b,
				to_char(k.waktupelaksanaan, 'dd Mon YYYY') as waktupelaksanaan_d,
				to_char(k.createdtime, 'MM') as createdtime_b,
				to_char(k.createdtime, 'dd Mon YYYY') as createdtime_d
				from {$this->km}.knowledge k
				left join {$this->km}.jeniskm e on k.idjeniskm=e.idjeniskm 
				left join {$this->um}.unit u on k.kodeunit=u.kodeunit
				left join {$this->um}.users us on k.pemateri=us.iduser
				left join {$this->um}.jabatan j on us.kodejabatan=j.kodejabatan
				left join {$this->um}.subdit d1 on us.kodesubdit=d1.kodesubdit
				left join {$this->um}.direktorat d2 on us.kodedirektorat=d2.kodedirektorat
				left join {$this->um}.staff f on us.kodestaff=f.kodestaff
				left join {$this->km}.kmsubyek ks on k.idknowledge=ks.idknowledge 
				left join {$this->km}.subyek s on ks.idsubyek=s.idsubyek
				";
		$sql_staf = $sql_struktural = $sql;
		$sql .= "where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm $filter_subyek
				order by k.waktupelaksanaan desc";
		$sql_staf .= "where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm $filter_subyek and upper(k.jabatanpemateri) like '%STAF%'
				order by k.waktupelaksanaan desc";
		$sql_struktural .= "where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm $filter_subyek and upper(k.jabatanpemateri) not like '%STAF%'
				order by k.waktupelaksanaan desc";
		$rows = dbGetRows($sql);
		$rows_staf = dbGetRows($sql_staf);
		$rows_struktural = dbGetRows($sql_struktural);
		$knowledges = array();
		$peserta_line_x = array();
		$peserta_line_y = array();
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$peserta_line_x[] = $i;
			$peserta_line_y[] = (int) $row['JUMLAHPESERTA'];
			$knowledges[$row['IDKNOWLEDGE_H']] = $row;
		}
		$i_staf=0;
		foreach ($rows_staf as $row) {
			$i_staf++;
			$peserta_line_x_staf[] = $i_staf;
			$peserta_line_y_staf[] = (int) $row['JUMLAHPESERTA'];
		}
		$i_struktural=0;
		foreach ($rows_struktural as $row) {
			$i_struktural++;
			$peserta_line_x_struktural[] = $i_struktural;
			$peserta_line_y_struktural[] = (int) $row['JUMLAHPESERTA'];
		}

		# per subyek
		$sql = "select RAWTOHEX(k.idknowledge) as idknowledge_h, k.*, s.nama 
			from {$this->km}.kmsubyek k join {$this->km}.subyek s on k.idsubyek=s.idsubyek 
			where k.idknowledge in ";
		$sql_staf = $sql_struktural = $sql;
		$sql .= "(select idknowledge from {$this->km}.knowledge k where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm)";
		$sql_staf .= "(select idknowledge from {$this->km}.knowledge k where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm and upper(k.jabatanpemateri) like '%STAF%')";
		$sql_struktural .= "(select idknowledge from {$this->km}.knowledge k where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm and upper(k.jabatanpemateri) not like '%STAF%')";
		$rows = dbGetRows($sql);
		$rows_staf = dbGetRows($sql_staf);
		$rows_struktural = dbGetRows($sql_struktural);
		
		$subyeks = array();
		foreach ($rows as $row)
			$subyeks[$row['NAMA']][] = $row;
		$subyek_bar = array();
		foreach ($subyeks as $k=>$v)
			$subyek_bar[$k] = count($v);
		
		$subyeks_staf = array();
		foreach ($rows_staf as $row)
			$subyeks_staf[$row['NAMA']][] = $row;
		$subyek_bar_staf = array();
		foreach ($subyeks_staf as $k=>$v)
			$subyek_bar_staf[$k] = count($v);
		
		$subyeks_struktural = array();
		foreach ($rows_struktural as $row)
			$subyeks_struktural[$row['NAMA']][] = $row;
		$subyek_bar_struktural = array();
		foreach ($subyeks_struktural as $k=>$v)
			$subyek_bar_struktural[$k] = count($v);
		
		foreach ($knowledges as &$row) {
			$row['SUBYEKS'] = $subyeks[$row['IDKNOWLEDGE_H']];
		}

		# per unit
		$sql = "select nvl(u.nama, 'TANPA UNIT') as nama, count(*) as sum
			from {$this->km}.knowledge k left join {$this->um}.unit u on k.kodeunit=u.kodeunit
			where k.idknowledge in (select idknowledge from {$this->km}.knowledge k where k.isdelete=0 $filter_unit $filter_tahun $filter_bulan $filter_idjeniskm)";
		$sql_staf = $sql_struktural = $sql;
		$sql .= " group by nvl(u.nama, 'TANPA UNIT')";
		$sql_staf .= " and upper(k.jabatanpemateri) like '%STAF%' group by nvl(u.nama, 'TANPA UNIT')";
		$sql_struktural .= " and upper(k.jabatanpemateri) not like '%STAF%'	group by nvl(u.nama, 'TANPA UNIT')";
		$rowsbar = dbGetRows($sql);
		$rowsbar_staf = dbGetRows($sql_staf);
		$rowsbar_struktural = dbGetRows($sql_struktural);
		$unit_bar = array();
		foreach ($rowsbar as $rowbar) {
			$unit_bar[$rowbar['NAMA']] = $rowbar['SUM'];
		}
		$unit_bar_staf = array();
		foreach ($rowsbar_staf as $rowbar) {
			$unit_bar_staf[$rowbar['NAMA']] = $rowbar['SUM'];
		}
		$unit_bar_struktural = array();
		foreach ($rowsbar_struktural as $rowbar) {
			$unit_bar_struktural[$rowbar['NAMA']] = $rowbar['SUM'];
		}
		$this->data['report_rows'] = $knowledges;
		$this->data['report_rows_staf'] = $rows_staf;
		$this->data['report_rows_struktural'] = $rows_struktural;
		$this->data['peserta_line_x'] = $peserta_line_x;
		$this->data['peserta_line_y'] = $peserta_line_y;
		$this->data['peserta_line_x_staf'] = $peserta_line_x_staf;
		$this->data['peserta_line_y_staf'] = $peserta_line_y_staf;
		$this->data['peserta_line_x_struktural'] = $peserta_line_x_struktural;
		$this->data['peserta_line_y_struktural'] = $peserta_line_y_struktural;
		$this->data['subyek_bar'] = $subyek_bar;
		$this->data['subyek_bar_staf'] = $subyek_bar_staf;
		$this->data['subyek_bar_struktural'] = $subyek_bar_struktural;
		$this->data['unit_bar'] = $unit_bar;
		$this->data['unit_bar_staf'] = $unit_bar_staf;
		$this->data['unit_bar_struktural'] = $unit_bar_struktural;

		if ($_GET['a'] == 'excel')
			$this->_excel($this->data['report_rows'],$this->data['kmsubyek']);
	}
	
	function _reportP() {		
		if ($_SESSION[G_SESSION]['report']['selected_mode_waktu'] == 'tahun') {
			$tahun_str = implode(',', $_SESSION[G_SESSION]['report']['selected_tahunwaktu']);
			if ($tahun_str) {
				$tahun_str2 = "'" . implode("','", $_SESSION[G_SESSION]['report']['selected_tahunwaktu']) . "'";
				$this->data['tahun_str'] = 'TAHUN ' . $tahun_str;
				$filter_tanggal .= " and (to_char(t.tinserttime,'YYYY') in ($tahun_str2)) ";
			}
			if (isset($_SESSION[G_SESSION]['report']['use_bulanwaktu'])) {
				foreach ($_SESSION[G_SESSION]['report']['selected_bulanwaktu'] as $key => $value) {
					$newbulan[$key] = str_pad($value, 2, '0', STR_PAD_LEFT);
				}
				$bulan =  implode("','", $newbulan);
				if ($_SESSION[G_SESSION]['report']['selected_bulanwaktu']) {
					$this->data['bulan_str'] = 'BULAN ' . strtoupper(xIndoMonth($_SESSION[G_SESSION]['report']['selected_bulanwaktu']));
					$filter_tanggal .= " and (to_char(t.tinserttime,'MM') in ('$bulan')) ";
				}
			}
			unset($_SESSION[G_SESSION]['report']['tanggal1']);
			unset($_SESSION[G_SESSION]['report']['tanggal2']);
		} else {
			$alias = array();
			$alias['P'] = 't';
			$alias['L'] = 't';
			$alias['D'] = 't';
			
			$tanggal1 =  xRemoveSpecial($_SESSION[G_SESSION]['report']['tanggal1']);
			$tanggal2 =  xRemoveSpecial($_SESSION[G_SESSION]['report']['tanggal2']);
			if ($_SESSION[G_SESSION]['report']['tanggal1']) {
				$this->data['tanggal_str'] = ' - ' . $_SESSION[G_SESSION]['report']['tanggal1'];
				//$filter_tanggal = " and t.tinserttime >= (to_date('$tanggal1','dd-mm-yyyy')) ";
				$filter_tanggal = " and to_char(".$alias[$_SESSION[G_SESSION]['report']['selected_jenis']].".TINSERTTIME,'dd-mm-yyyy') >= '$tanggal1' ";
				if ($_SESSION[G_SESSION]['report']['tanggal2']) {
					$this->data['tanggal_str'] .= ' sampai ' . $_SESSION[G_SESSION]['report']['tanggal2'];
					$filter_tanggal .= " and to_char(".$alias[$_SESSION[G_SESSION]['report']['selected_jenis']].".TINSERTTIME,'dd-mm-yyyy') <= '$tanggal2' ";
				}			
			}
			unset($_SESSION[G_SESSION]['report']['selected_tahunwaktu']);
			unset($_SESSION[G_SESSION]['report']['selected_bulanwaktu']);
		}

		$subyeks = array();
		foreach ($this->data['list_subyek'] as $subyek) {
			if (in_array($subyek['IDSUBYEK'], $_SESSION[G_SESSION]['report']['selected_subyek']))
				$subyeks[] = $subyek['NAMA'];
		}
		$subyek_str = implode(',', $subyeks);
		if ($subyek_str)
			$this->data['subyek_str'] = 'SUBYEK ' . $subyek_str;

		$idsubyek_str = implode(',', $_SESSION[G_SESSION]['report']['selected_subyek']);
		
		if ($_SESSION[G_SESSION]['report']['selected_jenis'] == 'P') {
			$this->data['title_chart'] = "JUMLAH PENGUNJUNG WEBSITE";
			$sql = "select n.kodeunit, n.nama, count(*) as jumlah 
				from {$this->um}.userlog t 
				join {$this->um}.users u on t.iduser=u.iduser 
				left join {$this->um}.unit n on u.kodeunit=n.kodeunit
				where 1=1 $filter_tanggal 
				group by n.kodeunit, n.nama order by count(*) desc";
			$sql_user = "select u.nid, u.KODEUNIT, u.nama, count(*) as jumlah 
				from {$this->um}.userlog t 
				join {$this->um}.users u on t.iduser=u.iduser 
				where 1=1 $filter_tanggal 
				group by u.nid, u.KODEUNIT, u.nama order by count(*) desc";
			$rows_user = dbGetRows($sql_user);
		}
		else if ($_SESSION[G_SESSION]['report']['selected_jenis'] == 'L') {
			$join_subyek = " left join {$this->km}.kmsubyek ks on t.idknowledge=ks.idknowledge 
							left join {$this->km}.subyek s on ks.idsubyek=s.idsubyek ";
			if ($idsubyek_str) {
				$filter_subyek = " and ks.idsubyek in ($idsubyek_str) ";
			}

			$this->data['title_chart'] = "JUMLAH LIKE";
			$sql = "select u.kodeunit, n.nama, count(*) as jumlah 
					from {$this->km}.suka t join {$this->um}.users u on t.iduser=u.iduser 
					join {$this->um}.unit n on u.kodeunit=n.kodeunit
					$join_subyek
					where 1=1 $filter_tanggal $filter_subyek group by u.kodeunit, n.nama order by count(*) desc";
		}
		else if ($_SESSION[G_SESSION]['report']['selected_jenis'] == 'D') {
			$join_subyek = " join {$this->km}.lampiran l on t.idlampiran=l.idlampiran
							join {$this->km}.knowledge k on l.idknowledge=k.idknowledge 
							left join {$this->km}.kmsubyek ks on k.idknowledge=ks.idknowledge 
							left join {$this->km}.subyek s on ks.idsubyek=s.idsubyek ";
			if ($idsubyek_str) {
				$filter_subyek = " and ks.idsubyek in ($idsubyek_str) ";
			}

			$this->data['title_chart'] = "JUMLAH DOWNLOAD";
			$sql = "select u.kodeunit, n.nama, count(*) as jumlah 
					from {$this->km}.downloadlampiran t join {$this->um}.users u on t.iduser=u.iduser 
					join {$this->um}.unit n on u.kodeunit=n.kodeunit
					$join_subyek
					where 1=1 $filter_tanggal $filter_subyek group by u.kodeunit, n.nama order by count(*) desc";
		}

		$rows = dbGetRows($sql);
		$jumlah_other = array();
		foreach ($rows as $keys => $row) {
			$jumlah_other[$row['NAMA']]['jumlah'] = $row['JUMLAH'];
			if ($rows_user) {
				foreach ($rows_user as $key => $value) {
					if ($value['KODEUNIT'] == $row['KODEUNIT']) {
						$rows[$keys]['users'][] = $value;
					}
				}
			}
		}
		
		$this->data['jumlah_other'] = $jumlah_other;
		$this->data['report_rows'] = $rows;
		if ($_GET['a'] == 'excel')
			$this->_excelP($this->data['report_rows']);

	}

	function _excelP($list_row, $param) {
		$this->load->library('excel');
		$objPHPExcel = $this->excel->getExcel();
		
		$objPHPExcel->getProperties()->setTitle('title')
			->setDescription("description");
			
		$objPHPExcel->getDefaultStyle()->getFont()->setSize("10");

		$title = "{$this->data['title_chart']} {$this->data['unit_str']}";
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue("A1", $title);
		
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '5BC0DE')
				)
			)
		);

		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight('30');

		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getAlignment()
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getAlignment()
			->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle("A2:C2")->getFont()->setSize("11");
		$objPHPExcel->getActiveSheet()->getStyle("A2:C2")->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('50');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');

		$styleArray = array(
						'borders' => array(
							'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
						)
						);

		$iurut = 0;
		$type_string = PHPExcel_Cell_DataType::TYPE_STRING;
			
		$objPHPExcel->getActiveSheet()->setCellValue("A2", "No.");
		$objPHPExcel->getActiveSheet()->setCellValue("B2", "UNIT");
		$objPHPExcel->getActiveSheet()->setCellValue("C2", "JUMLAH");	

		$i = 0;
		foreach ($list_row as $row) {
			$i++;
			$r = $i+2;
			
			$objPHPExcel->getActiveSheet()->setCellValue("A$r", $i);
			$objPHPExcel->getActiveSheet()->setCellValue("B$r", $row['NAMA']);
			$objPHPExcel->getActiveSheet()->setCellValue("C$r", $row['JUMLAH']);
		}
		
		for ($i=1; $i<=$r; $i++) {
			$objPHPExcel->getActiveSheet()->getStyle("A2:C$i")
				->getBorders()
				->getTop()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:C$i")
				->getBorders()
				->getBottom()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:C$i")
				->getBorders()
				->getLeft()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:C$i")
				->getBorders()
				->getRight()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		
		# excel 2003
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");

		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;filename=\"$title {$this->data['jeniskm']} {$this->data['tahun_str']} {$this->data['unit_str']} {$this->data['sub_desc']}.xls\"");
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
		
		exit;
	}
	
	function _excel($list_row, $kmsubyek, $param) {
		$this->load->library('excel');
		$objPHPExcel = $this->excel->getExcel();
		
		$objPHPExcel->getProperties()->setTitle('title')
			->setDescription("description");
			
		$objPHPExcel->getDefaultStyle()->getFont()->setSize("10");

		$title = "REKAP JUMLAH PEMATERI & PESERTA {$this->data['jeniskm']} {$this->data['tahun_str']} {$this->data['unit_str']}";
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue("A1", $title);
		
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize("14");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
		$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray(
			array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => '5BC0DE')
				)
			)
		);

		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight('30');

		$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->getAlignment()
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->getAlignment()
			->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle("A2:P2")->getFont()->setSize("11");
		$objPHPExcel->getActiveSheet()->getStyle("A2:P2")->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('10');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('15');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('20');
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('15');
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('15');
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('15');


		$styleArray = array(
						'borders' => array(
							'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
							'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
						)
						);

		$iurut = 0;
		$type_string = PHPExcel_Cell_DataType::TYPE_STRING;
		/* kolom triwulan dihapus 
		 * $objPHPExcel->getActiveSheet()->setCellValue("F2", "TRIWULAN");
		 */	
		$objPHPExcel->getActiveSheet()->setCellValue("A2", "No.");
		$objPHPExcel->getActiveSheet()->setCellValue("B2", "JUDUL");
		$objPHPExcel->getActiveSheet()->setCellValue("C2", "TEMA");	
		$objPHPExcel->getActiveSheet()->setCellValue("D2", "TIPE");	
		$objPHPExcel->getActiveSheet()->setCellValue("E2", "UNIT");	
		$objPHPExcel->getActiveSheet()->setCellValue("F2", "TRIWULAN");	
		$objPHPExcel->getActiveSheet()->setCellValue("G2", "BULAN PELAKSANAAN");	
		$objPHPExcel->getActiveSheet()->setCellValue("H2", "TANGGAL PELAKSANAAN");	
		$objPHPExcel->getActiveSheet()->setCellValue("I2", "BULAN PENGIRIMAN DATA");
		$objPHPExcel->getActiveSheet()->setCellValue("J2", "TANGGAL PENGIRIMAN DATA");
		
		$objPHPExcel->getActiveSheet()->setCellValue("K2", "PEMATERI");	
		$objPHPExcel->getActiveSheet()->setCellValue("L2", "NID");	
		$objPHPExcel->getActiveSheet()->setCellValue("M2", "JABATAN");
		$objPHPExcel->getActiveSheet()->setCellValue("N2", "ACTION PLAN");
		$objPHPExcel->getActiveSheet()->setCellValue("O2", "PROGRESS");	
		$objPHPExcel->getActiveSheet()->setCellValue("P2", "KETERANGAN");	
		$i = 0;
		foreach ($list_row as $key => $row) {
			$i++;
			$r = $i+2;
			
			$sbyk = array();
			if(!empty($kmsubyek[$key])){
				foreach($kmsubyek[$key] as $s){
					$sbyk[] = $s;
				}
				$sbjkm = implode(", ",$sbyk);
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue("A$r", $i);
			$objPHPExcel->getActiveSheet()->setCellValue("B$r", $row['JUDUL']);
			$objPHPExcel->getActiveSheet()->setCellValue("C$r", $sbjkm);
			$objPHPExcel->getActiveSheet()->setCellValue("D$r",  $row['SJKM']);
			$objPHPExcel->getActiveSheet()->setCellValue("E$r", $row['NAMAUNIT'] ? $row['NAMAUNIT'] : $row['KODEUNIT']);
			$objPHPExcel->getActiveSheet()->setCellValue("F$r", xTriwulan($row['WAKTUPELAKSANAAN_B'], TRUE));
			$objPHPExcel->getActiveSheet()->setCellValue("G$r", $row['WAKTUPELAKSANAAN_B']);
			$objPHPExcel->getActiveSheet()->setCellValue("H$r", $row['WAKTUPELAKSANAAN_D']);
			$objPHPExcel->getActiveSheet()->setCellValue("I$r", $row['CREATEDTIME_B']);
			$objPHPExcel->getActiveSheet()->setCellValue("J$r", $row['CREATEDTIME_D']);
			
			$objPHPExcel->getActiveSheet()->setCellValue("K$r", $row['PEMATERI']?$row['NAMAPEMATERI']:$row['PEMATERILUAR']);
			$objPHPExcel->getActiveSheet()->setCellValue("L$r", $row['NID']);
			$objPHPExcel->getActiveSheet()->setCellValue("M$r",  $row['JABATANPEMATERI']);
			$objPHPExcel->getActiveSheet()->setCellValue("N$r",  $row['ACTIONPLAN']);
			$objPHPExcel->getActiveSheet()->setCellValue("O$r",  $row['PROGRESS']);
			$objPHPExcel->getActiveSheet()->setCellValue("P$r", $row['ABSTRAK']);
			
			
			foreach (range('A', 'N') as $char) {
				$objPHPExcel->getActiveSheet()->getStyle("{$char}{$r}")->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("{$char}{$r}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			}

		}
		
		for ($i=1; $i<=$r; $i++) {
			$objPHPExcel->getActiveSheet()->getStyle("A2:P$i")
				->getBorders()
				->getTop()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:P$i")
				->getBorders()
				->getBottom()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:P$i")
				->getBorders()
				->getLeft()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle("A2:P$i")
				->getBorders()
				->getRight()
					->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		
		# excel 2003
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");

		header('Content-Type: application/vnd.ms-excel');
		header("Content-Disposition: attachment;filename=\"$title {$this->data['jeniskm']} {$this->data['tahun_str']} {$this->data['unit_str']} {$this->data['sub_desc']}.xls\"");
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
		
		exit;
		
	}	
	
}
