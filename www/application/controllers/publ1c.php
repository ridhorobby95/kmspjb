<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publ1c extends Base_Controller {

    function __construct() {
		parent::__construct();
	}
	
	function authperpus($item, $idrole) {
		if (!$item || !$idrole) 
			die('as');
		
		$item = rawurldecode($item);

		$this->load->model('Um_model', 'UM');
		$row = $this->UM->getAccessItem($idrole, $item);

		if (!$row)
			die();
		
		$access = array();

		if ($row['ISCREATE'])
			$access['cancreate'] = 1;
		if ($row['ISREAD']) {
			$access['canread'] = 1;
			$access['canlist'] = 1;
		}
		if ($row['ISUPDATE']){
			$access['canedit'] = 1;
			$access['canupdate'] = 1;
		}if ($row['ISDELETE'])
			$access['candelete'] = 1;
		
		echo serialize($access);
		
		exit;
	}
	
	function getmenu($iduser, $idrole) {
		if ($this->config->item('use_apc')) {
			$key = 'menu_'.$idrole;
			$apc_val = apc_fetch($key);
			if ($apc_val)
				$this->data['menus'] = $apc_val;
		}
		
		$this->load->model('Um_model', 'UM');
		
		$this->menu_unserialize = unserialize($this->UM->getNilaiSetting());

		$rows = $this->UM->getRoleItems($idrole);
		$roleitems = array();
		foreach ($rows as $row) {
			$roleitems[$row['IDITEM']] = $row;
		}

		$rows = $this->UM->getItemAccess($idrole);
		foreach ($rows as $row) {
			$this->item_access[$row['IDITEM']] = $row;
		}
		
		$rows = $this->UM->getMenuAccess();
		foreach ($rows as $row) {
			$this->menu_access[$row['IDMENU']] = $roleitems[$row['IDITEM']];
		}

		$this->data['menus'] = 	"<ul class=\"nav navbar-nav\">\n" . $this->_drawMenu() . "</ul>\n";

		if ($this->config->item('use_apc')) {
			apc_store($key, $this->data['menus']);
		}
		
		unset($this->menu_access);
		unset($this->menu_unserialize);	
		
		echo rawurlencode(serialize($this->data['menus']));
		
		exit;		
	}
	
	function thumbnail($jenis='foto') {
		$this->load->database();
		
		if ($jenis == 'foto') {		
			$id = xDecrypt($_GET['q']);
			if (!$id)
				xNoImage();

			$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $id;
			if (file_exists($thumbnail_file)) {
				header('Content-type: image/jpeg');
				echo file_get_contents($thumbnail_file);
				exit;
			}

			$sql = "select f.*, k.isdelete from {$this->km}.foto f join {$this->km}.knowledge k on f.idknowledge=k.idknowledge where idfoto=$id";
			$foto = dbGetRow($sql);
			if (!$foto || $foto['ISDELETE']) {
				xNoImage();
			}

			$file_extension = strtolower(substr(strrchr($foto['NAMAFILE'],"."),1));

			switch( $file_extension ) {
				case "gif": $ctype="image/gif"; break;
				case "png": $ctype="image/png"; break;
				case "bmp": $ctype="image/bmp"; break;
				case "jpeg":
				case "jpg": $ctype="image/jpeg"; break;
				default:
			}
			header('Content-type: ' . $ctype);		

			$sql = "select thumbnail from {$this->lp}.lampiranfotokm where idfoto=$id";
			$thumbnail = dbGetOne($sql);
			if (!$thumbnail)
				xNoImage();
			
			$content = $thumbnail->load();

			# buat file thumbnail ke disk
			$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $id;
			file_put_contents($thumbnail_file, $content);
			
			echo $content;
			exit;
		}
		elseif ($jenis == 'doc') {
			$id = xDecrypt($_GET['q']);

			if (!$id)
				xNoImage();
			
			$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $id;
			if (file_exists($thumbnail_file)) {
				header('Content-type: image/jpeg');
				echo file_get_contents($thumbnail_file);
				exit;
			}

			$sql = "select f.*, k.isdelete from {$this->km}.lampiran f join {$this->km}.knowledge k on f.idknowledge=k.idknowledge where idlampiran=$id";
			$doc = dbGetRow($sql);
			if (!$doc || $doc['ISDELETE'])
				xNoImage();

			$file_extension = strtolower(substr(strrchr($doc['NAMAFILE'],"."),1));

			$sql = "select thumbnail from {$this->lp}.lampirankm where idlampiran=$id";
			$thumbnail = dbGetOne($sql);
			if ($thumbnail) {
				header('Content-type: image/jpeg');
				
				$content = $thumbnail->load();
				
				# buat file thumbnail ke disk
				$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $id;
				file_put_contents($thumbnail_file, $content);
				
				echo $content;
				exit;
			}
			
			xNoImage($file_extension);
		}
		elseif ($jenis == 'video') {
			$id = xDecrypt($_GET['q']);

			if (!$id)
				xNoImage();
			
			xNoImage('video');
		}
	}
	
	function foto() {
		$this->load->database();
		
		$id = xDecrypt($_GET['q']);
		$sql = "select f.*, k.isdelete from {$this->km}.foto f join {$this->km}.knowledge k on f.idknowledge=k.idknowledge where idfoto=$id";
		$foto = dbGetRow($sql);
		
		if (!$foto || $foto['ISDELETE'])
			xNoImage();
		
		$file_extension = strtolower(substr(strrchr($foto['NAMAFILE'],"."),1));

		switch( $file_extension ) {
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpeg"; break;
			default:
		}
		header('Content-type: ' . $ctype);
		
		if (xIsLoggedin()) {
			$this->load->database();
			xDownloadFotoLog($id);
		}

		$sql = "select isifile from {$this->lp}.lampiranfotokm where idfoto=$id";
		$isifile = dbGetOne($sql);
		echo $isifile->load();
		exit;
	}
	
	function togglejeniskm() {
		$jenis = (int) $_POST['jenis'];
		$_SESSION[G_SESSION]['filter_km']['idjeniskm'] = $jenis;
	}

	function togglejeniskm2() {
		$jenis = (int) $_POST['jenis'];
		$_SESSION[G_SESSION]['filter_km'][$jenis] = (-1 * $_SESSION[G_SESSION]['filter_km'][$jenis]) + 1;
	}
	
	function togglejeniskmlist() {
		$jenis = (int) $_POST['jenis'];
		$_SESSION[G_SESSION]['filter_km_list'][$jenis] = (-1 * $_SESSION[G_SESSION]['filter_km_list'][$jenis]) + 1;
	}
	
	function profile($iduser=false) {
		if (!$iduser) {
			$this->_fotoprofile();
		}
		
		$id = xDecrypt($_GET['q']);
		
		$this->load->model('User_model', 'User');
		$this->load->model('UNIT_model', 'UNIT');
		$profile = $this->User->GetRow($id);
		$this->data['profile'] = $profile;
		
		$this->load->model('KM_model', 'KM');
		
		$this->data['a_unit'] = $this->UNIT->getRow($profile['KODEUNIT']);
		$this->data['num_knowledge'] = $this->KM->getNumKnowledgeSubmit($id);
		$this->data['num_komentar'] = $this->KM->getNumKomentarSubmit($id);
		$this->data['num_tulis'] = $this->KM->getNumNulisSubmit($id);
		$this->data['num_anggota'] = $this->KM->getNumAnggotaSubmit($id);

		$this->renderView('profile_detail');		
	}
	
	function _fotoprofile() {
		// hanya untuk jpg
		
		$id = xDecrypt($_GET['q']);
		$s = $_GET['s'];
		
		$profile_path = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR;
		$file = $profile_path . $id . '.jpg';
		if ($s)
			$file = $profile_path . $id . '_' . $s . '.jpg';
		
		if (file_exists($file)) {
			header('Content-type: image/jpeg');
			echo file_get_contents($file);
			exit;
		}else{
			$file = $profile_path.'noimage.png';
			header('Content-type: image/jpeg');
			echo file_get_contents($file);
			exit;
		
		}
		$this->load->database();

		$sql = "select foto, fotoext from {$this->um}.users where iduser=$id";
		$profile = dbGetRow($sql);
		if (!$profile['FOTOEXT']) {
			$file = $profile_path . "noimage_$s.jpg";
			header('Content-type: ' . $ctype);
			echo file_get_contents($file);
			exit;
		}

		switch( $profile['FOTOEXT'] ) {
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpeg"; break;
			default:
		}
		header('Content-type: ' . $ctype);		

		echo $profile['FOTO']->load();
		exit;
	}
	
	function _setFoto() {
		$this->load->library('image_lib');
		
		$max_width = 800;
		$max_height = 800;
		
		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image']	= $data['full_path'];
		$config['maintain_ratio'] = TRUE;
		
		for ($i=1; $i<=5; $i++) {
			if ($i==1) {
				$config['width'] = 300;
				$config['height'] = 300;
				$config['new_image'] = strtolower($iduser."_300".$data['file_ext']);
			}
			elseif ($i==2) {
				$config['width'] = 150;
				$config['height'] = 150;
				$config['new_image'] = strtolower($iduser."_150".$data['file_ext']);
			}
			elseif ($i==3) {
				$config['width'] = 50;
				$config['height'] = 50;
				$config['new_image'] = strtolower($iduser."_50".$data['file_ext']);
			}
			elseif ($i==4) {
				$config['width'] = 40;
				$config['height'] = 40;
				$config['new_image'] = strtolower($iduser."_40".$data['file_ext']);
			}
			elseif ($i==5) {
				$config['width'] = 32;
				$config['height'] = 32;
				$config['new_image'] = strtolower($iduser."_32".$data['file_ext']);
			}
			
			$this->image_lib->initialize($config); 
			if ( ! $this->image_lib->resize())
			{
				$error = array('error' => $this->image_lib->display_errors());
			}
		}

		if ($data['image_width'] > $max_width || $data['image_height'] > $max_height) {
			$config['width'] = $max_width;
			$config['height'] = $max_height;
			$config['new_image'] = strtolower($iduser.$data['file_ext']);
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
		}
	}
	
	function lastphotos() {
		$this->load->database();
		$this->data['last_photos'] = xLastPhotos();
		$this->load->view("default/_ajax_last_photos", $this->data);
		return;
	}
	
	function lastfiles() {
		$this->load->database();
		$this->data['last_files'] = xLastFiles();
		$this->load->view("default/_ajax_last_files", $this->data);
		return;
	}
	
	function populerkm() {
		$this->load->database();
		$this->data['populerkm'] = xPopulerKM();
		$this->load->view("default/_ajax_populerkm", $this->data);
		return;
	}


	function updateNotifStatus(){
		$this->load->model('Notification_model');
		$dataNotif = $this->Notification_model->getForUpdate();
		$dateNow = date("m/d/Y");
		foreach ($dataNotif as $notif) {
			$datePelaksanaan = date("m/d/Y", strtotime($notif['WAKTUPELAKSANAAN']));
			$datetime1 = new DateTime($dateNow);
            $datetime2 = new DateTime($datePelaksanaan);
            $difference = $datetime1->diff($datetime2);
            echo $notif['IDNOTIFICATIONS']." ====> ".$dateNow." : ".$datePelaksanaan."<br>";
            if($difference->days <= 7){ // jika dilaksanaan kurang dari 7 hari, langsung kirim notif
            	echo "=>Masuk 7 hari, akan diupdate<br>";
                $status     = Notification_model::SENT;
                // $sendDate   = $dateNow;
                $dateFormat = date("Y/m/d", strtotime($dateNow));
                $waktupelaksanaan = date("Y/m/d", strtotime($datePelaksanaan));
                $sendDate = $dateFormat;
                $dataUpdate = array(
	                'STATUS'    => $status,
	                'SEND_DATE' => "TO_DATE('".$sendDate."', 'yyyy/mm/dd')"
	            );
	            dbQuery("update notifications set STATUS = '".$status."', SEND_DATE = TO_DATE('".$sendDate."', 'yyyy/mm/dd') where idnotifications=".$notif['IDNOTIFICATIONS']);
	            // dbUpdate('notifications', $dataUpdate, "idnotifications=".$notif['IDNOTIFICATIONS']);
                // $sendDate = "to_date('$dateNow','yyyy/mm/dd')";
                
            }
            else{
            	echo "=>belum masuk 7 hari<br>";
            }
            
		}
	}
	
	function updatestatistik($key='') {
		if ($key != '0C7F12CA14474E80B4DD798DF7B37985') {
			if ($_SESSION[G_SESSION]['idrole'] != 1) {
				die('Forbidden access');
			}
		}
		
		$this->load->database();
		
		$sql = "delete from {$this->km}.populerkm";
		dbQuery($sql);
		
		echo '<p style="font-weight:bold">' . $sql . '</p>';
		
		$sql = "select a.*, RAWTOHEX(a.idknowledge) as idknowledge_h from (select s.idknowledge, count(*) as num from {$this->km}.lihat s join {$this->km}.knowledge k on s.idknowledge=k.idknowledge and k.isdelete=0 group by s.idknowledge order by count(*) desc) a
				where rownum<= 20";
		$list_lihat = dbGetRows($sql);

		$sql = "select a.*, RAWTOHEX(a.idknowledge) as idknowledge_h from (select s.idknowledge, count(*) as num from {$this->km}.suka s join {$this->km}.knowledge k on s.idknowledge=k.idknowledge and k.isdelete=0 group by s.idknowledge order by count(*) desc) a
				where rownum<= 20";
		$list_suka = dbGetRows($sql);
		
		$sql = "select a.*, RAWTOHEX(a.idknowledge) as idknowledge_h from (select s.idknowledge, count(*) as num from {$this->km}.diskusi s join {$this->km}.knowledge k on s.idknowledge=k.idknowledge and k.isdelete=0 group by s.idknowledge order by count(*) desc) a
				where rownum<= 20";
		$list_comment = dbGetRows($sql);
/*				
		$sql = "select a.*, RAWTOHEX(a.idknowledge) as idknowledge_h from (select k.idknowledge, f.idfoto, count(*) as num from {$this->km}.downloadfoto d join {$this->km}.foto f on f.idfoto=d.idfoto 
				join {$this->km}.knowledge k on f.idknowledge=k.idknowledge and k.isdelete=0 group by k.idknowledge, f.idfoto order by count(*) desc) a
				where rownum<= 50";
		$list_foto = dbGetRows($sql);
*/		
		$sql = "select a.*, RAWTOHEX(a.idknowledge) as idknowledge_h from (select k.idknowledge, f.idlampiran, count(*) as num from {$this->km}.downloadlampiran d join {$this->km}.lampiran f on f.idlampiran=d.idlampiran 
				join {$this->km}.knowledge k on f.idknowledge=k.idknowledge and k.isdelete=0 group by k.idknowledge, f.idlampiran order by count(*) desc) a
				where rownum<= 100";
		$list_file = dbGetRows($sql);
		
		$top = array();
		
		foreach ($list_lihat as $km) {
			$top[$km['IDKNOWLEDGE_H']]['lihat'] = $km['NUM'];
		}

		foreach ($list_suka as $km) {
			$top[$km['IDKNOWLEDGE_H']]['suka'] = $km['NUM'];
		}

		foreach ($list_comment as $km) {
			$top[$km['IDKNOWLEDGE_H']]['komentar'] = $km['NUM'];
		}

		foreach ($list_file as $km) {
			$top[$km['IDKNOWLEDGE_H']]['download'] = $top[$km['IDKNOWLEDGE_H']]['download'] + $km['NUM'];
		}
		
		$i = 0;
		foreach ($top as $k=>$v) {
			$i++;
			
			$record = array();
			$record['idknowledge'] = $k;
			foreach ($v as $key=>$val) {
				$record[$key] = $val;
			}
			$sql = "select modifiedtime, trunc(mod(months_between(sysdate,modifiedtime),12)) month, trunc(sysdate-add_months(modifiedtime,trunc(months_between(sysdate,modifiedtime)/12)*12+trunc(mod(months_between(sysdate,modifiedtime),12)))) day from {$this->km}.knowledge where idknowledge='$k'";
			$update = dbGetRow($sql);
			$month = $update['MONTH'];
			$day = $update['DAY'];
			$record['lastupdate'] = $update['MODIFIEDTIME'];
			$record['skor'] = (int) ($record['download']*1.5) + ($record['lihat']*1.4) + ($record['suka']*1.3) + ($record['komentar']*1.2) - ($day*1.1);
			dbInsert("{$this->km}.populerkm", $record);
			echo '<div style="color:blue">' . $i . '. Insert populerkm: ' . $k . ' - Skor : ' . $record['skor'] . '</div>';
		}
		echo '<p style="font-weight:bold">Statistik updated</p>';
	}
	
	function pengunjung($jenis=false) {
		$this->load->database();
		
		if (!$_SESSION[G_SESSION]['opsi_pengunjung'])
			$_SESSION[G_SESSION]['opsi_pengunjung'] = 1;
		if (!$jenis) 
			$jenis = (int) $_SESSION[G_SESSION]['opsi_pengunjung'];
		else 
			$_SESSION[G_SESSION]['opsi_pengunjung'] =  (int) $jenis;
		
		$d = date('Y-m-d');
		$m = date('Y-m');
		$y = date('Y');
		
		if ($jenis == 1) 
			$waktu = $d;
		elseif ($jenis == 3)
			$waktu = $m;
		elseif ($jenis == 4)
			$waktu = $y;

		$sql = "select r.*, u.nama from {$this->um}.rekapuserlog r left join {$this->um}.unit u on u.kodeunit = r.kodeunit 
				where r.waktu in ('$waktu') order by r.jumlah desc";
		$rows = dbGetRows($sql);
		$str = '<div>';
		foreach ($rows as $row) {
			$str .= "<div class=\"col-xs-4 col-md-4\" style=\"padding-left:0px\"><span title=\"{$row['NAMA']}\"class=\"label label-info\">{$row['KODEUNIT']}</span> <span style=\"font-size:11px\">" . xFormatNumber($row['JUMLAH']) . "</span> </div>";
		}
		$str .= '</div>';
		die($str);
	}
	
/********************************************************************************
    NOTE :
    kalau mau edit function _import
    jangan lupa edit juga kembarannya yakni function _cronimport di bawahnya
*****									********
***********							****************
*********************				           *********************
*****************************			   *****************************
********************************************************************************
********************************************************************************
*********************************************************************************/
	function importellipse($key='') {
		//if ($key != '0C7F12CA14474E80B4DD798DF7B37985') {
		//	if ($_SESSION[G_SESSION]['idrole'] != 1)
		//		die();
		//}

		error_reporting(0);
		set_time_limit(3600);
		ob_implicit_flush(1);
		
		//$this->_importmaster('ellipse.knowvation_master_direktorat', "{$this->um}.direktorat", 'kodedirektorat', 'nama');
		//$this->_importmaster('ellipse.knowvation_master_subdit', "{$this->um}.subdit", 'kodesubdit', 'nama');
		//$this->_importmaster('ellipse.knowvation_master_unit', "{$this->um}.unit", 'kodeunit', 'nama');
		//$this->_importmaster('ellipse.knowvation_master_fungsi', "{$this->um}.fungsi", 'kodefungsi', 'nama');
		//$this->_importmaster('ellipse.knowvation_master_staff', "{$this->um}.staff", 'kodestaff', 'nama');
		$this->_importmaster('ellipse.knowvation_master_status', "{$this->um}.status", 'kodestatus', 'nama');
		//$this->_importmaster('ellipse.knowvation_master_jabatan', "{$this->um}.jabatan", 'kodejabatan', 'nama');
		
		$this->_importmasterunit();
		$this->_importmastersubdit();
		$this->_importmasterjabatan();
		$this->_importmasterdirektorat();
		$this->_importusers();
		
	}
	
	function _importmasterunit() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select TABLE_CODE, TABLE_DESC   
			from ELLIPSE.KNOWVATION_MASTER_UNIT ";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['TABLE_CODE']);
			$nama = trim($row['TABLE_DESC']);
			
			$sql = "select 1 from {$this->um}.unit where kodeunit='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.unit set nama='$nama' where kodeunit='$kode'";
			else
				$sql = "insert into {$this->um}.unit (kodeunit, nama) values ('$kode', '$nama')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
	function _importmastersubdit() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select KODE_SUBDIT, SUBDIT, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY KODE_SUBDIT, SUBDIT 
			ORDER BY KODE_SUBDIT, SUBDIT";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['KODE_SUBDIT']);
			$nama = trim($row['SUBDIT']);
			
			$sql = "select 1 from {$this->um}.subdit where kodesubdit='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.subdit set nama='$nama' where kodesubdit='$kode'";
			else
				$sql = "insert into {$this->um}.subdit (kodesubdit, nama) values ('$kode', '$nama')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
	function _importmasterjabatan() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select POSITION_ID, POSISI, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY POSITION_ID, POSISI 
			ORDER BY POSITION_ID, POSISI";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['POSITION_ID']);
			$nama = trim($row['POSISI']);
			
			$sql = "select 1 from {$this->um}.jabatan where kodejabatan='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.jabatan set nama='$nama' where kodejabatan='$kode'";
			else
				$sql = "insert into {$this->um}.jabatan (kodejabatan, nama) values ('$kode', '$nama')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
	function _importmasterdirektorat() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select KODE_POSISI_DITBID, POSISI_DITBID, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY KODE_POSISI_DITBID, POSISI_DITBID 
			ORDER BY KODE_POSISI_DITBID, POSISI_DITBID";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['KODE_POSISI_DITBID']);
			$nama = trim($row['POSISI_DITBID']);
			
			$sql = "select 1 from {$this->um}.direktorat where kodedirektorat='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.direktorat set nama='$nama' where kodedirektorat='$kode'";
			else
				$sql = "insert into {$this->um}.direktorat (kodedirektorat, nama) values ('$kode', '$nama')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
	function _importmaster($table_src, $table_dest, $kode_dest, $nama_dest) {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select * from $table_src";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['TABLE_CODE']);
			$nama = trim($row['TABLE_DESC']);
			if ($table_src == 'ellipse.knowvation_master_jabatan') {
				$kode = trim($row['POSITION_ID']);
				$nama = trim($row['POS_TITLE']);
			}
			else if ($table_src == 'ellipse.knowvation_master_unit') {
				if (strstr($kode, '**'))
					continue;
			}
			
			$sql = "select 1 from $table_dest where $kode_dest='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update $table_dest set $nama_dest='$nama' where $kode_dest='$kode'";
			else
				$sql = "insert into $table_dest ($kode_dest, $nama_dest) values ('$kode', '$nama')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
	function _importusers() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		$table_dest = "{$this->um}.users";
		$sql = "select * from ellipse.moffice_datapegawai_v1";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<p style="font-weight:bold">' . str_repeat(' ',1024*64)  . $sql . '</p>';
		$i = 0;
		foreach ($rows as $row) {
			$i++;
			$nid = trim($row['NID']);
			$nama = str_replace("'", "''",trim($row['NAMA_LENGKAP']));
			$kodeunit = trim($row['KODE_UNIT']);
			$kodedirektorat = trim($row['KODE_POSISI_DITBID']);
			$kodesubdit = trim($row['KODE_SUBDIT']);
			$kodefungsi = null;//trim($row['FUNGSI']); /*gak dipake*/
			$kodestaff = null;//trim($row['KDSTAFF']); /*gak dipake*/
			$kodestatus = 1; /*senua yang dari ellipse dianggap tetap*/
			$kodejabatan = trim($row['POSITION_ID']);
			$email = trim($row['EMAIL']);
			
			$sql = "select 1 from $table_dest where nid='$nid'";
			if (dbGetOne($sql)) 
				$sql = "update $table_dest set nama='$nama', email='$email', kodeunit='$kodeunit', kodejabatan='$kodejabatan', kodedirektorat='$kodedirektorat', kodesubdit='$kodesubdit',
				kodefungsi='$kodefungsi', kodestaff='$kodestaff', kodestatus='$kodestatus'
				where nid='$nid'";
			else
				$sql = "insert into $table_dest (nid, nama, email, kodeunit, kodejabatan, kodedirektorat, kodesubdit, kodefungsi, kodestaff, kodestatus) 
				values ('$nid', '$nama', '$email', '$kodeunit', '$kodejabatan', '$kodedirektorat', '$kodesubdit', '$kodefungsi', '$kodestaff', '$kodestatus')";
			
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' . $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
	}
	
/*******************************   END   ***************************************
    NOTE :
    kalau mau edit function _import
    jangan lupa edit juga kembarannya yakni function _cronimport di bawahnya
*****									********
***********							****************
*********************				           *********************
*****************************			   *****************************
********************************************************************************
********************************************************************************
*********************************************************************************/	

	function t333st() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select * from ellipse.karyawan where rownum<10";
		$rows = dbGetRows($sql, '', '', $ellipse);
		echo '<pre>';
		var_dump($rows);
	}
	
	function inituserakses() {
		set_time_limit(3600);
		ob_implicit_flush(1);

		$this->load->database();
		
		$sql = "select iduser from {$this->um}.users";
		$users = dbGetRows($sql);
		$i=0;
		foreach ($users as $user) {
			$i++;
			$iduser = $user['IDUSER'];
			$sql = "insert into {$this->um}.userrole (iduser, idrole) values ($iduser, 3)";
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $i . '. ' . $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) . $i . '. ' .  $sql . '</div>';
		}
		
	}
	
	function diagnosa() {
		echo '<h3>Diagnosa Aplikasi</h3>';
		echo '<p>Segera perbaiki bila ada warna merah di bawah ini</p>';
		
		$error = false;
		
		if (!function_exists('oci_connect')) {
			echo '<div style="color:red">Belum ada OCI8 atau OCI8_11g</div>';
			$error = 1;
		}
		if (!function_exists('imagejpeg')) {
			echo '<div style="color:red">Belum ada GD2</div>';
			$error = 1;
		}
		if (!class_exists('imagick')) {
			echo '<div style="color:red">Belum ada Image Magick</div>';
			$error = 1;
		}

		if (!is_writable($this->config->item('upload_temp'))) {
			echo '<div style="color:red">Harus writable: '.$this->config->item('upload_temp') . '</div>';
			$error = 1;
		}
		
		if (!$error)
			echo '.<br>.<br>.<br><h3 style="color:green;font-weight:bold">Aplikasi sepertinya sudah memenuhi syarat</h3>';
	
	}
	
	function importlampiran($year, $idjeniskm) {
		if (!$year || !$idjeniskm)
			die('STOP');
		
		set_time_limit(0);
		ob_implicit_flush(1);
		
		$this->load->database();

		if ($idjeniskm == 1) {
			$item = 'SK';
		}
		else if ($idjeniskm == 4) {
			$item = 'KI';
		}

		$root = "/home/sevima/$item $year";

		$sql = "select RAWTOHEX(idknowledge) as idknowledge, nama from km.knowledge where idjeniskm=$idjeniskm and (to_char(waktupelaksanaan,'YYYY') in ('$year')) order by nama";
		$rows = dbGetRows($sql);
		$knowledges = array();
		foreach ($rows as $row) {
			$nomor = $row['NAMA'];
			
			if ($idjeniskm == 1) {
				// SK
				$nomor = str_ireplace('sharing knowledge ke','',$nomor);
				$nomor = str_ireplace('sharing kownledge ke','',$nomor);
				$nomor = str_ireplace("tahun $year",'',$nomor);
				$nomor = (int) str_ireplace('-', '', $nomor);
			}
			else if ($idjeniskm == 4) {
				// KI
				$nomor = str_ireplace('innovation','',$nomor);
				$nomor = str_ireplace("tahun $year",'',$nomor);
				$nomor = (int) $nomor;
			}
			$nomor = (int) $nomor;
			if (!$nomor)
				continue;
			

			$knowledges[$nomor]['idknowledge'] = $row['IDKNOWLEDGE'];
		}
		ksort($knowledges);

		if ($handle = opendir($root)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$handle2 = opendir($root . DIRECTORY_SEPARATOR . $entry);

					$nomor = false;

					if ($idjeniskm == 1) {
						// SK
						$nomor = str_ireplace('materi sk ','',$entry);
						$nomors = explode(' ', $nomor);
						$nomor = (int) $nomors[0];
					}
					else if ($idjeniskm == 4) {
						// KI
						$nomor = str_ireplace('inovation','',$entry);
						$nomor = str_ireplace("th $year",'',$nomor);
						$nomor = (int) trim($nomor);
					}			


					if (!$nomor)
						continue;

					while (false !== ($entry2 = readdir($handle2))) {
						if ($entry2 != "." && $entry2 != ".." && !is_dir($root . DIRECTORY_SEPARATOR . $entry . DIRECTORY_SEPARATOR . $entry2) ) {
							$knowledges[$nomor]['file'][] = $root . DIRECTORY_SEPARATOR . $entry . DIRECTORY_SEPARATOR . $entry2;
						}
					}
					closedir($handle2);
				}
			}

			closedir($handle);
		}

		foreach ($knowledges as $nomor=>$knowledge) {
			$idknowledge = $knowledge['idknowledge'];
			
			$sql = "select idlampiran from {$this->km}.lampiran where idknowledge='$idknowledge'";
			$rows = dbGetRows($sql);
			foreach ($rows as $row) {
				$idlampiran = $row['IDLAMPIRAN'];
				if ($idlampiran) {
					$sql = "delete from {$this->lp}.lampirankm where idlampiran=$idlampiran";
					if (!dbQuery($sql))
						echo '<div style="color:red">' . str_repeat(' ',1024*64) . $sql . '</div>';
					else
						echo '<div style="color:blue">' . str_repeat(' ',1024*64) .  $sql . '</div>';
				}
			}
			$sql = "delete from {$this->km}.lampiran where idknowledge='$idknowledge'";
			if (!dbQuery($sql))
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) .  $sql . '</div>';

			foreach ($knowledge['file'] as $file) {
				//echo $file . '<br/>';
				$this->_importDoc($idknowledge, $file);
			}
		}
		
	}

	function _importDoc($idknowledge, $file) {
		$image = file_get_contents($file);
		
		$thumbnail = false;
		try {
			$thumbnail = new imagick($file.'[0]');
			$thumbnail->setImageFormat('jpg');
			$thumbnail = xResizeCropImage($thumbnail);
		}
		catch(Exception $e) {
			
		}
		
		$this->load->model('KM_model', 'KM');
					
		$record = array();
		$record['idknowledge'] = $idknowledge;
		$record['namafile'] =  $namafile = substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1);
		$record['ukuranfile'] = filesize($file);
		$record['tiduser'] = $_SESSION[G_SESSION]['iduser'];
		$idlampiran = $this->KM->addDoc($record);
		
		if ($thumbnail) {
			$sql = "insert into {$this->lp}.lampirankm (idlampiran, thumbnail, isifile) values ($idlampiran, EMPTY_BLOB(), EMPTY_BLOB()) RETURNING thumbnail, isifile INTO :thumbnail, :isifile";
			$ret = dbQueryBlob($sql, array(':thumbnail', ':isifile'), array($thumbnail, $image));
			if (!$ret)
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) .  $sql . '</div>';

			$thumbnail_doc = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $idlampiran;
			file_put_contents($thumbnail_doc, $thumbnail);
		}
		else {
			$sql = "insert into {$this->lp}.lampirankm (idlampiran, isifile) values ($idlampiran, EMPTY_BLOB()) RETURNING isifile INTO :isifile";
			$ret = dbQueryBlob($sql, array(':isifile'), array($image));
			if (!$ret)
				echo '<div style="color:red">' . str_repeat(' ',1024*64) . $sql . '</div>';
			else
				echo '<div style="color:blue">' . str_repeat(' ',1024*64) .  $sql . '</div>';
		}
		unset($thumbnail);
		unset($image);
	}
	
################################################################################################
##############################keperluan crontab linux
/********************************************************************************
    NOTE :
    SCRIPT DI BAWAH INI DISAMAKAN DENGAN SCRIPT DI ATAS ;)
*****									********
***********							****************
*********************				           *********************
*****************************			   *****************************
********************************************************************************
********************************************************************************
*********************************************************************************/	
	function cronimportellipse($key='') {

		error_reporting(0);
		set_time_limit(3600);
		ob_implicit_flush(1);
		
		$this->_cronimportmaster('ellipse.knowvation_master_status', "{$this->um}.status", 'kodestatus', 'nama');		
		$this->_cronimportmasterunit();
		$this->_cronimportmastersubdit();
		$this->_cronimportmasterjabatan();
		$this->_cronimportmasterdirektorat();
		$this->_cronimportusers();
		
	}
	
	function _cronimportmasterunit() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select TABLE_CODE, TABLE_DESC   
			from ELLIPSE.KNOWVATION_MASTER_UNIT ";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['TABLE_CODE']);
			$nama = trim($row['TABLE_DESC']);
			
			$sql = "select 1 from {$this->um}.unit where kodeunit='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.unit set nama='$nama' where kodeunit='$kode'";
			else
				$sql = "insert into {$this->um}.unit (kodeunit, nama) values ('$kode', '$nama')";
			
			dbQuery($sql);
		}
	}
	
	function _cronimportmastersubdit() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select KODE_SUBDIT, SUBDIT, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY KODE_SUBDIT, SUBDIT 
			ORDER BY KODE_SUBDIT, SUBDIT";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['KODE_SUBDIT']);
			$nama = trim($row['SUBDIT']);
			
			$sql = "select 1 from {$this->um}.subdit where kodesubdit='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.subdit set nama='$nama' where kodesubdit='$kode'";
			else
				$sql = "insert into {$this->um}.subdit (kodesubdit, nama) values ('$kode', '$nama')";
			
			dbQuery($sql);
		}
	}
	
	function _cronimportmasterjabatan() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select POSITION_ID, POSISI, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY POSITION_ID, POSISI 
			ORDER BY POSITION_ID, POSISI";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['POSITION_ID']);
			$nama = trim($row['POSISI']);
			
			$sql = "select 1 from {$this->um}.jabatan where kodejabatan='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.jabatan set nama='$nama' where kodejabatan='$kode'";
			else
				$sql = "insert into {$this->um}.jabatan (kodejabatan, nama) values ('$kode', '$nama')";
			
			dbQuery($sql);
		}
	}
	
	function _cronimportmasterdirektorat() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select KODE_POSISI_DITBID, POSISI_DITBID, count(*)  
			from ELLIPSE.MOFFICE_DATAPEGAWAI_V1 
			GROUP BY KODE_POSISI_DITBID, POSISI_DITBID 
			ORDER BY KODE_POSISI_DITBID, POSISI_DITBID";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['KODE_POSISI_DITBID']);
			$nama = trim($row['POSISI_DITBID']);
			
			$sql = "select 1 from {$this->um}.direktorat where kodedirektorat='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update {$this->um}.direktorat set nama='$nama' where kodedirektorat='$kode'";
			else
				$sql = "insert into {$this->um}.direktorat (kodedirektorat, nama) values ('$kode', '$nama')";
			
			dbQuery($sql);
		}
	}
	
	function _cronimportmaster($table_src, $table_dest, $kode_dest, $nama_dest) {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		
		$sql = "select * from $table_src";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i=0;
		foreach ($rows as $row) {
			$i++;
			$kode = trim($row['TABLE_CODE']);
			$nama = trim($row['TABLE_DESC']);
			if ($table_src == 'ellipse.knowvation_master_jabatan') {
				$kode = trim($row['POSITION_ID']);
				$nama = trim($row['POS_TITLE']);
			}
			else if ($table_src == 'ellipse.knowvation_master_unit') {
				if (strstr($kode, '**'))
					continue;
			}
			
			$sql = "select 1 from $table_dest where $kode_dest='$kode'";
			if (dbGetOne($sql)) 
				$sql = "update $table_dest set $nama_dest='$nama' where $kode_dest='$kode'";
			else
				$sql = "insert into $table_dest ($kode_dest, $nama_dest) values ('$kode', '$nama')";
			
			dbQuery($sql);
		}
	}
	
	function _cronimportusers() {
		$this->load->database();
		$ellipse = $this->load->database('ellipse', true);
		$table_dest = "{$this->um}.users";
		$sql = "select * from ellipse.moffice_datapegawai_v1";
		$rows = dbGetRows($sql, '', '', $ellipse);

		$i = 0;
		foreach ($rows as $row) {
			$i++;
			$nid = trim($row['NID']);
			$nama = str_replace("'", "''",trim($row['NAMA_LENGKAP']));
			$kodeunit = trim($row['KODE_UNIT']);
			$kodedirektorat = trim($row['KODE_POSISI_DITBID']);
			$kodesubdit = trim($row['KODE_SUBDIT']);
			$kodefungsi = null;//trim($row['FUNGSI']); /*gak dipake*/
			$kodestaff = null;//trim($row['KDSTAFF']); /*gak dipake*/
			$kodestatus = 1; /*senua yang dari ellipse dianggap tetap*/
			$kodejabatan = trim($row['POSITION_ID']);
			$email = trim($row['EMAIL']);
			
			$sql = "select 1 from $table_dest where nid='$nid'";
			if (dbGetOne($sql)) 
				$sql = "update $table_dest set nama='$nama', email='$email', kodeunit='$kodeunit', kodejabatan='$kodejabatan', kodedirektorat='$kodedirektorat', kodesubdit='$kodesubdit',
				kodefungsi='$kodefungsi', kodestaff='$kodestaff', kodestatus='$kodestatus'
				where nid='$nid'";
			else
				$sql = "insert into $table_dest (nid, nama, email, kodeunit, kodejabatan, kodedirektorat, kodesubdit, kodefungsi, kodestaff, kodestatus) 
				values ('$nid', '$nama', '$email', '$kodeunit', '$kodejabatan', '$kodedirektorat', '$kodesubdit', '$kodefungsi', '$kodestaff', '$kodestatus')";
			
			dbQuery($sql);
		}
	}

}
