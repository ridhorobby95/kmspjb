<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Base_Controller {

	protected $model_p = 'role_model';
	protected $detail_view = 'role_detail';
	protected $list_view = 'role_list';
	protected $list_limit = 100;
	protected $add_title = 'Tambah Role';
	protected $edit_title = 'Edit Role';
	protected $detail_title = 'Detil Role';
	protected $daftar_title = 'Daftar Role';
	protected $posts = array('nama');
	protected $field_list = array(
									array('name'=>'nama','type'=>'C','label'=>'Nama'),
								);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
	}
	
	protected function _preEdit() {
		$this->data['items'] = $this->model->getItems($this->data['row']['IDROLE']);
		$this->data['users'] = $this->model->getUsers($this->data['row']['IDROLE']);
	}

	protected function _preDetail() {
		$this->data['items'] = $this->model->getItems($this->data['row']['IDROLE'], false);
		$this->data['users'] = $this->model->getUsers($this->data['row']['IDROLE']);
	}
	
	protected function _postEdit() {
		$idrole = $this->data['row']['IDROLE'];
		
		if (!$this->model->deleteRoleItem($idrole))
			return false;
		
		$arrItem = array();
		foreach ($_POST['iscreate'] as $item) {
			$arrItem[$item]['iscreate'] = 1;
		}
		foreach ($_POST['isread'] as $item) {
			$arrItem[$item]['isread'] = 1;
		}
		foreach ($_POST['isupdate'] as $item) {
			$arrItem[$item]['isupdate'] = 1;
		}
		foreach ($_POST['isdelete'] as $item) {
			$arrItem[$item]['isdelete'] = 1;
		}
		
		foreach ($arrItem as $iditem=>$item) {
			$record = array();

			$record['IDROLE'] = $idrole;
			$record['IDITEM'] = $iditem;
			if ($item['iscreate'])
				$record['ISCREATE'] = 1;
			if ($item['isread'])
				$record['ISREAD'] = 1;
			if ($item['isupdate'])
				$record['ISUPDATE'] = 1;
			if ($item['isdelete'])
				$record['ISDELETE'] = 1;
			
			$ret = $this->model->insertRoleItem($record);
		}
		
	}

}
