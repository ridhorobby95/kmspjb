<?php

class Akses extends Base_Controller {

    public function __construct()
    {
		parent::__construct();
		$this->load->model('user_model','User');
		$this->data['menus'] = '';
	}
	
    function index() {
		if (!xIsLoggedIn())
			redirect('login');
		
        $iduser = $_SESSION[G_SESSION]['iduser'];
		$this->data['user'] = $user = $this->User->getRow($iduser);
        // var_dump($this->data['user']);die();

        if ($_POST) {
            list($idrole,$idjabatan) = explode(':',$_POST['rolejabatan']);
            $_SESSION[G_SESSION]['idrole'] = $idrole;
			
			$sql = "select nama from {$this->um}.role where idrole=$idrole";
            $_SESSION[G_SESSION]['namarole'] = dbGetOne($sql);

            $_SESSION[G_SESSION]['kodeunit'] = $user['KODEUNIT'];			
			xUserLog("akses. idrole:$idrole | kodeunit:$kodeunit");
			redirect('home');
        }
        		
        $roles = $this->User->getRoles($_SESSION[G_SESSION]['iduser']);
        $this->data['roles'] = $roles;

        $this->renderView('akses');
    }

}

