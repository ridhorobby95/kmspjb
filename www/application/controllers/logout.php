<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	function index() {
		if (xIsLoggedIn()) {
			$this->load->database();
			xUserLog('logout');
		}
		unset($_SESSION[G_SESSION]);
		redirect('home');
	}

	
	
}
