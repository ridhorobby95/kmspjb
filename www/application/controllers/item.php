<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends Base_Controller {

	protected $model_p = 'item_model';
	protected $detail_view = 'item_detail';
	protected $list_view = 'item_list';
	protected $list_limit = 100;
	protected $add_title = 'Tambah Item';
	protected $edit_title = 'Edit Item';
	protected $detail_title = 'Detil Item';
	protected $daftar_title = 'Daftar Item';
	protected $posts = array('nama', 'namafile', 'idmodul','isactive');
	protected $field_list = array(
					array('name'=>'nama','type'=>'C','label'=>'Nama','width'=>'250px'),
					array('name'=>'namafile','type'=>'C','label'=>'File/Controller'),
				);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
	}
	
}
