<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Base_Controller {

	protected $model_p = 'user_model';
	protected $detail_view = 'profile_detail';
	
    function __construct() {
		parent::__construct();
		
		$this->data['custom_form'] = 1;
		
		$this->_initModel();
		$this->responsePost();
	}
	
	function index() {
		if ($_FILES) {
			if ($_FILES['foto_profile']) {
				if ($this->_updateFoto())
					redirect('profile');
			}
		}
		$profile = $this->model->GetRow($_SESSION[G_SESSION]['iduser']);
		
		$this->load->model('KM_model', 'KM');
		$this->load->model('UNIT_model', 'UNIT');
		
		$this->data['a_unit'] = $this->UNIT->getRow($_SESSION[G_SESSION]['kodeunit']);		
		$this->data['num_knowledge'] = $this->KM->getNumKnowledgeSubmit($_SESSION[G_SESSION]['iduser']);
		$this->data['num_komentar'] = $this->KM->getNumKomentarSubmit($_SESSION[G_SESSION]['iduser']);
		$this->data['num_tulis'] = $this->KM->getNumNulisSubmit($_SESSION[G_SESSION]['iduser']);
		$this->data['num_anggota'] = $this->KM->getNumAnggotaSubmit($_SESSION[G_SESSION]['iduser']);

		$this->data['profile'] = $profile;

		if ($_SESSION[G_SESSION]['idrole'] == '1') {
			$this->data['stat'] = $this->KM->getStat($_SESSION[G_SESSION]['iduser']);
		}
		// echo "<pre>";print_r($this->data);die();
		$this->renderView($this->detail_view);
	}
	
	function _updateFoto() {
		$pathinfo = pathinfo($_FILES['foto_profile']['tmp_name']);
		$dir_temp = $pathinfo['dirname'];

		$ext = strtolower(substr(strrchr($_FILES['foto_profile']['name'],"."),1));

		if ($ext != 'jpg') {
			//$this->session->set_flashdata('err_msg', 'File yang diijinkan adalah file .jpg');
			$this->_setNotification('File yang diijinkan untuk foto profile adalah file .jpg', 'E');
			redirect('profile');
		}
		
		$profile_path = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR;
		
		$iduser = $_SESSION[G_SESSION]['iduser'];

		$max_width = 500;
		$max_height = 500;
		
		$this->load->library('image_lib');
		
		$file = $_FILES['foto_profile']['tmp_name'];
		
		$thumbnail = xResizeCropImage($file, false, $max_width, $max_height);
		$new_file = $profile_path."$iduser.$ext";
		file_put_contents($new_file, $thumbnail);
		
		for ($i=1; $i<=5; $i++) {
			if ($i==1) {
				$config['width'] = 300;
				$config['height'] = 300;
				$config['new_image'] = strtolower($iduser."_300.$ext");
			}
			elseif ($i==2) {
				$config['width'] = 150;
				$config['height'] = 150;
				$config['new_image'] = strtolower($iduser."_150.$ext");
			}
			elseif ($i==3) {
				$config['width'] = 50;
				$config['height'] = 50;
				$config['new_image'] = strtolower($iduser."_50.$ext");
			}
			elseif ($i==4) {
				$config['width'] = 40;
				$config['height'] = 40;
				$config['new_image'] = strtolower($iduser."_40.$ext");
			}
			elseif ($i==5) {
				$config['width'] = 32;
				$config['height'] = 32;
				$config['new_image'] = strtolower($iduser."_32.$ext");
			}
			
			$thumbnail = xResizeCropImage($file, false, $config['width'], $config['height']);
			file_put_contents($profile_path .$config['new_image'], $thumbnail);
		
		}
		
		return true;
	}
	
	function opsi() {
		$iduser = $_SESSION[G_SESSION]['iduser'];
		if ($_POST['id'] != $iduser)
			die();
		$sql = "select opsi from um.users where iduser=$iduser";
		$opsi = dbGetOne($sql);
		$opsi = json_decode($opsi);
		if (!is_array($opsi))
			$opsi = array();
		
		$opsi[$_POST['opsi']] = $_POST['val'];
		
		$opsi = json_encode($opsi);
		
		$sql = "update um.users set opsi='$opsi' where iduser=$iduser";
		if (dbQuery($sql)) {
			$_SESSION[G_SESSION]['opsi_home'] = $_POST['val'];
			// ringkas
			if ($_POST['val'] == 'r') {
				unset($_SESSION[G_SESSION]['opsi_home_session']);
			}
			die('Opsi berhasil diubah');
		}
		
		die('ERROR');
		
	}
	
}
