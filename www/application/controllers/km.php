<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Km extends Base_Controller {

	protected $model_p = 'km_model';
	protected $detail_view = 'km_detail';
	protected $list_view = 'km_list';
	protected $list_limit = 20;
	protected $daftar_title =  'Daftar Knowledge';
	protected $posts = array('idjeniskm', 'nama', 'judul', 
		// 'pemateri', 'idpemateri', 'jabatanpemateri', 
		'abstrak', 'tahuntulis', 'kodeunit', 'waktupelaksanaan', 'jumlahpeserta',
		'createdby', 'createdtime', 'modifiedby', 'modifiedtime', 'scorecard', 'pelatihan' , 'actionplan', 'progress', 'link');
	protected $field_list = array(
									array('name'=>'judul','type'=>'C','label'=>'Judul'),
									array('name'=>'j.nama','type'=>'C','label'=>'Jenis'),
									array('name'=>'u.nama','type'=>'C','label'=>'Pemateri'),
									array('name'=>'t.nama','type'=>'C','label'=>'Unit'),
									array('name'=>'k.createdtime','type'=>'C','label'=>'Waktu', 'width'=>'80px'),
								);
	
    function __construct() {
		parent::__construct();
		
		$this->data['custom_form'] = 1;
		$this->load->helper('text');
		
		if ($_POST['act'] == 'reset_filter') {
			unset($_SESSION[G_SESSION][$this->ctl]);
			unset($_SESSION[G_SESSION]['cari_km']);
			unset($_SESSION[G_SESSION]['filter_km']);
			unset($_SESSION[G_SESSION]['filter_unit']);
			redirect('km/lst');
		}

		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
		
		$this->data['list_jenis'] = $this->model->getListJenisKM();
		$this->data['list_subyek'] = $this->model->getListSubyekKM();
		$this->data['list_subyek_pgd'] = $this->model->getListSubyekKMPGD();
		$this->data['keyword'] = dbGetRows('select * from KM.KEYWORDS');
		$this->load->model('user_model');
		$this->data['list_users'] = $this->user_model->getListUsers();
		
		$this->load->model('Unit_model', 'UNIT');
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$this->data['list_unit'] = $this->UNIT->getList();
	}
	
	function lst($offset=0) {
		$this->data['custom_form'] = false;
		// delete by fach, cz admin unit can see you
		//if (!$this->_isAllowList())
		//	$this->toNoData('Anda tidak berhak mengakses halaman ini');
		$this->_setKMFilter2();
		
		parent::lst($offset);
	}
	
	function draft() {
		parent::lst();
	}

	function cobaFitur(){
		echo "<pre>";print_r($this->posts);die();
		// $this->load->model('Notification_model');
		// $id_user_notif = $this->Notification_model->show_sql(false)->column('iduser')->filter('where idnotifications=17')->getOne();
		// echo $this->model->getOne('WAKTUPELAKSANAAN', 'AE4758D3167E457E90296EE4C648341B');
		// $now  = date("m/d/Y");
		// // 12/25/2018 ::01/01/2019
		// $date = "01/01/2019";
		// // // strtotime('now') >= strtotime('+1 month', strtotime($date));
	 // // //    $date1= '2015-02-05';
	 // // //    $date2= '2015-02-03';
	 // // //    $datetime1 = new DateTime($date1);
	 // // //    $datetime2 = new DateTime($date2);
		// echo $date." : ".$now;
	 //    if (strtotime($date) > strtotime($now)){ 
		// 	echo "masuk";
		// }
	 //    // $difference = $datetime1->diff($datetime2);
	 //    // echo $difference->days;
		// // $sql = "SELECT WAKTUPELAKSANAAN FROM KNOWLEDGE";
		// // $data = array(
		// // 	'a'	=> 1,
		// // 	'b'	=> 2
		// // );
		// $a = $this->model->cobaModel($data);
	}

	
	function add($idjeniskm) {
		$this->load->model('km_reference_model');
		if ($_POST['act'] == 'add_km') {
			$post = $this->input->post();
			// echo "<pre>";print_r($post);die();
			if ($this->_add())
				die('0');
			else
				die('ERROR');
		}
		
		if (!$_SESSION[G_SESSION]['new_id'])
			$_SESSION[G_SESSION]['new_id'] = dbGuid();
		
		$this->data['new_id'] = $_SESSION[G_SESSION]['new_id'];
		$this->data['idjeniskm'] = $idjeniskm;
		foreach ($this->data['list_jenis'] as $jenis) {
			if ($jenis['IDJENISKM'] == $idjeniskm) {
				$this->data['page_title'] = $jenis['NAMA'];
				break;
			}
		}
			
		$this->renderView('km_add');
	}

	// function addkeywords(){
	// 	$data = array(
	// 			'keyword' 		=> 'abasdsad',
	// 			'created_at'	=> "TO_DATE('".date("d/m/Y h:i:s")."' , 'DD/MM/YY HH24:MI:SS')"
	// 	);
	// 	// print_r($data);die();
	// $date = date("d/m/Y h:i:s");
	// $a = "INSERT INTO KEYWORDS (KEYWORD, CREATED_AT) VALUES ('sadsadsadsa', TO_DATE('".$date."' , 'DD/MM/YY HH24:MI:SS')) ";
	// dbQuery($a);
	// $sql_id = "SELECT * FROM KEYWORDS WHERE CREATED_AT=TO_TIMESTAMP('".$date."','dd-mm-yyyy hh24:mi:ss')";
	// $id = dbGetOne($sql_id);
	// echo $id;
	// 	// dbInsert('keywords', $data);
	// }
	function _add() {
		if (!$_SESSION[G_SESSION]['new_id'])
			$_SESSION[G_SESSION]['new_id'] = dbGuid();
		
		$record = $this->_initRecord();
		$record['idknowledge'] = $_SESSION[G_SESSION]['new_id'];
		$record['nama'] = $this->_nextnama($record['idjeniskm']);
		$record['createdby'] = $_SESSION[G_SESSION]['iduser'];
		if ($record['waktupelaksanaan'])
			$record['waktupelaksanaan'] = dbGetRecordDate($record['waktupelaksanaan']);
			
		// if (!$record['pemateri']) {
		// 	$record['pemateriluar'] = $_POST['namapemateri'];
		// 	$record['pemateri'] = '{{null}}';
		// }
		// INSERT PEMATERI LEBIH DARI 1
		if ($_POST['pemateri']) {
			foreach ($_POST['pemateri'] as $k => $v)
				$pemateri[$k]['NAMAPEMATERI'] = $v;
			foreach ($_POST['idpemateri'] as $k => $v)
				$pemateri[$k]['IDPEMATERI'] = $v;
			foreach ($_POST['jabatanpemateri'] as $k => $v)
				$pemateri[$k]['JABATANPEMATERI'] = $v;
		}
		foreach ($pemateri as $key => $value) {
			// INSERT PEMATERI UTAMA
			if ($key == 0) {
				$pemateri[$key]['ISPEMATERIUTAMA'] = 1;
				$record['pemateri'] = $value['IDPEMATERI'] != 0 ? $value['IDPEMATERI'] : '{{null}}';
				$record['pemateriluar'] = $value['IDPEMATERI'] != 0 ? '{{null}}' : $value['NAMAPEMATERI'];
				$record['jabatanpemateri'] = $value['JABATANPEMATERI'];
			}
		}

		if($_POST['keywords_km']){
			$keywords = $_POST['keywords_km'];
		}
		if($_POST['peserta_km']){
			$peserta = $_POST['peserta_km'];
			$record['jumlahpeserta'] = count($_POST['peserta_km']);
		}
		$idsubyek = $_POST['idsubyek'];
		if($record['idjeniskm'] == 5){
			$idsubyek = $_POST['idsubyekpgd'];
		}
		// $keywords;
		// echo "<pre>";print_r($record);echo "</pre>";
		// echo "<pre>";print_r($pemateri);echo "</pre>";
		$ret = $this->model->add($record, $keywords, $peserta, $pemateri);
		if (!$ret)
			return false;
		
		$this->_insertLampiran();
		$this->model->updateSubyek($_SESSION[G_SESSION]['new_id'], $idsubyek);

		if($_POST['referensi_km'])
			$this->km_reference_model->updateReferenceKM($_SESSION[G_SESSION]['new_id'], $_POST['referensi_km']);
		
		if($_POST['anggotatim'])
			$this->model->updateAnggotaTim($_SESSION[G_SESSION]['new_id'], $_POST['anggotatim']);
		
		xKMLog($_SESSION[G_SESSION]['new_id'], 'add');
		
		unset($_SESSION[G_SESSION]['new_id']);
		return true;
	}
	
	function _preEdit() {
		if (!$this->model->_isAllowEdit($this->data['row']))
			$this->toNoData('Anda tidak bisa mengedit knowledge ini');

		$this->load->model('Peserta_knowledge_model');
		$this->load->model('Km_pemateri_model');
		$peserta = $this->Peserta_knowledge_model->getRows(['idknowledge' => $this->data['row']['IDKNOWLEDGE_H']]);
		foreach ($peserta as $key => $value)
			$this->data['row']['peserta'][] = $value['IDUSER'];
		$this->data['row']['pemateri_knowledge'] = $this->Km_pemateri_model->getRows(['idknowledge' => $this->data['row']['IDKNOWLEDGE_H']]);
		
		$this->data['page_title'] = $this->data['row']['NAMA'];

		$this->data['arr_subyek'] = array('x');
		foreach ($this->data['row']['list_subyek'] as $subyek)
			$this->data['arr_subyek'][] = $subyek['IDSUBYEK'];

		$this->data['arr_keywords'] = array('x');
		foreach ($this->data['row']['list_keywords'] as $keywords)
			$this->data['arr_keywords'][] = $keywords['IDKEYWORDS'];

		$arr_anggota = array();
		foreach ($this->data['row']['list_anggota'] as $anggota) {
			$arr_anggota[] = array('id'=>$anggota['IDUSER'],
						   'name'=> $anggota['NAMA'],
						   );
		}
		if (count($arr_anggota))
			$this->data['anggotatim_json'] = json_encode($arr_anggota);
	}

	function edit($id) {
		// echo "<pre>";print_r($this->data);die();
		if ($_POST['act'] == 'update_km') {
			if ($this->_edit($id))
				die('0');
			else
				die('ERROR');
		}
		elseif ($_POST['act'] == 'delete_foto') {
			if ($this->_deleteFoto())
				die('0');
			else
				die('ERROR');
		}
		elseif($_POST['act'] == 'delete_doc') {
			if ($this->_deleteDoc())
				die('0');
			else
				die('ERROR');			
		}
		elseif($_POST['act'] == 'delete_video') {
			if ($this->_deleteVideo())
				die('0');
			else
				die('ERROR');			
		}
		elseif ($_POST['act'] == 'add_file') {
			$ret = $this->_insertLampiran($id);
			if (!$ret)
				die('ERROR');
			
			die($ret);
		}
		$this->data['edited'] = 1;
		
		parent::edit($id, 'km_edit');
	}
	/* fixing bugs pengecekan seelum update anggota tim dan subyek*/
	function _edit($id) {
		$record = $this->_initRecord();
		$record['modifiedby'] = $_SESSION[G_SESSION]['iduser'];
		$record['modifiedtime'] = '{{sysdate}}';
		if ($record['waktupelaksanaan'])
			$record['waktupelaksanaan'] = dbGetRecordDate($record['waktupelaksanaan']);
		else 
			$record['waktupelaksanaan'] = '{{null}}';
		// if (!$record['pemateri']) {
		// 	$record['pemateriluar'] = $_POST['namapemateri'];
		// 	$record['pemateri'] = '{{null}}';
		// }
		if ($_POST['pemateri']) {
			foreach ($_POST['pemateri'] as $k => $v)
				$pemateri[$k]['NAMAPEMATERI'] = $v;
			foreach ($_POST['idpemateri'] as $k => $v)
				$pemateri[$k]['IDPEMATERI'] = $v;
			foreach ($_POST['jabatanpemateri'] as $k => $v)
				$pemateri[$k]['JABATANPEMATERI'] = $v;
		}
		foreach ($pemateri as $key => $value) {
			// INSERT PEMATERI UTAMA
			if ($key == 0) {
				$pemateri[$key]['ISPEMATERIUTAMA'] = 1;
				$record['pemateri'] = $value['IDPEMATERI'] != 0 ? $value['IDPEMATERI'] : '{{null}}';
				$record['pemateriluar'] = $value['IDPEMATERI'] != 0 ? '{{null}}' : $value['NAMAPEMATERI'];
				$record['jabatanpemateri'] = $value['JABATANPEMATERI'];
			}
		}

		if (!$record['jumlahpeserta']) {
			$record['jumlahpeserta'] = '{{null}}';
		}

		$ret = $this->model->update($record, $id);
		if (!$ret) {
			$this->_setNotification(lang('data_updated_fail', 'E'));
			return false;
		}
		
		if($_POST['keywords_km']){
			$keywords = $_POST['keywords_km'];
			$this->checkKeywordsEdit($keywords, $id);
		}

		if (!empty ($_POST['idsubyek']))
			$this->model->updateSubyek($id, $_POST['idsubyek']);

		
		if (!empty ($_POST['anggotatim']))
			$this->model->updateAnggotaTim($id, $_POST['anggotatim']);

		if (!empty ($_POST['peserta_km'])) {
			$this->load->model('Peserta_knowledge_model');
			$this->Peserta_knowledge_model->delete2(['idknowledge' => $id]);
			
	        foreach ($_POST['peserta_km'] as $val) {
	        	$waktupelaksanaan = $this->model->getOne('WAKTUPELAKSANAAN', $id);
	        	$dataPeserta = array(
	        		'IDUSER'		=> $val,
	        		'IDKNOWLEDGE'	=> $id
	        	);
	        	$this->Peserta_knowledge_model->add($dataPeserta);

	        	$this->load->model('Notification_model');
            	$dataNotif = array(
            		'iduser'			=> $val,
            		'judul'				=> $record['judul'],
            		'idknowledge'		=> $id,
            		'waktupelaksanaan'	=> $waktupelaksanaan
            	);
				$insertNotif = $this->Notification_model->add($dataNotif);
	        }
		}

		if (!empty ($_POST['pemateri'])) {
			$this->load->model('Km_pemateri_model');
			$this->Km_pemateri_model->delete2(['idknowledge' => $id]);
			
            foreach ($pemateri as $val) {
            	$dataInsert = array(
            		'IDPEMATERI'		=> $val['IDPEMATERI'] != "" ? $val['IDPEMATERI'] : '{{null}}',
            		'NAMAPEMATERI'		=> $val['NAMAPEMATERI'],
            		'JABATANPEMATERI'	=> $val['JABATANPEMATERI'],
            		'ISPEMATERIUTAMA'	=> $val['ISPEMATERIUTAMA'] != "" ? $val['ISPEMATERIUTAMA'] : '{{null}}',
            		'IDKNOWLEDGE'		=> $id
            	);
				$insert = $this->Km_pemateri_model->add($dataInsert);
            }
		}
		
		xKMLog($id, 'update');
		
		$this->_setNotification(lang('data_updated'));
		
		return true;
	}

	function notification(){
		$this->load->model('Notification_model');
		$this->data['dataNotif'] = $this->Notification_model->getNotification();
		// echo "<pre>";print_r($this->data['dataNotif']);die();
		$this->renderView('notifications');

	}

	function checkKeywordsEdit($keywords, $idknowledge){
		$datakeywords = "{";
        for ($j=0; $j < count($keywords) ; $j++) {
        	$date = date("d/m/Y h:i:s");
            $dataInsertKeywords = array(
                'keyword'       => $keywords[$j],
                'created_at'	=> $date

            );
            // echo "<pre>";print_r($dataInsertKeywords);echo "</pre>";
            // die();
            $checkinteger = (int)$keywords[$j];
            if($checkinteger == 0){
                $sql_exist = "select idkeywords from {$this->km}.keywords where keyword='".$keywords[$j]."'";
            }
            else{
                $sql_exist = "select idkeywords from {$this->km}.keywords where idkeywords='".$keywords[$j]."'";
            }
            $exist_id = dbGetOne($sql_exist);
            if($exist_id != null){
                $keywords_id = $exist_id;
            }
            else{
	// 
                $insertKeywords = dbQuery("INSERT INTO {$this->km}.KEYWORDS (KEYWORD, CREATED_AT) VALUES ('".$keywords[$j]."', TO_DATE('".$date."' , 'DD/MM/YY HH24:MI:SS'))");
                $sql_id = "SELECT IDKEYWORDS FROM {$this->km}.KEYWORDS WHERE CREATED_AT=TO_TIMESTAMP('".$date."','dd-mm-yyyy hh24:mi:ss')";
                $keywords_id = dbGetOne($sql_id);
            }   
            $datakeywords .= "\"".$j."\":\"".$keywords_id."\"";
            if($j != count($keywords)-1){
                $datakeywords .= ",";
            }
            $dataInsertKnowKey = array(
                'IDKEYWORDS'    => $keywords_id,
                'IDKNOWLEDGE'   => $idknowledge
            );
            $check_exist = "select * from {$this->km}.knowledge_keywords where idkeywords=".$keywords_id." and idknowledge='".$idknowledge."'";
            $keyword_exist = dbGetRow($check_exist);
            if($keyword_exist == null){
                $dataInsertKnowKey = array(
                    'id_keyword'    => $keywords_id,
                    'id_knowledge'  => $idknowledge
                );
                $insert_knowledge_keywords = dbInsert('km.knowledge_keywords', $dataInsertKnowKey);
            }    
        }
        $datakeywords .= "}";
        $updatekeywords = array(
            'keywords' => $datakeywords
        );
        dbUpdate('km.knowledge', $updatekeywords, "idknowledge='".$idknowledge."'");
        return $datakeywords;
	}
	
	function delete($id) {
		$id = xRemoveSpecial($id);
		$this->data['row'] = $this->model->GetRow($id);
		if (!$this->model->_isAllowDelete($this->data['row']))
			$this->toNoData('Anda tidak bisa menghapus knowledgebase ini.');

		if ($_POST['act'] == 'delete') {
			$sql = "update {$this->km}.knowledge set isdelete=1 where idknowledge='$id'";
			if (dbQuery($sql)) {
				xKMLog($id, 'delete');
				$this->_setNotification(lang('data_deleted'));
				die('0');
			}
			else
				die('ERROR');
		}
		
		$this->renderView('km_detail');
	}
	
	function _deleteFoto() {
		list($idknowledge, $idfoto) = explode('-',$_POST['id']);
		$idfoto = (int) $idfoto;
		$idknowledge = xRemoveSpecial($idknowledge);
		
		if ($this->model->deleteFoto($idknowledge, $idfoto)) {
			$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $idfoto;
			unlink($thumbnail_file);
			return true;
		}
		return false;
	}

	function _deleteDoc() {
		list($idknowledge, $iddoc) = explode('-',$_POST['id']);
		$iddoc = (int) $iddoc;
		$idknowledge = xRemoveSpecial($idknowledge);
		
		if ($this->model->deleteDoc($idknowledge, $iddoc)) {
			$thumbnail_file = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $iddoc;
			unlink($thumbnail_file);
			return true;
		}
		return false;
	}
	
	function _deleteVideo() {
		list($idknowledge, $idvideo) = explode('-',$_POST['id']);
		$idvideo = (int) $idvideo;
		$idknowledge = xRemoveSpecial($idknowledge);
		
		$sql = "select namafile from {$this->km}.video where idvideo=$idvideo";		
		$namafile = dbGetOne($sql);
		
		$namafiledir = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $idknowledge . '-' . $namafile;
		$namafiletempdir = $this->config->item('upload_temp') . DIRECTORY_SEPARATOR . $idknowledge . $namafile;
		

		if ($this->model->deleteVideo($idknowledge, $idvideo)) {
			unlink($namafiledir);
			unlink($namafiletempdir);

			return true;
		}
		return false;
	}
	
	function _preDetail() {
		$this->load->model('Peserta_knowledge_model');
		$this->load->model('Km_pemateri_model');
		$this->data['page_title'] = $this->data['row']['NAMA'];
		if ($_SESSION[G_SESSION]['idrole'] == '1') {
			$this->data['stat'] = $this->model->getStat($this->data['row']['IDKNOWLEDGE_H']);
		}
		if($this->data['row']){
			$this->data['row']['peserta'] = $this->Peserta_knowledge_model->getRowsJoinUser(['idknowledge' => $this->data['row']['IDKNOWLEDGE_H']]);
			$this->data['row']['pemateri_knowledge'] = $this->Km_pemateri_model->getRows(['idknowledge' => $this->data['row']['IDKNOWLEDGE_H']]);	
		}
		
	}
	
	function lampiran() {
		$id = xDecrypt(rawurldecode($_GET['q']));
		$sql = "select f.*, k.isdelete, RAWTOHEX(k.idknowledge) as idknowledge_h from {$this->km}.lampiran f join {$this->km}.knowledge k on f.idknowledge=k.idknowledge where idlampiran=$id";
		$doc = dbGetRow($sql);
		if (!$doc || $doc['ISDELETE'])
			die('File tidak ada');	
		
		$file_extension = strtolower(substr(strrchr($doc['NAMAFILE'],"."),1));

		switch( $file_extension ) {
			case "pdf": $ctype="application/pdf"; break;
			case "doc": $ctype="application/doc"; break;
			case "docx": $ctype="application/docx"; break;
			case "xls": $ctype="application/xls"; break;
			case "xlsx": $ctype="application/xlsx"; break;
			case "ppt": $ctype="application/ppt"; break;
			case "pptx": $ctype="application/pptx"; break;
			default:
			$ctype="application/force-download";
		}
		
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=\"{$doc['NAMAFILE']}\"");
		header("Content-Type: $ctype");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . $doc['UKURANFILE']);
		$sql = "select isifile from {$this->lp}.lampirankm where idlampiran=$id";
		$isifile = dbGetOne($sql);
		if ($isifile) {
			xDownloadLampiranLog($id);
			xLihatLog($doc['IDKNOWLEDGE_H']);
			echo $isifile->load();
			exit;
		}
		die('File tidak ada');	
	}
	
	function uploadfile($idknowledge=false) {
		if (!$_SESSION[G_SESSION]['new_id'])
			$_SESSION[G_SESSION]['new_id'] = dbGuid();
		
		$idknowledge = $idknowledge?$idknowledge:$_SESSION[G_SESSION]['new_id'];
		if (!empty($_FILES)) {
			$temp_file = $_FILES['file']['tmp_name'];
			$target_path = $this->config->item('upload_temp') . DIRECTORY_SEPARATOR;
			
			/* tambahan untuk upload mp4 */
			if ( $_FILES['file']['type'] == 'application/octet-stream' and !(strpos($_FILES['file']['name'].'.mp4')) )
			$ext = '.mp4';
			
			$main_file = $target_path . $idknowledge . $_FILES['file']['name'].($ext ? $ext : null);
			
			move_uploaded_file($temp_file, $main_file);
		}
	}
	
	function _insertLampiran($idknowledge=false) {
		$target_path = $this->config->item('upload_temp');
		$id = $idknowledge?$idknowledge:$_SESSION[G_SESSION]['new_id'];

		$files = glob($target_path . DIRECTORY_SEPARATOR . $id . '*.*');
		$contents = array();
		$i=0;
		$images = array('jpg', 'jpeg', 'gif', 'png', 'bmp');
		$videos = array('mp4');
		$docs = array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx');
		foreach($files as $file) {
			$i++;
			$pathinfo = pathinfo($file);
			$ext = strtolower($pathinfo['extension']);
			
			if (in_array($ext, $images)) {
				$this->load->library('image_lib');
				if ($this->_insertFoto($file, $id)) {
					//if ($idknowledge)
						//return 'foto';
				}
			}
			elseif (in_array($ext, $videos)) {
				if ($this->_insertVideo($file, $id)) {
					// if ($idknowledge)
						// return 'video';
				}
			}
			elseif (in_array($ext, $docs)) {
				if ($this->_insertDoc($file, $id, $ext)) {
					// if ($idknowledge)
						// return 'doc';
				}
			}
		}
		if ($idknowledge)
			return true;
		return false;
	}
	
	function _insertFoto($file, $idknowledge=false) {
		$target_path = $this->config->item('upload_temp');
		$idknowledge = $idknowledge?$idknowledge:$_SESSION[G_SESSION]['new_id'];
		$image_content = file_get_contents($file);
					
		$record = array();
		$record['idknowledge'] = $idknowledge;
		$record['namafile'] =  $namafile = str_replace($idknowledge, '', substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1));
		$record['ukuranfile'] = filesize($file);
		$record['tiduser'] = $_SESSION[G_SESSION]['iduser'];
		$idfoto = $this->model->addFoto($record);

		$new_filename = $target_path . DIRECTORY_SEPARATOR . $idknowledge . $i;

		$thumbnail = xResizeCropImage($file, $new_filename);
		
		$sql = "insert into {$this->lp}.lampiranfotokm (idfoto, thumbnail, isifile) values ($idfoto, EMPTY_BLOB(), EMPTY_BLOB()) RETURNING thumbnail, isifile INTO :thumbnail, :isifile";
		$ret = dbQueryBlob($sql, array(':thumbnail',':isifile'), array($thumbnail, $image_content));
		
		if ($ret) {
			$thumbnail_foto = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $idfoto;
			file_put_contents($thumbnail_foto, $thumbnail);

			xKMLog($idknowledge, 'add photo: ' . $namafile);
		}

		unlink($file);
		
		return $ret;
	}
	
	function _insertDoc($file, $idknowledge=false) {
		$target_path = $this->config->item('upload_temp');
		$idknowledge = $idknowledge?$idknowledge:$_SESSION[G_SESSION]['new_id'];
		$image = file_get_contents($file);
		
		$thumbnail = false;
		try {
			$thumbnail = new imagick($file.'[0]');
			$thumbnail->setImageFormat('jpg');
			$thumbnail = xResizeCropImage($thumbnail);
		}
		catch(Exception $e) {
			
		}
					
		$record = array();
		$record['idknowledge'] = $idknowledge;
		$record['namafile'] =  $namafile = str_replace($idknowledge, '', substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1));
		$record['ukuranfile'] = filesize($file);
		$record['tiduser'] = $_SESSION[G_SESSION]['iduser'];
		$idlampiran = $this->model->addDoc($record);

		
		if ($thumbnail) {
			$sql = "insert into {$this->lp}.lampirankm (idlampiran, thumbnail, isifile) values ($idlampiran, EMPTY_BLOB(), EMPTY_BLOB()) RETURNING thumbnail, isifile INTO :thumbnail, :isifile";
			$ret = dbQueryBlob($sql, array(':thumbnail', ':isifile'), array($thumbnail, $image));

			$thumbnail_doc = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . $idlampiran;
			file_put_contents($thumbnail_doc, $thumbnail);
		}
		else {
			$sql = "insert into {$this->lp}.lampirankm (idlampiran, isifile) values ($idlampiran, EMPTY_BLOB()) RETURNING isifile INTO :isifile";
			$ret = dbQueryBlob($sql, array(':isifile'), array($image));
		}

		if ($ret) {
			xKMLog($idknowledge, 'add doc: ' . $namafile);
		}

		unlink($file);
			
		return $ret;
	}

	function _insertVideo($file, $idknowledge=false) {
		# harus mp4 (support html5)
		
		$target_path = $this->config->item('upload_temp');
		$idknowledge = $idknowledge?$idknowledge:$_SESSION[G_SESSION]['new_id'];
		$image = file_get_contents($file);
		
		$record = array();
		$record['idknowledge'] = $idknowledge;
		$record['namafile'] =  $namafile = str_replace($idknowledge, '', substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1));
		$record['ukuranfile'] = filesize($file);
		$record['tiduser'] = $_SESSION[G_SESSION]['iduser'];
		$idlampiran = $this->model->addVideo($record);

		$filename = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $idknowledge . '-' . $namafile;
		
		$ret = file_put_contents($filename, $image);
		
		if ($ret)
			unlink ($this->config->item('upload_temp') . DIRECTORY_SEPARATOR . $target_path . $idknowledge . $namafile);
		
		if ($ret) {
			xKMLog($idknowledge, 'add video: ' . $namafile);
			return true;
		}
		
		return false;
	}

	function deletefile() {
		$target_path = $this->config->item('upload_temp');
		$id = $_SESSION[G_SESSION]['new_id'] . '_';
		$file = $target_path . DIRECTORY_SEPARATOR . $id . $_POST['file'];
		unlink($file);
	}

	function video() {
		$id = xDecrypt(rawurldecode($_GET['q']));
		$sql = "select f.*, k.isdelete, RAWTOHEX(k.idknowledge) as idknowledge_h from {$this->km}.lampiran f join {$this->km}.knowledge k on f.idknowledge=k.idknowledge where idlampiran=$id";
		$doc = dbGetRow($sql);
		if (!$doc || $doc['ISDELETE'])
			die('File tidak ada');	
		
		$file_extension = strtolower(substr(strrchr($doc['NAMAFILE'],"."),1));

		switch( $file_extension ) {
			case "pdf": $ctype="application/pdf"; break;
			case "doc": $ctype="application/doc"; break;
			case "docx": $ctype="application/docx"; break;
			case "xls": $ctype="application/xls"; break;
			case "xlsx": $ctype="application/xlsx"; break;
			default:
			$ctype="application/force-download";
		}
		
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=\"{$doc['NAMAFILE']}\"");
		header("Content-Type: $ctype");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length: ' . $doc['UKURANFILE']);
		$sql = "select isifile from {$this->lp}.lampirankm where idlampiran=$id";
		$isifile = dbGetOne($sql);
		if ($isifile) {
			xDownloadLampiranLog($id);
			xLihatLog($doc['IDKNOWLEDGE_H']);
			echo $isifile->load();
			exit;
		}
		die('File tidak ada');	
	}
	
	function cari() {
		$_SESSION[G_SESSION]['km_offset'] = 0;
		
		$q = xRemoveSpecial($_POST['q']);
		$_SESSION[G_SESSION]['cari_km'] = strtolower($q);
		$this->_addCari();
	}
	
	function datacari() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = strtolower($this->input->get('term'));
		$term = xRemoveSpecial($term);

		$sql = "select count(*), rtrim(cari) cari from {$this->km}.cari where lower(cari) like '%$term%' and rownum<=10 group by rtrim(cari) order by cari";
		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['CARI'];
			$arr[] = array('id'=>$row['CARI'],
						   'label'=> $row['CARI'],
						   'value'=> $row['CARI'],
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}
	
	function _addCari(){
		$record = array();
		$record['iduser'] = $_SESSION[G_SESSION]['iduser'];
		$record['namauser'] = substr($_SESSION[G_SESSION]['nama'],0,100);
		$record['tipaddress'] = substr($_SERVER['REMOTE_ADDR'],0,100);
		$record['cari'] = $_SESSION[G_SESSION]['cari_km'];
		$ret = $this->model->addCari($record);
	}

	function addfilter(){
		if($_POST['start'] && !isset($_SESSION[G_SESSION]['cari_km_startdate'])){
			$_SESSION[G_SESSION]['cari_km_startdate'] = date('m/d/Y', strtotime($_POST['start']));
		}
		if($_POST['end'] != null && !isset($_SESSION[G_SESSION]['cari_km_enddate'])){
			$_SESSION[G_SESSION]['cari_km_enddate'] =  date('m/d/Y', strtotime($_POST['end']));
		}
	}
	function removefilter(){
		unset($_SESSION[G_SESSION]['cari_km_startdate']);
		unset($_SESSION[G_SESSION]['cari_km_enddate']);
	}
	function removecari() {
		unset($_SESSION[G_SESSION]['cari_km']);
		unset($_SESSION[G_SESSION]['cari_km_startdate']);
		unset($_SESSION[G_SESSION]['cari_km_enddate']);
		if (strstr($_SERVER['HTTP_REFERER'], 'km/lst'))
			redirect('km/lst');
		else
			redirect('home');
	}
	
	function changeunit($id) {
		$id = xRemoveSpecial($id);
		$sql = "select 1 from {$this->um}.unit where kodeunit='$id'";
		if (!dbGetOne($sql)) {
			unset($_SESSION[G_SESSION]['filter_unit']);
		}
		else
			$_SESSION[G_SESSION]['filter_unit'] =  $id;
		
		if (strstr($_SERVER['HTTP_REFERER'], 'km/lst'))
			redirect('km/lst');
		else
			redirect('home');
	}
	
	function next() {
		if (!$_SESSION[G_SESSION]['km_page'])
			$_SESSION[G_SESSION]['km_page'] = 1;
		
		$_SESSION[G_SESSION]['km_page']++;
		$offset = ($this->model->home_limit * ($_SESSION[G_SESSION]['km_page'] - 1)) + 1;
		
		$this->_setKMFilter();

		$this->data['list_km'] = $this->model->getList($this->model->home_limit, $offset);
		
		$next_offset = ($this->model->home_limit * $_SESSION[G_SESSION]['km_page']) + 1;
		$this->data['is_next_exist'] = $this->model->getList($this->model->home_limit, $next_offset, true);
		
		$this->data['is_condensed'] = $_SESSION[G_SESSION]['is_condensed'];
		
		$this->load->view("default/_ajax_list_km", $this->data);
	}
	
	function reloadstat() {
		$id = xRemoveSpecial($_POST['id']);
		$sql = "select count(*) from {$this->km}.suka where idknowledge='$id'";
		$this->data['row']['NUM_SUKA'] = dbGetOne($sql);
		$sql = "select count(*) from {$this->km}.diskusi where idknowledge='$id' and isdelete=0";
		$this->data['row']['NUM_KOMENTAR'] = dbGetOne($sql);
		$this->data['row']['list_suka'] = $this->model->getListSuka($id);
		$this->load->view("default/_ajax_stat_km", $this->data);
		return;
	}
	
	function tambah() {
		$record = $this->_initRecord();
		$record['nama'] = $record['judul'];
		if ($record['waktupelaksanaan'])
			$record['waktupelaksanaan'] = dbGetRecordDate($record['waktupelaksanaan']);
		
		$ret = $this->model->add($record);
		if (!$ret)
			die('Error');
		else {
			die('0');
		}
	}
	
	function user() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = strtolower($this->input->get('term'));
		$term = xRemoveSpecial($term);

		$sql = "select * from {$this->um}.users where lower(nama) like '%$term%' and rownum<=10 order by nama";
		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['NAMA'];
			$arr[] = array('id'=>$row['IDUSER'],
						   'label'=> $row['NAMA'],
						   'value'=> $row['NAMA'],
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}

	function tesUser(){
		
	}

	function usertag() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = strtolower($this->input->get('q'));
		$term = xRemoveSpecial($term);

		$sql = "select * from {$this->um}.users where lower(nama) like '%$term%' and rownum<=10 order by nama";
		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['NAMA'];
			$arr[] = array('id'=>$row['IDUSER'],
						   'name'=> $row['NAMA'],
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}

	function unit() {
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = strtolower($this->input->get('term'));
		$term = xRemoveSpecial($term);

		$sql = "select * from {$this->um}.unit where lower(nama) like '%$term%' and rownum<=10 order by nama";
		$list = dbGetRows($sql);
		
		$arr = array();
		foreach ($list as $row) {
			$label_user = $row['NAMA'];
			$arr[] = array('id'=>$row['KODEUNIT'],
						   'label'=> $row['NAMA'],
						   'value'=> $row['NAMA'],
						   );
		}

		$json = json_encode($arr);
		echo $json;
		exit;
	}
	
	function jabatan($id) {
		$id = (int) $_POST['id'];
		$sql = "select j.nama as namajabatan, s.nama as namastaff
				from {$this->um}.users u 
				left join {$this->um}.jabatan j on u.kodejabatan=j.kodejabatan
				left join {$this->um}.staff s on u. kodestaff=s.kodestaff
				where u.iduser=$id";
		$user = dbGetRow($sql);
		$jabatan = $user['NAMASTAFF'];
		if ($user['NAMAJABATAN'])
			$jabatan .= ' - ' . $user['NAMAJABATAN'];
		echo $jabatan;
		exit;
	}
	
	function nextnama($idjeniskm) {
		die($this->_nextnama($idjeniskm));
	}
	
	function _nextnama($idjeniskm) {
		$idjeniskm = (int) $idjeniskm;
		$num = $this->model->nextNum($idjeniskm);
		if ($idjeniskm == '1')
			$str = "Sharing Knowledge $num Tahun ";
		elseif ($idjeniskm == '2')
			$str = "CoP $num Tahun ";
		elseif ($idjeniskm == '3')
			$str = "DOC $num Tahun ";
		elseif ($idjeniskm == '4')
			$str = "Innovation $num Tahun ";
		elseif ($idjeniskm == '5')
			$str = "Peer Group Discussion $num Tahun ";
		
		return $str . date('Y');
	}
	
	function thumbnail() {
		$id = xDecrypt(rawurldecode($_GET['q']));
		$sql = "select * from {$this->km}.foto where idfoto=$id";

		$foto = dbGetRow($sql);
		$file_extension = strtolower(substr(strrchr($foto['NAMAFILE'],"."),1));

		switch( $file_extension ) {
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "bmp": $ctype="image/bmp"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpeg"; break;
			default:
		}
		header('Content-type: ' . $ctype);		

		$thumbnail_foto = $this->config->item('static_path') . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $id;
		if (file_exists($thumbnail_foto)) {
			echo file_get_contents($thumbnail_foto);
			exit;
		}
		
		$sql = "select thumbnail from {$this->lp}.lampiranfotokm where idfoto=$id";
		$thumbnail = dbGetOne($sql);
		echo $thumbnail->load();
		exit;
	}
	
	function foto() {
		$id = xDecrypt(rawurldecode($_GET['q']));
		$sql = "select * from {$this->km}.foto where idfoto=$id";

		$foto = dbGetRow($sql);
		$file_extension = strtolower(substr(strrchr($foto['NAMAFILE'],"."),1));

		switch( $file_extension ) {
			case "gif": $ctype="image/gif"; break;
			case "png": $ctype="image/png"; break;
			case "jpeg":
			case "jpg": $ctype="image/jpeg"; break;
			default:
		}
		header('Content-type: ' . $ctype);
		
		$sql = "select isifile from {$this->lp}.lampiranfotokm where idfoto=$id";
		$isifile = dbGetOne($sql);
		echo $isifile->load();
		exit;
	}

	function _setKMFilter() {
		$this->model->filter = " where k.isdelete=0 ";
/*		
		$filter_jenis = ' 1=-1 '; 
		foreach ($this->data['list_jenis'] as $jenis) {
			if (!isset($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]))
				$_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']] = 1;
			
			if ($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]) {
				if ($filter_jenis)
					$filter_jenis .= " or ";
				$filter_jenis .= " k.idjeniskm={$jenis['IDJENISKM']} ";
			}
		}

		if ($filter_jenis)	
			$this->model->filter .= ' and (' . $filter_jenis . ')';
*/
		if ((int) $_SESSION[G_SESSION]['filter_km']['idjeniskm'])
			$filter_jenis = " k.idjeniskm={$_SESSION[G_SESSION]['filter_km']['idjeniskm']} ";
		if ($filter_jenis)	
			$this->model->filter .= ' and (' . $filter_jenis . ')';
		
	}

	function _setKMFilter2() {
		$this->model->filter = "where k.isdelete=0 " . $this->model->filter;
		
		$filter_jenis = ' 1=-1 '; 
		foreach ($this->data['list_jenis'] as $jenis) {
			if (!isset($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]))
				$_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']] = 1;
			
			if ($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]) {
				if ($filter_jenis)
					$filter_jenis .= " or ";
				$filter_jenis .= " k.idjeniskm={$jenis['IDJENISKM']} ";
			}
		}

		if ($filter_jenis)	
			$this->model->filter .= ' and (' . $filter_jenis . ')';
	}
	
	function komentar($status) {
		if ($_POST['postid']) {
			list($idknowledge, $iddiskusi) = explode('-',$_POST['postid']);
			$iddiskusi = (int) $iddiskusi;
			$idknowledge = xRemoveSpecial($idknowledge);
			$this->data['row'] = $knowledge = $this->model->GetRow($idknowledge);
		}

		if ($status == 'add') {
			$komentar = $_POST['subpost'];
			
			if ($this->model->_isAllowView($knowledge)) {
				$iddiskusi = $this->model->addKomentar($idknowledge, $komentar);
				if ($iddiskusi) {
					$this->data['komentar'] = $this->model->getKomentar($iddiskusi);
					$this->load->view("default/_ajax_post_km", $this->data);
					return;
				}
			}
			die('ERROR');
		}
		elseif ($status == 'edit') {
			$komentar = $_POST['subpost'];
			if ($this->model->_isAllowEdit($knowledge, $iddiskusi)) {
				if ($this->model->updateKomentar($idknowledge, $iddiskusi, $komentar)) {		
					die($komentar);
				}
			}
			die('ERROR');
		}
		elseif ($status == 'delete') {
			$komentar = $_POST['subpost'];
			if ($this->model->_isAllowEdit($knowledge, $iddiskusi)) {
				if ($this->model->deleteKomentar($idknowledge, $iddiskusi)) {		
					die('OK');
				}
			}
			die('ERROR');
		}
		die('ERROR');
	}
	
	function suka() {
		$idknowledge = $_POST['postid'];
		$idknowledge = xRemoveSpecial($idknowledge);
		$ret = $this->model->toggleSuka($idknowledge);
		
		die($ret);
	}
	
	function file($idknowledge) {
		if ($_POST['act'] == 'foto') {
			$this->data['row'] = $this->model->GetRow($idknowledge);
			
			$sql = "select * from {$this->km}.foto where idknowledge='$idknowledge' and rownum=1 order by idfoto desc";
			$this->data['foto'] = dbGetRow($sql);
			
			$this->data['add_foto'] = 1;
			$this->data['visible'] = 1;
			$this->data['edited'] = 1;
			
			$this->load->view("default/_ajax_detail_foto", $this->data);
			return;
		}
		elseif ($_POST['act'] == 'video') {
			return;
		}
		elseif ($_POST['act'] == 'doc') {
			$this->data['row'] = $this->model->GetRow($idknowledge);
			
			$sql = "select * from {$this->km}.lampiran where idknowledge='$idknowledge' and rownum=1 order by idlampiran desc";
			$this->data['doc'] = dbGetRow($sql);
			
			$this->data['add_doc'] = 1;
			$this->data['visible'] = 1;
			$this->data['edited'] = 1;
			
			$this->load->view("default/_ajax_detail_doc", $this->data);
			return;
		}
		exit;
	}
	
	function toggledetail() {
		$id = xRemoveSpecial($_POST['id']);
		if (!$id)
			die('ERROR');
		
		if (!isset($_SESSION[G_SESSION]['opsi_home_session'][$id]['is_condensed']))
			$_SESSION[G_SESSION]['opsi_home_session'][$id]['is_condensed'] = $_SESSION[G_SESSION]['is_condensed'];
		
		if ($_SESSION[G_SESSION]['opsi_home_session'][$id]['is_condensed']) {
			$this->data['is_condensed'] = 0;
			$_SESSION[G_SESSION]['opsi_home_session'][$id]['is_condensed'] = 0;
		}
		else {
			$this->data['is_condensed'] = 1;
			$_SESSION[G_SESSION]['opsi_home_session'][$id]['is_condensed'] = 1;
		}
		$this->data['row'] = $this->model->GetRow($id);
		$this->load->view("default/_ajax_detail_km", $this->data);
		return;
	}
	
	function refresh() {
		$id = xRemoveSpecial($_POST['id']);
		if (!$id)
			die('ERROR');
		
		$this->data['row'] = $this->model->GetRow($id);
		$this->load->view("default/_ajax_detail_km", $this->data);
		return;
	}
	
	function _isAllowList() {
		if ($_SESSION[G_SESSION]['idrole'] == '1')
			return true;
		return false;
	}

}
