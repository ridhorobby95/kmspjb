<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller {

	public function index() {
		$this->load->model('Peserta_knowledge_model');
		$this->load->model('Km_pemateri_model');
		$this->data['custom_form'] = 1;
		unset($_SESSION[G_SESSION]['new_id']);

		if ($_SESSION[G_SESSION]['cari_km'])
			$this->load->helper('text');
		
		$this->data['is_condensed'] = $_SESSION[G_SESSION]['is_condensed'];
		
		$_SESSION[G_SESSION]['km_page'] = 1;
		$this->load->model('km_model', 'KM');
		$this->data['list_jenis'] = $this->KM->getListJenisKM();
		$this->data['list_subyek'] = $this->KM->getListSubyekKM();
		$this->data['list_subyek_pgd'] = $this->KM->getListSubyekKMPGD();
		
		$this->load->model('unit_model', 'UNIT');
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$this->data['list_unit'] = $this->UNIT->getList();

		$this->_setKMFilter();
		$list_km = $this->KM->getList($this->KM->home_limit, 0);
		foreach ($list_km as $key => $value) {
			$list_km[$key]['peserta'] = $this->Peserta_knowledge_model->getRowsJoinUser(['idknowledge' => $value['IDKNOWLEDGE_H']]);
			$list_km[$key]['pemateri_knowledge'] = $this->Km_pemateri_model->getRows(['idknowledge' => $value['IDKNOWLEDGE_H']]);
		}
		$this->data['list_km'] = $list_km;
		$this->data['list_km_ref'] = $this->KM->getListRef();

		$this->load->model('user_model');
		$this->data['list_users'] = $this->user_model->getListUsers();
		
		$this->data['is_next_exist'] = $this->KM->isNextExist();

		$this->data['keyword'] = dbGetRows('select * from KM.KEYWORDS');


		// echo "<pre>";print_r($this->data);die();
		$this->data['page_subtitle'] = 'Home';
		$this->renderView('home');
	}
	
	function _setKMFilter() {
		$this->KM->filter = "where k.isdelete=0 ";
		//$filter_jenis = '1=-1'; 
		/*
		foreach ($this->data['list_jenis'] as $jenis) {
			if (!isset($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]))
				$_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']] = 1;
			
			if ($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']]) {
				if ($filter_jenis)
					$filter_jenis .= " or ";
				$filter_jenis .= " k.idjeniskm={$jenis['IDJENISKM']} ";
			}
		}
		*/
		if ((int) $_SESSION[G_SESSION]['filter_km']['idjeniskm'])
			$filter_jenis = " k.idjeniskm={$_SESSION[G_SESSION]['filter_km']['idjeniskm']} ";
		if ($filter_jenis)	
			$this->KM->filter .= ' and (' . $filter_jenis . ')';
	}
	
	function jenis($id) {
		$id = (int) $id;
		$_SESSION[G_SESSION]['filter_km']['idjeniskm'] = $id;
		redirect('home');
	}
}
