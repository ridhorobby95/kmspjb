<?php

class Menu extends Base_Controller {

	protected $model_p = 'menu_model';
	protected $detail_view = 'menu_detail';
	protected $list_view = 'menu_list';
	protected $list_limit = 1000;
	protected $add_title = 'Tambah Menu';
	protected $edit_title = 'Edit Menu';
	protected $detail_title = 'Detil Menu';
	protected $daftar_title = 'Daftar Menu';
	protected $posts = array('nama', 'namafile', 'iditem', 'idparent', 'itemaccess', 'urutan', 'isactive');
	protected $field_list = array(
								array('name'=>'nama','type'=>'C','label'=>'Nama','width'=>'250px','allow_filter'=>false),
								array('name'=>'namafile','type'=>'C','label'=>'Controller','allow_filter'=>false),
								array('name'=>'namaitem','type'=>'C','label'=>'Item','width'=>'200px','allow_filter'=>false),
								array('name'=>'itemaccess','type'=>'C','label'=>'Akses','width'=>'80px','allow_filter'=>false),
								);

	
    function __construct() {
		$_SESSION[G_SESSION]['IDMODUL'] = 2;
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
		$this->model->saveMenuSetting();
		$this->data['itemsControl'] = $this->_getItemList();
		$this->data['itemaccessControl'] = $this->_getItemAccessList();
		$this->data['modulControl'] = $this->_getModulList();	
		$this->data['urutanControl'] = $this->_getUrutanList();
		$this->data['parentsControl'] = $this->_getParentList($this->data['row']['IDMENU'], $this->data['row']['TINGKAT']);	
		// menu KM tidak dipakai
		$_SESSION[G_SESSION]['IDMODUL'] = 2;
	}
	
	function _preEdit() {
		$this->data['parentsControl'] = $this->_getParentList($this->data['row']['IDMENU'], $this->data['row']['TINGKAT']);		
	}
	
	function _initRecordAfter(&$record) {
		if ($_POST['idparent']) {
			$sql = "select tingkat from {$this->um}.menu where idmenu={$_POST['idparent']}";
			$level = dbGetOne($sql);
			if ($level) {
				$record['tingkat'] = $level+1;
			}
		}
		else {
			$record['tingkat'] = 1;
		}
	}
	
	function modul($idmodul) {
		$_SESSION[G_SESSION]['IDMODUL'] = (int) $idmodul;
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	function urut() {
		$dir = isset($this->uri_segments['dir'])?$this->uri_segments['dir']:null;
		$id1 = (int) $this->uri_segments['id'];
		
		$sql = "select * from {$this->um}.menu where idmenu=$id1";
		$menu = dbGetRow($sql);

		$urutan = (int) $menu['URUTAN'];
		$tingkat = $menu['TINGKAT'];
		$idparent = $menu['IDPARENT'];
		
		if ($dir === 'up') {
			$sql = "select idmenu from {$this->um}.menu where urutan<=$urutan and tingkat=$tingkat and idmenu not in ($id1) ";
			if ($idparent)
				$sql .= "and idparent=$idparent ";
			$sql .= "order by urutan desc, nama desc";
		}
		elseif ($dir === 'down') {
			$sql = "select idmenu from {$this->um}.menu where urutan>=$urutan and tingkat=$tingkat and idmenu not in ($id1) ";
			if ($idparent)
				$sql .= "and idparent=$idparent ";
			$sql .= "order by urutan, nama";
		}
		$id2 = (int) dbGetOne($sql);
		$sql = "select * from {$this->um}.menu where tingkat=$tingkat ";
		if ($idparent)
			$sql .= "and idparent=$idparent ";
		$sql .= "order by urutan, nama";
		
		$menus = dbGetRows($sql);
		$i=1;
		$isswap = false;
		foreach ($menus as $menu) {
			if ($menu['IDMENU'] == $id1 || $menu['IDMENU'] == $id2) {
				if ($dir === 'up' && $menu['IDMENU'] == $id2 && !$isswap) {
					$sql = "update {$this->um}.menu set urutan=$i where idmenu=$id1 ";
					$ret = dbQuery($sql);

					$i++;
					if ($i>count($menus))
						continue;

					$sql = "update {$this->um}.menu set urutan=$i where idmenu=$id2 ";
					$ret = dbQuery($sql);
					
					$isswap = true;
					
					continue;
				}
				elseif ($dir === 'down' && $menu['IDMENU'] == $id1 && !$isswap) {
					$sql = "update {$this->um}.menu set urutan=$i where idmenu=$id2 ";
					$ret = dbQuery($sql);
					$i++;
					if ($i>count($menus))
						continue;
					
					$sql = "update {$this->um}.menu set urutan=$i where idmenu=$id1 ";
					$ret = dbQuery($sql);
					$isswap = true;
					
					continue;
				}
			}
			else {
				$sql = "update {$this->um}.menu set urutan=$i where idmenu={$menu['IDMENU']} ";
				$ret = dbQuery($sql);
			}
			$i++;
		}
		redirect('menu');
	}
	
	function _getItemList() {
		$this->load->model('Item_model', 'Item');
		$this->Item->setFilter(" and idmodul = {$_SESSION[G_SESSION]['IDMODUL']} ");
		$rows = $this->Item->getList();
		if (!$rows)
			return false;
		
		$list = array();
		foreach ($rows as $row) {
			$list[$row['IDITEM']] = $row['NAMA'];
		}
		return $list;
	}

	function _getParentList($idmenu=null,$tingkat=null) {
		$rows = $this->model->GetMenuParent($idmenu, $tingkat);
		if (!$rows)
			return false;
		
		$list = array();
		foreach ($rows as $row) {
			$list[$row['IDMENU']] = $row;
		}
		//echo '<pre>';
		//var_dump($list);
		//die();
		$ret = array();
		$this->_generateMenuTree($ret, $list);
		
		return $ret;
	}
	
	function _generateMenuTree(&$ret, $array, $parent = 0, $tingkat = 0) {
		$has_children = false;
		foreach($array as $key => $value) {
			if ($value['IDPARENT'] == $parent) {                   
				if ($has_children === false) {
					$has_children = true;  
					$tingkat++;
				}
				$str_tingkat = str_repeat('...',$tingkat-1);
				$ret[$value['IDMENU']] = $str_tingkat . $value['NAMA'] . ' &nbsp; ';
				$this->_generateMenuTree($ret, $array, $key, $tingkat); 
			}
		}
	}		

	function _getItemAccessList() {
		$list = array();
		$list['R'] = 'Read';
		$list['C'] = 'Create';
		$list['U'] = 'Update';
		$list['D'] = 'Delete';
		return $list;
	}

	function _getUrutanList() {
		$list = array();
		for ($i=1;$i<=50;$i++)
			$list[$i] = $i;

		return $list;
	}

	private function _generateParents(&$ret, $array, $parent = 0, $tingkat = 0, $val='') {
		$has_children = false;
		foreach($array as $key => $value) {
			if ($value['IDPARENT'] == $parent) {                   
				if ($has_children == false) {
					$has_children = true;  
					$tingkat++;
				}
	
				$value['tingkat'] = $tingkat;
				$ret[] = $value;	
	
				$this->_generateParents($ret, $array, $key, $tingkat, $val); 
			}
		}
	}
	
	function _getModulList() {
		$this->load->model('Modul_model', 'Modul');
		$rows = $this->Modul->getList();
		$list = array();
		foreach ($rows as $row) {
			$list[$row['IDMODUL']] = $row['NAMA'];
		}
		return $list;
	}
	
}
