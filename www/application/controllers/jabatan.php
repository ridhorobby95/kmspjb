<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends Base_Controller {

	protected $model_p = 'jabatan_model';
	protected $detail_view = 'jabatan_detail';
	protected $list_view = 'jabatan_list';
	protected $list_limit = 50;
	protected $add_title = 'Tambah Jabatan';
	protected $edit_title = 'Edit Jabatan';
	protected $detail_title = 'Detil Jabatan';
	protected $daftar_title = 'Daftar Jabatan';
	protected $posts = array('nama', 'kodejabatan');
	protected $field_list = array(
					array('name'=>'nama','type'=>'C','label'=>'Nama'),
				);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
		if ($this->method != 'detail' && $this->method != 'lst' && $this->method != 'index')
			$this->toNoData('<b>Data tidak bisa diubah</b><br/><br/>Perubahan hanya dari Ellipse');
	}
	

}
