<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subyek extends Base_Controller {

	protected $model_p = 'subyek_model';
	protected $detail_view = 'subyek_detail';
	protected $list_view = 'subyek_list';
	protected $list_limit = 100;
	protected $add_title = 'Tambah Stream';
	protected $edit_title = 'Edit Subyek';
	protected $detail_title = 'Detil Stream';
	protected $daftar_title = 'Daftar Stream';
	protected $posts = array('nama');
	protected $field_list = array(
									array('name'=>'nama','type'=>'C','label'=>'Nama'),
								);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
	}
	
}
