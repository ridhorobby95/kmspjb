<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends Base_Controller {

	protected $model_p = 'unit_model';
	protected $detail_view = 'unit_detail';
	protected $list_view = 'unit_list';
	protected $list_limit = 100;
	protected $add_title = 'Tambah Unit';
	protected $edit_title = 'Edit Unit';
	protected $detail_title = 'Detil Unit';
	protected $daftar_title = 'Daftar Unit';
	protected $posts = array('nama', 'kodeunit', 'idparent', 'urutan', 'isactive');
	protected $field_list = array(
					array('name'=>'nama','type'=>'C','label'=>'Nama'),
					array('name'=>'kodeunit','type'=>'C','label'=>'Kode Unit','width'=>'150px'),
				);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
		
		/*if ($this->method != 'detail' && $this->method != 'lst' && $this->method != 'index')
			$this->toNoData('<b>Data tidak bisa diubah</b><br/><br/>Perubahan hanya dari Ellipse');*/
	}

}

