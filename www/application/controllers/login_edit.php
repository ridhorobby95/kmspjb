<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Base_Controller {

    private $max_login_trial = 5;
    
	function index() {
		if (xIsLoggedIn()) 
			redirect('home');
		
        if (!isset($_SESSION[G_SESSION]['num_login_failed']))
            $_SESSION[G_SESSION]['num_login_failed'] = 0;
            
        if ($_POST['act_login'] == 'login'){
            if ($_SESSION[G_SESSION]['num_login_failed'] < $this->max_login_trial) {
                
				$this->load->helper('date');
    			
				$this->load->model('user_model','User');
				
				$this->session->set_flashdata('username',  $_POST['usernamelogin']);
				
				if (!trim($_POST['usernamelogin']) || !trim($_POST['passwordlogin'])) {
					$this->session->set_flashdata('error_login', 'Username atau Password tidak boleh kosong');
					redirect('login');
				}
					
				$username = $_POST['usernamelogin'];
				$password = $_POST['passwordlogin'];

				$user = $this->User->getUserByUsername($username);
				if (!$user['ISACTIVE']) {
					$this->session->set_flashdata('error_login', 'Pengguna dinonaktifkan dari sistem.');
					redirect('login');
				}
				
				if (!$user['ISEXTRAUSER']) {
					$this->_loginLDAP($username, $password);
				}
				else {
					$passwordDB = md5($password);
					if ($passwordDB !== $user['PASSWORD']) {
						$_SESSION[G_SESSION]['num_login_failed']++;
						$this->session->set_flashdata('error_login', 'Login gagal');
						redirect('login');
					}
				}

				$url_redirect = $_SESSION[G_SESSION]['url_redirect'];

                session_regenerate_id(true);
                
                $_SESSION[G_SESSION]['url_redirect'] = $url_redirect;

                $_SESSION[G_SESSION]['iduser'] = $user['IDUSER'];
                $_SESSION[G_SESSION]['nid'] = $user['NID'];
                $_SESSION[G_SESSION]['nama'] = $user['NAMA'];
                $_SESSION[G_SESSION]['email'] = $user['EMAIL'];
				$_SESSION[G_SESSION]['kodeunit'] = $user['KODEUNIT'];
				$_SESSION[G_SESSION]['remote_addr'] = $_SERVER['REMOTE_ADDR'];
				$_SESSION[G_SESSION]['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
				$_SESSION[G_SESSION]['lastlogintime'] = $user['LASTLOGINTIME'];
				$_SESSION[G_SESSION]['lastloginip'] = $user['LASTLOGINIP'];
				$opsi = json_decode($user['OPSI'], true);
				$_SESSION[G_SESSION]['opsi_home'] = $opsi['opsi_home']; 

				if (!$_SESSION[G_SESSION]['opsi_home'])
					$_SESSION[G_SESSION]['opsi_home'] = 'r';
				
				$sql = "update {$this->um}.users set lastlogintime=sysdate, lastloginip='{$_SERVER['REMOTE_ADDR']}' where iduser={$user['IDUSER']}";
				dbQuery($sql);
				
				xUserLog('login');
				$this->_rekapUserLog();
				
				$ret = $this->_setOtorisasi();
				
				if ($ret)
					redirect($ret);
				else {
					$this->session->set_flashdata('error_login', 'Pengguna dinonaktifkan dari sistem.');
					redirect('login');
				}
			}
        }

        $this->data['num_login_exceeded'] = 0;
        
        if ($_SESSION[G_SESSION]['num_login_failed'] >= $this->max_login_trial) {
            $data['num_login_exceeded'] = 1;
        }

		$this->renderViewSimple('login');
	}
	
    function _setOtorisasi() {
		$ret = false;
        
        $iduser = $_SESSION[G_SESSION]['iduser'];
        $sql = "select * from {$this->um}.userrole where iduser=$iduser";
        $roles = dbGetRows($sql);

        if (count($roles) == 1) {            
            $_SESSION[G_SESSION]['idrole'] = $roles[0]['IDROLE'];
            $_SESSION[G_SESSION]['num_role'] = 1;
            
			$sql = "select nama from {$this->um}.role where idrole={$roles[0]['IDROLE']}";
            $_SESSION[G_SESSION]['namarole'] = dbGetOne($sql);
			
            # jika hanya memiliki 1 role maka langsung ke base_url()
            if ($_SESSION[G_SESSION]['url_redirect']) {
                $ret = $_SESSION[G_SESSION]['url_redirect'];
                unset($_SESSION[G_SESSION]['url_redirect']);
            }
            else
                $ret = site_url('home');
        }
        else {
            $_SESSION[G_SESSION]['num_role'] = count($roles);
            $ret = site_url('akses');
        }

        return $ret;        
    }
	
	function _loginLDAP($username, $password) {
		$ldap_server = 'ad.pjbservices.com';
		//$ldap_server = '172.16.30.106';
		$ldaprdn = 'ho' . "\\". $username;
			$ad = ldap_connect($ldap_server) ;

		if (!$ad) {
			$this->session->set_flashdata('error_login', 'Login gagal');
			redirect('login');
		}

		ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
			
		$bound = ldap_bind($ad, $ldaprdn , $password);
		if (!$bound) {
			$this->session->set_flashdata('error_login', 'Login gagal');
			redirect('login');

			$filter="(sAMAccountName=$username)";
            $result = ldap_search($ad,"dc=ho,dc=pjbservices,dc=com",$filter);
            ldap_sort($ad,$result,"sn");
            $info = ldap_get_entries($ad, $result);
			if (!$info) {
				$this->session->set_flashdata('error_login', 'Login gagal');
				redirect('login');
			}
			
		}
	}
	
	function _rekapUserLog() {
		$d = date('Y-m-d');
		$m = date('Y-m');
		$y = date('Y');
		$kodeunit = $_SESSION[G_SESSION]['kodeunit'];
		
		if (!$kodeunit)
			$kodeunit = 'TAMU';
		
		$sql = "select 1 from {$this->um}.rekapuserlog where waktu='$d' and kodeunit='$kodeunit'";
		if (dbGetOne($sql)) {
			$sql = "update {$this->um}.rekapuserlog set jumlah=jumlah+1 where waktu='$d' and kodeunit='$kodeunit'";
			dbQuery($sql);
		}
		else {
			$sql = "insert into {$this->um}.rekapuserlog (waktu, kodeunit, jumlah) values ('$d', '$kodeunit', 1)";
			dbQuery($sql);
		}

		$sql = "select 1 from {$this->um}.rekapuserlog where waktu='$m' and kodeunit='$kodeunit'";
		if (dbGetOne($sql)) {
			$sql = "update {$this->um}.rekapuserlog set jumlah=jumlah+1 where waktu='$m' and kodeunit='$kodeunit'";
			dbQuery($sql);
		}
		else {
			$sql = "insert into {$this->um}.rekapuserlog (waktu, kodeunit, jumlah) values ('$m', '$kodeunit', 1)";
			dbQuery($sql);
		}
		
		$sql = "select 1 from {$this->um}.rekapuserlog where waktu='$y' and kodeunit='$kodeunit'";
		if (dbGetOne($sql)) {
			$sql = "update {$this->um}.rekapuserlog set jumlah=jumlah+1 where waktu='$y' and kodeunit='$kodeunit'";
			dbQuery($sql);
		}
		else {
			$sql = "insert into {$this->um}.rekapuserlog (waktu, kodeunit, jumlah) values ('$y', '$kodeunit', 1)";
			dbQuery($sql);
		}

	}

}