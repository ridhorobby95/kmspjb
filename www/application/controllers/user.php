<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller {

	protected $model_p = 'user_model';
	protected $detail_view = 'user_detail';
	protected $list_view = 'user_list';
	protected $list_limit = 100;
	protected $add_title = 'Tambah User';
	protected $edit_title = 'Edit User';
	protected $detail_title = 'Detil User';
	protected $daftar_title = 'Daftar User';
	protected $posts = array('nama', 'nid', 'email', 'telp', 'isactive', 'isextrauser', 'password','kodeunit');
	protected $field_list = array(
					array('name'=>'nama','type'=>'C','label'=>'Nama','width'=>'150px'),
					array('name'=>'nid','type'=>'C','label'=>'NID','width'=>'150px'),
					array('name'=>'email','type'=>'C','label'=>'Email','width'=>'150px'),
					array('name'=>'isactive','type'=>'C','label'=>'Status','width'=>'150px')
				);
	
    function __construct() {
		parent::__construct();
		
		$this->_initModel();
		$this->responsePost();
		$this->setModelFilter();
		$this->data['role_control'] = $this->_getRoleList();
		if ($this->method == 'delete_mode')
			redirect('user');
	}

	function _preLst() {
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->model->addFilter(" and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
	}
	
	
	function _preDetail() {
		$this->data['roles'] = $this->model->getRoles($this->data['row']['IDUSER']);
		$this->load->model('unit_model', 'UNIT');
		
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$all_unit = $this->UNIT->getList();
		
		foreach ($all_unit as $u)
			$a_unit[$u['KODEUNIT']] = $u['NAMA'];
		
		$this->data['a_unit'] = $a_unit;

	}
	
	protected function _preEdit() {
		$this->load->model('unit_model', 'UNIT');
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$all_unit = $this->UNIT->getList();
		
		foreach ($all_unit as $u)
			$a_unit[$u['KODEUNIT']] = $u['NAMA'];
		
		$this->data['a_unit'] = $a_unit;
		
	}
	protected function _preAdd() {
		$this->load->model('unit_model', 'UNIT');
		
		if ($_SESSION[G_SESSION]['idrole'] == $this->config->item('id_admin_unit')) {
			$this->UNIT->setFilter("and kodeunit = '{$_SESSION[G_SESSION]['kodeunit']}'");
		}
		$all_unit = $this->UNIT->getList();
		
		foreach ($all_unit as $u)
			$a_unit[$u['KODEUNIT']] = $u['NAMA'];
		
		$this->data['a_unit'] = $a_unit;
	}	
	/* override client minta tidak hapus, namun hanya me Non-Aktifkan User
	 * reason : Permintaan client
	 * last update : 22 januari 2018 
	 * updater : SVM programmer 
	function delete($id) {
		if (!$this->data['c_delete'])
			$this->toNoData();
		
		$record['ISACTIVE'] = null;
		$this->model->update($record, $id);	
		$this->_postEdit();
		$this->_setNotification(lang('data_updated').'<br> Proses ini tidak menghapus User namun hanya membuat user Non-Aktif');	
		redirect("$this->ctl/detail/$id");					
		
	}	*/
	
	protected function _initRecordAfter(&$record) {
		if (!$_POST['isactive']) {
			$record['isactive'] = 0;
		}
		if (!$_POST['isextrauser']) {
			$record['isextrauser'] = 0;
			$record['password'] = '';
		}
		else {
			if ($this->method == 'add') {
				$record['password'] = md5($_POST['password']);
			}
			else {
				if ($_POST['password'])
					$record['password'] = md5($_POST['password']);
				else
					unset($record['password']);
			}
		}
	}

	function _getRoleList($val) {
		$sql = "select nama as label, idrole as val from {$this->um}.role order by nama";
		$rows = dbGetRows($sql);
		
		return $this->genList('idrole', $rows, $val, 'class="form-control" style="width:200px"', "-- Role --");
	}
	
	function addrole() {
		$iduser = $_POST['iduser'];
		$idrole = $_POST['idrole'];
		
		$sql = "insert into {$this->um}.userrole (iduser, idrole) values ($iduser, $idrole)";
		dbQuery($sql);
		exit;
	}
	
	function delrole() {
		list($iduser, $idrole, $idjabatan) = explode('-', $_POST['val']);
		$sql = "delete from {$this->um}.userrole where iduser=$iduser and idrole=$idrole";
		dbQuery($sql);
		exit;
	}
	function active($id){
		$data = $this->model->getRow($id);
		$record = array();
		$record['ISACTIVE'] = '1';
		
		if ($data['ISACTIVE'] == '1')
		$record['ISACTIVE'] = null;	
		
		$this->model->update($record, $id);
		
		$this->_setNotification(lang('data_updated'));	
		
		redirect("$this->ctl/lst");	
	}
}
