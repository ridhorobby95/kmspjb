<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploader extends CI_Controller 
{
	protected $tmp_path		= "";
	protected $upload_path	= "";
	protected $request_time = 0;

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('file');
		$this->load->library('zip');

		$this->load->model('filemanager_model', 'filemanager');

		//unset($_SESSION[G_SESSION]['UPLOADER']);

		$this->tmp_path 	= $this->config->item('upload_temp');
		$this->scan_path 	= $this->config->item('scan_temp');
		$this->upload_path	= $this->config->item('upload_path');
	}

	public function index()
	{
		#jika ada upload file
		if ( $_FILES['file'] )
		{
			#initiate path upload
			$path	= $this->tmp_path;

			$file 	= $_FILES['file'];

			#get extension file
			$a_file = explode('.', $file['name']);

			$ext 	= trim($a_file[count($a_file) - 1]);

			#cek tipe file rar atau zip
			if ( $ext != "zip" )
			{
				echo '{"error_code" : "100", "error_msg" : "File tidak sesuai, tipe file yang diijinkan .zip"}';

				return;
			}
			else
			{
				error_reporting(0);

				#create directory upload
				if ( ! is_dir( $path ) and mkdir( $path ) )
				{
					#change modified
					chmod( $path, 0775 );	
				}

				if ( move_uploaded_file( $file['tmp_name'], $path . $file['name'] ) )
				{
					#extract file
					list($p_posterr, $p_postmsg, $dest_dir) = $this->zip->unzip($path . $file['name'], false, true, true);					

					#reset session
					unset($_SESSION[G_SESSION]['UPLOADER']);

					#save extract file
					$_SESSION[G_SESSION]['UPLOADER']['ZIP'] = $path . $file['name'];
					$_SESSION[G_SESSION]['UPLOADER']['EXT'] = $dest_dir;

					#preprocessing inisialisasi directory
					$count = $this->_preprocessing($dest_dir);

					echo '{"error_code" : "0", "progress" : "1", "log" : "Inisiasi Data...<br>", "count" : '. $count .'}';
				}
			}

			return;
		}

		if( $_POST['process'] )
		{
			$this->request_time = $_POST['request_time'];

			$this->_process();

			return;
		}

		if( $_POST['scan'] )
		{
			if( $_POST['scaninit'] )
			{
				#preprocessing inisialisasi directory
				$count = $this->_preprocessing($this->scan_path);	

				if( $count )
					echo '{"error_code" : "0", "progress" : "1", "log" : "Inisiasi Data...<br>", "count" : '. $count .'}';
				else
					echo '{"error_code" : "100", "error_msg" : "Folder kosong, mohon dipastikan file sudah berada pada folder '. $this->scan_path .'"}';

				return;
			}

			$this->request_time = $_POST['request_time'];

			$this->_process();

			return;
		}

		$this->data['tmp_path'] 	= $this->tmp_path;
		$this->data['upload_path']	= $this->upload_path;
		$this->data['scan_path']	= $this->scan_path;

		$this->load->view('default/uploader_list', $this->data);
	}

	private function _preprocessing($dir)
	{
		$page_handler = opendir($dir);

		while( ($sub = readdir($page_handler)) !== FALSE )
		{
			if( $sub != "." && $sub != ".." )
			{
				if( is_dir($dir . $sub) )
				{
					#recursive inisialisasi direktory
					$this->_preprocessing(  $dir . $sub . '/' );
				}
				elseif( is_file($dir . $sub) )
				{
					$_SESSION[G_SESSION]['UPLOADER']['FILE'][] = $dir . $sub;
				}
			}
		}

		closedir($page_handler);

		return count($_SESSION[G_SESSION]['UPLOADER']['FILE']);
	}

	private function _process()
	{
		$i = isset($_SESSION[G_SESSION]['UPLOADER']['PRC']) ? $_SESSION[G_SESSION]['UPLOADER']['PRC'] : 0;

		$c = count($_SESSION[G_SESSION]['UPLOADER']['FILE']);

		for ( $j = $i; $j < $c; $j++ )
		{
			#main process
			$a_status[$j] = $this->_grabFile($j);

			#break response
			if( (time() - $this->request_time) > 1 )
			{
				$response = array(
					"error_code" 	=> 0, 
					"progress" 		=> floor(($j / $c) * 100), 
					"request_time" 	=> $this->request_time, 
					"response_time" => time(),
					"data"			=> $c,
					"iterasi" 		=> $i,
					"status"		=> $a_status
				);

				echo json_encode($response);

				return;
			}
		}

		if ( floor(($j / $c) * 100) == 100 )
		{
			#output response
			$response = array(
				"error_code" 	=> 0, 
				"progress" 		=> floor(($j / $c) * 100), 
				"request_time" 	=> $this->request_time, 
				"response_time" => time(),
				"data"			=> $c,
				"iterasi" 		=> $i,
				"log" 			=> "Selesai...<br>",
				"status"		=> $a_status
			);

			echo json_encode($response);

			#remove folder
			$this->_postprocessing($this->tmp_path);

			#remove all session
			unset($_SESSION[G_SESSION]['UPLOADER']);
		}

		return;
	}

	private function _postprocessing($dir)
	{
		#remove zip file
		if( file_exists($_SESSION[G_SESSION]['UPLOADER']['ZIP']) )
			unlink( $_SESSION[G_SESSION]['UPLOADER']['ZIP'] );

		#remove folder, sub folder and file on folder
		$page_handler = opendir($dir);

		while( ($sub = readdir($page_handler)) !== FALSE )
		{
			if( $sub != "." && $sub != ".." )
			{
				if( is_dir($dir . $sub) )
				{
					#recursive postprocessing directory
					$this->_postprocessing(  $dir . $sub . '/' );

					rmdir( $dir . $sub . '/' );
				}
				elseif( is_file($dir . $sub) )
				{
					if( file_exists($dir . $sub) )
						unlink( $dir . $sub );
				}
			}
		}

		closedir($page_handler);
	}

	private function _grabFile($index)
	{
		error_reporting(0);

		$a_status = $record = $content = array();

		$a_permit_image = array('jpeg', 'jpg', 'png');
		$a_permit_file 	= array('pdf');

		$file 			= $_SESSION[G_SESSION]['UPLOADER']['FILE'][$index];
		
		$pathinfo 		= pathinfo($file);
		$parentdir		= end(explode('/', $pathinfo['dirname']));

		if( is_numeric($parentdir) )
		{
			#image
			if( in_array($pathinfo['extension'], $a_permit_image) )
			{
				#record
				$record['IDLAMPIRAN']	= $parentdir;
				$record['FILENAME']		= $pathinfo['basename'];
				$record['JENIS']		= 1;	

				#content
				$content['ISIFILE']		= file_get_contents( $file );
				$content['THUMBNAIL']	= file_get_contents( $file );

				if ( ! $this->filemanager->fileExists($record['IDLAMPIRAN'], $record['FILENAME']) ) 
				{
					$a_status = $this->filemanager->insert($record, $content);
				}
			}
			#insert sinopsis
			elseif( strtolower($pathinfo['filename']) == 'sinopsis' )
			{
				#record
				$record['IDLAMPIRAN']	= $parentdir;
				$record['FILENAME']		= $pathinfo['basename'];
				$record['JENIS']		= 2;				

				#insert into LP
				if ( ! $this->filemanager->fileExists($record['IDLAMPIRAN'], $record['FILENAME']) ) 
				{
					#content
					$content['ISIFILE']		= file_get_contents( $file );

					$a_status = $this->filemanager->insert($record, $content);
				}

				#update pustaka
				if( $a_status['tipe'] == 'info' )
				{
					$content = array();

					#create directory
					if( !is_dir($this->upload_path . 'sinopsis/' . $parentdir . '/') )
						mkdir($this->upload_path . 'sinopsis/' . $parentdir . '/', 0775, true);

					#copy file ke directory upload
					$newpath = $this->upload_path . 'sinopsis/' . $parentdir . '/' . $record['FILENAME'];
					copy( $file, $newpath );

					#record
					$data['SINOPSIS_UPLOAD'] 	= $newpath;
					#content
					$content['SINOPSIS']		= file_get_contents( $file );

					$a_status = $this->filemanager->update($data, $content, $record['IDLAMPIRAN'], $pathinfo['basename']);
				}
			}
			#insert file
			else
			{
				#record
				$record['IDLAMPIRAN']	= $parentdir;
				$record['FILENAME']		= $pathinfo['basename'];
				$record['JENIS']		= 3;	

				#content
				$content['ISIFILE']		= file_get_contents( $file );

				if ( ! $this->filemanager->fileExists($record['IDLAMPIRAN'], $record['FILENAME']) ) 
				{
					#create directory
					if( !is_dir($this->upload_path . 'file/' . $parentdir . '/') )
						mkdir($this->upload_path . 'file/' . $parentdir . '/', 0775, true);

					#copy file ke directory upload
					$newpath = $this->upload_path . 'file/' . $parentdir . '/' . $record['FILENAME'];
					copy( $file, $newpath );

					$a_status = $this->filemanager->insert($record, $content);
				}	
			}
		}

		++$_SESSION[G_SESSION]['UPLOADER']['PRC'];

		return $a_status;	
	}
}
?>