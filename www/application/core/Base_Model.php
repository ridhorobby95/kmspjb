<?php

class Base_Model extends CI_Model
{
    protected $table;
    protected $id;
	protected $sort;
	protected $filter;
	protected $um;
	protected $km;
	protected $lp;
	protected $listquery;

    function __construct() {
		$this->um = $this->config->item('UM');
		$this->km = $this->config->item('KM');
		$this->lp = $this->config->item('LP');

		$this->load->database();
	
        parent::__construct();
    }

    function getList($limit=0, $offset=0) {
        $sql = "select * from $this->table where 1=1 ";
		$sql .= $this->filter;
		$sql .= " order by $this->sort ";
		
		$this->listquery = $sql;
		
        return dbGetRows($sql, $limit, $offset);
    }
	
	function getCount() {
        $sql = "select count(*) from ($this->listquery)";

        return dbGetOne($sql);
	}

    function getRow($id) {
		if (!is_array($id)) {
			$id = $this->db->escape($id);
			
			$sql = "select * from $this->table where $this->id=$id ";
	
			return dbGetRow($sql);
		}

		$sql = "select * from $this->table where 1=1";
		foreach ($id as $k=>$v) {
			$v = $this->db->escape($v);
			$sql .= " and {$k}=$v";
		}
		
        $row = dbGetRow($sql);
		
		$sep = $this->config->item('id_separator');
		$pks = implode($sep, $this->getID());
		$pk_str = '';
		foreach ($this->getID() as $pk) {
			if ($pk_str)
				$pk_str .= $sep;
			$pk_str .= "{$pk}:{$row[$pk]}";
		}
		$row[$pks] = $pk_str;
		
		return $row;
    }


    function getRows($data) {
		$sql = "select * from $this->table where 1=1";
		foreach ($data as $k=>$v) {
			$v = $this->db->escape($v);
			$sql .= " and {$k}=$v";
		}
		if ($this->sort) {
			$sql .= " order by $this->sort";
		}
		
        $row = dbGetRows($sql);
		
		// $sep = $this->config->item('id_separator');
		// $pks = implode($sep, $this->getID());
		// $pk_str = '';
		// foreach ($this->getID() as $pk) {
		// 	if ($pk_str)
		// 		$pk_str .= $sep;
		// 	$pk_str .= "{$pk}:{$row[$pk]}";
		// }
		// $row[$pks] = $pk_str;
		
		return $row;
    }

    function add($fields) {
		$ret = dbInsert($this->table, $fields);
		if ($ret) {
			// cek sequence
			$lid = strtolower($this->id);
			$uid = strtoupper($this->id);
			
			if(empty($fields[$lid]) and empty($fields[$uid])) {
				$sql = "select max({$this->id}) from $this->table ";
				return dbGetOne($sql);
			}
			else if(!empty($fields[$lid]))
				return $fields[$lid];
			else if(!empty($fields[$uid]))
				return $fields[$uid];
			
			return true;
		}
		return false;
	}

    function add2($fields) {
		$ret = dbInsert($this->table, $fields);
		if ($ret) {
			$arr = array();
			foreach ($this->id as $id) {
				$arr[$id] = $fields[$id];
			}
			return $arr;
		}
		return false;
	}
	
    function update($fields, $id) {
		$id = $this->db->escape($id);
		
		$where = "$this->id = $id";
		return dbUpdate($this->table, $fields, $where);
    }

    function update2($fields, $ids) {
		$where = '1=1';

		foreach ($ids as $k=>$v) {
			$v = $this->db->escape($v);
			$where .= " and {$k}=$v";
		}
		
		return dbUpdate($this->table, $fields, $where);
    }
	
    function delete($id) {
		$id = $this->db->escape($id);
		
		return dbDelete($this->table, "where $this->id=$id");
    }

    function delete2($ids) {
		$where = '1=1';

		foreach ($ids as $k=>$v) {
			$v = $this->db->escape($v);
			$where .= " and {$k}=$v";
		}
		
		return dbDelete($this->table, "where $where");
    }
	
	function getID($arr=false) {
		if ($arr)
		return $this->ArrId;
		else
		return $this->id;
	}
	
	function setFilter($filter) {
		if (!empty($filter))
			$this->filter = $filter;
	}

	function addFilter($filter) {
		$filter = trim($filter);
		
		if(!empty($filter)) {
			if(empty($this->filter)) {
				/*if(substr($filter,0,6) != 'where ')
					$filter = ' where '.$filter;*/
				
				$this->filter = $filter;
			}
			else {
				if(substr($filter,0,4) != 'and ')
					$filter = ' and '.$filter;
				
				$this->filter .= $filter;
			}
		}
	}

	function getFilter() {
		return	$this->filter;
	}

	function setSort($sort) {
		if (!empty($sort))
			$this->sort = $sort;
	}

	function getSort() {
		return $this->sort;
	}
	
	// tambahan
	
	function getAlias($col) {
		$array = $this->getListAlias();
		$alias = $array[$col];
		
		if(empty($alias))
			$alias = $col;
		
		return $alias;
	}
	
	function getListAlias() {
		return array();
	}
	
	function getListColumn() {
		/* $table = strtoupper($this->db->escape($this->table));
		
		$sql = "select column_name from user_tab_columns
				where table_name = $table"; */
		
		$table = strtoupper($this->table);
		
		$sql = $this->db->_list_columns($table);
		$rows = dbGetRows($sql);
		
		$data = array();
		foreach($rows as $row)
			$data[$row['COLUMN_NAME']] = false;
		
		return $data;
	}
	
	// id (COL1:val1___COL2:val2) dari argumen val, salah satu kosong, kosong
	function getDataID() {
		return $this->getDataIDExt($this->id,func_get_args());
	}
	
	// id (COL1:val1___COL2:val2) dari array val, salah satu kosong, kosong
	function getDataIDExt($id,$param) {
		$sep = $this->config->item('id_separator');
		
		$data = array();
		$cols = explode($sep,$id);
		foreach($cols as $i => $col) {
			if(strval($param[$i]) == '')
				return '';
			
			$data[] = $col.':'.$param[$i];
		}
		
		return implode($sep,$data);
	}
	
	// id (COL1:val1___COL2:val2) menjadi array index
	function getDataArrID($id,$case=false) {
		$cols = explode($this->config->item('id_separator'),$id);
		
		$data = array();
		foreach($cols as $col) {
			list($colname,$coldata) = explode(':',$col);
			if($case == -1)
				$colname = strtolower($colname);
			else if($case == 1)
				$colname = strtoupper($colname);
			
			$data[$colname] = $coldata;
		}
		
		return $data;
	}
	
	// id url val1/val2/val3 dari parameter
	function setDataID() {
		$sep = $this->config->item('id_separator');
		$args = func_get_args();
		
		$data = array();
		
		// jika array dengan indeks kolom
		if(!empty($this->id)) {
			$cols = explode($sep,$this->id);
			
			if(is_array($args[0])) {
				$uargs = array();
				foreach($args[0] as $colname => $coldata)
					$uargs[strtoupper($colname)] = $coldata;
				
				foreach($cols as $i => $col) {
					list($colname,$coldata) = explode(':',$col);
					
					$data[] = $uargs[$colname];
				}
			}
			else if(!empty($this->id)) {
				foreach($cols as $i => $col)
					$data[] = $args[$i];
			}
		}
		else
			$data = $args;
		
		return implode('/',$data);
	}
	
	// mencari val dari suatu COL (dari id)
	function getDataColID($id,$ccol) {
		$sep = $this->config->item('id_separator');
		$id = explode($sep,$id);
		
		$cols = explode($sep,$this->id);
		foreach($cols as $i => $col) {
			if($col == $ccol) {
				list(,$id) = explode(':',$id[$i]);
				return $id;
			}
		}
		
		// tidak ditemukan
		return false;
	}
	
	function getConditionID($id,$alias='') {
		$sep = $this->config->item('id_separator');
		
		$cond = array();
		$arrid = explode($sep,$id);
		foreach($arrid as $sid) {
			list($col,$data) = explode(':',$sid);
			$cond[] = (empty($alias) ? '' : $alias.'.').$col.' = '.$this->db->escape($data);
		}
		
		$cond = implode(' and ',$cond);
		
		return $cond;
	}
	
	function getConditionArray($arr,$alias='') {
		$cond = array();
		foreach($arr as $col => $data)
			$cond[] = (empty($alias) ? '' : $alias.'.').$col.' = '.$this->db->escape($data);
		
		$cond = implode(' and ',$cond);
		
		return $cond;
	}	
}
