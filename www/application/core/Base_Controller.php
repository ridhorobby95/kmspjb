<?php
session_start();

class Base_Controller extends CI_Controller
{
    public $data = array();
	protected $uri_segments = array();
	protected $ctl;
	protected $method;
	protected $model;
	
	protected $cache_time = 86400; // 3600 * 60 * 24 = 1 day

	protected $model_p;
	protected $detail_view;
	protected $list_view;
	protected $list_limit;

	protected $view_items = array();
	
	public $auth = array();
	protected $menu_unserialize = array();
	
	# debug sql hooks
	public $list_sql = array();

	# ora messages
	public $ora_messages = array();

	public $um;
	public $km;
	public $lp;

    function __construct() {
        parent::__construct();

		$this->um = $this->config->item('UM');
		$this->km = $this->config->item('KM');
		$this->lp = $this->config->item('LP');

		if ($_SESSION[G_SESSION]['opsi_home'] == 'r')
			$_SESSION[G_SESSION]['is_condensed'] = 1;
		else {
			$_SESSION[G_SESSION]['is_condensed'] = 0;
			$_SESSION[G_SESSION]['opsi_home'] = 's';
		}
		
		$this->load->helper('language');
		$this->lang->load('general', 'id');
		$this->data['notification'] = $this->session->flashdata('notification');
		
		$this->data['page_title'] = lang('page_title');	// ada di lang nya bro di general

		$this->uri_segments = $this->uri->uri_to_assoc(3);

		$this->ctl = $this->router->fetch_class();
		$this->data['ctl'] = $this->ctl;
		$this->method = $this->router->fetch_method();

		$this->data['method'] = $this->method;
		
		$free_classes = array('login','logout','ajax','cron','publ1c','error', 'akses', 'profile');
		if (!xIsLoggedIn()) {
			$free_classes[] = 'home';
		}
		
		if (in_array($this->router->class, $free_classes)) {
			$this->data['c_create'] = true;
			$this->data['c_read'] = true;
			$this->data['c_update'] = true;
			$this->data['c_delete'] = true;
			
			$this->auth = array('c_read','c_create','c_update','c_delete');
			
			$idrole = $_SESSION[G_SESSION]['idrole'];
			$iduser = $_SESSION[G_SESSION]['iduser'];
			
			if($idrole)
				$this->_setMenu();
			
		
			return true;
		}
		if($_SESSION[G_SESSION]['iduser']){
			$this->load->model("Notification_model");
            $this->data['jumlahNotif'] = $this->Notification_model->show_sql(false)->getCountNotifications();
        }
		if (!xIsLoggedIn()) {
			//$_SESSION[G_SESSION]['url_redirect'] = current_url();
			redirect('home');
		}
		
	
		$this->load->helper('html');
		
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$iduser = $_SESSION[G_SESSION]['iduser'];
		if($idrole){
		    $this->_setMenu();
		}else{
		    redirect("akses");
		}
		
		
		$this->checkRoleAuth();
		
		$this->data['with_form'] = true;
		
    }
	
	function index() {
		redirect("$this->ctl/lst");
	}
	
	function lst($offset=0) {
		if (!$this->data['c_read'])
			$this->toNoData();
		
		$this->data['page_title'] = $this->daftar_title;
		
		$this->_preLst();

		$this->data['field_list'] = $this->field_list;

		$this->data['rows'] = $this->model->getList($this->list_limit, $offset);
		$this->data['num_start'] = ($offset)+1;
		$this->data['num_end'] = ($offset)+count($this->data['rows']);
		$this->data['num_all'] = $this->model->getCount();
		
		$_SESSION[G_SESSION][$this->ctl.'_offset'] = $offset;

		$this->renderPagination("$this->ctl/lst", $this->data['num_all'], $offset, $this->list_limit);
		$this->renderView($this->list_view);		
	}
	
	function add($view=false) {
		$this->data['page_title'] = $this->add_title;
		$this->data['edited'] = true;

		if (!$this->data['c_create'])
			$this->toNoData();
		
		$this->_preAdd();

		if ($_POST['act'] == 'save') {
			$record = $this->_initRecord();
			$id = $this->model->add($record);
			if ($id) {
				$this->_postAdd();
				$this->session->set_flashdata('suc_msg', lang('data_added'));
				redirect("$this->ctl/detail/$id");					
			}
			else {
				$this->data['row'] = $record;
				$this->data['err_msg'] = lang('data_added_fail');
			}
			
		}
		if ($view)
			$this->detail_view = $view;
		$this->renderView($this->detail_view);
	}
	
	function edit($id, $view=false) {
		if (!$this->data['c_update'])
			$this->toNoData();

		$this->data['page_title'] = $this->edit_title;
		
		$this->data['row'] = $this->model->getRow($id);
		$this->_preEdit();

		if (!$this->data['row'])
			$this->toNoData();
		
		if ($_POST['act'] == 'save') {
			$record = $this->_initRecord();
			if ($this->model->update($record, $id)) {
				$this->_postEdit();
				$this->_setNotification(lang('data_updated'));	
				redirect("$this->ctl/edit/$id");					
			}
			else {
				$this->data['row'] = $record;
				$this->_setNotification(lang('data_updated_fail'), 'E');	
				redirect("$this->ctl/detail/$id");
			}
		}
		$this->data['edited'] = true;
		if ($view)
			$this->detail_view = $view;
		// echo "<pre>";print_r($this->data);die();
		$this->renderView($this->detail_view);
	}
	
	function delete($id) {
		if (!$this->data['c_delete'])
			$this->toNoData();
		
		$this->_preDelete();

		if ($this->model->delete($id)) {
			$this->_postDelete();
			$this->_setNotification(lang('data_deleted'));	
			redirect("$this->ctl/lst");
		}
		else {
			$this->_setNotification(lang('data_deleted_fail'), 'E');	
			redirect("$this->ctl/detail/$id");
		}
	}

	function detail($id, $id_notif=null , $view=false) {
		$this->load->model('Notification_model');
        if($id_notif != null || $id_notif != 0){
            $id_user_notif = $this->Notification_model->show_sql(false)->column('iduser')->filter('where idnotifications='.$id_notif)->getOne();
        }
        if(($id_notif != null || $id_notif != 0) && $_SESSION[G_SESSION]['iduser'] == $id_user_notif){
            $data['status'] = Notification_model::BACA;
            dbUpdate("NOTIFICATIONS",$data, "idnotifications=$id_notif");
            $this->load->model('Notification_model');
            $jumlahNotif = $this->Notification_model->show_sql(false)->getCountNotifications();
            unset($this->data['jumlahNotif']);
            $this->data['jumlahNotif'] = $jumlahNotif;
        }
		if (!$this->data['c_read'])
			$this->toNoData();
		
		$this->data['page_title'] = $this->detail_title;
				
		$this->data['row'] = $this->model->getRow($id);

		$this->_preDetail();
		
		if (!$this->data['row'])
			$this->toNoData();
			
		$this->data['edited'] = false;
        
		if ($view)
			$this->detail_view = $view;
		
	// echo "<pre>";print_r($this->data);die();
		$this->renderView($this->detail_view);		
	}
	
	protected function _preAdd() {
		#override di controllers
	}

	protected function _preEdit() {
		#override di controllers
	}

	protected function _preDelete() {
		#override di controllers
	}

	protected function _preDetail() {
		#override di controllers
	}
	
	protected function _preLst() {
		#override di controllers
	}
	
	protected function _postAdd() {
		#override di controllers
	}

	protected function _postEdit() {
		#override di controllers
	}

	protected function _postDelete() {
		#override di controllers
	}
	
	protected function _setNotification($message, $jenis='S') {
		$class = 'alert ';
		if ($jenis == 'S')
			$class .= 'alert-success';
		elseif ($jenis == 'E')
			$class .= 'alert-danger';
		$alert = "<div class=\"$class\">$message</div>";
		$this->session->set_flashdata('notification', $alert);
	}
	
	protected function _initRecord() {
		$record = array();
		foreach ($this->posts as $post)
			if (trim($_POST[$post]))
				$record[$post] = $this->input->post($post);
		
		$this->_initRecordAfter($record);

		return $record;
	}
	
	protected function _initRecordAfter($record=false) {
		#override di controllers
	}

	protected function _initModel() {
        $this->load->model($this->model_p);
		$model = $this->model_p;
		$this->model = $this->$model;
		
		$this->data['id'] = $this->model->getID();		
	}
	
	private function _setMenu() {
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$iduser = $_SESSION[G_SESSION]['iduser'];
		
		if ($this->config->item('use_apc')) {
			$key = 'menu_'.$idrole;
			$apc_val = apc_fetch($key);
			if ($apc_val)
				$this->data['menus'] = $apc_val;
		}
		
		$this->load->model('Um_model', 'UM');
		
		$this->menu_unserialize = unserialize($this->UM->getNilaiSetting('menukm'));
		$rows = $this->UM->getRoleItems($idrole);
		$roleitems = array();	
		foreach ($rows as $row) {
			$roleitems[$row['IDITEM']] = $row;
		}

		$rows = $this->UM->getItemAccess($idrole);
		foreach ($rows as $row) {
			$this->item_access[$row['IDITEM']] = $row;
		}
		
		$rows = $this->UM->getMenuAccess();
		foreach ($rows as $row) {
			$this->menu_access[$row['IDMENU']] = $roleitems[$row['IDITEM']];
		}

		//$this->data['menus'] = 	"<ul class=\"nav navbar-nav\">\n" . $this->_drawMenu() . "</ul>\n";

		if ($this->config->item('use_apc')) {
			apc_store($key, $this->data['menus']);
		}
		
		unset($this->menu_access);
		unset($this->menu_unserialize);
	}
	
	protected function _drawMenu($idparent=false) {
		$ret = '';
		foreach ($this->menu_unserialize as $menu) {
			if ($menu['IDPARENT'] != $idparent)
				continue;
			
			if (!$menu['ISACTIVE'])
				continue;
			
			if ($menu['NAMA'] == '---') {
				$ret .= '<li><hr/></li>';
				continue;
			}
			
			
			$has_child = $this->_menuHasChild($menu['IDMENU']);
			
			if (!$has_child && !$menu['IDITEM']) {
				continue;
			}
			
			if (!$this->menu_access[$menu['IDMENU']]['ISREAD'] && !$has_child) {
				continue;
			}
			
			$class_li = '';
			$tabindex = '';
			$data_toggle = '';
			$dropdown_toggle = '';
			$class_dropdown_toggle = '';
			if (!$menu['IDPARENT']) {
				$class_li = 'class="dropdown"';
				$data_toggle = 'data-toggle="dropdown"';
				$class_dropdown_toggle = 'class="dropdown-toggle"';
			}
			else {
				if ($has_child) {
					$tabindex = 'tabindex="-1"';
					$class_li = 'class="dropdown-submenu"';
				}
			}
			
			$ret .= "<li $class_li>";
			
			if ($has_child) {
				$ahref = '';
				if ($menu['NAMAFILE'])
					$ahref = 'href="' . site_url($menu['NAMAFILE']) . '"';
					
				$ret .= "<a $tabindex $data_toggle $class_dropdown_toggle $ahref>{$menu['NAMA']} ";
					
				if(empty($menu['IDPARENT']))
					$ret .= "<b class=\"caret\"></b>";
				$ret .= "</a>";
			}
			else {
				if ($menu['NAMAFILE'])
					$ret .= "<a href=\"".site_url($menu['NAMAFILE'])."\">{$menu['NAMA']}</a>";
				else
					$ret .= $menu['NAMA'];
			}
			
			if ($has_child)
				$ret .= "\n<ul class=\"dropdown-menu\" role=\"menu\">\n";

			$ret .= $this->_drawMenu($menu['IDMENU']);

			if ($has_child)
				$ret .= "</ul>\n";

			$ret .= "</li>\n";
		}
		
		return $ret;
	}
	
	private function _menuHasChild($idmenu) {
        $ret = 0;
		foreach ($this->menu_unserialize as $menu) {
			if ($menu['IDPARENT'] == $idmenu) {
				if ($menu['ISACTIVE'] && $this->menu_access[$menu['IDMENU']]) {
					$ret += 1;
                    return $ret;
				}

				$ret += $this->_menuHasChild($menu['IDMENU']);
			}
		}
        
		return $ret;
	}
		
	protected function checkRoleAuth($item=null) {
		if (!$item)
			$item = $this->router->class;
		$this->load->model('Um_model', 'UM');
		$iditem = $this->UM->getItemFile($item);
		if (!$iditem) 
			$this->toNoData();

		$access = 'ISREAD';
		if ($this->method == 'add')
			$access = 'ISCREATE';
		elseif ($this->method == 'edit')
			$access = 'ISUPDATE';
		elseif ($this->method == 'detail')
			$access = 'ISREAD';
		elseif ($this->method == 'delete')
			$access = 'ISDELETE';
			
		if (!$this->item_access[$iditem][$access]) {
			$this->toNoData();
		}
		
		$arr = array();
		if ($this->item_access[$iditem]['ISCREATE'])
			$arr[] = 'c_create';
		if ($this->item_access[$iditem]['ISREAD'])
			$arr[] = 'c_read';
		if ($this->item_access[$iditem]['ISUPDATE'])
			$arr[] = 'c_update';
		if ($this->item_access[$iditem]['ISDELETE'])
			$arr[] = 'c_delete';
		
		$this->auth = $arr;

		$this->data['c_create'] = $this->item_access[$iditem]['ISCREATE'];
		$this->data['c_read'] = $this->item_access[$iditem]['ISREAD'];
		$this->data['c_update'] = $this->item_access[$iditem]['ISUPDATE'];
		$this->data['c_delete'] = $this->item_access[$iditem]['ISDELETE'];
	}
	
	protected function getRoleAuth($item) {
		$idrole = $_SESSION[G_SESSION]['idrole'];
		$sql = "select * from roleitem where idrole=$idrole and iditem in
			(select iditem from item where namafile='$item' and isactive=1)";
		return dbGetRow($sql);
	}
		

	function isAuth($page, $modul='1', $access='ISREAD') {
		$sql = "select iditem from {$this->um}.item where idmodul=$modul and namafile='$page'";
		$iditem = dbGetOne($sql);
		
		if (!$this->item_access[$iditem][$access])
			return false;
		
		return true;
	}

	protected function updateGateMenuAkses() {
		if ($_SESSION[G_SESSION]['MODUL_KODEMODUL'] == 'ADMIN') {
			$table = 'gate_setting';

			$val = serialize($this->Menu->getList($_SESSION[G_SESSION]['MODUL_KODEMODUL']));
			$record = array();
			$record['NILAI'] = $val;
			$ret = dbUpdateCLOB($table, $record, "IDSETTING='MENUS'");		

			$val = serialize($this->Akses->getAksesModul($_SESSION[G_SESSION]['MODUL_KODEMODUL']));
			$record = array();
			$record['NILAI'] = $val;
			$ret = dbUpdateCLOB($table, $record, "IDSETTING='AKSES'");		

			oci_commit($this->db->conn_id);
		}
	}
	
	protected function genList($name, $rows, $val, $add, $title) {
		$list = "<select name=\"$name\" id=\"$name\" $add>";
		if ($title)
			$list .= "<option value=\"\">$title</option>";
		foreach ($rows as $row) {
			$selected = '';
			if ($row['VAL'] == $val)
				$selected = 'selected';
			$list .= "<option value=\"{$row['VAL']}\" $selected>{$row['LABEL']}</option>";
		}
		$list .= "</select>";
		
		return $list;
	}
	
	protected function retParseAjax($inputRet) {
		$sep1 = '_##_';
		$sep2 = '__|__';
		$sep3 = '::';
		
		$ret = '';
		
		foreach ($inputRet as $arr) {
			$id = $arr['id'];
			$type = $arr['type'];
			$value = $arr['value'];
			$ret .= "{$id}{$sep3}{$type}{$sep2}{$value}{$sep1}";
		}
		return $ret;
	}

	protected function toNoData($message=null) {
		$template = 'default/';

		$this->data['message'] = $message;
		
		$this->view->layout = $template.'_layout';
		$this->view->data($this->data);
		$this->view->load(array( 
			'header'     => $template.'_header',    
			'menu'       => $template.'_menu',
			'footer'     => $template.'_footer',
			'content'    => $template.'error', 
		));
		$this->view->render();

		echo $this->output->get_output();
		exit;
	}

	protected function renderView($content, $only_content=false) {
		$template = 'default/';

		$this->view->data($this->data);

		$this->view_items = array();
		if ($only_content) {
			$this->view_items['content'] = $template.$content;
		}
		else {
			$this->view->layout = $template.'_layout';
			$this->view_items['header'] = $template .'_header';
			$this->view_items['menu'] = $template.'_menu';
			$this->view_items['login_strip'] = $template.'_login_strip';
			$this->view_items['content']  = $template.$content;
			$this->view_items['footer'] = $template.'_footer';
		}

		$this->view->load($this->view_items);
		$this->view->render();
	}


	public static function toMap($data = array(), $key, $val, $output = array()) {
        foreach ($data as $row) {
            $output[$row[$key]] = $row[$val];
        }
        return $output;
    }

    
	protected function renderViewSimple($content) {
		$template = 'default/';

		$this->view->data($this->data);

		$this->view_items = array();
		$this->view->layout = $template.'_layout_simple';
		$this->view_items['content']  = $template.$content;
		//$this->view_items['footer'] = $template.'_footer';
		
		$this->view->load($this->view_items);
		$this->view->render();
	}
	
	protected function curl($url, $fields) {
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
		
		if (!function_exists('curl_exec')) {
			# no curl
			$this->sendEmail($fields);
			
			return false;
		}
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
		curl_exec($ch);
		curl_close($ch);
	}

	protected function sendEmail($data) {
		if (!$this->config->item('allow_send_email'))
			return false;
		
		$this->load->library('mailer');
		
		$to = $data['to'];
		$bcc = $this->config->item('email_test');

		$mail = $this->mailer->getMailer();

		$mail->IsSMTP();
		$mail->SMTPDebug = 2;
		$mail->SMTPAuth = $this->config->item('smtp_auth');
		if ($this->config->item('smtp_secure')) {
			$mail->SMTPSecure = $this->config->item('smtp_secure');
		}

		$mail->Host = $this->config->item('smtp_host');
		$mail->Port = $this->config->item('smtp_port'); 
		$mail->Username = $this->config->item('smtp_user');  
		$mail->Password = $this->config->item('smtp_pass');
		$mail->From = $this->config->item('email_from'); 
		$mail->FromName = $this->config->item('email_from_name'); 
		$mail->Subject = $data['subject'];
		$mail->IsHTML(true);
		$mail->Body = $data['body'];
		$to_list = explode(',', $to);
		foreach ($to_list as $to) {
			$mail->AddAddress($to);
		}
		
        if($this->config->item('using_test_email')) {
		    $bcc_list = explode(',', $bcc);
		    foreach ($bcc_list as $bcc) {
			    $mail->AddBCC($bcc);
		    }
		}
        
		if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo; 
			$this->output->set_output($error);
            echo $error;
            die();
		}
		return true;
	}

	protected function createPassword($length){
	    $chrs = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z");

	    $num_chrs = count($chrs);
		$password = "";
	    for($i = 0; $i < $length; $i++){
	        $password .= $chrs[rand(0, $num_chrs - 1)];
	    }
	    return substr($password, 0, $length);
	}

	protected function renderPagination($site_url, $num_rows, $offset=0, $limit=10, $num_links=3) {
		if ($num_rows > $limit) {
			$this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = site_url($site_url);
            $config['total_rows'] = $num_rows;
            $config['per_page'] = $limit;
			$config['num_links'] = $num_links;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
        }
	}
	
	protected function renderPagination2($site_url, $num_rows, $offset=0, $limit=10, $num_links=3) {
		if ($num_rows > $limit) {
			$this->data['offset'] = $offset;
            $this->load->library('pagination');
            $config['base_url'] = site_url($site_url);
            $config['total_rows'] = $num_rows;
            $config['per_page'] = $limit;
			$config['num_links'] = $num_links;
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '&laquo';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '&raquo';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
        }
	}

	protected function initFilter($filter) {
		$sql = '';
		
		if (!empty($filter)) {
			$filterarray = explode(':',$filter);
			for ($i=0;$i<count($filterarray);$i = $i + 3) 
			{
				$filterstr = '';
				$filtercol = $filterarray[$i];
				$filterdata = $filterarray[$i+1];
				if($filterdata == '')
					$filterdata = '*';
				$filtertype = $filterarray[$i+2];
				if($filtertype == '')
					$filtertype = 'C';

				$filterop = '';
				// pemeriksaan operator perbandingan
				$arrop = array('<>','<=','>=','<','>','=');
				for ($n=0;$n<count($arrop);$n++) {
					$oppos = strpos($filterdata,$arrop[$n]);
					if ($oppos !== false) { // operator perbandingan ditemukan
						$filterop = $arrop[$n];
						$filterdata = str_replace($filterop,'',$filterdata); // hilangkan operator dari string filter
						break;
					}
				}
				if (!$filterop)
					$filterop = '='; // default operator
				
				switch ($filtertype) {
					case 'C' : 	// char atau varchar
								if (strstr($filtercol,',')) {
									$filtercols = explode(',', $filtercol);
									foreach ($filtercols as $k=>$filtercol) {
										$filterstr .= 'lower('.$filtercol.") like '%".strtr(strtolower(trim($filterdata)),'*','%')."%' ";
										if ($k < count($filtercols)-1)
											$filterstr .= ' or ';
									}
								}
								else 
									$filterstr .= 'lower('.$filtercol.") like '%".strtr(strtolower(trim($filterdata)),'*','%')."%'";
								break;
					case 'I' : 	// integer
					case 'N' : 	// numeric atau float
								$filterstr .= $filtercol.' '.$filterop.' '.$filterdata;
								break;
					default	 :	// yang lain
								$filterstr .= $filtercol.' '.$filterop." '".$filterdata."'";
								break;
				}
				$sql .= ' and ('.$filterstr.')';
			}
		}
		return $sql;
	}

	protected function responsePost() {
		$act = $this->input->post('act');
		if ($act === 'add_mode' && $this->data['c_create'] && !$this->input->post('key'))
			redirect ("/$this->ctl/add/");
		elseif ($act === 'detail_mode' && $this->data['c_read'] && $this->input->post('key'))
			redirect ("/$this->ctl/detail/".$this->input->post('key'));
		elseif ($act === 'edit_mode' && $this->data['c_update'] && $this->input->post('key'))
			redirect ("/$this->ctl/edit/".$this->input->post('key'));
		elseif ($act === 'delete_mode' && $this->data['c_delete'] && $this->input->post('key'))
			redirect ("/$this->ctl/delete/".$this->input->post('key'));
		elseif ($act === 'list') {
			$offset = (int) $_SESSION[G_SESSION][$this->ctl]['offset'];
			if ($offset)
				redirect ("$this->ctl/lst/". $offset);
			else
				redirect ("$this->ctl/lst");
		}
		elseif ($act === 'do_filter') {
			unset($_SESSION[G_SESSION][$this->ctl]);
			$_SESSION[G_SESSION][$this->ctl]['filter_string'] = $this->input->post('filter_string');
			redirect ("/$this->ctl/lst/".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'do_sort') {
			$filter_sort = $this->input->post('filter_sort');
			$sort = 'asc';
			if (stristr($filter_sort,'desc'))
				$sort = 'desc';
				
			if (strstr($filter_sort,',')) {
				$columns = explode(',', $filter_sort);
				$filter_sort = '';
				foreach ($columns as $column) {
					if (!preg_match('(asc|desc)', $column))
						$filter_sort .= $column . ' ' . $sort . ', ';
					else
						$filter_sort .= $column;
				}
			}
			
			$_SESSION[G_SESSION][$this->ctl]['filter_sort'] = $filter_sort;
			redirect ("$this->ctl/lst/".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'num_record') {
			unset($_SESSION[G_SESSION][$this->ctl]['offset']);
			$_SESSION[G_SESSION][$this->ctl]['filter_num_record'] = $this->input->post('filter_num_record');
			redirect ("$this->ctl/lst/".$_SESSION[G_SESSION][$this->ctl]['offset']);
		}
		elseif ($act === 'reset_filter') {
			unset($_SESSION[G_SESSION][$this->ctl]);
			redirect ("$this->ctl");
		}
	}
	
	protected function setModelFilter() {
		$this->model->setFilter($this->initFilter($_SESSION[G_SESSION][$this->ctl]['filter_string']));
		$this->model->setSort($_SESSION[G_SESSION][$this->ctl]['filter_sort']);
		
		$this->data['filter_string'] = $_SESSION[G_SESSION][$this->ctl]['filter_string'];
	}
	
	##########################
	
	private function _setStickyNews() {
		$sql = "select val from misc where idmisc='sticky_news' and ispublish=1 ";
		$this->data['sticky_news'] = dbGetOne($sql);		
	}

	private function initLayoutType() {
		if(!class_exists('Mobile_Detect')){ return 'classic'; }
	
		$detect = new Mobile_Detect;
		$isMobile = $detect->isMobile();
		$isTablet = $detect->isTablet();
	
		$layoutTypes = array('classic', 'mobile', 'tablet');
	
		if ( isset($_GET['layoutType']) ){
			$layoutType = $_GET['layoutType'];
		} else {
			if(empty($_SESSION[G_SESSION]['layoutType'])){
				$layoutType = ($isMobile ? ($isTablet ? 'tablet' : 'mobile') : 'classic');	
			} else {
				$layoutType =  $_SESSION[G_SESSION]['layoutType'];
			}
		}
	
		if( !in_array($layoutType, $layoutTypes) ){ $layoutType = 'classic'; }
	
		$this->layout_type = $_SESSION[G_SESSION]['layoutType'] = $layoutType;
	
		return $layoutType;
	}
	
}
