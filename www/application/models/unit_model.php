<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_model extends Base_Model {
	## EDIT HERE ##
    public $table = 'UNIT';
    public $id = 'KODEUNIT';
	public $sort = 'NAMA ASC';
	public $filter = '';

    function Unit_model() {
        parent::__construct();
		$this->table = $this->um . '.'. $this->table;
    }
    function listComboUnit(){
		$data = array();
		
		$list_unit = $this->getList();
		
		foreach ($list_unit as $k => $v)
			$data[$v['KODEUNIT']] = $v['NAMA'];
			
		return $data;
	}
	
}
