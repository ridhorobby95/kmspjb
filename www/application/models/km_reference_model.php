<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Km_reference_model extends Base_Model {
	## EDIT HERE ##
    public $table = 'KNOWLEDGE_REFERENCE';
    public $id = 'IDREFERENCE';
	public $sort = 'IDREFERENCE ASC';
	public $filter = '';

    function Km_reference_model() {
        parent::__construct();
		$this->table = $this->km . '.'. $this->table;
    }

    public function updateReferenceKM($idknowledge, $list)
    {
		foreach ($list as $key => $idref) {
			$sql = "insert into {$this->table} (idknowledge, idknowledge_ref) values ('$idknowledge', '$idref')";
			dbQuery($sql);
		}
    }

    public function getListAll()
    {
    	$sql = "select idreference, RAWTOHEX(idknowledge) as idknowledge, RAWTOHEX(idknowledge_ref) as idknowledge_ref from {$this->table}";
		$row = dbGetRows($sql);
		if (!$row)
			return false;

		return $row;
    }

}
