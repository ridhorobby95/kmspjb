<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends Base_Model {
	## EDIT HERE ##
    public $table = 'USERS';
    public $id = 'IDUSER';
	public $sort = 'NAMA asc';
	public $filter = '';

    function User_model() {
		parent::__construct();
		$this->table = $this->um . '.'. $this->table;
    }
	
	function getUserByUsername($username) {
		$sql = "select * from {$this->table} where nid='{$username}'";
		return dbGetRow($sql);
	}
	
	function getListUsers() {
		$sql = "select * from {$this->table}";
		return dbGetRows($sql);
	}

	function getRoles($iduser) {
		$sql = "select ur.*, r.nama as namarole
				from {$this->um}.userrole ur 
				join {$this->um}.role r on ur.idrole=r.idrole
				and ur.iduser=$iduser order by r.idrole ";
		$rows = dbGetRows($sql);
		return $rows;
	}
	
	function addRole($record) {
        $ret = dbInsert("{$this->um}.userrole", $record);
        if (!$ret)
            return false;
        
		return true;
	}

    function deleteRole($iduser, $idrole, $idjabatan) {
		$sql = "delete from {$this->um}.userrole where iduser=$iduser and idrole=$idrole and idjabatan=$idjabatan";
        $ret = dbQuery($sql);
        if (!$ret)
            return false;
		return true;
    }
	
    
}
