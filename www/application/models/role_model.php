<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends Base_Model {
    public $table = 'ROLE';
    public $id = 'IDROLE';
	public $sort = 'IDROLE asc';
	public $filter = '';

    function Role_model() {
        parent::__construct();
		$this->table = $this->um . '.'. $this->table;
    }

	function getItems($idrole, $isall=true) {
		$sql = "select i.*, r.iscreate, r.isread, r.isupdate, r.isdelete 
			from {$this->um}.item i left join {$this->um}.roleitem r on i.iditem=r.iditem and r.idrole=$idrole ";
		if (!$isall) {
			$sql .= "where (coalesce(iscreate,0) != 0 or coalesce(isread,0) != 0 
			or coalesce(isupdate,0) != 0 or coalesce(isdelete,0) != 0) ";
		}
		$sql .= "order by i.nama ";
		
		return dbGetRows($sql);
	}
	
	function getUsers($idrole) {
		$sql = "select r.*, u.nama as namauser
			from {$this->um}.userrole r join {$this->um}.users u on u.iduser=r.iduser and r.idrole=$idrole 
			order by u.nama ";
		return dbGetRows($sql);
	}

	function deleteRoleItem($idrole) {
		$sql = "delete from {$this->um}.roleitem where idrole=$idrole";
		return dbQuery($sql);
	}
	
	function insertRoleItem($record) {
		return dbInsert("{$this->um}.roleitem", $record);
	}
	
}
