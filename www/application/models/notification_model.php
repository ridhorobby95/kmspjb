<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends Base_Model {
    const BARU = 'N';
    const SENT = 'S';
    const BACA = 'R';
    public $table = 'NOTIFICATIONS';

    function Notification_model() {
        parent::__construct();
        $this->table = $this->km . '.'. $this->table;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function coba(){
        echo "sdfdsf";
    }
    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function join($join='id'){
        $this->_join = $join;
        return $this;
    }

    public function group_by($group_by=''){
        $this->_group_by = $group_by;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getBy(){
       $sql = "select $this->_columns from $this->_table $this->_alias $this->_join $this->_filter $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne(){
        $sql = "select $this->_columns from $this->table $this->_filter $this->_limit $this->_group_by $this->_order" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetOne($sql);
        return $column;
    }

    public function getAll(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetRows($sql);
        return $column;
    }

    public function getNotification($offset=0){
        $sql ="select * from km.notifications where iduser=".$_SESSION[G_SESSION]['iduser']." and status='".self::SENT."' or status='".self::BACA."' order by idnotifications desc";
        if ($this->_show_sql){
            echo nl2br($sql);
            die;
        }
        $notif = dbGetRows($sql);
        // if(!empty($notif)){
        //     for ($i=0; $i < count($notif) ; $i++) { 
        //         if ($_SERVER['SERVER_ADDR'] != '::1') {
        //             $pisah =explode('/', $notif[$i]['url']);
        //             $pisah[2] = gethostbyname(gethostname());
        //             $new_url = implode('/', $pisah);
        //             $notif[$i]['url'] = $new_url;
        //         }
        //     }
        // }
        return $notif;
    }

    public function getCountNotifications(){
        $sql ="select count(idnotifications) as jumlah_unread from km.notifications where iduser=".$_SESSION[G_SESSION]['iduser']." and status='".self::SENT."'";
        if ($this->_show_sql){
            echo nl2br($sql);
            die;
        }
        $jumlah = dbGetOne($sql);
        return $jumlah;
    }


    public function countUnreadNotification(){
        $sql ="select count(id) as jumlah_unread from km.notifications where id_user=".SessionManagerWeb::getUserID()." and status='N'";
        if ($this->_show_sql){
            echo nl2br($sql);
            die;
        }
        return dbGetOne($sql);
    }

    public function setReadAll() {
        $condition = array('id_user' => SessionManagerWeb::getUserID(), 'status' => self::BARU);
        return $this->update_by($condition, array('status' => self::BACA));
    }

    public function add($data){
        $sender     = $_SESSION[G_SESSION]['iduser'];
        $namaSender   = $_SESSION[G_SESSION]['nama'];
        $dateNow = date("m/d/Y");
        $datePelaksanaan = date("m/d/Y", strtotime($data['waktupelaksanaan']));
        // echo $dateNow." ::".$datePelaksanaan."<br>";
        // echo $this->table;
        if(strtotime($datePelaksanaan) >= strtotime($dateNow)){
            // echo "insert masuk";
            $datetime1 = new DateTime($dateNow);
            $datetime2 = new DateTime($datePelaksanaan);
            $difference = $datetime1->diff($datetime2);
            if($difference->days <= 7){ // jika dilaksanaan kurang dari 7 hari, langsung kirim notif
                $status     = trim(self::SENT);
                // $sendDate   = $dateNow;
                $dateFormat = date("Y/m/d", strtotime($dateNow));
                $waktupelaksanaan = date("Y/m/d", strtotime($datePelaksanaan));
                $sendDate = $dateFormat;
                // $sendDate = "to_date('$dateNow','yyyy/mm/dd')";
                
            }
            else{
                $status = self::BARU;
                $waktupelaksanaan = date("Y/m/d", strtotime($datePelaksanaan));
                $sendDate = null;
            }
            $dataInsert = array(
                'IDUSER'    => $data['iduser'],
                'SENDER'    => $sender,
                'MESSAGE'   => "<b>".$namaSender."</b> telah mengundang anda pada Knowledge <b>".$data['judul']."</b>",
                'URL'       => "km/detail/".$data['idknowledge'],
                'STATUS'    => $status,
                'SEND_DATE' => $sendDate
            );
            $checkdata = dbGetOne("select idnotifications from km.notifications where iduser=".$data['iduser']." and idknowledge='".$data['idknowledge']."'");
            if(!$checkdata){
                dbQuery("insert into km.NOTIFICATIONS (IDUSER, SENDER, MESSAGE, URL, STATUS, SEND_DATE, WAKTUPELAKSANAAN, IDKNOWLEDGE) values (".$data['iduser'].", ".$sender.", '".$dataInsert['MESSAGE']."', '".$dataInsert['URL']."', '".$status."', TO_DATE('".$sendDate."', 'yyyy/mm/dd'), TO_DATE('".$waktupelaksanaan."', 'yyyy/mm/dd'), '".$data['idknowledge']."')");
            }
            // echo "<pre>";print_r($dataInsert);echo "</pre>";
            // dbInsert($this->table, $dataInsert);
        }
    }

    public function getForUpdate(){
        $sql ="select * from km.notifications where status='".self::BARU."'";
        $column = dbGetRows($sql);
        return $column;
    }

}
