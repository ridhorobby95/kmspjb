<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filemanager_model extends Base_Model 
{
	## EDIT HERE ##
    public $table          = 'LP.LAMPIRANPERPUS';
    public $pustakatable   = 'PERPUS.MS_PUSTAKA';
    public $id 		       = 'IDLAMPIRANPERPUS';
	public $sort           = 'FILENAME asc';
	public $filter         = '';

	public function __construct()
	{
		parent::__construct();
	}    
    
    public function insert($data, $content = NULL)
    {
        if( $data )
        {
            foreach( $data as $key => $val ) 
            {
                $a_key[]     = $key;
                $a_val[]     = "'". $val ."'"; 
            }

            foreach( $content as $key => $val ) 
            {
                $a_key[] 				= $key;
                $a_keyblob[] 			= $key;
                $a_keybind[] 			= ":".$key;
                $a_content[":".$key] 	= $val;
                $a_val[] 				= "EMPTY_BLOB()";
            }
            
            $sql = "INSERT INTO $this->table (". implode(',', $a_key) . ") VALUES (".implode(',', $a_val) .") ";

            if($a_keybind) {
                $sql .= "RETURNING " . implode(",", $a_keyblob) . " INTO " . implode(",", $a_keybind);
            }

			#statement id
            $stid = oci_parse($this->db->conn_id, $sql);
            
            if($a_keybind) {
                foreach ($a_keybind as $ndx => $keys) {
                    $$a_keyblob[$ndx] = oci_new_descriptor($this->db->conn_id, OCI_D_LOB);
                    oci_bind_by_name($stid, "$keys", $$a_keyblob[$ndx], -1, OCI_B_BLOB);
                }
            }
            
            #execute
            $status = oci_execute($stid, OCI_DEFAULT);

            if($a_keybind) 
            {
            	$statusok = $statusfalse = 0;

                foreach ($a_keybind as $ndx => $keys) 
                {
                    if( ! $$a_keyblob[$ndx]->save($a_content[$keys]) ){
    					++$statusfalse;
                    }
                    else {
                        ++$statusok;
                    }
                }
            }

            if( $statusok == count($a_keybind) or $status ){
            	$status = TRUE;
            	oci_commit($this->db->conn_id);
            }
            else{
            	$status = FALSE;
            	oci_rollback($this->db->conn_id);
            }

            #free statement
            oci_free_statement($stid);
            
            if($a_keybind) {
                foreach ($a_keybind as $ndx => $keys) {
                    $$a_keyblob[$ndx]->free();    
                }
            } 

            #get inserted id
            $sql = ociparse($this->db->conn_id, "select max(IDLAMPIRANPERPUS) ID from $this->table");
            ociexecute($sql);
        
            while ($row = oci_fetch_array ($sql, OCI_ASSOC)) {
                $a_row = $row;        
            }
        }

        if( $status )
			return array('id' => $a_row['IDLAMPIRANPERPUS'], 'tipe' => 'info', 'message' => 'File ' . $data['FILENAME'] . ' berhasil disimpan...<br>');
		else
			return array('tipe' => 'error', 'message' => 'File ' . $data['FILENAME'] . ' gagal disimpan...<br>');
    }

	public function update($data, $content=null, $id, $filename)
	{
		if( $data )
		{
			foreach( $data as $key => $val ) 
            {
                $a_key[]	= $key;
                $a_values[]	= "$key = '$val'"; 
            }

            foreach( $content as $key => $val ) 
            {
                $a_key[] 				= $key;
                $a_keyblob[] 			= $key;
                $a_keybind[] 			= ":".$key;
                $a_content[":".$key] 	= $val;
                $a_val[] 				= "EMPTY_BLOB()";
                $a_values[] 			= "$key = EMPTY_BLOB()";
            }

			$sql  = "UPDATE $this->pustakatable SET " . implode(',', $a_values) . " WHERE IDPUSTAKA = '$id' ";

			if($a_keybind) {
                $sql .= "RETURNING " . implode(",", $a_keyblob) . " INTO " . implode(",", $a_keybind);
            }

			#statement id
            $stid = oci_parse($this->db->conn_id, $sql);
            
            if($a_keybind) {
                foreach ($a_keybind as $ndx => $keys) {
                    $$a_keyblob[$ndx] = oci_new_descriptor($this->db->conn_id, OCI_D_LOB);
                    oci_bind_by_name($stid, "$keys", $$a_keyblob[$ndx], -1, OCI_B_BLOB);
                }
            }
            
            #execute
            $status = oci_execute($stid, OCI_DEFAULT);

            if($a_keybind) 
            {
            	$statusok = $statusfalse = 0;

                foreach ($a_keybind as $ndx => $keys) 
                {
                    if( ! $$a_keyblob[$ndx]->save($a_content[$keys]) ){
    					++$statusfalse;
                    }
                    else {
                        ++$statusok;
                    }
                }
            }

            if( $statusok == count($a_keybind) or $status ){
            	$status = TRUE;
            	oci_commit($this->db->conn_id);
            }
            else{
            	$status = FALSE;
            	oci_rollback($this->db->conn_id);
            }

            #free statement
            oci_free_statement($stid);
            
            if($a_keybind) {
                foreach ($a_keybind as $ndx => $keys) {
                    $$a_keyblob[$ndx]->free();    
                }
            }
		}

		if( $status )
			return array('tipe' => 'info', 'message' => 'File ' . $filename . ' berhasil diupdate...<br>');
		else
			return array('tipe' => 'error', 'message' => 'File ' . $filename . ' gagal diupdate...<br>');	
	}

	public function fileExists($idlampiran, $namafile)
	{
		$this->db->where('IDLAMPIRAN', $idlampiran);
		$this->db->where('FILENAME', $namafile);
		$this->db->from($this->table);

		if( $this->db->count_all_results() )
			return TRUE;
		else
			return FALSE;
	}
}