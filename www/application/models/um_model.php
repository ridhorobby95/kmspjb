<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Um_model extends Base_Model {

    public function __construct() {
        parent::__construct();
    }

	function getNilaiSetting($menu) {
		$sql = "select nilai from {$this->um}.setting where idsetting='$menu'";
		return dbGetOne($sql);
	}
	
	function getRoleItems($idrole) {
		$sql = "select * from {$this->um}.roleitem where idrole=$idrole";
		return dbGetRows($sql);
	}
	
	function getAccessItem($idrole, $item) {
		$item = strtolower(trim(xRemoveSpecial($item)));
		$sql = "select iditem from {$this->um}.item where lower(namafile) = '$item'";

		$iditem = dbGetOne($sql);
		if (!$iditem)
			return false;
		
		$sql = "select * from {$this->um}.roleitem where idrole=$idrole and iditem=$iditem";
		return dbGetRow($sql);
	}
    
	function getItemAccess($idrole) {
		$sql = "select r.iditem, iscreate, isread, isupdate, isdelete from {$this->um}.roleitem r join {$this->um}.item i on r.iditem=i.iditem 
			where i.isactive=1 and idrole=$idrole";

		return dbGetRows($sql);
	}
	
	function getMenuAccess() {
		$sql = "select idmenu, nama, idparent, iditem, itemaccess from {$this->um}.menu where isactive=1 ";
		return dbGetRows($sql);
	}
	
	function getItemFile($namafile) {
		$sql = "select iditem from {$this->um}.item where namafile='$namafile'";
		return dbGetOne($sql);
	}
}
