<?php

class Menu_model extends Base_Model {
	## EDIT HERE ##
    public $table = 'MENU';
    public $id = 'IDMENU';
	public $sort = 'a.tingkat asc, a.urutan asc';
	public $filter = '';

    function Menu_model() {
        parent::__construct();
		$this->table = $this->um . '.'. $this->table;
    }

	function getList($limit, $offset=0) {
		$sql = "select a.*, b.nama as namaparent, i.nama as namaitem  
			from $this->table a left join $this->table b on a.idparent=b.idmenu 
			left join {$this->um}.item i on a.iditem=i.iditem
			where 1=1 
			order by a.tingkat, a.idparent, a.urutan ";
		$menus =  dbGetRows($sql, $limit, $offset);
		$this->listquery = $sql;
			
		if (!$menus)
			return false;
	
		$menu_arr = array();
		$i=0;
		foreach ($menus as $row) {
			$i++;
			$row['URUTAN'] = $i;
			$menu_arr[] = $row;
		}

		ksort($menu_arr);
		$list = array();
		$this->_generateParents($list, $menu_arr);
			
		return $list;
	}

	private function _generateParents(&$ret, $array, $parent = 0, $tingkat = 0, $val='') {
		$has_children = false;
		foreach($array as $value) {
			
			$key = $value['IDMENU'];
			
			if ($value['IDPARENT'] == $parent ) {
				if ($has_children == false and !empty($key) ) {
					$has_children = true;  
					$tingkat++;
				}
	
				$value['TINGKAT'] = $tingkat;
				$ret[] = $value;
	
				$this->_generateParents($ret, $array, $key, $tingkat, $val); 
			}
		}
	}
	
	function getListTree() {
		$sql = "select * from {$this->table} a left join {$this->um}.item i on a.iditem=i.iditem 
		where (i.idmodul='{$_SESSION[G_SESSION]['IDMODUL']}' or a.iditem is null) order by tingkat, idparent, urutan ";
		$rs = dbGetRows($sql);
		$list = array();
		foreach ($rs as $row) {
			$list[$row['IDMENU']] = $row;
		}
		$ret = array();
		$this->setListTree($ret, $list);
		
		return $ret;
	}
	
	function setListTree(&$ret, $array, $parent = 0, $tingkat = 0) {
		$has_children = false;
		foreach($array as $key => $value) {
			if ($value['IDPARENT'] == $parent) {                   
				if ($has_children === false) {
					$has_children = true;  
					$tingkat++;
				}
				$sql = "select * from {$this->table} where idmenu='{$value['IDMENU']}' order by urutan";
				$row = dbGetRow($sql);
				$row['TINGKAT'] = $tingkat;
				$ret[$value['IDMENU']] = $row;
				$this->setListTree($ret, $array, $key, $tingkat); 
			}
		}
	}
	
	function getMenuParent($idmenu=null,$tingkat=null) {
		$sql = "select idmenu, a.nama, tingkat, idparent from {$this->table} a left join {$this->um}.item b on a.iditem=b.iditem 
		where (b.idmodul='{$_SESSION[G_SESSION]['IDMODUL']}' or a.iditem is null) and a.isactive=1 ";
		if ($idmenu)
			$sql .= "and idmenu not in ($idmenu) ";
		if ($tingkat)
			$sql .= "and tingkat<=$tingkat ";
		$sql .= "order by tingkat, urutan";
		return dbGetRows($sql);
	}

	function saveMenuSetting() {
		$list = $this->getListTree();
		$record = array();
		$file = $this->config->item('static_path')."/nilaiperpus.txt"; 
		$menu = 'menuperpus';
		file_put_contents($file,serialize($list));
		dbUpdate("{$this->um}.setting", $record, "idsetting='$menu'");
	}
    
}
