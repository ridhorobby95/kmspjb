<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Km_model extends Base_Model {
    public $table = 'KNOWLEDGE';
    public $id = 'IDKNOWLEDGE';
	//public $sort = 'nvl(waktupelaksanaan,createdtime) desc, tahuntulis desc, createdtime desc';
	public $sort = 'nvl(komentime, createdtime) desc';
	public $filter = '';
	public $home_limit = 10;

    function Km_model() {
        parent::__construct();
		$this->table = $this->km . '.'. $this->table;
    }
	

	function getOne($row, $idknowledge){
		return dbGetOne("select ".$row." from $this->table where idknowledge='".$idknowledge."'");
	}
	function getRow($id) {
		$sql = "select k.*, RAWTOHEX(k.idknowledge) as idknowledge_h, u.nama as namapemateri, t.nama as namaunit, j.nama as namajeniskm, j.singkat as namajeniskmsingkat,
				to_char(waktupelaksanaan, 'DD Mon YYYY') as waktupelaksanaan_h,
				to_char(waktupelaksanaan, 'DD-MM-YYYY') as waktupelaksanaan_h2,
				to_char(createdtime, 'DD Mon YYYY HH24:MI') as createdtime_h,
				to_char(modifiedtime, 'DD Mon YYYY HH24:MI') as modifiedtime_h,
				u.nama as namapemateri, us.nama as namauser ";
		if (xIsLoggedIn())
			$sql .= " ,sk.iduser as suka ";
		$sql .= "from $this->table k 
				join {$this->km}.jeniskm j on j.idjeniskm=k.idjeniskm
				left join {$this->um}.users u on k.pemateri=u.iduser
				left join {$this->um}.unit t on t.kodeunit=k.kodeunit
				left join {$this->um}.users us on us.iduser=k.createdby ";
		if (xIsLoggedIn())
			$sql .= "left join {$this->km}.suka sk on sk.idknowledge=k.idknowledge and sk.iduser={$_SESSION[G_SESSION]['iduser']} ";
		$sql .= "where k.idknowledge='$id' ";
		$row = dbGetRow($sql);
		if (!$row)
			return false;
		
		$row['allow_edit'] = $this->_isAllowEdit($row);
		$row['list_foto'] = $this->getListFoto($row['IDKNOWLEDGE_H']);
		$row['list_keywords'] = $this->getListKeywords($row['KEYWORDS']);
		$row['list_doc'] = $this->getListDoc($row['IDKNOWLEDGE_H']);
		$row['list_video'] = $this->getListVideo($row['IDKNOWLEDGE_H']);
		$row['list_subyek'] = $this->getListSubyek($row['IDKNOWLEDGE_H']);
		$row['list_anggota'] = $this->getListAnggotaTim($row['IDKNOWLEDGE_H']);
		$row['list_komentar'] = $this->getListKomentar($row['IDKNOWLEDGE_H']);
		$row['list_suka'] = $this->getListSuka($row['IDKNOWLEDGE_H']);
		$row['NUM_SUKA'] = $this->getNumSuka($row['IDKNOWLEDGE_H']);
		$row['NUM_KOMENTAR'] = $this->getNumKomentar($row['IDKNOWLEDGE_H']);

		return $row;
	}

    function getList($limit=0, $offset=0, $is_count=false) {
		if ($is_count)
			$sql = "select 1 as num, (select max(d.tinserttime) from {$this->km}.diskusi d where d.idknowledge = k.idknowledge) as komentime ";
		else {
			$sql = "select distinct k.*, RAWTOHEX(k.idknowledge) as idknowledge_h, t.nama as namaunit, j.nama as namajeniskm, j.singkat as namajeniskmsingkat,
					to_char(waktupelaksanaan, 'DD Mon YYYY') as waktupelaksanaan_h,
					to_char(waktupelaksanaan, 'DD-MM-YYYY') as waktupelaksanaan_h2,
					to_char(createdtime, 'DD Mon YYYY HH24:MI') as createdtime_h,
					to_char(modifiedtime, 'DD Mon YYYY HH24:MI') as modifiedtime_h,
					u.nama as namapemateri, us.nama as namauser,
					(select max(d.tinserttime) from {$this->km}.diskusi d where d.idknowledge = k.idknowledge) as komentime "; /* */
			if (xIsLoggedIn())
				$sql .= " ,sk.iduser as suka ";
		}
		
		$sql .= " from $this->table k 
				join {$this->km}.jeniskm j on j.idjeniskm=k.idjeniskm
				left join {$this->um}.users u on u.iduser=k.pemateri 
				left join {$this->um}.unit t on t.kodeunit=k.kodeunit 
				left join {$this->um}.users us on us.iduser=k.createdby
				";
			if($_SESSION[G_SESSION]['cari_km']){
				$sql .=" left join {$this->km}.kmsubyek y on k.idknowledge = y.idknowledge
				         left join {$this->km}.subyek x on y.idsubyek = x.idsubyek
						 left join {$this->km}.knowledge_keywords s on k.idknowledge = s.idknowledge
						 left join {$this->km}.keywords z on s.idkeywords= z.idkeywords and lower(z.keyword) like '%{$_SESSION[G_SESSION]['cari_km']}%'";
			}
			if (xIsLoggedIn())	
				$sql .= " left join {$this->km}.suka sk on sk.idknowledge=k.idknowledge and sk.iduser={$_SESSION[G_SESSION]['iduser']}	";
			
		if ($_SESSION[G_SESSION]['filter_unit'])
			$this->filter .= " and k.kodeunit='{$_SESSION[G_SESSION]['filter_unit']}' ";
		if ($_SESSION[G_SESSION]['cari_km'])
			$this->filter .= " and (lower(judul) like '%{$_SESSION[G_SESSION]['cari_km']}%' or lower(k.nama) like '%{$_SESSION[G_SESSION]['cari_km']}%' 
				or lower(abstrak) like '%{$_SESSION[G_SESSION]['cari_km']}%' or lower(u.nama) like '%{$_SESSION[G_SESSION]['cari_km']}%' or lower(x.nama) like '%{$_SESSION[G_SESSION]['cari_km']}%') ";
		if(isset($_SESSION[G_SESSION]['cari_km_startdate']) && isset($_SESSION[G_SESSION]['cari_km_enddate'])){
				$this->filter .=" and k.modifiedtime >= TO_DATE('".$_SESSION[G_SESSION]['cari_km_startdate']."','mm/dd/yyyy hh24:mi:ss') and k.modifiedtime <= TO_DATE('".$_SESSION[G_SESSION]['cari_km_enddate']."','mm/dd/yyyy hh24:mi:ss')";
		}
		elseif(isset($_SESSION[G_SESSION]['cari_km_startdate']) && !isset($_SESSION[G_SESSION]['cari_km_enddate'])){
				$this->filter .=" and k.modifiedtime >= TO_DATE('".$_SESSION[G_SESSION]['cari_km_startdate']."','mm/dd/yyyy hh24:mi:ss')";
		}
		
		$sql .= $this->filter;
		$sql .= " order by $this->sort ";
		echo "<!-- //8gX $sql -->";
		$this->listquery = $sql;
		if ($is_count) {
			$limit = $offset + $limit;
			$sql = "SELECT count(*) FROM (select inner_query.*, rownum rnum FROM ($sql) inner_query WHERE rownum <= $limit)";
	
			if ($offset != 0)
				$sql .= " WHERE rnum > $offset";

			return dbGetOne($sql);
		}
        $rows = dbGetRows($sql, $limit, $offset);
        // echo "<pre>";print_r($rows);die();
		if (count($rows)) {
			foreach ($rows as &$row) {
				$param = array('JUDUL', 'NAMA', 'ABSTRAK');
				xHighlightHomeSearch($row, $param);
				
				if ($_SESSION[G_SESSION]['is_condensed']) {
					if (isset($_SESSION[G_SESSION]['opsi_home_session'][$row['IDKNOWLEDGE_H']]['is_condensed']) && !$_SESSION[G_SESSION]['opsi_home_session'][$row['IDKNOWLEDGE_H']]['is_condensed'])
						$x = 1;
					else
						$row['ABSTRAK'] = character_limiter($row['ABSTRAK'], 100);
				}
				else {
					if ($_SESSION[G_SESSION]['opsi_home_session'][$row['IDKNOWLEDGE_H']]['is_condensed'])
						$row['ABSTRAK'] = character_limiter($row['ABSTRAK'], 100);
				}

				$row['allow_edit'] = $this->_isAllowEdit($row);

				$row['list_foto'] = $this->getListFoto($row['IDKNOWLEDGE_H']);
				$row['list_keywords'] = $this->getListKeywords($row['KEYWORDS']);
				$row['list_doc'] = $this->getListDoc($row['IDKNOWLEDGE_H']);
				$row['list_video'] = $this->getListVideo($row['IDKNOWLEDGE_H']);
				$row['list_subyek'] = $this->getListSubyek($row['IDKNOWLEDGE_H']);
				$row['list_anggota'] = $this->getListAnggotaTim($row['IDKNOWLEDGE_H']);
				$row['list_komentar'] = $this->getListKomentar($row['IDKNOWLEDGE_H']);
				$row['list_suka'] = $this->getListSuka($row['IDKNOWLEDGE_H']);
				$row['NUM_SUKA'] = $this->getNumSuka($row['IDKNOWLEDGE_H']);
				$row['NUM_KOMENTAR'] = $this->getNumKomentar($row['IDKNOWLEDGE_H']);
				
			}
		}
		// echo "<pre>";print_r($rows);die();
		return $rows;
    }
	

    function add($fields, $keywords, $peserta, $pemateri) {
        $fields['keywords'] = $keywords;
		$ret = dbInsert($this->table, $fields);
		if ($ret) {
			// $sql = "select {$this->id} from $this->table where createdby={$_SESSION[G_SESSION]['iduser']} and rownum=1 order by createdtime desc";
			$sql = "select k.$this->id, RAWTOHEX(k.$this->id) as insert_id, k.WAKTUPELAKSANAAN, k.JUDUL from (select $this->id,WAKTUPELAKSANAAN,JUDUL from $this->table where createdby={$_SESSION[G_SESSION]['iduser']} order by createdtime desc) k where rownum=1";
			$knowledge = dbGetRow($sql);
			$id_knowledge = $knowledge['IDKNOWLEDGE'];
			$insert_id = $knowledge['INSERT_ID'];

			// insert keywords
			$datakeywords = "{";
	        for ($j=0; $j < count($keywords) ; $j++) {
	        	$date = date("d/m/Y h:i:s");
	            $dataInsertKeywords = array(
	                'keyword'       => $keywords[$j],
	                'created_at'	=> $date

	            );
	            // echo "<pre>";print_r($dataInsertKeywords);echo "</pre>";
	            // die();
	            $checkinteger = (int)$keywords[$j];
	            if($checkinteger == 0){
	                $sql_exist = "select idkeywords from {$this->km}.keywords where keyword='".$keywords[$j]."'";
	            }
	            else{
	                $sql_exist = "select idkeywords from {$this->km}.keywords where idkeywords='".$keywords[$j]."'";
	            }
	            $exist_id = dbGetOne($sql_exist);
	            if($exist_id != null){
	                $keywords_id = $exist_id;
	            }
	            else{
		// 
	                $insertKeywords = dbQuery("INSERT INTO {$this->km}.KEYWORDS (KEYWORD, CREATED_AT) VALUES ('".$keywords[$j]."', TO_DATE('".$date."' , 'DD/MM/YY HH24:MI:SS'))");
	                $sql_id = "SELECT IDKEYWORDS FROM {$this->km}.KEYWORDS WHERE CREATED_AT=TO_TIMESTAMP('".$date."','dd-mm-yyyy hh24:mi:ss')";
	                $keywords_id = dbGetOne($sql_id);
	            }   
	            $datakeywords .= "\"".$j."\":\"".$keywords_id."\"";
	            if($j != count($keywords)-1){
	                $datakeywords .= ",";
	            }
	            $dataInsertKnowKey = array(
	                'IDKEYWORDS'    => $keywords_id,
	                'IDKNOWLEDGE'   => $insert_id
	            );
	            // $insert_knowledge_keywords = dbInsert('knowledge_keywords', $dataInsertKnowKey);
	            dbInsert("{$this->km}.knowledge_keywords", $dataInsertKnowKey);
	        }
	        $datakeywords .= "}";
	        $updatekeywords = array(
                'keywords' => $datakeywords
            );
            dbUpdate("{$this->km}.knowledge", $updatekeywords, "idknowledge='$insert_id'");


            // insert peserta dan notifikasi
            $this->load->model('Notification_model');
            $this->load->model('Km_pemateri_model');
            foreach ($peserta as $pes) {
            	
            	// $this->load->model('Notification_model');
            	// $CI =& get_instance();
             //    $jabatan_atasan = $CI->Notification_model->getAtasan();
            	$dataPeserta = array(
            		'IDUSER'		=> $pes,
            		'IDKNOWLEDGE'	=> $insert_id
            	);
            	dbInsert("{$this->km}.peserta_knowledge", $dataPeserta);

            	$dataNotif = array(
            		'iduser'			=> $pes,
            		'judul'				=> $knowledge['JUDUL'],
            		'idknowledge'		=> $knowledge['INSERT_ID'],
            		'waktupelaksanaan'	=> $knowledge['WAKTUPELAKSANAAN']
            	);
				$insertNotif = $this->Notification_model->add($dataNotif);
            }

            // insert pemateri
            foreach ($pemateri as $val) {
            	$dataInsert = array(
            		'IDPEMATERI'		=> $val['IDPEMATERI'] != "" ? $val['IDPEMATERI'] : '{{null}}',
            		'NAMAPEMATERI'		=> $val['NAMAPEMATERI'],
            		'JABATANPEMATERI'	=> $val['JABATANPEMATERI'],
            		'ISPEMATERIUTAMA'	=> $val['ISPEMATERIUTAMA'] != "" ? $val['ISPEMATERIUTAMA'] : '{{null}}',
            		'IDKNOWLEDGE'		=> $insert_id
            	);
            	// $insertKeywords = dbQuery("INSERT INTO KNOWLEDGE_PEMATERI (IDPEMATERI, NAMAPEMATERI, JABATANPEMATERI, ISPEMATERIUTAMA, IDKNOWLEDGE) VALUES (".$dataInsert['IDPEMATERI'].", '".$dataInsert['NAMAPEMATERI']."', '".$dataInsert['JABATANPEMATERI']."', ".$dataInsert['ISPEMATERIUTAMA']." , '".$dataInsert['IDKNOWLEDGE']."')");
            	// echo "<pre>";print_r($dataInsert);echo "<pre>";
				$insert = $this->Km_pemateri_model->add($dataInsert);
            }
            
			return $id_knowledge;
		}
		return false;
		// die();
	}
	
	function cobaModel($dataNotif){
		$this->load->model('Notification_model');
    	$dataNotif = array(
    		'iduser'			=> 1,
    		'judul'				=> 'TES JUDUL',
    		'idknowledge'		=> '123456765',
    		'waktupelaksanaan'	=> '30-DEC-18'
    	);
		$insertNotif = $this->Notification_model->add($dataNotif);
	}
	function updateSubyek($idknowledge, $list_subyek) {
		$sql = "delete from {$this->km}.kmsubyek where idknowledge='$idknowledge'";
		if (dbQuery($sql)) {
			foreach ($list_subyek as $k=>$idsubyek) {
				$sql = "insert into {$this->km}.kmsubyek (idknowledge, idsubyek) values ('$idknowledge', $idsubyek)";
				dbQuery($sql);
			}
		}
	}
	
	function updateAnggotaTim($idknowledge, $list_anggota) {
		$list_anggota = explode(',', $list_anggota);
		$sql = "delete from {$this->km}.tim where idknowledge='$idknowledge'";
		if (dbQuery($sql)) {
			foreach ($list_anggota as $iduser) {
				$sql = "insert into {$this->km}.tim (idknowledge, iduser) values ('$idknowledge', $iduser)";
				dbQuery($sql);
			}
		}
	}

	function getListAnggotaTim($idknowledge) {
		$sql = "select t.*, u.nama from {$this->km}.tim t join {$this->um}.users u on t.iduser=u.iduser where idknowledge='$idknowledge' order by u.nama";
		return dbGetRows($sql);
	}
	function getListKeywords($keywords_json){
		$keywords = json_decode($keywords_json,true);
        for ($i=0; $i < count($keywords) ; $i++) {
            $sql_kw = "select idkeywords,keyword from {$this->km}.keywords where idkeywords=$keywords[$i]";
            $word[$i] = dbGetRow($sql_kw);
            $param = array('KEYWORD');
            xHighlightHomeSearch($word[$i], $param);
        }
        return $word;
	}
	function getListJenisKM() {
		$sql = "select * from {$this->km}.jeniskm where is_active=1 order by idjeniskm";
		return dbGetRows($sql);
	}
	
	function getListSubyekKM() {
		$sql = "select * from {$this->km}.subyek order by nama";
		return dbGetRows($sql);
	}

	function getListSubyekKMPGD() {
		$sql = "select * from {$this->km}.subyek where idsubyek=1 or idsubyek=2 or idsubyek=4 or idsubyek=7 or idsubyek=8 or idsubyek=10 or idsubyek=22 or idsubyek=46 or idsubyek=21 or idsubyek=81 or idsubyek=82 or idsubyek=83 or idsubyek=84 order by nama ";
		return dbGetRows($sql);
	}

	function getListSubyekStr() {
		$sql = "select nama from {$this->km}.subyek order by nama";
		$rows = dbGetRows($sql);
		$arr = array();
		foreach ($rows as $row)
			$arr[] = '"' . $row['NAMA'] . '"';
		return implode(',', $arr);
	}
	
	function getListTahunKM() {
		$sql = "select distinct tahuntulis as tahun  from {$this->km}.knowledge order by tahuntulis";
		$rows = dbGetRows($sql);
		$tahun = array();
		foreach ($rows as $row) {
			if ($row['TAHUN'] && $row['TAHUN'] > 1900)
				$tahun[$row['TAHUN']] = $row['TAHUN'];
		}
		
		$sql = "select distinct to_char(waktupelaksanaan,'YYYY') as tahun  from {$this->km}.knowledge order by to_char(waktupelaksanaan,'YYYY')";
		$rows = dbGetRows($sql);
		foreach ($rows as $row) {
			if ($row['TAHUN'] && $row['TAHUN'] > 1900)
				$tahun[$row['TAHUN']] = $row['TAHUN'];
		}

		$sql = "select distinct to_char(createdtime,'YYYY') as tahun  from {$this->km}.knowledge order by to_char(createdtime,'YYYY')";
		$rows = dbGetRows($sql);
		foreach ($rows as $row) {
			if ($row['TAHUN'] && $row['TAHUN'] > 1900)
				$tahun[$row['TAHUN']] = $row['TAHUN'];
		}
		
		sort($tahun);

		return $tahun;
	}

	function nextNum($idjeniskm) {
		$tahun = date('Y');
		$sql = "select count(*) from $this->table where idjeniskm=$idjeniskm and to_char(createdtime,'yyyy') = '$tahun'";
		$num = dbGetOne($sql);
		return $num+1;
	}
	
	function addFoto($fields) {
		if (!dbInsert("{$this->km}.foto", $fields))
			return false;
		
		$sql = "select max(idfoto) from {$this->km}.foto where tiduser={$_SESSION[G_SESSION]['iduser']}";
		return dbGetOne($sql);
	}
	
	function deleteFoto($idknowledge, $idfoto) {
		$sql = "select 1 from {$this->km}.foto where idknowledge='$idknowledge' and idfoto=$idfoto";
		if (!dbGetOne($sql))
			return false;

		$sql = "delete from {$this->lp}.lampiranfotokm where idfoto=$idfoto";
		dbQuery($sql);

		$sql = "select namafile from {$this->km}.foto where idknowledge='$idknowledge' and idfoto=$idfoto";
		$namafile = dbGetOne($sql);
		
		$sql = "delete from {$this->km}.foto where idknowledge='$idknowledge' and idfoto=$idfoto";
		if (dbQuery($sql)) {
			xKMLog($idknowledge, 'delete photo: ' . $namafile);
			return true;
		}			

		return false;
	}
	
	function getListSuka($idknowledge) {
		$sql = "select distinct u.iduser, u.nama from {$this->km}.suka d join {$this->km}.knowledge k on d.idknowledge=k.idknowledge and k.isdelete=0 
				join {$this->um}.users u on u.iduser=d.iduser
				where k.isdelete=0 and k.idknowledge='$idknowledge'";
		return dbGetRows($sql);
	}
	
	function getListFoto($idknowledge) {
		$sql = "select * from {$this->km}.foto where idknowledge='$idknowledge'";
		return dbGetRows($sql);
	}

	function addDoc($fields) {
		$ret = dbInsert("{$this->km}.lampiran", $fields);
		$sql = "select max(idlampiran) from {$this->km}.lampiran where tiduser={$_SESSION[G_SESSION]['iduser']}";
		return dbGetOne($sql);
	}
	
	function deleteDoc($idknowledge, $idlampiran) {
		$sql = "select 1 from {$this->km}.lampiran where idknowledge='$idknowledge' and idlampiran=$idlampiran";
		if (!dbGetOne($sql))
			return false;
		
		$sql = "delete from {$this->lp}.lampirankm where idlampiran=$idlampiran";
		dbQuery($sql);
		
		$sql = "select namafile from {$this->km}.lampiran where idknowledge='$idknowledge' and idlampiran=$idlampiran";
		$namafile = dbGetOne($sql);

		$sql = "delete from {$this->km}.lampiran where idknowledge='$idknowledge' and idlampiran=$idlampiran";
		if (dbQuery($sql)) {
			xKMLog($idknowledge, 'delete doc: ' . $namafile);
			return true;
		}			

		return false;
	}
	
	function getListDoc($idknowledge) {
		$sql = "select * from {$this->km}.lampiran where idknowledge='$idknowledge'";
		return dbGetRows($sql);
	}

	function addVideo($fields) {
		if (!dbInsert("{$this->km}.video", $fields))
			return false;
		
		$sql = "select max(idvideo) from {$this->km}.video where tiduser={$_SESSION[G_SESSION]['iduser']}";
		return dbGetOne($sql);
	}
	
	function deleteVideo($idknowledge, $idvideo) {
		$sql = "select 1 from {$this->km}.video where idknowledge='$idknowledge' and idvideo=$idvideo";
		if (!dbGetOne($sql))
			return false;

		$sql = "select namafile from {$this->km}.video where idknowledge='$idknowledge' and idvideo=$idvideo";
		$namafile = dbGetOne($sql);
		
		$sql = "delete from {$this->km}.video where idknowledge='$idknowledge' and idvideo=$idvideo";
		if (dbQuery($sql)) {
			xKMLog($idknowledge, 'delete video: ' . $namafile);
			return true;
		}			

		return false;
	}

	function getListRef($filter)
	{
		$sql = "select RAWTOHEX(idknowledge) as idknowledge_h, judul from {$this->table} where idjeniskm=2 order by createdtime desc"; //ID JENIS KM = COP
		return dbGetRows($sql);
	}
	
	function getListVideo($idknowledge) {
		$sql = "select * from {$this->km}.video where idknowledge='$idknowledge'";
		return dbGetRows($sql);
	}

	function addKomentar($idknowledge, $komentar) {
		$record = array();
		$record['IDKNOWLEDGE'] =  $idknowledge;
		$record['KOMENTAR'] =  $komentar;
		$record['IDUSER'] =  $_SESSION[G_SESSION]['iduser'];
		$record['TIPADDRESS'] =  $_SERVER['REMOTE_ADDR'];
		if (!dbInsert("{$this->km}.diskusi", $record))
			return false;
		
		xKMLog($idknowledge, 'add comment: ' . $komentar);
		
		$sql = "select max(iddiskusi) from {$this->km}.diskusi where iduser={$_SESSION[G_SESSION]['iduser']}";
		return dbGetOne($sql);
	}
	
	function updateKomentar($idknowledge, $iddiskusi, $komentar) {
		$record = array();
		$record['KOMENTAR'] =  $komentar;
		$record['TIPADDRESS'] =  $_SERVER['REMOTE_ADDR'];
		
		if (!dbUpdate("{$this->km}.diskusi", $record, "idknowledge='$idknowledge' and iddiskusi=$iddiskusi"))
			return false;

		xKMLog($idknowledge, 'update comment: ' . $komentar);
		return true;
	}

	function deleteKomentar($idknowledge, $iddiskusi) {
		$record = array();
		$record['ISDELETE'] =  1;
		$record['TIPADDRESS'] =  $_SERVER['REMOTE_ADDR'];
		
		if (!dbUpdate("{$this->km}.diskusi", $record, "idknowledge='$idknowledge' and iddiskusi=$iddiskusi")) 
			return false;

		xKMLog($idknowledge, 'delete comment');
		return true;
	}

	function getListKomentar($idknowledge) {
		$sql = "select d.*, u.nama as namauser from {$this->km}.diskusi d join {$this->um}.users u on u.iduser=d.iduser
			where idknowledge='$idknowledge' and isdelete=0 ";
		return dbGetRows($sql);
	}

	function getKomentar($iddiskusi) {
		$sql = "select d.*, u.nama as namauser from {$this->km}.diskusi d join {$this->um}.users u on u.iduser=d.iduser
			where d.iddiskusi='$iddiskusi' and isdelete=0 ";
		return dbGetRow($sql);
	}
	
	function getListSubyek($idknowledge) {
		$sql = "select s.* from {$this->km}.kmsubyek k join {$this->km}.subyek s on k.idsubyek=s.idsubyek where k.idknowledge='$idknowledge'";
		$subyek = dbGetRows($sql);
		for ($i=0; $i < count($subyek) ; $i++) { 
			$subyek[$i]['NAMA'] = highlight_phrase($subyek[$i]['NAMA'], $_SESSION[G_SESSION]['cari_km'], '<span style="background-color:red;">', '</span>');
		}
		return $subyek;
	}
	
	function isNextExist() {
		$next_offset = ($this->home_limit * $_SESSION[G_SESSION]['km_page']) + 1;
		return $this->getList($this->home_limit, $next_offset, true);
	}
	
	function toggleSuka($idknowledge) {
		$iduser = $_SESSION[G_SESSION]['iduser'];
		$sql = "select 1 from {$this->km}.suka where idknowledge='$idknowledge' and iduser=$iduser";
		if (dbGetOne($sql)) {
			$sql = "delete from {$this->km}.suka where idknowledge='$idknowledge' and iduser=$iduser";
			if (dbQuery($sql)) {
				xKMLog($idknowledge, 'unlike');
				return 'off';
			}
			return 'on';
		}
		$record = array();
		$record['iduser'] = $iduser;
		$record['idknowledge'] = $idknowledge;
		$record['tipaddress'] = $_SERVER['REMOTE_ADDR'];
		if (dbInsert("{$this->km}.suka", $record)) {
			xKMLog($idknowledge, 'like');
			return 'on';
		}
		return 'off';		
	}

	function getNumSuka($idknowledge) {
		$sql = "select count(*) from {$this->km}.suka where idknowledge='$idknowledge'";
		return dbGetOne($sql);
	}
	
	function getNumKomentar($idknowledge) {
		$sql = "select count(*) from {$this->km}.diskusi where idknowledge='$idknowledge' and isdelete=0";
		return dbGetOne($sql);
	}
	
	function _isAllowView($knowledge) {
		return true;
	}
	
	function _isAllowEdit($knowledge, $iddiskusi=false) {
		if ($_SESSION[G_SESSION]['idrole'] == '1')
			return true;
		
		if ($knowledge['CREATEDBY'] == $_SESSION[G_SESSION]['iduser'])
			return true;
		
		return false;
	}

	function _isAllowDelete($knowledge, $iddiskusi=false) {
		if ($_SESSION[G_SESSION]['idrole'] == '1')
			return true;
		
		if ($knowledge['CREATEDBY'] == $_SESSION[G_SESSION]['iduser'])
			return true;
		
		return false;
	}
	
	function getNumKnowledgeSubmit($iduser) {
		$sql = "select count(*) from {$this->km}.knowledge where createdby='$iduser' and isdelete=0";
		return dbGetOne($sql);
	}

	function getNumKomentarSubmit($iduser) {
		$sql = "select count(*) from {$this->km}.diskusi d join {$this->km}.knowledge k on d.idknowledge=k.idknowledge and k.isdelete=0  where d.iduser='$iduser' and d.isdelete=0";
		return dbGetOne($sql);
	}

	function getNumNulisSubmit($iduser) {
		$sql = "select count(*) from {$this->km}.knowledge where pemateri='$iduser' and isdelete=0";
		return dbGetOne($sql);
	}

	function getNumAnggotaSubmit($iduser) {
		$sql = "select count(*) from {$this->km}.tim d join {$this->km}.knowledge k on d.idknowledge=k.idknowledge and k.isdelete=0  where d.iduser='$iduser'";
		return dbGetOne($sql);
	}
	
	function getStat($idknowledge) {
		$stat = array();
		$sql = "select count(*) from {$this->km}.lihat d join {$this->km}.knowledge k on d.idknowledge=k.idknowledge and k.isdelete=0 where k.isdelete=0";
		$stat['lihat'] = dbGetOne($sql);
		
		$sql = "select count(*) as num from {$this->km}.downloadlampiran d 
				join {$this->km}.lampiran f on f.idlampiran=d.idlampiran 
				join {$this->km}.knowledge k on f.idknowledge=k.idknowledge and k.isdelete=0
				where k.idknowledge='$idknowledge'";
		$stat['download'] = dbGetOne($sql);

		$sql = "select distinct u.iduser, u.nama from {$this->km}.suka d join {$this->km}.knowledge k on d.idknowledge=k.idknowledge and k.isdelete=0 
				join {$this->um}.users u on u.iduser=d.iduser
				where k.isdelete=0 and k.idknowledge='$idknowledge'";
		$stat['list_suka'] = dbGetRows($sql);

		$sql = "select * from (select l.*, u.nama, to_char(l.tinserttime, 'YYYY-MM-DD HH24:MI') as tinserttime_h from {$this->km}.kmlog l join {$this->um}.users u on l.iduser=u.iduser where idknowledge='$idknowledge' order by l.tinserttime desc) where rownum <= 100";
		$stat['list_log'] = dbGetRows($sql);
		
		return $stat;
	}
	
	function addCari($fields) {
	    //$this->config->item('debug_db');
	    $ret = dbInsert("{$this->km}.CARI", $fields);		
	}

}
