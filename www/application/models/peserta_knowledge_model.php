<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta_knowledge_model extends Base_Model {
    public $table = 'PESERTA_KNOWLEDGE';
    public $id = 'IDPESERTA_KNOWLEDGE';
	//public $sort = 'nvl(waktupelaksanaan,createdtime) desc, tahuntulis desc, createdtime desc';
	public $sort = 'IDPESERTA_KNOWLEDGE desc';
	public $filter = '';
	public $home_limit = 10;

    function Peserta_knowledge_model() {
        parent::__construct();
		$this->table = $this->km . '.'. $this->table;
    }

    function getRowsJoinUser($data='')
    {
		$sql = "select * from $this->table pk left join UM.USERS u on u.iduser=pk.iduser where 1=1";
		foreach ($data as $k=>$v) {
			$v = $this->db->escape($v);
			$sql .= " and {$k}=$v";
		}
		
        $row = dbGetRows($sql);
		
		return $row;
    }
}