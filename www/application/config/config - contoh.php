<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define(G_SESSION, 'PJB');
$config['app_title'] = 'PJB';

$config['use_ldap'] = false;
$config['use_ssl'] = false;
$config['use_apc'] = false;
$config['max_file_size'] = 10;
$config['id_admin_unit'] = 2;

#setting perpus
$config['perpus_url'] = "http://".$_SERVER['SERVER_NAME']."/pjb/www/perpus/index.php?page=home";//"http://km.pjbservices.com/perpus/index.php?page=home";
$config['digilib_url'] = "http://".$_SERVER['SERVER_NAME']."/pjb/www/digilib/index.php?page=functionx&s=logbykm";
$config['cronperpus'] = "http://".$_SERVER['SERVER_NAME']."/pjb/www/perpus/index.php";

# skema database
$config['UM'] = 'UM';
$config['KM'] = 'KM';
$config['LP'] = 'LP';

$protocol = 'http://';
if ($config['use_ssl'])
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://':'http://';

$config['base_url']	= '';

$config['index_page'] = 'index.php';

# debug_db: menampilkan semua query di halaman, matikan debug_db jika sudah production
$config['debug_db'] = true;

$config['allow_send_email'] = true;
$config['using_test_email'] = true; # semua recipient ke email_test

$config['email_protocol'] = 'smtp';

$config['upload_temp'] = '/var/www/pjb/www/temp';
$config['static_path'] = '/var/www/pjb/www/_static';

$config['uri_protocol']	= 'AUTO';
$config['url_suffix'] = '';
$config['language']	= 'id';
$config['charset'] = 'UTF-8';
$config['enable_hooks'] = TRUE;
$config['subclass_prefix'] = 'Base_';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-';
$config['allow_get_array']		= TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger']	= 'c';
$config['function_trigger']		= 'm';
$config['directory_trigger']	= 'd'; // experimental not currently in use

$config['log_threshold'] = 0;

$config['log_path'] = '';
$config['log_date_format'] = 'Y-m-d H:i:s';

$config['cache_path'] = '';

$config['encryption_key'] = 'ae8a7a587a5a3e5738025b3b43392ab6';

$config['sess_cookie_name']		= 'PJB';
$config['sess_expiration']		= 3600; // 1 jam = 3600 detik
$config['sess_expire_on_close']	= FALSE;
$config['sess_encrypt_cookie']	= TRUE;
$config['sess_use_database']	= FALSE;
$config['sess_table_name']		= 'ci_sessions';
$config['sess_match_ip']		= FALSE;
$config['sess_match_useragent']	= TRUE;
$config['sess_time_to_update']	= 300;

$config['cookie_prefix']	= "";
$config['cookie_domain']	= "";
$config['cookie_path']		= "/";
$config['cookie_secure']	= FALSE;

$config['global_xss_filtering'] = FALSE;



/*
|--------------------------------------------------------------------------
| Cross Site Request Forgery
|--------------------------------------------------------------------------
| Enables a CSRF cookie token to be set. When set to TRUE, token will be
| checked on a submitted form. If you are accepting user data, it is strongly
| recommended CSRF protection be enabled.
|
| 'csrf_token_name' = The token name
| 'csrf_cookie_name' = The cookie name
| 'csrf_expire' = The number in seconds the token should expire.
*/
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;


$config['compress_output'] = FALSE;

/*
|--------------------------------------------------------------------------
| Master Time Reference
|--------------------------------------------------------------------------
|
| Options are 'local' or 'gmt'.  This pref tells the system whether to use
| your server's local time as the master 'now' reference, or convert it to
| GMT.  See the 'date helper' page of the user guide for information
| regarding date handling.
|
*/
$config['time_reference'] = 'local';

$config['rewrite_short_tags'] = FALSE;

/*
|--------------------------------------------------------------------------
| Reverse Proxy IPs
|--------------------------------------------------------------------------
|
| If your server is behind a reverse proxy, you must whitelist the proxy IP
| addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
| header in order to properly identify the visitor's IP address.
| Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
|
*/
$config['proxy_ips'] = '';


/* End of file config.php */
/* Location: ./application/config/config.php */
