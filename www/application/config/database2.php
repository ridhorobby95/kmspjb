<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = "default";
$active_record = TRUE;

$db['default']['hostname'] = "localhost";//"192.168.3.95/dbperpusprd"; //"172.16.30.215/dbperpusprd";
$db['default']['username'] = "km";//"system";
$db['default']['password'] = "km";//"manager";
$db['default']['database'] = "";
$db['default']['dbdriver'] = "oci8";
$db['default']['dbprefix'] = "";
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = true;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = "";
$db['default']['char_set'] = "utf8";
$db['default']['dbcollat'] = "utf8_general_ci";

$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

// $db['ellipse']['hostname'] = "192.168.1.197/ellprd";
// $db['ellipse']['username'] = "mimseo";
// $db['ellipse']['password'] = "mims";
// $db['ellipse']['database'] = "ellprd";
// $db['ellipse']['dbdriver'] = "oci8";
// $db['ellipse']['dbprefix'] = "";
// $db['ellipse']['pconnect'] = FALSE;
// $db['ellipse']['db_debug'] = true;
// $db['ellipse']['cache_on'] = FALSE;
// $db['ellipse']['cachedir'] = "";
// $db['ellipse']['char_set'] = "utf8";
// $db['ellipse']['dbcollat'] = "utf8_general_ci";

