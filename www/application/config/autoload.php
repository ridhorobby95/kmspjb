<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$autoload['packages'] = array();

$autoload['libraries'] = array('session','view');

$autoload['helper'] = array('url','text', 'db', 'x');

$autoload['config'] = array();

$autoload['language'] = array('id');

$autoload['model'] = array();


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */