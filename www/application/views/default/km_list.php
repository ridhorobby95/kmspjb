<script src="<?= base_assets() ?>js/filtertable.js"></script>

<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class="panel panel-default">

		<div class="panel panel-body">
			<?= xPageTitle($page_title) ?>

			<?= $notification ?>
			
			<?php if ($pagination) { ?>
			<div class="row-fluid">
			<span class="pagination pagination-sm" ><?=$pagination?></span>
			</div>
			<?php } ?>
			
			<div class="input-group text-center">
				<input type="text" class="form-control input-sm" id="cari_km2" placeholder="Cari di knowledge management" value="<?= $_SESSION[G_SESSION]['cari_km'] ?>">
				<span class="input-group-btn">
					<button class="btn btn-sm btn-danger btn-remove-cari" type="button" style="display:<?= ($_SESSION[G_SESSION]['cari_km'])?'display':'none';?>"><i class="fa fa-remove"></i></button>
					<button class="btn btn-sm btn-primary" id="btn_cari_km2" type="button"><i class="fa fa-search"></i></button>
				</span>
			</div>
			<div style="font-size:smaller;padding-top:10px">
				<?php 
					$sess_check = 0;
					foreach ($_SESSION[G_SESSION]['filter_km'] as $sess) {
						if (!$sess) {
							$sess_check = 1;
							break;
						}
					}
				?>
				<?php foreach ($list_jenis as $jenis) { ?>
				<button type="button" class="btn btn-xs btn-link" onclick="toggleJenisKM('<?= $jenis['IDJENISKM'] ?>')" style="text-decoration:none"><i class="fa <?= ($_SESSION[G_SESSION]['filter_km'][$jenis['IDJENISKM']])?'fa-check-square-o':'fa-square-o' ?>" style="font-size:14px"></i> <?= $jenis['NAMA'] ?></button>
				<?php } ?>
				<?php if ($sess_check || $_SESSION[G_SESSION]['cari_km'] || $_SESSION[G_SESSION]['filter_unit'] || $_SESSION[G_SESSION][$ctl]['filter_string']  || $_SESSION[G_SESSION][$ctl]['filter_sort']) { ?>
				<span style="float:right">
				<?= xGetButton('reset') ?>
				</span>
				<?php } ?>
			</div>
			<br/><br/>
			
			<table class="table table-striped table-condensed" style="font-size:13px">
				<tr class="info">
					<th style="width:20px">No.</th>
					<?php foreach ($field_list as $field): ?>
					<th <?=xSetFilterTable($ctl, $field);?>><?= $field['label'];?> <?= $field['sort'];?></th>
					<?php endforeach; ?>
				</tr>
			<?php
			if (count($rows)) {
				$i=$offset;
				foreach ($rows as $row) {
					$idknowledge = rawurlencode($row['IDKNOWLEDGE']);
				$i++;
				
				$created = DateTime::createFromFormat( 'd-M-y h.i.s.u A', $row['CREATEDTIME'] );
				$created_f1 = strtotime($created->format('Y-m-d H:i:s'));
				$created_f2 = xTime2String($created_f1);
				
			?>
				<tr>
					<td><?= $i ?></td>
					<td><a href="<?= site_url("$ctl/detail/{$row['IDKNOWLEDGE_H']}"); ?>"><?= word_limiter($row['JUDUL'], 10); ?></a></td>
					<td><span class="label label<?= $row['IDJENISKM'] ?>"><?= $row['NAMAJENISKMSINGKAT']?></span></td>
					<td><?php if ($row['PEMATERI']) echo anchor(site_url('publ1c/profile/id?q='.xEncrypt($row['PEMATERI'])), $row['NAMAPEMATERI']); else echo $row['PEMATERILUAR']; ?></td>
					<td style="font-size:smaller"><?= $row['NAMAUNIT'] ?></td>
					<td style="text-align:right;font-size:smaller" title="<?= $created->format('Y-m-d H:i:s') ?>"><?= $created_f2 ?></td>
				</tr>
			<?php 
				}
			}
			?>
			
			<?php if (!count($rows)): ?>
				<tr><td colspan="<?=count($field_list)+1?>">Tidak ada data</td></tr>
			<?php endif; ?>
			
			</table>
		</div>

		<div class="panel-footer">
		<?= xFilterPaging($pagination, $filter_string, $num_start, $num_end, $num_all) ?>
		</div>
		
	</div>
</div>


<?= $this->view('default/_km_popup'); ?>

<?= $this->view('default/_km_js'); ?>

<script>
	$('#btn_cari_km2').click(function (e) {
		url = '<?= site_url('km/cari') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"q":$('#cari_km2').val()
					},
			success: function(ret){
				window.location.replace("<?= site_url('km/lst') ?>");
			}
		});
	})
	
	$('#cari_km2').keypress(function(e){
		var p = e.which;
		if(p==13){
			$('#btn_cari_km2').click();
		}
	})
	
	function toggleJenisKM(jenis) {
		$.ajax({
			type: "POST",
			url: "<?= site_url('publ1c/togglejeniskm2') ?>",
			data: {"jenis":jenis},
			success: function(ret){
				window.location.replace("<?= site_url('km/lst') ?>");
			}
		});
	}	
</script>
