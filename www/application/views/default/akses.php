<div class="container">
<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class=" panel panel-content">
	<?= xPageTitle('Silakan pilih hak akses anda') ?>
	<div class="panel-body" style="min-height:250px">
		<table class="table table-condensed table-striped">
		<?php
			$i = 0;
			foreach ($roles as $role):
			$i++;
		?>
			<tr>
				<td style="vertical-align:top" width="2%">
				<br/>
					<input type="radio" id="r<?=$role['IDROLE'].$i?>" name="rolejabatan" value="<?=$role['IDROLE']?>"
						<?php if ($_SESSION[G_SESSION]['idrole'] == $role['IDROLE'])
							echo "checked";
						?>
					onclick="this.form.submit()" />
				</td>
				<td style="padding-top:2px;vertical-align:top;padding-left:10px">
					<br/>
					<label for="r<?=$role['IDROLE'].$i?>" style="cursor:pointer">
						<strong><?=$role['NAMAROLE'];?></strong>
					</label>
					<br/>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

	<br/>
				
	<?php if (!count($roles)): ?>                
	<div class="label label-danger">Akun anda belum memiliki hak akses. Silakan hubungi administrator.</div>
	<?php endif; ?>
	</div>

	</div>	
	
	
	</div>
</div>