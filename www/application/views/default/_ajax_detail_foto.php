<div class="grid-item img_hover imagelay" id="foto-<?= $row['IDKNOWLEDGE_H'] ?>-<?= $foto['IDFOTO'] ?>" data-foto="<?= rawurlencode(xEncrypt($foto['IDFOTO'])) ?>">
	<?php if ($edited): ?><div class="hover"><button type="button" class="btn btn-xs btn-default btn_delete_foto" val="<?= $row['IDKNOWLEDGE_H'] ?>-<?= $foto['IDFOTO'] ?>"><i class="fa fa-remove"></i></button></div> <?php endif; ?>
	<a href="<?= site_url('publ1c/foto?q='. rawurlencode(xEncrypt($foto['IDFOTO']))) ?>" rel="prettyPhoto[<?= $row['IDKNOWLEDGE_H'] ?>]" >
	<img src="<?= site_url('publ1c/thumbnail?q='. rawurlencode(xEncrypt($foto['IDFOTO']))) ?>" class="img-thumbnail" title="<?= $foto['NAMAFILE'] ?>" alt="<?= $foto['NAMAFILE'] ?>" style="visibility:<?= $visible?'visible':'hidden'; ?>">
	</a>
</div>