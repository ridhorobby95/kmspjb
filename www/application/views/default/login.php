<link rel="stylesheet" href="<?= base_assets(); ?>css/login.css" />
<div class="header">
			<div class="container">
			<div class="col-md-2">
				<a href="<?= site_url('home') ?>"><img src="<?= base_assets() . 'images/logo-pjb-i.png' ?>" height="52px"></a>
			</div>
			<div class=" col-md-4" style="margin:11px auto">
			<div class="input-group text-center">
				<input type="text" class="form-control input-sm" id="cari_km_b" placeholder="Cari di knowledge management" value="<?= $_SESSION[G_SESSION]['cari_km'] ?>">
				<span class="input-group-btn">
					<button class="btn btn-sm btn-danger btn-remove-cari" type="button" style="height:30px; display:<?= ($_SESSION[G_SESSION]['cari_km'])?'display':'none';?>"><i class="fa fa-remove"></i></button>
					<button class="btn btn-sm btn-primary" id="btn_cari_km_b" type="button" style="height:30px;"><i class="fa fa-search"></i></button>
				</span>
			 </div>
			 </div>
</div>
    <div class="container">
		  <div class="box">
			<div class="row-fluid">
			  <!-- main right col -->
				<div class="column col-md-12 col-sm-12 col-xs-12" id="main">

				<!-- top nav -->

			</div>
		</div>
		</div>

		<div class="card card-container">
            <img align="center" id="profile-img" class="profile-img-card" src="<?= base_assets() . 'images/logo.png'?>" />
            <p align="center" id="profile-name" class="profile-name-card">Knowledege Management</p><br/>
			<?php if ($this->session->flashdata('error_login')) { ?>
			<div class="alert alert-danger" role="alert"><?= $this->session->flashdata('error_login') ?></div>
			<?php } ?>
            <form class="form-signin" method="post" action="<?= site_url('login') ?>">
				<input type="hidden" name="act_login" value="login" />
                <input type="text" id="inputEmail" name="usernamelogin" class="form-control" value="<?= $this->session->flashdata('username')?>" placeholder="Username" required autofocus><br/>
                <input type="password" id="inputPassword" name="passwordlogin" class="form-control" placeholder="Password" required>
                <div id="remember" class="checkbox">
                    <label style="color:#bbb">
                        <input type="checkbox" value="remember-me" style="color:#333"> Ingat saya
                    </label>
                </div>
                <button class="btn btn-md btn-primary btn-block btn-signin" type="submit">Login</button>
            </form><!-- /form -->
        </div><!-- /card-container -->

    </div><!-- /container -->
