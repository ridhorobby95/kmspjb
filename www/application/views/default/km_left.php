<div class="sidebar-nav-fixed pull-right affix">
<!-- menu -->
<div class="hidden-xs">
<img src="<?= base_assets() . 'images/km.png' ?>" style="max-width:80%;">
<div class="left-menu">
<p>Menu Utama</p>
<a href="<?= site_url('home') ?>"><span class="label label-primary"><i class="fa fa-home"></i></span>&nbsp; Home</a><br/>
<?php if (xIsLoggedIn()) { ?>
<a href="<?= site_url('profile') ?>"><span class="label label-primary"><i class="fa fa-user"></i></span>&nbsp; Profile</a><br/>
<a href="<?= site_url('km/notification') ?>"><span class="label label-primary"><i class="fa fa-bell"></i></span>&nbsp; Notifications <?php if ($jumlahNotif != 0): ?><span style="background-color: red; color: white; border-radius: 10%"><?php echo $jumlahNotif ?></span><?php endif ?></a><br/>
<?php } ?>
<?php if (xIsAuth('perpus:page=home','2')) { ?>
<a href="<?=$this->config->item('perpus_url');?>" target="_blank"><span class="label label-primary"><i class="fa fa-book"></i></span>&nbsp; SIM Perpustakaan</a><br/>
<?php } ?>
<a href="<?=$this->config->item('digilib_url');?>" target="_blank"><span class="label label-primary"><i class="fa fa-book"></i></span>&nbsp; Digital Library</a><br/>
<?php if (xIsAuth('administrasi')) { ?>
<a href="<?= site_url('administrasi') ?>"><span class="label label-primary"><i class="fa fa-database"></i></span>&nbsp; Administrasi</a><br/>
<?php } ?>
</div>

<div class="left-menu">
<p>Kategori KM</p>
<a href="<?= site_url('home/jenis/0') ?>"><span class="label label-primary"><i class="fa fa-globe"></i></span>&nbsp; Semua</a><br/>
<a href="<?= site_url('home/jenis/1') ?>"><span class="label label-success"><i class="fa fa-comment"></i></span>&nbsp; KSF</a><br/>
<a href="<?= site_url('home/jenis/2') ?>"><span class="label label-warning"><i class="fa fa-pencil"></i></span>&nbsp; Community of Practice</a><br/>
<a href="<?= site_url('home/jenis/3') ?>"><span class="label label-danger"><i class="fa fa-file"></i></span>&nbsp; Document Expert</a><br/>
<!-- <a href="<?= site_url('home/jenis/4') ?>"><span class="label label-info"><i class="fa fa-star"></i></span>&nbsp; Innovation</a><br/> -->
<a href="<?= site_url('home/jenis/5') ?>"><span class="label label-primary"><i class="fa fa-circle"></i></span>&nbsp; Peer Group Discussion</a><br/>
</div>

<?php //if (xIsLoggedIn()) { 
	$list_filter_unit = xGetUnits();
?>

<div class="left-menu">
<p>Filter Unit</p>
<select id="filter-unit" name="filter-unit" class="form-control" style="width:80%;font-size:11px;padding:2px;<?php if ($_SESSION[G_SESSION]['filter_unit']) { ?>border:2px solid red<?php } ?>">
	<option>-- Pilih Unit --</option>
	<?php foreach ($list_filter_unit as $unit) { ?>
	<option value="<?= $unit['KODEUNIT'] ?>" <?php if ($unit['KODEUNIT'] == $_SESSION[G_SESSION]['filter_unit']) { ?>selected<?php } ?>><?= $unit['NAMA'] ?></option>
	<?php } ?>
</select>
</div>

<div class="left-menu">
	<a href="amio.ptpjb.com" target="_blank"><h2>AMIO</h2></a>
	<?/*img src="<?= base_assets() . 'images/KLRA.png';?>" height="60"/*/?>
</div>

<script>
	$(document).on('change', '#filter-unit', function(e) {
		id = $(this).val();
		document.location.href = "<?= site_url('km/changeunit') ?>/"+id;
	});
</script>

<?php //} ?>

</div>
</div>
