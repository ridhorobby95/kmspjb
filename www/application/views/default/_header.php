<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="generator" content="Bootply" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->    
	<link rel="shortcut icon" href="<?= base_assets(); ?>images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/hamburger.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/styles.css" />
    <script src="<?= base_assets(); ?>js/jquery.js"></script> 
    <script src="<?= base_assets(); ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_assets(); ?>js/hamburger.js"></script> 
    <script src="<?= base_assets(); ?>js/main.js"></script> 
    <title><?= $this->config->item('app_title')  ?> <?php if (isset($page_title)) echo " - $page_title"; ?></title>
</head>
