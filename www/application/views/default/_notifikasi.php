<?php if (!empty($err_msg)): ?>
<div class="ret_error">
    <?php echo $err_msg; ?>
</div>
<?php endif; ?>

<?php if (!empty($suc_msg)): ?>
<div class="ret_success">
    <?php echo $suc_msg; ?>
</div>
<?php endif; ?>
