<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<script src="<?= base_assets(); ?>js/jquery-ui.min.js"></script> 
<link rel="stylesheet" href="<?= base_assets(); ?>css/validationEngine.jquery.css" type="text/css" />
<script src="<?= base_assets(); ?>js/jquery.validationEngine.js"></script>
<script src="<?= base_assets(); ?>js/jquery.validationEngine-en.js"></script>

	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">

		<div class="panel panel-default">
			
			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span style="float:right"><?= xShowButtonMode($method,$row[$id]); ?></span>
			<div style="clear:both"></div>
			</div>

			<div class="panel-body">
			
				<?= $notification ?>
			
			<input type="hidden" name="iduser" value="<?=$row['IDUSER']?>" />
				<table>
					<tr>
						<td style="width:100%">
							<table class="table table-condensed">
								<tr>
									<td class="info" width="170">Nama</td>
									<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,50,$edited,'validate[required]'); ?></td>
								</tr>   
								<tr>
									<td class="info">NID/Username</td>
									<td style="vertical-align:middle"><?= xHTMLTextBox('nid',$row['NID'],30,30,$edited,'validate[required]'); ?></td>
								</tr>   
								<tr>
									<td class="info">Email</td>
									<td style="vertical-align:middle"><?= xHTMLTextBox('email',$row['EMAIL'],100,50,$edited,'validate[required]'); ?></td>
								</tr>
								<tr>
									<td class="info">No Telp</td>
									<td style="vertical-align:middle"><?= xHTMLTextBox('telp',$row['TELP'],30,30,$edited,'validate[required]'); ?></td>
								</tr>
								<tr>
									<td class="info">Unit</td>
									<td style="vertical-align:middle"><?= xHTMLSelect('kodeunit',$a_unit,$row['KODEUNIT'],$edited,'','validate[required]'); ?></td>
								</tr>
								<tr>
									<td class="info">Extra User</td>
									<td class="checkboxes">
										<?php
											if ($edited): 
											if ($method === 'add')
												$row['ISEXTRAUSER'] = 1;
										?>
										<?= xHTMLCheckBox('isextrauser','1',$row['ISEXTRAUSER'],$edited); ?> <label for="isextrauser">Pengguna di luar Ellipse</label>
										<?php
											else:
											echo xStatusAktif($row['ISEXTRAUSER'], 'Extra User', 'User dari Ellipse');
											endif;
										?>
										
										<?php if ($edited): ?>
										<table id="table_password" class="table table-condensed" <?php if (!$row['ISEXTRAUSER']): ?>style="display:none"<?php endif; ?>>
										<tr>
											<td>Password</td>
											<td style="vertical-align:middle">
												<input type="text" name="password" id="password" value="" />
												<?php if ($method == 'edit') echo '<span style="font-size:10px">[kosongkan bila password tidak berubah]</span>';?>
											</td>
										</tr>
										</table>
										<?php endif; ?>
										
									</td>
								</tr>   
								<tr>
									<td class="info">Status</td>
									<td class="checkboxes">
										<?php
											if ($edited): 
											if ($method === 'add')
												$row['ISACTIVE'] = 1;
										?>
										<?= xHTMLCheckBox('isactive','1',$row['ISACTIVE'],$edited); ?> <label for="isactive">Aktif</label>
										<?php
											else:
											echo xStatusAktif($row['ISACTIVE']);
											endif;
										?>
									</td>
								</tr>   

								<?php if ($edited): ?>
								<!--
								<tr>
									<td class="info">Foto Profil</td>
									<td>
										<table>
											<tr>
												<td><input type="file" name="userfile" id="userfile" size="40" /></td>
												<td><span class="btn btn-xs btn-danger" onclick="$('#userfile').val('')">clear</span></td>
											</tr>
										</table>
									</td>
								</tr>
								-->
								<?php endif; ?>
							</table>
						</td>
						<td style="vertical-align:top;padding-left:25px;padding-right:25px">
							<img style="border:2px solid gray;padding:5px" src="<?= site_url('publ1c/profile?q='. xEncrypt($row['IDUSER'])) ?>&s=150" />
						</td>
					</tr>
				</table>
			</div>
				
			<?php if ($edited): ?>
			<div class="panel-footer">
				<div class="save-line">
				<?php echo xShowButtonMode('save',$row[$id]);?>
				</div>
				</div>
			<?php endif; ?>

			<div class="panel-body">

			<?php
				if (count($roles)) {
					echo '<br/>';
					echo '<h4>Hak akses</h4>';
					$i = 0;
					echo '<div>';
					echo '<table class="table table-condensed">';
					foreach ($roles as $role){
						echo "<tr>";
						echo '<td>' . anchor(site_url('role/detail/'.$role['IDROLE']), $role['NAMAROLE']) . '</td>';
						echo '<td><span class="btn btn-xs btn-danger delrole" val="' . $role['IDUSER'] .'-' . $role['IDROLE'] . '"><i class="glyphicon glyphicon-remove"></i></span><td>';
						echo '</tr>';
					}
					echo '</table></div>';
				}
			?>

			<?php if (!$edited): ?>
			<h4>Tambah hak akses <span class="btn btn-xs btn-primary" onclick="$('#tambah_div').toggle()">+</span></h4>
			<div id="tambah_div" style="display:none">
			<table class="table table-condensed">
				<tr>
					<td width="170">Role</td>
					<td><?= $role_control ?></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<span class="btn btn-sm btn-primary" id="btn_tambah">Tambah</span>
					</td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
			</table>
			</div>
			<?php endif; ?>
			
			</div>
			
		</div>
	</div>

<script>
	$(document).ready(function(){
		<?php if (!$edited): ?>
		$('#btn_tambah').click(function (e) {
			url = '<?= site_url('user/addrole') ?>';
			$.ajax({
				type: "POST",
				url: url,
				data: {"iduser":<?=$row['IDUSER']?>, "idrole":$('#idrole').val()},
				success: function(ret){
					window.location.replace("<?= site_url('user/detail/'.$row['IDUSER']) ?>");
				}
			});
		})
		
		$('.delrole').click(function (e) {
			if (!confirm('Hak akses akan dihapus. Dilanjutkan?'))
				return false;
			
			url = '<?= site_url('user/delrole') ?>';
			$.ajax({
				type: "POST",
				url: url,
				data: {"val":$(this).attr('val')},
				success: function(ret){
					window.location.replace("<?= site_url('user/detail/'.$row['IDUSER']) ?>");
				}
			});
		})
		<?php endif; ?>
	});
	
	<?php if ($edited):?>

		<?php if ($edited):?>
		$("#main_form").validationEngine();
		<?php endif; ?>

		$(document).on('click', '#isextrauser', function () { 
			if ($(this).is(':checked'))
				$('#table_password').show();
			else
				$('#table_password').hide();
		});		
	<?php endif; ?>
	

</script>
