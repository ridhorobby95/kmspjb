<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class="panel panel-default">
		<div class="panel-body" style="height:400px">
	<?= $notification ?>
	<table>
		<tr>
			<td style="min-width:300px;vertical-align:top">
			<table class="table table-condensed">
				<tr><td><strong><?= $profile['NAMA'] ?></strong></td></tr>
				<tr><td>NID: <?= $profile['NID'] ?></td></tr>
				<tr><td>Unit: <?= $a_unit['NAMA']; ?></td></tr>
				<?php if ($profile['IDUSER'] == $_SESSION[G_SESSION]['iduser']) { ?>
				<tr><td>Login Sebagai: <?= $_SESSION[G_SESSION]['namarole']; ?></td></tr>
				<tr>
					<td>
						<h5>Opsi</h5>
						Tampilan knowledge di home: <label style="margin-left:20px"><input type="radio" name="opsi_home" class="opsi" value="s" <?php if ($_SESSION[G_SESSION]['opsi_home'] == 's') echo 'checked' ?>> Standar</label> <label><input type="radio" name="opsi_home" class="opsi" value="r" <?php if ($_SESSION[G_SESSION]['opsi_home'] == 'r') echo 'checked' ?>> Ringkas</label>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td>
					<br/>
					<p><strong>Statistik</strong></p>
					Jumlah knowledge yang pernah ditulis: <?= (int) $num_tulis ?><br/>
					Jumlah knowledge yang pernah jadi anggota: <?= (int) $num_anggota ?><br/>
					Jumlah knowledge yang diupload: <?= (int) $num_knowledge ?><br/>
					Jumlah komentar: <?= (int) $num_komentar ?><br/>
					</td>
				</tr>
			</table>
			
			</td>
			<td style="padding-left:50px;vertical-align:top">
				<div class="img_hover ">
				<?php if ($profile['IDUSER'] == $_SESSION[G_SESSION]['iduser']) { ?>
				<div class="hover" style=""><button type="button" id="btn_upload_foto" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button></div>
				<?php } ?>
				<img id="profile_img" src="<?= site_url('publ1c/profile?q='. xEncrypt($profile['IDUSER'])) ?>&s=150" class="img-thumbnail">
				</div>
			</td>
		</tr>
	</table>
	<form id="form_profile" method="post" enctype="multipart/form-data"><input type="file" id="foto_profile" name="foto_profile" style="width:0px;" /></form>
	</div>
	</div>
</div>


<style>
.img_hover {
    display: inline;
    position: relative;
}
.img_hover .hover {
    display: none;
    position: absolute;
    right:0;
    z-index: 2;
}
</style>
<?php if ($profile['IDUSER'] == $_SESSION[G_SESSION]['iduser']) { ?>
<script>
$(function() {
    $(".img_hover").hover(
        function() {
            $(this).children("img").fadeTo(100, 0.85).end().children(".hover").show();
        },
        function() {
            $(this).children("img").fadeTo(100, 1).end().children(".hover").hide();
		}
	);
	
	$("#btn_upload_foto").click(
		function () { $("#foto_profile").click();}
	);
	
	$('input[type=file]').change(function() {
		if (!confirm('Foto profile akan diganti. Dilanjutkan?'))
			return false;
		$("#form_profile").submit();
	});	
	
	$('.opsi').click(function() {
		var opsi = $(this).attr('name');
		var val = $(this).val();
		url = '<?= site_url('profile/opsi') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: { "id":<?=$_SESSION[G_SESSION]['iduser']?>,"opsi":opsi, "val":val},
			success: function(ret){
				if (ret != 'ERROR')
					alert(ret);
				else 
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
			}
		});		
	});
	
});
</script>
<?php } ?>
