<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">

	<?= $notification ?>

	<span id="km-<?=$row['IDKNOWLEDGE_H']?>">
	<?= $this->view('default/_ajax_detail_km'); ?>
	</span>
	<?php if ($_SESSION[G_SESSION]['idrole'] == '1') { ?>
	<div class="panel panel-default panel-content">
		<div class="panel-heading">
			Statistik dan Log
			<a title="Expand" style="float:right" onclick="$('#log-km').toggle()"><i class="glyphicon glyphicon-eye-open"></i></a>
		</div>
		<div class="panel-body" id="log-km" style="display:none;font-size:smaller">
		Dilihat: <?= $stat['lihat'] ?><br/>
		Download: <?= $stat['download'] ?><br><br/>
		<strong>Suka</strong><br/>
		<div style="max-height:200px;overflow:auto">
		<table class="table table-condensed">
			<?php foreach ($stat['list_suka'] as $suka) { ?>
			<tr>
				<td style="width:30px"><a href="<?= site_url('publ1c/profile/id?q='. xEncrypt($suka['IDUSER'])) ?>"><img src="<?= site_url('publ1c/profile?q='. xEncrypt($suka['IDUSER'])) ?>&s=32" style="width:20px"></a></td><td><a href="<?= site_url('publ1c/profile/id?q='. xEncrypt($suka['IDUSER'])) ?>"><span style="font-size:10px"><?= $suka['NAMA'] ?></span></a></td>
			</tr>
			<?php } ?>
		</table>
		</div>
		<strong>Log</strong><br/>
		<div style="max-height:200px;overflow:auto;">
		<table class="table table-condensed">
			<?php foreach ($stat['list_log'] as $log) { ?>
			<tr>
			<td style="width:120px"><?= $log['TINSERTTIME_H'] ?></td>
			<td style="word-wrap:wrap"><?= $log['NAMA'] ?></td>
			<td><?= $log['KETERANGAN'] ?></td>
			</tr>
			<?php } ?>
		</table>
		</div>
		
		</div>
	</div>
	<?php } ?>
</div>

<?= $this->view('default/_km_popup'); ?>

<?= $this->view('default/_km_js'); ?>
