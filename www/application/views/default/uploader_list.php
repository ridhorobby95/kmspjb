<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="generator" content="Bootply" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->    
	<link rel="shortcut icon" href="<?= base_assets(); ?>images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/hamburger.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/styles.css" />
    <script src="<?= base_assets(); ?>js/jquery.js"></script> 
	<script src="<?= base_assets(); ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_assets(); ?>js/hamburger.js"></script> 
    <script src="<?= base_assets(); ?>js/main.js"></script> 
    <script src="<?= base_assets(); ?>js/jquery-1.12.4.js"></script>
  	<script src="<?= base_assets(); ?>js/jquery-ui.js"></script>
    <title><?= $this->config->item('app_title')  ?> <?php if (isset($page_title)) echo " - $page_title"; ?></title>

    <style type="text/css">
    	.mydiv {
		    position:fixed;
		    top: 10%;
		    left: 50%;
		    width:50em;
		    height:18em;
		    margin-left: -25em; /*set to a negative number 1/2 of your width*/
		}  
		.ui-progressbar {
		    position: relative;
		}

		.progress-label {
		    position: absolute;
		    left: 50%;
		    top: 4px;
		    font-weight: bold;
		    text-shadow: 1px 1px 0 #fff;
		} 

		.progressscan-label {
		    position: absolute;
		    left: 50%;
		    top: 4px;
		    font-weight: bold;
		    text-shadow: 1px 1px 0 #fff;
		}

		span.error{
		 	color: red;
		} 	
    </style>
</head>
	<body>
		<div class="header hidden-xs">
			<div class="container">
				<div class="col-md-2">
					<a>
						<img src="<?= base_assets(); ?>images/logo-pjb-i.png" height="52px">
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-7 mydiv">					
			<div class="scroll">
				<form id="form" method="post" enctype="multipart/form-data">
					<div class="panel panel-default panel-content panel3">
						<div class="panel-heading" style="margin-top:10px">
							<h3>Upload file</h3>
								<div id="progressbar" style="display: none;"><div class="progress-label">Loading...</div></div>
								<input type="file" id="file" name="file">
								<br>
								<button type="submit" class="btn btn-success" onclick="goUpload(); return false;">Upload</button>
							<span class="error"><i>* File yang dapat diupload adalah .zip</i></span>
						</div>
					</div>

					<div class="panel panel-default panel-content panel3">
						<div class="panel-heading" style="margin-top:10px">
							<h3>Scan folder</h3>
								<div id="progressbarscan" style="display: none;"><div class="progressscan-label">Loading...</div></div>
								<div id="scan">
									<span>Folder yang akan di scan : <?= $scan_path?></span><br>
									<span class="error"><i>* Pastikan data sudah ada di folder</i></span>	
								</div><br>
								<button type="submit" class="btn btn-success" onclick="goScan(); return false;">Scan</button>
						</div>
					</div>
				</form>

				<div class="panel panel-default panel-content panel3">
					<div class="panel-heading" style="margin-top:10px">
						<h4><b>Log</b></h4>
						<div id="log"></div>
					</div>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">

		var progressbar;

		$( function() {
		    progressbar 	= $( "#progressbar" ), progressLabel = $( ".progress-label" );
		    progressbarscan = $( "#progressbarscan" ), progressLabelScan = $( ".progressscan-label" );
		 
		    progressbar.progressbar({
		    	value 	: 0,
		      	change 	: function() {
		        	progressLabel.text( progressbar.progressbar( "value" ) + "%" );
		      	},
		      	complete: function() {
		        	progressLabel.text( "Complete!" );
		      	}
		    });

		    progressbarscan.progressbar({
		    	value 	: 0,
		      	change 	: function() {
		        	progressLabelScan.text( progressbarscan.progressbar( "value" ) + "%" );
		      	},
		      	complete: function() {
		        	progressLabelScan.text( "Complete!" );
		      	}
		    });

		    <?php if( isset($_SESSION[G_SESSION]['UPLOADER']) ) {
		    	echo "progress();\n";
		    }?>
		 });

		function progress() {
		    var val = progressbar.progressbar( "value" ) || 0;

		    $( '#file' ).hide();
   			$( '#progressbar' ).show();

			$.ajax({
				type 			: "POST",
		        url 			: "<?= base_url(). '/index.php/uploader'?>",
		        data 			: {process : true, request_time : Math.floor(Date.now() / 1000)},
		        dataType		: 'json',
		        success 		: function( data )
		        {
				  	progressbar.progressbar( "value", data.progress );

				  	if( data.status )
				  	{
				  		$.each( data.status, function(i, v)
				  		{
				  			if( v.tipe == 'info' )
				  				$('#log').append('<span>' + v.message + '</span>' );
				  			else if( v.tipe == 'error') 
				  				$('#log').append('<span class="error">' + v.message + '</span>' );
				  		})		
				  	}

				  	if( data.log )
				  		$('#log').append('<span>' + data.log + '</span>' );

				  	if ( data.progress < 99 || val < 99 ) 
				  	{
						progress();
					}
				}
			});
		}

		function goUpload() {
			var file = $('#file').val();

			if( ! file )
			{
				alert('File upload kosong, silahkan periksa kembali');
			}
			else
			{
		        var data = new FormData($('#form')[0]);

		        $.ajax({
		            type 			: "POST",
		            enctype 		: 'multipart/form-data',
		            url 			: "<?= base_url(). '/index.php/uploader'?>",
		            data 			: data,
		            dataType		: 'json',
		            processData 	: false,
		            contentType 	: false,
		            cache 			: false,
		            timeout 		: 600000,
		            beforeSend		: function ( xhr )
		            {
		            	$( '#log' ).empty();
		            },
		            success 		: function (data) 
		            {
   						if ( data.error_code == '100' ) 
   						{
   							alert( data.error_msg );
   						}
   						else if( data.error_code == '0' )
   						{
   							$( '#file' ).hide();
   							$( '#progressbar' ).show();
   							$( '#log' ).append( data.log );

   							progressbar.progressbar( "value", parseInt(data.progress) );

		      				progress();
   						}
		            }
		        });   
			}

			return false;
		}

		function scan() {
		    var val = progressbarscan.progressbar( "value" ) || 0;

		    $( '#scan' ).hide();
   			$( '#progressbarscan' ).show();

			$.ajax({
				type 			: "POST",
		        url 			: "<?= base_url(). '/index.php/uploader'?>",
		        data 			: {scan : true, request_time : Math.floor(Date.now() / 1000)},
		        dataType		: 'json',
		        success 		: function( data )
		        {
				  	progressbarscan.progressbar( "value", data.progress );

				  	if( data.status )
				  	{
				  		$.each( data.status, function(i, v)
				  		{
				  			if( v.tipe == 'info' )
				  				$('#log').append('<span>' + v.message + '</span>' );
				  			else if( v.tipe == 'error') 
				  				$('#log').append('<span class="error">' + v.message + '</span>' );
				  		})		
				  	}

				  	if( data.log )
				  		$('#log').append('<span>' + data.log + '</span>' );

				  	if ( data.progress < 99 || val < 99 ) 
				  	{
						scan();
					}
				}
			});
		}

		function goScan() 
		{
		    $.ajax({
		        type 			: "POST",
		        url 			: "<?= base_url(). '/index.php/uploader'?>",
		        data 			: {scan : true, scaninit : true},
		        dataType		: 'json',
		        timeout 		: 600000,
		        beforeSend		: function ( xhr )
		        {
		            $( '#log' ).empty();
		            $( '#scan' ).hide();
		        },
		        success 		: function (data) 
		        {
   					if ( data.error_code == '100' ) 
   					{
   						alert( data.error_msg );
   					}
   					else if( data.error_code == '0' )
   					{
   						$( '#progressbarscan' ).show();
   						$( '#log' ).append( data.log );

   						progressbarscan.progressbar( "value", data.progress );

		      			scan();
   					}
		        }
		    });   

			return false;
		}

	</script>
</html>