<div class="modal" id="foto_modal" tabindex="-1" role="dialog"  data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Foto</h4>
			</div>
			<div class="modal-body" style="padding:5px">
				<div id="foto_div" style="text-align:center"><i class="fa fa-spinner fa-spin"></i></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<!-- <link rel="stylesheet" href="<?= base_assets(); ?>gijgo/css/gijgo.css" /> -->
<!-- <script src="<?= base_assets(); ?>gijgo/js/gijgo.js"></script> -->

<!-- Modal -->
<div class="modal fade" id="filterTimeline" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content col-sm-12">
      <div class="modal-header col-sm-12">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Filter</h4>
      </div>
      <div class="modal-body col-sm-12">
        <form action="" method="post">
        	<div class="form-group col-sm-6">
        		<label>Tanggal mulai</label>

        		<input type="text" name="start_date" id="start_date"  class="form-control startDate" value="<?php echo (!empty($_SESSION[G_SESSION]['cari_km_startdate'])) ? $_SESSION[G_SESSION]['cari_km_startdate'] : '';?>"placeholder="" required="">
        	</div>
        	<div class="form-group col-sm-6">
        		<label>Tanggal Akhir</label>
        		<input type="text" name="end_date" id="end_date" class="form-control endDate" value="<?php echo (!empty($_SESSION[G_SESSION]['cari_km_enddate'])) ? $_SESSION[G_SESSION]['cari_km_enddate'] : '';?>" placeholder="" disabled required="">

        	</div>
        	<div class="form-group col-sm-12">
		        <button type="button" id="btn_cari_filter" class="btn btn-primary pull-right">Filter</button>
        		<button type="button" id="btn_hapus_filter" class="btn btn-default pull-right" data-dismiss="modal">Hapus Filter</button>
        	</div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

	$('#btn_cari_filter').click(function (e) {
		start = $('#start_date').val();
		end   = $('#end_date').val();
		url = '<?= site_url('km/addfilter') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"start":start,
					"end":end
					},
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	});

	$('#btn_hapus_filter').click(function (e) {
		start = $('#start_date').val();
		end   = $('#end_date').val();
		url = '<?= site_url('km/removefilter') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"start":start,
					"end":end
					},
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	})

	$(document).ready(function(){
		var dateToday = new Date();

	    $(".startDate").datepicker({
	        // uiLibrary: 'bootstrap',
	        // iconsLibrary: 'fontawesome',
	        // format: "mm/dd/yyyy",
	        dateFormat: "mm/dd/yy",
	    });

	    $(".startDate").change(function(){

	    	var dateVal = $(this).val();

	    	if(dateVal == ""){
			 $(".endDate").prop("disabled", true);

	         // $(".endDate").attr("readonly", true);
	         $(".endDate").siblings(".input-group-append").find(".btn-outline-secondary").css({"display" : "none"});
	    	}
	    	else{ 
	    	 $(".endDate").datepicker({
			        // uiLibrary: 'bootstrap',
			        // iconsLibrary: 'fontawesome',
			        // format: "mm/dd/yyyy",
			        dateFormat: "mm/dd/yy",
			        // minDate: new Date('2017-12-5'),
			        // minDate: $(".startDate").val(),
			        // minDate: function () {
		         //        return $('.startDate').val();
		         //        // console.log(min);
		         //    }
			 });
			 $(".endDate").prop("disabled", false);

	         // $(".endDate").attr("readonly", false); 
	         $(".endDate").siblings(".input-group-append").find(".btn-outline-secondary").css({"display" : "block"});
	    	}
	   	});
	});
</script>