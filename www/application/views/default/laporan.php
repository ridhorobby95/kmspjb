<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<link href="<?= base_assets(); ?>css/select2.min.css" rel="stylesheet" />
<script src="<?= base_assets(); ?>js/select2.min.js"></script>
<script src="<?= base_assets(); ?>js/jquery.tokeninput.js"></script> 
<script src="<?= base_assets(); ?>js/highchart/highcharts.js"></script> 
<script src="<?= base_assets(); ?>js/highchart/highcharts-3d.js"></script> 
<script src="<?= base_assets(); ?>js/highchart/modules/exporting.js"></script> 
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<script src="<?= base_assets(); ?>js/jquery-ui.min.js"></script> 

	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-14 col-md-10" style="padding:0;">
		<div class="panel panel-default">

			<?= xPageTitle($page_title) ?>
			
		<div class="panel-heading add-header" style="padding-bottom:5px;">
			<ul class="btn-km">
			<?php foreach ($list_jenis as $jenis) { 
				if ($jenis['IDJENISKM'] == 5)
					continue;
			?>
				<li>				
				<span class="btn-knowledge" id="btn_tab_<?= $jenis['IDJENISKM'] ?>" data-km="<?= $jenis['IDJENISKM'] ?>">
					<?php switch ($jenis['IDJENISKM']){
						case 1:
							echo '<span class="label label-success"><i class="fa fa-comment"></i></span>';
							break;
						case 2:
							echo '<span class="label label-warning"><i class="fa fa-pencil"></i></span>';
							break;
						case 3:
							echo '<span class="label label-danger"><i class="fa fa-file"></i></span>';
							break;
						case 4:
							echo '<span class="label label-info"><i class="fa fa-star"></i></span>';
							break;
					}?>&nbsp; 
					<span class="hidden-xs" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= $jenis['SINGKAT'] ?></strong></span>
					<span class="visible-xs nama" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= substr($jenis['SINGKAT'],0,3) ?> <?= strlen($jenis['SINGKAT'])>3?'..':'' ?></strong></span>
				</span>
				<div class="arrow"></div>
				</li>
			<?php } ?>
				<li>
				<span class="btn-knowledge" id="btn_tab_P" data-km="P"><span class="label label-default"><i class="fa fa-circle"></i></span> Pengunjung</span>
				<div class="arrow"></div>
				</li>
				<li>
				<span class="btn-knowledge" id="btn_tab_L" data-km="L"><span class="label label-default"><i class="fa fa-circle"></i></span> Like</span>
				<div class="arrow"></div>
				</li>
				<li>
				<span class="btn-knowledge" id="btn_tab_D" data-km="D"><span class="label label-default"><i class="fa fa-circle"></i></span> Download</span>
				<div class="arrow"></div>
				</li>
			</ul>
			<?php if (!$edited) { ?>
			<span class="fa fa-angle-up toggle-nav" id="navigation-toggle"></span>
			<?php } ?>
		</div>
		
		<div class="panel-body" id="filter-report" style="display:none">
			<div id="unit_rep" style="display:none">
			<p>
			Unit Kerja<br/>
			<select id="kodeunit" name="kodeunit[]" class="form-control input-md" style="border:0;width:200px">
				<?php foreach ($list_unit as $unit) { ?>
				<option value="<?= $unit['KODEUNIT'] ?>" <?php if (in_array($unit['KODEUNIT'], $_SESSION[G_SESSION]['report']['selected_unit'])) { ?>selected<?php } ?>><?= $unit['NAMA'] ?></option>
				<?php } ?>	
			</select>
			</p>
			</div>
			
			<div id="subyek_rep" style="display:none">
			<p>
			Stream<br/>
			<select id="idsubyek" name="idsubyek[]" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
				<?php foreach ($list_subyek as $subyek) { ?>
				<option value="<?= $subyek['IDSUBYEK'] ?>" <?php if (in_array($subyek['IDSUBYEK'], $_SESSION[G_SESSION]['report']['selected_subyek'])) { ?>selected<?php } ?>><?= $subyek['NAMA'] ?></option>
				<?php } ?>
			</select>
			</p>
			</div>

			<div id="tahun_rep" style="display:none">
			<p>
			Tahun<br/>
			<select id="tahun" name="tahun[]" class="form-control input-md" multiple="multiple" style="border:0;width:200px"> 
				<?php foreach ($list_tahun as $tahun) { ?>
				<option value="<?= $tahun ?>" <?php if (in_array($tahun, ($_SESSION[G_SESSION]['report']['selected_tahun'] ? $_SESSION[G_SESSION]['report']['selected_tahun'] : date('Y') ))) { ?>selected<?php } ?>><?= $tahun ?></option>
				<?php } ?>
			</select>
			</p>
			</div>
			
			<div id="bulan_rep" style="display:none">
			<p>
			Bulan<br/>
			<select id="bulan" name="bulan" class="form-control input-md" style="width:200px">
				<option value=''> -- semua bulan --</option>
				<?php for ($i=1; $i<=12; $i++) { ?>
				<option value="<?= $i ?>" <?php if ($i == $_SESSION[G_SESSION]['report']['selected_bulan']) { ?>selected<?php } ?>><?= xIndoMonth($i) ?></option>
				<?php } ?>
			</select>
			</p>
			</div>
			
			<div id="tanggal_rep" style="display:none">
			<p>
			Waktu
			
			<div class="row" style="margin-bottom:10px">
			<div class="col-md-1"><input checked="checked" type="radio" name="waktu" value="tahun" <?php if ($_SESSION[G_SESSION]['report']['selected_mode_waktu'] == 'tahun') { ?> checked <?php } ?>> Tahun</div>
			<div class="col-md-2">
				<select id="tahunwaktu" name="tahunwaktu[]" multiple="multiple" class="form-control input-md">
					<option></option>
					<?php foreach ($list_tahun as $tahun) { ?>
					<option value="<?= $tahun ?>" <?php if (in_array($tahun, $_SESSION[G_SESSION]['report']['selected_tahunwaktu'])) { ?>selected<?php } ?>><?= $tahun ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1"><input type="checkbox" name="with_bulan" id="with_bulan" <?php if ($_SESSION[G_SESSION]['report']['selected_mode_waktu'] == 'tahun' && isset($_SESSION[G_SESSION]['report']['use_bulanwaktu'])) { ?> checked <?php } ?>> Bulan</div>
			<div class="col-md-2">
				<select id="bulanwaktu" name="bulanwaktu[]" multiple="multiple" class="form-control input-md" <?php if ($_SESSION[G_SESSION]['report']['selected_mode_waktu'] != 'tahun' || empty($_SESSION[G_SESSION]['report']['use_bulanwaktu'])) { ?> disabled <?php } ?>>
					<option></option>
					<?php for ($i=1; $i<=12; $i++) { ?>
					<option value="<?= $i ?>" <?php if (in_array($i,$_SESSION[G_SESSION]['report']['selected_bulanwaktu'])) { ?>selected<?php } ?>><?= xIndoMonth($i) ?></option>
					<?php } ?>
				</select>
			</div>
			</div>
			
			<div class="row">
			<div class="col-md-1"><input type="radio" name="waktu" value="range" <?php if ($_SESSION[G_SESSION]['report']['selected_mode_waktu'] == 'range') { ?> checked <?php } ?>> Tanggal</div>
			<div class="col-md-2 col-sm-2"><input placeholder="tanggal mulai" type="text" id="tanggal1" name="tanggal1" class="form-control input-md" value="<?= $_SESSION[G_SESSION]['report']['tanggal1'] ?>"></div>
			<div class="col-md-1 col-sm-1"> Sampai </div>
			<div class="col-md-2 col-sm-2"><input placeholder="tanggal selesai" type="text" id="tanggal2" name="tanggal2" class="form-control input-md" value="<?= $_SESSION[G_SESSION]['report']['tanggal2'] ?>"></div>
			</div>
			
			</p>
			</div>

			<br/>
			
			<input type="hidden" name="act_laporan" id="act_laporan" value="1" />
			<input type="hidden" name="idjeniskm" id="idjeniskm" />
			<button class="btn btn-primary" onclick="$('#act_laporan').val('html')">Report HTML</button>
			<button class="btn btn-primary" onclick="$('#act_laporan').val('excel')">Report Excel</button>
			<button class="btn btn-primary" onclick="$('#act_laporan').val('chart')">Diagram</button>
			
			<hr/>
			
		</div>

		<?php if ($_GET['q'] == 'submit' && $_GET['a'] == 'html' && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('1','2','3','4')) ) { ?>	
		<div class="panel-body" style="overflow-x:scroll;">
			<div>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#total" aria-controls="total" role="tab" data-toggle="tab">Total</a></li>
				    <li role="presentation"><a href="#staf" aria-controls="staf" role="tab" data-toggle="tab">Staf</a></li>
				    <li role="presentation"><a href="#struktural" aria-controls="struktural" role="tab" data-toggle="tab">Struktural</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="total">
						<h4>REKAP JUMLAH PEMATERI & PESERTA <?= $jeniskm ?> <?= $tahun_str ?> <?= $bulan_str ?> <?= $unit_str ?></h4>
						<table class="table table-condensed table-striped">
						<tr>
							<th>No.</th>
							<th>JUDUL</th>
							<th>STREAM</th>
							<th>TIPE</th>
							<th>UNIT</th>
							<th>TRIWULAN</th>
							<th>BULAN PELAKSANAAN</th>
							<th>TANGGAL PELAKSANAAN</th>
							<th>BULAN PENGIRIMAN DATA</th>
							<th>TANGGAL PENGIRIMAN DATA</th>
							<th>PEMATERI</th>
							<th>NID</th>
							<th>JABATAN</th>
							<th>ACTION PLAN</th>
							<th>PROGRESS</th>
							<th>KETERANGAN</th>
						</tr>
						<?php 
							$j=0;
							foreach ($report_rows as $row) {
								$j++;
						?>
							<tr>
								<td><?= $j ?></td>
								<td><?= $row['JUDUL']?></td>
								<td><?php
									$sbyk = array();
									if(!empty($kmsubyek[$row['IDKNOWLEDGE_H']])){
										foreach($kmsubyek[$row['IDKNOWLEDGE_H']] as $s){
											$sbyk[] = $s;
										}
										echo implode(", ",$sbyk);
									}?>
								</td>
								<td><?= $row['SJKM']?></td>
								<td><?= $row['NAMAUNIT'] ? $row['NAMAUNIT'] : $row['KODEUNIT'] ?></td>
								<td><?= xTriwulan((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= xIndoMonth((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= $row['WAKTUPELAKSANAAN_D']?></td>
								<td><?= xIndoMonth((int)$row['CREATEDTIME_B'])?></td>
								<td><?= $row['CREATEDTIME_D']?></td>
								<td><?= $row['PEMATERI']?$row['NAMAPEMATERI']:$row['PEMATERILUAR']?></td>
								<td><?= $row['NID']?$row['NID']:'-'?></td>
								<td><?= $row['JABATANPEMATERI']?$row['JABATANPEMATERI']:'-'?></td>
								<td><?= $row['ACTIONPLAN']?></td>
								<td><?= $row['PROGRESS']?></td>
								<td><?= $row['ABSTRAK']?></td>
							</tr>
						<?php } ?>
						</table>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="staf">
						<h4>(STAFF) REKAP JUMLAH PEMATERI & PESERTA <?= $jeniskm ?> <?= $tahun_str ?> <?= $bulan_str ?> <?= $unit_str ?></h4>
						<table class="table table-condensed table-striped">
						<tr>
							<th>No.</th>
							<th>JUDUL</th>
							<th>STREAM</th>
							<th>TIPE</th>
							<th>UNIT</th>
							<th>TRIWULAN</th>
							<th>BULAN PELAKSANAAN</th>
							<th>TANGGAL PELAKSANAAN</th>
							<th>BULAN PENGIRIMAN DATA</th>
							<th>TANGGAL PENGIRIMAN DATA</th>
							<th>PEMATERI</th>
							<th>NID</th>
							<th>JABATAN</th>
							<th>ACTION PLAN</th>
							<th>PROGRESS</th>
							<th>KETERANGAN</th>
						</tr>
						<?php 
							$j=0;
							foreach ($report_rows_staf as $row) {
								$j++;
						?>
							<tr>
								<td><?= $j ?></td>
								<td><?= $row['JUDUL']?></td>
								<td><?php
									$sbyk = array();
									if(!empty($kmsubyek[$row['IDKNOWLEDGE_H']])){
										foreach($kmsubyek[$row['IDKNOWLEDGE_H']] as $s){
											$sbyk[] = $s;
										}
										echo implode(", ",$sbyk);
									}?>
								</td>
								<td><?= $row['SJKM']?></td>
								<td><?= $row['NAMAUNIT'] ? $row['NAMAUNIT'] : $row['KODEUNIT'] ?></td>
								<td><?= xTriwulan((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= xIndoMonth((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= $row['WAKTUPELAKSANAAN_D']?></td>
								<td><?= xIndoMonth((int)$row['CREATEDTIME_B'])?></td>
								<td><?= $row['CREATEDTIME_D']?></td>
								<td><?= $row['PEMATERI']?$row['NAMAPEMATERI']:$row['PEMATERILUAR']?></td>
								<td><?= $row['NID']?$row['NID']:'-'?></td>
								<td><?= $row['JABATANPEMATERI']?$row['JABATANPEMATERI']:'-'?></td>
								<td><?= $row['ACTIONPLAN']?></td>
								<td><?= $row['PROGRESS']?></td>
								<td><?= $row['ABSTRAK']?></td>
							</tr>
						<?php } ?>
						</table>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="struktural">
						<h4>(STRUKTURAL) REKAP JUMLAH PEMATERI & PESERTA <?= $jeniskm ?> <?= $tahun_str ?> <?= $bulan_str ?> <?= $unit_str ?></h4>
						<table class="table table-condensed table-striped">
						<tr>
							<th>No.</th>
							<th>JUDUL</th>
							<th>STREAM</th>
							<th>TIPE</th>
							<th>UNIT</th>
							<th>TRIWULAN</th>
							<th>BULAN PELAKSANAAN</th>
							<th>TANGGAL PELAKSANAAN</th>
							<th>BULAN PENGIRIMAN DATA</th>
							<th>TANGGAL PENGIRIMAN DATA</th>
							<th>PEMATERI</th>
							<th>NID</th>
							<th>JABATAN</th>
							<th>ACTION PLAN</th>
							<th>PROGRESS</th>
							<th>KETERANGAN</th>
						</tr>
						<?php 
							$j=0;
							foreach ($report_rows_struktural as $row) {
								$j++;
						?>
							<tr>
								<td><?= $j ?></td>
								<td><?= $row['JUDUL']?></td>
								<td><?php
									$sbyk = array();
									if(!empty($kmsubyek[$row['IDKNOWLEDGE_H']])){
										foreach($kmsubyek[$row['IDKNOWLEDGE_H']] as $s){
											$sbyk[] = $s;
										}
										echo implode(", ",$sbyk);
									}?>
								</td>
								<td><?= $row['SJKM']?></td>
								<td><?= $row['NAMAUNIT'] ? $row['NAMAUNIT'] : $row['KODEUNIT'] ?></td>
								<td><?= xTriwulan((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= xIndoMonth((int)$row['WAKTUPELAKSANAAN_B'])?></td>
								<td><?= $row['WAKTUPELAKSANAAN_D']?></td>
								<td><?= xIndoMonth((int)$row['CREATEDTIME_B'])?></td>
								<td><?= $row['CREATEDTIME_D']?></td>
								<td><?= $row['PEMATERI']?$row['NAMAPEMATERI']:$row['PEMATERILUAR']?></td>
								<td><?= $row['NID']?$row['NID']:'-'?></td>
								<td><?= $row['JABATANPEMATERI']?$row['JABATANPEMATERI']:'-'?></td>
								<td><?= $row['ACTIONPLAN']?></td>
								<td><?= $row['PROGRESS']?></td>
								<td><?= $row['ABSTRAK']?></td>
							</tr>
						<?php } ?>
						</table>
				    </div>
				</div>

			</div>				
		</div>
		<?php } else if ($_GET['q'] == 'submit' && $_GET['a'] == 'html' && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('P','L','D')) ) { ?>
		<div class="panel-body">			
			<h4><?= $title_chart ?> <?= $tanggal_str ?></h4>
			<table class="table table-condensed table-striped">
			<tr>
				<th style="width:5%">No.</th>
				<th>UNIT</th>
				<th style="width:10%">JUMLAH</th>
			</tr>
			<?php 
				$j=0;
				foreach ($report_rows as $row) {
					$j++;
			?>
				<tr>
					<td><?= $j ?></td>
					<td><?= $row['NAMA']?></td>
					<td><?= xFormatNumber($row['JUMLAH']) ?></td>
				</tr>
				<?php if(isset($row['users'])){ ?>
					<?php foreach ($row['users'] as $key => $value): ?>
						<tr>
							<td style="background-color:#f1f1f1"></td>
							<td style="background-color:#f1f1f1"><?= $value['NID'] ?> - <?= $value['NAMA'] ?></td>
							<td style="background-color:#f1f1f1"><?= xFormatNumber($value['JUMLAH']) ?></td>
						</tr>
					<?php endforeach ?>
				<?php } ?>
			<?php } ?>
			</table>						
		</div>
		<?php } ?>

		<br/><br/>
		<hr/>
		
		<?php 
		if ($_GET['q'] == 'submit' && $_GET['a'] == 'chart' && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('1','2','3','4'))) { ?>
		<div class="panel-body">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#total" aria-controls="total" role="tab" data-toggle="tab">Total</a></li>
			    <li role="presentation"><a href="#staf" aria-controls="staf" role="tab" data-toggle="tab">Staf</a></li>
			    <li role="presentation"><a href="#struktural" aria-controls="struktural" role="tab" data-toggle="tab">Struktural</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="total">
					<div id="pie_by_subyek" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php if ($_SESSION[G_SESSION]['report']['selected_jenis'] == '1') { ?>
					<div id="pie_by_jabatan" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php } ?>
					<div id="peserta_line" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_subyek" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_unit" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_direktorat" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_subdit" style=" width:60%; max-height: 400px;margin-top:50px"></div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="staf">
					<div id="pie_by_subyek_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php if ($_SESSION[G_SESSION]['report']['selected_jenis'] == '1') { ?>
					<div id="pie_by_jabatan_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php } ?>
					<div id="peserta_line_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_subyek_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_unit_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_direktorat_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_subdit_staf" style=" width:60%; max-height: 400px;margin-top:50px"></div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="struktural">
					<div id="pie_by_subyek_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php if ($_SESSION[G_SESSION]['report']['selected_jenis'] == '1') { ?>
					<div id="pie_by_jabatan_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<?php } ?>
					<div id="peserta_line_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_subyek_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="jumlah_per_unit_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_direktorat_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
					<div id="pemateri_per_subdit_struktural" style=" width:60%; max-height: 400px;margin-top:50px"></div>
			    </div>
			</div>
		</div>
		<?php } else if ($_GET['q'] == 'submit' && $_GET['a'] == 'chart' && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('P','L','D'))) { ?>
		<div class="panel-body">
		<div id="jumlah_other_pie" style="max-height: 400px;margin-top:50px"></div>
		<div id="jumlah_other_bar" style="max-height: 400px;margin-top:50px"></div>
		</div>
		<?php } ?>
		
		</div>
	</div>

<?php //$this->view('default/_km_js'); ?>

<script>

	$(document).ready(function() {
		<?php if ($_GET['q'] == 'submit'): ?>
		$('#btn_tab_<?= $_SESSION[G_SESSION]['report']['selected_jenis']?>').click();
		<?php endif; ?>
	});

	$('.btn-knowledge').click(function (e) {
		jeniskm = $(this).data('km');
		arrow = $(this).parent().find('div.arrow');
		$('#idjeniskm').val(jeniskm);
		
		toggleKM(jeniskm);
		
		if ($('.btn-knowledge').hasClass('active')) {
			$('.btn-knowledge').removeClass('active');
			$('.btn-knowledge').parent().find('div.arrow').hide();
		}
		$(this).addClass('active');
		arrow.show();
	})

	function toggleKM(idjeniskm) {
		$('#idjeniskm').val(idjeniskm);
		
		$('#filter-report').show();
		$('#unit_rep').hide();
		$('#subyek_rep').hide();
		$('#tahun_rep').hide();
		$('#bulan_rep').hide();
		$('#tanggal_rep').hide();
		if (idjeniskm == '1' || idjeniskm == '2') {
			$('#unit_rep').show();
			$('#subyek_rep').show();
			$('#tahun_rep').show();
			$('#bulan_rep').show();
		}
		if (idjeniskm == '4' ) {
			$('#unit_rep').show();
		}
		if (idjeniskm == '3' || idjeniskm == '4') {
			$('#tahun_rep').show();
		}
		
		if (idjeniskm == 'P' || idjeniskm == 'L' || idjeniskm == 'D') {
			$('#subyek_rep').show();
			$('#tanggal_rep').show();
		}
		
	}

<?php if ($_GET['q'] == 'submit' && $_GET['a'] == 'chart'  && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('1','2','3','4')) ) { ?>	
	$(function () {
	    $('#pie_by_subyek').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
			credits: {
				enabled: false
			},
	        title: {
	            text: '<?= $jeniskm ?> PER BIDANG <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name}'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Persentase',
	            data: [
					<?php foreach ($tot_stat_subyek as $stat) { ?>
	                ['<?= $stat['NAMASUBYEK'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
					<?php } ?>
	            ]
	        }]
	    });
	    $('#pie_by_subyek_staf').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
			credits: {
				enabled: false
			},
	        title: {
	            text: '(STAFF) <?= $jeniskm ?> PER BIDANG <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name}'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Persentase',
	            data: [
					<?php foreach ($tot_stat_subyek_staf as $stat) { ?>
	                ['<?= $stat['NAMASUBYEK'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
					<?php } ?>
	            ]
	        }]
	    });
	    $('#pie_by_subyek_struktural').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
			credits: {
				enabled: false
			},
	        title: {
	            text: '(STRUKTURAL) <?= $jeniskm ?> PER BIDANG <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name}'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Persentase',
	            data: [
					<?php foreach ($tot_stat_subyek_struktural as $stat) { ?>
	                ['<?= $stat['NAMASUBYEK'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
					<?php } ?>
	            ]
	        }]
	    });

		<?php if ($_SESSION[G_SESSION]['report']['selected_jenis'] == '1') { ?>
		    $('#pie_by_jabatan').highcharts({
		        chart: {
		            type: 'pie',
		            options3d: {
		                enabled: true,
		                alpha: 45,
		                beta: 0
		            }
		        },
				credits: {
					enabled: false
				},
		        title: {
		            text: '<?= $jeniskm ?> PER JABATAN <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                depth: 35,
		                dataLabels: {
		                    enabled: true,
		                    format: '{point.name}'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Persentase',
		            data: [
						<?php foreach ($tot_stat_jabatan as $stat) { ?>
		                ['<?= $stat['NAMAJABATAN'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
						<?php } ?>
		            ]
		        }]
		    });
		    $('#pie_by_jabatan_staf').highcharts({
		        chart: {
		            type: 'pie',
		            options3d: {
		                enabled: true,
		                alpha: 45,
		                beta: 0
		            }
		        },
				credits: {
					enabled: false
				},
		        title: {
		            text: '<?= $jeniskm ?> PER JABATAN <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                depth: 35,
		                dataLabels: {
		                    enabled: true,
		                    format: '{point.name}'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Persentase',
		            data: [
						<?php foreach ($tot_stat_jabatan_staf as $stat) { ?>
		                ['<?= $stat['NAMAJABATAN'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
						<?php } ?>
		            ]
		        }]
		    });
		    $('#pie_by_jabatan_struktural').highcharts({
		        chart: {
		            type: 'pie',
		            options3d: {
		                enabled: true,
		                alpha: 45,
		                beta: 0
		            }
		        },
				credits: {
					enabled: false
				},
		        title: {
		            text: '<?= $jeniskm ?> PER JABATAN <?= $tahun_str ?> <br/> <?= $unit_str ?> <br/> <?= $subyek_str ?>' 
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                depth: 35,
		                dataLabels: {
		                    enabled: true,
		                    format: '{point.name}'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Persentase',
		            data: [
						<?php foreach ($tot_stat_jabatan_struktural as $stat) { ?>
		                ['<?= $stat['NAMAJABATAN'] ?>: <?= $stat['SUM'] ?>',   <?= $stat['SUM']*0.04 ?>],
						<?php } ?>
		            ]
		        }]
		    });
		<?php } ?>
	
	    $('#peserta_line').highcharts({
	        title: {
	            text: 'JUMLAH PESERTA <?= $jeniskm ?> <?= $tahun_str ?>', 
	            x: -20 //center
	        },
			credits: {
				enabled: false
			},
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	            x: -20
	        },
	 		credits: {
				enabled: false
			},
	        xAxis: {
	            categories: [<?= implode(',', $peserta_line_x) ?>]
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah Peserta'
	            },
				min:0,
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valueSuffix: ''
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: '<?= $jeniskm ?>',
	            data: [<?= implode(',', $peserta_line_y) ?>]
	        }]
	    });	
	
	    $('#peserta_line_staf').highcharts({
	        title: {
	            text: '(STAFF) JUMLAH PESERTA <?= $jeniskm ?> <?= $tahun_str ?>', 
	            x: -20 //center
	        },
			credits: {
				enabled: false
			},
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	            x: -20
	        },
	 		credits: {
				enabled: false
			},
	        xAxis: {
	            categories: [<?= implode(',', $peserta_line_x_staf) ?>]
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah Peserta'
	            },
				min:0,
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valueSuffix: ''
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: '<?= $jeniskm ?>',
	            data: [<?= implode(',', $peserta_line_y_staf) ?>]
	        }]
	    });	
	
	    $('#peserta_line_struktural').highcharts({
	        title: {
	            text: '(STRUKTURAL) JUMLAH PESERTA <?= $jeniskm ?> <?= $tahun_str ?>', 
	            x: -20 //center
	        },
			credits: {
				enabled: false
			},
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	            x: -20
	        },
	 		credits: {
				enabled: false
			},
	        xAxis: {
	            categories: [<?= implode(',', $peserta_line_x_struktural) ?>]
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah Peserta'
	            },
				min:0,
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            valueSuffix: ''
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        series: [{
	            name: '<?= $jeniskm ?>',
	            data: [<?= implode(',', $peserta_line_y_struktural) ?>]
	        }]
	    });	
	
	    $('#jumlah_per_subyek').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: 'JUMLAH <?= $jeniskm ?> PER BIDANG <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($subyek_bar as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	
	    $('#jumlah_per_subyek_staf').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: 'JUMLAH <?= $jeniskm ?> PER BIDANG <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($subyek_bar_staf as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	
	    $('#jumlah_per_subyek_struktural').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: 'JUMLAH <?= $jeniskm ?> PER BIDANG <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($subyek_bar_struktural as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });		
	
	    $('#jumlah_per_unit').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: 'JUMLAH <?= $jeniskm ?> PER UNIT <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($unit_bar as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	
	
	    $('#jumlah_per_unit_staf').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: '(STAFF) JUMLAH <?= $jeniskm ?> PER UNIT <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($unit_bar_staf as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	
	
	    $('#jumlah_per_unit_struktural').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: '(STRUKTURAL) JUMLAH <?= $jeniskm ?> PER UNIT <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?> <?= $subyek_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Bidang",
	            colorByPoint: true,
	            data: [
				<?php foreach ($unit_bar_struktural as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	
	
	});
<?php } else if ($_GET['q'] == 'submit' && $_GET['a'] == 'chart'  && in_array($_SESSION[G_SESSION]['report']['selected_jenis'], array('P','L','D')) ) { ?>	
	$(function () {


	    $('#jumlah_other_pie').highcharts({
	        chart: {
	            type: 'pie',
	            options3d: {
	                enabled: true,
	                alpha: 45,
	                beta: 0
	            }
	        },
			credits: {
				enabled: false
			},
	        title: {
	            text: '<?= $title_chart ?> PER UNIT <?= $tahun_str ?>', 
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                depth: 35,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.name}'
	                }
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: 'Persentase',
	            data: [
					<?php foreach ($jumlah_other as $k=>$v) { ?>
	                ['<?= $k ?>: <?= $v['jumlah'] ?>',   <?= $v['jumlah']*0.04 ?>],
					<?php } ?>
	            ]
	        }]
	    });
	
	    $('#jumlah_other_bar').highcharts({
	        chart: {
	            type: 'column'
	        },
			credits: {
				enabled: false
			},
	        title: {
	             text: '<?= $title_chart ?> PER UNIT <?= $tahun_str ?>', 
	        },
	        subtitle: {
	            text: '<?= $unit_str ?>',
	        },
	        xAxis: {
	            type: 'category'
	        },
	        yAxis: {
	            title: {
	                text: 'Jumlah'
	            }

	        },
	        legend: {
	            enabled: false
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y}'
	                }
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
	        },

	        series: [{
	            name: "Hit",
	            colorByPoint: true,
	            data: [
				<?php foreach ($jumlah_other as $k=>$v) { ?>
				{
	                name: "<?= $k ?>",
	                y: <?= $v['jumlah'] ?>
	            },
				<?php } ?> 
				]
	        }]
	        
	    });	

	});
<?php } ?>
</script>

<script>
	$("#idsubyek").select2();
	$("#tahun").select2();
	$("#tahunwaktu").select2();
	$("#bulanwaktu").select2();
	$("#kodeunit").select2({placeholder: "Pilih unit kerja"});		
	$("#tanggal1").datepicker(
		{
			dateFormat:'dd-mm-yy',
			onClose: function () {this.focus();this.blur()}
		}
	);
	$("#tanggal2").datepicker(
		{
			dateFormat:'dd-mm-yy',
			onClose: function () {this.focus();this.blur()}
		}
	);
	$('#with_bulan').click(function() {
		if ($(this).prop('checked')) {
			$('#bulanwaktu').prop('disabled',false);
		} else 
			$('#bulanwaktu').prop('disabled',true);
	});
</script>
