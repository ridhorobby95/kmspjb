	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">

		<div class="panel panel-default">
			
			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span style="float:right"><?= xGetButton('lst'); ?></span>
			<div style="clear:both"></div>
			</div>

			<div class="panel-body">
			
				<?= $notification ?>
			
			<input type="hidden" name="idjabatan" value="<?=$row['IDJABATAN']?>" />
			<table class="table table-condensed">
				<tr>
					<td class="info" width="170">Kode Jabatan</td>
					<td style="vertical-align:middle"><?= xHTMLTextBox('kodejabatan',$row['KODEJABATAN'],30,30,$edited,'validate[required]'); ?></td>
				</tr>   
				<tr>
					<td class="info">Nama Jabatan</td>
					<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,50,$edited,'validate[required]'); ?></td>
				</tr>   

			</table>
			</div>
				
			<?php if ($edited): ?>
			<div class="panel-footer">
				<div class="save-line">
				<?php echo xShowButtonMode('save',$row[$id]);?>
				</div>
				</div>
			<?php endif; ?>


			
		</div>
	</div>

<script>
	$(document).ready(function(){
		<?php if ($edited):?>
		$("#main_form").validationEngine();
		<?php endif; ?>
	});

</script>