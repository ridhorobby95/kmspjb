<script src="<?= base_assets() ?>js/filtertable.js"></script>

	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">
		<div class="panel panel-default">

			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span class="pull-left">
			<?php //echo xHTMLSelect('idmodul',$modulControl,$_SESSION[G_SESSION]['IDMODUL'],true,'form-control','',true); ?>
			</span>
			<span class="pull-right"><?= xGetButton('add'); ?></span>
			<div style="clear:both"></div>
			</div>
			
			<div class="panel-body">

			<table class="table table-striped table-condensed">
				<thead>
				<tr class="info">
					<th style="width:20px">No.</th>
					<?php foreach ($field_list as $field): ?>
					<th <?=xSetFilterTable($ctl, $field);?>><?= $field['label'];?> <?= $field['sort'];?></th>
					<?php endforeach; ?>
					<th style="width:150px">Aksi</th>
				</tr>
				</thead>
				
				<?php if (!$rows || !count($rows)): ?>
					<tr><td colspan="<?=count($field_list)+2?>">Tidak ada data</td></tr>
				<?php else: ?>
					<?php
						$i=$offset;
						foreach ($rows as $row) :
						$i++;
					?>
						<tr class="<?=($i%2===1)?'odd ':''?> <?=(!$row['ISACTIVE'])?'not_active':''?>" >
							<td><?=$i?></td>
							<td>
								<?=str_repeat('&nbsp;',($row['TINGKAT']-1)*5)?>
								<a href="<?= site_url("$ctl/detail/{$row['IDMENU']}"); ?>"><?= $row['NAMA']; ?></a>
							</td>
							<td><?= $row['NAMAFILE']; ?></td>
							<td><?= $row['NAMAITEM']; ?></td>
							<td>
								<?php
									if ($row['ITEMACCESS'] == 'C')
										echo 'Create';
									elseif ($row['ITEMACCESS'] == 'R')
										echo 'Read';
									elseif ($row['ITEMACCESS'] == 'U')
										echo 'Update';
									elseif ($row['ITEMACCESS'] == 'D')
										echo 'Delete';
								?>
							</td>
							<td>
								<?php if ($c_update) : ?>
									<a class="btn btn-xs btn-default" href="<?php echo site_url("$ctl/urut/dir/down/id/{$row[$id]}")?>"><i class="glyphicon glyphicon-chevron-down"></i></a>
									<a class="btn btn-xs btn-default" href="<?php echo site_url("$ctl/urut/dir/up/id/{$row[$id]}")?>"><i class="glyphicon glyphicon-chevron-up"></i></a>
									&nbsp;
									<?php echo xEditList(site_url("$ctl/edit/{$row[$id]}")); ?>
								<?php endif; ?>
								<?php if ($c_delete) {echo xDeleteList($row[$id], $row['NAMA']);} ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>

			</table>
			</div>
			
		</div>
	</div>

<script>
	function deleteList(id, desc) {
		if (confirm('<?php echo lang('are_you_sure_delete') ?> ('+desc+') ? '))
			location.href='<?php echo site_url("$ctl/delete") ?>/' + id ;
	}
	
	$('#idmodul').change(function() {
		location.href='<?php echo site_url("$ctl/modul") ?>/' + $(this).val() ;
	});
</script>
