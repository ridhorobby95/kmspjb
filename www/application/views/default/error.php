	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	
	<div class="col-sm-12 col-md-8" style="padding:0;height:400px">
		<div class="panel panel-default">
			<?php echo xPageTitle($page_title) ?>

			
			<div class="panel-body">
				<a href="<?= site_url('home')?>"><span class="btn btn-xs btn-info"><i class="glyphicon glyphicon-home"></i> <?= lang('home') ?></span></a>
				<span class="btn btn-xs btn-info" onclick="history.back()"><i class="glyphicon glyphicon-chevron-left"></i> <?= lang('kembali') ?></span>
				
			
			<h5 class="alert alert-danger">

			<?php
				if ($message)
					echo $message;
				else
					echo lang('no_data_no_right');
			?>

			
			</h5>
		</div>
	</div>