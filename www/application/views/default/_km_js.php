<script src="<?= base_assets(); ?>js/readmore.min.js"></script> 
<script src="<?= base_assets(); ?>js/masonry.pkgd.min.js"></script>
<script src="<?= base_assets(); ?>js/jquery.prettyPhoto.js"></script>
<link rel="stylesheet" href="<?= base_assets(); ?>css/prettyPhoto.css"/>

<style>
	.listOfPosts{
		list-style-type:none;
		margin:3px;
		margin-left:15px;
		padding:0px;
	}

	.post {
		display:block;
		margin-right:15px;
		margin-top:3px;
	}

	.avatarColumn {
		text-align:center;
		float:left;
		margin-right:2px;
	}

	.avatarColumn a:link, .avatarColumn .authorName a:link {
		padding:2px;
		text-align:center;
		text-decoration:none;
	}

	.postDetails, .categorizationDetails, .actionsNavBar {
		list-style-type:none;   
	}

	.postDetails li {
		display:block;
	}

	.categorizationDetails li, .actionsNavBar li{
		display:inline;
		float:left;
	}

	.actionsNavBar li a {
		padding:2px;
	}

	.wcid {
		font-weight:bold;
		color:black;
	}

	.category {
		background-color:red;
		color:white;
		font-weight:bold;
		margin-left:1px;
		margin-right:1px;
	}
	.clear {
		clear:both;
	}
	.img_hover {
		display: inline;
		position: relative;
	}
	.img_hover .hover {
		display: none;
		position: absolute;
		right:0;
		z-index: 2;
	}

	/* Example tokeninput style #1: Token vertical list*/
	ul.token-input-list {
		overflow: hidden; 
		height: auto !important; 
		height: 1%;
		border: 1px solid #ddd;
		cursor: text;
		font-size: 12px;
		font-family: Verdana;
		z-index: 999;
		margin: 0;
		padding: 0;
		background-color: #fff;
		list-style-type: none;
		clear: left;
	}

	ul.token-input-list li {
		list-style-type: none;
	}

	ul.token-input-list li input {
		border: 0;
		width: 100%;
		padding: 3px 8px;
		background-color: white;
		-webkit-appearance: caret;
	}

	li.token-input-token {
		overflow: hidden; 
		height: auto !important; 
		height: 1%;
		margin: 3px;
		padding: 3px 5px;
		background-color: #f5f5f5;
		border: 1px solid #D9D9D9;
		color: #000;
		font-weight: bold;
		cursor: default;
		display: block;
	}

	li.token-input-token p {
		float: left;
		padding: 0;
		margin: 0;
	}

	li.token-input-token span {
		float: right;
		color: #777;
		cursor: pointer;
	}

	li.token-input-selected-token {
		background-color: #08844e;
		color: #fff;
	}

	li.token-input-selected-token span {
		color: #bbb;
	}

	div.token-input-dropdown {
		position: absolute;
		max-width: 400px;
		background-color: #fff;
		overflow: hidden;
		border-left: 1px solid #ccc;
		border-right: 1px solid #ccc;
		border-bottom: 1px solid #ccc;
		cursor: default;
		font-size: 12px;
		font-family: Verdana;
		z-index: 1;
	}

	div.token-input-dropdown p {
		margin: 0;
		padding: 5px;
		font-weight: bold;
		color: #777;
	}

	div.token-input-dropdown ul {
		margin: 0;
		padding: 0;
	}

	div.token-input-dropdown ul li {
		background-color: #fff;
		padding: 3px;
		list-style-type: none;
	}

	div.token-input-dropdown ul li.token-input-dropdown-item {
		background-color: #fafafa;
	}

	div.token-input-dropdown ul li.token-input-dropdown-item2 {
		background-color: #fff;
	}

	div.token-input-dropdown ul li em {
		font-weight: bold;
		font-style: normal;
	}

	div.token-input-dropdown ul li.token-input-selected-dropdown-item {
		background-color: #d0efa0;
	}
	.imagelay {
		position:relative;
		width:100px;
		height:100px;
		cursor:pointer;
	}
	.imagelay img {
		width:100%;
		vertical-align:top;
	}
	.imagelay:after, .imagelay:before {
		position:absolute;
		opacity:0;
		transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}
	.imagelay:after {
		width:100%; height:100%;
		top:0; left:0;
		background:rgba(0,0,0,0.6);
	}
	.imagelay:before {
		content: attr(data-content);
		width:100%;
		color:#fff;
		z-index:1;
		bottom:0;
		padding:4px 10px;
		text-align:center;
		background:#034489;
		box-sizing:border-box;
		-moz-box-sizing:border-box;
	}
	.imagelay:hover:after, .imagelay:hover:before {
		opacity:1;
	}
	.grid-item { width: 100px; }
	.grid-item2 { width: 70px; }
	
</style>

<script>

 $(document).ready(function(){
	$('.img-thumbnail').css('visibility','visible'); 
 });

<?php if (xIsLoggedIn()) { ?>
	function subPost(id,e){
        var text = $('input#subpost-'+id).val();
        e.which = e.which || e.keyCode;
        if(e.keyCode == 13 && text) {
            $.ajax({
                url : '<?= site_url('km/komentar/add') ?>',
                type: 'post',
                cache: false,
                data: {subpost:text,postid:id},
                success: function(respon){
					if (respon != 'ERROR') {
						$('#divposts-' + id).show();
						$('#posts-' + id).append(respon);
						$('#subpost-'+id).val('');
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}		
					reloadStat(id);
                }
            });
        }
        return false;    
    }
	
    function postAction(act,rel){
        $.ajax({
            url : '<?= current_url()?>',
            type: 'post',
            data:{act:act,postid:rel},
            success:function(respon){
                if(respon){
                    var obj = jQuery.parseJSON(respon);
                    $('span#like-'+rel).text(obj.postlike);    
                    $('span#unlike-'+rel).text(obj.postunlike);
                }    
            }
        });
        return false;    
    }	

	$(document).on('mouseover','.post', function (e) {
		$(this).find('.edit-post').css('display', 'block');
	});
	
	$(document).on('mouseout','.post', function (e) {
		$(this).find('.edit-post').css('display', 'none');
	});
	
	$(document).on('click', '.edit-button-post', function(e) {
		$('#edit-'+$(this).attr('val')).toggle();
		$('#detail-'+$(this).attr('val')).toggle();
	});
	
	$(document).on('click','.delete-button-post', function(e) {
		if (confirm('Anda akan menghapus komentar anda. Dilanjutkan?')) {
			var id = $(this).attr('val');
			var idk = $(this).attr('idk');
            $.ajax({
                url : '<?= site_url('km/komentar/delete') ?>',
                type: 'post',
                cache: false,
                data: {postid:id},
                success: function(respon){
					if (respon != 'ERROR') {
						$('#post-'+id).hide();
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
					reloadStat(idk);
                }
            });
		}
	});

	$(document).on('keypress','.edit-text', function(e) {
		var text = $(this).val();
		var id = $(this).attr('val');
        e.which = e.which || e.keyCode;
        if(e.keyCode == 13 && text) {
            $.ajax({
                url : '<?= site_url('km/komentar/edit') ?>',
                type: 'post',
                cache: false,
                data: {subpost:text,postid:id},
                success: function(respon){
					if (respon != 'ERROR') {
						$('#edit-'+id).hide();
						$('#detail-'+id).html(respon);
						$('#detail-'+id).show();
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
                }
            });
        }
	});
	
	$(document).on('click', '.exp-col-post', function(){
		var id = $(this).attr('id-km');		
		$.ajax({
			url : '<?= site_url('km/toggledetail') ?>',
			type: 'post',
			cache: false,
			data: {"id":id},
			success: function(respon){
				if (respon != 'ERROR') {
					reloadDetail(id, respon);
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}		
			}
		});
	}); 
	
	$(document).on('click', '.refresh-post', function(){
		var id = $(this).attr('id-km');
		$.ajax({
			url : '<?= site_url('km/refresh') ?>',
			type: 'post',
			cache: false,
			data: {"id":id},
			success: function(respon){
				if (respon != 'ERROR') {
					reloadDetail(id, respon);
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}		
			}
		});			
	}); 

	$(document).on('click', '.suka', function() {
		var id = $(this).attr('val');
		$.ajax({
			url : '<?= site_url('km/suka') ?>',
			type: 'post',
			cache: false,
			data: {postid:id},
			success: function(respon){
				if (respon != 'ERROR') {
					if (respon == 'on') {
						$('#suka-'+id).removeClass('btn-default');
						$('#suka-'+id).addClass('btn-primary');
					}
					else {
						$('#suka-'+id).removeClass('btn-primary');
						$('#suka-'+id).addClass('btn-default');
					}
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}
				reloadStat(id);
			}
		});
	});
	
	function reloadStat(id) {
		$.ajax({
			url : '<?= site_url('km/reloadstat') ?>',
			type: 'post',
			cache: false,
			data: {"id":id},
			success: function(respon){
				if (respon != 'ERROR') {
					$('#stat-'+id).html(respon);
				}
			}
		});
	}

	$(document).on('click', '.btn_delete_foto',	function () { 
		if (!confirm('Anda akan menghapus lampiran foto ini. Dilanjutkan?'))
			return false;
		
		var id = $(this).attr('val');
		url = '<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {"act":"delete_foto", "id":id, },
			success: function(ret){
				if (ret == '0') {
					$('.grid-foto').masonry('remove',$('#foto-' + id));
					$('.grid-foto').masonry('layout');
				}
				else
					alert(ret);
			}
		});		

		return false;
	});		

	$(document).on('click', '.btn_delete_doc',	function () { 
		if (!confirm('Anda akan menghapus lampiran file ini. Dilanjutkan?'))
			return false;
		
		var id = $(this).attr('val');
		url = '<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"act":"delete_doc", 
					"id":id, 
					},
			success: function(ret){
				if (ret == '0') {
					$('.grid-doc').masonry('remove',$('#doc-' + id));
					$('.grid-doc').masonry('layout');
				}
				else
					alert(ret);
			}
		});		

		return false;
	});		
		
	$(document).on('click', '.btn_delete_video',	function () { 
		if (!confirm('Anda akan menghapus video ini. Dilanjutkan?'))
			return false;
		
		var id = $(this).attr('val');
		url = '<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"act":"delete_video", 
					"id":id, 
					},
			success: function(ret){
				if (ret == '0') {
					location.reload();
				}
				else
					alert(ret);
			}
		});		

		return false;
	});		

	function reloadDetail(id, respon) {
		$('#km-' + id).html(respon);
		$('.grid-foto').masonry({itemSelector: '.grid-item',gutter: 3}).masonry();
		$('.grid-doc').masonry({itemSelector: '.grid-item',gutter: 3}).masonry();
		$('.grid-video').masonry({itemSelector: '.grid-item',gutter: 3}).masonry();
		$("a[rel^='prettyPhoto']").prettyPhoto();
		$('.article-more').readmore({
		  <?php if ($is_condensed) { ?>
		  collapsedHeight: 40,
		  <?php } ?>
		});
		$('.img-thumbnail').css('visibility','visible');
	}
<?php } ?>

	function hoverFoto() {
		$(".img_hover").hover(
			function() {
				$(this).children("img").fadeTo(100, 0.85).end().children(".hover").show();
			},
			function() {
				$(this).children("img").fadeTo(100, 1).end().children(".hover").hide();
			}
		);
	}

	$(function () {
		hoverFoto();
		$('.grid-foto').masonry({itemSelector: '.grid-item',gutter: 3});
		$('.grid-doc').masonry({itemSelector: '.grid-item',gutter: 3});
		$('.grid-video').masonry({itemSelector: '.grid-item',gutter: 3});
		$('[data-toggle="tooltip"]').tooltip();
		$("a[rel^='prettyPhoto']").prettyPhoto({
				social_tools:false,
				deeplinking:false,
			});
		$('.article-more').readmore({
		  speed: 75,
		  collapsedHeight: 40,
		  lessLink: '<a href="#">Read less</a>'
		});
	})
	
	$("#tambah").click(function () {
		$header = $(this);
		$content = $header.next();
		$content.slideDown();
		$('#navigation-toggle').show();
		$header.hide();
	});
	$("#navigation-toggle").click(function(){
		$content = $(this).parent().parent();
		$header = $content.prev();
		$(this).hide();
		$content.slideUp();
		$header.show();
	});
	
</script>