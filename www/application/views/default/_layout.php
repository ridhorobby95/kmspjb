<?php $header->render(); ?>

<body>
<!-- <h1 style="text-align:center">Sedang maintenance, mohon ditunggu.. </h1> -->
<div id="container">
<!--The Hamburger Button in the Header-->
    <header class="visible-xs row-fluid">
		<div class="col-xs-1" style="padding:0">
			<div id="hamburger">
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
		<div class="col-xs-9">
			<div class="input-group text-center">
				<input type="text" class="form-control input-sm" id="cari_km_a" placeholder="Cari di knowledge management" value="<?= $_SESSION[G_SESSION]['cari_km'] ?>">
				<span class="input-group-btn">
					<button class="btn btn-sm btn-danger btn-remove-cari" style="display:<?= ($_SESSION[G_SESSION]['cari_km'])?'display':'none';?>"><i class="fa fa-remove"></i></button>
					<button class="btn btn-sm btn-primary" id="btn_cari_km_a" type="button"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</div>
		<div class="col-xs-2  col-sm-2" style="padding:0" align="right">
			<?php if (xIsLoggedIn()) { ?>
			<ul class="notif">
				  <li class="dropdown"> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <img src="<?= site_url('publ1c/profile?q='. xEncrypt($_SESSION[G_SESSION]['iduser'])) ?>&s=50" alt="" class="user-img"> <span class=" fa fa-angle-down"></span> </a>
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
					  <li><a href="<?= site_url('profile') ?>"> Profile</a> </li>
					  <?php if ($_SESSION[G_SESSION]['num_role'] > 1) { ?>
					  <li><a href="<?= site_url('akses') ?>"> Akses</a> </li>
					  <?php } ?>
					  <li><a href="<?= site_url('logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
					</ul>
				  </li>
				</ul>
			<?php } else { ?>
			<ul class="notif">
				  <li class="dropdown"> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> <h5 class="glyphicon glyphicon-user" style="margin:0; color:#fff; padding:5px 0;"></h5> <span class=" fa fa-angle-down"></span> </a>
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
					  <li>
						<?php if (!xIsLoggedIn()) $login_strip->render(); ?>
					  </li>
					</ul>
				  </li>
				</ul>

			<?php } ?>
		</div>
    </header>

    <!--The mobile navigation Markup hidden via css-->
    <nav>
		<span><a href="<?= site_url('home') ?>"><img src="<?= base_assets() . 'images/logo-pjb-i.png' ?>" height="52px"></a></span><span style="color:#fff; font-weight:bold; font-size:14px;">Knowledge Management</span><br/>
        <ul>
            <li><a href="<?= site_url('home') ?>"><span class="label label-primary"><i class="fa fa-home"></i></span>&nbsp; Home</a></li>
			<li><a href="<?= site_url('profile') ?>"><span class="label label-primary"><i class="fa fa-user"></i></span>&nbsp; Profile</a></li>
			<li><a href="<?=$this->config->item('perpus_url');?>"><span class="label label-primary"><i class="fa fa-book"></i></span>&nbsp; Perpustakaan</a></li>
			<li><a href="<?= site_url('administrasi') ?>"><span class="label label-primary"><i class="fa fa-database"></i></span>&nbsp; Administrasi</a></li>
        </ul>
    </nav>
	<div id="contentLayer"></div>

    <div class="row-fluid">
      <!-- main right col -->
      <div class="column col-md-12 col-sm-12 col-xs-12" id="main">

        <!-- top nav -->
        <div class="header hidden-xs">
			<div class="container">
			<div class="col-md-2">
			<a href="<?= site_url('home') ?>"><img src="<?= base_assets() . 'images/logo-pjb-i.png' ?>" height="52px"></a>
			</div>
			<div class=" col-md-4" style="margin:11px auto">
			<div class="input-group text-center">
				<input type="text" class="form-control input-sm" id="cari_km_b" placeholder="Cari di knowledge management" value="<?= $_SESSION[G_SESSION]['cari_km'] ?>">
				<span class="input-group-btn">
					<button class="btn btn-sm btn-danger btn-remove-cari" type="button" style="display:<?= ($_SESSION[G_SESSION]['cari_km'])?'display':'none';?>"><i class="fa fa-remove"></i></button>
					<button class="btn btn-sm btn-primary" id="btn_cari_km_b" type="button"><i class="fa fa-search"></i></button>
				</span>
			 </div>
			 </div>
			<?php if (xIsLoggedIn()) { ?>
			<div class="pull-right">
				<ul class="notif">
				  <li class="dropdown"> 
					  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> 
						  <img src="<?= site_url('publ1c/profile?q='. xEncrypt($_SESSION[G_SESSION]['iduser'])) ?>&s=50" alt="" class="user-img"> 
						<?= $_SESSION[G_SESSION]['nama'] ?> <?= $_SESSION[G_SESSION]['namarole'] ? '('.$_SESSION[G_SESSION]['namarole'].')' : ''; ?><span class=" fa fa-angle-down"></span> 
					  </a>
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
					  <li><a href="<?= site_url('profile') ?>"> Profile</a> </li>
					  <?php if ($_SESSION[G_SESSION]['num_role'] > 1) { ?>
					  <li><a href="<?= site_url('akses') ?>"> Akses</a> </li>
					  <?php } ?>
					  <li><a href="<?= site_url('logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
					</ul>
				  </li>
				</ul>
			</div>
			<?php } else { ?>
				<?php if (!xIsLoggedIn()) $login_strip->render(); ?>
			<?php } ?>
			</div>
		</div>

		<?php //$menu->render() ?>

		<div style="padding-top:10px" id="content" class="row">
			<?php if (!$custom_form): ?>
			<form name="main_form" id="main_form" method="post" enctype="multipart/form-data">
			<input type="hidden" id="act" name="act" />
			<input type="hidden" id="key" name="key" />
			<input type="hidden" id="filter_num_record" name="filter_num_record" />
			<input type="hidden" id="filter_string" name="filter_string" />
			<input type="hidden" id="filter_sort" name="filter_sort" />
			<input type="hidden" id="filter_page" name="filter_page" />
			<input type="hidden" id="filter_field_search_list" name="filter_field_search_list" value="<?php echo $field_search_list; ?>" />
			<?php endif; ?>

			<?php $content->render(); ?>

			<?php if (!$custom_form): ?>
			</form>
			<?php endif; ?>



		</div>

		<!--/row-->
      </div>
      <!-- /col-9 -->
    </div>
    <!-- /padding -->
  </div>
  <!-- /main -->
<footer class="footer-container footer"><?php $footer->render(); ?></footer>

<!--
<br/>
<br/>
<div style="font-size:10px"><?php echo 'Memory: '.$this->benchmark->memory_usage();?></div>
<div style="font-size:10px"><?php echo 'Time: ' .$this->benchmark->elapsed_time();?></div>
-->

<style>
	#div-loader {
		height: 400px;
		position: fixed;
		background: transparent;
	}
	#ajax-loader {
		display:none;
		z-index: 1000;
		position: fixed;
		left: 0;
		top: 0;
		right: 0;
		bottom: 0;
		margin: auto;
	}
</style>
<img src="<?= base_assets() ?>images/loading.gif" id="ajax-loader">

<script>
	$( document ).ajaxStart(function() {
		$( "#ajax-loader" ).show();
	});

	$( document ).ajaxComplete(function() {
		//alert(1);
		$( "#ajax-loader" ).hide();
	});

	$('#btn_cari_km_a').click(function (e) {
		url = '<?= site_url('km/cari') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"q":$('#cari_km_a').val()
					},
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	})

	$('#cari_km_a').keypress(function(e){
		var p = e.which;
		if(p==13){
			$('#btn_cari_km_a').click();
		}
	})

	$('#btn_cari_km_b').click(function (e) {
		url = '<?= site_url('km/cari') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"q":$('#cari_km_b').val()
					},
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	})

	$('#cari_km_b').keypress(function(e){
		var p = e.which;
		if(p==13){
			$('#btn_cari_km_b').click();
		}
	})

	$('.btn-remove-cari').click(function (e) {
		document.location.href = '<?= site_url('km/removecari') ?>';
	})
	
	$(function() {
		$( "#cari_km_b" ).autocomplete({
			source: "<?php echo site_url('km/datacari'); ?>",
			minLength: 2,
			select: function( event, ui ) {
				if (ui.item) {
					$(this).css('background-color', '#efefef');
					$('#btn_cari_km_b').click();
				}
			},
			open: function (event, ui) {
				$(this).css('background-color', 'white');
			},
			close: function (event, ui) {
			}
		});	
	});

</script>
</body>
</html>
