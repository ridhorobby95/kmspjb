	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	
	<div class="col-sm-12 col-md-8" style="padding:0;">


		<div class="panel panel-default">
			
			<?= xPageTitle($page_title) ?>
	
			<div class="panel-heading">
			<span style="float:right"><?= xGetButton('lst'); ?></span>
			<div style="clear:both"></div>
			</div>

			<div class="panel-body">
			
				<?= $notification ?>
			
				<table class="table table-condensed">
				<tr>
					<td class="info" width="170">Kode Unit</td>
					<td style="vertical-align:middle"><?= xHTMLTextBox('kodeunit',$row['KODEUNIT'],30,30,false,'span2'); ?></td>
				</tr>   
				<tr>
					<td class="info">Nama</td>
					<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,80,$edited,'validate[required] span5'); ?></td>
				</tr> 

			</table>
			</div>
			
			<?php if ($edited): ?>
				<div class="panel-footer">
				<?php echo xShowButtonMode('save',$row[$id]);?>
				</div>
			<?php endif; ?>
					
			<br/>
			

		</div>
	</div>
	
<script>
	$(document).ready(function(){
		<?php if ($edited):?>
		$("#main_form").validationEngine();
		<?php endif; ?>
	});
</script>