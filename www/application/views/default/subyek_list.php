<script src="<?= base_assets() ?>js/filtertable.js"></script>


	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">
		<div class="panel panel-default">

			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span style="float:right"><?= xShowButtonMode($method); ?></span>
			<span class="pagination pagination-sm" style="float:left;margin-top:0"><?=$pagination?></span>
			<div style="clear:both"></div>
			</div>
			
			<div class="panel-body">
			<table class="table table-striped table-condensed">
		<tr class="info">
			<th style="width:20px">No.</th>
			<?php foreach ($field_list as $field): ?>
			<th <?=xSetFilterTable($ctl, $field);?>><?= $field['label'];?> <?= $field['sort'];?></th>
			<?php endforeach; ?>
			<th style="width:70px">Aksi</th>
		</tr>   
		<?php
			$i=$offset;
			foreach ($rows as $row) :
			$i++;
		?>
			<tr class="<?=($i%2===1)?'odd ':''?> <?=(!$row['ISACTIVE'])?'not_active':''?>" >
				<td><?=$i?></td>
				<td><a href="<?= site_url("$ctl/detail/{$row['IDSUBYEK']}"); ?>"><?= $row['NAMA']; ?></a></td>
				<td style="text-align:center">
					<?php if ($c_update) : ?><?php echo xEditList(site_url("$ctl/edit/{$row[$id]}")); ?><?php endif; ?>
					<?php if ($c_delete) {echo xDeleteList($row[$id], $row['NAMA']);} ?>
				</td>
			</tr>
		<?php endforeach; ?>
		
		<?php if (!count($rows)): ?>
			<tr><td colspan="<?=count($field_list)+1?>">Tidak ada data</td></tr>
		<?php endif; ?>
		
		</table>
			</div>
		
			<div class="panel-footer">
			<?= xFilterPaging($pagination, $filter_string, $num_start, $num_end, $num_all) ?>
			</div>
		</div>
	 
</div>

<script>
	function deleteList(id, desc) {
		if (confirm('<?php echo lang('are_you_sure_delete') ?> ('+desc+') ? '))
			location.href='<?php echo site_url("$ctl/delete") ?>/' + id ;
	}
</script>
