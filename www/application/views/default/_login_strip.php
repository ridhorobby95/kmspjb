<div class="pull-right">
<form class="form-inline" method="post" action="<?= site_url('login') ?>" style="padding:10px;text-align:right">
  <div class="form-group">
    <label class="sr-only" for="usernamelogin">Username</label>
    <input type="text" class="form-control" id="usernamelogin" name="usernamelogin" placeholder="Username">
  </div>
  <div class="form-group">
    <label class="sr-only" for="passwordlogin">Password</label>
    <input type="password" class="form-control" id="passwordlogin" name="passwordlogin" placeholder="Password">
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Ingat saya
    </label>
  </div>
  <input type="hidden" name="act_login" value="login" />
  <button type="submit" class="btn btn-default">Login</button>
</form>
</div>