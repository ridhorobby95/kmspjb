<?php if (xIsAuth('km', '1', 'ISCREATE')) : ?>

<link href="<?= base_assets(); ?>css/select2.min.css" rel="stylesheet" />
<script src="<?= base_assets(); ?>js/select2.min.js"></script>
<script src="<?= base_assets(); ?>js/jquery.tokeninput.js"></script> 

<div class="panel-heading add-header" style="padding-bottom:5px;">
	<ul class="btn-km">
	<?php foreach ($list_jenis as $jenis) { ?>
		<li>
		
		<span class="btn-knowledge" id="btn_tab_<?= $jenis['IDJENISKM'] ?>" data-km="<?= $jenis['IDJENISKM'] ?>">
		<!-- icon goes here -->
					<?php switch ($jenis['IDJENISKM']){
						case 1:
							echo '<span class="label label-success"><i class="fa fa-comment"></i></span>';
							break;
						case 2:
							echo '<span class="label label-warning"><i class="fa fa-pencil"></i></span>';
							break;
						case 3:
							echo '<span class="label label-danger"><i class="fa fa-file"></i></span>';
							break;
						case 4:
							echo '<span class="label label-info"><i class="fa fa-star"></i></span>';
							break;
						case 5:
							echo '<span class="label label-primary"><i class="fa fa-circle-o"></i></span>';
							break;
					}?>&nbsp; 
					<!-- / icon goes here -->
		<span class="hidden-xs" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= $jenis['SINGKAT'] ?></strong></span>
		
		<span class="visible-xs nama" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= substr($jenis['SINGKAT'],0,3) ?> <?= strlen($jenis['SINGKAT'])>3?'..':'' ?></strong></span>
		</span>
		<div class="arrow"></div>
		</li>
	<?php } ?>
	</ul>
	<?php if (!$edited): ?>
	<span class="fa fa-angle-up toggle-nav" id="navigation-toggle"></span>
	<?php endif; ?>
</div>

<form id="form_dialog" method="post" action="<?= site_url('km/action') ?>" class="dropzone" style="padding:0;margin:0">

<div id="knowledge_content">
	<input type="hidden" id="idpemateri_km" value="<?= $row['PEMATERI'] ?>" />
	<input type="hidden" id="idknowledge" value="<?= $row['IDKNOWLEDGE_H'] ?>" />
	<input type="hidden" id="idjenis_km" value="<?= $row['IDJENISKM'] ?>" />
	<div class="panel-body" id="knowledge_body" style="padding-top:0px;">
	<table class="table table-add table-condensed">
		<tr id="tr_nama_km"><td class="label-form">Nama</td><td><input type="text" id="nama_km" class="form-control input-md" value="<?= $row['NAMA'] ?>"  placeholder="..."/><i>URUTAN pada Nama KM bisa tidak sesuai jika ada user lain yang melakukan simpan data terlebih dahulu sebelum anda simpan data</i></td></tr>
		<tr id="tr_judul_km" style="display:none;margin-top:3px"><td class="label-form">Judul</td><td><input type="text" id="judul_km" class="form-control input-md" value="<?= $row['JUDUL'] ?>" placeholder="Apa judul topik yang ingin Anda bagikan?"/></td></tr>
		<tr id="tr_pelatihan_km">
			<td class="label-form">Nama Pelatihan</td>
			<td><input type="text" id="pelatihan_km" class="form-control input-md" value="<?= $row['PELATIHAN'] ?>" placeholder="Nama Pelatihan yang diikuti (tidak wajib diisi)"/></td>
		</tr>
		<tr id="tr_subyek_km" style="display:none;margin-top:3px"><td class="label-form">Subyek</td>
			<td><select id="idsubyek_km" name="idsubyek_km" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
			<?php foreach ($list_subyek as $subyek) { ?>
			<option value="<?= $subyek['IDSUBYEK'] ?>" <?php if ($method=='edit' && in_array($subyek['IDSUBYEK'], $arr_subyek)) { ?>selected<?php } ?>><?= $subyek['NAMA'] ?></option>
			<?php } ?>
			</select></td>
		</tr>
		<tr id="tr_subyek_pgd_km" style="display:none;margin-top:3px"><td class="label-form">Subyek</td>
			<td><select id="idsubyekpgd_km" name="idsubyekpgd_km" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
			<?php foreach ($list_subyek_pgd as $subyek) { ?>
			<option value="<?= $subyek['IDSUBYEK'] ?>" <?php if ($method=='edit' && in_array($subyek['IDSUBYEK'], $arr_subyek)) { ?>selected<?php } ?>><?= $subyek['NAMA'] ?></option>
			<?php } ?>
			</select></td>
		</tr>
		<tr id="tr_keywords_km">
			<td class="label-form">
				Keywords
			</td>
			<td>
				<select id="keywords_km" name="keywords_km" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
				<?php foreach ($keyword as $value) { ?>
					<option value="<?= $value['IDKEYWORDS'] ?>" <?php if ($method=='edit' && in_array($value['IDKEYWORDS'], $arr_keywords)) { ?>selected<?php } ?>><?= $value['KEYWORD'] ?></option>
				<?php } ?>
				</select>
			<td>
		</tr>
		<tr id="tr_referensi_km" style="display:none;margin-top:3px">
			<td class="label-form">
				Referensi
			</td>
			<td>
				<select id="referensi_km" name="referensi_km" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
				<?php foreach ($list_km_ref as $value) { ?>
					<option value="<?= $value['IDKNOWLEDGE_H'] ?>" <?php if ($method=='edit' && $value['IDKNOWLEDGE_H'] == $row['IDKNOWLEDGE_H']) { ?>selected<?php } ?>><?= $value['JUDUL'] ?></option>
				<?php } ?>
				</select>
			<td>
		</tr>
		<tr id="tr_unit_km" style="display:none;margin-top:3px"><td class="label-form">Unit</td><td>
			<select id="kodeunit_km" name="kodeunit_km" class="form-control input-md" style="border:0;width:200px">
			<?php foreach ($list_unit as $unit) { ?>
			<option value="<?= $unit['KODEUNIT'] ?>" <?php if ($method=='edit' && $unit['KODEUNIT'] == $row['KODEUNIT']) { ?>selected<?php } ?>><?= $unit['NAMA'] ?></option>
			<?php } ?>
			</select>
			</td></tr>	
		<tr id="tr_anggotatim_km" style="display:none;margin-top:3px"><td class="label-form">Anggota Tim</td><td><input type="text" id="anggotatim_km" name="anggotatim_km" class="form-control input-md"  placeholder="Siapa anggota tim yang terlibat?" /><td></tr>	
		<tr id="tr_pemateri_km" style="display:none;margin-top:3px">
			<td class="label-form" style="vertical-align:top"><?php if ($row['IDJENISKM'] == 3): ?>Penulis<?php else: ?>Pemateri<?php endif; ?></td>
			<td>
				<!-- <input type="text" id="pemateri_km" class="form-control input-md" value="<?= $row['NAMAPEMATERI']?$row['NAMAPEMATERI']:$row['PEMATERILUAR'] ?>"  placeholder="Siapa yang menyampaikan?"/>
				<input type="text" id="jabatanpemateri_km" class="form-control input-md" value="<?= $row['JABATANPEMATERI'] ?>"  placeholder="Jabatan pemateri" style="margin-top:3px"/>
				 -->
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tambahPemateri">
				  	Pemateri
				</button>
			</td>
		</tr>

		<tr id="tr_peserta_km">
			<td class="label-form">
				Peserta
			</td>
			<td>
				<select id="peserta_km" name="peserta_km" class="form-control input-md" multiple="multiple" style="border:0;width:200px">
				<?php foreach ($list_users as $value) { ?>
					<option value="<?= $value['IDUSER'] ?>" <?php if ($method=='edit' && in_array($value['IDUSER'], $row['peserta'])) { ?>selected<?php } ?>><?= $value['NAMA'] ?></option>
				<?php } ?>
				</select>
			<td>
		</tr>
		<tr id="tr_waktu_km" style="display:none;margin-top:3px"><td class="label-form">Pelaksanaan</td><td><input type="text" id="waktu_km" class="form-control input-md" value="<?= $row['WAKTUPELAKSANAAN_H2'] ?>" autocomplete="off" placeholder="Kapan waktu pelaksanannya?" /><td></tr>	
		<!-- <tr id="tr_jumlahpeserta_km" style="display:none;margin-top:3px"><td class="label-form">Peserta</td><td><input type="text" id="jumlahpeserta_km" class="form-control input-md" value="<?= $row['JUMLAHPESERTA'] ?>" placeholder="Jumlah peserta?" maxlength="5" style="width:120px;text-align:right"/><td></tr>	 -->
		<tr id="tr_tahun_km" style="display:none;margin-top:3px"><td class="label-form">Tahun Penulisan</td><td><input type="text" id="tahun_km" value="<?= $row['TAHUNTULIS'] ?>" class="form-control input-md"  placeholder="Tahun berapa materi tersebut ditulis?"/><td></tr>	
		<tr id="tr_abstrak_km" style="display:none;margin-top:3px"><td class="label-form">Abstrak</td><td><textarea id="abstrak_km" class="form-control" title="Abstrak" cols="90" rows="5"  placeholder="Deskripsi singkat materi.."><?= $row['ABSTRAK'] ?></textarea><td></tr>
		<tr id="tr_scorecard_km" style="display:none;margin-top:3px">
			<td class="label-form">
				Score Card
			</td>
			<td>
				<input type="number" data-toggle="tooltip" data-placement="top" title="Masukkan score card (angka desimal 0-5)" name="scorecard" id="scorecard" class="form-control input-md" step="0.1" value="<?= $row['SCORECARD'] ?>" placeholder="Score Card" min="0" max="5">
			<td>
		</tr>
		<tr id="tr_plan_km">
			<td class="label-form">Action Plan</td>
			<td><input type="text" id="plan_km" class="form-control input-md" value="<?= $row['ACTIONPLAN'] ?>" placeholder="Action Plan"/></td>
		</tr>
		<tr id="tr_plan_km">
			<td class="label-form">Progress</td>
			<td><input type="number" id="progress_km" class="form-control input-md" value="<?= $row['PROGRESS'] ?>" placeholder="Presentase"/> %</td>
		</tr>
		<tr id="tr_link" style="display:none;margin-top:3px">
			<td class="label-form">
				Link attachment
			</td>
			<td>
    			<div class="input-group">
			      <div class="input-group-addon" style="font-size: 10px">http://</div>
					<input type="text" name="link" id="link" style="z-index: 1;" class="form-control input-md" value="<?= $row['LINK'] ?>" placeholder="Link attachment">
			    </div>
			<td>
		</tr>
	</table>
	<?php if ($method == 'edit') { ?>
	<hr/>
	<div class="grid-foto">
		<?php 
			foreach ($row['list_foto'] as $foto) {
				include (dirname ( __FILE__ ) . '/_ajax_detail_foto.php');
			}
		?>
	</div>
	<div style="clear:both"></div>
	<hr/>
	<div class="grid-doc">
		<?php 
			foreach ($row['list_doc'] as $doc) {
				include (dirname ( __FILE__ ) . '/_ajax_detail_doc.php');
			}
		?>
	</div>
	<div style="clear:both"></div>
	<hr/>
	<div>
		<?php 
			foreach ($row['list_video'] as $video) {
				include (dirname ( __FILE__ ) . '/_ajax_detail_video.php');
			}
		?>
	</div>
	<div style="clear:both"></div>
	
	<?php } ?>
	
	</div>
	<div class="dz-default dz-message"><span></span></div>
	<div class="panel-footer row clearfix">
	<ul class="list-inline pull-left" style="margin:0">
	  <li class="btn btn_attach"><i class="fa fa-file" style="font-size:16px"  data-toggle="tooltip" data-placement="top" title="Upload file (max: <?= $this->config->item('max_file_size') ?>M)"></i></li>
	</ul>
	<div class="pull-right">
		<?php if ($method == 'edit')  {?><button class="btn btn-default btn-sm" type="button" onclick="location.href='<?= site_url('km/detail/'.$row['IDKNOWLEDGE_H']) ?>'">Batal</button> <?php } ?>
		<button class="btn btn-primary btn-sm" id="btn_submit_km" type="button"><strong><?php if ($method == 'edit')  {?> Simpan <?php } else { ?> Tambah <?php } ?></strong></button>
	</div>
	
	</div>
</div>

<div id="preview-template" style="display: none;"></div>

<div class="modal fade" id="tambahPemateri">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pemateri Knowledge</h4>
      </div>
      <div class="modal-body" id="modal_pemateri" style="background: #f3f3f3; padding: 10px 20px;">
      	<div class="row">
      		<div class="col-md-12">
      			<button type="button" class="btn btn-success" id="btn-add-pemateri"><i class="fa fa-plus"></i> Tambah Pemateri</button>
      		</div>
      	</div>
      	<?php if ($method == 'edit'): ?>
      		<?php foreach ($row['pemateri_knowledge'] as $key => $value): ?>
	      		<div class="row"><hr>
		        	<div class="col-md-2">
		        		<?php if ($key == 0): ?>
		        			Pemateri Utama
		        		<?php else: ?>
		        			<button type="button" class="delete-pemateri"><i class="fa fa-trash"></i></button>
		        		<?php endif ?>
		        	</div>
		        	<div class="col-md-10">
		        		<input type="hidden" name="idpemateri[]" value="<?= $value['IDPEMATERI'] ? $value['IDPEMATERI'] : 0 ?>" class="idpemateri_input">
						<input type="text" name="pemateri[]" class="pemateri_input form-control input-md" value="<?= $value['NAMAPEMATERI'] ?>"  placeholder="Siapa yang menyampaikan?"/>
						<input type="text" name="jabatanpemateri[]" class="jabatanpemateri_input form-control input-md" value="<?= $value['JABATANPEMATERI'] ?>"  placeholder="Jabatan pemateri" style="margin-top:3px"/>
		        	</div>
		        </div>
      		<?php endforeach ?>
      	<?php endif ?>
      </div>
      <div class="modal-footer">
      	<button type="button" data-dismiss="modal" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</div>
</form>

<script>
	$(document).ready(function(){

		<?php if ($method == 'edit') { ?>
			Dropzone.autoDiscover = false;
			$("#form_dialog").dropzone({
				url: '<?= site_url('km/uploadfile/'.$row['IDKNOWLEDGE_H']) ?>',
				dictDefaultMessage: "",
				dictFileTooBig: "Ukuran file terlalu besar ({{filesize}}M). Max: {{maxFilesize}}M.",
				previewTemplate: '<div id="preview-template" style="display: none;"></div>',
				//uploadMultiple: true,
				//parallelUploads: 10,
				//autoProcessQueue: false
				maxFilesize:<?= $this->config->item('max_file_size') ?>,
				clickable: ".btn_attach",
				init: function() {
					this.on("error", function(file, message) { 
						alert(message);
					});
					this.on("sending", function(file) { $( "#ajax-loader" ).show(); });
					this.on("complete", function(file) { $( "#ajax-loader" ).hide(); });
				},
				queuecomplete: function(file) {
					addFile();
				},
			});
			$('#btn_tab_'+<?= $row['IDJENISKM'] ?>).click();
			
		<?php } else { ?>
			Dropzone.autoDiscover = false;
			$("#form_dialog").dropzone({
				url: '<?= site_url('km/uploadfile') ?>',
				dictDefaultMessage: "",
				dictFileTooBig: "Ukuran file terlalu besar ({{filesize}}M). Max: {{maxFilesize}}M.",
				uploadMultiple: false,
				maxFilesize:<?= $this->config->item('max_file_size') ?>,
				addRemoveLinks: true,
				clickable: ".btn_attach",
				removedfile: function(file) {
					var name = file.name;        
					$.ajax({
						type: 'POST',
						url: '<?= site_url('km/deletefile') ?>',
						data: "file="+name,
						dataType: 'html'
					});
					var _ref;
					return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
				}
			});
		<?php } ?>
	
		//hoverFoto();
	});
	
	$('.btn-knowledge').click(function (e) {
		jeniskm = $(this).data('km');
		arrow = $(this).parent().find('div.arrow');
		$('#idjenis_km').val(jeniskm);
		
		toggleKM(jeniskm);
		
		if ($('.btn-knowledge').hasClass('active')) {
			$('.btn-knowledge').removeClass('active');
			$('.btn-knowledge').parent().find('div.arrow').hide();
			//$(this).addClass('btn-success');
			//$('#knowledge_body').hide();
			
		}
		$(this).addClass('active');
		arrow.show();
	});
	$('.btn-link').click(function() {
		$( "#judul_km" ).toggleClass('show');
	});

	function toggleKM(idjeniskm) {
		$('#nama_km').attr('readonly',true);
		$('#knowledge_body').show();
		$('#tr_judul_km').show();
		$('#tr_subyek_km').hide();
		$('#tr_subyek_pgd_km').hide();
		$('#tr_referensi_km').hide();
		$('#tr_unit_km').hide();
		$('#tr_anggotatim_km').hide();
		$('#tr_waktu_km').hide();
		$('#tr_tahun_km').hide();
		$('#tr_abstrak_km').hide();
		$('#tr_pemateri_km').hide();
		$('#tr_jumlahpeserta_km').hide();
		$('#tr_peserta_km').hide();
		$('#tr_scorecard_km').hide();
		

		$('#tr_link').show();

		updateNama(idjeniskm);
		if (idjeniskm == '1' || idjeniskm == '2' || idjeniskm == '5') {
			$('#tr_pemateri_km').show();
			$('#pemateri_div').html('Pemateri');
			$('#tr_waktu_km').show();
			$('#tr_subyek_km').show();
			$('#tr_jumlahpeserta_km').show();
			$('#tr_peserta_km').show();
		}
		if (idjeniskm == '2') {
			$('#tr_referensi_km').show();
		}
		if (idjeniskm == '3' || idjeniskm == '4') {
			$('#tr_abstrak_km').show();
		}
		
		if (idjeniskm == '3') {
			$('#tr_unit_km').show();
			$('#tr_pemateri_km').show();
			$('#pemateri_div').html('Penulis');
			$('#tr_tahun_km').show();
		}
		if (idjeniskm == '4') {
			$('#tr_anggotatim_km').show();
		}
		if (idjeniskm == '1' || idjeniskm == '2' || idjeniskm == '4' || idjeniskm == '5') {
			$('#tr_waktu_km').show();
			$('#tr_unit_km').show();
			$('#tr_scorecard_km').show();
		}
		if(idjeniskm == '5'){
			$('#tr_subyek_km').hide();
			$('#tr_subyek_pgd_km').show();
		}
		// if($idjeniskm == '5'){
		// 	$('#tr_subyek_km').hide();
		// 	// $('#tr_subyek_pgd_km').show();
		// }
		
		$('#tr_tambah').show();
	}
	
	function updateNama(idjeniskm) {
		<?php if ($row['NAMA']) : ?>
		return false;
		<?php endif; ?>
		if (!idjeniskm)
			return false;
		
		$('#nama_km').attr('disabled','true');
		
		ajaxurl = "<?php echo site_url('km/nextnama'); ?>/"+idjeniskm;
		$.ajax({
			type: "POST",
			url: ajaxurl,
			success: function(ret){
				$('#nama_km').removeAttr('disabled');
				$('#nama_km').val(ret);
			}
		});
	}

	var urut_pemateri = <?= $method == 'edit' ? count($row['pemateri_knowledge'])+1 : 1 ?>;
	$('#btn-add-pemateri').click(function() {
		var text = '<div class="row"><hr>\
        	<div class="col-md-2">';
        if (urut_pemateri == 1)
        	text += 'Pemateri Utama';
        else
        	text += '<button type="button" class="delete-pemateri"><i class="fa fa-trash"></i></button>';
        text +=	'</div>\
        	<div class="col-md-10">\
        		<input type="hidden" name="idpemateri[]" class="idpemateri_input">\
				<input type="text" name="pemateri[]" class="pemateri_input form-control input-md" placeholder="Siapa yang menyampaikan?"/>\
				<input type="text" name="jabatanpemateri[]" class="jabatanpemateri_input form-control input-md" placeholder="Jabatan pemateri" style="margin-top:3px"/>\
        	</div>\
        </div>';
		$('#modal_pemateri').append(text);
		urut_pemateri++;

		functionPemateri();

        return false;
	});
	<?php if($method != 'edit'){ ?>
		$('#btn-add-pemateri').click();
	<?php } else { ?>
		functionPemateri();
	<?php } ?>
	
	$(function() {
		$("#waktu_km").datepicker(
			{
				dateFormat:'dd-mm-yy',
				onClose: function () {this.focus();this.blur()}
			}
		);
		$("#idsubyek_km").select2({width: '100%'});
		$("#idsubyekpgd_km").select2({width: '100%'});
		$("#kodeunit_km").select2({placeholder: "Pilih unit kerja", width: '100%'});	
		$("#referensi_km").select2({placeholder: "Pilih referensi km", width: '100%'});
		$("#peserta_km").select2({placeholder: "Pilih peserta km", width: '100%'});
		$("#keywords_km").select2({placeholder: "Tambah keywords km", width: '100%', tags: true});
		$("#anggotatim_km").tokenInput("<?= site_url('km/usertag') ?>"<?php if ($anggotatim_json) { ?>, {prePopulate:<?= $anggotatim_json ?>} <?php } ?>);
	});
	
	$('#btn_submit_km').click(function (e) {
		idjeniskm = $('#idjenis_km').val();
		nama = $('#nama_km').val();
		judul = $('#judul_km').val();
		kodeunit = $('#kodeunit_km').val();
		abstrak = $('#abstrak_km').val();
		waktu = $('#waktu_km').val();
		tahun = $('#tahun_km').val();
		pemateri = $('input[name="pemateri[]"]').map(function(){return $(this).val();}).get();
		idpemateri = $('input[name="idpemateri[]"]').map(function(){return $(this).val();}).get();
		jabatanpemateri = $('input[name="jabatanpemateri[]"]').map(function(){return $(this).val();}).get();
		anggotatim = $('#anggotatim_km').val();
		idsubyek = $('#idsubyek_km').val();
		idsubyek_pgd = $('#idsubyekpgd_km').val();
		jumlahpeserta = $('#jumlahpeserta_km').val();
		scorecard = $('#scorecard').val();
		referensi_km = $('#referensi_km').val();
		peserta_km = $('#peserta_km').val();
		keywords_km = $('#keywords_km').val();
		pelatihan_km = $('#pelatihan_km').val();
		plan_km = $('#plan_km').val();
		progress_km = $('#progress_km').val();
		link = $('#link').val();
		
		if (!judul) {
			alert('Judul harus diisi');
			return false;
		}
		
		if (idjeniskm == '1' || idjeniskm == '2') {
			if (!idsubyek) {
				alert('Subyek harus diisi');
				return false;
			}
			if (!waktu) {
				alert('Waktu pelaksanaan harus diisi');
				return false;
			}
		}
		
		if(idjeniskm == '5'){
			if (!idsubyek_pgd) {
				alert('Subyek harus diisi');
				return false;
			}
			if (!waktu) {
				alert('Waktu pelaksanaan harus diisi');
				return false;
			}
		}
		if (idjeniskm == '1' || idjeniskm == '2' || idjeniskm == '3' || idjeniskm == '5') {
			if (!pemateri && !jabatanpemateri) {
				alert('Pemateri harus diisi');
				return false;
			}
		}

		if (idjeniskm == '3') {
			if (!tahun) {
				alert('Tahun penulisan harus diisi');
				return false;
			}
		}

		if (idjeniskm == '4') {
			if (!anggotatim) {
				alert('Anggota tim harus diisi');
				return false;
			}
		}

		<?php if ($method == 'edit') { ?>
		var str = 'Anda akan mengubah knowledge ini. Dilanjutkan?';
		<?php } else { ?>
		var str = 'Anda akan menambahkan knowledge baru. Dilanjutkan?';
		<?php } ?>
		if (!confirm(str))
			return false;
	
		<?php if ($method == 'edit') { ?>
			url = '<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>';
			$.ajax({
				type: "POST",
				url: url,
				data: {
						"act":"update_km", 
						"idjeniskm":idjeniskm, 
						"idsubyek":idsubyek, 
						"nama":nama, 
						"judul":judul, 
						"kodeunit":kodeunit, 
						"pemateri":pemateri, 
						"idpemateri":idpemateri, 
						"jabatanpemateri":jabatanpemateri, 
						"anggotatim":anggotatim, 
						"waktupelaksanaan":waktu, 
						"jumlahpeserta":jumlahpeserta, 
						"tahuntulis":tahun, 
						"abstrak":abstrak, 
						"scorecard":scorecard, 
						"referensi_km":referensi_km, 
						"peserta_km":peserta_km,
						"keywords_km":keywords_km,
						"pelatihan":pelatihan_km,
						"actionplan":plan_km,
						"progress":progress_km,
						"link":link,
						"idsubyekpgd":idsubyek_pgd
						},
				success: function(ret){
					if (ret == '0')
						window.location.replace("<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>");
					else
						alert(ret);
				}
			});		
		<?php } else { ?>
			url = '<?= site_url('km/add') ?>/'+idjeniskm;
			$.ajax({
				type: "POST",
				url: url,
				data: {
						"act":"add_km", 
						"idjeniskm":idjeniskm, 
						"idsubyek":idsubyek, 
						"nama":nama, 
						"judul":judul, 
						"kodeunit":kodeunit, 
						"pemateri":pemateri, 
						"idpemateri":idpemateri, 
						"jabatanpemateri":jabatanpemateri, 
						"anggotatim":anggotatim, 
						"waktupelaksanaan":waktu, 
						"jumlahpeserta":jumlahpeserta, 
						"tahuntulis":tahun, 
						"abstrak":abstrak, 
						"scorecard":scorecard, 
						"referensi_km":referensi_km, 
						"peserta_km":peserta_km,
						"keywords_km":keywords_km,
						"pelatihan":pelatihan_km,
						"actionplan":plan_km,
						"progress":progress_km,
						"link":link,
						"idsubyekpgd":idsubyek_pgd
						},
				success: function(ret){
					if (ret == '0')
						window.location.replace("<?= site_url('home') ?>");
					else
						console.log(ret);
				}
			});
		<?php } ?>
	})

	function toggleJenisKM(jenis) {
		$.ajax({
			type: "POST",
			url: "<?= site_url('publ1c/togglejeniskm') ?>",
			data: {"jenis":jenis },
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	}

	function functionPemateri() {
		$( ".pemateri_input" ).autocomplete({
			source: "<?php echo site_url('km/user'); ?>",
			minLength: 2,
			select: function( event, ui ) {
				var that = $(this);
				if (ui.item) {
					that.closest('.row').find(".idpemateri_input").val(ui.item.id);
					$(this).css('background-color', '#efefef');

					// SET JABATAN
					$.ajax({
						type: "POST",
						url: '<?= site_url('km/jabatan') ?>',
						data: {"id":ui.item.id},
						success: function(ret){
							that.closest('.row').find(".jabatanpemateri_input").val(ret);
						}
					});
				}
			},
			search: function() {
				$(this).closest('.row').find(".idpemateri_input").val('0');
			},
			open: function (event, ui) {
			},
			close: function (event, ui) {
			}
		});	
		$( ".pemateri_input" ).autocomplete( "option", "appendTo", "#tambahPemateri" );

		$('.delete-pemateri').click(function() {
			$(this).closest('.row').remove();
		});
	}

	function addFile() {
		url = '<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {	"act":"add_file"},
			success: function(ret){
				location.reload();
				/*
				if (ret == 'foto')
					getFoto();
				else if (ret == 'doc') 
					getDoc();
				else if (ret == 'video') {
					location.reload();
				}
				*/
			}
		});		

		return false;
	}
	
	function getFoto() {
		url = '<?= site_url('km/file/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {	"act":"foto"},
			success: function(ret){
				$('.grid-foto').append(ret).masonry( 'reloadItems' );
				$('.grid-foto').masonry('layout');
				hoverFoto();
			}
		});		
	}

	function getDoc() {
		url = '<?= site_url('km/file/' . $row['IDKNOWLEDGE_H']) ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: {	"act":"doc"},
			success: function(ret){
				$('.grid-doc').append(ret).masonry( 'reloadItems' );
				$('.grid-doc').masonry('layout');
				hoverFoto();
			}
		});		
	}

</script>

<?php endif; ?>
