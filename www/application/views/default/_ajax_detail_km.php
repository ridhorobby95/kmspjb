	<?php if (!$row || $row['ISDELETE']): ?>

	<div class="panel panel-default panel-content" style="height:450px">
		<div class="panel-heading">
			Knowledge tidak tersedia
		</div>
		<div class="panel-body">
			<h5>Knowledge tidak tersedia</h5>
			<br/><a href="<?= site_url('home') ?>">Kembali ke home</a>
		</div>
	</div>
	
	<?php else: 
	
	$is_condensed = $_SESSION[G_SESSION]['is_condensed'];
	if (isset($_SESSION[G_SESSION]['opsi_home_session'][$row['IDKNOWLEDGE_H']]['is_condensed']))
		$is_condensed = $_SESSION[G_SESSION]['opsi_home_session'][$row['IDKNOWLEDGE_H']]['is_condensed'];
	if ($ctl == 'km' && $method == 'detail')
		$is_condensed = false;
	?>
	<div class="panel panel-default panel-content panel<?= $row['IDJENISKM'] ?>">
		<?php if ($method == 'delete'): ?>
		<div class="panel-heading" style="background-color:#efefef">
			<span style="font-size:16px;color:#333">Knowledge ini akan dihapus: </span>
			<button type="button" class="btn btn-danger" id="btn_hapus_km">Hapus</button>
			<button type="button" class="btn btn-default" onclick="location.href='<?= site_url('km/detail/'. $row['IDKNOWLEDGE_H']) ?>'">Batal</button>
		</div>
		<script>
			$('#btn_hapus_km').click(function() {
				if (!confirm('Knowledge ini akan dihapus. Dilanjutkan?'))
					return false;
				url = '<?= site_url('km/delete/' . $row['IDKNOWLEDGE_H']) ?>';
				$.ajax({
					type: "POST",
					url: url,
					data: {	"act":"delete"},
					success: function(ret){
						if (ret != 'ERROR')
							location.href = '<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>';
						else
							alert('Data gagal dihapus. Silakan dicoba lagi atau hubungi Administrator');
					}
				});		
				
			});
		</script>
		<?php endif; ?>		
		<div class="panel-heading" style="padding-top:0px">
			<div class="post-heading prof-pic" style="margin-top:15px"> <a href="<?= site_url('publ1c/profile/id?q='. xEncrypt($row['CREATEDBY'])) ?>"><img src="<?= site_url('publ1c/profile?q='. xEncrypt($row['CREATEDBY'])) ?>&s=50"></a> </div>
			<div class="post-heading info" style="margin-top:15px"> <a href="<?= site_url('publ1c/profile/id?q='. xEncrypt($row['CREATEDBY'])) ?>"><strong><?= $row['NAMAUSER'] ?></strong> <?= $row['NAMAJABATAN'] ?></a><br/>
				<span><?= $row['MODIFIEDTIME_H'] ?></span>
			</div>
			<span class="tag hidden-xs"><a href="<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>"><?= $row['NAMA'] ?></a></span>
			<span class="tag visible-xs nama"><a href="<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>"><?= substr($row['NAMA'],0,3) ?></a></span>
			<?php if (xIsLoggedIn()) { ?>
			<div class="dropdown pull-right info">
				<span class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="margin-top:17px">
				<span class="caret"></span>
				</span>
				<ul class="dropdown-menu">
					<li><a href="<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>"><i class="glyphicon glyphicon-file"></i> Detail</a></li>
					<?php if (xIsAuth('km', '1', 'ISUPDATE')) { ?>
					<li><a href="<?= site_url('km/edit/' . $row['IDKNOWLEDGE_H']) ?>"><i class="glyphicon glyphicon-pencil"></i> Edit</a></li>
					<?php } ?>
					<?php if (xIsAuth('km', '1', 'ISDELETE')) { ?>
					<li><a href="<?= site_url('km/delete/' . $row['IDKNOWLEDGE_H']) ?>"><i class="glyphicon glyphicon-remove"></i> Hapus</a></li>
					<?php } ?>
					<li><a id-km="<?= $row['IDKNOWLEDGE_H'] ?>" class="refresh-post"><i class="glyphicon glyphicon-refresh"></i> Refresh</a></li>
					<?php if ($is_condensed) { ?>
					<li><a id-km="<?= $row['IDKNOWLEDGE_H'] ?>" class="exp-col-post"><i class="glyphicon glyphicon-chevron-down"></i> Expand</a></li>
					<?php } else { ?>
					<li><a id-km="<?= $row['IDKNOWLEDGE_H'] ?>" class="exp-col-post"><i class="glyphicon glyphicon-chevron-up"></i> Collapse</a></li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		</div>
		<div class="panel-info">
			
			<?php if ($row['WAKTUPELAKSANAAN']) { ?> 		
			<i class="fa fa-calendar"></i> &nbsp;
			<!--Pelaksanaan--> <b><?= $row['WAKTUPELAKSANAAN_H'] ?></b> &nbsp; | &nbsp; <?php } ?>			
			
			<?php if ($row['NAMAUNIT']) { ?> 					
			<i class="fa fa-building"></i> &nbsp;
			<!--Unit--> <b><?= $row['NAMAUNIT'] ?></b> &nbsp; | &nbsp;  <?php } ?>
			
			<?php if ($row['TAHUNTULIS']) { ?> 			 					
			<i class="fa fa-edit"></i> &nbsp;
			<b><?= $row['TAHUNTULIS'] ?></b> &nbsp; | &nbsp;  <?php } ?>
			<?php if ($row['peserta']) { ?> 			 					
			<i class="fa fa-user"></i> Peserta: 
			<b><a href="#" data-toggle="modal" data-target="#jumlahPeserta-<?=$row['IDKNOWLEDGE_H']?>"><?= count($row['peserta']) ?></a></b> &nbsp; | &nbsp;  <?php } ?>
			<div style="font-size:smaller">
				<i class="fa fa-user"></i> Pemateri :<br>
				<?php 
				if (($row['NAMAPEMATERI'] || $row['PEMATERILUAR']) || $row['pemateri_knowledge']) {
					if ($row['pemateri_knowledge']) {
						$pemateri_arr = array();
						foreach ($row['pemateri_knowledge'] as $key => $value) {
							if ($value['IDPEMATERI'])
								$pemateri_arr[] = '<a href="' . 'publ1c/0/profile/id?q='. xEncrypt($value['IDPEMATERI']) . '">' . $value['NAMAPEMATERI'] . ' ('.$value['JABATANPEMATERI'].')</a>';
							else
								$pemateri_arr[] = $value['NAMAPEMATERI'].' ('.$value['JABATANPEMATERI'].')';
						}
						$pemateri = implode(", <br>- ", $pemateri_arr);
					?>
						<b>- <?= $pemateri ?></b>
					<?php } else if ($row['NAMAPEMATERI'] || $row['PEMATERILUAR']) { 
						if ($row['PEMATERI'])
							$pemateri = '<a href="' . 'publ1c/0/profile/id?q='. xEncrypt($row['PEMATERI']) . '">' . $row['NAMAPEMATERI'] . '</a>';
						else
							$pemateri = $row['PEMATERILUAR'];
						?>
						<?php if ($row['IDJENISKM'] == '3') { ?><!--Penulis--> <?php } else { ?><!--Pemateri--> <?php } ?><b><?= $pemateri ?></b> 
					<?php } ?>
				<?php } ?>	
			</div>
		</div>
		<div class="panel-body">
			<?php if ($row['JUDUL']) { ?><h4><strong><a href="<?= site_url('km/detail/' . $row['IDKNOWLEDGE_H']) ?>"><?= $row['JUDUL'] ?></a></strong></h4><br/> <?php } ?>
			<?php if ($row['ABSTRAK']) { ?><p class="article-more"><?= $row['ABSTRAK'] ?></p> <?php } ?>
			<?php if ($row['SCORECARD']) { ?><p style="font-size: 12px;" data-toggle="tooltip" title="Score Card"><i class="fa fa-star"></i> <?= number_format($row['SCORECARD'], 1, '.', ',') ?></p> <?php } ?>
			
			<?php if (count($row['list_anggota'])) { ?>
			<br/>
				<span>Anggota Tim :</span>&nbsp;
				<?php 
					$j = 0;
					foreach ($row['list_anggota'] as $anggota) {
						$j++;
						$nama_anggota = '<a href="' . site_url('publ1c/profile/id?q='. xEncrypt($anggota['IDUSER'])) . '">' . $anggota['NAMA'] . '</a>';
						if ($j < count($row['list_anggota']))
							echo '<b>'. $nama_anggota . ',&nbsp; </b>';
						else
							echo '<b>'. $nama_anggota . '</b>';
					}
				?>
			<?php } ?>
			<?php if (count($row['list_subyek'])) { ?> 
				<p><?php foreach ($row['list_subyek'] as $subyek) { ?>
				<label class="label label-default"><?= $subyek['NAMA'] ?></label>
				<?php } ?>
				</p> 
			<?php } ?>

				<p><?php foreach ($row['list_keywords'] as $keywords) { ?>
				<label class="label label-keyword">#<?= $keywords['KEYWORD'] ?></label>
				<?php } ?>
				</p> 

			<?php if ($row['LINK']) { ?><hr/><i class="fa fa-link"></i> <a href="http://<?=$row['LINK']?>"> <?= $row['LINK'] ?></a> <?php } ?>
			<?php if (count($row['list_foto'])  && !$is_condensed) { ?>
				<hr/>
				<div class="grid-foto">
				<?php 
					foreach ($row['list_foto'] as $foto) {
						include (dirname ( __FILE__ ) . '/_ajax_detail_foto.php');
					}
				?>
				</div>
			<?php } ?>

			<?php if (count($row['list_doc']) && !$is_condensed) { ?>
				<hr/>
				<div class="grid-doc">
					<?php 
						foreach ($row['list_doc'] as $doc) {
							include (dirname ( __FILE__ ) . '/_ajax_detail_doc.php');
						}
					?>
				</div>
				<div style="clear:both"></div>
			<?php } ?>
			<?php if (count($row['list_video'])  && !$is_condensed) { ?>
				<hr/>
				<div>
				<?php 
					foreach ($row['list_video'] as $video) {
						include (dirname ( __FILE__ ) . '/_ajax_detail_video.php');
					}
				?>
				</div>
			<?php } ?>
			
			<?php if (xIsLoggedIn() && !$is_condensed) { ?>
			<div class="clearfix"></div>
			<div style="padding-top:10px; margin-top:10px; border-top:1px solid #f2f2f2">
				<?php 
					$btn_suka = 'btn-default';
					if ($row['SUKA']) 
						$btn_suka = 'btn-primary';
				?>
				<button class="btn <?= $btn_suka ?> btn-sm suka" id="suka-<?= $row['IDKNOWLEDGE_H'] ?>" val="<?= $row['IDKNOWLEDGE_H'] ?>"><i class="fa fa-thumbs-up"></i> Suka </button>
				<!-- <button class="btn btn-default btn-sm"><i class="fa fa-comment"></i> Komentar </button> -->
				<span id="stat-<?= $row['IDKNOWLEDGE_H'] ?>" style="margin-left:20px;color:#777">
				<?php include (dirname ( __FILE__ ) . '/_ajax_stat_km.php'); ?>
				</span>
				<div class="pull-right" style="margin-right:-10px">
					<a id-km="<?= $row['IDKNOWLEDGE_H'] ?>" class="exp-col-post" title="Collapse" style="float:right"><i class="glyphicon glyphicon-chevron-up"></i>	
					</a>
					<div style="clear:both"></div>
				</div>
			</div>
			<?php } ?>
		
		</div>

		<?php if (!$is_condensed) { ?>
		<div id="divposts-<?= $row['IDKNOWLEDGE_H'] ?>" style="background-color:#fafafa; padding:5px 0; border-top: 1px solid #ddd;<?php if (!count($row['list_komentar'])) { ?>display:none<?php } ?>">
		<ul class="listOfPosts" id="posts-<?= $row['IDKNOWLEDGE_H'] ?>">
			<?php foreach ($row['list_komentar'] as $komentar) { 
				include (dirname ( __FILE__ ) . '/_ajax_post_km.php');
			}
			?>			
		</ul>
		</div>
		<?php } ?>
		
		<?php if (xIsLoggedIn()  && !$is_condensed) { ?>
		<div class="panel-footer">
			<article class="media tim"> <a> <img src="<?= site_url('publ1c/profile?q='. xEncrypt($_SESSION[G_SESSION]['iduser'])) ?>&s=32" class="pull-left"> </a>
			  <div class="media-body">
				<input type="text" id="subpost-<?= $row['IDKNOWLEDGE_H'] ?>" class="form-control input-sm" placeholder="Tambahkan komentar..." onkeyup="subPost('<?= $row['IDKNOWLEDGE_H'] ?>',event)">
			  </div>
			</article>
		</div>
		
			
				
		<?php } ?>
		<?php if ($is_condensed) { ?>
		<div class="panel-footer" style="padding:5px;background-color:#fff">
		<a id-km="<?= $row['IDKNOWLEDGE_H'] ?>" class="exp-col-post" title="Expand" style="float:right"><i class="glyphicon glyphicon-chevron-down"></i></a>
		<div style="clear:both"></div>
		</div>
		<?php } ?>
		<div class="modal fade" id="jumlahPeserta-<?=$row['IDKNOWLEDGE_H']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Peserta (<?=count($row['peserta'])?> orang)</h4>
		      </div>
		      <div class="modal-body" style="background: #f3f3f3; padding: 10px 20px;">
		        <div class="row">	
		        	<div class="col-md-12">
		        		<ul>
		        			<?php foreach($row['peserta'] as $val) { ?>
		        			<li>
		        				<?= $val['NAMA'] ?>
		        			</li>
		        			<?php } ?>
		        		</ul>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
</div>

	<?php endif; ?>