<div class="container " > 

	<!-- content -->
	<div class="row-fluid"> 
  
	<!-- main col left -->
	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>

	<!-- main col right -->
	<div class="col-sm-12 col-md-10" style="padding:0;">
		<div class="panel panel-content">
		<div class="panel-heading">
			<h4 style="margin:0; font-weight:bold">Silahkan Pilih Menu di bawah ini</h4>
		</div>
		<div class="panel-body menu-grid">
		
		<div class="col-xs-6 col-md-4">
		
			<a href="<?= site_url('user') ?>">
		<div class="thumbnail">
			<h1 class="fa fa-group"></h1>
			<div class="caption">
				User
			</div>
			</a>
		</div>
		</div>
<!--		
		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('jabatan') ?>">
			<h1 class="fa fa-briefcase"></h1>
			<div class="caption">
				Jabatan
			</div>
			</a>
		</div>
		</div>
-->
		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('unit') ?>">
			<h1 class="fa fa-building"></h1>
			<div class="caption">
				Unit
			</div>
			</a>
		</div>
		</div>
	
		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('menu') ?>">
			<h1 class="fa fa-th"></h1>
			<div class="caption">
				Menu
			</div>
			</a>
		</div>
		</div>

		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('role') ?>">
			<h1 class="fa fa-key"></h1>
			<div class="caption">
				Role
			</div>
			</a>
		</div>
		</div>


		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('item') ?>">
			<h1 class="fa fa-list-ol"></h1>
			<div class="caption">
				Item
			</div>
			</a>
		</div>
		</div>


		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('subyek') ?>">
			<h1 class="fa fa-tag"></h1>
			<div class="caption">
				Stream
			</div>
			</a>
		</div>
		</div>

		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('km/lst') ?>">
			<h1 class="fa fa-file-pdf-o"></h1>
			<div class="caption">
				Daftar Knowledge
			</div>
			</a>
		</div>
		</div>

		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('laporan') ?>">
			<h1 class="fa fa-bar-chart"></h1>
			<div class="caption">
				Laporan
			</div>
			</a>
		</div>
		</div>

		<div class="col-xs-6 col-md-4">
		<div class="thumbnail">
			<a href="<?= site_url('adminsetting') ?>">
			<h1 class="fa fa-cogs"></h1>
			<div class="caption">
				Admin Setting
			</div>
			</a>
		</div>
		</div>
		
		</div>
		</div>
	</div>

	</div>

</div>
