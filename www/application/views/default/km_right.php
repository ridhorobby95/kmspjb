<div class="panel panel-default">
	<div class="panel-body">
		<p class="right-title">Konten Populer</p><br/>
		<div id="populer-div" class="grid-populer"></div>
		<script>
		$("#populer-div").load("<?= site_url('publ1c/populerkm') ?>", function() {
			$('.grid-populer').masonry({itemSelector: '.grid-item',gutter: 3});
			$("#populer-div").attr('style','height:auto !important;');
		});
		</script>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<p class="right-title">FILE TERBARU</p><br/>
		<div id="last-files-div" class="grid-doc"></div>
		<script>
		$("#last-files-div").load("<?= site_url('publ1c/lastfiles') ?>", function() {
			$('.grid-doc').masonry({itemSelector: '.grid-item',gutter: 3});
			$("#last-files-div").attr('style','height:auto !important;');
		});
		</script>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<p class="right-title">FOTO TERBARU</p><br/>
		<div id="last-photos-div"></div>
		<script>
		$("#last-photos-div").load("<?= site_url('publ1c/lastphotos') ?>", function() {
			$("a[rel^='prettyPhoto']").prettyPhoto({social_tools:false,deeplinking:false});
		});
		</script>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<p class="right-title">PENGUNJUNG</p>
		<select id="pengunjung-opsi" class="form-control" style="width:70%;font-size:11px;padding:0px;height:22px">
			<option value="1" <?= ($_SESSION[G_SESSION]['opsi_pengunjung'] == 1)?'selected':'' ?>>Hari ini</option>
			<option value="3" <?= ($_SESSION[G_SESSION]['opsi_pengunjung'] == 3)?'selected':'' ?>>Bulan ini</option>
			<option value="4" <?= ($_SESSION[G_SESSION]['opsi_pengunjung'] == 4)?'selected':'' ?>>Tahun ini</option>
		</select>
		<div id="pengunjung-div" style="margin-top:5px"></div>
		<script>
		$("#pengunjung-opsi").on('change', function() {
			opsi = $(this).val();
			$("#pengunjung-div").load("<?= site_url('publ1c/pengunjung') ?>/"+opsi, function() {});
		});
		$("#pengunjung-div").load("<?= site_url('publ1c/pengunjung') ?>", function() {});
		</script>
	</div>
</div>
