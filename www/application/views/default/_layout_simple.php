<!DOCTYPE html>
<html lang="id" dir="ltr">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= base_assets(); ?>images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?= base_assets(); ?>bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_assets(); ?>css/font-awesome.min.css" />
    <script src="<?= base_assets(); ?>js/jquery.js"></script> 
	<script src="<?= base_assets(); ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_assets(); ?>js/main.js"></script> 

    <title><?= $page_title?> <?php if (isset($page_subtitle)) echo " - $page_subtitle"; ?></title>

	
</head>

<body>
<div class="container-fluid">
	<?php $content->render(); ?>
</div>

</body>
</html>