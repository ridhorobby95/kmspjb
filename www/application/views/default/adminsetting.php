<div class="container " > 

	<!-- content -->
	<div class="row-fluid"> 
  
	<!-- main col left -->
	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>

	<!-- main col right -->
	<div class="col-sm-12 col-md-10" style="padding:0;">
		<div class="panel panel-content">
		<div class="panel-heading">
			<h4 style="margin:0; font-weight:bold">Cron Sinkronisasi</h4>
		</div>
		<div class="panel-body menu-grid">
		<table class="table">
			<tr>
				<td style="width:200px">KM - Sinkronisasi Data Ellipse</td>
				<td><a href="<?= site_url('publ1c/importellipse') ?>" target="_blank"><span class="btn btn-primary">Sinkronisasi</span></a></td>
			</tr>
			<tr>
				<td>KM - Rekap ranking knowledge</td>
				<td><a href="<?= site_url('publ1c/updatestatistik') ?>" target="_blank"><span  class="btn btn-primary">Rekap</span></a></td>
			</tr>
			<tr>
				<td>Perpustakaan - General</td>
				<td><a href="<?= $this->config->item('cronperpus') . '?page=crongeneral' ?>" target="_blank"><span  class="btn btn-primary">Perpustakaan - General</span></a></td>
			</tr>
			<tr>
				<td>Perpustakaan - Tagihan</td>
				<td><a href="<?= $this->config->item('cronperpus') . '?page=crontagihan' ?>" target="_blank"><span  class="btn btn-primary">Perpustakaan - Tagihan</span></a></td>
			</tr>
		</table>
		
		</div>
		</div>
	</div>

	</div>

</div>
