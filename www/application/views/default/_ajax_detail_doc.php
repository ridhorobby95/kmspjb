<div class="grid-item img_hover"  id="doc-<?= $row['IDKNOWLEDGE_H'] ?>-<?= $doc['IDLAMPIRAN'] ?>" title="<?= $doc['NAMAFILE'] ?>" alt="<?= $doc['NAMAFILE'] ?>">
	<?php if ($edited): ?>
		<div class="hover" style=""><button type="button" class="btn btn-xs btn-default btn_delete_doc" val="<?= $row['IDKNOWLEDGE_H'] ?>-<?= $doc['IDLAMPIRAN'] ?>"><i class="fa fa-remove"></i></button></div> 
	<?php endif; ?>
	<?php if ( $_SESSION[G_SESSION]['namarole'] == 'Staff' && $_SESSION[G_SESSION]['kodeunit'] != $row['KODEUNIT']){ ?>
	<div class="imagelay" data-content="<?= ellipsize($doc['NAMAFILE'], 20, .5) ?>">
	<?php } else{?>
	<div class="imagelay" <?php if (xIsLoggedIn()) { ?> onclick="window.open('<?= site_url('km/lampiran?q=' . rawurlencode(xEncrypt($doc['IDLAMPIRAN']))) ?>','_blank')" <?php } else { ?> onclick="alert('Silakan login terlebih dahulu untuk melihat dokumen ini')" <?php } ?> data-content="<?= ellipsize($doc['NAMAFILE'], 20, .5) ?>">
	<?php } ?>
	<img src="<?= site_url('publ1c/thumbnail/doc?q='. rawurlencode(xEncrypt($doc['IDLAMPIRAN']))) ?>" class="img-thumbnail" title="<?= $doc['NAMAFILE'] ?>" alt="<?= $doc['NAMAFILE'] ?>">
	</div>
</div>