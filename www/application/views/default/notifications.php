<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class="panel panel-default">
		<div class="panel-body" style="height:400px">
	<?= $notification ?>
	<table width="100%">
		<tr>
			<td style="vertical-align:top">
			<table class="table notifikasi table-condensed">
				<?php foreach ($dataNotif as $notif): ?>
					<?php if (trim($notif['STATUS']) == 'S'){
						$color = "#81D4FA";
					}
					else{
						$color = "white";
					}?>
				<tr><td class="hover_notif" style="background-color:<?php echo $color ?>"><a  href="<?php echo base_url('index.php/'.$notif['URL'].'/'.$notif['IDNOTIFICATIONS']) ?>" style="text-decoration: none; color: black"><?= $notif['MESSAGE'] ?><?php echo "<br>Waktu Pelaksanaan :".$notif['WAKTUPELAKSANAAN'] ?></a></strong></td>
				<?php endforeach ?>
				
				
	</table>
	<form id="form_profile" method="post" enctype="multipart/form-data"><input type="file" id="foto_profile" name="foto_profile" style="width:0px;" /></form>
	</div>
	</div>
</div>


<style>

.notifikasi .hover_notif:hover {
   background-color: #2196F3!important;
}

.img_hover {
    display: inline;
    position: relative;
}
.img_hover .hover {
    display: none;
    position: absolute;
    right:0;
    z-index: 2;
}


</style>
<?php if ($profile['IDUSER'] == $_SESSION[G_SESSION]['iduser']) { ?>
<script>
$(function() {
    $(".img_hover").hover(
        function() {
            $(this).children("img").fadeTo(100, 0.85).end().children(".hover").show();
        },
        function() {
            $(this).children("img").fadeTo(100, 1).end().children(".hover").hide();
		}
	);
	
	$("#btn_upload_foto").click(
		function () { $("#foto_profile").click();}
	);
	
	$('input[type=file]').change(function() {
		if (!confirm('Foto profile akan diganti. Dilanjutkan?'))
			return false;
		$("#form_profile").submit();
	});	
	
	$('.opsi').click(function() {
		var opsi = $(this).attr('name');
		var val = $(this).val();
		url = '<?= site_url('profile/opsi') ?>';
		$.ajax({
			type: "POST",
			url: url,
			data: { "id":<?=$_SESSION[G_SESSION]['iduser']?>,"opsi":opsi, "val":val},
			success: function(ret){
				if (ret != 'ERROR')
					alert(ret);
				else 
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
			}
		});		
	});
	
});
</script>
<?php } ?>
