<div class="col-md-12" style="padding:0">
<?php foreach ($last_files as $doc) { ?>
	<tr>
		<div class="col-xs-2 col-md-4" style="padding:0">
			<div class="" <?php if (xIsLoggedIn()) { ?> onclick="window.open('<?= site_url('km/lampiran?q=' . rawurlencode(xEncrypt($doc['IDLAMPIRAN']))) ?>','_blank')" <?php } ?> data-content="<?= ellipsize($doc['NAMAFILE'], 20, .5) ?>">
				<a><img src="<?= site_url('publ1c/thumbnail/doc?q='. rawurlencode(xEncrypt($doc['IDLAMPIRAN']))) ?>" class="img-thumbnail" title="<?= $doc['NAMAFILE'] ?>" alt="<?= $doc['NAMAFILE'] ?>"></a>
			</div>
		</div>
		<div  class="col-xs-10 col-md-8" style="padding-right:0">
			<div>
				<strong><a href="<?= site_url('km/detail/' . $doc['IDKNOWLEDGE_H']) ?>" title="<?= $doc['JUDULKNOWLEDGE'] ?>"><?= word_limiter($doc['JUDULKNOWLEDGE'],10) ?></a></strong><br/>
				<span class="label label<?= $doc['IDJENISKM'] ?>"><?= $doc['SINGKAT']?></span>
			</div>
		</div>
		<div class="clearfix" style="border-bottom:1px solid #eee; margin:5px 0"></div>
<?php } ?>
</div>
