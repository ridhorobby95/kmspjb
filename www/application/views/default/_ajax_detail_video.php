<div class="grid-item img_hover"  id="video-<?= $row['IDKNOWLEDGE_H'] ?>-<?= $video['IDVIDEO'] ?>" title="<?= $video['NAMAFILE'] ?>" alt="<?= $video['NAMAFILE'] ?>">
	<?php if ($edited): ?><div class="hover" style=""><button type="button" class="btn btn-xs btn-default btn_delete_video" val="<?= $row['IDKNOWLEDGE_H'] ?>-<?= $video['IDVIDEO'] ?>"><i class="fa fa-remove"></i></button></div> <?php endif; ?>
		<video width="320" height="240" poster="<?= site_url('publ1c/thumbnail/video?q='. rawurlencode(xEncrypt($video['IDVIDEO']))) ?>" controls>
			<source src="<?= base_url() ?>/_static/video/<?= $row['IDKNOWLEDGE_H'] ?>-<?= $video['NAMAFILE'] ?>" type="video/mp4">
			Browser anda tidak bisa membuka video ini
		</video>
</div>
