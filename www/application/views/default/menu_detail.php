	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">

			 <div class="panel panel-default">
			
			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span class="pull-left">
			<?php // echo xHTMLSelect('idmodul',$modulControl,$_SESSION[G_SESSION]['IDMODUL'],true,'form-control','',true); ?>
			</span>
			<span class="pull-right"><?= xShowButtonMode($method,$row[$id]); ?></span>
			<div style="clear:both"></div>
			</div>

			<div class="panel-body">
			
				<?= $notification ?>

				<?php if ($_SESSION[G_SESSION]['IDMODUL']) { ?>
				<table class="table table-condensed">
					<tr>
						<td class="info" width="170">Nama</td>
						<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,50,$edited); ?></td>
					</tr>   
					<tr>
						<td class="info">File/Controller</td>
						<td style="vertical-align:middle"><?= xHTMLTextBox('namafile',$row['NAMAFILE'],100,50,$edited); ?></td>
					</tr>   
					<tr>
						<td class="info">Lebar</td>
						<td style="vertical-align:middle">
							<?= xHTMLTextBox('lebar',$row['LEBAR'],10,10,$edited); ?>
							<?php if ($edited): ?>
							<span style="font-size:10px">[Berlaku untuk menu level 1]</span>
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td class="info">Parent</td>
						<td>
							<?= xHTMLSelect('idparent',$parentsControl,$row['IDPARENT'],$edited,'','',true); ?>
						</td>
					</tr>
					<tr>
						<td class="info">Relasi Hak Akses Item</td>
						<td>
							<?= xHTMLSelect('iditem',$itemsControl,$row['IDITEM'],$edited,'','',true); ?>
							:
							<?= xHTMLSelect('itemaccess',$itemaccessControl,$row['ITEMACCESS'],$edited,'','',true); ?>
						</td>
					</tr>
					<tr>
						<td class="info">Urutan</td>
						<td>
							<?= xHTMLSelect('urutan',$urutanControl,$row['URUTAN'],$edited,'','',true); ?>
						</td>
					</tr>
					<tr>
						<td class="info">Status</td>
						<td style="vertical-align:middle">
							<?php
								if ($edited): 
								if ($method === 'add')
									$row['ISACTIVE'] = 1;
							?>
							<?= xHTMLCheckBox('isactive','1',$row['ISACTIVE'],$edited); ?> <label for="isactive">Aktif</label>
							<?php
								else:
								echo xStatusAktif($row['ISACTIVE']);
								endif;
							?>
						</td>
					</tr>   
				</table>
				<?php } else { ?>
				<h3>Silakan pilih Modul yang ada di pilihan di atas</h3>
				<?php } ?>
				
		</div>
		<?php if ($edited): ?>
			<div class="panel-footer">
			<?php echo xShowButtonMode('save',$row[$id]);?>
			</div>
		<?php endif; ?>
	</div>
<script>
	$('#idmodul').change(function() {
		location.href='<?php echo site_url("$ctl/modul") ?>/' + $(this).val() ;
	});
</script>
