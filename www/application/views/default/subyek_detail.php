	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">

		<div class="panel-default">
			
			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span style="float:right"><?= xShowButtonMode($method,$row[$id]); ?></span>
			</div>

			<div class="panel panel-body">
			
				<?= $notification ?>
			
				<table class="table table-condensed">
					<tr>
						<td class="info" width="170">Nama</td>
						<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,50,$edited); ?></td>
					</tr>   
				</table>
			</div>
			<?php if ($edited): ?>
				<div class="panel-footer">
				<?php echo xShowButtonMode('save',$row[$id]);?>
				</div>
			<?php endif; ?>
		</div>
	</div>