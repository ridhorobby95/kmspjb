<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<script src="<?= base_assets(); ?>js/jquery.jscroll.js"></script> 
<script src="<?= base_assets(); ?>js/jquery-ui.min.js"></script> 
<link rel="stylesheet" href="<?= base_assets(); ?>css/dropzone.css" />
<script src="<?= base_assets(); ?>js/dropzone.js"></script> 

<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class="panel panel-default">

		<div class="panel panel-body">
			<?= xPageTitle($page_title) ?>

			<?= $notification ?>
			
			<div id="knowledge_content" class="form-group">
				<form id="form_dialog" method="post" action="<?= site_url('km/action') ?>" class="dropzone">
				<input type="hidden" id="idpemateri_km" />
				<input type="hidden" id="idunit_km" />
				<input type="hidden" id="idknowledge" />
				<input type="hidden" id="idjenis_km" />
				<h4 id="p_jenis_km"></h4>
				<div id="tr_nama_km" style="margin-top:3px" class="input-group col-md-5"><div class="input-group-addon" style="width:170px;text-align:left">Nama</div><input type="text" id="nama_km" class="form-control input-md" value="<?= $nama_km ?>" /></div>
				<div id="tr_judul_km" style="display:none;margin-top:3px" class="input-group col-md-10"><div class="input-group-addon" style="width:170px;text-align:left">Judul</div><input type="text" id="judul_km" class="form-control input-md" /></div>
				<div id="tr_subyek_km" style="display:none;margin-top:3px" class="input-group col-md-4"><div class="input-group-addon" style="width:170px;text-align:left">Subyek</div><select id="idsubyek_km" name="idsubyek_km" class="form-control input-md">
					<option></option>
					<?php foreach ($list_subyek as $subyek) { ?>
					<option value="<?= $subyek['IDSUBYEK'] ?>"><?= $subyek['NAMA'] ?></option>
					<?php } ?>
					</select></div>
				<div id="tr_unit_km" style="display:none;margin-top:3px" class="input-group col-md-5"><div id="unit_div" class="input-group-addon" style="width:170px;text-align:left">Unit</div><input type="text" id="unit_km" class="form-control input-md" /></div>	
				<div id="tr_anggotatim_km" style="display:none;margin-top:3px" class="input-group col-md-5"><div id="anggotatim_div" class="input-group-addon" style="width:170px;text-align:left">Anggota Tim</div><input type="text" id="anggotatim_km" class="form-control input-md" /></div>	
				<div id="tr_pemateri_km" style="display:none;margin-top:3px" class="input-group col-md-5"><div id="pemateri_div" class="input-group-addon" style="width:170px;text-align:left">Pemateri</div><input type="text" id="pemateri_km" class="form-control input-md" /></div>
				<div id="tr_waktu_km" style="display:none;margin-top:3px" class="input-group col-md-4"><div class="input-group-addon" style="width:170px;text-align:left">Waktu Pelaksanaan</div><input type="text" id="waktu_km" class="form-control input-md" /></div>
				<div id="tr_tahun_km" style="display:none;margin-top:3px" class="input-group col-md-3"><div class="input-group-addon" style="width:170px;text-align:left">Tahun Penulisan</div><input type="text" id="tahun_km" class="form-control input-md" /></div>
				<div id="tr_abstrak_km" style="display:none;margin-top:3px" class="input-group"><div class="input-group-addon" style="width:170px;text-align:left;vertical-align:top">Abstrak</div><textarea id="abstrak_km" class="form-control" title="Abstrak" cols="90" rows="5"></textarea></div>
				</form>
				<button class="btn btn-primary pull-left" id="btn_tambah_km" type="button">Tambah</button> &nbsp; <button class="btn btn-default"id="btn_attach"><i class="fa fa-paperclip"></i></button>
			</div>
			
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		Dropzone.autoDiscover = false;
		$("#form_dialog").dropzone({
			//paramName: 'photos',
			url: '<?= site_url('km/uploadfile') ?>',
			dictDefaultMessage: "",
			//clickable: false,
			enqueueForUpload: true,
			//maxFilesize: 1,
			//autoProcessQueue:false,
			uploadMultiple: false,
			addRemoveLinks: true,
			clickable: "#btn_attach",
			removedfile: function(file) {
				var name = file.name;        
				$.ajax({
					type: 'POST',
					url: '<?= site_url('km/deletefile') ?>',
					data: "file="+name,
					dataType: 'html'
				});
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
            }
		});
		
		toggleKM('<?=$idjeniskm?>');
	});
	
	
	function toggleKM(idjeniskm) {
		$('#tr_judul_km').show();
		$('#judul_km').focus();
		$('#tr_subyek_km').hide();
		$('#tr_unit_km').hide();
		$('#tr_anggotatim_km').hide();
		$('#tr_waktu_km').hide();
		$('#tr_tahun_km').hide();
		$('#tr_abstrak_km').hide();
		$('#tr_pemateri_km').hide();
		if (idjeniskm == '1' || idjeniskm == '2') {
			$('#tr_pemateri_km').show();
			$('#pemateri_div').html('Pemateri');
			$('#tr_waktu_km').show();
			$('#tr_subyek_km').show();
		}
		if (idjeniskm == '3' || idjeniskm == '4') {
			$('#tr_abstrak_km').show();
		}
		if (idjeniskm == '3') {
			$('#tr_pemateri_km').show();
			$('#pemateri_div').html('Penulis');
			$('#tr_tahun_km').show();
		}
		if (idjeniskm == '4') {
			$('#tr_waktu_km').show();
			$('#tr_unit_km').show();
			$('#tr_anggotatim_km').show();
		}
		$('#tr_tambah').show();
	}


	$('#btn_tambah_km').click(function (e) {
		if (!confirm('Anda akan menambahkan Knowledge baru. Dilanjutkan?'))
			return false;
		
		idjeniskm = $('#idjenis_km').val();
		nama = $('#nama_km').val();
		judul = $('#judul_km').val();
		idunit = $('#idunit_km').val();
		abstrak = $('#abstrak_km').val();
		waktu = $('#waktu_km').val();
		tahun = $('#tahun_km').val();
		pemateri = $('#idpemateri_km').val();
		idsubyek = $('#idsubyek_km').val();
		
		url = '<?= site_url('km/add') ?>/'+idjeniskm;
		$.ajax({
			type: "POST",
			url: url,
			data: {
					"act":"add_km", 
					"idjeniskm":idjeniskm, 
					"idsubyek":idsubyek, 
					"nama":nama, 
					"judul":judul, 
					"idunit":idunit, 
					"pemateri":pemateri, 
					"waktupelaksanaan":waktu, 
					"tahuntulis":tahun, 
					"abstrak":abstrak
					},
			success: function(ret){
				if (ret == '0')
					window.location.replace("<?= site_url('km/detail/'. $new_id) ?>");
				else
					alert(ret);
			}
		});
	})

	var refresh_nama_time = 10000;
	var interval_nama;
	interval_nama = setInterval(function ()	{updateNama();}, refresh_nama_time);
	updateNama();

	function updateNama() {
		ajaxurl = "<?php echo site_url('km/nextnama'); ?>/<?= $idjeniskm?>";
		$.ajax({
			type: "POST",
			url: ajaxurl,
			success: function(ret){
				$('#nama_km').val(ret);
				clearInterval(interval_nama);
				interval_nama = setInterval(function ()	{updateNama();}, refresh_nama_time);
			}
		});
	}
			
	$(function() {
		$( "#pemateri_km" ).autocomplete({
			source: "<?php echo site_url('km/user'); ?>",
			minLength: 2,
			select: function( event, ui ) {
				if (ui.item) {
					$('#idpemateri_km').val(ui.item.id);
				}
			},
			open: function (event, ui) {
				$(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
				$(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
			},
			close: function (event, ui) {
			}
		});	
		
		$( "#unit_km" ).autocomplete({
			source: "<?php echo site_url('km/unit'); ?>",
			minLength: 2,
			select: function( event, ui ) {
				if (ui.item) {
					$('#idunit_km').val(ui.item.id);
				}
			},
			open: function (event, ui) {
				$(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
				$(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
			},
			close: function (event, ui) {
			}
		});	

		$("#waktu_km").datepicker(
			{
				dateFormat:'dd-mm-yy',
				onClose: function () {this.focus();this.blur()}
			}
		);

	});
	
  
</script>