<?php if ($logged_in): ?>
<div class="container">
	<div class="col-md-6">
		Copyright &copy; 2015 PJB Knowledge Management. Powered by <a href="http://www.sevima.co.id" target="_blank"> Sevima</a>.
	</div>
	<div class="col-md-6">
			<?php if ($_SESSION[G_SESSION]['namaunit']) { ?>
			<div class="pull-left"><strong><?= $_SESSION[G_SESSION]['namarole'] . ' | ' . $_SESSION[G_SESSION]['namajabatan'] . ' | ' . $_SESSION[G_SESSION]['namaunit']?></strong></div>
			<?php } ?>

			<div class="pull-right">
				<a href="<?= base_url()."profile/".$_SESSION[G_SESSION]['username']?>" style="font-weight: bold"><?php echo $_SESSION[G_SESSION]['nama']  ?></a> |
				<a href="<?php echo site_url('logout'); ?>">Logout</a>
			</div>
	</div>
</div>

<?php endif; ?>
