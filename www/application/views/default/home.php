<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<script src="<?= base_assets(); ?>js/jquery.jscroll.js"></script> 
<script src="<?= base_assets(); ?>js/jquery-ui.min.js"></script> 
<link rel="stylesheet" href="<?= base_assets(); ?>css/dropzone.css" />
<script src="<?= base_assets(); ?>js/dropzone.js"></script> 

<div class="container"> 

	<!-- content -->
	<div class="row-fluid"> 
  
	<!-- main col left -->
	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	  <!-- main col right -->
	  <div class="col-sm-12 col-md-7" style="padding:0;">
  
		<?php if (xIsLoggedIn() && xIsAuth('km', '1', 'ISCREATE') ) { ?>
		<div id="tambah" class="panel panel-default">
				<div class="panel-heading">
					<b style="color: #333;">Tambah Knowledge Baru</b>
					<span class="fa fa-angle-down toggle-nav"></span>
				</div>
		</div>
		<div class="panel panel-default" id="add-form" style="display:none;">
			<?= $this->view('default/_km_form'); ?>
		</div>
		<?php } ?>

		<?php if (xIsLoggedIn()) { ?>
		<div class="panel panel-default">
		<div class="panel-body" style="padding:0 !important; margin:0 auto;">
				<ul class="nav nav-tabs filter" role="tablist">
					<li class="<?php if ($_SESSION[G_SESSION]['filter_km']['idjeniskm'] == '0' || !$_SESSION[G_SESSION]['filter_km']['idjeniskm']) echo 'active';  ?>" onclick="toggleJenisKM('0')"><a href="">
					<span class="label label-primary"><i class="fa fa-globe"></i></span>
					<strong>Semua</strong></a></li>
				<?php foreach ($list_jenis as $jenis) { ?>
					<li class="<?php if ($_SESSION[G_SESSION]['filter_km']['idjeniskm'] == $jenis['IDJENISKM']) echo 'active';  ?>"  onclick="toggleJenisKM('<?= $jenis['IDJENISKM'] ?>')">
					<a href="">
					<!-- icon goes here -->
					<?php switch ($jenis['IDJENISKM']){
						case 1:
							echo '<span class="label label-success"><i class="fa fa-comment"></i></span>';
							break;
						case 2:
							echo '<span class="label label-warning"><i class="fa fa-pencil"></i></span>';
							break;
						case 3:
							echo '<span class="label label-danger"><i class="fa fa-file"></i></span>';
							break;
						case 4:
							echo '<span class="label label-info"><i class="fa fa-star"></i></span>';
							break;
						case 5:
							echo '<span class="label label-primary"><i class="fa fa-circle-o"></i></span>';
							break;
					}?>&nbsp; 
					<!-- / icon goes here -->
					<span class="hidden-xs" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= $jenis['SINGKAT'] ?></strong>
					</span>
		
					<span class="visible-xs nama" data-toggle="tooltip" data-placement="top" title="<?= $jenis['NAMA'] ?>"><strong><?= substr($jenis['SINGKAT'],0,3) ?> <?= strlen($jenis['SINGKAT'])>3?'..':'' ?></strong></span>
					</a>
					</li>
				<?php } ?>
					<li class="pull-right"><a href="" data-toggle="modal" data-target="#filterTimeline">
					<span class="label label-default"><i class="fa fa-filter"></i></span>
					<strong>Filter</strong></a></li>
				</ul>
			</div>
		</div>
		<?php } ?>
			
		<div class="scroll">
			<?= $this->view('default/_ajax_list_km'); ?>
		</div>
	  </div>
	  <div class="col-sm-3 col-md-3 col-lg-3 right-bar">
		<?= $this->view('default/km_right'); ?>
      </div>

	</div>

</div>

<?= $this->view('default/_km_popup'); ?>

<?= $this->view('default/_km_js'); ?>

<script>
	$('.scroll').jscroll({
		loadingHtml: '<i class="fa fa-spinner fa-spin"></i>',
        nextSelector:'a.next-selector:last',
		autoTrigger: false,
	});

	$('body').ready(function(e){
		$('[data-km=1]').click();
		//$('[data-km2=<?= $_SESSION[G_SESSION]['filter_km']['idjeniskm'] ?>]').hover();
	});
	
	<?php if (!xIsLoggedIn()) { ?>
	$(document).on('click', 'a', function () { 
		alert('Silakan anda login terlebih dahulu untuk melihat dokumen ini');
		return false;
	});		
	<?php } ?>

	function toggleJenisKM(jenis) {
		$.ajax({
			type: "POST",
			url: "<?= site_url('publ1c/togglejeniskm') ?>",
			data: {"jenis":jenis },
			success: function(ret){
				window.location.replace("<?= site_url('home') ?>");
			}
		});
	}
	
</script>		  
