<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.theme.min.css" />
<link rel="stylesheet" href="<?= base_assets(); ?>css/jquery-ui.structure.min.css" />
<script src="<?= base_assets(); ?>js/jquery.jscroll.js"></script> 
<script src="<?= base_assets(); ?>js/jquery-ui.min.js"></script> 
<link rel="stylesheet" href="<?= base_assets(); ?>css/dropzone.css" />
<script src="<?= base_assets(); ?>js/dropzone.js"></script> 
<script src="<?= base_assets(); ?>js/readmore.min.js"></script> 

<div class="col-md-2">
	<?= $this->view('default/km_left'); ?>
</div>
<div class="col-md-10">
	<div class="panel panel-default">
	<?= $this->view('default/_km_form'); ?>
	</div>
</div>

<?= $this->view('default/_km_popup'); ?>

<?= $this->view('default/_km_js'); ?>
