	<div class="col-sm-2 col-md-2 col-lg-2">		
		<?= $this->view('default/km_left'); ?>
	</div>
	  
	 <!-- main col right -->
	 <div class="col-sm-12 col-md-8" style="padding:0;">
		<div class="panel panel-default">
			
			<?= xPageTitle($page_title) ?>

			<div class="panel-heading">
			<span style="float:right"><?= xShowButtonMode($method,$row[$id]); ?></span>
			<div style="clear:both"></div>
			</div>

			<div class="panel-body">
			
				<?= $notification ?>
			
				<table class="table table-condensed">
				<tr>
					<td class="info" width="170">Nama</td>
					<td style="vertical-align:middle"><?= xHTMLTextBox('nama',$row['NAMA'],100,50,$edited); ?></td>
				</tr>   
					<tr>
						<td class="info">Status</td>
						<td style="vertical-align:middle">
							<?php
								if ($edited): 
								if ($method === 'add')
									$row['ISACTIVE'] = 1;
							?>
							<?= xHTMLCheckBox('isactive','1',$row['ISACTIVE'],$edited); ?> <label for="isactive">Aktif</label>
							<?php
								else:
								echo xStatusAktif($row['ISACTIVE']);
								endif;
							?>
						</td>
					</tr>   
			</table>

			<br/>
			
			<?php if (count($items)) { ?>
			<table class="table table-striped table-condensed">
				<tr class="info">
					<th colspan="2">File Item</th>
					<th width="70">Read</th>
					<th width="70">Create</th>
					<th width="70">Update</th>
					<th width="70">Delete</th>
				</tr>
				<?php
					$i=0;
					foreach ($items as $item):
				?>
				<tr>
					<td><?php echo anchor(site_url('item/detail/'.$item['IDITEM']),$item['NAMA']);?></td>
					<td style="width:10px;background-color:#FFFF99">
						<?php if ($method !== 'detail'): ?>
							<?= xHTMLCheckBox('checkall[]','1','',$edited,'',"onClick=\"shiftCheckRow($i,this);\""); ?>
						<?php endif; ?>
					</td>
					<td style="text-align:center">
						<?= xHTMLCheckBox('isread[]',$item['IDITEM'],$item['ISREAD']?$item['IDITEM']:'',$edited); ?>
					</td>
					<td style="text-align:center">
						<?= xHTMLCheckBox('iscreate[]',$item['IDITEM'],$item['ISCREATE']?$item['IDITEM']:'',$edited); ?>
					</td>
					<td style="text-align:center">
						<?= xHTMLCheckBox('isupdate[]',$item['IDITEM'],$item['ISUPDATE']?$item['IDITEM']:'',$edited); ?>
					</td>
					<td style="text-align:center">
						<?= xHTMLCheckBox('isdelete[]',$item['IDITEM'],$item['ISDELETE']?$item['IDITEM']:'',$edited); ?>
					</td>
				</tr>
				<?php
					$i++;
					endforeach;
				?>
			</table>
			<?php } ?>

			</div>
			
			<?php if ($edited): ?>
				<div class="panel panel-footer">
				<?php echo xShowButtonMode('save',$row[$id]);?>
				</div>
			<?php endif; ?>

			<div class="panel-body">
			<?php
				if (count($users)) {
					echo '<br/>';
					echo '<h4>Penguna yang ada di role ini</h4>';
					$i = 0;
					echo '<div style="overflow:auto;width:650px">';
					echo '<table class="table table-condensed" style="width:600px">';
					foreach ($users as $user){
						echo "<tr>";
						$i++;
						echo "<td width=\"50px\">$i. </td>";
						echo '<td>' . anchor(site_url('user/detail/'.$user['IDUSER']), ucwords(strtolower($user['NAMAUSER']))) . '</td></tr>';
					}
					echo '</table></div>';
				}
			?>
			</div>
			
		</div>			
	</div>

</div>

<script>
	$('#main_form').submit(function() {

	});

	function shiftCheckRow(idx,call) {
		var checkstat = call.checked;
		
		var check_read = document.getElementsByName("isread[]");
		var check_create = document.getElementsByName("iscreate[]");
		var check_update = document.getElementsByName("isupdate[]");
		var check_delete = document.getElementsByName("isdelete[]");
		
		check_read[idx].checked = check_create[idx].checked = check_update[idx].checked = check_delete[idx].checked = checkstat;
	}
	
</script>